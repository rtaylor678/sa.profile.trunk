﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Solomon.ProfileII.WCFService
{
    public static class DbHelper
    {
        private static string _connectionString = ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString();

        public static DataSet QueryDb(string sqlString )
        {

        
         DataSet ds = new DataSet();
         SqlConnection SqlConnection1 = new SqlConnection();
  
         SqlConnection1.ConnectionString = _connectionString;

            //'Reads only the first sql statement. 'It's to gaurd against attacks on the data
            try
            {
                if (sqlString.Trim().StartsWith("SELECT"))
                {
                    SqlDataAdapter da = new SqlDataAdapter(sqlString, SqlConnection1);
                    da.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An database error just occurred." + sqlString);
            }
            finally
            {
                SqlConnection1.Close();
            }

            return ds;
        }
    }
}