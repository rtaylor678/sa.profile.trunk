<!-- config-start -->

<!-- config-end -->
<!-- template-start -->


<table class='small' width=100% border=0>
  <tr>
    <td colspan=12 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>

SECTION(DataCheck,,)
BEGIN
Header('
  <tr>
  <tr>
    <td colspan=12 height=60 valign=bottom><strong>@Header</strong></td>
  </tr>
  <tr>
    <td colspan=12>@DataCheckText</td>
  </tr>
  <tr>
    <td height=45 valign=bottom colspan=2 align=center valign=bottom><strong>@ItemDescHeader</strong></td>
    <td height=45 valign=bottom colspan=2 align=center valign=bottom><strong>@Value1Header</strong></td>
    <td height=45 valign=bottom colspan=2 align=center valign=bottom><strong>@Value2Header</strong></td>
    <td height=45 valign=bottom colspan=2 align=center valign=bottom><strong>@Value3Header</strong></td>
    <td height=45 valign=bottom colspan=2 align=center valign=bottom><strong>@Value4Header</strong></td>
    <td height=45 valign=bottom colspan=2 align=center valign=bottom><strong>@Value5Header</strong></td>
  </tr>')

  <tr>
    <td width=2% valign=bottom></td>
    <td width=38% valign=bottom>@ItemDesc</td>
    <td width=10% align=right valign=bottom>@Value1</td>
    <td width=2% valign=bottom></td>
    <td width=10% align=right valign=bottom>@Value2</td>
    <td width=2% align=right valign=bottom></td>
    <td width=10% align=right valign=bottom>@Value3</td>
    <td width=2% align=right valign=bottom></td>
    <td width=10% align=right valign=bottom>@Value4</td>
    <td width=2% align=right valign=bottom></td>
    <td width=10% align=right valign=bottom>@Value5</td>
    <td width=2% align=right valign=bottom></td>
  </tr>
  END
</table>
<!-- template-end -->
