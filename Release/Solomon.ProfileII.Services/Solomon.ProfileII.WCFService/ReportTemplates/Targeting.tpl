<!-- config-start -->

<!-- config-end -->
<!-- template-start -->

  <style>
    .Indent2{padding-left:10px;              vertical-align:text-bottom;}
    .Indent3{padding-left:20px;              vertical-align:text-bottom;}

    .Header0{padding-left: 0px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .Header1{padding-left:0px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .Header2{padding-left:10px; Height:10px; vertical-align:text-bottom; font-weight:700;}
    .Header3{padding-left:20px; Height:10px; vertical-align:text-bottom; font-weight:700;}
    .SubTot3{padding-left:30px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .SubTot4{padding-left:40px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .SubTot5{padding-left:50px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .hvy    {                                                            font-weight:700;}
  </style>

<table class='small' border=0 width=100%>

	<tr>
		<td width=158><b>RefList Name:</b></td>
		<td width=158><b>Rank Break:</b></td>
		<td width=158><b>Break Value:</b></td>
		<td width=158><b>Rank Variable:</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Top 2</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Break 1/2</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Break 2/3</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Break 3/4</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Bottom 2</b></td>
		<td>&nbsp;</td>
	</tr>

 
        <tr>
          <td p style="font-size:14px">Performance Indicators</td>
        </tr>

	SECTION(Table1,SectionHeader='Performance Indicators',SortKey ASC)
	BEGIN

	<tr>
		<td class="Indent2">@RefListName</td>
		<td>@RankBreak</td>
		<td align="left">@BreakValue</td>
		<td align="left">@RankVariable</td>
		<td>&nbsp;</td>
		<td align="left">Format(@Top2,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile1Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile2Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile3Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Btm2,'#,##0')</td>
		<td>&nbsp;</td>
	</tr>
	END
        <tr><td>&nbsp;</td></tr>

        <tr>
          <td p style="font-size:14px">Energy</td>
        </tr>

	SECTION(Table1,SectionHeader='Energy',SortKey ASC)
	BEGIN

	<tr>
		<td class="Indent2">@RefListName</td>
		<td>@RankBreak</td>
		<td align="left">@BreakValue</td>
		<td align="left">@RankVariable</td>
		<td>&nbsp;</td>
		<td align="left">Format(@Top2,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile1Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile2Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile3Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Btm2,'#,##0')</td>
		<td>&nbsp;</td>
	</tr>
	END

        <tr><td>&nbsp;</td></tr>

        <tr>
          <td p style="font-size:14px">Maintenance</td>
        </tr>

	SECTION(Table1,SectionHeader='Maintenance',SortKey ASC)
	BEGIN

	<tr>
		<td class="Indent2">@RefListName</td>
		<td>@RankBreak</td>
		<td align="left">@BreakValue</td>
		<td align="left">@RankVariable</td>
		<td>&nbsp;</td>
		<td align="left">Format(@Top2,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile1Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile2Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile3Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Btm2,'#,##0')</td>
		<td>&nbsp;</td>
	</tr>
	END

        <tr><td>&nbsp;</td></tr>

        <tr>
          <td p style="font-size:14px">Operating Expenses</td>
        </tr>

	SECTION(Table1,SectionHeader='Operating Expenses',SortKey ASC)
	BEGIN

	<tr>
		<td class="Indent2">@RefListName</td>
		<td>@RankBreak</td>
		<td align="left">@BreakValue</td>
		<td align="left">@RankVariable</td>
		<td>&nbsp;</td>
		<td align="left">Format(@Top2,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile1Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile2Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile3Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Btm2,'#,##0')</td>
		<td>&nbsp;</td>
	</tr>
	END

        <tr><td>&nbsp;</td></tr>

        <tr>
          <td p style="font-size:14px">Personnel</td>
        </tr>

	SECTION(Table1,SectionHeader='Personnel',SortKey ASC)
	BEGIN

	<tr>
		<td class="Indent2">@RefListName</td>
		<td>@RankBreak</td>
		<td align="left">@BreakValue</td>
		<td align="left">@RankVariable</td>
		<td>&nbsp;</td>
		<td align="left">Format(@Top2,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile1Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile2Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile3Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Btm2,'#,##0')</td>
		<td>&nbsp;</td>
	</tr>
	END

        <tr><td>&nbsp;</td></tr>

        <tr>
          <td p style="font-size:14px">Yields and Margins</td>
        </tr>

	SECTION(Table1,SectionHeader='Yields and Margins',SortKey ASC)
	BEGIN

	<tr>
		<td class="Indent2">@RefListName</td>
		<td>@RankBreak</td>
		<td align="left">@BreakValue</td>
		<td align="left">@RankVariable</td>
		<td>&nbsp;</td>
		<td align="left">Format(@Top2,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile1Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile2Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile3Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Btm2,'#,##0')</td>
		<td>&nbsp;</td>
	</tr>
	END

</table>
<!-- template-end -->