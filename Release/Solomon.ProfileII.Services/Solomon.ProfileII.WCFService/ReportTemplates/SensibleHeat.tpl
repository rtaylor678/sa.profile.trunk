<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class='small' width=100% border=0>
  <tr>
      <td width=10></td>
      <td width=90></td>
      <td width=250></td>
      <td width=125></td>
      <td width=125></td>
  </tr>

  <tr>
      <td colspan=2 align=Left valign=bottom><strong>Material ID</strong></td>
      <td align=Left valign=bottom><strong>Material Name</strong></td>
      <td align=right valign=bottom><strong>Total Barrels</strong></td>
      <td align=right valign=bottom><strong>Barrels per day</strong></td>
  </tr>
  <tr>
    <td height=30 colspan=5></td>
  </tr>

  SECTION(SensibleHeat,,SortKey ASC)
  BEGIN
  <tr>
      <td></td>
      <td align=left valign=bottom>@MaterialID</td>
      <td align=left valign=bottom>@MaterialName</td>
      <td align=right valign=bottom>NoShowZero(Format(BBL,'#,##0'))</td>
      <td align=right valign=bottom>NoShowZero(Format(BPD,'#,##0'))</td>
  </tr>
  END
</table>
<!-- template-end -->