﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Data;

namespace Solomon.Profile.ChartService
{
    internal class ChartBuilder
    {
        public Chart BuildChart(ChartInfo chartInfo, DataSet dataSet, ref Chart chartTemp) 
        {
            //Chart chartTemp = new Chart();
            ChartArea area = new ChartArea();
            area.Name = "ChartArea1";
            area.BorderWidth = 0;
            chartTemp.ChartAreas.Add(area);

            for (int counter = 0; counter < chartInfo.SeriesList.Count; counter++)
            {
                ChartPageSeries thisSeries = chartInfo.SeriesList[counter];

                thisSeries.Legend.DockingPosition = Docking.Bottom;
                thisSeries.Legend.Alignment = System.Drawing.StringAlignment.Center;

                Series newSeries = new Series(thisSeries.NameToUse);
                newSeries.ChartType = thisSeries.ChartType;
                
                if (thisSeries.Legend != null)
                {
                    Legend legend = new Legend();
                    legend.Docking = thisSeries.Legend.DockingPosition;
                    legend.Alignment = thisSeries.Legend.Alignment;
                    chartTemp.Legends.Add(legend);
                }
                
                newSeries.Color = thisSeries.Color;
                
                if (thisSeries.XAxisAngle != 0)
                    chartTemp.ChartAreas[counter].AxisX.LabelStyle.Angle = thisSeries.XAxisAngle;
                
                ChartPageSeriesPoints points = thisSeries.Points;
                newSeries.MarkerSize = points.Size;
                newSeries.MarkerStyle = points.Shape;
                newSeries.MarkerColor = points.Color;

                if (thisSeries.ChartType.ToString().Contains("Polar") || thisSeries.ChartType.ToString().Contains("Radar"))
                {
                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable);
                    List<double> y = PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    newSeries.Points.DataBindXY(x, y);
                }
                else
                {
                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable);
                    List<double> y =PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    newSeries.Points.DataBindXY(x,y);
                }
                chartTemp.Series.Add(newSeries);
            }                                   
            //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = True;
            if (chartTemp.Legends.Count > 0)
                chartTemp.Legends[0].Enabled = true;
            /*
            if (chartTemp.Series[0].ChartType.ToString().Contains("Polar") || chartTemp.Series[0].ChartType.ToString().ToUpper().Contains("RADAR"))
            {
                chartTemp.Series[0]["RadarDrawingStyle"] = "Line";
                chartTemp.Series[0].BorderDashStyle = ChartDashStyle.Dash;
                chartTemp.Series[0]["AreaDrawingStyle"] = "Polygon";
                chartTemp.Series[0]["CircularLabelsStyle"] = "Horizontal";
            }
            */
            
            return chartTemp;
        }

        public void BuildRadarChart(RadarChartInfo chartInfo,
            DataSet dataSet, ref Chart radarChart) 
        {
            //radarChart = new Chart();
            ChartArea area = new ChartArea();
            area.Name = "ChartArea1";
            area.BorderWidth = 0;
            radarChart.ChartAreas.Add(area);

            for (int counter = 0; counter < chartInfo.SeriesList.Count; counter++)
            {
                RadarChartPageSeries thisSeries = chartInfo.SeriesList[counter];
                Series newSeries = new Series(thisSeries.NameToUse);
                newSeries.ChartType = thisSeries.ChartType;
                newSeries.Name = thisSeries.NameToUse;
                if (thisSeries.Legend != null)
                {
                    Legend legend = new Legend();
                    legend.Docking = thisSeries.Legend.DockingPosition;
                    legend.Alignment = thisSeries.Legend.Alignment;
                    radarChart.Legends.Add(legend);
                }

                newSeries.Color = thisSeries.Color;

                if (thisSeries.XAxisAngle != 0)
                    radarChart.ChartAreas[counter].AxisX.LabelStyle.Angle = thisSeries.XAxisAngle;

                RadarChartPageSeriesPoints points = thisSeries.Points;
                newSeries.MarkerSize = points.Size;
                newSeries.MarkerStyle = points.Shape;
                newSeries.MarkerColor = points.Color;

                if (thisSeries.ChartType.ToString().ToUpper().Contains("RADAR"))
                {

                    //thisSeries.XColumnDataTable.

                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable, true);
                    List<double> y = null;
                    if (thisSeries.FinalYValues == null)
                    {
                        y = PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    }
                    else
                    {
                        y = thisSeries.FinalYValues; 
                        //chg from 0 to 8 for higher visibility on chart
                        for (int i=0; i<y.Count; i++)
                        {
                            if (y[i]< 8)
                                y[i] = 8;
                        }                         
                    }

                    //"Enumeration has either not started or has already finished."
                    try
                    {
                        //System.Diagnostics.Debug.Print(newSeries.Name);
                        newSeries.Points.DataBindXY(x, y);
                    }catch(Exception problem)
                    {
                        if (x.Count != y.Count)
                        {
                            string breakPoint = string.Empty;
                        }
                        string msg = problem.Message;
                    }
                }
                else
                {
                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable);
                    List<double> y = PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    newSeries.Points.DataBindXY(x, y);
                }
                if (thisSeries.SeriesRadarBorderDashStyle != ChartDashStyle.NotSet)
                {
                    newSeries["RadarDrawingStyle"] = "Line";
                    newSeries.BorderWidth = 2;  //line width
                    //newSeries.BorderDashStyle = thisSeries.SeriesRadarBorderDashStyle;
                    
                }
                radarChart.Series.Add(newSeries);
            }
            /*
            radarChart.Series[0]["RadarDrawingStyle"] = "Line";
            radarChart.Series[0].BorderDashStyle = ChartDashStyle.Dash;
            radarChart.Series[0]["AreaDrawingStyle"] = "Polygon";
            radarChart.Series[0]["CircularLabelsStyle"] = "Horizontal";
            */
            radarChart.ChartAreas[0].AxisY.MajorGrid.Enabled = false; //removes the target-style circles
            //radarChart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            //radarChart.ChartAreas[0].AxisY.LabelStyle.Format = "C";
            radarChart.ChartAreas[0].AxisY.LabelStyle.Enabled = false;
            
            //no effect on visible part.  radarChart.Height = 800;
            

            //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = True;
            if (radarChart.Legends.Count > 0)
                radarChart.Legends[0].Enabled = true;
        }
        private List<double> rowsOrdinals(DataTable dataTable)
        {
            List<double> result = new List<double>();
            int counter = -1;
            foreach (DataRow row in dataTable.Rows)
            {
                counter++;
                result.Add(counter);
            }
            return result;            
        }
        
        public List<double> PointsToBind(string YColumnName, System.Data.DataTable dataTable) //e, int tableNumberToUse = 0)
        {
            List<double> result = new List<double>();
            for (int counter = 0; counter < dataTable.Rows.Count; counter++)
            {
                if (dataTable.Rows[counter][YColumnName] != DBNull.Value)
                {
                    result.Add(double.Parse(dataTable.Rows[counter][YColumnName].ToString()));
                    // var test = dataTable.Rows[counter][columnPosition];
                    //result.Add((double)test);
                }
                else
                {
                    result.Add(0);
                }
            }
            return result;            
        }

        public List<string> PointsToBindString(int columnPosition, System.Data.DataTable dataTable, bool isRadarChart=false) //, int tableNumberToUse = 0)
        {
            List<string> result = new List<string>();
            for (int counter = 0; counter < dataTable.Rows.Count; counter++)
            {
                if (dataTable.Rows[counter][columnPosition] != DBNull.Value)
                {
                    if (!isRadarChart)
                    {
                        result.Add(dataTable.Rows[counter][columnPosition].ToString());
                    }
                    else
                    {
                        //if text too long, it shrinks chart so you can't see it. Fix this later.
                        //result.Add(((string)dataTable.Rows[counter][columnPosition].ToString()).Substring(0, 1));
                        result.Add((string)dataTable.Rows[counter][columnPosition].ToString());
                    }
                }
                else
                {
                    result.Add(string.Empty);
                }
            }
            return result;
        }

        /// <summary>
        /// Takes list of points, puts them in hi to low order
        /// </summary>
        /// <param name="listsIn"></param>
        /// <returns>List<List<double>></returns>
        public List<List<double>> OrderHiToLow(List<List<double>> listsIn)
        {
            List<List<double>> lists = listsIn;
            int itemsCount = lists[0].Count;

            int row = 0; //quartile values across ALL kpis
            //int col = 0; //each diff kpi

            //check each row to see if it is hi to low or low to hi
            for (row = 0; row <= lists[0].Count - 1; row++)
            {
                double firstListRowValue = lists[0][row];
                double lastListRowValue = lists[lists.Count - 1][row];
                if (firstListRowValue < lastListRowValue)
                {
                    //Change each to 0 to 100
                    List<double> temp = new List<double>();
                    //create temp list in reverse order
                    for (int i = lists.Count - 1; i >= 0; i--)
                    {
                        temp.Add(lists[i][row]);
                    }
                    //reassign starting list with temp list values
                    for (int i = 0; i < lists.Count; i++)
                    {
                        lists[i][row] = temp[i];
                    }
                }
            }
            return lists;
        }

        public List<List<double>> HighToLow_OLD(List<List<double>> listsIn, int avgTgtYtdSeriesCount)
        {
            int quartileListsCount = listsIn.Count - avgTgtYtdSeriesCount;
            List<List<double>> listsOut = new List<List<double>>();
            foreach (var l in listsIn)
            {
                listsOut.Add(new List<double>());
            }
            int kpiCount = listsIn[0].Count;
            //loop thru each kpi column of all lists, 
            //collect the #s in a new list
            for (int kpi = 0; kpi < kpiCount; kpi++)
            {
                List<double> lst = new List<double>();
                //loop thru each list
                //for (int i = 0; i < 5; i++)
                for (int i = 0; i < listsIn.Count; i++)
                {
                    lst.Add(listsIn[i][kpi]);
                }
                lst.Sort();
                //add column back in to outgoing lists
                int counter = 0;
                //for (int i = 4; i >=0; i--)
                for (int i = listsIn.Count - 1; i >= 0; i--)
                {
                    double d = lst[i];
                    listsOut[counter].Add(d);
                    counter++;
                }
                /*
                I don't think I need this; why not add them in above????
                //add back in the mo,av, ytd
                //-there might not be any of these
                for (int i = quartileListsCount; i < listsIn.Count; i++)
                {
                    listsOut[i].Add(listsIn[i][kpi]);
                }                    
                */
            }
            return listsOut;
        }

        public List<List<double>> HighToLow(List<List<double>> listsIn)//, int avgTgtYtdSeriesCount)
        {
            int quartileListsCount = listsIn.Count;// - avgTgtYtdSeriesCount;
            List<List<double>> listsOut = new List<List<double>>();
            for(int i=0;i< quartileListsCount;i++)// each(var l in listsIn)
            {
                listsOut.Add(new List<double>());
            }
            int kpiCount = listsIn[0].Count;
            //loop thru each kpi column of all lists, 
            //collect the #s in a new list
            for (int kpi = 0; kpi < kpiCount; kpi++)
            {
                List<double> lst = new List<double>();
                //loop thru each list
                //for (int i = 0; i < 5; i++)
                for (int i = 0; i < listsIn.Count; i++)
                {
                    lst.Add(listsIn[i][kpi]);
                }
                lst.Sort();
                //add column back in to outgoing lists
                int counter = 0;
                //for (int i = 4; i >=0; i--)
                for (int i= listsIn.Count - 1; i >=0; i--)
                {
                    double d = lst[i];
                    listsOut[counter].Add(d);
                    counter++;
                }
                /*
                I don't think I need this; why not add them in above????
                //add back in the mo,av, ytd
                //-there might not be any of these
                for (int i = quartileListsCount; i < listsIn.Count; i++)
                {
                    listsOut[i].Add(listsIn[i][kpi]);
                }                    
                */
            }
            return listsOut;
        }

        public List<List<double>> ZeroTo100a1(List<List<double>> listsIn, ref List<Multiplier> zeroTo100DivisorList)
        {
            //conver to 100 thru 0
            List<List<double>> lists = new List<List<double>>(); // listsIn;

            foreach (var lst1 in listsIn)
            {
                List<double> lstNew = new List<double>();
                foreach (double dbl in lst1)
                {
                    //List<double> lst = lst2;
                    lstNew.Add(dbl);
                }
                lists.Add(lstNew);
            }

            int row = 0; //quartile values across ALL kpis
            //int col = 0; //each diff kpi
            for (row = 0; row <= lists[0].Count - 1; row++)
            {
                List<double> temp = new List<double>();
                double highVal = lists[0][row];
                double lowVal = lists[lists.Count - 1][row];
                double spread = highVal - lowVal; // lists[0][row] - lists[lists.Count-1][row]; //lists[lists.Count - 1][row];
                double divisor = spread * 100;
                double lowValue = lists[lists.Count - 1][row];
                Multiplier mult = new Multiplier();
                mult.Spread = spread;
                mult.LowValue = lowValue;
                zeroTo100DivisorList.Add(mult);
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    List<double> thisList = lists[i];
                    double temp2 = thisList[row];
                    double temp5 = (temp2 - lowValue) / spread * 100;
                    temp.Add(temp5);
                }
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    lists[i][row] = temp[i];
                }
            }
            return lists;
        }

        //this changes a range of #s to 10-0
        public List<double> QuartileAvgMoYtdZeroTo100(List<double> listIn)
        {
            List<double> listOut = new List<double>();
            int listCount = listIn.Count;
            //make temp list to sort high to low
            List<double> sorts = new List<double>();
            foreach (double d in listIn)
            {
                sorts.Add(d);
            }
            sorts.Sort();  //sorts low to high
            //calc spread between low and high
            double spread = sorts[sorts.Count - 1] - sorts[0];
            //if low < 0, 
            double bumpUp = 0;
            //if (sorts[0] < 0)
            //{

                bumpUp = 0 - sorts[0];
            //}
            //else
            //{
            //    bumpUp = 0 - sorts[0];
            //}
            foreach (double d in listIn)
            {
                //if low < 0,
                //-make new list and add a # to each in list so low is now 0
                //divide each # in list by the spread, add to listOut
                listOut.Add(((d + bumpUp) / spread) * 100);
            }
            return listOut;
        }

        public List<List<double>> ZeroTo100Test(List<List<double>> listsIn)
        {
            //8 lists; P want only 1s5 5 to not be 0.
            List<List<double>> listsOut = new List<List<double>>();
            //for (int i = 0; i < 5; i++)
            
                List<double> lst = new List<double>{100, 100, 100};
                listsOut.Add(lst);
                lst = new List<double>{80,80,80};
                listsOut.Add(lst);
                lst = new List<double> { 60, 60, 60 };
                listsOut.Add(lst);
                lst = new List<double> { 40, 40, 40 };
                listsOut.Add(lst);
                lst = new List<double> { 20, 20, 20 };
                listsOut.Add(lst);

                lst = new List<double> { 0, 0, 0 };
                listsOut.Add(lst);
                lst = new List<double> { 0, 0, 0 };
                listsOut.Add(lst);
                lst = new List<double> { 0, 0, 0 };
                listsOut.Add(lst);
                return listsOut;
            //conver to 100 thru 0
        }

        //fits them into new list where 100 is high and 0  is low.
        public List<List<double>> ZeroTo100(List<List<double>> listsIn)
        {
            //conver to 100 thru 0
            List<List<double>> lists = listsIn;
            int row = 0; //quartile values across ALL kpis
            //int col = 0; //each diff kpi
            for (row = 0; row <= lists[0].Count - 1; row++)
            {
                List<double> temp = new List<double>();
                double spread = lists[lists.Count - 1][row] - lists[0][row];
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    if (i == 0)
                    {
                        temp.Add(100);
                    }
                    else if (i == lists.Count - 1)
                    {
                        temp.Add(0);
                    }
                    else
                    {
                        double tempValue = 0;
                        tempValue = lists[lists.Count - 1][row] - lists[i][row];
                        tempValue = tempValue / spread;
                        tempValue = tempValue * 100;
                        temp.Add(tempValue);
                    }
                }
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    lists[i][row] = temp[i];
                }
            }
            return lists;
        }

        /*
        public List<List<double>> ZeroTo100(List<List<double>> listsIn, ref List<double> zeroTo100MultiplierList)
        {
            //conver to 100 thru 0
            List<List<double>> listsOut   = listsIn;
            int row = 0; //quartile values across ALL kpis
            //int col = 0; //each diff kpi
            for (row = 0; row <= lists[0].Count - 1; row++)
            {
                List<double> temp = new List<double>();
                double spread = lists[lists.Count - 1][row] - lists[0][row];
                double divisor = spread / 100;
                zeroTo100MultiplierList.Add(1 / spread * 100);
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    if (i == 0)
                    {
                        temp.Add(100);
                    }
                    else if (i == lists.Count - 1)
                    {
                        temp.Add(0);
                    }
                    else
                    {
                        double tempValue = 0;
                        tempValue = lists[lists.Count - 1][row] - lists[i][row];
                        tempValue = tempValue / spread * 100;
                        temp.Add(tempValue);
                    }
                }
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    lists[i][row] = temp[i];
                }
            }
            return lists;
        }
        */
        /*
         public List<List<double>> ZeroTo100a(List<List<double>> listsIn, ref List<Multiplier> zeroTo100DivisorList)
            {
                //conver to 100 thru 0
                List<List<double>> lists = new List<List<double>>(); // listsIn;

                foreach (var lst1 in listsIn)
                {
                    List<double> lstNew = new List<double>();
                    foreach (double dbl in lst1)
                    {
                        //List<double> lst = lst2;
                        lstNew.Add(dbl);
                    }
                    lists.Add(lstNew);
                }

                    int row = 0; //quartile values across ALL kpis
            //int col = 0; //each diff kpi
            for (row = 0; row <= lists[0].Count - 1; row++)
            {
                List<double> temp = new List<double>();
                double spread = lists[0][row] - lists[lists.Count - 1][row];
                double divisor = spread * 100;
                double lowValue = lists[lists.Count - 1][row];
                Multiplier mult = new Multiplier();
                mult.Spread = spread;
                mult.LowValue = lowValue;
                zeroTo100DivisorList.Add(mult);
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    List<double> thisList = lists[i];
                    double temp2 = thisList[row];
                    double temp5 = (temp2 - lowValue) / spread * 100;
                    temp.Add(temp5);
                }
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    lists[i][row] = temp[i];
                }
            }

            
            return lists;
        }
      */
        /*
        public List<List<double>> ZeroTo100b(List<List<double>> listsIn, ref List<double> zeroTo100DivisorList)
        {
            //conver to 100 thru 0
            List<List<double>> listsOut  = new List<List<double>>(listsIn);
            int row = 0; //quartile values across ALL kpis
            //int col = 0; //each diff kpi
            for (row = 0; row <= listsOut[0].Count - 1; row++)
            {
                List<double> temp = new List<double>();
                double spread = lists[0][row] - lists[lists.Count - 1][row];
                double divisor = spread * 100;
                double lowValue = lists[lists.Count - 1][row];
                zeroTo100DivisorList.Add(spread * 100);
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    List<double> thisList = lists[i];
                    double temp2 = thisList[row];
                    double temp5 = (temp2 - lowValue) / spread * 100;
                    temp.Add(temp5);
                }
                for (int i = 0; i <= lists.Count - 1; i++)
                {
                    lists[i][row] = temp[i];
                }
            }
            return lists;
        }
        */
    }    

    public class ChartInfo
    {
        public List<ChartPageSeries> SeriesList { get; set; }
    }
    public class ChartPageSeries
    {
        public int SeriesNumber {get;set;}
        public DataTable XColumnDataTable { get; set; }
        public DataTable YColumnDataTable {get;set;}
        public ChartPageLegend Legend {get;set;}
        public SeriesChartType ChartType { get; set; }
        public int XColumnNumber { get; set; }
        public string YColumnName { get; set; }
        public System.Drawing.Color Color { get; set; }
        public int XAxisAngle { get; set; }
        public string NameToUse { get; set; }
        public ChartPageSeriesPoints Points {get;set;}  
    }

    public class ChartPageSeriesPoints
    {
        public MarkerStyle Shape  { get; set; }
        public System.Drawing.Color Color  { get; set; }
        public int Size  { get; set; }
    }
    public class ChartPageLegend
    {
        public Docking DockingPosition {get;set;}
        public System.Drawing.StringAlignment Alignment  {get;set;}
    }    
}
