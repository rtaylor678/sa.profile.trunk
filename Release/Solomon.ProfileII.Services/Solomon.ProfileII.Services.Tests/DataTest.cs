﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Solomon.ProfileII.Contracts;
using Solomon.ProfileII.Services;
using System.Configuration;
using System.Web;



namespace Solomon.ProfileII.Services.Tests
{
   
    [TestClass]
    public class DataTest
    {
        private string _db10Conx = string.Empty;
        private string _db12Conx = string.Empty;
        private string _filesDownloadUploadTestFolderPath = @"C:\Projects\z4.6\Solomon.ProfileII.Services\Solomon.ProfileII.Services.Tests\bin\Debug\6.0.0.0";
        private string _clientKey = "X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=";
        private string _userHostAddress = "TEST USER HOST ADDRESS";


        [TestInitialize]
        public void startup()
        {
            _db10Conx = ConfigurationManager.ConnectionStrings["Profile10Conx"].ToString();
            _db12Conx = ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString();
        }


        [TestMethod]
        public void DataCheckSvcTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            Assert.IsTrue(svc.CheckService().Contains("Success"));
        }
        [TestMethod]
        public void DataRefineryIs2012Test()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            Assert.IsTrue(svc.RefineryIs2012("XXPAC"));
        }

        [TestMethod]
        public void DataGetLookupsTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetLookups("XXPAC", "123");
            Assert.IsTrue(ds.Tables[0].Rows.Count > 0);
        }

        [TestMethod]
        public void DataGetDataDumpTest()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetDataDump("EII", "Actual", "2012", "USD", startDate, "US", 2016, true, true, true, "XXPAC");
            Assert.IsTrue(ds.Tables.Count > 0);
        }

        [TestMethod]
        public void DataGetReferencesTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetReferences("XXPAC");
            Assert.IsTrue(ds.Tables[0].Rows.Count > 0);
        }


        [TestMethod]
        public void FileCheckServiceTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            Assert.IsTrue(svc.CheckService().Contains("Success"));
        }

        [TestMethod]
        public void FileDownloadTplFileTest()
        {

            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            if (!System.IO.Directory.Exists(_filesDownloadUploadTestFolderPath))
            {
                System.IO.Directory.CreateDirectory(_filesDownloadUploadTestFolderPath);
            }
            if (!System.IO.File.Exists(_filesDownloadUploadTestFolderPath + "\\Table1.tpl"))
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(_filesDownloadUploadTestFolderPath + "\\Table1.tpl"))
                {
                    for(int i=0;i<100;i++)
                    {
                        writer.WriteLine("TEST");
                    }
                }
            }
            byte[] result = svc.DownloadTplFile("Table1.tpl", "6.0.0.0", "X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            Assert.IsTrue(result.Length > 100);
            System.IO.Directory.Delete(_filesDownloadUploadTestFolderPath, true);
        }

         [TestMethod]
        public void FileUploadFileTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            byte[] bytes = new byte[] { 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45 };
            Assert.IsTrue(svc.UploadFile("Table1.tpl",bytes , _clientKey).Length<1);
        }

        
        [TestMethod]
        public void SubmitGetDataByPeriodTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DateTime start = new DateTime(2015, 6, 1);
            DateTime finish = new DateTime(2016, 6, 1);
            DataSet ds = svc.GetDataByPeriod(start,finish,"XXPAC");
            Assert.IsTrue(ds != null);
            Assert.IsTrue(ds.Tables.Count ==25);
            Assert.IsTrue(ds.Tables[0].TableName == "Settings");
            Assert.IsTrue(ds.Tables[24].TableName == "OpexAll");
        }

        [TestMethod]
        public void SubmitGetInputDataTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetInputData ("XXPAC");
            Assert.IsTrue(ds != null);
            Assert.IsTrue(ds.Tables.Count ==23);
            Assert.IsTrue(ds.Tables[0].TableName == "Settings");
            Assert.IsTrue(ds.Tables[22].TableName == "UserDefined");
        }

        [TestMethod]
        public void SubmitSubmitRefineryDataTest()
        {
            Assert.Inconclusive("We don't really want to create a new submission in Prod");
            //throw new Exception("We don't really want to create a new submission in Prod");

            //ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx,"");
            //svc.SubmitRefineryData  (start, finish, "XXPAC");
        }


    }
}
