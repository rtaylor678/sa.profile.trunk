﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.ProfileII.Services;
using System.Collections.Generic;
using Solomon.ProfileII.Logger;


namespace Solomon.ProfileII.Services.Tests
{
    [TestClass]
    public class ReportsTests
    {
        [TestMethod]
        public void AreCalcsDoneTest()
        {
            ProfileReportsManager rpts = new ProfileReportsManager("XXPAC");
            PrivateObject po = new PrivateObject(rpts);
            object[] p = new object[] { "ACTUAL", "6-1-2016", "XXPAC" };
            Assert.IsTrue((bool)po.Invoke("AreCalcsDone", p));
        }

        [TestMethod]
        public void GetListOfChartRowsTest()
        {
            ProfileReportsManager rpts = new ProfileReportsManager("XXPAC");
            PrivateObject po = new PrivateObject(rpts);
            object[] p = new object[] { "Chart_LU" };
            object result = po.Invoke("GetListOfChartRows", p);
            IList<string> test = (List<string>)result;
            Assert.IsTrue(test.Count > 5);
        }
        /*
        [TestMethod]
        public void RemoveRowsTest()
        {

        }
        */
        [TestMethod]
        public void GetOrdinalsTest()
        {
            ProfileReportsManager rpts = new ProfileReportsManager("XXPAC");
            PrivateObject po = new PrivateObject(rpts);
            object[] p = new object[] { "000102" };
            object result = po.Invoke("GetOrdinals", p);
            IList<int> test = (List<int>)result;
            Assert.IsTrue(test.Count == 3);
            Assert.AreEqual(test[0], 0);
            Assert.AreEqual(test[1], 1);
            Assert.AreEqual(test[2], 2);
        }

        [TestMethod]
        public void GetColocTest()
        {
            Assert.Inconclusive("Need to populate _dataSet in GetCoLoc() ");
            ProfileReportsManager rpts = new ProfileReportsManager("XXPAC");
            PrivateObject po = new PrivateObject(rpts);
            object[] p = new object[] { "06-01-2016", "XXPAC" };
            object result = po.Invoke("GetCoLoc", p);
            Assert.IsTrue(result.ToString() == "Solomon Refining - Singapore");
        }

        /*
        [TestMethod]
        public void GetDataSetForReportTest()
        {
            ProfileReportsManager rpts = new ProfileReportsManager();
            PrivateObject po = new PrivateObject(rpts);
            object[] p = new object[] { "XXPAC", "EII and VEI Breakdown", 2016,6,"ACTUAL","2014", "scenario","USD","US","1" };
            object result = po.Invoke("GetDataSetForReport", p);

        }
        */

        [TestMethod]
        public void GetReportTest()
        {
            string qs = "rn=Refinery Scorecard&sd=6/1/2016&ds=ACTUAL&UOM=US&currency=SGD&yr=2014&target=True&avg=True&ytd=True&SN=2014&TS=6/14/2017 3:06:51 PM&rows=302303304&WsP=X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=";
            ProfileReportsManager mgr = new ProfileReportsManager("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            string result = mgr.GetReportHtml(qs, "XXPAC");
            Assert.IsFalse(result.Contains("Error"));

        }

        [TestMethod]
        public void LoggerTest()
        {
            Logger.ProfileLogManager.LogInfo("Message from ReportsTests.cs");
            Assert.IsTrue(1 == 1);
        }

        [TestMethod]
        public void QuerystringTest()
        {
            string queryString = "rn=Report Name&yr=2016&wsp=abcd";
            Assert.IsTrue(Utilities.GetQuerystringVariable(queryString, "rn") == "Report Name");
            Assert.IsTrue(Utilities.GetQuerystringVariable(queryString, "rn") == "2016");
            Assert.IsTrue(Utilities.GetQuerystringVariable(queryString, "rn") == "abcd");
        }

        [TestMethod]
        public void EncryptionTEst()
        {
            Byte[] bytes = new Byte[] { 1, 2 };
            //Solomon.Common.Cryptography.Integrity.Asymmetric.Sign(null, null, out bytes);


        }

        [TestMethod]
        public void TestRefScorecardNewShort()
        {
            ProfileReportsManager mgr = new ProfileReportsManager("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            //string queryString = "sd=6/1/2016&SN=2014&currency=SGD&UOM=MET&rows=102110115132133140";
            string queryString = "sd=6/1/2016&SN=2014&currency=SGD&UOM=MET&rows=110300";
            string html = mgr.GetRefineryScorecardReport("XXPAC",queryString);
            Assert.IsTrue(html.Length > 0);
        }

        [TestMethod]
        public void TestRefScorecardNewAll()
        {
            ProfileReportsManager mgr = new ProfileReportsManager("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            string queryString = "sd=6/1/2016&SN=2014&currency=SGD&UOM=MET&rows=";
            string html = mgr.GetRefineryScorecardReport("XXPAC", queryString); //calls _Orig
            Assert.IsTrue(html.Length > 0);
        }

        [TestMethod]
        public void TestGetRefineryTrendsReportNewll()
        {
            ProfileReportsManager mgr = new ProfileReportsManager("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            string queryString = "sd=6/1/2016&SN=2014&currency=SGD&UOM=MET&rows=";
            string html = mgr.GetRefineryTrendsReport("XXPAC", queryString);
            Assert.IsTrue(html.Length > 0);

        }

        [TestMethod]
        public void TestGetRefineryTrendsReportNewShort()
        {
            ProfileReportsManager mgr = new ProfileReportsManager("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            string queryString = "sd=6/1/2016&SN=2014&currency=SGD&UOM=MET&rows=110300"; // 102110115132133140";
            string html = mgr.GetRefineryTrendsReport("XXPAC", queryString);
            Assert.IsTrue(html.Length > 0);
            
        }

        [TestMethod]
        public void GetTargetingReportTest()
        {
            // string qs = "rn=Targeting&sd=6/1/2016&ds=ACTUAL&UOM=US&currency=SGD&yr=2014&target=True&avg=True&ytd=True&SN=2014&TS=7/6/2017 2:29:49 PM&peers=6806"; //  peers=15105";
            string qs = "rn=Targeting&sd=4/1/2017&ds=ACTUAL&UOM=US&currency=USD&yr=2014&target=True&avg=True&ytd=True&SN=CLIENT&TS=7/20/2017 10:41:54 PM&peers=15224";

            ProfileReportsManager mgr = new ProfileReportsManager("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            //string result = mgr.GetReportHtml(qs, "XXPAC");
            string result = mgr.GetTargetingReport(qs, "XXPAC");
            Assert.IsFalse(result.Contains("Error"));

        }

        [TestMethod]
        public void TempReportTest()
        {
            string qs = "rn=Refinery Trends Report&sd=3/1/2016&ds=ACTUAL&UOM=US&currency=SGD&yr=2014&target=True&avg=True&ytd=True&SN=2014&TS=7/12/2017 8:54:38 AM&rows=132133135140145150155160165170175180185190195197200205210215220225230233235240245250255260265270275280285295300305310315320323325330335340345350355360365370375380385390"; 
            ProfileReportsManager mgr = new ProfileReportsManager("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            string result = mgr.GetRefineryTrendsReport("XXPAC", qs);
            Assert.IsFalse(result.Contains("Error"));

        }
    }
}
