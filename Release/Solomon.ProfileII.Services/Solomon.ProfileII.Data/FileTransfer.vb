﻿Imports System.Reflection
Imports System.IO
Imports System.Web.Services.Protocols
Imports System.Configuration

Public Class FileTransfer
    Private _refNum As String = String.Empty
    Private _activityLog As ActivityLog
    Private _db12Conx As String = String.Empty
    Public Sub New(db12Conx As String)
        'DownloadTplFile("Header.tpl", "6.0.0.0", "X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=")
        _db12Conx = db12Conx
        _activityLog = New ActivityLog(_db12Conx)
    End Sub

    Public Function CheckService(userHostAddress As String) As String
        Try 'running test here
            Return "Check Service: Success"
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "CheckService", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Service Error.", New System.Xml.XmlQualifiedName())
        End Try
    End Function


    Public Function DownloadTplFile(fileinfo As String, appVersion As String, userHostAddress As String) As Byte()
        Dim fileStream As System.IO.FileStream = Nothing
        Dim filePath As String = AppDomain.CurrentDomain.BaseDirectory.ToString() + appVersion + "\" + fileinfo '+ ".tpl"
        If Not File.Exists(filePath) Then
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "DownloadTplFile", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), "Can't find tpl file at '" + filePath + "'. ", String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Server error, unable to find file " + fileinfo, System.Xml.XmlQualifiedName.Empty)
        End If
        Try
            fileStream = System.IO.File.Open(filePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim byteStream As Byte() = New Byte(fileStream.Length) {}
            fileStream.Read(byteStream, 0, fileStream.Length)
            fileStream.Close()
            Return byteStream
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "DownloadTplFile", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Server error, unable to download your file.", System.Xml.XmlQualifiedName.Empty)
        End Try
        _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetReferences", String.Empty, Nothing, Nothing, "Success", My.Application.Info.Version.ToString(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
    End Function


    Public Function UploadFile(fileName As String, bytes As Byte(), refnum As String, userHostAddress As String) As String
        Try
            Dim dateStamp As String = refnum & "_" & DateTime.Today.Year.ToString() & "-" & DateTime.Today.Month.ToString.PadLeft(2, "0") & "-" & DateTime.Today.Day.ToString().PadLeft(2, "0")
            Dim folder As String = AppDomain.CurrentDomain.BaseDirectory.ToString() & "Uploads\" & dateStamp
            If Not Directory.Exists(folder) Then
                Directory.CreateDirectory(folder)
            End If
            Using fileStream As New FileStream(folder + "\" + fileName, FileMode.Create)
                fileStream.Write(bytes, 0, bytes.Length)
            End Using
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "UploadFile", String.Empty, Nothing, Nothing, "Error", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Server error, unable to upload your file.", System.Xml.XmlQualifiedName.Empty)
        End Try
        _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "UploadFile", String.Empty, Nothing, Nothing, "Success", My.Application.Info.Version.ToString(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
        Return String.Empty
    End Function

    Private Function ValidateKey(clientKey As String, ByRef refNum As String) As Boolean
        'done at svc level
        Return True

        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(clientKey) Then
                company = Decrypt(clientKey).Split("$".ToCharArray)(0)

                locIndex = Decrypt(clientKey).Split("$".ToCharArray).Length - 2
                location = Decrypt(clientKey).Split("$".ToCharArray)(locIndex)
                Dim array As String() = Decrypt(clientKey).Split("$".ToCharArray)
                refNum = array(array.Length - 1) 'last position
                _refNum = refNum

                Dim clientKeysFolder As String = String.Empty
                Try
                    clientKeysFolder = ConfigurationManager.AppSettings("ClientKeysFolder").ToString()
                Catch
                End Try

                If clientKeysFolder.Length > 0 Then
                    If File.Exists(clientKeysFolder & company & "_" & location & ".key") Then
                        path = clientKeysFolder & company & "_" & location & ".key"
                    End If
                Else
                    If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                        path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                    ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                        path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                    Else
                        Return False
                    End If
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = clientKey.Trim)
                End If
            Else
                Return False
            End If
            Return False
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, String.Empty, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "ValidateKey", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw ex
        End Try
    End Function
End Class
