using System;
using System.Data;
using System.Collections;
namespace GenrateInTableHtml
{
	

	/// <summary>
	/// Summary description for Page.
	/// </summary>
	public class Page
	{
		DataSet _ds;
		string _company;
		string _location;
		string _reportName;
		string _tableName;
		string _tableDesc;
		string _UOM;
		string _currencyCode;
		int _month;
		int _year;
		Hashtable _replacementValues;

		public Page()
		{
		
		}


		public string createPage(DataSet ds,string tableName,string company,string location,string tableDesc,string UOM,string currencyCode, int month ,int year,Hashtable replacementValues)
		{
			_reportName = string.Empty;
			_ds = ds;
			_company=company;
			_location=location;
			_tableName=tableName;
			_tableDesc=tableDesc;
			_UOM=UOM;
			_currencyCode=currencyCode;
			_month=month;
			_year=year;
			_replacementValues=replacementValues;

			string htmlPage = toHtmlText();
			htmlPage = htmlPage.Replace("@ReportName",tableName);
			htmlPage = htmlPage.Replace("@Description",tableDesc +"<br>"+_company+" - "+_location);
			htmlPage = htmlPage.Replace("@Date", Convert.ToDateTime(month+"/1/"+year).ToString( "MMMM yyyy" ));
            htmlPage = HandleReplacementValues(htmlPage,replacementValues);
			return htmlPage;
		}

		public string createPage(DataSet ds,string reportName,string UOM,string currencyCode,Hashtable replacementValues)
		{
			_ds = ds;
			_reportName=reportName;
			_UOM=UOM;
			_currencyCode=currencyCode;
			_replacementValues=replacementValues;

			string htmlPage = toHtmlText();
			htmlPage = HandleReplacementValues(htmlPage,replacementValues);
			return htmlPage;
		}


		protected string HandleReplacementValues(string body , Hashtable replacementValues)
		{
		
			IDictionaryEnumerator en = replacementValues.GetEnumerator();
			while (en.MoveNext())
			{

				body = body.Replace((string)en.Key,(string)en.Value );
			}

			return body;
		}
		/// <summary>
		/// Construct whole page
		/// </summary>
		/// <returns></returns>
		protected string toHtmlText()
		{
			System.Text.StringBuilder pageBuilder = new System.Text.StringBuilder();
			pageBuilder.Append("<HTML>");
			pageBuilder.Append("<HEAD>");
			pageBuilder.Append("<title></title>");
		
			pageBuilder.Append("<META http-equiv=\"PRAGMA\" content=\"NO-CACHE\">");
			pageBuilder.Append("<meta content=\"Microsoft Visual Studio .NET 7.1\" name=\"GENERATOR\">");
			pageBuilder.Append("<meta content=\"Visual Basic .NET 7.1\" name=\"CODE_LANGUAGE\">");
			pageBuilder.Append("<meta content=\"JavaScript\" name=\"vs_defaultClientScript\">");
			pageBuilder.Append("<meta content=\"http://schemas.microsoft.com/intellisense/ie5 \" name=\"vs_targetSchema\">");
			pageBuilder.Append("<style>.small { FONT-SIZE: 12px; FONT-FAMILY: tahoma }");
			pageBuilder.Append("</style>");
			pageBuilder.Append("<LINK media=\"print\" href=\"print.css\" type=\"text/css\" rel=\"StyleSheet\">");
			pageBuilder.Append("</HEAD>");
			pageBuilder.Append("<body style=\"PAGE-BREAK-BEFORE: auto\" color=\"#FFFFFF\" MS_POSITIONING=\"GridLayout\">");
			pageBuilder.Append("<form id=\"Form1\" method=\"post\" runat=\"server\">");
			pageBuilder.Append("<table border=0>");
			pageBuilder.Append("<tr>");
			pageBuilder.Append("<td>");
			
			//Add Header
			pageBuilder.Append(buildHeader());


			pageBuilder.Append("</td>");
		    pageBuilder.Append("</tr>");
			pageBuilder.Append("<tr>");
			pageBuilder.Append("<td>");

			//Add Data Section
			pageBuilder.Append(buildBody());
						
			pageBuilder.Append("</td>");
			pageBuilder.Append("</tr>");
			pageBuilder.Append("</table>");
			pageBuilder.Append("</form>");
			pageBuilder.Append("</body>");
			pageBuilder.Append("</HTML>");

			return pageBuilder.ToString();
		}

		/// <summary>
		/// Construct page header
		/// </summary>
		/// <returns></returns>
		protected string buildHeader()
		{
			string htmlText="";
			if (System.Configuration.ConfigurationSettings.AppSettings["Header"]!=null)
			{
				string filename = System.Configuration.ConfigurationSettings.AppSettings["Header"];
				//filename = System.Windows.Forms.Application.StartupPath  + "//" + filename;
				
				FileHelper fsHelper = new FileHelper();
				htmlText = fsHelper.ReadFile(filename);
			}

			return htmlText;
		}

		/// <summary>
		/// Construct body 
		/// </summary>
		/// <returns></returns>
		protected string buildBody()
		{
			ReportFactory factory = new ReportFactory();
			string htmlOutput ;
			
			if(_reportName.Length > 0) 
				htmlOutput = factory.createReport(_ds,_reportName,_UOM,_currencyCode,_replacementValues);
            else
				htmlOutput = factory.createReport(_ds,_tableName+";"+_tableDesc,_company,_location,_UOM,_currencyCode,_month,_year,_replacementValues);

			return htmlOutput;
		}

	}
}
