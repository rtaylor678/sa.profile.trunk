using System;
using System.Data;
using System.Collections;

namespace GenrateInTableHtml
{
	/// <summary>
	/// Summary description for ReportFactory.
	/// </summary>
	public class ReportFactory
	{
		public ReportFactory()
		{

		}

		public string createReport(DataSet ds,string reportName,string UOM,string currencyCode,Hashtable replacementValues)
		{
			IReport report = null; 
			string htmlText="";
			string filename="";

			if (System.Configuration.ConfigurationSettings.AppSettings[reportName]!=null)
			{
				filename = System.Configuration.ConfigurationSettings.AppSettings[reportName];
				//filename = System.Windows.Forms.Application.StartupPath  + "//" + filename;  
				report = new Report(filename);
				htmlText = report.getReport(ds,UOM,currencyCode,replacementValues);
				htmlText = htmlText.Replace("UOM",UOM);
				htmlText = htmlText.Replace("CurrencyCode",currencyCode);
				
			}
			else
				htmlText = reportName + " is not found.";
			return htmlText;
		}

		public string createReport(DataSet ds,string reportName,string company,string location,string UOM,string currencyCode,int month, int year,Hashtable replacementValues)
		{
			IReport report = null; 
			string htmlText="";
			string filename="";

			if (System.Configuration.ConfigurationSettings.AppSettings[reportName]!=null)
			{
				filename = System.Configuration.ConfigurationSettings.AppSettings[reportName];
				//filename = System.Windows.Forms.Application.StartupPath  + "//" + filename;  
				report = new Report(filename);
				htmlText = report.getReport(ds,company,location,UOM,currencyCode,month,year,replacementValues);
				htmlText = htmlText.Replace("UOM",UOM);
				htmlText = htmlText.Replace("CurrencyCode",currencyCode);
			}
			return htmlText;
		}
	}
}
