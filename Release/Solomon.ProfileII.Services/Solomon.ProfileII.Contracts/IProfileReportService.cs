﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;



namespace Solomon.ProfileII.Contracts
{
    [ServiceContract]
    public interface IProfileReportService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        string GetReportHtml(string queryString, byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        string GetRefineryScorecardReport(string queryString, byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        string GetRefineryTrendsReport(string queryString, byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        string GetTargetingReport(string queryString, byte[] cert);

    }
}
