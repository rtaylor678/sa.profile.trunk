﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.DSG.Security.CryptoServices;
using ProfileLiteSecurity;

namespace Solomon.ProfileII.Services
{
    internal static class  Cryptographer
    {

        private static string EncryptionKey = "0S1A2C3T4W5S6G7L8C9DBRMF";
        private static string EncryptionIV = "SACTW0S1G2L3C4D5B6R7M8F9";


        public static string Encrypt(string aString)
        {
            // Note that the key and IV must be the same for the encrypt and decrypt calls.
            string results = string.Empty;
            try
            {
                ProfileLiteSecurity.Cryptography.Cryptographer crypto = new ProfileLiteSecurity.Cryptography.Cryptographer();
                results= crypto.Encrypt(aString);
                 
                //TDES tdesEngine = new TDES(EncodingType.ASCIIEncoding);
                //tdesEngine.StringKey = EncryptionKey;
                //tdesEngine.StringIV = EncryptionIV;
                //results = tdesEngine.Encrypt(aString);
            }
            catch(Exception ex)
                {
                throw ex;
            }
            return results;
        }

        public static string Decrypt(string aString)
        {
            // Note that the key and IV must be the same for the encrypt and decrypt calls.
            string results = string.Empty;
            try
            {
                ProfileLiteSecurity.Cryptography.Cryptographer crypto = new ProfileLiteSecurity.Cryptography.Cryptographer();
                return crypto.Decrypt(aString);
                //TDES tdesEngine = new TDES(EncodingType.ASCIIEncoding);
                //tdesEngine.StringKey = EncryptionKey;
                //tdesEngine.StringIV = EncryptionIV;
                //results = tdesEngine.Decrypt(aString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return results;
        }
    }
}
