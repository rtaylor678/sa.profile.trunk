'********************************************************************************
'*  TODO: Comments go here...                                                   *
'********************************************************************************
Option Explicit On

Imports System
Imports System.Text
Imports System.IO
Public Class Exceptions


    Shared Sub Display(ByVal MessageType As String, ByVal ex As System.Exception, WhereFrom As String)
        Dim ALog As New RemoteSubmitServices
        Dim myFile As Stream = File.Create("Profile.log")
        Dim myTextListener As New TextWriterTraceListener(myFile)

        Trace.Listeners.Add(myTextListener)
        System.Diagnostics.Debug.WriteLine("Module: " & WhereFrom & vbCrLf & "Error: " & ex.Message, MessageType)
        MessageBox.Show("Module: " & WhereFrom & vbCrLf & "Error: " & ex.Message, MessageType)
        Trace.WriteLine(vbCrLf & "Date: " & Now.ToString & vbCrLf & "Module: " & WhereFrom & vbCrLf & "Error: " & ex.Message & vbCrLf)
        Trace.Flush()
        ALog.SubmitActivityLog("", "", "", Environment.UserName, Environment.MachineName.ToString(), WhereFrom, "", "", "", "", ex.Message, "ERROR")

        System.Environment.ExitCode = 0
        myFile.Close()
    End Sub

   
End Class
'TODO: Add Serializable attributes...
Friend Class InvalidKeyLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid key values.

#Region " Constructors "
    Friend Sub New()
        'Default constructor.
    End Sub

    Friend Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Friend Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

'TODO: Add Serializable attributes...
Friend Class InvalidIVLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid IV values.

#Region " Constructors "
    Friend Sub New()
        'Default constructor.
    End Sub

    Friend Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Friend Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

'TODO: Add additional custom exception classes...
