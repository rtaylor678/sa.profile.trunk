<!-- config-start -->
FILE=_CONFIG/MaintRoutHist.xml
<!-- config-end -->
<!-- template-start -->
<table class='small' width="100%" border="0">
  <tr>
    <td width=75%></td>
    <td width=10%></td>
	<td></td>
  </tr>
  <tr>
     <td colspan=3 align=center><strong>Refinery Non-Turnaround Maintenance Costs</strong></td>
  </tr>
  <tr>
     <td colspan=3 align=center><strong>----- (CurrencyCode/1000) -----</strong></td>
  </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="3">&nbsp;</td></tr>
  <tr>
    <td align=left><strong>Months</strong></td>
	<td align=right><strong>Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
	<td></td>
  </tr>
  SECTION(MaintRoutHist,,PeriodStart DESC)
  BEGIN
  <tr>
    <td align=left>&nbsp;&nbsp;&nbsp;Format(PeriodStart,'MMMM yyyy') </td>
    <td align=right> Format(RoutCostLocal,'#,##0.0')</td>
	<td></td>
   </tr>
  END
 <tr><td colspan="3">&nbsp;</td></tr>
 <tr><td colspan="3">&nbsp;</td></tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align=right><strong>Formula(MaintRoutHist,SUM(RoutCostLocal),,'#,##0.0')</strong></td>
	<td></td>
  </tr>
</table>
<!-- template-end -->
