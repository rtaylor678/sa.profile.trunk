Option Explicit On

Friend Class RemoteSubmitServices
    Inherits SubmitServicesExtension

#Region "Data Submission Records"

    Friend Sub SubmitData(ByVal ds As DataSet, refineryID As String)
        If AppCertificate.IsCertificateInstall() Then
            If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            End If
            Dim b As New Solomon.Submit12.SubmitServices
            b.SubmitRefineryData(ds, refineryID)
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Sub
    Friend Sub SubmitActivityLog(Methodology As String, refineryID As String, CallerIP As String, userid As String, computername As String, service As String, method As String, EntityName As String, PeriodStart As String, PeriodEnd As String, Notes As String, status As String)
        If AppCertificate.IsCertificateInstall() Then
            If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            End If
            Dim b As New Solomon.Submit12.SubmitServices
            b.WriteActivityLog(Methodology.Trim(), refineryID, CallerIP, userid, computername, service, method, EntityName, PeriodStart, PeriodEnd, Notes, status)
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Sub

    Function GetData(ByVal startDate As Date, ByVal endDate As Date, refineryID As String) As DataSet
        If AppCertificate.IsCertificateInstall() Then
            If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            End If
            Dim b As New Solomon.Submit12.SubmitServices
            Return b.GetDataByPeriod(startDate, endDate, refineryID)
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If
    End Function
#End Region

#Region "Test code"
    Friend Shared Sub Main(refineryID As String)
        Dim service As New RemoteSubmitServices
        Dim ds As New DataSet

        'Dim startPrd As String = "1" + _
        '                        "/1/" + _
        '                        "2000" + _
        '                        " 12:00:00 AM"

        'Dim endPrd As String = DateAdd(DateInterval.Month, 1, CDate(startPrd)).ToString
        'Console.WriteLine("StartDate " + startPrd + " EndDate " + endPrd)

        'Console.WriteLine("StartDate " + CDate(startPrd).ToString + " EndDate " + CDate(endPrd).ToString)
        ' ds.ReadXml("C:\MonthlyUpload.xml")
        'service.SubmitData(ds)
        ds = service.GetData(#1/1/2004#, #2/1/2004#, refineryID)
        ds.WriteXml("C:\dump.xml", XmlWriteMode.WriteSchema)
        'For y = 0 To ds.Tables.Count - 1
        '    Console.WriteLine(ds.Tables(y).TableName + "," + ds.Tables(y).Rows.Count.ToString)
        'Next
    End Sub
#End Region

End Class
