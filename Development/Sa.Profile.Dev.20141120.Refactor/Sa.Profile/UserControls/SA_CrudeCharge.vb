Option Explicit On

Friend Class SA_CrudeCharge
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents gbTools As System.Windows.Forms.GroupBox
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents txtBBL As System.Windows.Forms.TextBox
    Friend WithEvents txtCostPerBBL As System.Windows.Forms.TextBox
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents pnlLU As System.Windows.Forms.Panel
    Friend WithEvents cmbCountry As System.Windows.Forms.ComboBox
    Friend WithEvents dgvLU As System.Windows.Forms.DataGridView
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents CNum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CrudeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Gravity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sulfur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CNum_LU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProdCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CrudeName_LU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypicalAPI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypicalSulfur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SortKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbFooter As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbFilePath As System.Windows.Forms.TextBox
    Friend WithEvents cbSheets As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvLinks As System.Windows.Forms.DataGridView
    Friend WithEvents tcCrude As System.Windows.Forms.TabControl
    Friend WithEvents pageCrude As System.Windows.Forms.TabPage
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbData As System.Windows.Forms.RadioButton
    Friend WithEvents rbLinks As System.Windows.Forms.RadioButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtGravity As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents StartRow As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EndRow As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CNum2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CrudeName2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Gravity2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sulfur2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BBL2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostPerBBL2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblHeaderCrude As System.Windows.Forms.Label
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents CostPerBBL As System.Windows.Forms.DataGridViewTextBoxColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_CrudeCharge))
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlData = New System.Windows.Forms.Panel
        Me.tcCrude = New System.Windows.Forms.TabControl
        Me.pageCrude = New System.Windows.Forms.TabPage
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.CNum = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CrudeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Gravity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sulfur = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CostPerBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvLinks = New System.Windows.Forms.DataGridView
        Me.StartRow = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EndRow = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CNum2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CrudeName2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Gravity2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sulfur2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BBL2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CostPerBBL2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblHeaderCrude = New System.Windows.Forms.Label
        Me.gbFooter = New System.Windows.Forms.GroupBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtGravity = New System.Windows.Forms.TextBox
        Me.txtCostPerBBL = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtBBL = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.gbTools = New System.Windows.Forms.GroupBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.rbLinks = New System.Windows.Forms.RadioButton
        Me.rbData = New System.Windows.Forms.RadioButton
        Me.btnEdit = New System.Windows.Forms.Button
        Me.lblView = New System.Windows.Forms.Label
        Me.btnAdd = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbSheets = New System.Windows.Forms.ComboBox
        Me.tbFilePath = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.pnlLU = New System.Windows.Forms.Panel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.cmbCountry = New System.Windows.Forms.ComboBox
        Me.dgvLU = New System.Windows.Forms.DataGridView
        Me.CNum_LU = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ProdCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CrudeName_LU = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TypicalAPI = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TypicalSulfur = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SortKey = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label8 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.pnlData.SuspendLayout()
        Me.tcCrude.SuspendLayout()
        Me.pageCrude.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLinks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFooter.SuspendLayout()
        Me.pnlTools.SuspendLayout()
        Me.gbTools.SuspendLayout()
        Me.pnlLU.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvLU, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlData
        '
        Me.pnlData.BackColor = System.Drawing.SystemColors.Control
        Me.pnlData.Controls.Add(Me.tcCrude)
        Me.pnlData.Controls.Add(Me.pnlTools)
        Me.pnlData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(4, 40)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(742, 556)
        Me.pnlData.TabIndex = 492
        '
        'tcCrude
        '
        Me.tcCrude.Controls.Add(Me.pageCrude)
        Me.tcCrude.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcCrude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcCrude.ItemSize = New System.Drawing.Size(100, 18)
        Me.tcCrude.Location = New System.Drawing.Point(0, 105)
        Me.tcCrude.Name = "tcCrude"
        Me.tcCrude.SelectedIndex = 0
        Me.tcCrude.Size = New System.Drawing.Size(742, 451)
        Me.tcCrude.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcCrude.TabIndex = 945
        '
        'pageCrude
        '
        Me.pageCrude.Controls.Add(Me.dgvData)
        Me.pageCrude.Controls.Add(Me.dgvLinks)
        Me.pageCrude.Controls.Add(Me.lblHeaderCrude)
        Me.pageCrude.Controls.Add(Me.gbFooter)
        Me.pageCrude.Location = New System.Drawing.Point(4, 22)
        Me.pageCrude.Name = "pageCrude"
        Me.pageCrude.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageCrude.Size = New System.Drawing.Size(734, 425)
        Me.pageCrude.TabIndex = 1
        Me.pageCrude.Text = "Table 14"
        Me.pageCrude.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CNum, Me.CrudeName, Me.Gravity, Me.Sulfur, Me.BBL, Me.CostPerBBL})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(6, 36)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersWidth = 25
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(722, 305)
        Me.dgvData.TabIndex = 0
        '
        'CNum
        '
        Me.CNum.DataPropertyName = "CNum"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CNum.DefaultCellStyle = DataGridViewCellStyle2
        Me.CNum.FillWeight = 80.0!
        Me.CNum.Frozen = True
        Me.CNum.HeaderText = "Crude Number"
        Me.CNum.Name = "CNum"
        Me.CNum.ReadOnly = True
        Me.CNum.Width = 80
        '
        'CrudeName
        '
        Me.CrudeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CrudeName.DataPropertyName = "CrudeName"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.MediumBlue
        Me.CrudeName.DefaultCellStyle = DataGridViewCellStyle3
        Me.CrudeName.FillWeight = 200.0!
        Me.CrudeName.HeaderText = "Crude Name"
        Me.CrudeName.Name = "CrudeName"
        '
        'Gravity
        '
        Me.Gravity.DataPropertyName = "Gravity"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle4.Format = "N1"
        Me.Gravity.DefaultCellStyle = DataGridViewCellStyle4
        Me.Gravity.FillWeight = 80.0!
        Me.Gravity.HeaderText = "Gravity, API"
        Me.Gravity.Name = "Gravity"
        Me.Gravity.Width = 80
        '
        'Sulfur
        '
        Me.Sulfur.DataPropertyName = "Sulfur"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle5.Format = "N2"
        Me.Sulfur.DefaultCellStyle = DataGridViewCellStyle5
        Me.Sulfur.FillWeight = 80.0!
        Me.Sulfur.HeaderText = "Sulfur, wt %"
        Me.Sulfur.Name = "Sulfur"
        Me.Sulfur.Width = 80
        '
        'BBL
        '
        Me.BBL.DataPropertyName = "BBL"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle6.Format = "N0"
        Me.BBL.DefaultCellStyle = DataGridViewCellStyle6
        Me.BBL.HeaderText = "Barrels Processed"
        Me.BBL.Name = "BBL"
        '
        'CostPerBBL
        '
        Me.CostPerBBL.DataPropertyName = "CostPerBBL"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle7.Format = "C2"
        Me.CostPerBBL.DefaultCellStyle = DataGridViewCellStyle7
        Me.CostPerBBL.FillWeight = 80.0!
        Me.CostPerBBL.HeaderText = "USD/bbl"
        Me.CostPerBBL.Name = "CostPerBBL"
        Me.CostPerBBL.Width = 80
        '
        'dgvLinks
        '
        Me.dgvLinks.AllowUserToAddRows = False
        Me.dgvLinks.AllowUserToDeleteRows = False
        Me.dgvLinks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLinks.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvLinks.ColumnHeadersHeight = 47
        Me.dgvLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinks.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StartRow, Me.EndRow, Me.CNum2, Me.CrudeName2, Me.Gravity2, Me.Sulfur2, Me.BBL2, Me.CostPerBBL2})
        Me.dgvLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLinks.Location = New System.Drawing.Point(6, 36)
        Me.dgvLinks.MultiSelect = False
        Me.dgvLinks.Name = "dgvLinks"
        Me.dgvLinks.RowHeadersVisible = False
        Me.dgvLinks.RowHeadersWidth = 25
        Me.dgvLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLinks.Size = New System.Drawing.Size(722, 305)
        Me.dgvLinks.TabIndex = 944
        '
        'StartRow
        '
        Me.StartRow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.StartRow.DataPropertyName = "lkStartRow"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StartRow.DefaultCellStyle = DataGridViewCellStyle9
        Me.StartRow.HeaderText = "Starting Row (integer)"
        Me.StartRow.Name = "StartRow"
        Me.StartRow.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.StartRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'EndRow
        '
        Me.EndRow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.EndRow.DataPropertyName = "lkEndRow"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndRow.DefaultCellStyle = DataGridViewCellStyle10
        Me.EndRow.HeaderText = "Ending Row (integer)"
        Me.EndRow.Name = "EndRow"
        Me.EndRow.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.EndRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'CNum2
        '
        Me.CNum2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CNum2.DataPropertyName = "lkCNum"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.MediumBlue
        Me.CNum2.DefaultCellStyle = DataGridViewCellStyle11
        Me.CNum2.HeaderText = "Crude Number Column (letter)"
        Me.CNum2.Name = "CNum2"
        Me.CNum2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.CNum2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'CrudeName2
        '
        Me.CrudeName2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CrudeName2.DataPropertyName = "lkCrudeName"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.MediumBlue
        Me.CrudeName2.DefaultCellStyle = DataGridViewCellStyle12
        Me.CrudeName2.HeaderText = "Crude Name Column (letter)"
        Me.CrudeName2.Name = "CrudeName2"
        Me.CrudeName2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.CrudeName2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Gravity2
        '
        Me.Gravity2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Gravity2.DataPropertyName = "lkGravity"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle13.Format = "N1"
        Me.Gravity2.DefaultCellStyle = DataGridViewCellStyle13
        Me.Gravity2.HeaderText = "Gravity Column (letter)"
        Me.Gravity2.Name = "Gravity2"
        Me.Gravity2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gravity2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Sulfur2
        '
        Me.Sulfur2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Sulfur2.DataPropertyName = "lkSulfur"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle14.Format = "N2"
        Me.Sulfur2.DefaultCellStyle = DataGridViewCellStyle14
        Me.Sulfur2.HeaderText = "Sulfur Column (letter)"
        Me.Sulfur2.Name = "Sulfur2"
        Me.Sulfur2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Sulfur2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'BBL2
        '
        Me.BBL2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.BBL2.DataPropertyName = "lkBBL"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle15.Format = "N0"
        Me.BBL2.DefaultCellStyle = DataGridViewCellStyle15
        Me.BBL2.HeaderText = "Barrels Processed Column (letter)"
        Me.BBL2.Name = "BBL2"
        Me.BBL2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.BBL2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'CostPerBBL2
        '
        Me.CostPerBBL2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CostPerBBL2.DataPropertyName = "lkCostPerBBL"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle16.Format = "C2"
        Me.CostPerBBL2.DefaultCellStyle = DataGridViewCellStyle16
        Me.CostPerBBL2.HeaderText = "Price Column (letter)"
        Me.CostPerBBL2.Name = "CostPerBBL2"
        Me.CostPerBBL2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.CostPerBBL2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lblHeaderCrude
        '
        Me.lblHeaderCrude.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeaderCrude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderCrude.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblHeaderCrude.Location = New System.Drawing.Point(6, 0)
        Me.lblHeaderCrude.Name = "lblHeaderCrude"
        Me.lblHeaderCrude.Size = New System.Drawing.Size(722, 36)
        Me.lblHeaderCrude.TabIndex = 951
        Me.lblHeaderCrude.Text = "Crude Charge Detail: DATA"
        Me.lblHeaderCrude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFooter
        '
        Me.gbFooter.BackColor = System.Drawing.Color.Transparent
        Me.gbFooter.Controls.Add(Me.Label11)
        Me.gbFooter.Controls.Add(Me.Label10)
        Me.gbFooter.Controls.Add(Me.Label9)
        Me.gbFooter.Controls.Add(Me.txtGravity)
        Me.gbFooter.Controls.Add(Me.txtCostPerBBL)
        Me.gbFooter.Controls.Add(Me.Label6)
        Me.gbFooter.Controls.Add(Me.txtBBL)
        Me.gbFooter.Controls.Add(Me.Label7)
        Me.gbFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbFooter.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbFooter.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbFooter.Location = New System.Drawing.Point(6, 341)
        Me.gbFooter.Name = "gbFooter"
        Me.gbFooter.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbFooter.Size = New System.Drawing.Size(722, 78)
        Me.gbFooter.TabIndex = 950
        Me.gbFooter.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(6, 12)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 13)
        Me.Label11.TabIndex = 8
        Me.Label11.Text = "Helpful hint:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(6, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(154, 39)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "To delete a row, simply select" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "the entire row and click delete " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "on your keyboar" & _
            "d."
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(661, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 26)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Gravity, " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "API"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtGravity
        '
        Me.txtGravity.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGravity.BackColor = System.Drawing.SystemColors.Control
        Me.txtGravity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGravity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtGravity.Location = New System.Drawing.Point(659, 40)
        Me.txtGravity.Name = "txtGravity"
        Me.txtGravity.ReadOnly = True
        Me.txtGravity.Size = New System.Drawing.Size(52, 21)
        Me.txtGravity.TabIndex = 5
        Me.txtGravity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCostPerBBL
        '
        Me.txtCostPerBBL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCostPerBBL.BackColor = System.Drawing.SystemColors.Control
        Me.txtCostPerBBL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostPerBBL.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCostPerBBL.Location = New System.Drawing.Point(579, 40)
        Me.txtCostPerBBL.Name = "txtCostPerBBL"
        Me.txtCostPerBBL.ReadOnly = True
        Me.txtCostPerBBL.Size = New System.Drawing.Size(73, 21)
        Me.txtCostPerBBL.TabIndex = 3
        Me.txtCostPerBBL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(577, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 26)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Average Cost," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "USD/bbl"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtBBL
        '
        Me.txtBBL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBBL.BackColor = System.Drawing.SystemColors.Control
        Me.txtBBL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBBL.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtBBL.Location = New System.Drawing.Point(473, 40)
        Me.txtBBL.Name = "txtBBL"
        Me.txtBBL.ReadOnly = True
        Me.txtBBL.Size = New System.Drawing.Size(100, 21)
        Me.txtBBL.TabIndex = 2
        Me.txtBBL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(491, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 26)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Total Barrels" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Processed"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTools
        '
        Me.pnlTools.Controls.Add(Me.gbTools)
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(0, 0)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 105)
        Me.pnlTools.TabIndex = 0
        '
        'gbTools
        '
        Me.gbTools.BackColor = System.Drawing.SystemColors.Control
        Me.gbTools.Controls.Add(Me.btnBrowse)
        Me.gbTools.Controls.Add(Me.chkComplete)
        Me.gbTools.Controls.Add(Me.rbLinks)
        Me.gbTools.Controls.Add(Me.rbData)
        Me.gbTools.Controls.Add(Me.btnEdit)
        Me.gbTools.Controls.Add(Me.lblView)
        Me.gbTools.Controls.Add(Me.btnAdd)
        Me.gbTools.Controls.Add(Me.Label1)
        Me.gbTools.Controls.Add(Me.cbSheets)
        Me.gbTools.Controls.Add(Me.tbFilePath)
        Me.gbTools.Controls.Add(Me.Label2)
        Me.gbTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTools.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbTools.Location = New System.Drawing.Point(0, 0)
        Me.gbTools.Name = "gbTools"
        Me.gbTools.Padding = New System.Windows.Forms.Padding(0, 4, 0, 8)
        Me.gbTools.Size = New System.Drawing.Size(742, 99)
        Me.gbTools.TabIndex = 0
        Me.gbTools.TabStop = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(711, 15)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(21, 21)
        Me.btnBrowse.TabIndex = 963
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for bridge workbook")
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'chkComplete
        '
        Me.chkComplete.AutoSize = True
        Me.chkComplete.BackColor = System.Drawing.Color.Transparent
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkComplete.Location = New System.Drawing.Point(9, 49)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(136, 17)
        Me.chkComplete.TabIndex = 962
        Me.chkComplete.Text = "Check when completed"
        Me.chkComplete.UseVisualStyleBackColor = False
        '
        'rbLinks
        '
        Me.rbLinks.AutoSize = True
        Me.rbLinks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLinks.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbLinks.Location = New System.Drawing.Point(92, 70)
        Me.rbLinks.Name = "rbLinks"
        Me.rbLinks.Size = New System.Drawing.Size(53, 17)
        Me.rbLinks.TabIndex = 948
        Me.rbLinks.Text = "LINKS"
        Me.rbLinks.UseVisualStyleBackColor = True
        '
        'rbData
        '
        Me.rbData.AutoSize = True
        Me.rbData.Checked = True
        Me.rbData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbData.Location = New System.Drawing.Point(41, 70)
        Me.rbData.Name = "rbData"
        Me.rbData.Size = New System.Drawing.Size(52, 17)
        Me.rbData.TabIndex = 947
        Me.rbData.TabStop = True
        Me.rbData.Text = "DATA"
        Me.rbData.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.Image = Global.Solomon_Profile.My.Resources.Resources.log
        Me.btnEdit.Location = New System.Drawing.Point(76, 15)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(64, 28)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.TabStop = False
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnEdit, "Change the current Crude Number")
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'lblView
        '
        Me.lblView.AutoSize = True
        Me.lblView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblView.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblView.Location = New System.Drawing.Point(6, 72)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(33, 13)
        Me.lblView.TabIndex = 949
        Me.lblView.Text = "View"
        Me.lblView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.Image = Global.Solomon_Profile.My.Resources.Resources.add
        Me.btnAdd.Location = New System.Drawing.Point(6, 15)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(64, 28)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.TabStop = False
        Me.btnAdd.Text = "Add"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnAdd, "Create a new crude entry")
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(389, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 945
        Me.Label1.Text = "Workbook"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbSheets
        '
        Me.cbSheets.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets.FormattingEnabled = True
        Me.cbSheets.Location = New System.Drawing.Point(464, 42)
        Me.cbSheets.Name = "cbSheets"
        Me.cbSheets.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets.TabIndex = 8
        '
        'tbFilePath
        '
        Me.tbFilePath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFilePath.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbFilePath.Location = New System.Drawing.Point(464, 15)
        Me.tbFilePath.Name = "tbFilePath"
        Me.tbFilePath.ReadOnly = True
        Me.tbFilePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbFilePath.Size = New System.Drawing.Size(241, 21)
        Me.tbFilePath.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(389, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 946
        Me.Label2.Text = "Worksheet"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLU
        '
        Me.pnlLU.Controls.Add(Me.TabControl1)
        Me.pnlLU.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLU.Location = New System.Drawing.Point(4, 40)
        Me.pnlLU.Name = "pnlLU"
        Me.pnlLU.Padding = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.pnlLU.Size = New System.Drawing.Size(742, 556)
        Me.pnlLU.TabIndex = 946
        Me.pnlLU.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 6)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(742, 550)
        Me.TabControl1.TabIndex = 15
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnOK)
        Me.TabPage1.Controls.Add(Me.btnCancel)
        Me.TabPage1.Controls.Add(Me.cmbCountry)
        Me.TabPage1.Controls.Add(Me.dgvLU)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.TabPage1.Size = New System.Drawing.Size(734, 524)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Select a Crude Number"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.Image = Global.Solomon_Profile.My.Resources.Resources.check
        Me.btnOK.Location = New System.Drawing.Point(594, 4)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(64, 28)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.Image = Global.Solomon_Profile.My.Resources.Resources.error_1
        Me.btnCancel.Location = New System.Drawing.Point(664, 4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(64, 28)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'cmbCountry
        '
        Me.cmbCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCountry.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cmbCountry.FormattingEnabled = True
        Me.cmbCountry.Location = New System.Drawing.Point(111, 9)
        Me.cmbCountry.Name = "cmbCountry"
        Me.cmbCountry.Size = New System.Drawing.Size(158, 21)
        Me.cmbCountry.TabIndex = 0
        '
        'dgvLU
        '
        Me.dgvLU.AllowUserToAddRows = False
        Me.dgvLU.AllowUserToDeleteRows = False
        Me.dgvLU.AllowUserToResizeColumns = False
        Me.dgvLU.AllowUserToResizeRows = False
        Me.dgvLU.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLU.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvLU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLU.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CNum_LU, Me.ProdCountry, Me.CrudeName_LU, Me.TypicalAPI, Me.TypicalSulfur, Me.SortKey})
        Me.dgvLU.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLU.Location = New System.Drawing.Point(6, 36)
        Me.dgvLU.MultiSelect = False
        Me.dgvLU.Name = "dgvLU"
        Me.dgvLU.RowHeadersWidth = 25
        Me.dgvLU.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvLU.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvLU.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLU.Size = New System.Drawing.Size(722, 482)
        Me.dgvLU.TabIndex = 0
        '
        'CNum_LU
        '
        Me.CNum_LU.DataPropertyName = "CNum"
        Me.CNum_LU.HeaderText = "Crude Number"
        Me.CNum_LU.Name = "CNum_LU"
        Me.CNum_LU.ReadOnly = True
        Me.CNum_LU.Width = 110
        '
        'ProdCountry
        '
        Me.ProdCountry.DataPropertyName = "ProdCountry"
        Me.ProdCountry.HeaderText = "Country"
        Me.ProdCountry.Name = "ProdCountry"
        Me.ProdCountry.ReadOnly = True
        '
        'CrudeName_LU
        '
        Me.CrudeName_LU.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CrudeName_LU.DataPropertyName = "CrudeName"
        Me.CrudeName_LU.FillWeight = 200.0!
        Me.CrudeName_LU.HeaderText = "Solomon Crude Name"
        Me.CrudeName_LU.Name = "CrudeName_LU"
        Me.CrudeName_LU.ReadOnly = True
        '
        'TypicalAPI
        '
        Me.TypicalAPI.DataPropertyName = "TypicalAPI"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle18.Format = "N1"
        Me.TypicalAPI.DefaultCellStyle = DataGridViewCellStyle18
        Me.TypicalAPI.HeaderText = "Gravity, API"
        Me.TypicalAPI.Name = "TypicalAPI"
        Me.TypicalAPI.ReadOnly = True
        '
        'TypicalSulfur
        '
        Me.TypicalSulfur.DataPropertyName = "TypicalSulfur"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle19.Format = "N2"
        Me.TypicalSulfur.DefaultCellStyle = DataGridViewCellStyle19
        Me.TypicalSulfur.HeaderText = "Sulfur, wt %"
        Me.TypicalSulfur.Name = "TypicalSulfur"
        Me.TypicalSulfur.ReadOnly = True
        '
        'SortKey
        '
        Me.SortKey.DataPropertyName = "SortKey"
        Me.SortKey.HeaderText = "Sort Key"
        Me.SortKey.Name = "SortKey"
        Me.SortKey.ReadOnly = True
        Me.SortKey.Visible = False
        '
        'Label8
        '
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(6, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(722, 36)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Filter by country"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon_Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon_Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(500, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 2
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Update input form with linked values")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1034
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(500, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Crude Charge Detail"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_CrudeCharge
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.pnlData)
        Me.Controls.Add(Me.pnlLU)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.MediumBlue
        Me.Name = "SA_CrudeCharge"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.pnlData.ResumeLayout(False)
        Me.tcCrude.ResumeLayout(False)
        Me.pageCrude.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLinks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFooter.ResumeLayout(False)
        Me.gbFooter.PerformLayout()
        Me.pnlTools.ResumeLayout(False)
        Me.gbTools.ResumeLayout(False)
        Me.gbTools.PerformLayout()
        Me.pnlLU.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dgvLU, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private dv As New DataView
    Private dvWB As New DataView
    Private dv_LU As New DataView
    Private MyParent As Main
    Friend CrudeNotSaved As Boolean
    Private IsEditing, IsLoaded, HasRights As Boolean

    Friend Sub SetCountryList()
        Dim tmpCountry As String = ""
        Dim prevCountry As String = ""
        Dim row As DataRow
        cmbCountry.Items.Clear()
        cmbCountry.Items.Add("")
        For Each row In MyParent.MyParent.ds_Ref.Tables("Crude_LU").Rows
            tmpCountry = row!ProdCountry.ToString
            If prevCountry <> tmpCountry Then cmbCountry.Items.Add(tmpCountry)
            prevCountry = tmpCountry
        Next
    End Sub

    Friend Sub SetDataSet()
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.CrudeRights
        dv.Table = MyParent.MyParent.ds_Ref.Tables("Crude_LU")
        dgvData.DataSource = MyParent.dsCrude.Tables(0)
        dgvLU.DataSource = dv
        dgvLinks.DataSource = MyParent.dsMappings.Tables("Crude")


        'DWW 20111118 for checking
        Dim rowIdx As Integer = 0
        For Each dataTable As DataTable In MyParent.dsMappings.Tables
            Console.Write("Table " & Convert.ToString(rowIdx) & " :Name = " & dataTable.TableName & ": Row count = " & dataTable.Rows.Count)
            Console.Write(Environment.NewLine)
            rowIdx = rowIdx + 1
        Next

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Crude Charge Detail'")
        chkComplete.Checked = CBool(row(0)!Checked)

        'tbFilePath.Text = My.Settings.workbookCrude
        'cbSheets.Text = My.Settings.worksheetCrude

        Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = 'Crude'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
            cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
        End If

        SetCountryList()
        PopulateSummary()
        IsLoaded = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveCrude()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim result As DialogResult

        If rbLinks.Checked Then
            result = ProfileMsgBox("YN", "CLEAR", "crude mappings")
            If result = DialogResult.Yes Then
                For Each col As DataColumn In MyParent.dsMappings.Tables("Crude").Columns
                    MyParent.dsMappings.Tables("Crude").Rows(0).Item(col) = Nothing
                Next
            End If
        Else
            result = ProfileMsgBox("YN", "CLEAR", "crude table")
            If result = DialogResult.Yes Then
                For Each row As DataRow In MyParent.dsCrude.Tables(0).Rows
                    row.Delete()
                Next row
                '20090114 RRH .Clear() also 'AccpetsChanges'... MyParent.dsCrude.Tables(0).Clear()
            End If

        End If

        If result = DialogResult.Yes Then
            ChangesMade(btnSave, chkComplete, CrudeNotSaved)
            PopulateSummary()
        End If
    End Sub

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        If IsLoaded = False Then Exit Sub
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "Crude Charge Detail")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Crude Charge Detail'")
        row(0)!Checked = chk.Checked

        CrudeNotSaved = True
        btnSave.BackColor = Color.Red
        btnSave.ForeColor = Color.White
    End Sub

    Friend Sub PopulateSummary()
        Dim Volume, Cost, Grav As Double
        Dim row As DataRow
        Dim dt As DataTable = MyParent.dsCrude.Tables(0)

        For Each row In dt.Rows
            Try
                Volume = Volume + CDbl(row!BBL)
            Catch ex As Exception
            End Try
            Try
                Cost = Cost + (CDbl(row!BBL) * CDbl(row!CostPerBBL))
            Catch ex As Exception
            End Try
            Try
                Grav = Grav + (CDbl(row!BBL) * CDbl(row!Gravity))
            Catch ex As Exception
            End Try
        Next

        txtBBL.Text = FormatDoubles(Volume, 0)

        If Volume > 0 Then
            txtCostPerBBL.Text = FormatDoubles(Cost / Volume, 2)
            txtGravity.Text = FormatDoubles(Grav / Volume, 1)
        Else
            txtCostPerBBL.Text = FormatDoubles(0, 2)
            txtGravity.Text = FormatDoubles(0, 1)
        End If
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        MyParent.PrintPreviews("Table 14", Nothing)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewingLUs(pnlHeader, pnlLU, pnlData, False)
        cmbCountry.Text = ""
        dgvLU.CurrentCell = dgvLU(0, 0)
    End Sub

    Private Sub DataGridView1_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvData.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, CrudeNotSaved)
            PopulateSummary()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
    End Sub

    Private Sub cmbCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCountry.SelectedIndexChanged
        SetRowFilter()
    End Sub

    Private Sub SetRowFilter()
        Select Case cmbCountry.Text
            Case ""
                dv.RowFilter = ""
            Case Else
                dv.RowFilter = "ProdCountry = '" & cmbCountry.Text & "'"
        End Select
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        On Error GoTo NothingSelected
        Dim result As DialogResult = ProfileMsgBox("YN", "CRUDEDEFAULTS", "")

        Dim CNum, CrudeName As String
        CNum = dgvLU.CurrentRow.Cells("CNum_LU").Value.ToString
        CrudeName = dgvLU.CurrentRow.Cells("CrudeName_LU").Value.ToString
        Dim row As DataRow

        Select Case result
            Case DialogResult.Yes
                If IsEditing Then
                    dgvData.CurrentRow.Cells("CNum").Value = CNum
                    dgvData.CurrentRow.Cells("CrudeName").Value = CrudeName
                    dgvData.CurrentRow.Cells("Gravity").Value = dgvLU.CurrentRow.Cells("TypicalAPI").Value
                    dgvData.CurrentRow.Cells("Sulfur").Value = dgvLU.CurrentRow.Cells("TypicalSulfur").Value
                Else
                    row = MyParent.dsCrude.Tables(0).NewRow
                    row!CNum = CNum
                    row!CrudeName = CrudeName
                    row!Gravity = dgvLU.CurrentRow.Cells("TypicalAPI").Value
                    row!Sulfur = dgvLU.CurrentRow.Cells("TypicalSulfur").Value
                    MyParent.dsCrude.Tables(0).Rows.Add(row)
                End If
            Case DialogResult.No
                If IsEditing Then
                    dgvData.CurrentRow.Cells("CNum").Value = CNum
                    CrudeName = dgvData.CurrentRow.Cells("CrudeName").Value.ToString
                Else
                    row = MyParent.dsCrude.Tables(0).NewRow
                    row!CNum = CNum
                    CrudeName = ""
                    row!CrudeName = CrudeName
                    MyParent.dsCrude.Tables(0).Rows.Add(row)
                End If
            Case DialogResult.Cancel
                IsEditing = False
                Exit Sub
        End Select

        ViewingLUs(pnlHeader, pnlLU, pnlData, True)

        ChangesMade(btnSave, chkComplete, CrudeNotSaved)
        PopulateSummary()
        IsEditing = False

        For Each SelectRow As DataGridViewRow In dgvData.Rows
            If SelectRow.Cells("CNum").Value.ToString = CNum And SelectRow.Cells("CrudeName").Value.ToString = CrudeName Then
                SelectRow.Selected = True
                dgvData.CurrentCell = SelectRow.Cells("CNum")
                Exit For
            Else
                SelectRow.Selected = False
            End If
        Next
        pnlHeader.Enabled = True
        Exit Sub
NothingSelected:
        HandleNothingSelected()
    End Sub

    Private Sub EditCrude()
        IsEditing = True
        Dim cnum, Country As String
        On Error GoTo NothingSelected

        dgvData.Rows(dgvData.SelectedCells(0).RowIndex).Selected = True
        cnum = dgvData.SelectedRows(0).Cells("CNum").Value.ToString

        dv.RowFilter = "CNum = '" & cnum & "'"
        Country = dv.Item(0).Item("ProdCountry").ToString
        cmbCountry.Text = Country
        SetRowFilter()

        For Each SelectRow As DataGridViewRow In dgvLU.Rows
            If SelectRow.Cells("CNum_LU").Value.ToString = cnum Then
                SelectRow.Selected = True
                dgvLU.CurrentCell = SelectRow.Cells("CNum_LU")
                Exit For
            Else
                SelectRow.Selected = False
            End If
        Next
        Exit Sub
NothingSelected:
        HandleNothingSelected()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EditCrude()
        ViewingLUs(pnlHeader, pnlLU, pnlData, False)
    End Sub

    Private Sub HandleNothingSelected()
        IsEditing = False
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
        ProfileMsgBox("CRIT", "SELECT", "crude")
    End Sub

    Private Sub DataGridView1_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEndEdit
        ChangesMade(btnSave, chkComplete, CrudeNotSaved)
        PopulateSummary()
    End Sub

    Private Sub DataGridView1_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentDoubleClick
        If e.ColumnIndex = 0 Then EditCrude()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If CrudeNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Crude Charge Detail")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveCrude()
                Case DialogResult.No
                    MyParent.dsCrude.RejectChanges()
                    MyParent.dsMappings.Tables("Crude").RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, CrudeNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        selectWorkBook(Me.Cursor, tbFilePath, cbSheets, MyParent.MyParent.pnlProgress, dvWB)
    End Sub

	Friend Function ImportCrude() As Boolean

		'Dim MyExcel As Object = OpenExcel()
		'Dim MyWB As Object = OpenWorkbook(MyExcel, tbFilePath.Text)
		'Dim MyWS As Object = OpenSheet(MyExcel, MyWB, cbSheets.Text)

		'If MyExcel Is Nothing Then Exit Function
		'If MyWB Is Nothing Then Exit Function
		'If MyWS Is Nothing Then Exit Function

		Dim MyExcel As Microsoft.Office.Interop.Excel.Application = Sa.Core.SaExcel.CurrentExcelApplication
		Dim MyWB As Microsoft.Office.Interop.Excel.Workbook = Sa.Core.SaExcel.OpenWorkbook_ReadOnly(MyExcel, tbFilePath.Text)
		Dim MyWS As Microsoft.Office.Interop.Excel.Worksheet = CType(MyWB.Worksheets(cbSheets.Text), Microsoft.Office.Interop.Excel.Worksheet)

		Dim CNum As String

		Try

			Dim newDV_LU As New DataView
			newDV_LU.Table = MyParent.MyParent.ds_Ref.Tables("Crude_LU")
			Dim row As DataRow = MyParent.dsMappings.Tables("Crude").Rows(0)
			Dim newrow As DataRow
			Dim dt As DataTable = MyParent.dsCrude.Tables("Crude")
			Dim rng As Object

			dt.Clear()

			ChangesMade(btnSave, chkComplete, CrudeNotSaved)
			PopulateSummary()

			For i As Integer = CInt(row!lkStartRow) To CInt(row!lkEndRow)

				If Not IsDBNull(MyWS.Range(row!lkCNum.ToString & i.ToString).Value) Then
					CNum = CStr(MyWS.Range(row!lkCNum.ToString & i.ToString).Value)
				Else
					GoTo GoToNext
				End If

				newDV_LU.RowFilter = "CNum = '" & CNum & "'"

				If newDV_LU.Count = 1 Then
					newrow = dt.NewRow
					newrow!CNum = CNum

					If Not IsDBNull(MyWS.Range(row!lkCrudeName.ToString & i.ToString).Value) AndAlso CStr(MyWS.Range(row!lkCrudeName.ToString & i.ToString).Value) <> "" Then
						newrow!CrudeName = CStr(MyWS.Range(row!lkCrudeName.ToString & i.ToString).Value)
					Else
						newrow!CrudeName = newDV_LU.Item(0)!CrudeName.ToString
					End If

					rng = MyWS.Range(row!lkGravity.ToString & i.ToString)
					If ImportingExcel.RangeHasValue(rng) Then
						newrow!Gravity = ReturnSingle(rng, 0.0)
					Else
						newrow!Gravity = CSng(newDV_LU.Item(0)!TypicalAPI)
					End If

					rng = MyWS.Range(row!lkSulfur.ToString & i.ToString)
					If ImportingExcel.RangeHasValue(rng) Then
						newrow!Sulfur = ReturnSingle(rng, 0.0)
					Else
						newrow!Sulfur = CSng(newDV_LU.Item(0)!TypicalSulfur)
					End If

					rng = MyWS.Range(row!lkBBL.ToString & i.ToString)
					If ImportingExcel.RangeHasValue(rng) Then
						newrow!BBL = ReturnDouble(rng, 0.0)
					End If

					rng = MyWS.Range(row!lkCostPerBBL.ToString & i.ToString)
					If ImportingExcel.RangeHasValue(rng) Then
						newrow!CostPerBBL = ReturnDouble(rng, 0.0)
					End If

					dt.Rows.Add(newrow)

				End If
GoToNext:
			Next i

			ImportCrude = True

		Catch ex As Exception
			ProfileMsgBox("CRIT", "MAPPING", "Crude")
		End Try

CleanupExcel:
		Sa.Core.SaExcel.CloseExcel(MyExcel, MyWB)
		'CloseExcel(MyExcel, MyWB, MyWS)
		Return ImportCrude
	End Function

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If ProfileMsgBox("YN", "IMPORT", "Crude Charge Detail") = DialogResult.Yes Then ImportCrude()
    End Sub

    Private Sub SA_CrudeCharge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.btnSave.TabIndex = 0
        Me.btnEdit.TabIndex = 1
        Me.btnImport.TabIndex = 2
        Me.btnAdd.TabIndex = 3
        Me.btnClear.TabIndex = 5

        Me.tbFilePath.TabIndex = 6
        Me.btnBrowse.TabIndex = 7
        Me.cbSheets.TabIndex = 8
        Me.rbData.TabIndex = 9
        Me.rbLinks.TabIndex = 10

        Me.dgvData.TabIndex = 11
        Me.dgvLinks.TabIndex = 12

        Me.cmbCountry.TabIndex = 13
        Me.dgvLU.TabIndex = 14
        Me.btnOK.TabIndex = 15
        Me.btnCancel.TabIndex = 16

        Me.btnPreview.TabIndex = 17
        Me.chkComplete.TabIndex = 18
        Me.txtBBL.TabIndex = 19
        Me.txtCostPerBBL.TabIndex = 20

        Me.btnClose.TabIndex = 21

    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub viewLinksData()
        dgvData.Visible = rbData.Checked
        dgvLinks.Visible = Not rbData.Checked

        If rbData.Checked Then
            lblHeaderCrude.Text = "Crude Charge Detail:   DATA"
        Else
            lblHeaderCrude.Text = "Crude Charge Detail:   LINKS"
        End If

        If IsLoaded And HasRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)
    End Sub

    Private Sub dgvData_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvData.UserDeletingRow
        If HasRights Then
            Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "crude entry")
            If result = DialogResult.Yes Then
                ChangesMade(btnSave, chkComplete, CrudeNotSaved)
                PopulateSummary()
            Else
                e.Cancel = True
            End If
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub tbFilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Crude", tbFilePath.Name, tbFilePath.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, CrudeNotSaved)

        My.Settings.workbookCrude = tbFilePath.Text
        My.Settings.Save()
    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Crude", cbSheets.Name, cbSheets.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, CrudeNotSaved)

        My.Settings.worksheetCrude = cbSheets.Text
        My.Settings.Save()
    End Sub

    Private Sub dgvData_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError
        dgvDataError(sender, e)
    End Sub
End Class

