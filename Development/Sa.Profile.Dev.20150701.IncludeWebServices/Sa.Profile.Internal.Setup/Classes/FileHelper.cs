﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace UpdateSetup.Classes
{
    static class FileHelper
    {
        public static List<string> GetFilesRecursive(string rootDir)
        {
            
            // Store results in the file results list.
            List<string> result = new List<string>();

            // Store a stack of our directories.
            Stack<string> stack = new Stack<string>();

            // Add initial directory.
            stack.Push(rootDir);

            // Continue while there are directories to process
            int rowIdx = 0;
            while (stack.Count > 0)
            {
                // A.
                // Get top directory
                string dir = stack.Pop();

                List<string> temp = new List<string>();

                if (rowIdx != 0)
                {
                    temp.Add(dir + "\\");
                }

                result.AddRange(temp);
                //result.AddRange(temp[0].ToList<string>());

                try
                {
                    // B
                    // Add all files at this directory to the result List.
                    result.AddRange(Directory.GetFiles(dir, "*.*"));

                    // C
                    // Add all directories at this directory.
                    foreach (string dn in Directory.GetDirectories(dir))
                    {
                        stack.Push(dn);
                    }
                }
                catch
                {
                    // D
                    // Could not open the directory
                }

                rowIdx++;
            }
            return result;
        }
    }

}
