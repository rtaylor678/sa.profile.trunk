<%@ Register TagPrefix="cc1" Namespace="C1.Web.C1WebReport" Assembly="C1.Web.C1WebReport, Version=2.5.20043.144, Culture=neutral, PublicKeyToken=3065690e254f28f6" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportControl.aspx.vb" Inherits="ReportControls.ReportControl"%>
<%@ Register TagPrefix="c1c" Namespace="C1.Web.C1Command" Assembly="C1.Web.C1Command, Version=1.0.20044.68, Culture=neutral, PublicKeyToken=96d8a77dc0c22f6b" %>
<HTML>
	<HEAD>
		<title></title>
		<!--DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"-->
		<META http-equiv="PRAGMA" content="NO-CACHE">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style>
			.small { FONT-SIZE: 12px; FONT-FAMILY: tahoma }
		</style>
		<LINK media="print" href="print.css" type="text/css" rel="StyleSheet">
	</HEAD>
	<body style="PAGE-BREAK-BEFORE: auto" MS_POSITIONING="GridLayout" color="#FFFFFF">
		<form id="Form1" method="post" runat="server">
			<table border="0">
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-REPEAT: no-repeat" width="350" background="ReportBGLeft.gif" bgColor="darkgray">
									<DIV id="LabelName" style="DISPLAY: inline; FONT-WEIGHT: bold; FONT-SIZE: 12pt; Z-INDEX: 106; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 27px"
										align="center">
										<%
										' This section added by GMO 12/6/2007
										' Code behind function invoked in order to get long description
										' for the report title.  Used only for process unit reports.
										Select Case Request.QueryString("rn").ToUpper
										Case "CDU REPORT", "FCC REPORT", "HYC REPORT", "REF REPORT", "PXYL REPORT"
											Response.Write(GetReportTitle(Request.QueryString("rn")))
										Case Else
										' END GMO 12/6/2007
											Response.Write(Request.QueryString("rn"))
										End Select
										
										%>
									</DIV>
									<br>
									<DIV id="LabelMonth" style="DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px"
										align="center"><%= IIf ( Request.QueryString("rn")="Gross Product Value",Format(CDate(Request.QueryString("sd")),"MMMM yyyy"), Request.QueryString("yr") + " Methodology" )%>
									</DIV>
									<br>
									<DIV id="LabelDataFilter" style="DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px"
										align="center">
										<%
										Dim bYTD As Boolean = CType(Request.QueryString("ytd"), Boolean)
										Dim bAVG As Boolean = CType(Request.QueryString("avg"), Boolean)

										If Request.QueryString("rn") = "Refinery Trends Report" then
											If (bYTD = True And bAVG = True) Or (bYTD = False And bAVG = False) Then
												' show total fields only
												Response.Write("Current Month")
											Else
												If bYTD = True Then
													' show ytd fields only
													Response.Write("Year To Date")
												Else
													If bAVG = True Then
														' show avg fields only
														Response.Write("Rolling Average")
													End If
												End If
											End If
										end if
										%>
									</DIV>
								</td>
								<TD></TD>
								<td width="42%">
									<div align="right"><IMG height="50" alt="" src="SA logo RECT.jpg" width="300"><br>
									</div>
									<DIV id="LabelMonth" style="FONT-SIZE: 10pt; FONT-FAMILY: Tahoma; HEIGHT: 16px" align="right">
										<%=  Date.Today.ToLongDateString()%>
									</DIV>
								</td>
							</tr>
						</table>
						<hr width="100%" color="#000000" SIZE="1">
					</td>
				</tr>
				<tr>
					<td><asp:placeholder id="phReport" runat="server"></asp:placeholder>
						<%GetReport()%>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>