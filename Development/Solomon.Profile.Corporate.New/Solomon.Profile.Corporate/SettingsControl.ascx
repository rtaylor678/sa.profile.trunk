﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SettingsControl.ascx.vb" Inherits="Solomon.Profile.Corporate.SettingsControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<%--<style>
	.rTrigger {
		font: 10/14px geneva, arial, verdana, sans-serif;
	}
	.rBranch {
		display: block;
		font-weight: bold;
		margin-left: 5px;
	}
	.rBranch a:link {
		font-weight: bold;
		margin-left: 5px;
		color: #3f4755;
		text-decoration: none;
	}
	.rBranch a:visited {
		font-weight: bold;
		margin-left: 5px;
		color: #3f4755;
		text-decoration: none;
	}
	.rBranch a:hover {
		font-weight: bold;
		border-top: white thin solid;
		border-bottom: gray thin solid;
		border-right: gray thin solid;
		margin-left: 5px;
		color: darkcyan;
		text-decoration: none;
		width: 90%;
	}
	.rBranch a:active {
		font-weight: bold;
		border-top: white thin solid;
		border-bottom: gray thin solid;
		border-right: gray thin solid; /*border-right: black thin solid;border-top: black thin solid;*/
		margin: 4pt; /*border-left: black thin solid;border-bottom: black thin solid;*/
		background-color: #cccccc;
		width: 90%;
	}
</style>--%>

<script type="text/javascript">

    //function setSettingsValue(node) {
    //    //SummaryInfo(node);
    //    //$('ul > li').removeClass();
    //    //debugger;
    //    $(this).addClass('menuselected');
    //    $('#summary').html(node);
    //}

</script>


<ul class="nav nav-list settingsNav">
    <% If IsAdminUser Then%>
    <li runat="server"><a class="tree-toggle nav-header" runat="server" onserverclick="navigateSettings">Create User</a></li>
    <li runat="server"><a class="tree-toggle nav-header" runat="server" onserverclick="navigateSettings">Manage Users</a></li>
    <% End If %>
    <li runat="server"><a class="tree-toggle nav-header" runat="server" onserverclick="navigateSettings">Change Password</a></li>
</ul>