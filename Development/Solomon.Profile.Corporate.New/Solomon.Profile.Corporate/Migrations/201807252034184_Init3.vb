Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Init3
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Users", "Company", Function(c) c.String())
            DropColumn("dbo.Users", "CurrentCompany")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Users", "CurrentCompany", Function(c) c.String())
            DropColumn("dbo.Users", "Company")
        End Sub
    End Class
End Namespace
