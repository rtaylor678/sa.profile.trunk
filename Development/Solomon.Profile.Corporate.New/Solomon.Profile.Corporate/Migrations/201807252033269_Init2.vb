Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Init2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Users", "Company", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Users", "Company")
        End Sub
    End Class
End Namespace
