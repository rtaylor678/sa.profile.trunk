
'Imports C1.Win.C1Chart
Imports System.Web
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports System.Web.UI.DataVisualization
Imports System.Collections.Generic


Public Class ChartControl2
    Inherits System.Web.UI.UserControl
   
    Protected WithEvents ListUnits As System.Web.UI.WebControls.DropDownList
    Protected WithEvents PanelUnits As System.Web.UI.WebControls.Panel
    Protected chrtData As New DataSet
    '    Protected WithEvents C1WebChart1 As C1.Web.C1WebChart.C1WebChart
    Protected WithEvents ltMessage As System.Web.UI.WebControls.Literal
    Protected WithEvents Chart1 As System.Web.UI.DataVisualization.Charting.Chart


#Region " Class Properties and Variables"

    Const VALUECOLUMN = 6
    Const DECIMALCOLUMN = 3

    Protected _refineries As New ArrayList
    Public Property Refineries() As ArrayList
        Get
            Return _refineries
        End Get
        Set(ByVal Value As ArrayList)
            _refineries = Value
        End Set
    End Property

    Dim _company As String
    Property Company() As String
        Get
            Return _company
        End Get
        Set(ByVal Value As String)
            _company = Value
        End Set
    End Property

    Dim _startDate As Date
    Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal Value As Date)
            _startDate = Value
        End Set
    End Property

    Dim _endDate As Date
    Property EndDate() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal Value As Date)
            _endDate = Value
        End Set
    End Property

    Dim _chartName As String
    Property ChartName() As String
        Get
            Return _chartName
        End Get
        Set(ByVal Value As String)
            _chartName = Value
        End Set
    End Property


    Dim _location As String
    Property Location() As String
        Get
            Return _location
        End Get
        Set(ByVal Value As String)
            _location = Value
        End Set
    End Property

    Dim _dataSet As String
    Property DataSet() As String
        Get
            Return _dataSet
        End Get
        Set(ByVal Value As String)
            _dataSet = Value
        End Set
    End Property

    Dim _twelveMonthAvg As String
    Property TwelveMonthAvg() As String
        Get
            Return _twelveMonthAvg
        End Get
        Set(ByVal Value As String)
            _twelveMonthAvg = Value
        End Set
    End Property

    Dim _target As String
    Property Target() As String
        Get
            Return _target
        End Get
        Set(ByVal Value As String)
            _target = Value
        End Set
    End Property

    Dim _ytd As String
    Property YTD() As String
        Get
            Return _ytd
        End Get
        Set(ByVal Value As String)
            _ytd = Value
        End Set
    End Property

    Dim _studyYear As String
    Property StudyYear() As Integer
        Get
            Return _studyYear
        End Get
        Set(ByVal Value As Integer)
            _studyYear = Value
        End Set
    End Property

    Dim _currency As String
    Property Currency() As String
        Get
            Return _currency.Trim
        End Get
        Set(ByVal Value As String)
            _currency = Value
        End Set
    End Property

    Dim _table As String
    Property TableName() As String
        Get
            Return _table
        End Get
        Set(ByVal Value As String)
            _table = Value
        End Set
    End Property

    Dim _scenario As String
    Property Scenario() As String
        Get
            Return _scenario
        End Get
        Set(ByVal Value As String)
            _scenario = Value
        End Set
    End Property

    Dim _uom As String
    Property UOM() As String
        Get
            Return _uom
        End Get
        Set(ByVal Value As String)
            _uom = Value
        End Set
    End Property

    Dim _field1 As String
    Property ColumnName() As String
        Get
            Return _field1
        End Get
        Set(ByVal Value As String)
            _field1 = Value
        End Set
    End Property

    Dim _field2 As String
    Property Field2() As String
        Get
            Return _field2
        End Get
        Set(ByVal Value As String)
            _field2 = Value
        End Set
    End Property

    Dim _field3 As String
    Property Field3() As String
        Get
            Return _field3
        End Get
        Set(ByVal Value As String)
            _field3 = Value
        End Set
    End Property

    Dim _field4 As String

    Property Field4() As String
        Get
            Return _field4
        End Get
        Set(ByVal Value As String)
            _field4 = Value
        End Set
    End Property

    Dim _field5 As String
    Property Field5() As String
        Get
            Return _field5
        End Get
        Set(ByVal Value As String)
            _field5 = Value
        End Set
    End Property


    Protected _current As Boolean
    Property CurrentMonth() As Boolean
        Get
            Return _current
        End Get
        Set(ByVal Value As Boolean)
            _current = Value
        End Set
    End Property

    Protected _includeYTD As Boolean
    Property YTDAverage() As Boolean
        Get
            Return _includeYTD
        End Get
        Set(ByVal Value As Boolean)
            _includeYTD = Value
        End Set
    End Property


    Protected _includeAvg As Boolean
    Property RollingAverage() As Boolean
        Get
            Return _includeAvg
        End Get
        Set(ByVal Value As Boolean)
            _includeAvg = Value
        End Set
    End Property


    Protected _avgField As String
    Property RollingAverageField() As String
        Get
            Return _avgField
        End Get
        Set(ByVal Value As String)
            _avgField = Value
        End Set
    End Property

    Protected _ytdField As String
    Property YTDAverageField() As String
        Get
            Return _ytdField
        End Get
        Set(ByVal Value As String)
            _ytdField = Value
        End Set
    End Property

#End Region


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GET DATA FROM DATABASE 
        QueryRefinery()

        'LOAD RESULTS ON PAGE
        If chrtData.Tables.Count <= 0 Or chrtData.Tables(0).Rows.Count <= 0 Then
            Response.Redirect("NoData.htm")
        Else
            'PLOT DATA ON CHART
            Dim timespan As Integer = DateDiff(DateInterval.Month, StartDate, EndDate) + 1
            'Dim plotter As New PlotData
            Dim currMth = IIf(CurrentMonth, ColumnName, String.Empty)
            Dim rollAvg = IIf(RollingAverage, RollingAverageField, String.Empty)
            Dim ytdAvg = IIf(YTDAverage, YTDAverageField, String.Empty)

            ''Clear old data before replotting
            'If C1WebChart1.ChartGroups.Group0.ChartData.SeriesList.Count = 0 And _
            '    C1WebChart1.ChartGroups.Group1.ChartData.SeriesList.Count = 0 Then
            '    'C1WebChart1.ChartGroups.Group0.ChartData.SeriesList.Clear()
            '    'C1WebChart1.ChartGroups.Group1.ChartData.SeriesList.Clear()
            '    plotter.Plot(chrtData, C1WebChart1, timespan, UOM, Currency, currMth, ytdAvg, rollAvg)
            'End If
        End If

        GetWindowsChart()
    End Sub


    'Public Function GetChartData() As String
    '    Dim strRow As String
    '    Dim s, g, k As Integer

    '    If chrtData.Tables.Count > 0 AndAlso chrtData.Tables(0).Rows.Count > 0 Then
    '        Dim dtLocations As DataTable = SelectDistinct(chrtData.Tables(0), "Location")
    '        Dim dtPeriod As DataTable = SelectDistinct(chrtData.Tables(0), "PeriodStartMod")

    '        'BUILD TABLE HEADER

    '        strRow += "<table frame=box rules=rows width=85% style='font-size: xx-small; font-family: Tahoma;'   cellPadding=2 cellSpacing=0 bordercolor=black  >"
    '        strRow += "<tr >"
    '        strRow += "<th width=15% bgcolor='#5F6A7F'><font color='white' nowrap>Month</font></th>"

    '        For k = 0 To dtLocations.Rows.Count - 1
    '            strRow += "<th bgcolor='#5F6A7F'>"
    '            strRow += "<font color='white'>&nbsp;" & dtLocations.Rows(k)("Location").ToString.ToUpper() & "&nbsp;</font></th>"
    '        Next
    '        strRow += "</tr>"

    '        'BUILD TABLE ROWS
    '        For k = 0 To DateDiff(DateInterval.Month, StartDate, EndDate)
    '            Dim dv As New DataView
    '            Dim period As Date = DateAdd(DateInterval.Month, k, StartDate)

    '            strRow += "<tr>"
    '            strRow += "<td style=""border-right: solid thin;"">" & _
    '                      "<div align=center>" & period.ToShortDateString & "</div></td>"

    '            For s = 0 To dtLocations.Rows.Count - 1
    '                Dim loc As String = dtLocations.Rows(s)("Location")
    '                Dim lineColor As String = C1WebChart1.ChartGroups(0).ChartData(s).LineStyle.Color.Name
    '                lineColor = "black"
    '                With dv
    '                    .Table = chrtData.Tables(0)
    '                    .RowFilter = "Location='" + loc + "' AND PeriodStartMod='" + period.ToString("M/yyyy") + "'"
    '                End With

    '                If dv.Count > 0 Then
    '                    Dim row As DataRowView = dv.Item(0)
    '                    Dim fieldFormat As String = row(DECIMALCOLUMN)

    '                    'Fields
    '                    If CurrentMonth Then
    '                        If Not IsDBNull(row(VALUECOLUMN)) And Not IsNothing(row(VALUECOLUMN)) Then

    '                            strRow += "<td ><div align=center><font color='" + lineColor + "'>" & String.Format(fieldFormat, row(VALUECOLUMN)) & "</font></div></td>"
    '                        Else
    '                            strRow += "<td ><div align=center><font color='" + lineColor + "'> - </font></div></td>"
    '                        End If
    '                    End If

    '                    If RollingAverage Then
    '                        If Not IsDBNull(row("Rolling Average")) And Not IsNothing(row("Rolling Average")) Then
    '                            strRow += "<td ><div align=center><font color='" + lineColor + "'>" & String.Format(fieldFormat, row("Rolling Average")) & "</font></div></td>"
    '                        Else
    '                            strRow += "<td ><div align=center><font color='" + lineColor + "'> - </font></div></td>"
    '                        End If
    '                    End If

    '                    If YTDAverage Then
    '                        If Not IsDBNull(row("Year-To-Date")) And Not IsNothing(row("Year-To-Date")) Then
    '                            strRow += "<td ><div align=center><font color='" + lineColor + "'>" & String.Format(fieldFormat, row("Year-To-Date")) & "</font></div></td>"
    '                        Else
    '                            strRow += "<td ><div align=center><font color='" + lineColor + "'> - </font></div></td>"
    '                        End If
    '                    End If
    '                End If

    '            Next
    '            strRow += "</tr>"
    '        Next
    '        strRow += "</table>"

    '    End If


    '    ' Response.Write(strRow)
    '    Return strRow
    'End Function
    'BUILD DATA TABLE. GETS CALLED IN ChartControl2.aspx
    Public Function GetChartData() As String
        Dim strRow As String
        Dim s, g, k As Integer

        If chrtData.Tables.Count > 0 AndAlso chrtData.Tables(0).Rows.Count > 0 Then
            Dim dtLocations As DataTable = SelectDistinct(chrtData.Tables(0), "Location")
            Dim dtPeriod As DataTable = SelectDistinct(chrtData.Tables(0), "PeriodStartMod")

            'BUILD TABLE HEADER

            strRow += "<table frame=box rules=rows width=85% style='font-size: xx-small; font-family: Tahoma;'   cellPadding=2 cellSpacing=0 bordercolor=black  >"
            strRow += "<tr >"
            strRow += "<th width=15% bgcolor='#5F6A7F'><font color='white' nowrap>Month</font></th>"

            For k = 0 To dtLocations.Rows.Count - 1
                strRow += "<th bgcolor='#5F6A7F'>"
                strRow += "<font color='white'>&nbsp;" & dtLocations.Rows(k)("Location").ToString.ToUpper() & "&nbsp;</font></th>"
            Next
            strRow += "</tr>"

            'BUILD TABLE ROWS
            For k = 0 To DateDiff(DateInterval.Month, StartDate, EndDate)
                Dim dv As New DataView
                Dim period As Date = DateAdd(DateInterval.Month, k, StartDate)

                strRow += "<tr>"
                strRow += "<td style=""border-right: solid thin;"">" & _
                          "<div align=center>" & period.ToShortDateString & "</div></td>"

                For s = 0 To dtLocations.Rows.Count - 1
                    Dim loc As String = dtLocations.Rows(s)("Location")
                    Dim lineColor As String = "black" 'Chart1.ChartGroups(0).ChartData(s).LineStyle.Color.Name
                    lineColor = "black"
                    With dv
                        .Table = chrtData.Tables(0)
                        .RowFilter = "Location='" + loc + "' AND PeriodStartMod='" + period.ToString("M/yyyy") + "'"
                    End With

                    If dv.Count > 0 Then
                        Dim row As DataRowView = dv.Item(0)
                        Dim fieldFormat As String = row(DECIMALCOLUMN)

                        'Fields
                        If CurrentMonth Then
                            If Not IsDBNull(row(VALUECOLUMN)) And Not IsNothing(row(VALUECOLUMN)) Then

                                strRow += "<td ><div align=center><font color='" + lineColor + "'>" & String.Format(fieldFormat, row(VALUECOLUMN)) & "</font></div></td>"
                            Else
                                strRow += "<td ><div align=center><font color='" + lineColor + "'> - </font></div></td>"
                            End If
                        End If

                        If RollingAverage Then
                            If Not IsDBNull(row("Rolling Average")) And Not IsNothing(row("Rolling Average")) Then
                                strRow += "<td ><div align=center><font color='" + lineColor + "'>" & String.Format(fieldFormat, row("Rolling Average")) & "</font></div></td>"
                            Else
                                strRow += "<td ><div align=center><font color='" + lineColor + "'> - </font></div></td>"
                            End If
                        End If

                        If YTDAverage Then
                            If Not IsDBNull(row("Year-To-Date")) And Not IsNothing(row("Year-To-Date")) Then
                                strRow += "<td ><div align=center><font color='" + lineColor + "'>" & String.Format(fieldFormat, row("Year-To-Date")) & "</font></div></td>"
                            Else
                                strRow += "<td ><div align=center><font color='" + lineColor + "'> - </font></div></td>"
                            End If
                        End If
                    End If

                Next
                strRow += "</tr>"
            Next
            strRow += "</table>"

        End If


        ' Response.Write(strRow)
        Return strRow
    End Function

    '
    ' Query refinery charts 
    '
    Private Sub QueryRefinery()
        Dim strRow, query As String
        Me.Scenario = "CLIENT"
        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod," + _
                "AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" + _
                " WHEN 0 then '{0:#,##0}'" + _
                " WHEN 1 then '{0:#,##0.0}'" + _
                " WHEN 2 then '{0:N}' " + _
                " END AS DecFormat ,s.Location,s.PeriodStart," & Me.ColumnName

        If YTDAverage Then
            query += ",ISNULL(" & YTDAverageField + ",0) AS 'Year-To-Date' "
        End If

        If RollingAverage Then
            query += ",ISNULL(" & RollingAverageField + ",0) AS 'Rolling Average' "
        End If

        If Refineries.Count > 0 Then

            'this line sets refLst to 'System.String[]'
            'Dim refLst As String = "'" + String.Join("','", Refineries.ToArray(GetType(String))) + "'"
            Dim refLst As String = String.Empty
            For counter As Integer = 0 To Refineries.Count - 1
                refLst = refLst + "'" + Refineries(counter).ToString().Trim() + "',"
            Next
            refLst = refLst.Remove(refLst.Length - 1, 1) 'remove trailing comma

            query += " FROM Chart_LU," & Me.TableName & " m, Submissions s ,TSort t" & _
                 " WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID=t.RefineryID AND t.RefineryID IN (" + refLst + ") AND s.PeriodStart BETWEEN '" + StartDate.ToShortDateString + _
                 "' AND '" + Me.EndDate.ToShortDateString + "' AND " + _
                 "RTRIM(s.DataSet) = '" + Me.DataSet + "'  AND  RTRIM(ChartTitle)='" + Me.ChartName + "'  AND s.CalcsNeeded IS NULL "
        Else
            query += " FROM Chart_LU," & Me.TableName & " m, Submissions s,TSort t  " & _
                 " WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID=t.RefineryID AND t.CompanyID='" + Company + "' AND s.PeriodStart BETWEEN '" + Me.StartDate.ToShortDateString + _
                 "' AND '" + Me.EndDate.ToShortDateString + "' AND " + _
                 "RTRIM(s.DataSet) = '" + DataSet + "'  AND  RTRIM(ChartTitle)='" + Me.ChartName + "'  AND s.CalcsNeeded IS NULL "
        End If


        'If (Me.StudyYear.ToString() <> String.Empty) Then
        '    Me.Scenario = Me.StudyYear.ToString()
        '    'Response.Write("Making it")
        'End If

        If Me.TableName.ToUpper = "GENSUM" Then
            query += " AND (Currency='N/A' OR Currency='" + Me.Currency + "') AND (m.FactorSet='N/A' OR m.FactorSet = '" + Me.StudyYear.ToString + "') AND m.UOM='" + Me.UOM + "' AND m.Scenario='" + Me.Scenario + "' "
        ElseIf Me.TableName.ToUpper = "MAINTAVAILCALC" Then
            query += " AND ( FactorSet='N/A' OR FactorSet ='" + Me.StudyYear.ToString + "' ) "
        End If

        query += " ORDER BY s.Location,s.PeriodStart "
        'Response.Write(query)
        chrtData = QueryDb(query)
		'chrtData.WriteXml("C:\crtData.xml")
        InsertMissingDates()
    End Sub

    '
    ' Insert dates where there no dates entered 
    '
    Private Sub InsertMissingDates()
        Dim dtPeriod As DataTable = SelectDistinct(chrtData.Tables(0), "PeriodStartMod")
        Dim dtLocation As DataTable = SelectDistinct(chrtData.Tables(0), "Location")
        Dim i, p As Integer

        For i = 0 To dtLocation.Rows.Count - 1
            Dim location As String = dtLocation.Rows(i)("Location")
            For p = 0 To DateDiff(DateInterval.Month, StartDate, EndDate)
                Dim period As String = DateAdd(DateInterval.Month, p, StartDate).ToString("M/yyyy")
                Dim count = chrtData.Tables(0).Compute("Count(Location)", "Location='" + location + "' AND PeriodStartMod='" + period + "'")

                If count = 0 Then
                    Dim row As DataRow = chrtData.Tables(0).NewRow
                    Dim oldrow As DataRow = chrtData.Tables(0).Rows(0)
                    row("Location") = location
                    row("PeriodStartMod") = period
                    row(DECIMALCOLUMN) = oldrow("DecFormat")
                    row("AxisLabelUS") = oldrow("AxisLabelUS")
                    row("AxisLabelMetric") = oldrow("AxisLabelMetric")
                    row("PeriodStart") = DateAdd(DateInterval.Month, p, StartDate)

                    chrtData.Tables(0).Rows.Add(row)
                End If
            Next

        Next
    End Sub

    'Private Sub C1WebChart1_DrawDataSeries(ByVal sender As Object, ByVal e As C1.Win.C1Chart.DrawDataSeriesEventArgs) Handles C1WebChart1.DrawDataSeries
    '    Dim ds As C1.Win.C1Chart.ChartDataSeries = sender

    '    Dim clr1 As Color = ds.LineStyle.Color
    '    Dim clr2 As Color = Color.White

    '    If C1WebChart1.ChartGroups(e.GroupIndex).ChartType = C1.Win.C1Chart.Chart2DTypeEnum.Bar And _
    '       e.Bounds.Height > 0 And e.Bounds.Width > 0 Then
    '        Dim lgb As System.Drawing.Drawing2D.LinearGradientBrush = _
    '          New LinearGradientBrush(e.Bounds, clr1, clr2, LinearGradientMode.Horizontal)

    '        e.Brush = lgb
    '    End If
    'End Sub

    Sub GetWindowsChart()
        ltMessage.Text = String.Empty

        If chrtData.Tables(0).Rows.Count = 0 Then
            ltMessage.Text = CreateHTMLErrorMessage("No data was found")
        Else

            Dim dataColumn As String = Request.QueryString.Item("node").ToString()

            'AveragePurchasedEnergyCost
            'PrepLineChart(chrtData.Tables(0), "PeriodStartMod", "Average Purchased Energy Cost")

            'Average Produced Fuel Cost
            'PrepLineChart(chrtData.Tables(0), "PeriodStartMod", "Average Produced Fuel Cost")

            'Average+Energy+Cost
            'PrepLineChart(chrtData.Tables(0), "PeriodStartMod", "Average Energy Cost")

            'Energy Consumption per Barrel
            '"section=CHARTS&node=Energy+Consumption+per+Barrel&ds=Actual&scenario=Client&startdate=12/1/2010&methodology=2010&rptOptions=Current Month&UOM=US&currency=USD&enddate=12/1/2011&table=GenSum&ytd=EnergyConsPerBbl_YTD&average=EnergyConsPerBbl_AVG&column=ISNULL(EnergyConsPerBbl%2c0)+AS+%7cEnergy+Consumption+per+Barrel%7c"


            PrepLineChart(chrtData.Tables(0), "PeriodStartMod", dataColumn)
        End If
    End Sub
    Private Sub PrepLineChart(dt As DataTable, textCol As String, numCol As String)

        Chart1.Visible = True
        Chart1.Legends.Add(New System.Web.UI.DataVisualization.Charting.Legend("Legend1"))
        Chart1.Legends("Legend1").Docking = System.Web.UI.DataVisualization.Charting.Docking.Bottom
        Chart1.Legends("Legend1").Alignment = Drawing.StringAlignment.Center

        Dim locations As New List(Of String)

        For Each row As DataRow In dt.Rows
            Dim location As String = row("Location").ToString()
            If Not locations.Contains(location) Then
                locations.Add(location)
            End If
        Next

        Dim colors(3) As System.Drawing.Color
        colors(0) = Color.Blue
        colors(1) = Color.Orange
        colors(2) = Color.Green

        Dim firstOne As Boolean = False
        Dim colorCount As Integer = 0
            For Each location As String In locations
                If Not firstOne Then
                    Dim s As New Charting.Series()
                s = NewLineSeries(location, dt, textCol, numCol, colors(colorCount))
                    Chart1.Series(0) = s
                    firstOne = True
                Else
                Chart1.Series.Add(NewLineSeries(location, dt, textCol, numCol, colors(colorCount)))
            End If
            colorCount = colorCount + 1
            Next

        'Chart1.Series.Add(NewLineSeries("London", dt, textCol, numCol, Color.Green)) ', dates, qtys))

        'rotate dates
        Chart1.ChartAreas(0).AxisX.LabelStyle.Angle = -90
        Chart1.ChartAreas("ChartArea1").AxisX.IsMarginVisible = True
        Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = False 'True
    End Sub

    Private Sub PrepLineChart_Orig2(dt As DataTable, textCol As String, numCol As String)

        Chart1.Visible = True
        Chart1.Legends.Add(New System.Web.UI.DataVisualization.Charting.Legend("Legend1"))
        Chart1.Legends("Legend1").Docking = System.Web.UI.DataVisualization.Charting.Docking.Bottom
        Chart1.Legends("Legend1").Alignment = Drawing.StringAlignment.Center

        Dim s As New Charting.Series()
        s = NewLineSeries("Dallas, TX", dt, textCol, numCol, Color.Blue) '                              dates, qtys)
        Chart1.Series(0) = s

        Chart1.Series.Add(NewLineSeries("Singapore", dt, textCol, numCol, Color.Orange)) ', dates, qtys))

        Chart1.Series.Add(NewLineSeries("London", dt, textCol, numCol, Color.Green)) ', dates, qtys))

        'rotate dates
        Chart1.ChartAreas(0).AxisX.LabelStyle.Angle = -90
        Chart1.ChartAreas("ChartArea1").AxisX.IsMarginVisible = True
        Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = False 'True
    End Sub
    Private Sub PrepLineChartAveragePurchasedEnergyCost(dt As DataTable, textCol As String, numCol As String)
        Chart1.Visible = True
        Chart1.Legends.Add(New System.Web.UI.DataVisualization.Charting.Legend("Legend1"))
        Chart1.Legends("Legend1").Docking = System.Web.UI.DataVisualization.Charting.Docking.Bottom
        Chart1.Legends("Legend1").Alignment = Drawing.StringAlignment.Center


        Dim s As New Charting.Series()
        s = NewLineSeries("Dallas, TX", dt, textCol, numCol, Color.Blue) '                              dates, qtys)
        Chart1.Series(0) = s

        Chart1.Series.Add(NewLineSeries("Singapore", dt, textCol, numCol, Color.Orange)) ', dates, qtys))

        Chart1.Series.Add(NewLineSeries("London", dt, textCol, numCol, Color.Green)) ', dates, qtys))

        'rotate dates
        Chart1.ChartAreas(0).AxisX.LabelStyle.Angle = -90
        Chart1.ChartAreas("ChartArea1").AxisX.IsMarginVisible = True
        Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = False 'True
    End Sub

    Private Sub PrepLineChart_ORIG(dt As DataTable, textCol As String, numCol As String)

        Dim dates As New List(Of DateTime)
        Dim qtys As New List(Of Single)
        Chart1.Visible = True
        Chart1.Legends.Add(New System.Web.UI.DataVisualization.Charting.Legend("Legend1"))
        Chart1.Legends("Legend1").Docking = System.Web.UI.DataVisualization.Charting.Docking.Bottom
        Chart1.Legends("Legend1").Alignment = Drawing.StringAlignment.Center

        Chart1.Series(0).Color = Color.Blue
        Chart1.Series(0).Name = "Dallas, TX"
        NewLineSeries_ORIG("Dallas, TX", dt, textCol, numCol, dates, qtys)
        If dates.Count > 1 Then
            Chart1.Series(0).ChartType = Charting.SeriesChartType.Line
        Else
            Chart1.Series(0).ChartType = Charting.SeriesChartType.Point
        End If
        Chart1.Series(0).Points.DataBindXY(dates, qtys)
        Chart1.Series(0)("ShowMarkerLines") = True

        Chart1.Series.Add("Singapore")
        Chart1.Series(1).Color = Color.Orange
        dates = New List(Of DateTime)
        qtys = New List(Of Single)
        NewLineSeries_ORIG("Singapore", dt, textCol, numCol, dates, qtys)
        If dates.Count > 1 Then
            Chart1.Series(1).ChartType = Charting.SeriesChartType.Line
        Else
            Chart1.Series(1).ChartType = Charting.SeriesChartType.Point
        End If
        Chart1.Series(1).Points.DataBindXY(dates, qtys)
        Chart1.Series(1)("ShowMarkerLines") = True

        Chart1.Series.Add("London")
        Chart1.Series(2).Color = Color.Green
        dates = New List(Of DateTime)
        qtys = New List(Of Single)
        NewLineSeries_ORIG("London", dt, textCol, numCol, dates, qtys)
        If dates.Count > 1 Then
            Chart1.Series(2).ChartType = Charting.SeriesChartType.Line
        Else
            Chart1.Series(2).ChartType = Charting.SeriesChartType.Point
        End If
        Chart1.Series(2).Points.DataBindXY(dates, qtys)
        Chart1.Series(2)("ShowMarkerLines") = True

        'rotate dates
        Chart1.ChartAreas(0).AxisX.LabelStyle.Angle = -90
        Chart1.ChartAreas("ChartArea1").AxisX.IsMarginVisible = True
        Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = False 'True
    End Sub

    Private Sub NewLineSeries_ORIG(ByVal SeriesName As String, dt As DataTable,
                              textCol As String, numCol As String,
                              ByRef dates As List(Of DateTime), ByRef qtys As List(Of Single))
        For Each row As DataRow In dt.Rows
            If row("Location").ToString() = SeriesName _
            AndAlso Not IsDBNull(row(numCol)) Then
                dates.Add(row(textCol))
                qtys.Add(row(numCol))
            End If
        Next

    End Sub
    Private Function NewLineSeries(SeriesName As String, dt As DataTable,
                              textCol As String, numCol As String, theColor As System.Drawing.Color) As System.Web.UI.DataVisualization.Charting.Series
        'ByRef dates As List(Of DateTime), ByRef qtys As List(Of Single)) As System.Web.UI.DataVisualization.Charting.Series
        Dim series As New System.Web.UI.DataVisualization.Charting.Series
        series.Color = theColor
        series.Name = SeriesName

        Dim dates As New List(Of DateTime)
        Dim qtys As New List(Of Single)

        For Each row As DataRow In dt.Rows
            If row("Location").ToString() = SeriesName _
            AndAlso Not IsDBNull(row(numCol)) Then
                dates.Add(row(textCol))
                qtys.Add(row(numCol))
            End If
        Next

        If dates.Count > 1 Then
            series.ChartType = Charting.SeriesChartType.Line
            series.MarkerStyle = DataVisualization.Charting.MarkerStyle.Square
            series.MarkerSize = 10
        Else
            series.ChartType = Charting.SeriesChartType.Point
        End If
        series.Points.DataBindXY(dates, qtys)
        series("ShowMarkerLines") = "True"
        Return series
    End Function

    Private Function ChangeLineSeries(ByVal series As System.Web.UI.DataVisualization.Charting.Series,
                                     ByVal SeriesName As String, dt As DataTable,
                          textCol As String, numCol As String, theColor As System.Drawing.Color,
                          ByRef dates As List(Of DateTime), ByRef qtys As List(Of Single)) As System.Web.UI.DataVisualization.Charting.Series
        series.Color = theColor
        series.Name = SeriesName
        If dates.Count > 1 Then
            series.ChartType = Charting.SeriesChartType.Line
        Else
            series.ChartType = Charting.SeriesChartType.Point
        End If
        series.Points.DataBindXY(dates, qtys)
        series("ShowMarkerLines") = "True"
        Return series
    End Function

    Private Sub PrepColumnChart(dt As DataTable, textCol As String, numCol As String)
        Dim dates() As Object '= ds.Tables(0).Rows(0).ItemArray()
        Dim qtys() As Object '= ds.Tables(0).Rows(1).ItemArray()
        Chart1.Visible = True

        Chart1.Legends.Add(New System.Web.UI.DataVisualization.Charting.Legend("Legend1"))
        Chart1.Legends("Legend1").Docking = System.Web.UI.DataVisualization.Charting.Docking.Bottom
        Chart1.Legends("Legend1").Alignment = Drawing.StringAlignment.Center ' .Center() ' = System.Web.UI.DataVisualization.Charting.Are

        For count As Integer = 0 To dt.Rows.Count - 1
            ReDim Preserve dates(count)
            ReDim Preserve qtys(count)
            dates(count) = dt.Rows(count)(textCol)
            qtys(count) = dt.Rows(count)(numCol)
        Next
        Chart1.Series(0).Color = Color.MediumBlue
        Chart1.Series(0).Points.DataBindXY(dates, qtys)

        'rotate dates
        Chart1.ChartAreas(0).AxisX.LabelStyle.Angle = -90
        'Or else use:
        'MyChart.Series[0].SmartLabelStyle.Enabled = false;
        'MyChart.Series[0].LabelAngle = 90; // Can vary from -90 to 90;


        'Dim columnNames As New List(Of String)
        'For count As Integer = 0 To dt.Columns.Count - 1
        '    columnNames.Add(dt.Columns(count).ColumnName)
        'Next

        For Each col As DataColumn In dt.Columns
            Dim name As String = col.ColumnName
            If name = "Year-To-Date" Then
                NewSeries("YTD", dt, name, DataVisualization.Charting.SeriesChartType.Line, Color.Black, DataVisualization.Charting.MarkerStyle.Diamond, 10)
            ElseIf name = "Target" Then
                Dim targetBlanks() As Object
                Dim targetNumbers() As Object
                Dim moreThanZero As Boolean = False
                For count As Integer = 0 To dt.Rows.Count - 1
                    ReDim Preserve targetBlanks(count)
                    ReDim Preserve targetNumbers(count)
                    targetBlanks(count) = String.Empty
                    targetNumbers(count) = dt.Rows(count)(name)
                    If targetNumbers(count) > 0 Then moreThanZero = True
                Next
                If moreThanZero Then
                    Chart1.Series.Add("Target")
                    Chart1.Series("Target").ChartType = DataVisualization.Charting.SeriesChartType.Line
                    Chart1.Series("Target").Color = Drawing.Color.Red
                    Chart1.DataManipulator.IsStartFromFirst = True
                    Chart1.Series("Target").MarkerSize = 10
                    Chart1.Series("Target").MarkerStyle = DataVisualization.Charting.MarkerStyle.Square
                    Chart1.Series("Target").Points.DataBindXY(targetBlanks, targetNumbers)
                End If
            ElseIf name = "Rolling Average" Then
                NewSeries("RollingAverage", dt, name, DataVisualization.Charting.SeriesChartType.Point, Color.Black, DataVisualization.Charting.MarkerStyle.Circle, 10)
            End If
        Next

    End Sub

    Private Sub NewSeries(name As String, dt As DataTable, columnName As String, chartType As DataVisualization.Charting.SeriesChartType, _
                               color As Drawing.Color, style As DataVisualization.Charting.MarkerStyle, _
                               size As Integer)

        Chart1.Series.Add(name)
        Chart1.Series(name).ChartType = chartType ' DataVisualization.Charting.SeriesChartType.Point
        Chart1.Series(name).Color = color ' Drawing.Color.Black
        Chart1.DataManipulator.IsStartFromFirst = True
        Dim blanks() As Object
        Dim numbers() As Object
        For count As Integer = 0 To dt.Rows.Count - 1
            ReDim Preserve blanks(count)
            ReDim Preserve numbers(count)
            blanks(count) = String.Empty
            numbers(count) = dt.Rows(count)(columnName)
        Next
        If Not IsNothing(size) Then
            Chart1.Series(name).MarkerSize = size ' 10
        End If
        If Not IsNothing(style) Then
            Chart1.Series(name).MarkerStyle = style ' DataVisualization.Charting.MarkerStyle.Circle
        End If
        Chart1.Series(name).Points.DataBindXY(blanks, numbers)
    End Sub

    Private Function CreateHTMLErrorMessage(ByVal msg As String) As String
        Dim txt As String = "<table width=672 height=368 border=0 align=center cellspacing=0 bordercolor=#76777C>" + _
                                  " <tr>" + _
                                      "<td height=35 align=center valign=top><p class=style1>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>" + msg + "</p></td>" + _
                                  "</tr>" + _
                                  "</table>"

        Return txt
    End Function

End Class
