Imports System.Drawing.Bitmap
Imports System.Drawing.Image
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions


Public Class PopUpWin2
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents download As System.Web.UI.WebControls.Panel
    Protected WithEvents IMG2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnDataDump As System.Web.UI.WebControls.ImageButton
    Protected WithEvents pgSection As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgNode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pnlContent As System.Web.UI.WebControls.Panel
    Protected WithEvents phContent As System.Web.UI.WebControls.PlaceHolder
    'Protected WithEvents C1WebMenu1 As C1.Web.C1Command.C1WebMenu
    'Protected WithEvents C1MenuItem1 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1SubMenu1 As C1.Web.C1Command.C1SubMenu
    'Protected WithEvents C1MenuItem6 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1MenuItem7 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1MenuItem8 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1SubMenu2 As C1.Web.C1Command.C1SubMenu
    'Protected WithEvents C1MenuItem10 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1MenuItem11 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1MenuItem12 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1WebReport1 As C1.Web.C1WebReport.C1WebReport

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        If Request.QueryString("section").ToUpper = "CHARTS" Then
            'C1MenuItem1.Visible = False
        Else
            'C1MenuItem1.Visible = True
        End If

        'Response.Filter = New ResponseFilter(Response.Filter)


        'If Not IsNothing(Request.QueryString("C1MenuItem8")) Then

        'End If
    End Sub

#End Region

    Dim c3 As Control
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
		LoadResults()

    End Sub


    Sub LoadResults()
        Dim section As String = Request.QueryString("section")
        Dim selectedNode As String = Request.QueryString("node")
        Dim item As String
		Dim refineries() As String

        Session("ShowAll") = False

        Select Case section.ToUpper
            Case "REPORTS"
                c3 = LoadControl("ReportControl.ascx")
                CType(c3, ReportControl).ReportName = selectedNode
                CType(c3, ReportControl).Data = Request.QueryString("ds")  '"Actual"
                CType(c3, ReportControl).StartDate = Request.QueryString("startdate")
                CType(c3, ReportControl).Company = Session("CompID")
                CType(c3, ReportControl).StudyYear = Request.QueryString("methodology")
                CType(c3, ReportControl).Scenario = Request.QueryString("scenario")
                CType(c3, ReportControl).UnitsOfMeasure = Request.QueryString("UOM")
                CType(c3, ReportControl).Currency = Request.QueryString("currency")
                CType(c3, ReportControl).Refineries.Clear()

                Select Case Request.QueryString("rptOptions").ToUpper
                    Case "CURRENT MONTH"
                        CType(c3, ReportControl).CurrentMonth = True
                    Case "ROLLING AVERAGE"
                        CType(c3, ReportControl).RollingAverage = True
                    Case "YTD AVERAGE"
                        CType(c3, ReportControl).YTDAverage = True
                End Select

                If Not IsNothing(Session("Refineries")) Then
                    refineries = Session("Refineries").ToString.Split(",".ToCharArray())
                    For Each item In refineries
                        If item.Length > 0 Then
                            CType(c3, ReportControl).Refineries.Add(item)
                        End If
                    Next
                End If


            Case "CHARTS"
                'Pre-populate chart_lu
                c3 = LoadControl("ChartControl2.ascx")
                CType(c3, ChartControl2).ChartName = selectedNode
                CType(c3, ChartControl2).DataSet = Request.QueryString("ds") '"Actual"
				CType(c3, ChartControl2).StartDate = Request.QueryString("startdate")
				If Request.QueryString("enddate") = "" Then
					CType(c3, ChartControl2).EndDate = CType(c3, ChartControl2).StartDate
				Else
					CType(c3, ChartControl2).EndDate = Request.QueryString("enddate")
				End If
                CType(c3, ChartControl2).Company = Session("CompID")
                CType(c3, ChartControl2).StudyYear = Request.QueryString("methodology")
                CType(c3, ChartControl2).UOM = Request.QueryString("UOM")
                CType(c3, ChartControl2).Currency = Request.QueryString("currency")
                CType(c3, ChartControl2).Scenario = Request.QueryString("scenario")
                CType(c3, ChartControl2).TableName = Request.QueryString("table")

				If Not IsNothing(Request.QueryString("column")) Then
					CType(c3, ChartControl2).ColumnName = Server.UrlDecode(Request.QueryString("column").Replace("|", "'"))
				End If

				CType(c3, ChartControl2).RollingAverageField = Request.QueryString("average")
				CType(c3, ChartControl2).YTDAverageField = Request.QueryString("ytd")

				Select Case Request.QueryString("rptOptions").ToUpper
					Case "CURRENT MONTH"
						CType(c3, ChartControl2).CurrentMonth = True
					Case "ROLLING AVERAGE"
						CType(c3, ChartControl2).RollingAverage = True
					Case "YTD AVERAGE"
						CType(c3, ChartControl2).YTDAverage = True
				End Select

				CType(c3, ChartControl2).Refineries.Clear()

				If Not IsNothing(Session("Refineries")) Then
					refineries = Session("Refineries").ToString.Split(",".ToCharArray())
					For Each item In refineries
						If item.Length > 0 Then
							CType(c3, ChartControl2).Refineries.Add(item)
						End If
					Next
				End If

		End Select

        If Not IsNothing(c3) Then
            phContent.Controls.Add(c3)
        End If

    End Sub


    Private Sub btnDataDump_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDataDump.Click
        If TypeOf c3 Is ReportControl Then
            CType(c3, ReportControl).SendDataDump()
        End If

    End Sub


	Private Sub Exit_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs)

	End Sub


    Public Function GetHTML(ByVal writer As HtmlTextWriter) As String
        Dim sw As New System.IO.StringWriter
        Dim localWriter As New HtmlTextWriter(sw)
        Dim html As String = sw.ToString()

		Return html

	End Function


End Class
