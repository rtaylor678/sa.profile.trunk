﻿Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.Owin
Imports Owin

<Assembly: OwinStartupAttribute(GetType(Startup))>

Partial Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        ConfigureAuth(app)
        CreateRolesandUsers()
    End Sub

    Private Sub CreateRolesandUsers()
        Dim context As ApplicationDbContext = New ApplicationDbContext()
        Dim roleManager = New RoleManager(Of IdentityRole)(New RoleStore(Of IdentityRole)(context))
        Dim UserManager = New UserManager(Of ApplicationUser)(New UserStore(Of ApplicationUser)(context))

        If Not roleManager.RoleExists("SuperAdmin") Then
            Dim role = New IdentityRole()
            role.Name = "SuperAdmin"
            roleManager.Create(role)
            Dim user = New ApplicationUser()
            user.Email = "adminprofilecorp@solomononline.com"
            user.UserName = "admin"
            user.EmailConfirmed = True
            Dim userPWD As String = "All@4114all"
            Dim chkUser = UserManager.Create(user, userPWD)

            If chkUser.Succeeded Then
                Dim result1 = UserManager.AddToRole(user.Id, "SuperAdmin")
            End If
        End If

        If Not roleManager.RoleExists("Admin") Then
            Dim role = New IdentityRole()
            role.Name = "Admin"
            roleManager.Create(role)
        End If

        If Not roleManager.RoleExists("User") Then
            Dim role = New IdentityRole()
            role.Name = "User"
            roleManager.Create(role)
        End If
    End Sub
End Class
