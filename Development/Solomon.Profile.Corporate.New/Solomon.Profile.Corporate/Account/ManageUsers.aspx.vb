﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.Owin

Public Class ManageUsers
    Inherits System.Web.UI.Page

    Protected WithEvents sdUsers As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents DsUsers As Solomon.Profile.Corporate.dsUsers
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand

#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
    Dim _adminEmailAddress As String = ConfigurationManager.AppSettings("AdminEmailAddress").ToString()
    Private Shared _currentRecord As String
    Private Shared _currentAction As String
    Private stree As Control
#End Region


    Private Sub InitializeComponent()

        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.sdUsers = New System.Data.SqlClient.SqlDataAdapter
        Me.DsUsers = New Solomon.Profile.Corporate.dsUsers
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand

        CType(Me.DsUsers, System.ComponentModel.ISupportInitialize).BeginInit()

        Me.SqlConnection1.ConnectionString = _connectionString
        ' ---------------------------------------------------------------------
        ' sdUsers
        '
        Me.sdUsers.SelectCommand = Me.SqlSelectCommand1
        Me.sdUsers.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Users",
                                                                                                                             New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("UserName", "UserName"),
                                                                                                                             New System.Data.Common.DataColumnMapping("Email", "Email"),
                                                                                                                             New System.Data.Common.DataColumnMapping("Company", "Company"),
                                                                                                                             New System.Data.Common.DataColumnMapping("Id", "Id")})})

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id, UserName, Email, Company FROM dbo.Users"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' DsUsers
        '
        Me.DsUsers.DataSetName = "dsUsers"
        Me.DsUsers.Locale = New System.Globalization.CultureInfo("en-US")

        ' ---------------------------------------------------------------------

        CType(Me.DsUsers, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        Me.InitializeComponent()

        If Not IsPostBack Then
            GridDataLoad()
        End If

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("Company") Is Nothing Then
            HiddenField1.Value = Session("Company")
        End If
        If Not (User.Identity.IsAuthenticated) Then
            Response.Redirect("~/Account/Login")
        End If
        stree = Page.LoadControl("~/SettingsControl.ascx")
        PlaceHolder5.Controls.Add(stree)
    End Sub

    'Protected Sub DeleteUser_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    lblModalTitle.Text = "Delete User"
    '    lblModalBody.Text = "Are you sure you want to Delete the user:"
    '    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
    '    upModal.Update()
    'End Sub

    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Dim index As String = e.CommandArgument
        Dim currentUser As String = String.Empty
        Dim reader As SqlDataReader
        SqlConnection1 = New SqlConnection
        SqlConnection1.ConnectionString = _connectionString
        Dim cmd As New SqlCommand("SELECT * FROM Users WHERE Id='" + index + "'", SqlConnection1)
        cmd.Connection.Open()
        reader = cmd.ExecuteReader()
        If reader.Read Then
            currentUser = reader("UserName")
        End If

        If e.CommandName = "DeleteUser" Then
            _currentRecord = index.ToString()
            _currentAction = e.CommandName
            lblModalTitle.Text = "Delete User"
            lblModalBody.Text = "Are you sure you want to Delete the user: " + currentUser
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
            upModal.Update()
        End If
        If e.CommandName = "ResetPass" Then
            _currentRecord = index.ToString()
            _currentAction = e.CommandName
            lblModalTitle.Text = "Reset User Details"
            lblModalBody.Text = "Are you sure you want to reset the login credentials for this user :" + currentUser
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
            upModal.Update()
        End If
        If e.CommandName = "Refineries" Then
            _currentRecord = index.ToString()
            _currentAction = e.CommandName
            Session("UserId") = _currentRecord
            Response.Redirect("/Account/UserRefineries")
        End If
    End Sub

    Protected Sub Action_ServerClick(sender As Object, e As EventArgs)
        Try
            Dim manager = Context.GetOwinContext().GetUserManager(Of ApplicationUserManager)()

            If _currentAction = "DeleteUser" Then
                Dim user = manager.FindById(_currentRecord)
                Dim rolesForUser = manager.GetRoles(_currentRecord)
                If rolesForUser.Count() > 0 Then
                    For Each item In rolesForUser.ToList()
                        Dim result = manager.RemoveFromRoles(user.Id, item)
                    Next
                End If
                DeleteUserRefineries(user.Id)
                manager.Delete(user)
                SuccessMessage.Visible = True
                SuccessMessage.InnerText = "User Deleted Successfully!"
                _currentRecord = ""
                GridDataLoad()
            End If
            If _currentAction = "ResetPass" Then
                Dim resetToken = manager.GeneratePasswordResetToken(_currentRecord)
                If resetToken IsNot Nothing Then
                    Dim user = manager.FindById(_currentRecord)
                    If user Is Nothing Then
                        ErrorMessage.Visible = True
                        ErrorMessage.InnerText = "No user found"
                        Return
                    End If
                    Dim result = manager.ResetPassword(user.Id, resetToken, "All@4114all")
                    If result.Succeeded Then
                        SuccessMessage.Visible = True
                        SuccessMessage.InnerText = "User Password was reset successful! An email will be sent to the user with temporary login credentials"
                        Dim MailBody = GenerateMailBody(user.UserName, "All@4114all")
                        manager.SendEmail(user.Id, "Profile Corp New Login Credentials", MailBody)
                        Return
                    End If
                    ErrorMessage.Visible = True
                    ErrorMessage.InnerText = result.Errors.FirstOrDefault()
                    Return
                End If
                ErrorMessage.Visible = True
                ErrorMessage.InnerText = "An error has occurred"
                _currentRecord = ""
            End If
        Catch ex As Exception
            ErrorMessage.Visible = True
            ErrorMessage.InnerText = ex.ToString()
        End Try
    End Sub

    Public Function GenerateMailBody(ByVal userName As String, ByVal tempPassword As String) As String
        Dim mailBody As StringBuilder = New StringBuilder()
        mailBody.AppendFormat("Solomon Profile Corp administrator did a reset on your account.").AppendLine()
        mailBody.AppendFormat("Please sign in with the following credentials and make sure you change your password upon sign in").AppendLine()
        mailBody.AppendFormat("Username: {0}", userName).AppendLine()
        mailBody.AppendFormat("Password: {0}", tempPassword).AppendLine()
        mailBody.AppendFormat("").AppendLine()
        mailBody.AppendFormat("Thanks").AppendLine()
        mailBody.AppendFormat("Solomon Profile Corp Administrator").AppendLine()
        Return mailBody.ToString()
    End Function

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()
    End Sub

    'Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
    '    e.Row.Cells[0].Visible = False
    'End Sub


    Private Sub DeleteUserRefineries(ByVal userId As String)
        Using connection As New SqlConnection(_connectionString)
            Dim command As New SqlCommand("DELETE FROM UserRefineries WHERE UserID=@UserId;", connection)
            command.Parameters.Add("@UserId", SqlDbType.VarChar).Value = userId
            command.Connection.Open()
            command.ExecuteNonQuery()
        End Using
    End Sub

    Private Sub GridDataLoad()
        sdUsers.SelectCommand.CommandText += " WHERE UserName NOT IN ('Admin')"
        sdUsers.Fill(DsUsers)

        GridView1.DataSource = DsUsers
        GridView1.DataMember = DsUsers.Tables(0).TableName
        'GridView1.DataTextField = DsUsers.Tables(0).Columns(0).ColumnName
        'GridView1.DataValueField = DsUsers.Tables(0).Columns(0).ColumnName
        GridView1.AutoGenerateColumns = False
        GridView1.DataBind()
        'If Not GridView1.Contains(New ListItem("USD")) Then
        'End If
    End Sub
End Class