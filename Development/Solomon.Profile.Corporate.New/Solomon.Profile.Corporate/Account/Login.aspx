﻿<%--<%@ Page Title="Log in" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Login.aspx.vb" Inherits="Solomon.Profile.Corporate.Login" Async="true" %>



<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>

    <div class="row">
        <div class="col-md-8">
            <section id="loginForm">
                <div class="form-horizontal">
                    <h4>Use a local account to log in.</h4>
                    <hr />
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="The email field is required." />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="checkbox">
                                <asp:CheckBox runat="server" ID="RememberMe" />
                                <asp:Label runat="server" AssociatedControlID="RememberMe">Remember me?</asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
                <p>
                    <asp:HyperLink runat="server" ID="RegisterHyperLink" ViewStateMode="Disabled">Register as a new user</asp:HyperLink>
                </p>

            </section>
        </div>

        <div class="col-md-4">
            <section id="socialLoginForm">
                <uc:openauthproviders runat="server" id="OpenAuthLogin" />
            </section>
        </div>
    </div>
</asp:Content>--%>


<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="Login.aspx.vb" Inherits="Solomon.Profile.Corporate.Login" %>


<asp:Content ID="index_body" runat="server" ContentPlaceHolderID="PH_Work">

    <div class="col-sm-12 col-md-8 col-centered">
        <div class="row">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                <p class="text-danger">
                    <asp:Literal runat="server" ID="FailureText" />
                </p>
            </asp:PlaceHolder>
            <div class="alert alert-info alert-dismissible" role="alert" runat="server" id="SuccessMessage">
                You are currently on this domain: <asp:Literal ID="DomainName" runat="server" />
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section id="loginForm">
                        <asp:PlaceHolder runat="server" ID="PlaceHolder1" Visible="false">
                            <p class="text-danger">
                                <asp:Literal runat="server" ID="Literal1" />
                            </p>
                        </asp:PlaceHolder>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">Log In</div>
                            </div>
                            <div style="padding-top: 30px" class="panel-body">
                                <div style="display: none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="login-username" type="text" class="form-control" name="login-username" value="" placeholder="username">
                                </div>
                                <div style="margin-bottom: 15px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input id="login-password" type="password" class="form-control" name="login-password" placeholder="password">
                                </div>
                                <div class="input-group">
                                    <div class="checkbox remeberMe">
                                        <asp:CheckBox runat="server" CssClass="remeberMe" ID="RememberMe" />Remember me?
                                    </div>
                                </div>
                                <div style="margin-top: 10px" class="form-group">
                                    <!-- Button -->
                                    <asp:Button runat="server" OnClick="LogIn" Text="Login" CssClass="btn btn-default profile-button-primary" />
                                </div>
                            </div>
                        </div>
                        <%--   <div class="form-horizontal">
                            <h4>Please sign in</h4>
                            <hr />
                           
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-4 control-label">UserName</asp:Label>
                                <div class="col-md-8">
                                    <asp:TextBox runat="server" ID="UserName" CssClass="form-control" TextMode="SingleLine" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                                        CssClass="text-danger" ErrorMessage="The UserName field is required." />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-4 control-label">Password</asp:Label>
                                <div class="col-md-8">
                                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control"/>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-12">
                                    <div class="checkbox">
                                        <asp:CheckBox runat="server" ID="RememberMe" />
                                        <asp:Label runat="server" AssociatedControlID="RememberMe">Remember me?</asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-12">
                                    <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-primary" />
                                </div>
                            </div>
                        </div>--%>
                    </section>
                </div>

            </div>
        </div>
    </div>




    <%--<div class="row">
        <div class="col-sm-12 col-centered">
            <script type="text/javascript">

                Cookies.set('solomon_test', 'solomon_test', { expires: 1 });
                var cookieVal = Cookies.get('solomon_test');

                if (cookieVal == null) {
                    document.writeln("<center><span style=\"COLOR:RED;\">Cookies must be enabled to use this site.</span></center>");
                }
                else {
                    Cookies.remove('solomon_test');
                }

            </script>
        </div>
    </div>--%>

    <script type="text/javascript">
        $(function () {
            $('#summary').html('Login');
            // FOCUS ON USERNAME TEXTBOX
            $('#frmSignInUName').focus();
        });
    </script>

</asp:Content>




