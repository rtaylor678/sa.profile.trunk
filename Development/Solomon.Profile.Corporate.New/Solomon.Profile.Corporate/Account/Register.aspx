﻿<%@ Page Title="Create User" Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="Register.aspx.vb" Inherits="Solomon.Profile.Corporate.Register" %>

<%@ Import Namespace="Solomon.Profile.Corporate" %>
<%@ Import Namespace="Microsoft.AspNet.Identity" %>
<%--<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Create a new account</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="The email field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>--%>

<asp:Content ID="CreateUser_head" runat="server" ContentPlaceHolderID="PH_Head">
</asp:Content>

<asp:Content ID="CreateUser_Sidebar" runat="server" ContentPlaceHolderID="PH_Sidebar">
    <asp:PlaceHolder ID="PlaceHolder4" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="CreateUser_body" runat="server" ContentPlaceHolderID="PH_Work">



           <%-- <div class="form-horizontal">
                <hr />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label">UserName</asp:Label>
                    <div class="col-md-10">
                           <asp:TextBox runat="server" ID="UserName" CssClass="form-control" TextMode="SingleLine" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" />
                    </div>
                </div>
            </div>--%>

    <div class="row" id="main-work">
        <br />
        <br />
        <br />
        <br />
        <br />
        <div class="well">

            <div class="alert alert-success alert-dismissible" role="alert" runat="server" id="SuccessMessage" visible="false">>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="ErrorMessage" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <asp:HiddenField ID="HiddenField1" runat="server" /> 
            <div class="passwordBlock">

                <div class="form-group row">
                    <label for="inputfirstname" class="col-sm-2 col-form-label margin-top-10">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="inputfirstname" name="inputfirstname" placeholder="FirstName">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="inputlastname" name="inputlastname" placeholder="LastName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-2 col-form-label margin-top-10">UserName</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputUsername" name="inputUsername" placeholder="UserName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label margin-top-10">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email">
                    </div>
                </div>
                <%--<asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="inputEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>--%>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label margin-top-10">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="UserRole" class="col-sm-2 col-form-label margin-top-5">Role</label>
                    <div class="col-sm-10">
                        <asp:DropDownList ID="UserRole" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputCompany" class="col-sm-2 col-form-label margin-top-10">Company</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail39" runat="server" disabled="disabled">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 margin-top-10">
                        <asp:Button type="submit" ID="FormCreateUser" OnClick="CreateUser_Click" runat="server" class="btn btn-default profile-button-primary" Text="Create User"></asp:Button>
                        <asp:Button type="submit" ID="ClearForm" OnClientClick="this.form.reset();return false;" runat="server" class="btn btn-default profile-button-secondary" Text="Clear Form"></asp:Button>
                    </div>
                    <div class="col-sm-6 margin-top-10">
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $(".alert").fadeTo(5000, 500).slideUp(500, function () {
                $(".alert").slideUp(500);
            });
            $('#summary').html('Create User');

            //settings menu highlight active item
            $('.settingsNav li').removeClass('settingsActive');
            $('.settingsNav li:contains("Create User")').addClass('settingsActive');
            $('.wrap_side').css('height', '385px');

            $('.navigationStandard,.navigationHome').show();
            var hv = $("#"+ '<%= HiddenField1.ClientID %>').val();
            $('#Cmpany').text(hv)
        });
    </script>
</asp:Content>
