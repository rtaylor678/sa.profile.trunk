﻿Imports System.Data.SqlClient
Imports Microsoft.AspNet.Identity.Owin

Public Class UserRefineries
    Inherits Page

    Protected WithEvents DsTSort1 As dsTSort
    Protected WithEvents sdTSort As SqlClient.SqlDataAdapter
    Protected WithEvents DsUsers As Solomon.Profile.Corporate.dsUsers
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents SqlSelectCommand1 As SqlClient.SqlCommand
    Private stree As Control
    Private _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()

    Private Sub InitializeComponent()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.DsTSort1 = New dsTSort
        Me.sdTSort = New SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.DsTSort1, ComponentModel.ISupportInitialize).BeginInit()
        Me.SqlConnection1.ConnectionString = _connectionString
        ' DsTSort1
        '
        Me.DsTSort1.DataSetName = "dsTSort"
        Me.DsTSort1.Locale = New Globalization.CultureInfo("en-US")

        '
        'sdTSort
        '
        Me.sdTSort.SelectCommand = Me.SqlSelectCommand1
        Me.sdTSort.TableMappings.AddRange(New Common.DataTableMapping() {New Common.DataTableMapping("Table", "TSort", New Common.DataColumnMapping() {New Common.DataColumnMapping("Location", "Location"), New System.Data.Common.DataColumnMapping("RefineryID", "RefineryID")})})
        ' SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Location, RefineryID FROM dbo.TSort"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1


        CType(Me.DsTSort1, ComponentModel.ISupportInitialize).EndInit()
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        Me.InitializeComponent()

        If Not IsPostBack Then
            If Not IsNothing(Session("UserId")) Then
                Dim manager = Context.GetOwinContext().GetUserManager(Of ApplicationUserManager)()
                Dim appUser = manager.FindByIdAsync(Session("UserId"))
                SelectedUser.Text = appUser.Result.UserName
            End If

            sdTSort.SelectCommand.CommandText += " WHERE CompanyID='" + Session("Company") + "' ORDER BY Location"
            sdTSort.Fill(DsTSort1)

            lbRefinery.DataSource = DsTSort1
            lbRefinery.DataMember = "TSort"
            lbRefinery.DataTextField = "Location"
            lbRefinery.DataValueField = "RefineryID"
            lbRefinery.DataBind()



            Dim allRecords As ClassName() = Nothing
            Dim sql As String = "SELECT TS.Location FROM UserRefineries  AS UR INNER JOIN TSORT TS ON UR.RefineryID = TS.RefineryID
                                    WHERE UR.UserID =  @UserId"

            Using conn As SqlConnection = New SqlConnection(_connectionString)
                Using command = New SqlCommand(sql, conn)
                    conn.Open()
                    command.Parameters.Add("UserId", SqlDbType.VarChar).Value = Session("UserId")

                    Using reader = command.ExecuteReader()
                        Dim list = New List(Of ClassName)()

                        While reader.Read()
                            list.Add(New ClassName With {
                                .Col1 = reader.GetString(0)
                            })
                        End While

                        allRecords = list.ToArray()
                    End Using
                End Using
            End Using

            For Each i As ListItem In lbRefinery.Items
                For Each j In allRecords
                    If j.Col1 = i.Text Then
                        i.Selected = True
                    End If
                Next
            Next

        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
          If Not Session("Company") Is Nothing Then
                    HiddenField1.Value = Session("Company")
        End If
        If Not (User.Identity.IsAuthenticated) Then
            Response.Redirect("~/Account/Login")
        End If
        stree = Page.LoadControl("~/SettingsControl.ascx")
        PlaceHolder7.Controls.Add(stree)
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs)
        If Not IsNothing(Session("UserId")) Then
            'delete existing records
            Using connection As New SqlConnection(_connectionString)
                Dim command As New SqlCommand("DELETE FROM UserRefineries WHERE UserID=@UserId;", connection)
                command.Parameters.Add("@UserId", SqlDbType.VarChar).Value = Session("UserId")
                command.Connection.Open()
                command.ExecuteNonQuery()
            End Using

            'Insert new records
            Dim sql As String = "INSERT INTO UserRefineries (UserID, RefineryID, Company) VALUES (@Value1, @Value2, @Value3);"

            Using connection1 As New SqlConnection(_connectionString)
                Dim retvalue As Integer
                connection1.Open()

                For Each i As ListItem In lbRefinery.Items
                    If i.Selected = True Then
                        Dim command1 As New SqlCommand()
                        command1.Connection = connection1
                        command1.CommandText = sql
                        command1.Parameters.Add("@Value1", SqlDbType.VarChar).Value = Session("UserId")
                        command1.Parameters.Add("@Value2", SqlDbType.Char).Value = i.Value
                        command1.Parameters.Add("@Value3", SqlDbType.VarChar).Value = Session("Company")
                        retvalue = command1.ExecuteNonQuery()
                    End If
                Next
            End Using
            ltUserRefineries.Text = "User refineries access has been updated successfully!"
        End If
    End Sub
End Class

Friend Class ClassName
    Public Property Col1 As String
End Class
