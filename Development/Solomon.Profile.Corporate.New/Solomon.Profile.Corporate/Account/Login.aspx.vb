﻿Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security
Imports Owin

Partial Public Class Login
    Inherits Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Dim domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)
        'Dim path = HttpContext.Current.Request.Url.AbsolutePath
        'Dim port = HttpContext.Current.Request.Url.Port
        'Dim app_path = HttpContext.Current.Request.ApplicationPath
        'Dim path_and_query = HttpContext.Current.Request.Url.PathAndQuery


        'Dim currentRequest As HttpRequest = HttpContext.Current.Request
        'Dim ipAddress As String = currentRequest.ServerVariables("HTTP_X_FORWARDED_FOR")
        'If ipAddress Is Nothing OrElse ipAddress.ToLower() = "unknown" Then ipAddress = currentRequest.ServerVariables("REMOTE_ADDR")

        'Dim MyIP = IPAddress.Parse(((IPEndPoint).RemoteEndPoint).Address.ToString())
        'Dim gethost As IPHostEntry = Dns.GetHostEntry(ipAddress)
        'Response.Write(gethost.HostName)

        Dim domainName1 = NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName
        'Dim domainName2 = Environment.UserDomainName

        'Using _testData As StreamWriter = New StreamWriter(Server.MapPath("~/data.txt"), True)
        '    _testData.WriteLine("Your domain Name is:" + gethost.HostName)
        '    _testData.WriteLine("")
        '    '_testData.WriteLine("Your ip is:" + HttpContext.Current.Request.UserHostAddress)
        '    '_testData.WriteLine("")
        '    '_testData.WriteLine("Your host is:" + HttpContext.Current.Request.Url.Host)
        '    '_testData.WriteLine("")
        '    '_testData.WriteLine("Your authority is:" + HttpContext.Current.Request.Url.Authority)
        '    '_testData.WriteLine("")
        '    _testData.WriteLine("Your host1 is:" + domainName1)
        '    _testData.WriteLine("")
        '    _testData.WriteLine("Your host2 is:" + domainName2)
        '    _testData.WriteLine("")
        'End Using

        DomainName.Text = domainName1
        ' ForgotPasswordHyperLink.NavigateUrl = "Forgot"

        'Dim returnUrl = HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
        'If Not [String].IsNullOrEmpty(returnUrl) Then
        '    RegisterHyperLink.NavigateUrl += "?ReturnUrl=" & returnUrl
        'End If
    End Sub

    Protected Sub LogIn(sender As Object, e As EventArgs)
        If IsValid Then
            ' Validate the user password
            Dim manager = Context.GetOwinContext().GetUserManager(Of ApplicationUserManager)()
            Dim signinManager = Context.GetOwinContext().GetUserManager(Of ApplicationSignInManager)()
            Dim txtUName As String = Request.Form("login-username")
            Dim txtPWord As String = Request.Form("login-password")

            ' This doen't count login failures towards account lockout
            ' To enable password failures to trigger lockout, change to shouldLockout := True
            Dim result = signinManager.PasswordSignIn(txtUName, txtPWord, RememberMe.Checked, shouldLockout:=False)


            If DomainName.Text = "dc1.solomononline.com" Then
                Session("Company") = "DELEK"
            End If

            Select Case result
                Case SignInStatus.Success
                    ' IdentityHelper.RedirectToReturnUrl(Request.QueryString("ReturnUrl"), Response)
                    Response.Redirect("~/Main.aspx")
                    Exit Select
                Case SignInStatus.LockedOut
                    Response.Redirect("/Account/Lockout")
                    Exit Select
                Case SignInStatus.RequiresVerification
                    Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", Request.QueryString("ReturnUrl"), RememberMe.Checked), True)
                    Exit Select
                Case Else
                    FailureText.Text = "Invalid login attempt"
                    ErrorMessage.Visible = True
                    Exit Select
            End Select
        End If
    End Sub
End Class
