Imports System.Text
Imports System
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Configuration

Public Class ResponseFilter
    Inherits Stream
#Region "properties"

    Dim responseStream As Stream
    Dim html = New StringBuilder

#End Region

#Region "constructor"

    Public Sub New(ByVal inputStream As Stream)
        responseStream = inputStream
    End Sub

#End Region


#Region "implemented abstract members "

    Public Overrides ReadOnly Property CanRead() As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides ReadOnly Property CanSeek() As Boolean
        Get
            Return True
        End Get
    End Property
 
    Public Overrides ReadOnly Property CanWrite() As Boolean
        Get
            Return True
        End Get
    End Property
 

    Public Overrides Sub Close()
        responseStream.Close()
    End Sub

    Public Overrides Sub Flush()
        responseStream.Flush()
    End Sub

    Public Overrides ReadOnly Property Length() As Long
        Get
            Return 0
        End Get
    End Property

    Public Overrides Property Position() As Long
        Get
            Return position
        End Get
        Set(ByVal Value As Long)
            position = value
        End Set
    End Property

    Public Overrides Function Seek(ByVal offset As Long, ByVal direction As System.IO.SeekOrigin) As Long
        Return responseStream.Seek(offset, direction)
    End Function

    Public Overrides Sub SetLength(ByVal length As Long)
        responseStream.SetLength(length)
    End Sub

    Public Overrides Function Read(ByVal buffer As Byte(), ByVal offset As Integer, ByVal count As Integer) As Integer
        Return responseStream.Read(buffer, offset, count)
    End Function

#End Region

#Region "write method"

    Public Overrides Sub Write(ByVal buffer As Byte(), ByVal offset As Integer, ByVal count As Integer)

        ' string version of the buffer
        Dim sBuffer As String = System.Text.UTF8Encoding.UTF8.GetString(buffer, offset, count)

        ' end of the HTML file
        Dim oEndFile As New Regex("</html>", RegexOptions.IgnoreCase)
        Dim oEndExcel As New Regex("</Workbook>", RegexOptions.IgnoreCase) 'Check for Excel
        If (oEndFile.IsMatch(sBuffer)) Then

            ' Append the last buffer of data
            html.Append(sBuffer)

            Dim tempResponse As String = html.ToString()

            Dim newBuffer As String = Regex.Replace(tempResponse, "c1chartimage.aspx", ConfigurationManager.AppSettings("c1chartimageUrl"))
            'Dim newBuffer As String = Regex.Replace(tempResponse, "c1chartimage.aspx", "http://localhost/ProfileII-Corporate/c1chartimage.aspx")


            tempResponse = newBuffer

            Dim data As Byte() = System.Text.UTF8Encoding.UTF8.GetBytes(tempResponse)

            responseStream.Write(data, 0, data.Length)
        ElseIf (oEndExcel.IsMatch(sBuffer)) Then
            ' Append the last buffer of data
            html.Append(sBuffer)
            Dim data As Byte() = System.Text.UTF8Encoding.UTF8.GetBytes(html.ToString)
            responseStream.Write(data, 0, data.Length)
        Else
            html.Append(sBuffer)
        End If

    End Sub

#End Region


End Class
