﻿Public Class SettingsControl
    Inherits System.Web.UI.UserControl
    Public Shared IsAdminUser As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub navigateSettings(sender As Object, e As EventArgs)
        If (Not sender.InnerText Is Nothing) Then
            If (sender.InnerText = "Create User") Then
                Response.Redirect("/Account/Register.aspx")
                Return
            End If
            If (sender.InnerText = "Manage Users") Then
                Response.Redirect("/Account/ManageUsers.aspx")
                Return
            End If
            If (sender.InnerText = "Change Password") Then
                Response.Redirect("/Account/ManagePassword.aspx")
                Return
            End If
        End If
    End Sub

End Class