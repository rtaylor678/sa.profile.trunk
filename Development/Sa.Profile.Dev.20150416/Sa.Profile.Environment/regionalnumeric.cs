﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.ComponentModel;

namespace ProfileEnvironment
{
    /**
    * <summary>
    * This class provides regional numeric information
    * based on the current user 
    * </summary>
    * <remarks>
    * </remarks>
    * <returns>
    * An object of values representing the regional numeric properties
    * </returns>
    */
    [Serializable]
    public class RegionalNumeric
    {

        public RegionalNumeric()
        {
        }

        #region Public Properties

        [FieldNullable(IsNullable = false)]
        public string UserId { get; set; }


        [FieldNullable(IsNullable = false)]
        public string DecimalSymbol
        {
            get
            {
                return GetDecimalSymbol();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string NumberOfDigitsAfterDecimal
        {
            get
            {
                return GetNumberOfDigitsAfterDecimal();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string DigitGroupingSymbol
        {
            get
            {
                return GetDigitGroupingSymbol();
            }
        }

        //[FieldNullable(IsNullable = false)]
        //public string DigitGrouping
        //{
        //    get
        //    {
        //        return GetDigitGrouping();
        //    }
        //}

        [FieldNullable(IsNullable = false)]
        public string NegativeSignSymbol
        {
            get
            {
                return GetNegativeSignSymbol();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string NegativeNumberFormat
        {
            get
            {
                return GetNegativeNumberFormat();
            }
        }

        //[FieldNullable(IsNullable = false)]
        //public string DisplayLeadingZeros
        //{
        //    get
        //    {
        //        return GetDisplayLeadingZeros();
        //    }
        //}

        //[FieldNullable(IsNullable = false)]
        //public string ListSeparator
        //{
        //    get
        //    {
        //        return GetListSeparator();
        //    }
        //}

        //[FieldNullable(IsNullable = false)]
        //public string MeasurementSystem
        //{
        //    get
        //    {
        //        return GetMeasurementSystem();
        //    }
        //}

        //TODO:
        //public string StandardDigits { get; set; }
        //public string UseNativeDigits { get; set; }

        #endregion Public Properties

        #region Public Methods

        public string StringToFloatFormat(string value)
        {
            string retVal;

            //Get the numeric DecimalSymbol
            string decimalSymbol = DecimalSymbol;
            string groupSymbol = DigitGroupingSymbol;

            string groupSymbolResult = value.Replace(groupSymbol, "");
            string decimalSymbolResult = groupSymbolResult.Replace(decimalSymbol, ".");

            try
            {
                retVal = decimalSymbolResult;
                return retVal;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion Public Methods

        #region Private Methods

        private string GetDecimalSymbol()
        {
            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator.ToString();

            return retVal;
        }

        private string GetNumberOfDigitsAfterDecimal()
        {

            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalDigits.ToString();

            return retVal;
        }

        private string GetDigitGroupingSymbol()
        {

            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator.ToString();

            return retVal;
        }

        //private string GetDigitGrouping()
        //{

        //    string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSizes.ToString();

        //    return retVal;
        //}

        private string GetNegativeSignSymbol()
        {

            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.NegativeSign.ToString();

            return retVal;
        }

        private string GetNegativeNumberFormat()
        {

            int index = Convert.ToInt32(System.Globalization.NumberFormatInfo.CurrentInfo.NumberNegativePattern.ToString());
            string retVal = string.Empty;

            // Create a read-write NumberFormatInfo object for the current culture.
            NumberFormatInfo numberInfo = NumberFormatInfo.CurrentInfo.Clone() as NumberFormatInfo;
            decimal value = -1.1m;

            // Assign each possible value to the NegativePattern property.
            int rowIdx = 0;
            for (int ctr = 0; ctr <= 4; ctr++)
            {
                numberInfo.NumberNegativePattern = ctr;

                if (rowIdx == index)
                {

                    retVal += String.Format("{1}", ctr, value.ToString("N", numberInfo));
                    break;
                }

                rowIdx++;
            }

            return retVal;
        }

        //private string GetDisplayLeadingZeros()
        //{
        //    //TO DO:
        //    string retVal = System.Globalization.NumberFormatInfo.CurrentInfo...ToString();

        //    return retVal;
        //}

        //private string GetListSeparator()
        //{
        //    //TO DO:
        //    string retVal = System.Globalization.NumberFormatInfo.CurrentInfo..ToString();

        //    return retVal;
        //}

        //private string GetMeasurementSystem()
        //{
        //     //TO DO:
        //    string retVal = System.Globalization.CultureInfo.CurrentCulture..ToString();

        //    return retVal;
        //}

        #endregion Private Methods
    }


}
