﻿CREATE ROLE [Developer]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'Saroy';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'DMR';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'JLS';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'JBF';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'MGV';

