﻿CREATE ROLE [Consultants]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'MRH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'EJB';

