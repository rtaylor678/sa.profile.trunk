﻿
CREATE PROC [dbo].[spReportUnitAvailCalc](@RefineryID varchar(6), @PeriodYear smallint, @PeriodMonth smallint, @Dataset varchar(8) = 'ACTUAL', @FactorSet varchar(8) = '2012', @NumMonths tinyint)
AS
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime
SELECT @PeriodEnd = PeriodEnd FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @Dataset)
SELECT @PeriodStart = DATEADD(mm, -@NumMonths, @PeriodEnd)

;WITH downtime AS (
SELECT mr.UnitID, GlobalDB.dbo.WtAvg(fc.EDCNoMult, mr.PeriodHrs)/1000 AS AvgEDC, SUM(ISNULL(RegDown,0)) AS RegDown, SUM(ISNULL(MaintDown,0)) AS MaintDown, SUM(ISNULL(OthDown,0)) AS OthDown, SUM(ISNULL(RegSlow,0)+ISNULL(MaintSlow,0)+ISNULL(OthSlow,0)) AS Slowdowns, SUM(mr.PeriodHrs) AS PeriodHrs
	, MAX(s.PeriodStart) AS LastPeriodStart
	, MechUnavailTA = dbo.GetMechUnavailTA(@RefineryID, @Dataset, MIN(s.PeriodStart), MAX(s.PeriodEnd), mr.UnitID)
	, LastSubmission = (SELECT SubmissionID FROM dbo.GetSubmission(@RefineryID, DATEPART(yyyy, MAX(s.PeriodStart)), DATEPART(mm,MAX(s.PeriodStart)), @Dataset))
FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd) s
INNER JOIN MaintRout mr ON mr.SubmissionID = s.SubmissionID 
INNER JOIN FactorCalc fc ON fc.SubmissionID = s.SubmissionID AND fc.UnitID = mr.UnitID AND fc.FactorSet = @FactorSet
INNER JOIN ProcessID_LU lu ON lu.ProcessID = mr.ProcessID AND lu.ProcessGroup IN ('P','F','L','W','O')
GROUP BY mr.UnitID
)
SELECT c.UnitName, c.ProcessID, d.AvgEDC, d.RegDown, d.MaintDown, d.OthDown, d.Slowdowns, d.PeriodHrs, d.MechUnavailTA
	, MechAvail = 100 - MaintDown/PeriodHrs*100 - MechUnavailTA
	, OpAvail = 100 - (MaintDown+RegDown)/PeriodHrs*100 - MechUnavailTA
	, OnStream = 100 - (MaintDown+RegDown+OthDown)/PeriodHrs*100 - MechUnavailTA
	, OnStreamSlow = 100 - (MaintDown+RegDown+OthDown+Slowdowns)/PeriodHrs*100 - MechUnavailTA
FROM downtime d LEFT JOIN Config c ON c.SubmissionID = d.LastSubmission AND c.UnitID = d.UnitID
INNER JOIN ProcessID_LU pid ON pid.ProcessID = c.ProcessID
ORDER BY pid.SortKey, c.UnitID

