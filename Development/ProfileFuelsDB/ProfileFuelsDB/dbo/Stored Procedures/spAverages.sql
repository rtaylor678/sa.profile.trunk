﻿




CREATE               PROC [dbo].[spAverages] (@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @RefineryID varchar(6), @DataSet varchar(15)
DECLARE @Start12Mo smalldatetime, @StartYTD smalldatetime
SELECT @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @RefineryID = RefineryID, @DataSet = DataSet
FROM Submissions WHERE SubmissionID = @SubmissionID
SELECT @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1)
IF @RefineryID IN ('106FL','150FL','322EUR') AND DATEPART(yy, @Start12Mo) < 2011 AND DATEPART(yy, @PeriodStart) >= 2011
		SET @Start12Mo = '12/31/2010'

EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Started'
EXEC spAverageAvail @SubmissionID
EXEC spAverageMaint @SubmissionID
DECLARE @CrudeAPI_Avg real, @CrudeAPI_YTD real, @CrudeSulfur_Avg real, @CrudeSulfur_YTD real
DECLARE @GainPcnt_Avg real, @GainPcnt_YTD real, @NetInputBPD_Avg real, @NetInputBPD_YTD real
DECLARE @MPSAbsPcnt_Avg real, @MPSAbsPcnt_YTD real, @MPSOvtPcnt_Avg real, @MPSOvtPcnt_YTD real
DECLARE @OCCAbsPcnt_Avg real, @OCCAbsPcnt_YTD real, @OCCOvtPcnt_Avg real, @OCCOvtPcnt_YTD real
DECLARE @MaintOCCMPSRatio_Avg real, @MaintOCCMPSRatio_YTD real, @ProcOCCMPSRatio_Avg real, @ProcOCCMPSRatio_YTD real
-- Average Crude Gravity and Sulfur
SELECT @CrudeAPI_Avg = Gravity, @CrudeSulfur_Avg = Sulfur FROM dbo.CalcAverageCrude(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
SELECT @CrudeAPI_YTD = Gravity, @CrudeSulfur_YTD = Sulfur FROM dbo.CalcAverageCrude(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)

-- Average Net Input, kBPD and Gain, %
SELECT @NetInputBPD_Avg = NetInputkBPD, @GainPcnt_Avg = GainPcnt FROM dbo.CalcAverageYield(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
SELECT @NetInputBPD_YTD = NetInputkBPD, @GainPcnt_YTD = GainPcnt FROM dbo.CalcAverageYield(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)

-- Average Personnel percentages and ratios
SELECT @OCCAbsPcnt_Avg = OCCAbsPcnt, @MPSAbsPcnt_Avg = MPSAbsPcnt, @OCCOvtPcnt_Avg = OCCOVTPcnt, @MPSOvtPcnt_Avg = MPSOVTPcnt, @ProcOCCMPSRatio_Avg = PROCOCCMPSRatio, @MaintOCCMPSRatio_Avg = MaintOCCMPSRatio
FROM dbo.CalcAveragePers(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
SELECT @OCCAbsPcnt_YTD = OCCAbsPcnt, @MPSAbsPcnt_YTD = MPSAbsPcnt, @OCCOvtPcnt_YTD = OCCOVTPcnt, @MPSOvtPcnt_YTD = MPSOVTPcnt, @ProcOCCMPSRatio_YTD = PROCOCCMPSRatio, @MaintOCCMPSRatio_YTD = MaintOCCMPSRatio
FROM dbo.CalcAveragePers(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)

-- Energy Consumption per Bbl Input
DECLARE @KBTUPerBbl_Avg real, @MJPerBbl_Avg real, @KBTUPerBbl_YTD real, @MJPerBbl_YTD real
SELECT @KBTUPerBbl_Avg = dbo.GetAvgEnergyConsKBTUPerBbl(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd),
	@KBTUPerBbl_YTD = dbo.GetAvgEnergyConsKBTUPerBbl(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)
SELECT @MJPerBbl_Avg = GlobalDB.dbo.UnitsConv(@KBTUPerBbl_Avg, 'KBTU/B','MJ/B'), @MJPerBbl_YTD = GlobalDB.dbo.UnitsConv(@KBTUPerBbl_YTD, 'KBTU/B','MJ/B')

-- Update Gensum with these simple averages that are independent of UOM, Currency and FactorSet
UPDATE Gensum
SET CrudeAPI_Avg = @CrudeAPI_Avg, CrudeAPI_YTD = @CrudeAPI_YTD, CrudeSulfur_Avg = @CrudeSulfur_Avg, CrudeSulfur_YTD = @CrudeSulfur_YTD,
GainPcnt_Avg = @GainPcnt_Avg, GainPcnt_YTD = @GainPcnt_YTD, NetInputBPD_Avg = @NetInputBPD_Avg, NetInputBPD_YTD = @NetInputBPD_YTD,
MPSAbsPcnt_Avg = @MPSAbsPcnt_Avg, MPSAbsPcnt_YTD = @MPSAbsPcnt_YTD, MPSOvtPcnt_Avg = @MPSOvtPcnt_Avg, MPSOvtPcnt_YTD = @MPSOvtPcnt_YTD,
OCCAbsPcnt_Avg = @OCCAbsPcnt_Avg, OCCAbsPcnt_YTD = @OCCAbsPcnt_YTD, OCCOvtPcnt_Avg = @OCCOvtPcnt_Avg, OCCOvtPcnt_YTD = @OCCOvtPcnt_YTD,
MaintOCCMPSRatio_Avg = @MaintOCCMPSRatio_Avg, ProcOCCMPSRatio_Avg = @ProcOCCMPSRatio_Avg, 
MaintOCCMPSRatio_YTD = @MaintOCCMPSRatio_YTD, ProcOCCMPSRatio_YTD = @ProcOCCMPSRatio_YTD,
EnergyConsPerBbl_Avg = CASE UOM WHEN 'US' THEN @KBTUPerBbl_Avg WHEN 'MET' THEN @MJPerBbl_Avg END, 
EnergyConsPerBbl_YTD = CASE UOM WHEN 'US' THEN @KBTUPerBbl_YTD WHEN 'MET' THEN @MJPerBbl_YTD END
WHERE SubmissionID = @SubmissionID
-- Averages for variables that are a function of FactorSet only
DECLARE @FactorSet FactorSet
DECLARE @EDC_Avg float, @UEDC_Avg float, @EII_Avg real, @VEI_Avg real, @UtilPcnt_Avg real, @UtilOSTA_Avg real, @ProcessUtilPcnt_Avg real
DECLARE @EDC_YTD float, @UEDC_YTD float, @EII_YTD real, @VEI_YTD real, @UtilPcnt_YTD real, @UtilOSTA_YTD real, @ProcessUtilPcnt_YTD real
DECLARE @OCCWHrEDC_Avg real, @MPSWhrEDC_Avg real, @TotWHrEDC_Avg real, @TotMaintForceWHrEDC_Avg real, @PEI_Avg real, @MaintPEI_Avg real, @NonMaintPEI_Avg real
DECLARE @OCCWHrEDC_YTD real, @MPSWhrEDC_YTD real, @TotWHrEDC_YTD real, @TotMaintForceWHrEDC_YTD real, @PEI_YTD real, @MaintPEI_YTD real, @NonMaintPEI_YTD real
DECLARE @OCCEqPEDC_Avg real, @MPSEqPEDC_Avg real, @TotEqPEDC_Avg real
DECLARE @OCCEqPEDC_YTD real, @MPSEqPEDC_YTD real, @TotEqPEDC_YTD real
DECLARE cFactorSets CURSOR FAST_FORWARD FOR
	SELECT DISTINCT FactorSet FROM Gensum WHERE SubmissionID = @SubmissionID
OPEN cFactorSets
FETCH NEXT FROM cFactorSets INTO @FactorSet
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @EII_Avg = EII, @VEI_Avg = VEI, @UtilPcnt_Avg = UtilPcnt, @UtilOSTA_Avg = UtilOSTA, @EDC_Avg = AvgEDC, @UEDC_Avg = AvgUEDC, @ProcessUtilPcnt_Avg = ProcessUtilPcnt
	FROM dbo.CalcAverageFactors(@RefineryID, @Dataset, @FactorSet, @Start12Mo, @PeriodEnd)

	SELECT @EII_YTD = EII, @VEI_YTD = VEI, @UtilPcnt_YTD = UtilPcnt, @UtilOSTA_YTD = UtilOSTA, @EDC_YTD = AvgEDC, @UEDC_YTD = AvgUEDC, @ProcessUtilPcnt_YTD = ProcessUtilPcnt
	FROM dbo.CalcAverageFactors(@RefineryID, @Dataset, @FactorSet, @StartYTD, @PeriodEnd)

	SELECT @OCCWHrEDC_Avg = OCCWHrEDC, @MPSWhrEDC_Avg = MPSWHrEDC, @TotWHrEDC_Avg = TotWHrEDC, @TotMaintForceWHrEDC_Avg = TotMaintForceWHrEDC
		, @PEI_Avg = PEI, @MaintPEI_Avg = MaintPEI, @NonMaintPEI_Avg = NonMaintPEI
		, @OCCEqPEDC_Avg = OCCEqPEDC, @MPSEqPEDC_Avg = MPSEqPEDC, @TotEqPEDC_Avg = TotEqPEDC
	FROM dbo.CalcAveragePersKPIs(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd, @FactorSet)

	SELECT @OCCWHrEDC_YTD = OCCWHrEDC, @MPSWhrEDC_YTD = MPSWHrEDC, @TotWHrEDC_YTD = TotWHrEDC, @TotMaintForceWHrEDC_YTD = TotMaintForceWHrEDC
		, @PEI_YTD = PEI, @MaintPEI_YTD = MaintPEI, @NonMaintPEI_YTD = NonMaintPEI
		, @OCCEqPEDC_YTD = OCCEqPEDC, @MPSEqPEDC_YTD = MPSEqPEDC, @TotEqPEDC_YTD = TotEqPEDC
	FROM dbo.CalcAveragePersKPIs(@RefineryID, @Dataset, @StartYTD, @PeriodEnd, @FactorSet)
	
	UPDATE Gensum 
	SET  EDC_Avg = @EDC_Avg, UEDC_Avg = @UEDC_Avg, EII_Avg = @EII_Avg, VEI_Avg = @VEI_Avg, UtilPcnt_Avg = @UtilPcnt_Avg, UtilOSTA_Avg = @UtilOSTA_Avg, ProcessUtilPcnt_Avg = @ProcessUtilPcnt_Avg,
	     EDC_YTD = @EDC_YTD, UEDC_YTD = @UEDC_YTD, EII_YTD = @EII_YTD, VEI_YTD = @VEI_YTD, UtilPcnt_YTD = @UtilPcnt_YTD, UtilOSTA_YTD = @UtilOSTA_YTD, ProcessUtilPcnt_YTD = @ProcessUtilPcnt_YTD,
	     OCCWHrEDC_Avg = @OCCWHrEDC_Avg, MPSWhrEDC_Avg = @MPSWhrEDC_Avg, TotWhrEDC_Avg = @TotWHrEDC_Avg,
	     OCCWHrEDC_YTD = @OCCWHrEDC_YTD, MPSWhrEDC_YTD = @MPSWhrEDC_YTD, TotWhrEDC_YTD = @TotWHrEDC_YTD,
	     OCCEqPEDC_Avg = @OCCEqPEDC_Avg, MPSEqPEDC_Avg = @MPSEqPEDC_Avg, TotEqPEDC_Avg = @TotEqPEDC_Avg,
	     OCCEqPEDC_YTD = @OCCEqPEDC_YTD, MPSEqPEDC_YTD = @MPSEqPEDC_YTD, TotEqPEDC_YTD = @TotEqPEDC_YTD,
	     TotMaintForceWHrEDC_Avg = @TotMaintForceWHrEDC_Avg, TotMaintForceWHrEDC_YTD = @TotMaintForceWHrEDC_YTD,
	     PEI_Avg = @PEI_Avg, MaintPEI_Avg = @MaintPEI_Avg, NonMaintPEI_Avg = @NonMaintPEI_Avg, 
	     PEI_YTD = @PEI_YTD, MaintPEI_YTD = @MaintPEI_YTD, NonMaintPEI_YTD = @NonMaintPEI_YTD
	WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet
	
	FETCH NEXT FROM cFactorSets INTO @FactorSet
END
CLOSE cFactorSets
DEALLOCATE cFactorSets

-- Average CEI
UPDATE CEI2008
SET CEI_Avg = dbo.GetAverageCEI(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd, FactorSet)
, CEI_YTD = dbo.GetAverageCEI(@RefineryID, @Dataset, @StartYTD, @PeriodEnd, FactorSet)
WHERE SubmissionID = @SubmissionID

-- Average Energy Costs
UPDATE Gensum
SET EnergyCost_Avg = CASE UOM WHEN 'US' THEN a.TotCostMBTU WHEN 'MET' THEN a.TotCostGJ END
	, EnergyCost_Prod_Avg = CASE UOM WHEN 'US' THEN a.ProdCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
	, EnergyCost_Pur_Avg = CASE UOM WHEN 'US' THEN a.PurCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
FROM GenSum CROSS APPLY dbo.CalcAverageEnergyCost(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd) a 
WHERE GenSum.SubmissionID = @SubmissionID AND a.Currency = Gensum.Currency

UPDATE Gensum
SET EnergyCost_YTD = CASE UOM WHEN 'US' THEN a.TotCostMBTU WHEN 'MET' THEN a.TotCostGJ END
	, EnergyCost_Prod_YTD = CASE UOM WHEN 'US' THEN a.ProdCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
	, EnergyCost_Pur_YTD = CASE UOM WHEN 'US' THEN a.PurCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
FROM GenSum CROSS APPLY dbo.CalcAverageEnergyCost(@RefineryID, @DataSet, @StartYTD, @PeriodEnd) a 
WHERE GenSum.SubmissionID = @SubmissionID AND a.Currency = Gensum.Currency

-- Average Margins
UPDATE Gensum
SET GPV_Avg = a.GPV, RMC_Avg = a.RMC, GrossMargin_Avg = a.GrossMargin, TotCashOpexBbl_Avg = a.CashOpex, CashMargin_Avg = a.CashMargin
FROM GenSum CROSS APPLY dbo.CalcAverageMargins(RefineryID, DataSet, @Start12Mo, @PeriodEnd) a
WHERE Gensum.SubmissionID = @SubmissionID AND Gensum.Scenario = a.Scenario AND Gensum.Currency = a.Currency

UPDATE Gensum
SET GPV_YTD = a.GPV, RMC_YTD = a.RMC, GrossMargin_YTD = a.GrossMargin, TotCashOpexBbl_YTD = a.CashOpex, CashMargin_YTD = a.CashMargin
FROM GenSum CROSS APPLY dbo.CalcAverageMargins(RefineryID, DataSet, @StartYTD, @PeriodEnd) a
WHERE Gensum.SubmissionID = @SubmissionID AND Gensum.Scenario = a.Scenario AND Gensum.Currency = a.Currency

/*DECLARE @Currency CurrencyCode, @Scenario Scenario

DECLARE @ROI_Avg real, @ROI_YTD real, @VAI_Avg real, @VAI_YTD real, 
	@NonVolOpexUEDC_Avg real, @NonVolOpexUEDC_YTD real,
	@VolOpexUEDC_Avg real, @VolOpexUEDC_YTD real, @TotCashOpexUEDC_Avg real, @TotCashOpexUEDC_YTD real,
	@NEOpexEDC_Avg real, @NEOpexEDC_YTD real, @NEOpexUEDC_Avg real, @NEOpexUEDC_YTD real,
	@NEI_Avg real, @NEI_YTD real, @RV_Avg real, @RV_YTD real, @TotCptl_Avg real, @TotCptl_YTD real
DECLARE cOpex CURSOR FAST_FORWARD FOR
	SELECT DISTINCT FactorSet, Currency, Scenario
	FROM Gensum WHERE SubmissionID = @SubmissionID
OPEN cOpex
FETCH NEXT FROM cOpex INTO @FactorSet, @Currency, @Scenario
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC spAverageOpex @RefineryID, @DataSet, @Start12Mo, @PeriodEnd, @FactorSet, @Currency, @Scenario,
		@TotCashOpexUEDC = @TotCashOpexUEDC_Avg OUTPUT, 
		@VolOpexUEDC = @VolOpexUEDC_Avg OUTPUT, @NonVolOpexUEDC = @NonVolOpexUEDC_Avg OUTPUT, 
		@NEOpexUEDC = @NEOpexUEDC_Avg OUTPUT, @NEOpexEDC = @NEOpexEDC_Avg OUTPUT,
		@ROI = @ROI_Avg OUTPUT, @VAI = @VAI_Avg OUTPUT, @NEI = @NEI_Avg OUTPUT, 
		@RV = @RV_Avg OUTPUT, @TotCptl = @TotCptl_Avg OUTPUT
	EXEC spAverageOpex @RefineryID, @DataSet, @StartYTD, @PeriodEnd, @FactorSet, @Currency, @Scenario,
		@TotCashOpexUEDC = @TotCashOpexUEDC_YTD OUTPUT, 
		@VolOpexUEDC = @VolOpexUEDC_YTD OUTPUT, @NonVolOpexUEDC = @NonVolOpexUEDC_YTD OUTPUT, 
		@NEOpexUEDC = @NEOpexUEDC_YTD OUTPUT, @NEOpexEDC = @NEOpexEDC_YTD OUTPUT, 
		@ROI = @ROI_YTD OUTPUT, @VAI = @VAI_YTD OUTPUT, @NEI = @NEI_YTD OUTPUT,
		@RV = @RV_YTD OUTPUT, @TotCptl = @TotCptl_YTD OUTPUT
	UPDATE Gensum
	SET TotCashOpexUEDC_Avg = @TotCashOpexUEDC_Avg, 
	    VolOpexUEDC_Avg = @VolOpexUEDC_Avg, NonVolOpexUEDC_Avg = @NonVolOpexUEDC_Avg, 
	    NEOpexUEDC_Avg = @NEOpexUEDC_Avg, NEOpexEDC_Avg = @NEOpexEDC_Avg, ROI_Avg = @ROI_Avg,
	    TotCashOpexUEDC_YTD = @TotCashOpexUEDC_YTD, 
	    VolOpexUEDC_YTD = @VolOpexUEDC_YTD, NonVolOpexUEDC_YTD = @NonVolOpexUEDC_YTD, 
	    NEOpexUEDC_YTD = @NEOpexUEDC_YTD, NEOpexEDC_YTD = @NEOpexEDC_YTD, ROI_YTD = @ROI_YTD,
	    VAI_Avg = @VAI_Avg, VAI_YTD = @VAI_YTD, NEI_Avg = @NEI_Avg, NEI_YTD = @NEI_YTD,
	    RV_Avg = @RV_Avg, RV_YTD = @RV_YTD, TotCptl_Avg = @TotCptl_Avg, TotCptl_YTD = @TotCptl_YTD
	WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet
	AND Currency = @Currency AND Scenario = @Scenario
	FETCH NEXT FROM cOpex INTO @FactorSet, @Currency, @Scenario
END
CLOSE cOpex
DEALLOCATE cOpex*/

UPDATE Gensum
SET TotCashOpexUEDC_Avg = o.TotCashOpexUEDC, VolOpexUEDC_Avg = o.VolOpexUEDC, NonVolOpexUEDC_Avg = o.NonVolOpexUEDC, 
    NEOpexUEDC_Avg = o.NEOpexUEDC, NEOpexEDC_Avg = o.NEOpexEDC, NEI_Avg = o.NEI
FROM GenSum CROSS APPLY [dbo].[CalcAverageOpex](@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, NULL, 'CLIENT', NULL) o
WHERE Gensum.SubmissionID = @SubmissionID AND o.FactorSet = Gensum.FactorSet AND o.Currency = Gensum.Currency

UPDATE Gensum
SET TotCashOpexUEDC_YTD = o.TotCashOpexUEDC, VolOpexUEDC_YTD = o.VolOpexUEDC, NonVolOpexUEDC_YTD = o.NonVolOpexUEDC, 
    NEOpexUEDC_YTD = o.NEOpexUEDC, NEOpexEDC_YTD = o.NEOpexEDC, NEI_YTD = o.NEI
FROM GenSum CROSS APPLY [dbo].[CalcAverageOpex](@RefineryID, @DataSet, @StartYTD, @PeriodEnd, NULL, 'CLIENT', NULL) o
WHERE Gensum.SubmissionID = @SubmissionID AND o.FactorSet = Gensum.FactorSet AND o.Currency = Gensum.Currency

UPDATE Gensum
SET ROI_Avg = r.ROI, VAI_Avg = r.VAI, RV_Avg = r.RV, TotCptl_Avg = r.TotCptl
FROM GenSum CROSS APPLY [dbo].[CalcAverageROI](@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, NULL, NULL, NULL) r
WHERE Gensum.SubmissionID = @SubmissionID AND r.FactorSet = Gensum.FactorSet AND r.Currency = Gensum.Currency AND r.Scenario = Gensum.Scenario

UPDATE Gensum
SET ROI_YTD = r.ROI, VAI_YTD = r.VAI, RV_YTD = r.RV, TotCptl_YTD = r.TotCptl
FROM GenSum CROSS APPLY [dbo].[CalcAverageROI](@RefineryID, @DataSet, @StartYTD, @PeriodEnd, NULL, NULL, NULL) r
WHERE Gensum.SubmissionID = @SubmissionID AND r.FactorSet = Gensum.FactorSet AND r.Currency = Gensum.Currency AND r.Scenario = Gensum.Scenario

UPDATE Submissions
SET CalcsNeeded = NULL
WHERE SubmissionID = @SubmissionID 
AND CalcsNeeded IN ('A','M')
EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Completed'









