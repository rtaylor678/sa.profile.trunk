﻿
CREATE             PROC [dbo].[spFactorTotCalc] (@SubmissionID int)
AS

SET NOCOUNT ON
DELETE FROM FactorProcessCalc WHERE SubmissionID = @SubmissionID
SELECT f.FactorSet, c.ProcessID, f.MultGroup, c.Cap, f.EDCNoMult
INTO #UnitsForMult
FROM FactorCalc f INNER JOIN Config c ON c.SubmissionID = f.SubmissionID AND c.UnitID = f.UnitID
WHERE f.SubmissionID = @SubmissionID AND MultGroup <> '' AND c.Cap > 0 AND c.InservicePcnt > 50
SELECT FactorSet, ProcessID, MultGroup, COUNT(*) AS NumUnits, SUM(Cap) AS Cap, SUM(EDCNoMult) AS EDCNoMult, MultiFactor = CAST(1 AS real)
INTO #MultGroups
FROM #UnitsForMult
GROUP BY FactorSet, ProcessID, MultGroup
DECLARE @FactorSet FactorSet, @ProcessID ProcessID, @MultGroup varchar(8), 
	@NumUnits smallint, @Cap real, @MultiFactor real
DECLARE cMultGroups CURSOR LOCAL FORWARD_ONLY FOR
SELECT FactorSet, ProcessID, MultGroup, NumUnits, Cap FROM #MultGroups
WHERE NumUnits > 1
FOR UPDATE OF MultiFactor
OPEN cMultGroups
FETCH NEXT FROM cMultGroups INTO @FactorSet, @ProcessID, @MultGroup, @NumUnits, @Cap
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @NumUnits = MAX(NumUnits) FROM MultFactors
	WHERE FactorSet = @FactorSet AND MultGroup = @MultGroup AND NumUnits <= @NumUnits

	SELECT @MultiFactor = 1

	SELECT @MultiFactor = 0.5*(SUM(m.MaintFactor*POWER(u.Cap, 1+m.MaintExp))/(m1.MaintFactor*POWER(@Cap, 1+m1.MaintExp))
		+ SUM(m.PersFactor*POWER(u.Cap, 1+m.PersExp))/(m1.PersFactor*POWER(@Cap, 1+m1.PersExp)))
	FROM MultFactors m INNER JOIN MultFactors m1 
		ON m.FactorSet = m1.FactorSet AND m.MultGroup = m1.MultGroup AND m1.NumUnits = 1
	INNER JOIN #UnitsForMult u ON u.FactorSet = m.FactorSet AND u.MultGroup = m.MultGroup
	WHERE m.FactorSet = @FactorSet AND m.MultGroup = @MultGroup AND m.NumUnits = @NumUnits
	GROUP BY m1.MaintFactor, m1.MaintExp, m1.PersFactor, m1.PersExp

	SELECT @MultiFactor = CASE WHEN @MultiFactor < MinMultiplicity THEN MinMultiplicity 
				WHEN @MultiFactor > MaxMultiplicity THEN MaxMultiplicity 
				ELSE @MultiFactor END
	FROM MultLimits
	WHERE FactorSet = @FactorSet AND MultGroup = @MultGroup

	UPDATE #MultGroups
	SET MultiFactor = @MultiFactor
	WHERE CURRENT OF cMultGroups

	FETCH NEXT FROM cMultGroups INTO @FactorSet, @ProcessID, @MultGroup, @NumUnits, @Cap
END
CLOSE cMultGroups
DEALLOCATE cMultGroups
UPDATE FactorCalc
SET MultiFactor = ISNULL((SELECT MultiFactor FROM #MultGroups m WHERE m.FactorSet = FactorCalc.FactorSet AND m.ProcessID = FactorCalc.ProcessID AND m.MultGroup = FactorCalc.MultGroup), 1)
WHERE SubmissionID = @SubmissionID
DROP TABLE #UnitsForMult
DROP TABLE #MultGroups
INSERT INTO FactorProcessCalc (SubmissionID, FactorSet, ProcessID, EDCNoMult, UEDCNoMult, MultiFactor,
RV, RVBbl, StdEnergy, StdGain, YearsOper, 
EDC, UEDC, NonProratedEDC, PlantEDC, NonProratedPlantEDC, NonProratedRV, PlantRV, NonProratedPlantRV,
NEOpexEffDiv, NonProratedNEOpexEffDiv, PlantNEOpexEffDiv, NonProratedPlantNEOpexEffDiv, 
MaintEffDiv, NonProratedMaintEffDiv, PlantMaintEffDiv, NonProratedPlantMaintEffDiv, 
PersEffDiv, NonProratedPersEffDiv, PlantPersEffDiv, NonProratedPlantPersEffDiv,
MaintPersEffDiv, NonProratedMaintPersEffDiv, PlantMaintPersEffDiv, NonProratedPlantMaintPersEffDiv,
NonMaintPersEffDiv, NonProratedNonMaintPersEffDiv, PlantNonMaintPersEffDiv, NonProratedPlantNonMaintPersEffDiv)
SELECT f.SubmissionID, f.FactorSet, c.ProcessID, SUM(f.EDCNoMult), SUM(f.UEDCNoMult), 
CASE WHEN SUM(f.EDCNoMult) = 0 THEN 1 ELSE SUM(f.EDCNoMult*f.MultiFactor)/SUM(f.EDCNoMult) END,
SUM(f.RV), CASE WHEN SUM(c.ActualCap) > 0 THEN SUM(f.RV*1000000)/SUM(c.ActualCap) END, 
SUM(f.StdEnergy), SUM(f.StdGain), CASE WHEN SUM(f.NonProratedPlantEDC) > 0 THEN SUM(c.YearsOper*f.NonProratedPlantEDC)/SUM(f.NonProratedPlantEDC) END, 
SUM(f.EDCNoMult*f.MultiFactor), SUM(f.UEDCNoMult*f.MultiFactor), 
SUM(f.NonProratedEDC*f.MultiFactor), SUM(f.PlantEDC*f.MultiFactor), SUM(f.NonProratedPlantEDC*f.MultiFactor), 
SUM(f.NonProratedRV), SUM(f.PlantRV), SUM(f.NonProratedPlantRV),
SUM(NEOpexEffDiv), SUM(NonProratedNEOpexEffDiv), SUM(PlantNEOpexEffDiv), SUM(NonProratedPlantNEOpexEffDiv), 
SUM(MaintEffDiv), SUM(NonProratedMaintEffDiv), SUM(PlantMaintEffDiv), SUM(NonProratedPlantMaintEffDiv), 
SUM(PersEffDiv), SUM(NonProratedPersEffDiv), SUM(PlantPersEffDiv), SUM(NonProratedPlantPersEffDiv),
SUM(MaintPersEffDiv), SUM(NonProratedMaintPersEffDiv), SUM(PlantMaintPersEffDiv), SUM(NonProratedPlantMaintPersEffDiv),
SUM(NonMaintPersEffDiv), SUM(NonProratedNonMaintPersEffDiv), SUM(PlantNonMaintPersEffDiv), SUM(NonProratedPlantNonMaintPersEffDiv)
FROM FactorCalc f INNER JOIN Config c ON c.SubmissionID = f.SubmissionID AND c.UnitID = f.UnitID
WHERE f.SubmissionID = @SubmissionID
GROUP BY f.SubmissionID, f.FactorSet, c.ProcessID

INSERT INTO FactorProcessCalc (SubmissionID, FactorSet, ProcessID, EDCNoMult, UEDCNoMult, MultiFactor,
RV, RVBbl, StdEnergy, StdGain, 
EDC, UEDC, NonProratedEDC, PlantEDC, NonProratedPlantEDC, NonProratedRV, PlantRV, NonProratedPlantRV,
NEOpexEffDiv, NonProratedNEOpexEffDiv, PlantNEOpexEffDiv, NonProratedPlantNEOpexEffDiv, 
MaintEffDiv, NonProratedMaintEffDiv, PlantMaintEffDiv, NonProratedPlantMaintEffDiv, 
PersEffDiv, NonProratedPersEffDiv, PlantPersEffDiv, NonProratedPlantPersEffDiv, 
MaintPersEffDiv, NonProratedMaintPersEffDiv, PlantMaintPersEffDiv, NonProratedPlantMaintPersEffDiv, 
NonMaintPersEffDiv, NonProratedNonMaintPersEffDiv, PlantNonMaintPersEffDiv, NonProratedPlantNonMaintPersEffDiv)
SELECT f.SubmissionID, f.FactorSet, c.ProcessID, SUM(f.EDCNoMult), SUM(f.UEDCNoMult), 1,SUM(f.RV), CASE WHEN SUM(c.BPD) > 0 THEN SUM(f.RV*1000000)/SUM(c.BPD) END, SUM(f.StdEnergy), SUM(f.StdGain), 
SUM(f.EDCNoMult), SUM(f.UEDCNoMult), SUM(f.NonProratedEDC), SUM(f.PlantEDC), SUM(f.NonProratedPlantEDC), 
SUM(f.NonProratedRV), SUM(f.PlantRV), SUM(f.NonProratedPlantRV),
SUM(NEOpexEffDiv), SUM(NonProratedNEOpexEffDiv), SUM(PlantNEOpexEffDiv), SUM(NonProratedPlantNEOpexEffDiv), 
SUM(MaintEffDiv), SUM(NonProratedMaintEffDiv), SUM(PlantMaintEffDiv), SUM(NonProratedPlantMaintEffDiv), 
SUM(PersEffDiv), SUM(NonProratedPersEffDiv), SUM(PlantPersEffDiv), SUM(NonProratedPlantPersEffDiv), 
SUM(MaintPersEffDiv), SUM(NonProratedMaintPersEffDiv), SUM(PlantMaintPersEffDiv), SUM(NonProratedPlantMaintPersEffDiv), 
SUM(NonMaintPersEffDiv), SUM(NonProratedNonMaintPersEffDiv), SUM(PlantNonMaintPersEffDiv), SUM(NonProratedPlantNonMaintPersEffDiv)
FROM FactorCalc f INNER JOIN ConfigRS c ON c.SubmissionID = f.SubmissionID AND c.UnitID = f.UnitID
WHERE f.SubmissionID = @SubmissionID
GROUP BY f.SubmissionID, f.FactorSet, c.ProcessID

UPDATE FactorProcessCalc
SET UtilPcnt = CASE WHEN EDCNoMult > 0 THEN UEDCNoMult/EDCNoMult * 100 END
WHERE SubmissionID = @SubmissionID
DELETE FROM FactorTotCalc WHERE SubmissionID = @SubmissionID
DECLARE @EDC real, @UEDC real, @UtilPcnt real, @Complexity real, @EDCNoMult real, @NonProratedEDC real, 
	@InServicePcnt real, @PlantEDC real, @AllocPcntOfTime real, @NonProratedPlantEDC real, 
	@TotProcessEDC real, @TotProcessUEDC real, @TotProcessUtilPcnt real, @TotProcessComp real, @TotProcessEDCNoMult real, @PlantTotProcessEDC real, @NonProRatedProcessEDC real, 
	@TotRSBPSD real, @TotRSBPCD real, @TotRSEDC real, @TotRSUEDC real, 
	@RV real, @TotProcessRV real, @ProcessOffsitesRV real, @NonProcessOffsitesRV real, @CatChemRV real, @SparePartsRV real, @NonProRatedRV real, @NonProRatedProcessRV real, 
	@PlantRV real, @PlantTotProcessRV real, @PlantProcessOffsitesRV real, @PlantNonProcessOffsitesRV real, @PlantCatChemRV real, @PlantSparePartsRV real, @NonProratedPlantRV real, 
	@EII real, @EnergyUseDay real, @TotStdEnergy real, @SensHeatUtilCap real, @SensHeatStdEnergy real, @TotSensHeatPcnt real, 
	@OffsitesUtilCap real, @OffsitesStdEnergy real, @TotOffsitesPcnt real, @AspUtilCap real, @AspStdEnergy real, @TotAspPcnt real, 
	@EstGain real, @ReportLossGain real, @VEI real, @PEI real, @RefAge real, 
	@UPC real, @UPC_ProcessUEDCNM real, @OthProcessEDC real, @NumDays real, @NetInputBbl real, @AvgInputBPD real
DECLARE @CDUCap real, @VACCap real, @cmplxCap real, @NPCmplx real, @PlantCmplx real, @NPPlantCmplx real, @CrudeAPI real
DECLARE @rvProcOffsite real, @rvNonProcOffsite real, @rvCatChem real, @rvSpareParts real, @rvTotal real, @PostRVComplexityMessage bit
SELECT @NumDays = NumDays FROM Submissions WHERE SubmissionID = @SubmissionID
SELECT 	@TotRSBPSD = SUM(BPD), @TotRSBPCD = SUM(BPD)
FROM ConfigRS
WHERE SubmissionID = @SubmissionID
SELECT 	@NetInputBbl = NetInputBbl, @ReportLossGain = GainBbl FROM MaterialTot WHERE SubmissionID = @SubmissionID
SELECT 	@EnergyUseDay = TotEnergyConsMBTU/@NumDays FROM EnergyTot WHERE SubmissionID = @SubmissionID
SELECT	@AspUtilCap = ISNULL(SUM(Bbl)/@NumDays, 0)
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'ASP'
SELECT	@CrudeAPI = AvgGravity FROM CrudeTot WHERE SubmissionID = @SubmissionID
SELECT @AvgInputBPD = AnnInputBbl/365.0
FROM EDCStabilizers
WHERE SubmissionID = @SubmissionID 
IF @AvgInputBPD IS NULL
	SELECT @AvgInputBPD = TotInputBbl/@NumDays FROM MaterialTot
	WHERE SubmissionID = @SubmissionID

-- Sensible Heat Criteria based on configuration
SELECT S.CriteriaNum, SUM(C.Cap) TotCap
INTO #shcrit
FROM Config C, SensHeatCriteria S
WHERE C.ProcessID = S.ProcessID AND C.SubmissionID = @SubmissionID
AND (C.ProcessType = S.ProcessType OR S.ProcessType = '')
GROUP BY S.CriteriaNum
INSERT INTO FactorTotCalc (SubmissionID, FactorSet, EDC, UEDC, EDCNoMult, NonProratedEDC, PlantEDC, NonProratedPlantEDC, 
TotProcessEDC, TotProcessUEDC, TotProcessEDCNoMult, PlantTotProcessEDC, NonProRatedProcessEDC, OthProcessEDC, 
TotRSBPSD, TotRSBPCD, TotRSEDC, TotRSUEDC, TotProcessRV, PlantTotProcessRV, NonProRatedProcessRV, 
RV, NonProRatedRV, PlantRV, NonProratedPlantRV, 
TotStdEnergy, EstGain, RefAge,
NEOpexEffDiv, NonProratedNEOpexEffDiv, PlantNEOpexEffDiv, NonProratedPlantNEOpexEffDiv, 
MaintEffDiv, NonProratedMaintEffDiv, PlantMaintEffDiv, NonProratedPlantMaintEffDiv, 
PersEffDiv, NonProratedPersEffDiv, PlantPersEffDiv, NonProratedPlantPersEffDiv, 
MaintPersEffDiv, NonProratedMaintPersEffDiv, PlantMaintPersEffDiv, NonProratedPlantMaintPersEffDiv, 
NonMaintPersEffDiv, NonProratedNonMaintPersEffDiv, PlantNonMaintPersEffDiv, NonProratedPlantNonMaintPersEffDiv, 
TotRSNEOpexEffDiv, TotRSMaintEffDiv, TotRSPersEffDiv, TotRSMaintPersEffDiv, TotRSNonMaintPersEffDiv, 
OthProcessNEOpexEffDiv, OthProcessMaintEffDiv, OthProcessPersEffDiv, OthProcessMaintPersEffDiv, OthProcessNonMaintPersEffDiv)
SELECT f.SubmissionID, f.FactorSet, SUM(EDC), SUM(UEDC), SUM(EDCNoMult), SUM(NonProratedEDC), SUM(PlantEDC), SUM(NonProratedPlantEDC),
TotProcessEDC = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN EDC ELSE 0 END), 
TotProcessUEDC = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN UEDC ELSE 0 END), 
TotProcessEDCNoMult = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN EDCNoMult ELSE 0 END), 
PlantTotProcessEDC = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN PlantEDC ELSE 0 END), 
NonProRatedProcessEDC = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN NonProratedEDC ELSE 0 END),
OthProcessEDC = SUM(CASE WHEN lu.ProcessGroup = 'O' THEN EDC ELSE 0 END), 
@TotRSBPSD, @TotRSBPCD,
TotRSEDC = SUM(CASE WHEN lu.ProcessGroup = 'R' THEN EDC ELSE 0 END), 
TotRSUEDC = SUM(CASE WHEN lu.ProcessGroup = 'R' THEN UEDC ELSE 0 END), 
TotProcessRV = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN RV ELSE 0 END), 
PlantTotProcessRV = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN PlantRV ELSE 0 END), 
NonProRatedProcessRV = SUM(CASE WHEN lu.ProcessGroup NOT IN ('U','R') THEN NonProratedRV ELSE 0 END), 
SUM(RV), SUM(NonProRatedRV), SUM(PlantRV), SUM(NonProratedPlantRV), 
SUM(StdEnergy), SUM(StdGain*s.NumDays), SUM(YearsOper*EDC)/SUM(EDC),
SUM(NEOpexEffDiv), SUM(NonProratedNEOpexEffDiv), SUM(PlantNEOpexEffDiv), SUM(NonProratedPlantNEOpexEffDiv), 
SUM(MaintEffDiv), SUM(NonProratedMaintEffDiv), SUM(PlantMaintEffDiv), SUM(NonProratedPlantMaintEffDiv), 
SUM(PersEffDiv), SUM(NonProratedPersEffDiv), SUM(PlantPersEffDiv), SUM(NonProratedPlantPersEffDiv), 
SUM(MaintPersEffDiv), SUM(NonProratedMaintPersEffDiv), SUM(PlantMaintPersEffDiv), SUM(NonProratedPlantMaintPersEffDiv), 
SUM(NonMaintPersEffDiv), SUM(NonProratedNonMaintPersEffDiv), SUM(PlantNonMaintPersEffDiv), SUM(NonProratedPlantNonMaintPersEffDiv), 
SUM(CASE WHEN lu.ProcessGroup = 'R' THEN NEOpexEffDiv ELSE 0 END), 
SUM(CASE WHEN lu.ProcessGroup = 'R' THEN MaintEffDiv ELSE 0 END), 
SUM(CASE WHEN lu.ProcessGroup = 'R' THEN PersEffDiv ELSE 0 END), 
SUM(CASE WHEN lu.ProcessGroup = 'R' THEN MaintPersEffDiv ELSE 0 END), 
SUM(CASE WHEN lu.ProcessGroup = 'R' THEN NonMaintPersEffDiv ELSE 0 END), 
SUM(CASE WHEN lu.ProcessGroup = 'O' THEN NEOpexEffDiv ELSE 0 END), 
SUM(CASE WHEN lu.ProcessGroup = 'O' THEN MaintEffDiv ELSE 0 END), 
SUM(CASE WHEN lu.ProcessGroup = 'O' THEN PersEffDiv ELSE 0 END),
SUM(CASE WHEN lu.ProcessGroup = 'O' THEN MaintPersEffDiv ELSE 0 END),
SUM(CASE WHEN lu.ProcessGroup = 'O' THEN NonMaintPersEffDiv ELSE 0 END)
FROM FactorProcessCalc f INNER JOIN ProcessID_LU lu ON lu.ProcessID = f.ProcessID
INNER JOIN Submissions s ON s.SubmissionId = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID
GROUP BY f.SubmissionID, f.FactorSet

SELECT @CDUCap = SUM(CASE WHEN ProcessID = 'CDU' THEN InserviceCap ELSE 0 END),
@VACCap = SUM(CASE WHEN ProcessID = 'VAC' THEN InserviceCap ELSE 0 END)
FROM ProcessTotCalc
WHERE SubmissionID = @SubmissionID AND ProcessID IN ('CDU','VAC')
IF @CDUCap > 0
	SELECT @cmplxCap = @CDUCap 
ELSE
	SELECT @cmplxCap = @VACCap*CmplxVacCapMult, @Complexity = CmplxDefault
	FROM FactorSets WHERE RefineryType = 'FUELS' AND FactorSet = @FactorSet

DECLARE cFactorSets CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
	SELECT 	FactorSet, EDC, TotProcessEDC, NonProratedEDC, PlantEDC, NonProratedPlantEDC,
		RV, NonProratedRV, PlantRV, NonProratedPlantRV, TotStdEnergy, EstGain
	FROM FactorTotCalc WHERE SubmissionID = @SubmissionID
OPEN cFactorSets
FETCH NEXT FROM cFactorSets INTO @FactorSet, @EDC, @TotProcessEDC, @NonProratedEDC, @PlantEDC, @NonProratedPlantEDC, 
		@RV, @NonProratedRV, @PlantRV, @NonProratedPlantRV, @TotStdEnergy, @EstGain
WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @TnkStdCap real, @TnkStdEDC real
	DECLARE @FactorConstant real, @FactorExponent real, @MinCap real, @MaxCap real, @Factor real
	SELECT @TnkStdCap = TnkStdCapBPDConstant*POWER(@AvgInputBPD, TnkStdCapBPDExponent) +
			TnkStdCapPEDCConstant*POWER(@TotProcessEDC, TnkStdCapPEDCExponent)
	FROM FactorSets WHERE FactorSet = @FactorSet
	SELECT @FactorConstant = EDCConstant, @FactorExponent = EDCExponent, @MinCap = EDCMinCap, @MaxCap = EDCMaxCap
	FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'TNKSTD' AND ProcessType = 'FTNK'
	EXEC spCalcFactor @TnkStdCap, @FactorConstant, @FactorExponent, @MinCap, @MaxCap, @Factor OUTPUT, @TnkStdEDC OUTPUT
	SELECT @TnkStdEDC = ISNULL(@TnkStdEDC, 0)

	DECLARE @PurElecUCap real, @PurStmUCap real, @PurElecUEDC real, @PurStmUEDC real
	IF EXISTS (SELECT * FROM FactorSets WHERE FactorSet = @FactorSet AND RefineryType = 'FUELS' AND CalcPurElecStmUEDC = 'Y')
	BEGIN
		SELECT @PurStmUCap = SUM(ISNULL(SourceMBTU, 0) - ISNULL(UsageMBTU, 0)) *1000/1100/(24*@NumDays) 
		FROM Energy WHERE SubmissionID = @SubmissionID AND EnergyType = 'STM' AND TransType IN ('PUR','DST')
		SELECT @PurElecUCap = SUM(ISNULL(SourceMWH, 0) - ISNULL(UsageMWH, 0))*1000/(24*@NumDays) 
		FROM Electric WHERE SubmissionID = @SubmissionID AND EnergyType = 'ELE' AND TransType IN ('PUR','DST')
		SELECT @PurStmUcap = 0 WHERE @PurStmUCap < 0
		SELECT @PurElecUcap = 0 WHERE @PurElecUCap < 0
		SELECT @FactorConstant = EDCConstant, @FactorExponent = EDCExponent, @MinCap = EDCMinCap, @MaxCap = EDCMaxCap
		FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'FTCOGEN' AND ProcessType = 'INDT' AND CogenES = 'E'
		EXEC spCalcFactor @PurElecUCap, @FactorConstant, @FactorExponent, NULL, NULL, @Factor OUTPUT, @PurElecUEDC OUTPUT
		SELECT @PurElecUEDC = ISNULL(@PurElecUEDC, 0)
		SELECT @FactorConstant = EDCConstant, @FactorExponent = EDCExponent, @MinCap = EDCMinCap, @MaxCap = EDCMaxCap
		FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'FTCOGEN' AND ProcessType = 'INDT' AND CogenES = 'S'
		EXEC spCalcFactor @PurStmUCap, @FactorConstant, @FactorExponent, NULL, NULL, @Factor OUTPUT, @PurStmUEDC OUTPUT
		SELECT @PurStmUEDC = ISNULL(@PurStmUEDC, 0)
	END
	ELSE
		SELECT @PurElecUEDC = 0, @PurStmUEDC = 0

	SELECT	@EDC = @EDC + @TnkStdEDC,
		@NonProratedEDC = @NonProratedEDC + @TnkStdEDC, 
		@PlantEDC = @PlantEDC + @TnkStdEDC, 
		@NonProratedPlantEDC = @NonProratedPlantEDC + @TnkStdEDC

	IF @cmplxCap > 0
		SELECT 	@Complexity = @EDC/@cmplxCap, @TotProcessComp = @TotProcessEDC/@cmplxCap,
			@NPCmplx = @NonProratedEDC/@cmplxCap, @PlantCmplx = @PlantEDC/@cmplxCap,
			@NPPlantCmplx = @NonProratedPlantEDC/@cmplxCap
	ELSE
		SELECT 	@TotProcessComp = @Complexity, @NPCmplx = @Complexity, 
			@PlantCmplx = @Complexity, @NPPlantCmplx = @Complexity
	-- RV
	EXEC dbo.RVComponents @FactorSet, @Complexity, @RV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @ProcessOffsitesRV = @rvProcOffsite, @NonProcessOffsitesRV = @rvNonProcOffsite, @CatChemRV = @rvCatChem, @SparePartsRV = @rvSpareParts, @RV = @rvTotal
	-- Non-prorated RV
	EXEC dbo.RVComponents @FactorSet, @NPCmplx, @NonProratedRV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @NonProratedRV = @rvTotal
	-- Plant replacement values
	EXEC dbo.RVComponents @FactorSet, @PlantCmplx, @PlantRV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @PlantProcessOffsitesRV = @rvProcOffsite, @PlantNonProcessOffsitesRV = @rvNonProcOffsite, @PlantCatChemRV = @rvCatChem, @PlantSparePartsRV = @rvSpareParts, @PlantRV = @rvTotal
	-- Non-prorated Plant replacement values
	EXEC dbo.RVComponents @FactorSet, @NPPlantCmplx, @NonProratedPlantRV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @NonProratedPlantRV = @rvTotal
	-- Sensible Heat
	SELECT @SensHeatUtilCap = SUM(Y.BBL)
	FROM Yield Y, Material_LU L
	WHERE Y.SubmissionID = @SubmissionID AND Category IN ('RMI', 'OTHRM') AND Y.MaterialID <> 'CRD'
	AND Y.MaterialID = L.MaterialID AND L.SensHeatCriteria IS NOT NULL
	AND (L.SensHeatCriteria = 0 OR L.SensHeatCriteria IN (SELECT CriteriaNum FROM #shcrit WHERE #shcrit.TotCap > 0))
	IF EXISTS (SELECT * FROM FactorSets WHERE FactorSet = @FactorSet AND RefineryType = 'FUELS' AND CrudeInSensHeat = 'Y')
		SELECT @SensHeatUtilCap = ISNULL(@SensHeatUtilCap, 0) + ISNULL(TotBbl, 0) FROM CrudeTot WHERE SubmissionID = @SubmissionID
	SELECT 	@SensHeatUtilCap = ISNULL(@SensHeatUtilCap/@NumDays, 0)
	IF @CDUCap > 0 OR @VACCap > 0
		SELECT @OffsitesUtilCap = @NetInputBbl/@NumDays
        ELSE
		SELECT @OffsitesUtilCap = @SensHeatUtilCap
	SELECT 	@SensHeatStdEnergy = (@SensHeatUtilCap/1000) * (SensHeatConstant + SensHeatAPIFactor* ISNULL(@CrudeAPI, 32)),
		@OffsitesStdEnergy = (@OffsitesUtilCap/1000) * (OffsitesEnergyConstant + OffsitesEnergyCmplxFactor * (CASE WHEN @Complexity > OffsitesEnergyCmplxMax THEN OffsitesEnergyCmplxDef ELSE @Complexity END))
	FROM FactorSets WHERE RefineryType = 'FUELS' AND FactorSet = @FactorSet
	IF @AspUtilCap > 0
		SELECT @AspStdEnergy = @AspUtilCap/1000 * CONVERT(real, EIIFormula)
		FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'ASP'
	ELSE
		SELECT @AspStdEnergy = 0
	SELECT @TotStdEnergy = ISNULL(@TotStdEnergy, 0) + ISNULL(@SensHeatStdEnergy, 0) + ISNULL(@OffsitesStdEnergy, 0) + ISNULL(@AspStdEnergy, 0)
	IF @TotStdEnergy > 0
		SELECT 	@TotSensHeatPcnt = @SensHeatStdEnergy/@TotStdEnergy*100, 
			@TotOffsitesPcnt = @OffsitesStdEnergy/@TotStdEnergy*100, 
			@TotAspPcnt = @AspStdEnergy/@TotStdEnergy*100, 
			@EII = @EnergyUseDay/@TotStdEnergy*100
	ELSE
		SELECT 	@TotSensHeatPcnt = NULL, @TotOffsitesPcnt = NULL, @TotAspPcnt = NULL, @EII = NULL

	UPDATE FactorTotCalc
	SET TnkStdCap = @TnkStdCap, TnkStdEDC = @TnkStdEDC, 
	PurStmUEDC = @PurStmUEDC, PurElecUEDC = @PurElecUEDC,
	EDCNoMult = EDCNoMult + @TnkStdEDC, EDC = EDC + @TnkStdEDC, 
	UEDC = UEDC + @TnkStdEDC + @PurStmUEDC + @PurElecUEDC, 
	NonProratedEDC = NonProratedEDC + @TnkStdEDC, 
	PlantEDC = PlantEDC + @TnkStdEDC, 
	NonProratedPlantEDC = NonProratedPlantEDC + @TnkStdEDC
	WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

	IF NOT EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID IN ('FCC','HYC','COK'))
		SELECT @VEI = NULL
	ELSE IF @EstGain = 0
		SELECT @VEI = NULL
	ELSE
		SELECT @VEI = @ReportLossGain/@EstGain*100

	UPDATE FactorTotCalc
	SET Complexity = @Complexity, TotProcessComp = @TotProcessComp, 
	UtilPcnt = CASE WHEN EDC=0 THEN NULL ELSE (UEDC-PurElecUEDC-PurStmUEDC)/EDC*100 END,
	TotProcessUtilPcnt = CASE WHEN TotProcessEDC=0 THEN NULL ELSE TotProcessUEDC/TotProcessEDC*100 END,
	InservicePcnt = CASE WHEN NonProratedProcessEDC = 0 THEN NULL ELSE TotProcessEDC/NonProratedProcessEDC*100 END,
        AllocPcntOfTime = CASE WHEN PlantEDC = 0 THEN NULL ELSE EDC/PlantEDC*100 END,
	ProcessOffsitesRV = @ProcessOffsitesRV, NonProcessOffsitesRV = @NonProcessOffsitesRV, CatChemRV = @CatChemRV, SparePartsRV = @SparePartsRV, RV = @RV,
	NonProratedRV = @NonProratedRV, NonProratedPlantRV = @NonProratedPlantRV,
	PlantProcessOffsitesRV = @PlantProcessOffsitesRV, PlantNonProcessOffsitesRV = @PlantNonProcessOffsitesRV, PlantCatChemRV = @PlantCatChemRV, PlantSparePartsRV = @PlantSparePartsRV, PlantRV = @PlantRV,
	SensHeatUtilCap = @SensHeatUtilCap, SensHeatStdEnergy = @SensHeatStdEnergy, TotSensHeatPcnt = @TotSensHeatPcnt, 
	OffsitesUtilCap = @OffsitesUtilCap, OffsitesStdEnergy = @OffsitesStdEnergy, TotOffsitesPcnt = @TotOffsitesPcnt, 
	AspUtilCap = @AspUtilCap, AspStdEnergy = @AspStdEnergy, TotAspPcnt = @TotAspPcnt, 
	EII = @EII, EnergyUseDay = @EnergyUseDay, TotStdEnergy = @TotStdEnergy,
	ReportLossGain = @ReportLossGain, EstGain = @EstGain, 
	VEI = @VEI,
	PEI = CASE WHEN @NetInputBbl>0 THEN 100*(@NetInputBbl+@ReportLossGain) / (@NetInputBbl+@EstGain) END
	WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet


	FETCH NEXT FROM cFactorSets INTO @FactorSet, @EDC, @TotProcessEDC, @NonProratedEDC, @PlantEDC, @NonProratedPlantEDC, 
		@RV, @NonProratedRV, @PlantRV, @NonProratedPlantRV, @TotStdEnergy, @EstGain
END
CLOSE cFactorSets
DEALLOCATE cFactorSets
DROP TABLE #shcrit

UPDATE FactorCalc
SET UnitEII = c.EnergyPcnt*ftc.EnergyUseDay/CASE WHEN ftc.FactorSet = '2004' AND c.ProcessID = 'CDU' AND ftc.SensHeatUtilCap <> 0 THEN FactorCalc.StdEnergy + (c.UtilCap/ftc.SensHeatUtilCap * ftc.SensHeatStdEnergy) ELSE FactorCalc.StdEnergy END
FROM FactorCalc INNER JOIN Config c ON c.SubmissionID = FactorCalc.SubmissionID AND c.UnitID = FactorCalc.UnitID
INNER JOIN FactorTotCalc ftc ON ftc.SubmissionID = FactorCalc.SubmissionID AND ftc.FactorSet = FactorCalc.FactorSet
WHERE FactorCalc.StdEnergy > 0 AND FactorCalc.SubmissionID = @SubmissionID







