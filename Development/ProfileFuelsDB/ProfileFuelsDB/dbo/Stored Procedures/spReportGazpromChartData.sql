﻿


CREATE   PROC [dbo].[spReportGazpromChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2008', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	UtilPcnt real NULL, 
	VEI real NULL, 
	OpAvail real NULL, 
	RoutIndex real NULL,
	PersIndex real NULL, 
	NEOpexEDC real NULL,
	OpexUEDC real NULL,

	ProcessUtilPcnt real NULL, 
	MaintIndex real NULL
)

--- Everything Already Available in Gensum
INSERT INTO @data (PeriodStart, PeriodEnd, UtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex, RoutIndex, MaintIndex, NEOpexEDC, OpexUEDC)
SELECT	PeriodStart, PeriodEnd, UtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex = TotWHrEDC, RoutIndex, MaintIndex, NEOpexEDC, TotCashOpexUEDC
FROM Gensum
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario
AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd

IF (SELECT COUNT(*) FROM @data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
			INSERT @data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

UPDATE @Data
SET EII = NULL, UtilPcnt = NULL, VEI = NULL, RoutIndex = NULL, PersIndex = NULL, NEOpexEDC = NULL, OpexUEDC = NULL
WHERE DATEPART(yy, PeriodStart) < 2011

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilPcnt, VEI, OpAvail, RoutIndex, PersIndex, NEOpexEDC, OpexUEDC
FROM @Data
ORDER BY PeriodStart ASC





