﻿create PROC [dbo].[GetRefinery]
	@RefineryID varchar(20)
AS
	DECLARE @T varchar(20)
	SELECT @T = RefineryID from TSort where RefineryID = @RefineryID
	
	IF @T is null 
		select 0
	else
		select 1
	
	