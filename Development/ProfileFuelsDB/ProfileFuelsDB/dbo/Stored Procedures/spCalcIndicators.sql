﻿

CREATE   PROC [dbo].[spCalcIndicators](@SubmissionID int)
AS
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spCalcIndicators', @MessageText = 'Started'
	EXEC spPersCalcs @SubmissionID  -- Must be run after all turnrounds have been processed
	EXEC spCalcAvail @SubmissionID
	EXEC spUtilOSTA @SubmissionID
	EXEC spMaintCosts @SubmissionID	-- Must be run after Factors for EDC
	EXEC spOpex @SubmissionID	-- Must be run after Energy, MaintCosts, and Factors
	EXEC spMarginCalc @SubmissionID	-- Must be run after opex
	EXEC CalcCEI2008 @SubmissionID
	EXEC spGensum @SubmissionID
	EXEC spTargets @SubmissionID
	EXEC spAverages @SubmissionID

	IF EXISTS (SELECT * FROM Submissions WHERE SubmissionID = @SubmissionID AND RefineryID = '61PAC')
		EXEC spSKCustomUnitData @SubmissionID

	UPDATE Submissions SET CalcsNeeded = NULL, LastCalc = getdate()
	WHERE SubmissionID = @SubmissionID

	UPDATE Submissions
	SET CalcsNeeded = CASE WHEN Submissions.PeriodStart > DATEADD(mm, 12, a.PeriodStart) THEN 'M' ELSE 'A' END
	FROM Submissions INNER JOIN Submissions a ON a.RefineryID = Submissions.RefineryID AND a.DataSet = Submissions.DataSet
	WHERE a.SubmissionID = @SubmissionID AND (Submissions.PeriodStart > a.PeriodStart AND Submissions.PeriodStart < DATEADD(mm, 24, a.PeriodStart))
	AND (Submissions.CalcsNeeded IS NULL OR Submissions.CalcsNeeded NOT IN ('F','T'))

	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spCalcIndicators', @MessageText = 'Completed'





