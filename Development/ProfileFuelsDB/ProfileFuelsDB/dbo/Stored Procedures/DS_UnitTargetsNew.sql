﻿CREATE PROCEDURE [dbo].[DS_UnitTargetsNew]
		@RefineryID nvarchar(10),
		@Dataset nvarchar(20)='ACTUAL'
AS
BEGIN
		SELECT ut.UnitID,c.UnitName,c.ProcessID,ut.Property,ut.Target,ut.CurrencyCode, 
                   ul.USDescription, ul.MetDescription, ul.USDecPlaces, ul.MetDecPlaces,ul.SortKey 
                   FROM UnitTargetsNew ut,UnitTargets_LU ul, Config c  
                   WHERE ut.SubmissionID = c.SubmissionID AND ut.UnitID = c.UnitID AND ul.Property = ut.Property
                   AND (ul.ProcessID='ALL' OR ul.ProcessID=c.ProcessID)  
                   AND c.SubmissionID=(Select MAX(SubmissionID) FROM Submissions WHERE RefineryID= @RefineryID and DataSet = @Dataset and UseSubmission=1) ORDER BY ul.SortKey
END
