﻿




CREATE  PROC [dbo].[spReportBashneftChartData2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2008', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

	EXEC ProfileFuelsDev.dbo.spReportBashneftChartData2 @RefineryID, @PeriodYear, @PeriodMonth, @Dataset, @FactorSet, @Scenario, @Currency, @UOM, @IncludeTarget, @IncludeYTD, @IncludeAvg