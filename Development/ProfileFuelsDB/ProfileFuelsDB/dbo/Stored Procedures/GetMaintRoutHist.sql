﻿CREATE PROC [dbo].[GetMaintRoutHist] (@RefineryID varchar(6), @DataSet varchar(15))
AS

DECLARE @FirstEntry smalldatetime, @Currency CurrencyCode
DECLARE @Months tinyint, @HistDate as smalldatetime
DECLARE @RoutCost real, @RoutMatl real
SELECT @FirstEntry = MIN(PeriodStart) FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet

DECLARE @hist TABLE (
	PeriodStart smalldatetime NOT NULL, 
	RoutCostLocal real NULL, 
	RoutMatlLocal real NULL
)

SELECT TOP 1 @Currency = RptCurrency FROM Submissions 
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
ORDER BY PeriodStart DESC

SELECT @Months = 0
WHILE @Months < 24
BEGIN
	SELECT @Months = @Months + 1, @RoutCost = NULL, @RoutMatl = NULL
	SELECT @HistDate = DATEADD(mm, -@Months, @FirstEntry)
	
	SELECT @RoutCost = RoutCost, @RoutMatl = RoutMatl
	FROM MaintRoutHist
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND ABS(DATEDIFF(dd, PeriodStart, @HistDate)) <= 1
	AND Currency = @Currency
	
	INSERT INTO @hist(PeriodStart, RoutCostLocal, RoutMatlLocal)
	VALUES (@HistDate, @RoutCost, @RoutMatl)
END

SELECT * FROM @Hist ORDER BY PeriodStart


