﻿


CREATE    PROC [dbo].[spReportFactorDetailsUnit] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency= @Currency, UOM = @UOM, 
lu.description, c.UnitName, c.Processid, c.ProcessType, c.Cap, c.RptCap, c.UtilPcnt,
f.StdEnergy, f.StdGain, f.EDCFactor, ISNULL(EDCNoMult,0)*ISNULL(MultiFactor,1)/1000 as kEDC, ISNULL(UEDCNoMult,0)*ISNULL(MultiFactor,1)/1000 as kUEDC, 
MaintEffDiv/1000 as MaintEffDiv, NonMaintPersEffDiv/1000 as nmPersEffDiv, MaintPersEffDiv/1000 as mPersEffDiv, PersEffDiv/1000 as PersEffDiv, 
NEOpexEffDiv/1000 as NEOpexEffDiv, lu.SortKey, c.UnitID, 
ProcessGrouping = CASE WHEN lu.ProcessGroup = 'U' THEN 'Utility' ELSE 'Process' END
FROM Config c INNER JOIN FactorCalc f ON c.SubmissionID=f.SubmissionID AND f.UnitID=c.UnitID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
WHERE f.FactorSet=@FactorSet AND c.ProcessID NOT IN ('BLENDING')  
AND s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth)
ORDER BY s.PeriodStart DESC, lu.Sortkey ASC, c.UnitID ASC




