﻿
CREATE   PROC [dbo].[spUtilOSTA] (@SubmissionID int)
AS
SET NOCOUNT ON

DECLARE @NumDays real
SELECT @NumDays = NumDays FROM Submissions WHERE SubmissionID = @SubmissionID

UPDATE FactorCalc
SET UtilOSTA = UtilPcnt
WHERE SubmissionID = @SubmissionID
AND ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE InUtilOSTA = 'Y')

UPDATE FactorCalc
SET UtilOSTA = CASE WHEN (@NumDays * c.InServicePcnt/100 <= ta.HrsDownInPeriod/24.0) THEN 100 ELSE
	FactorCalc.UtilPcnt*((c.InservicePcnt/100 * @NumDays)/
	(@NumDays * c.InServicePcnt/100 - ta.HrsDownInPeriod/24.0)) END
FROM FactorCalc INNER JOIN Config c ON c.SubmissionID = FactorCalc.SubmissionID AND c.UnitID = FactorCalc.UnitID
INNER JOIN TAForSubmission ta ON ta.SubmissionID = c.SubmissionID AND ta.UnitID = c.UnitID
WHERE FactorCalc.SubmissionID = @SubmissionID AND ta.HrsDownInPeriod > 0
AND FactorCalc.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE InUtilOSTA = 'Y')

UPDATE FactorCalc
SET UtilOSTA = CASE WHEN UtilOSTA > 100 THEN CASE WHEN UtilPcnt > 100 THEN UtilPcnt ELSE 100 END ELSE UtilOSTA END,
UEDCOSTA = EDCNoMult*(CASE WHEN UtilOSTA > 100 THEN CASE WHEN UtilPcnt > 100 THEN UtilPcnt ELSE 100 END ELSE UtilOSTA END)/100
WHERE SubmissionID = @SubmissionID

SELECT f.FactorSet, f.ProcessID, SUM(f.UEDCOSTA) AS UEDCOSTA, GlobalDB.dbo.WtAvg(f.UtilOSTA, f.EDCNoMult*(100.0-mc.MechUnavailTA_Act)/100.0) AS UtilOSTA
INTO #ProcessOSTA
FROM FactorCalc f 
INNER JOIN MaintCalc mc ON mc.SubmissionID = f.SubmissionID AND mc.UnitID = f.UnitID
WHERE f.SubmissionID = @SubmissionID /*AND f.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE InUtilOSTA = 'Y')*/
GROUP BY f.FactorSet, f.ProcessID

UPDATE FactorProcessCalc
SET UEDCOSTA = FactorProcessCalc.MultiFactor * osta.UEDCOSTA,
UtilOSTA = osta.UtilOSTA
FROM FactorProcessCalc INNER JOIN #ProcessOSTA osta
	ON osta.FactorSet = FactorProcessCalc.FactorSet 
	AND osta.ProcessID = FactorProcessCalc.ProcessID
WHERE FactorProcessCalc.SubmissionID = @SubmissionID

DROP TABLE #ProcessOSTA

SELECT FactorSet, SUM(f.UEDCOSTA) AS UEDCOSTA, GlobalDB.dbo.WtAvg(f.UtilOSTA, f.EDCNoMult*(100.0-mc.MechUnavailTA_Act)/100.0) AS UtilOSTA
INTO #RefOSTA
FROM FactorCalc f 
INNER JOIN MaintCalc mc ON mc.SubmissionID = f.SubmissionID AND mc.UnitID = f.UnitID
WHERE f.SubmissionID = @SubmissionID AND f.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE InUtilOSTA = 'Y')
GROUP BY f.FactorSet

UPDATE FactorTotCalc
SET UEDCOSTA = osta.UEDCOSTA, UtilOSTA = osta.UtilOSTA
FROM FactorTotCalc INNER JOIN #RefOSTA osta ON osta.FactorSet = FactorTotCalc.FactorSet
WHERE FactorTotCalc.SubmissionID = @SubmissionID

DROP TABLE #RefOSTA





