﻿
CREATE  PROC spReportCustomUnit (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US', @ProcessID ProcessID)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.Numdays AS DaysInPeriod, curd.* 
FROM CustomUnitReportData curd INNER JOIN Submissions s ON s.SubmissionID = curd.SubmissionID
WHERE  s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth)
AND ProcessID = @ProcessID
AND (FactorSet = @FactorSet or FactorSet = 'N/A') 
AND (Currency = @Currency or Currency = 'N/A') 
ORDER BY UnitID, SortKey


