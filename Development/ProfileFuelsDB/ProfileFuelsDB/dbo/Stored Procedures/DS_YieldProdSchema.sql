﻿CREATE PROCEDURE [dbo].[DS_YieldProdSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT m.SortKey, RTRIM(y.Category) AS Category, RTRIM(y.MaterialID) as MaterialID, 
                   RTRIM(y.MaterialName) as MaterialName, CAST(0.0 AS FLOAT) AS BBL, CAST(0.0 AS FLOAT) AS PriceLocal FROM YIELD y , Material_LU m WHERE  m.MaterialID=y.MaterialID AND  
                   y.Category NOT IN ('OTHRM','RCHEM','RLUBE','RMI','RPF') AND  
                   y.MaterialID <> 'GAIN' AND y.SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID=@RefineryID)
                   
                   END
