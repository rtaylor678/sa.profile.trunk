﻿



CREATE     PROC spTACosts(@RefineryID varchar(6), @Dataset varchar(15), @UnitID UnitID, @TAID int)
AS
IF @TAID = 0
	RETURN
DECLARE @TACurrency CurrencyCode, @TADate smalldatetime, @TACostLocal real, @TAMatlLocal real, @AnnTACostLocal real, @AnnTAMatlLocal real, @NextTADate smalldatetime, @ProcessID ProcessID, @RestartDate smalldatetime
SELECT 	@TACurrency = TACurrency, @TADate = TADate, @TACostLocal = TACostLocal, @TAMatlLocal = TAMatlLocal, 
	@AnnTACostLocal = AnnTACostLocal, @AnnTAMatlLocal = AnnTAMatlLocal, 
	@NextTADate = NextTADate, @ProcessID = ProcessID, @RestartDate = RestartDate
FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID

DELETE FROM MaintTACost
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID

INSERT INTO MaintTACost (RefineryID, DataSet, UnitID, ProcessID, TAID, Currency, TADate, NextTADate, RestartDate, TACost, TAMatl, AnnTACost, AnnTAMatl)
SELECT @RefineryID, @DataSet, @UnitID, @ProcessID, @TAID, @TACurrency, @TADate, @NextTADate, @RestartDate, @TACostLocal, @TAMatlLocal, @AnnTACostLocal, @AnnTAMatlLocal

DECLARE @RptCurrency CurrencyCode, @ConvRate real
DECLARE cCurrencies CURSOR FOR
SELECT Currency FROM CurrenciesToCalc WHERE RefineryID = @RefineryID AND Currency <> @TACurrency
OPEN cCurrencies
FETCH NEXT FROM cCurrencies INTO @RptCurrency
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @ConvRate = dbo.ExchangeRate(@TACurrency, @RptCurrency, @TADate)
	INSERT INTO MaintTACost (RefineryID, DataSet, UnitID, ProcessID, TAID, Currency, TADate, NextTADate, RestartDate,
		TACost, TAMatl, AnnTACost, AnnTAMatl)
	SELECT 	@RefineryID, @DataSet, @UnitID, @ProcessID, @TAID, @RptCurrency, @TADate, @NextTADate, @RestartDate,
		@TACostLocal*@ConvRate, @TAMatlLocal*@ConvRate, @AnnTACostLocal*@ConvRate, @AnnTAMatlLocal*@ConvRate
	FETCH NEXT FROM cCurrencies INTO @RptCurrency
END
CLOSE cCurrencies
DEALLOCATE cCurrencies




