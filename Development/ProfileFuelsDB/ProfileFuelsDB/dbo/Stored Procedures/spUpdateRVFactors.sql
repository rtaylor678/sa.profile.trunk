﻿CREATE PROC spUpdateRVFactors(@RefineryID char(6), @EffDate smalldatetime, @RVLocFactor real, @InflFactor real)
AS
	UPDATE RefineryFactors
	SET EffUntil = @EffDate
	WHERE RefineryID = @RefineryID AND EffDate > @EffDate

	INSERT INTO RefineryFactors (RefineryID, EffDate, RVLocFactor, InflFactor)
	VALUES (@RefineryID, @EffDate, @RVLocFactor, @InflFactor)

