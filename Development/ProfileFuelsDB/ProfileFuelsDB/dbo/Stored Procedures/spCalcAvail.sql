﻿


CREATE      PROC [dbo].[spCalcAvail](@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @PeriodHrs int, @RefineryID varchar(6), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @DataSet varchar(15)
SELECT @RefineryID = RefineryID, @PeriodHrs = DATEDIFF(hh, PeriodStart, PeriodEnd), @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @DataSet = DataSet
FROM Submissions WHERE SubmissionID = @SubmissionID
UPDATE MaintRout
SET PeriodHrs = @PeriodHrs
WHERE SubmissionID = @SubmissionID
UPDATE MaintRout
SET PeriodHrs = PeriodHrs * c.InservicePcnt/100
FROM MaintRout INNER JOIN Config c 
ON c.SubmissionID = MaintRout.SubmissionID AND c.UnitID = MaintRout.UnitID
WHERE MaintRout.SubmissionID = @SubmissionID AND c.InservicePcnt <> 100
DELETE FROM MaintCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaintProcess WHERE SubmissionID = @SubmissionID
DELETE FROM MaintAvailCalc WHERE SubmissionID = @SubmissionID
INSERT INTO MaintCalc(SubmissionID, UnitID, ProcessID, PeriodHrs)
SELECT SubmissionID, UnitID, ProcessID, PeriodHrs
FROM MaintRout WHERE SubmissionID = @SubmissionID
DECLARE @UnitID int, @MechUnavailTA_Act real, @MechAvailOSTA real, @PeriodHrsOSTA real,
	@MechAvail_Act real, @MechAvailSlow_Act real, @OpAvail_Act real,
	@OpAvailSlow_Act real, @OnStream_Act real, @OnStreamSlow_Act real, 
	@MechUnavailTA_Ann real, @MechAvail_Ann real, @MechAvailSlow_Ann real, 
	@OpAvail_Ann real, @OpAvailSlow_Ann real, @OnStream_Ann real, @OnStreamSlow_Ann real,
	@MechUnavailPlan real, @MechUnavailUnp real, @MechUnavail_Ann real, @MechUnavail_Act real, 
	@RegUnavail real, @RegUnavailPlan real, @RegUnavailUnp real, @OpUnavail_Ann real, @OpUnavail_Act real,
	@OthUnavailEconomic real, @OthUnavailExternal real, @OthUnavailUnitUpsets real, @OthUnavailOffsiteUpsets real, @OthUnavailOther real, 
	@OthUnavail real, @OthUnavailPlan real, @OthUnavailUnp real, @TotUnavail_Ann real, @TotUnavail_Act real, @TotUnavailUnp real

DECLARE @cAvail CURSOR
EXEC spCalcUnitAvail @RefineryID, @DataSet, @PeriodStart, @PeriodEnd, @avail_cursor = @cAvail OUTPUT
FETCH NEXT FROM @cAvail INTO @UnitID, @PeriodHrs, @PeriodHrsOSTA, 
		@MechUnavailTA_Act, @MechAvailOSTA, @MechAvail_Act, @MechAvailSlow_Act, @OpAvail_Act,
		@OpAvailSlow_Act, @OnStream_Act, @OnStreamSlow_Act, @MechUnavailTA_Ann, @MechAvail_Ann, @MechAvailSlow_Ann, 
		@OpAvail_Ann, @OpAvailSlow_Ann, @OnStream_Ann, @OnStreamSlow_Ann,
		@MechUnavailPlan, @MechUnavailUnp, @MechUnavail_Ann, @MechUnavail_Act, 
		@RegUnavail, @RegUnavailPlan, @RegUnavailUnp, @OpUnavail_Ann, @OpUnavail_Act,
		@OthUnavailEconomic, @OthUnavailExternal, @OthUnavailUnitUpsets, @OthUnavailOffsiteUpsets, @OthUnavailOther, 
		@OthUnavail, @OthUnavailPlan, @OthUnavailUnp, @TotUnavail_Ann, @TotUnavail_Act, @TotUnavailUnp 
WHILE (@@FETCH_STATUS = 0)
BEGIN
	UPDATE MaintCalc
	SET 	MechUnavailTA_Act = @MechUnavailTA_Act, MechAvailOSTA = @MechAvailOSTA, 
		MechAvail_Act = @MechAvail_Act, MechAvailSlow_Act = @MechAvailSlow_Act, 
		OpAvail_Act = @OpAvail_Act, OpAvailSlow_Act = @OpAvailSlow_Act, 
		OnStream_Act = @OnStream_Act, OnStreamSlow_Act = @OnStreamSlow_Act,
		MechUnavailTA_Ann = @MechUnavailTA_Ann, PeriodHrsOSTA = @PeriodHrsOSTA,
		MechAvail_Ann = @MechAvail_Ann, MechAvailSlow_Ann = @MechAvailSlow_Ann, 
		OpAvail_Ann = @OpAvail_Ann, OpAvailSlow_Ann = @OpAvailSlow_Ann, 
		OnStream_Ann = @OnStream_Ann, OnStreamSlow_Ann = @OnStreamSlow_Ann,
		MechUnavailPlan	= @MechUnavailPlan, MechUnavailUnp = @MechUnavailUnp,
		MechUnavail_Ann	= @MechUnavail_Ann, MechUnavail_Act = @MechUnavail_Act,
		RegUnavail = @RegUnavail, RegUnavailPlan = @RegUnavailPlan, RegUnavailUnp = @RegUnavailUnp,
		OpUnavail_Ann = @OpUnavail_Ann, OpUnavail_Act = @OpUnavail_Act,
		OthUnavailEconomic = @OthUnavailEconomic, OthUnavailExternal = @OthUnavailExternal,
		OthUnavailUnitUpsets = @OthUnavailUnitUpsets, OthUnavailOffsiteUpsets = @OthUnavailOffsiteUpsets,
		OthUnavailOther = @OthUnavailOther, 
		OthUnavail = @OthUnavail, OthUnavailPlan = @OthUnavailPlan, OthUnavailUnp = @OthUnavailUnp,
		TotUnavail_Ann = @TotUnavail_Ann, TotUnavail_Act = @TotUnavail_Act, TotUnavailUnp = @TotUnavailUnp
	WHERE SubmissionID = @SubmissionID AND UnitID = @UnitID
	FETCH NEXT FROM @cAvail INTO @UnitID, @PeriodHrs, @PeriodHrsOSTA, 
		@MechUnavailTA_Act, @MechAvailOSTA, @MechAvail_Act, @MechAvailSlow_Act, @OpAvail_Act,
		@OpAvailSlow_Act, @OnStream_Act, @OnStreamSlow_Act, @MechUnavailTA_Ann, @MechAvail_Ann, @MechAvailSlow_Ann, 
		@OpAvail_Ann, @OpAvailSlow_Ann, @OnStream_Ann, @OnStreamSlow_Ann,
		@MechUnavailPlan, @MechUnavailUnp, @MechUnavail_Ann, @MechUnavail_Act, 
		@RegUnavail, @RegUnavailPlan, @RegUnavailUnp, @OpUnavail_Ann, @OpUnavail_Act,
		@OthUnavailEconomic, @OthUnavailExternal, @OthUnavailUnitUpsets, @OthUnavailOffsiteUpsets, @OthUnavailOther, 
		@OthUnavail, @OthUnavailPlan, @OthUnavailUnp, @TotUnavail_Ann, @TotUnavail_Act, @TotUnavailUnp 
END
CLOSE @cAvail
DEALLOCATE @cAvail

INSERT INTO MaintProcess(SubmissionID, FactorSet, ProcessID, MechUnavailTA_Ann, MechUnavailTA_Act, MechAvail_Ann, MechAvail_Act, MechAvailSlow_Ann, MechAvailSlow_Act, MechAvailOSTA, OpAvail_Ann, OpAvail_Act, OpAvailSlow_Ann, OpAvailSlow_Act, OnStream_Ann, OnStream_Act, OnStreamSlow_Ann, OnStreamSlow_Act,
MechUnavailPlan, MechUnavailUnp, MechUnavail_Ann, MechUnavail_Act, RegUnavail, RegUnavailPlan, RegUnavailUnp, OpUnavail_Ann, OpUnavail_Act,
OthUnavailEconomic, OthUnavailExternal, OthUnavailUnitUpsets, OthUnavailOffsiteUpsets, OthUnavailOther, OthUnavail, OthUnavailPlan, OthUnavailUnp,
TotUnavail_Ann, TotUnavail_Act, TotUnavailUnp)
SELECT m.SubmissionID, f.FactorSet, m.ProcessID, 
SUM(m.MechUnavailTA_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.MechUnavailTA_Act*f.EDCNoMult)/SUM(f.EDCNoMult),
SUM(m.MechAvail_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.MechAvail_Act*f.EDCNoMult)/SUM(f.EDCNoMult),
SUM(m.MechAvailSlow_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.MechAvailSlow_Act*f.EDCNoMult)/SUM(f.EDCNoMult),
SUM(m.MechAvailOSTA*f.EDCNoMult)/SUM(f.EDCNoMult), 
SUM(m.OpAvail_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OpAvail_Act*f.EDCNoMult)/SUM(f.EDCNoMult),
SUM(m.OpAvailSlow_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OpAvailSlow_Act*f.EDCNoMult)/SUM(f.EDCNoMult),
SUM(m.OnStream_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OnStream_Act*f.EDCNoMult)/SUM(f.EDCNoMult),
SUM(m.OnStreamSlow_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OnStreamSlow_Act*f.EDCNoMult)/SUM(f.EDCNoMult),
SUM(m.MechUnavailPlan*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.MechUnavailUnp*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.MechUnavail_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.MechUnavail_Act*f.EDCNoMult)/SUM(f.EDCNoMult), 
SUM(m.RegUnavail*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.RegUnavailPlan*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.RegUnavailUnp*f.EDCNoMult)/SUM(f.EDCNoMult), 
SUM(m.OpUnavail_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OpUnavail_Act*f.EDCNoMult)/SUM(f.EDCNoMult), 
SUM(m.OthUnavailEconomic*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OthUnavailExternal*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OthUnavailUnitUpsets*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OthUnavailOffsiteUpsets*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OthUnavailOther*f.EDCNoMult)/SUM(f.EDCNoMult), 
SUM(m.OthUnavail*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OthUnavailPlan*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.OthUnavailUnp*f.EDCNoMult)/SUM(f.EDCNoMult), 
SUM(m.TotUnavail_Ann*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.TotUnavail_Act*f.EDCNoMult)/SUM(f.EDCNoMult), SUM(m.TotUnavailUnp*f.EDCNoMult)/SUM(f.EDCNoMult)
FROM MaintCalc m INNER JOIN FactorCalc f ON m.SubmissionID = f.SubmissionID AND m.UnitID = f.UnitID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = m.ProcessID
WHERE m.SubmissionID = @SubmissionID AND lu.MaintDetails = 'Y' AND f.EDCNoMult > 0
GROUP BY m.SubmissionID, f.FactorSet, m.ProcessID

INSERT INTO MaintAvailCalc(SubmissionID, FactorSet, MechUnavailTA_Ann, MechUnavailTA_Act, MechAvail_Ann, MechAvail_Act, MechAvailSlow_Ann, MechAvailSlow_Act, MechAvailOSTA, OpAvail_Ann, OpAvail_Act, OpAvailSlow_Ann, OpAvailSlow_Act, OnStream_Ann, OnStream_Act, OnStreamSlow_Ann, OnStreamSlow_Act,
	OthProcessMechUnavailTA_Ann, OthProcessMechAvail_Ann, OthProcessMechAvailSlow_Ann, OthProcessMechAvailOSTA, OthProcessOpAvail_Ann, OthProcessOpAvailSlow_Ann, OthProcessOnStream_Ann, OthProcessOnStreamSlow_Ann, 
	OthProcessMechUnavailTA_Act, OthProcessMechAvail_Act, OthProcessMechAvailSlow_Act, OthProcessOpAvail_Act, OthProcessOpAvailSlow_Act, OthProcessOnStream_Act, OthProcessOnStreamSlow_Act)
SELECT m.SubmissionID, f.FactorSet, 
SUM(m.MechUnavailTA_Ann*f.PlantEDC)/SUM(f.PlantEDC), SUM(m.MechUnavailTA_Act*f.PlantEDC)/SUM(f.PlantEDC),
SUM(m.MechAvail_Ann*f.PlantEDC)/SUM(f.PlantEDC), SUM(m.MechAvail_Act*f.PlantEDC)/SUM(f.PlantEDC),
SUM(m.MechAvailSlow_Ann*f.PlantEDC)/SUM(f.PlantEDC), SUM(m.MechAvailSlow_Act*f.PlantEDC)/SUM(f.PlantEDC),
SUM(m.MechAvailOSTA*f.PlantEDC)/SUM(f.PlantEDC), 
SUM(m.OpAvail_Ann*f.PlantEDC)/SUM(f.PlantEDC), SUM(m.OpAvail_Act*f.PlantEDC)/SUM(f.PlantEDC),
SUM(m.OpAvailSlow_Ann*f.PlantEDC)/SUM(f.PlantEDC), SUM(m.OpAvailSlow_Act*f.PlantEDC)/SUM(f.PlantEDC),
SUM(m.OnStream_Ann*f.PlantEDC)/SUM(f.PlantEDC), SUM(m.OnStream_Act*f.PlantEDC)/SUM(f.PlantEDC),
SUM(m.OnStreamSlow_Ann*f.PlantEDC)/SUM(f.PlantEDC), SUM(m.OnStreamSlow_Act*f.PlantEDC)/SUM(f.PlantEDC),
-- OthProcessMechUnavailTA_Ann
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.MechUnavailTA_Ann*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessMechAvail_Ann
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.MechAvail_Ann*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessMechAvailSlow_Ann
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.MechAvailSlow_Ann*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessMechAvailOSTA
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.MechAvailOSTA*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOpAvail_Ann
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OpAvail_Ann*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOpAvailSlow_Ann
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OpAvailSlow_Ann*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOnStream_Ann
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OnStream_Ann*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOnStreamSlow_Ann
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OnStreamSlow_Ann*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessMechUnavailTA_Act
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.MechUnavailTA_Act*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessMechAvail_Act
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.MechAvail_Act*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessMechAvailSlow_Act
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.MechAvailSlow_Act*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOpAvail_Act
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OpAvail_Act*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOpAvailSlow_Act
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OpAvailSlow_Act*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOnStream_Act
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OnStream_Act*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END,
-- OthProcessOnStreamSlow_Act
CASE WHEN SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) > 0 THEN 
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN m.OnStreamSlow_Act*f.PlantEDC ELSE 0 END)/
 SUM(CASE WHEN lu.ProcessGroup = 'O' THEN f.PlantEDC ELSE 0 END) ELSE NULL END
FROM MaintProcess m INNER JOIN FactorProcessCalc f ON m.SubmissionID = f.SubmissionID AND m.ProcessID = f.ProcessID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = m.ProcessID
WHERE m.SubmissionID = @SubmissionID AND f.PlantEDC > 0 AND lu.ProcessGroup IN ('P','F','L','W','O')
GROUP BY m.SubmissionID, f.FactorSet

