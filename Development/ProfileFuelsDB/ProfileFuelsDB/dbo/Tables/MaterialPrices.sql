﻿CREATE TABLE [dbo].[MaterialPrices] (
    [RefineryID]  CHAR (6)           NOT NULL,
    [StudyYear]   [dbo].[StudyYear]  NOT NULL,
    [MaterialID]  [dbo].[MaterialID] NOT NULL,
    [PricePerBbl] REAL               NOT NULL,
    CONSTRAINT [PK_MaterialPrices] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [StudyYear] ASC, [MaterialID] ASC) WITH (FILLFACTOR = 90)
);

