﻿CREATE TABLE [dbo].[ProcessGroup_LU] (
    [ProcessGroup] CHAR (1)     NOT NULL,
    [Description]  VARCHAR (50) NOT NULL,
    [SortKey]      TINYINT      NULL,
    CONSTRAINT [PK_ProcessGroup_LU] PRIMARY KEY CLUSTERED ([ProcessGroup] ASC) WITH (FILLFACTOR = 90)
);

