﻿CREATE TABLE [dbo].[StudyValues] (
    [RefineryID]   CHAR (6)      NOT NULL,
    [EffDate]      SMALLDATETIME NOT NULL,
    [EffUntil]     SMALLDATETIME NOT NULL,
    [Property]     VARCHAR (50)  NOT NULL,
    [StudyValue]   REAL          NULL,
    [StudyP1Value] REAL          NULL,
    CONSTRAINT [PK_StudyValues] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [EffDate] ASC, [EffUntil] ASC, [Property] ASC) WITH (FILLFACTOR = 90)
);

