﻿CREATE TABLE [dbo].[StudyFCCCOBoilers] (
    [RefineryID] CHAR (6)      NOT NULL,
    [EffDate]    SMALLDATETIME NOT NULL,
    [EffUntil]   SMALLDATETIME NOT NULL,
    [UnitID]     INT           NOT NULL,
    [FiredDuty]  REAL          NULL,
    [Efficiency] REAL          NULL,
    [Cap]        REAL          NULL,
    CONSTRAINT [PK_StudyFCCCOBoilers] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [EffDate] ASC, [EffUntil] ASC, [UnitID] ASC) WITH (FILLFACTOR = 90)
);

