﻿CREATE TABLE [dbo].[MaterialTot] (
    [SubmissionID] INT        NOT NULL,
    [PVP]          FLOAT (53) NULL,
    [PVPPcnt]      REAL       NULL,
    [TotInputBbl]  FLOAT (53) NULL,
    [TotInputMT]   FLOAT (53) NULL,
    [NetInputBbl]  FLOAT (53) NULL,
    [NetInputMT]   FLOAT (53) NULL,
    [GainBbl]      REAL       NULL,
    CONSTRAINT [PK_MaterialTot] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);

