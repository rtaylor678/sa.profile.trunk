﻿CREATE TABLE [dbo].[ProcessTotCalc] (
    [SubmissionID]    INT               NOT NULL,
    [ProcessID]       [dbo].[ProcessID] NOT NULL,
    [NumUnits]        TINYINT           NULL,
    [Cap]             REAL              NULL,
    [UtilCap]         REAL              NULL,
    [StmCap]          REAL              NULL,
    [StmUtilCap]      REAL              NULL,
    [InServiceCap]    REAL              NULL,
    [InserviceStmCap] REAL              NULL,
    [ActualCap]       REAL              NULL,
    [ActualStmCap]    REAL              NULL,
    [AllocPcntOfTime] REAL              NULL,
    [ActualMFCap]     REAL              NULL,
    [MFCap]           REAL              NULL,
    [MFUtilCap]       REAL              NULL,
    [InserviceMFCap]  REAL              NULL,
    CONSTRAINT [PK_ProcessTotCalc] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [ProcessID] ASC) WITH (FILLFACTOR = 90)
);

