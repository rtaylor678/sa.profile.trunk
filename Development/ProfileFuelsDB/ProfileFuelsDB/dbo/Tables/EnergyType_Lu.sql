﻿CREATE TABLE [dbo].[EnergyType_Lu] (
    [EnergyType]    [dbo].[EnergyType] NOT NULL,
    [Description]   CHAR (35)          NOT NULL,
    [ProductID]     [dbo].[MaterialID] NULL,
    [MBTUPerBbl]    REAL               NULL,
    [MaterialID]    [dbo].[MaterialID] NULL,
    [SortKey]       TINYINT            NULL,
    [ResidGroup]    CHAR (1)           NULL,
    [CarbonContent] REAL               NULL,
    [OpexGroup]     CHAR (3)           NULL,
    [FOEBPerMT]     REAL               NULL,
    [BTUPerLb]      REAL               NULL,
    [CO_MTperMBTU]  REAL               NULL,
    CONSTRAINT [PK_EnergyType_Lu_1__14] PRIMARY KEY CLUSTERED ([EnergyType] ASC) WITH (FILLFACTOR = 90)
);

