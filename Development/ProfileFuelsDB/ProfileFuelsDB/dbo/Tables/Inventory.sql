﻿CREATE TABLE [dbo].[Inventory] (
    [SubmissionID] INT        NOT NULL,
    [TankType]     CHAR (3)   NOT NULL,
    [SortKey]      TINYINT    NOT NULL,
    [TotStorage]   FLOAT (53) NULL,
    [MktgStorage]  FLOAT (53) NULL,
    [MandStorage]  FLOAT (53) NULL,
    [FuelsStorage] FLOAT (53) NULL,
    [NumTank]      SMALLINT   NULL,
    [LeasedPcnt]   REAL       NULL,
    [AvgLevel]     REAL       NULL,
    [MandLevel]    REAL       NULL,
    [Inven]        REAL       NULL,
    CONSTRAINT [PK_Inventory_1] PRIMARY KEY NONCLUSTERED ([SubmissionID] ASC, [TankType] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE CLUSTERED INDEX [InventorySort_1]
    ON [dbo].[Inventory]([SubmissionID] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90);

