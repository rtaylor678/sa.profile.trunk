﻿CREATE TABLE [dbo].[LoadTA] (
    [RefineryID]   CHAR (6)             NOT NULL,
    [DataSet]      VARCHAR (15)         NOT NULL,
    [UnitID]       [dbo].[UnitID]       NOT NULL,
    [TAID]         INT                  NOT NULL,
    [SchedTAInt]   REAL                 NULL,
    [TADate]       SMALLDATETIME        NULL,
    [TAHrsDown]    REAL                 NULL,
    [TACostLocal]  REAL                 NULL,
    [TAMatlLocal]  REAL                 NULL,
    [TAOCCSTH]     REAL                 NULL,
    [TAOCCOVT]     REAL                 NULL,
    [TAMPSSTH]     REAL                 NULL,
    [TAMPSOVTPcnt] REAL                 NULL,
    [TAContOCC]    REAL                 NULL,
    [TAContMPS]    REAL                 NULL,
    [PrevTADate]   SMALLDATETIME        NULL,
    [TAExceptions] SMALLINT             NULL,
    [TACurrency]   [dbo].[CurrencyCode] NULL,
    [ProcessID]    [dbo].[ProcessID]    NULL,
    CONSTRAINT [PK_LoadTA] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [DataSet] ASC, [UnitID] ASC, [TAID] ASC) WITH (FILLFACTOR = 90)
);

