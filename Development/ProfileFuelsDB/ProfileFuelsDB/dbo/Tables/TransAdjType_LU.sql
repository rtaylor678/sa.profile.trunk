﻿CREATE TABLE [dbo].[TransAdjType_LU] (
    [AdjType]     CHAR (2)  NOT NULL,
    [Description] CHAR (25) NULL,
    [AdjMethod]   CHAR (1)  CONSTRAINT [DF__TransAdjT__AdjMe__4DFE9BEF] DEFAULT ('+') NOT NULL,
    CONSTRAINT [PK_APIAdjType_LU_1__14] PRIMARY KEY CLUSTERED ([AdjType] ASC) WITH (FILLFACTOR = 90)
);

