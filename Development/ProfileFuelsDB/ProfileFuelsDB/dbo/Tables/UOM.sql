﻿CREATE TABLE [dbo].[UOM] (
    [UOMCode]  VARCHAR (20) NOT NULL,
    [UOMDesc]  VARCHAR (50) NOT NULL,
    [UOMGroup] VARCHAR (20) NOT NULL,
    [Factor]   REAL         NULL,
    CONSTRAINT [PK_UOM] PRIMARY KEY CLUSTERED ([UOMCode] ASC) WITH (FILLFACTOR = 90)
);

