﻿CREATE TABLE [dbo].[MaterialTotCost] (
    [SubmissionID]           INT                  NOT NULL,
    [Scenario]               [dbo].[Scenario]     NOT NULL,
    [Currency]               [dbo].[CurrencyCode] NOT NULL,
    [RawMatCost]             REAL                 NULL,
    [ProdValue]              REAL                 NULL,
    [ThirdPartyTerminalRM]   REAL                 NULL,
    [ThirdPartyTerminalProd] REAL                 NULL,
    [POXO2]                  REAL                 NULL,
    CONSTRAINT [PK_MatTotCost] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [Currency] ASC) WITH (FILLFACTOR = 90)
);

