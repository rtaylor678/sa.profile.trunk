﻿CREATE TABLE [dbo].[Factors] (
    [FactorSet]               [dbo].[FactorSet]   NOT NULL,
    [ProcessID]               [dbo].[ProcessID]   NOT NULL,
    [ProcessType]             [dbo].[ProcessType] NOT NULL,
    [CogenES]                 CHAR (1)            CONSTRAINT [DF_Factors_CogenES] DEFAULT ('') NOT NULL,
    [EDCConstant]             REAL                NULL,
    [EDCExponent]             REAL                NULL,
    [EDCMinCap]               REAL                NULL,
    [EDCMaxCap]               REAL                NULL,
    [MultGroup]               [dbo].[ProcessID]   NULL,
    [RVConstant]              REAL                NOT NULL,
    [RVExponent]              REAL                NOT NULL,
    [RVNomCap]                REAL                NOT NULL,
    [SulfurAdj]               CHAR (1)            CONSTRAINT [DF_Factors_SulfurAdj] DEFAULT ('N') NOT NULL,
    [BaseSulfur]              REAL                NULL,
    [DataTable]               VARCHAR (30)        NULL,
    [EIIFormula]              VARCHAR (255)       NULL,
    [VEIFormula]              VARCHAR (255)       NULL,
    [UnitConvPcntFormula]     VARCHAR (255)       NULL,
    [EIIFormulaForReport]     VARCHAR (1000)      NULL,
    [VEIFormulaForReport]     VARCHAR (1000)      NULL,
    [NEOpexEffConstant]       REAL                NULL,
    [NEOpexEffExponent]       REAL                NULL,
    [NEOpexEffMinCap]         REAL                NULL,
    [NEOpexEffMaxCap]         REAL                NULL,
    [MaintEffConstant]        REAL                NULL,
    [MaintEffExponent]        REAL                NULL,
    [MaintEffMinCap]          REAL                NULL,
    [MaintEffMaxCap]          REAL                NULL,
    [PersEffConstant]         REAL                NULL,
    [PersEffExponent]         REAL                NULL,
    [PersEffMinCap]           REAL                NULL,
    [PersEffMaxCap]           REAL                NULL,
    [EIICheck]                VARCHAR (255)       NULL,
    [EIIDefault]              REAL                NULL,
    [EIIDefaultToYear]        SMALLINT            NULL,
    [VEICheck]                VARCHAR (255)       NULL,
    [VEIDefault]              REAL                NULL,
    [VEIDefaultToYear]        SMALLINT            NULL,
    [FormulaSymbols]          VARCHAR (50)        NULL,
    [NonMaintPersEffConstant] REAL                NULL,
    [NonMaintPersEffExponent] REAL                NULL,
    [NonMaintPersEffMinCap]   REAL                NULL,
    [NonMaintPersEffMaxCap]   REAL                NULL,
    [MaintPersEffConstant]    REAL                NULL,
    [MaintPersEffExponent]    REAL                NULL,
    [MaintPersEffMinCap]      REAL                NULL,
    [MaintPersEffMaxCap]      REAL                NULL,
    CONSTRAINT [PK_Factors] PRIMARY KEY CLUSTERED ([FactorSet] ASC, [ProcessID] ASC, [ProcessType] ASC, [CogenES] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE TRIGGER updProcessIDLUSulfurAdj ON dbo.Factors 
FOR INSERT, UPDATE, DELETE 
AS
UPDATE ProcessID_LU
SET SulfurAdj = CASE WHEN (EXISTS (SELECT * FROM Factors WHERE SulfurAdj = 'Y' AND BaseSulfur > 0 AND Factors.ProcessID=ProcessID_LU.ProcessID)) THEN 'Y'  ELSE 'N' END
