﻿CREATE TABLE [dbo].[MaterialSumCalc] (
    [SubmissionID]      INT              NOT NULL,
    [Scenario]          [dbo].[Scenario] NOT NULL,
    [SumCat]            CHAR (4)         NOT NULL,
    [OTHRMBBL]          REAL             NULL,
    [OTHRMMT]           REAL             NULL,
    [OTHRMPrice]        REAL             NULL,
    [OTHRMValue]        REAL             NULL,
    [OTHRMNetInputPcnt] REAL             NULL,
    [MPRODBBL]          REAL             NULL,
    [MPRODMT]           REAL             NULL,
    [MPRODPrice]        REAL             NULL,
    [MPRODValue]        REAL             NULL,
    [MPRODNetInputPcnt] REAL             NULL,
    CONSTRAINT [PK_MaterialSumCalc_1__18] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [SumCat] ASC) WITH (FILLFACTOR = 90)
);

