﻿CREATE TABLE [dbo].[Company_LU] (
    [CompanyID]    VARCHAR (10) NOT NULL,
    [CompanyName]  VARCHAR (50) NOT NULL,
    [CompanyLogin] VARCHAR (50) NOT NULL,
    [CompanyPwd]   VARCHAR (50) NOT NULL,
    [PwdSaltKey]   VARCHAR (50) CONSTRAINT [DF_Company_LU_PwdSaltKey] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_Company_LU] PRIMARY KEY CLUSTERED ([CompanyID] ASC) WITH (FILLFACTOR = 90)
);

