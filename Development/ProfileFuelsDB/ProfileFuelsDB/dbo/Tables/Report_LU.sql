﻿CREATE TABLE [dbo].[Report_LU] (
    [ReportCode]  CHAR (10)    NOT NULL,
    [ReportName]  VARCHAR (50) NOT NULL,
    [SortKey]     SMALLINT     NOT NULL,
    [CustomGroup] TINYINT      CONSTRAINT [DF_Report_LU_ReportGroup] DEFAULT (0) NOT NULL,
    [Template]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Report_LU] PRIMARY KEY CLUSTERED ([ReportCode] ASC, [CustomGroup] ASC) WITH (FILLFACTOR = 90)
);

