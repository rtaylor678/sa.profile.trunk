﻿CREATE FUNCTION [dbo].[AvgProcessEffIndex](@RefineryID varchar(6), @Dataset varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @FactorSet smalldatetime)
RETURNS real
AS
BEGIN
	DECLARE @EstGain real, @ReportLossGain real, @PEI real, @NetInputBbl real
	SELECT 	@EstGain = SUM(ftc.EstGain), @NetInputBbl = SUM(mt.NetInputBbl), @ReportLossGain = SUM(mt.GainBbl)
	FROM Submissions s INNER JOIN FactorTotCalc ftc ON ftc.SubmissionID = s.SubmissionID
	INNER JOIN MaterialTot mt ON mt.SubmissionID = s.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @Dataset
	AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
	AND ftc.FactorSet = @FactorSet AND mt.NetInputBbl > 0

	IF @NetInputBbl>0 
		SELECT @PEI = 100*(@NetInputBbl+@ReportLossGain) / (@NetInputBbl+@EstGain)
	ELSE
		SELECT @PEI = NULL
	
	RETURN @PEI
END


