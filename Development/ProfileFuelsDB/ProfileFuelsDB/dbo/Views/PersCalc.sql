﻿



CREATE    VIEW [PersCalc] AS
SELECT e.SubmissionID, e.FactorSet, p.PersID, Description = (SELECT Description FROM Pers_LU WHERE Pers_LU.PersID = p.PersID), 
p.SortKey, p.SectionID, 
p.CompEqP, p.CompEqP*100000/PlantEDC AS CompEqPEDC,
p.ContEqP, p.ContEqP*100000/PlantEDC AS ContEqPEDC, 
p.GAEqP, p.GAEqP*100000/PlantEDC AS GAEqPEDC,
p.TotEqP, p.TotEqP*100000/PlantEDC AS TotEqPEDC, 
p.CompWHr, p.CompWHr*100/(PlantEDC*s.FractionOfYear) AS CompWHrEDC, 
p.ContWHr, p.ContWHr*100/(PlantEDC*s.FractionOfYear) AS ContWHrEDC,
p.GAWHr, p.GAWHr*100/(PlantEDC*s.FractionOfYear) AS GAWHrEDC, 
p.TotWHr, p.TotWHr*100/(PlantEDC*s.FractionOfYear) AS TotWHrEDC,
p.NumPers, p.STH, p.OVTHours, p.OVTPcnt, p.Contract, p.GA, p.AbsHrs
FROM Pers p INNER JOIN FactorTotCalc e ON e.SubmissionID = p.SubmissionID 
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID
WHERE e.PlantEDC>0




