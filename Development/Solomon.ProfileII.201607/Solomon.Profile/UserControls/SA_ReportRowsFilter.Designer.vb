﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SA_ReportRowsFilter
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnReportRowsFilterGetReport = New System.Windows.Forms.Button()
        Me.btnReportRowsFilterSelectAll = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnReportRowsFilterGetReport
        '
        Me.btnReportRowsFilterGetReport.Location = New System.Drawing.Point(95, 34)
        Me.btnReportRowsFilterGetReport.Name = "btnReportRowsFilterGetReport"
        Me.btnReportRowsFilterGetReport.Size = New System.Drawing.Size(75, 23)
        Me.btnReportRowsFilterGetReport.TabIndex = 0
        Me.btnReportRowsFilterGetReport.Text = "Get Report"
        Me.btnReportRowsFilterGetReport.UseVisualStyleBackColor = True
        '
        'btnReportRowsFilterSelectAll
        '
        Me.btnReportRowsFilterSelectAll.Location = New System.Drawing.Point(3, 34)
        Me.btnReportRowsFilterSelectAll.Name = "btnReportRowsFilterSelectAll"
        Me.btnReportRowsFilterSelectAll.Size = New System.Drawing.Size(75, 23)
        Me.btnReportRowsFilterSelectAll.TabIndex = 1
        Me.btnReportRowsFilterSelectAll.Text = "Select All"
        Me.btnReportRowsFilterSelectAll.UseVisualStyleBackColor = True
        '
        'SA_ReportRowsFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.Controls.Add(Me.btnReportRowsFilterSelectAll)
        Me.Controls.Add(Me.btnReportRowsFilterGetReport)
        Me.Name = "SA_ReportRowsFilter"
        Me.Size = New System.Drawing.Size(548, 591)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnReportRowsFilterGetReport As System.Windows.Forms.Button
    Friend WithEvents btnReportRowsFilterSelectAll As System.Windows.Forms.Button

End Class
