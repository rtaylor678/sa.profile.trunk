Option Explicit On
Imports System.Configuration
Imports Solomon.ProfileII.Contracts
Imports Solomon.ProfileII.Proxies

Friend Class LookupTablesQueries
    'Inherits DataServiceExtension


#Region "Reference Queries"
    Friend Shadows Function GetReferences(refineryID As String, companyID As String) As DataSet

        If AppCertificate.IsCertificateInstall() Then

            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Dim ds As New DataSet()

            If Util.UseNonWCFWebService Then
                'Me.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                ''End If
                'Dim b As New Solomon.Query12.DataServices2
                'b.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                'Dim ds As DataSet = b.GetReferences(GetClientKey())
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                Try
                    'Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                    ds = proxy.GetReferences(Nothing)
                    'ds = svc.GetReferences(GetClientKey())
                Catch ex As Exception
                    Throw ex
                Finally
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() 
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                End Try
            End If
            Return ds

        Else
            Throw New Exception("Remote service call can not invoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function

    Friend Function GetRefineryData(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, _
                                    ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, _
                                    ByVal studyYear As Integer, ByVal includeTarget As Boolean, _
                                    ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, refineryID As String) As DataSet
        If AppCertificate.IsCertificateInstall() Then
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then

            Dim dset As New DataSet()

            If Util.UseNonWCFWebService Then
                'Me.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                ''End If
                'Dim b As New Solomon.Query12.DataServices2
                'b.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                'Dim dset As DataSet = b.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, refineryID)

            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                Try
                    'Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                    dset = proxy.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, Nothing)
                    'dset = svc.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, refineryID)
                Catch ex As Exception
                    Throw ex
                Finally
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                End Try
            End If

            Return dset
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function

#End Region


#Region "Test Code"
    Friend Shared Sub Main(refineryID As String, companyID As String)

        If Util.UseNonWCFWebService Then
            'Dim service As New LookupTablesQueries
            'service.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
            'Dim lookups As DataSet = service.GetLookups(GetClientKey(), companyID)

            'Dim y As Integer
            'For y = 0 To lookups.Tables.Count - 1
            '    Console.WriteLine(lookups.Tables(y).TableName + "," + lookups.Tables(y).Rows.Count.ToString)
            'Next
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim result As String = String.Empty
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            Try
                Dim lookups As DataSet = proxy.GetLookups(GetClientKey(), Nothing)
                'Dim lookups As DataSet = svc.GetLookups(GetClientKey(), companyID)
                Dim y As Integer
                For y = 0 To lookups.Tables.Count - 1
                    Console.WriteLine(lookups.Tables(y).TableName + "," + lookups.Tables(y).Rows.Count.ToString)
                Next
            Catch ex As Exception
                Throw ex
            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If
    End Sub
#End Region



End Class
