
Option Explicit On

Imports System.Configuration
Imports Solomon.ProfileII.Contracts
Imports Solomon.ProfileII.Proxies

Friend Class RefTablesQueries
    'Inherits DataServiceExtension


#Region "Reference Queries"
    Friend Shadows Function GetReferences(clientKey As String) As DataSet

        If AppCertificate.IsCertificateInstall() Then
            Dim ds As New DataSet()
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then


            If Util.UseNonWCFWebService Then
                'Me.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                ''End If
                'Dim b As New Solomon.Query12.DataServices2
                'b.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                'Dim ds As DataSet = b.GetReferences(clientKey)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try
                    ds = proxy.GetReferences(Nothing)
                    'ds = svc.GetReferences(clientKey)
                Catch ex As Exception
                    Throw ex
                Finally
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                End Try
            End If

            Return ds
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function

    Friend Function GetRefineryData(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, _
                                    ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, _
                                    ByVal studyYear As Integer, ByVal includeTarget As Boolean, _
                                    ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, refineryID As String) As DataSet
        If AppCertificate.IsCertificateInstall() Then
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Dim dset As New DataSet()

            If Util.UseNonWCFWebService Then
                'Me.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                ''End If
                'Dim b As New Solomon.Query12.DataServices2
                'b.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                'Dim dset As DataSet = b.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, GetClientKey())
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try
                    dset = proxy.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, Nothing)
                    'dset = svc.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, GetClientKey())
                Catch ex As Exception
                    Throw ex
                Finally
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                End Try
            End If
            Return dset
        Else
            Throw New Exception("Remote service call can not invoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function
#End Region

#Region "Test Code"
    Friend Shared Sub Main(refineryID As String)
        Dim service As New RefTablesQueries
        Dim refs As DataSet = service.GetReferences(refineryID)
        refs.WriteXml("C:\RefsDump.xml", XmlWriteMode.WriteSchema)
    End Sub

#End Region
End Class
