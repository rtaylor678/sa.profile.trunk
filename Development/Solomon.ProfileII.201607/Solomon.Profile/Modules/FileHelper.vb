Option Explicit On

Imports System
Imports System.IO

Friend Class FileHelper
    ' 
    ' TODO: Add constructor logic here 
    ' 
    'Friend Sub New()
    'End Sub

    ''' <summary> 
    ''' Reads the file and return the content 
    ''' </summary> 
    ''' <param name="filename"></param> 
    ''' <returns></returns> 
    Friend Function ReadFile(ByVal filename As String) As String
        Dim buf As String = ""
        If File.Exists(filename) Then
            Dim reader As StreamReader = File.OpenText(filename)
            Try
                buf = reader.ReadToEnd()
            Finally
                reader.Close()
            End Try
        End If
        Return buf
    End Function



End Class

Friend Module modFileHelper

    Friend Function getPath(ByVal strFilePath As String) As String
        Return Left(strFilePath, InStrRev(strFilePath, "\"))
    End Function

    Friend Function getFile(ByVal strFilePath As String) As String
        Return Right(strFilePath, Len(strFilePath) - InStrRev(strFilePath, "\"))
    End Function

    Friend Function getExt(ByVal strFilePath As String) As String
        Return Right(strFilePath, Len(strFilePath) - InStrRev(strFilePath, "."))
    End Function

    Friend Function getSafeName(ByVal strFilePath As String) As String
        Return Left(getFile(strFilePath), Len(getFile(strFilePath)) - Len(getExt(strFilePath)) - 1)
    End Function

End Module