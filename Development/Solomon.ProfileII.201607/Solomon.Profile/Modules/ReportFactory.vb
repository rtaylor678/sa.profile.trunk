Option Explicit On

Imports System
Imports System.Data
Imports System.Collections
Imports System.Configuration



Friend Class ReportFactory
    ' 
    ' TODO: Add constructor logic here 
    ' 
    'Friend Sub New()
    'End Sub

    Friend Function createReport(ByVal ds As DataSet, ByVal reportName As String, ByVal UOM As String, ByVal currencyCode As String, ByVal replacementValues As Hashtable) As String
        Dim report As IReport = Nothing
        Dim htmlText As String = ""
        Dim filename As String = ""

        For i As Integer = 0 To ConfigurationManager.AppSettings.Count - 1
            Dim settingName As String = ConfigurationManager.AppSettings.GetKey(i)
            If settingName.StartsWith(reportName) Then
                filename = System.Configuration.ConfigurationManager.AppSettings(settingName)
                ' 20081001 RRH Path - filename = frmMain.AppPath + "//" + filename
                filename = pathStartUp + "/" + filename
                report = New Report(filename)
                htmlText = report.getReport(ds, UOM, currencyCode, replacementValues)
                htmlText = htmlText.Replace("UOM", UOM)

                htmlText = htmlText.Replace("CurrencyCode", currencyCode)
                Return htmlText
            End If
        Next
        Return reportName + " is not found."



        'If System.Configuration.ConfigurationManager.AppSettings(reportName) IsNot Nothing Then
        '    filename = System.Configuration.ConfigurationManager.AppSettings(reportName)
        '    ' 20081001 RRH Path - filename = frmMain.AppPath + "//" + filename
        '    filename = pathStartUp + "/" + filename
        '    report = New Report(filename)
        '    htmlText = report.getReport(ds, UOM, currencyCode, replacementValues)
        '    htmlText = htmlText.Replace("UOM", UOM)

        '    htmlText = htmlText.Replace("CurrencyCode", currencyCode)
        'Else
        '    htmlText = reportName + " is not found."
        'End If
        'Return htmlText
    End Function

    Friend Function createReport(ByVal ds As DataSet, ByVal reportName As String, ByVal company As String, ByVal location As String, ByVal UOM As String, ByVal currencyCode As String, _
    ByVal month As Integer, ByVal year As Integer, ByVal replacementValues As Hashtable) As String
        Dim report As IReport = Nothing
        Dim htmlText As String = ""
        Dim filename As String = ""

        If System.Configuration.ConfigurationManager.AppSettings(reportName) IsNot Nothing Then
            filename = System.Configuration.ConfigurationManager.AppSettings(reportName)
            ' 20081001 RRH Path - filename = frmMain.AppPath + "//" + filename
            filename = pathStartUp +  filename
            report = New Report(filename)
            htmlText = report.getReport(ds, company, location, UOM, currencyCode, month, _
            year, replacementValues)
            htmlText = htmlText.Replace("UOM", UOM)
            htmlText = htmlText.Replace("CurrencyCode", currencyCode)
        End If
        Return htmlText
    End Function
End Class

