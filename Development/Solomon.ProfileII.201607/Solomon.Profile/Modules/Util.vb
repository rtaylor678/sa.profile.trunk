﻿Imports System.Configuration
Imports Solomon.ProfileII.Proxies

Module Util

    Private _useNonWCFWebService As Boolean = False
    Public Property UseNonWCFWebService As Boolean
        Get
            Return _useNonWCFWebService
        End Get
        Set(value As Boolean)
            Try
                Dim setting As String = ConfigurationManager.AppSettings("UseNonWCFWebService").ToString()
                Select Case setting.ToUpper()
                    Case "Y", "T", "YES", "TRUE"
                        _useNonWCFWebService = True
                End Select
            Catch
            End Try
        End Set
    End Property

    Public Function EndpointNameToUse() As String
        Try
            Return ConfigurationManager.AppSettings("EndpointNameToUse").ToString()
        Catch ex As Exception
            Throw New Exception("Error in EndpointNameToUse(): " & ex.Message)
        End Try
    End Function

    Public Function WcfServicesObject() As ProfileClient
        Try
            Dim svc As New ProfileClient("WCFServiceEndpoint")
            Return svc
        Catch ex As Exception
            Throw New Exception("Error in WcfServicesObject(): " & ex.Message)
        End Try
    End Function


End Module
