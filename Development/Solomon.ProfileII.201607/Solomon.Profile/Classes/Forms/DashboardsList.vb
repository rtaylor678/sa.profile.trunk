﻿Imports System.IO

Public Class DashboardsList
    Private _configPath As String = modAppDeclarations.pathConfig
    Private _control As SA_Browser

    Private Sub DashboardsList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadDashboardsToCbo()
    End Sub
    Public Sub AssignParentForm(ByRef cntrl As Object)
        _control = TryCast(cntrl, SA_Browser)
    End Sub

    Private Sub LoadDashboardsToCbo()
        Dim dir As New DirectoryInfo(_configPath)
        Dim files() As FileInfo = dir.GetFiles("Dashboard*.txt")
        For i As Integer = 0 To files.Length - 1
            Dim name As String = files(i).Name.Substring(9)
            name = name.Replace(".txt", "")
            cboDashboards.Items.Add(name)
        Next
    End Sub

    Private Sub Add_Click(sender As Object, e As EventArgs) Handles Add.Click
        _control.GetDashboardName(cboDashboards.Text)
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        _control.GetDashboardName(String.Empty)
        Me.Close()
    End Sub
End Class