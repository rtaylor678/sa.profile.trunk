﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RadarChartChoices
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblRankVariables = New System.Windows.Forms.Label()
        Me.chkBoxRankVariables = New System.Windows.Forms.CheckedListBox()
        Me.chkMonth = New System.Windows.Forms.CheckBox()
        Me.chkYTD = New System.Windows.Forms.CheckBox()
        Me.chkRollingAve = New System.Windows.Forms.CheckBox()
        Me.lblColor = New System.Windows.Forms.Label()
        Me.cboMonthColor = New System.Windows.Forms.ComboBox()
        Me.cboYtdColor = New System.Windows.Forms.ComboBox()
        Me.cboAveColor = New System.Windows.Forms.ComboBox()
        Me.btnRadarChart = New System.Windows.Forms.Button()
        Me.lblPeerGroups = New System.Windows.Forms.Label()
        Me.cboPeerGroups = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'lblRankVariables
        '
        Me.lblRankVariables.AutoSize = True
        Me.lblRankVariables.Location = New System.Drawing.Point(0, 56)
        Me.lblRankVariables.Name = "lblRankVariables"
        Me.lblRankVariables.Size = New System.Drawing.Size(79, 13)
        Me.lblRankVariables.TabIndex = 0
        Me.lblRankVariables.Text = "Rank Variables"
        '
        'chkBoxRankVariables
        '
        Me.chkBoxRankVariables.FormattingEnabled = True
        Me.chkBoxRankVariables.Location = New System.Drawing.Point(3, 73)
        Me.chkBoxRankVariables.Name = "chkBoxRankVariables"
        Me.chkBoxRankVariables.Size = New System.Drawing.Size(269, 169)
        Me.chkBoxRankVariables.TabIndex = 1
        '
        'chkMonth
        '
        Me.chkMonth.AutoSize = True
        Me.chkMonth.Location = New System.Drawing.Point(6, 278)
        Me.chkMonth.Name = "chkMonth"
        Me.chkMonth.Size = New System.Drawing.Size(56, 17)
        Me.chkMonth.TabIndex = 4
        Me.chkMonth.Text = "Month"
        Me.chkMonth.UseVisualStyleBackColor = True
        '
        'chkYTD
        '
        Me.chkYTD.AutoSize = True
        Me.chkYTD.Location = New System.Drawing.Point(6, 306)
        Me.chkYTD.Name = "chkYTD"
        Me.chkYTD.Size = New System.Drawing.Size(90, 17)
        Me.chkYTD.TabIndex = 5
        Me.chkYTD.Text = "Year To Date"
        Me.chkYTD.UseVisualStyleBackColor = True
        '
        'chkRollingAve
        '
        Me.chkRollingAve.AutoSize = True
        Me.chkRollingAve.Location = New System.Drawing.Point(6, 334)
        Me.chkRollingAve.Name = "chkRollingAve"
        Me.chkRollingAve.Size = New System.Drawing.Size(106, 17)
        Me.chkRollingAve.TabIndex = 6
        Me.chkRollingAve.Text = "Study Equivalent"
        Me.chkRollingAve.UseVisualStyleBackColor = True
        '
        'lblColor
        '
        Me.lblColor.AutoSize = True
        Me.lblColor.Location = New System.Drawing.Point(156, 254)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(63, 13)
        Me.lblColor.TabIndex = 7
        Me.lblColor.Text = "Color to use"
        '
        'cboMonthColor
        '
        Me.cboMonthColor.FormattingEnabled = True
        Me.cboMonthColor.Location = New System.Drawing.Point(151, 278)
        Me.cboMonthColor.Name = "cboMonthColor"
        Me.cboMonthColor.Size = New System.Drawing.Size(121, 21)
        Me.cboMonthColor.TabIndex = 8
        '
        'cboYtdColor
        '
        Me.cboYtdColor.FormattingEnabled = True
        Me.cboYtdColor.Location = New System.Drawing.Point(151, 306)
        Me.cboYtdColor.Name = "cboYtdColor"
        Me.cboYtdColor.Size = New System.Drawing.Size(121, 21)
        Me.cboYtdColor.TabIndex = 9
        '
        'cboAveColor
        '
        Me.cboAveColor.FormattingEnabled = True
        Me.cboAveColor.Location = New System.Drawing.Point(151, 334)
        Me.cboAveColor.Name = "cboAveColor"
        Me.cboAveColor.Size = New System.Drawing.Size(121, 21)
        Me.cboAveColor.TabIndex = 10
        '
        'btnRadarChart
        '
        Me.btnRadarChart.Location = New System.Drawing.Point(74, 386)
        Me.btnRadarChart.Name = "btnRadarChart"
        Me.btnRadarChart.Size = New System.Drawing.Size(116, 23)
        Me.btnRadarChart.TabIndex = 11
        Me.btnRadarChart.Text = "Get Radar Chart"
        Me.btnRadarChart.UseVisualStyleBackColor = True
        '
        'lblPeerGroups
        '
        Me.lblPeerGroups.AutoSize = True
        Me.lblPeerGroups.Location = New System.Drawing.Point(0, 7)
        Me.lblPeerGroups.Name = "lblPeerGroups"
        Me.lblPeerGroups.Size = New System.Drawing.Size(66, 13)
        Me.lblPeerGroups.TabIndex = 19
        Me.lblPeerGroups.Text = "Peer Groups"
        '
        'cboPeerGroups
        '
        Me.cboPeerGroups.FormattingEnabled = True
        Me.cboPeerGroups.Location = New System.Drawing.Point(3, 22)
        Me.cboPeerGroups.Name = "cboPeerGroups"
        Me.cboPeerGroups.Size = New System.Drawing.Size(269, 21)
        Me.cboPeerGroups.TabIndex = 20
        '
        'RadarChartChoices
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 424)
        Me.Controls.Add(Me.cboPeerGroups)
        Me.Controls.Add(Me.lblPeerGroups)
        Me.Controls.Add(Me.btnRadarChart)
        Me.Controls.Add(Me.cboAveColor)
        Me.Controls.Add(Me.cboYtdColor)
        Me.Controls.Add(Me.cboMonthColor)
        Me.Controls.Add(Me.lblColor)
        Me.Controls.Add(Me.chkRollingAve)
        Me.Controls.Add(Me.chkYTD)
        Me.Controls.Add(Me.chkMonth)
        Me.Controls.Add(Me.chkBoxRankVariables)
        Me.Controls.Add(Me.lblRankVariables)
        Me.Name = "RadarChartChoices"
        Me.Text = "RadarChartChoices"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkBoxRankVariables As System.Windows.Forms.CheckedListBox
    Friend WithEvents chkMonth As System.Windows.Forms.CheckBox
    Friend WithEvents chkYTD As System.Windows.Forms.CheckBox
    Friend WithEvents chkRollingAve As System.Windows.Forms.CheckBox
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents cboMonthColor As System.Windows.Forms.ComboBox
    Friend WithEvents cboYtdColor As System.Windows.Forms.ComboBox
    Friend WithEvents cboAveColor As System.Windows.Forms.ComboBox
    Friend WithEvents btnRadarChart As System.Windows.Forms.Button
    Friend WithEvents lblRankVariables As System.Windows.Forms.Label
    Friend WithEvents lblPeerGroups As System.Windows.Forms.Label
    Friend WithEvents cboPeerGroups As System.Windows.Forms.ComboBox
End Class
