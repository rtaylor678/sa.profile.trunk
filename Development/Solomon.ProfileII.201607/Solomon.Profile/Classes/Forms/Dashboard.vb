﻿Imports System.IO
Imports System.Collections.Generic
Imports Solomon.ProfileII.Contracts
Imports Solomon.ProfileII.Proxies
Imports System.Configuration

Public Class Dashboard
    Private _widthAdjuster As Integer = 30 'width adjuster
    Private _heightAdjuster As Integer = 90 'height adjuster
    Private _maxDock As MaxDock
    Private _tileDock As TileDock
    Private _dashboardNames() As String
    Private _dashboardControls As List(Of SA_Browser)
    Private _delim As String = Chr(14)
    Private _configPath As String = modAppDeclarations.pathConfig
    Private _parentForm As frmMain = Nothing
    Private _graphicType As Integer = 0  '0=table, 1=report, 2=custom report, 3=column/stacked column chart, 4 = radar chart
    Private _loadedMonths As List(Of DropdownItem)

    Private Sub Dashboard_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pnlOther.Visible = False
        _loadedMonths = New List(Of DropdownItem)
        If modGeneral.AdminRights Then
            LoadLoadedMonthsIntoCbo()
        End If
        'pnlTable.Dock = DockStyle.Top
        'pnlTable.AutoSize = True
        btnLoad.Enabled = False
        LoadDashboardsToCbo()
        If Not modGeneral.AdminRights Then
            TextBox1.Enabled = False
            btnCreate.Enabled = False
            btnDashboardChgDates.Enabled = False
            DashboardDateChoices.Enabled = False
            btnDelete.Enabled = False
        End If
    End Sub

    Public Sub SetParentForm(frm As Form)
        _parentForm = TryCast(frm, frmMain)
    End Sub


    Private Sub LoadDashboardsToCbo()
        Dim count As Integer = 0
        Dim dir As New DirectoryInfo(_configPath)
        Dim files() As FileInfo = dir.GetFiles("Dashboard*.txt")
        For i As Integer = 0 To files.Length - 1
            Dim name As String = files(i).Name.Substring(9)
            name = name.Replace(".txt", "")
            cboDashboards.Items.Add(name)
            count = count + 1
        Next
        If count < 1 Then
            cboDashboards.Enabled = False
            btnLoad.Enabled = False
            btnDelete.Enabled = False
        Else
            cboDashboards.Enabled = True
            btnLoad.Enabled = True
            btnDelete.Enabled = True
        End If
    End Sub

    Private Sub cboDashboards_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboDashboards.SelectionChangeCommitted
        If cboDashboards.SelectedItem.ToString().Trim().Length < 1 Then
            btnLoad.Enabled = False
        Else
            btnLoad.Enabled = True
        End If
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        If cboDashboards.Text.Trim().Length < 1 Then
            MsgBox("No dashboard was selected.")
            Exit Sub
        End If
        LoadRoutine()
    End Sub

    Private Sub LoadRoutine()
        pnlOther.Visible = False
        _dashboardControls = ReadUCsFromFile(_configPath, cboDashboards.Text)
        TilePanel(_dashboardControls)
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If cboDashboards.Text.Trim().Length < 1 Then
            MsgBox("No dashboard was selected.")
            Exit Sub
        End If
        If MsgBox("Are you sure you want to delete Dashboard " + cboDashboards.Text + "?", MsgBoxStyle.YesNo, "Profile Dashboard Deletion") <> vbYes Then
            Exit Sub
        End If
        DeleteDashboard(cboDashboards.Text)
        LoadDashboardsToCbo()
    End Sub

    Private Sub DeleteDashboard(dashboardName As String)
        cboDashboards.Items.Clear()
        Dim dir As New DirectoryInfo(_configPath)
        Dim files() As FileInfo = dir.GetFiles("Dashboard*.txt")
        For i As Integer = 0 To files.Length - 1
            Dim name As String = files(i).Name.Substring(9)
            name = name.Replace(".txt", "")
            If name = dashboardName Then
                File.Delete(files(i).FullName)
                Exit Sub
            End If
        Next
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        If TextBox1.Text.Trim().Length < 1 Then
            MsgBox("No dashboard name was entered.")
            Exit Sub
        End If
        For Each item As Object In cboDashboards.Items
            If item.ToString().ToUpper() = TextBox1.Text.Trim().ToUpper() Then
                MsgBox("That dashboard name already exists.")
                Exit Sub
            End If
        Next

        Dim fileName As String = "Dashboard" + TextBox1.Text.Trim() + ".txt"
        'File.Create(_configPath + fileName)
        Using sw As New StreamWriter(_configPath + fileName)

        End Using

        'File.Exists(_configPath + fileName) 'seems to help unlock this file after it is created. !!??!!??
        cboDashboards.Items.Clear()
        LoadDashboardsToCbo()
        TextBox1.Text = String.Empty

    End Sub


    Private Sub TilePanel(controls As List(Of SA_Browser))
        pnlOther.Visible = False
        For Each ctrl As Control In pnlTable.Controls
            If Not TypeOf (ctrl) Is Panel Then
                pnlTable.Controls.RemoveAt(0)
            End If
        Next

        If IsNothing(controls) Then Exit Sub 'controls = GetControls()
        pnlTable.Controls.Clear()
        pnlTable.ColumnCount = 0
        pnlTable.RowCount = 0
        _tileDock = New TileDock(pnlTable, controls.ToArray())
        _tileDock.Visible = True
        _tileDock.Top = pnlHeader.Height ' MenuStrip1.Height + MenuStrip2.Height
        _tileDock.Left = 0 ' ControlPanel.Width
        _tileDock.Width = Me.Width - _widthAdjuster '- ControlPanel.Width - w
        _tileDock.Height = Me.Height - pnlHeader.Height - _heightAdjuster
        _tileDock.PrepPanel()
    End Sub
    Private Sub CascadePanel()
        '    pnlTable.Visible = False
        '    For Each ctrl As Control In pnlWorkspace.Controls
        '        If TypeOf (ctrl) Is UC Then
        '            pnlWorkspace.Controls.RemoveAt(0)
        '        End If
        '    Next
        '    Dim controls() As UC = GetControls()
        '    Dim c As New CascadeDock(pnlWorkspace, controls)
        '    c.Visible = True
        '    c.Top = MenuStrip1.Height + MenuStrip2.Height
        '    c.Left = ControlPanel.Width
        '    c.Width = Me.Width - ControlPanel.Width - w
        '    c.Height = Me.Height - h
        '    c.PrepPanel()
    End Sub
    Private Sub MaxPanel()
        '    pnlTable.Visible = False
        '    For Each ctrl As Control In pnlWorkspace.Controls
        '        If Not TypeOf (ctrl) Is Panel Then
        '            pnlWorkspace.Controls.RemoveAt(0)
        '        End If
        '    Next
        '    While pnlButtons.Controls.Count > 1 'leaving 1st for now
        '        pnlButtons.Controls.RemoveAt(1)
        '    End While
        '    Dim controls() As UC = GetControls()
        '    _maxDock = New MaxDock(pnlWorkspace, controls)
        '    _maxDock.Visible = True
        '    _maxDock.Top = MenuStrip1.Height + MenuStrip2.Height
        '    _maxDock.Left = ControlPanel.Width
        '    _maxDock.Width = Me.Width - ControlPanel.Width - w
        '    _maxDock.Height = Me.Height - MenuStrip1.Height - MenuStrip2.Height - 50
        '    'pnlWorkspace.BackColor = Color.Violet
        '    _maxDock.PrepPanel()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        _maxDock.PanelButtonsClick(sender, e)
    End Sub


    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        WriteUCsToFile(_dashboardControls, _configPath, cboDashboards.Text)
    End Sub

    Private Sub WriteUCsToFile(controls As List(Of SA_Browser), folderPath As String, dashboardName As String)
        'NOTE: I'm not using XML because I'm writing the URLs, which will have some
        ' characters that XML will need to transform back and forth, etc.

        If IsNothing(controls) Then Exit Sub
        Dim count As Integer = controls.Count
        Dim path As String = folderPath + "\Dashboard" + dashboardName + ".txt"
        If count > 0 Then
            Try
                Using writer As New StreamWriter(path, False)
                    For i As Integer = 0 To count - 1
                        writer.WriteLine(controls(i).Ordinal.ToString() + _delim + controls(i).Url)
                    Next
                    writer.Flush()
                End Using
            Catch ex As Exception
                MsgBox("Error in WriteUCsToFile()" + ex.Message)
            End Try
        End If
    End Sub
    Private Function ReadUCsFromFile(folderPath As String, dashboardName As String) As List(Of SA_Browser)
        Dim path As String = folderPath + "\Dashboard" + dashboardName + ".txt"
        Try
            Dim list As New List(Of String)
            Using reader As New StreamReader(path)
                While Not reader.EndOfStream
                    Dim line As String = reader.ReadLine()
                    If line.Length > 0 Then
                        list.Add(line)
                    End If
                End While
            End Using
            Dim controls As New List(Of SA_Browser)
            For i As Integer = 0 To list.Count - 1
                Dim c As New SA_Browser
                Dim parts() As String = list(i).Split(_delim)
                c.Ordinal = Int32.Parse(parts(0))

                If parts(1) = 0 Then
                    Dim testhtml As String = _parentForm.GetReportFromTitleAndDesc(parts(2), parts(3), Nothing) ' c)"<HTML><HEAD><title>HERE</title><body>HERE</body></HTML>"
                    c.wbWebBrowser.Navigate("about:blank")
                    c.wbWebBrowser.DocumentText = testhtml
                    c.wbWebBrowser.Document.Write(testhtml)
                    c.Url = parts(2) + _delim + parts(3)
                    c.CallingDashboard = Me
                ElseIf parts(1) = 1 Or parts(1) = 2 Then
                    'need call WCF , is report.

                    'Dim endpointName As String = ConfigurationManager.AppSettings("WcfReportsEndpointName")
                    Dim proxy As New ProfileClient(Util.EndpointNameToUse())
                    'Dim svc As ReportWebSvc.ReportService = Util.ReportServicesObject()
                    Dim result As String = String.Empty
                    Try
                        result = proxy.GetReportHtml(parts(2), Nothing)
                        Dim myParent As Main ' = DirectCast(_parentForm, Main)
                        For Each z As Form In _parentForm.MdiChildren
                            If z.Name = "Main" Then
                                myParent = DirectCast(z, Main)
                                Exit For
                            End If
                        Next
                        ' Dim myParent As Main ' = DirectCast(_parentForm, Main)
                        Dim coLocation As String = String.Empty
                        If myParent.txtLocation.Text = "" Then coLocation = "ND" Else coLocation = myParent.txtLocation.Text.Trim()
                        Dim reportCode As String = modGeneral.GetQuerystringVariable(parts(2), "rn")
                        Dim scenario As String = modGeneral.GetQuerystringVariable(parts(2), "SN").Trim()
                        Dim heading As String = modGeneral.GetWcfReportHeading(reportCode, scenario, myParent.txtCompany.Text.Trim(), coLocation)
                        Dim inttest As Integer = result.IndexOf("runat=" & Chr(34) & "server" & Chr(34) & ">")

                        result = result.Replace("runat=" & Chr(34) & "server" & Chr(34) & ">", "runat=" & Chr(34) & "server" & Chr(34) & ">" & heading)
                    Catch ex As Exception
                        result = "<HTML>Error</HTML>"
                    End Try
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()

                    c.wbWebBrowser.Navigate("about:blank")
                    c.wbWebBrowser.DocumentText = result
                    c.wbWebBrowser.Document.Write(result)
                    'c.Url = parts(2) + _delim + parts(3)
                    c.Url = parts(0) + _delim + parts(2)
                    c.CallingDashboard = Me
                Else
                    'chart

                    c.Url = parts(2)
                    c.CallingDashboard = Me
                    Dim header As String = "WsP: " & modGeneral.GetClientKey() + vbCrLf
                    c.wbWebBrowser.Navigate(c.Url, "", Nothing, header)


                End If
                controls.Add(c)
            Next
            Return controls
        Catch ex As Exception
            MsgBox("Error in ReadUCsFromFile(): " + ex.Message)
        End Try
        Return Nothing
    End Function

    Public Sub RemoveControl(ordinal As Integer, url As String)
        For Each contrl As SA_Browser In _dashboardControls
            If contrl.Ordinal = ordinal Then
                _dashboardControls.Remove(contrl)
                'WriteUCsToFile(_dashboardControls, _configPath, cboDashboards.Text)
                'TilePanel(_dashboardControls)
                Exit For
            End If
        Next

        'Now reorder the controls, and change the ordinals.
        Dim count As Integer = 1
        For i As Integer = 0 To _dashboardControls.Count - 1
            _dashboardControls(i).Ordinal = count
            count += 1
        Next
        'now save in the new order
        WriteUCsToFile(_dashboardControls, _configPath, cboDashboards.Text)
        TilePanel(_dashboardControls)
    End Sub


    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        'btnTest.Text = Me.Width.ToString() + "|" + Me.Height.ToString()
        'MsgBox("Rows: " + pnlTable.RowCount.ToString() + ", cols " + pnlTable.ColumnCount.ToString())
        MsgBox(_dashboardControls(0).wbWebBrowser.DocumentText)
    End Sub

 
    Private Sub btnDashboardChgDates_Click(sender As Object, e As EventArgs) Handles btnDashboardChgDates.Click
        'Dim val As String = DashboardDateChoices.SelectedItem.ToString() ' DashboardDateTimePicker.Value
        'If Not IsDate(val) Then
        '    MsgBox(val + " is not a valid date")
        '    Exit Sub
        'End If
        'If CDate(val).Day <> 1 Then
        '    MsgBox("You have selected day " = CDate(val).Day.ToString() + ". Please select the first day of the month and try again.")
        '    Exit Sub
        'End If
        Dim foundItem As DropdownItem
        For Each itm As DropdownItem In _loadedMonths
            If itm.Text = DashboardDateChoices.SelectedItem.ToString() Then
                foundItem = itm
                Exit For
            End If
        Next

        Dim newEndDate As New Date(foundItem.Year, foundItem.Month, 1)
        Dim dir As New DirectoryInfo(_configPath)
        Dim files() As FileInfo = dir.GetFiles("Dashboard*.txt")
        For i As Integer = 0 To files.Length - 1
            Dim dashboardName As String = files(i).Name.Remove(0, 9).Replace(".txt", "")
            If Not ChangeThisFilesStartOrEndDates(files(i).FullName, newEndDate, dashboardName,
                                                  System.Threading.Thread.CurrentThread.CurrentCulture.ToString()) Then Exit Sub
        Next
        'then click load button
        If Not IsNothing(cboDashboards.SelectedItem) AndAlso Not cboDashboards.SelectedItem.ToString().Trim() = String.Empty Then
            LoadRoutine()
        End If

    End Sub

    Private Function DelocalizeDate(cult As String, theDate As String) As String
        Dim dtmDate As Date = Convert.ToDateTime(theDate)
        Dim usDate As String = String.Empty
        'Using wr As New StreamWriter("C:\Projects\LocalizTest\LocalizTest\ChgLocal2.txt", False)
        'wr.WriteLine(dtmDate.ToString())
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        usDate = dtmDate.ToShortDateString()
        'usDate = theDate.ToString()
        'wr.WriteLine(dtmDate)
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(cult)
        Return usDate
        'End Using

    End Function

    'Private Function ChangeThisFilesStartDates_obsolete(fullPath As String, dropdownNewEndDate As Date, dashboardName As String) As Boolean
    '    Try
    '        Dim contents As New List(Of String)
    '        Dim usDate As String = String.Empty
    '        Dim usDtm As Date = Nothing
    '        Dim cult As String = System.Threading.Thread.CurrentThread.CurrentCulture.ToString()
    '        Using reader As New StreamReader(fullPath)
    '            While Not reader.EndOfStream
    '                Dim line As String = reader.ReadLine()
    '                'check for if Radar chart or refinery trends report. If so, then there is no end date, and make the start date in the URL match the date in the dropdown.
    '                MsgBox("check for if Radar chart or refinery trends report. If so, then there is no end date, and make the start date in the URL match the date in the dropdown.")
    '                '0=table, 1=report, 2=custom report, 3=column/stacked column chart, 4 = radar chart
    '                MsgBox("Change logic below, it treats cbo date as start date but now make end date match cbo instead.")


    '                'check for start date
    '                If line.Trim().Length > 0 And line.Contains("&sd=") Then
    '                    Dim startPos As Integer = line.IndexOf("sd=") + 3
    '                    Dim endPos As Integer = line.IndexOf("&", startPos)
    '                    Dim oldStartDate As Date = CDate(line.Substring(startPos, endPos - startPos))
    '                    usDate = DelocalizeDate("en-SG", oldStartDate)

    '                    System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")

    '                    oldStartDate = Convert.ToDateTime(usDate)
    '                    Dim dateDif As Long = DateDiff(DateInterval.Month, oldStartDate, newStartDate)
    '                    'chg start date
    '                    'usDate = DelocalizeDate(System.Globalization.CultureInfo.CurrentCulture.ToString(), newStartDate.ToString())
    '                    line = line.Substring(0, startPos) + newStartDate.ToString() + line.Substring(endPos)
    '                    System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(cult)



    '                    'check for end date
    '                    If line.Trim().Length > 0 And line.Contains("&ed=") Then
    '                        startPos = line.IndexOf("&ed=") + 3
    '                        endPos = line.IndexOf("&", startPos)
    '                        Dim oldEndDate As Date = CDate(line.Substring(startPos + 1, endPos - startPos - 1))
    '                        'System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
    '                        usDate = DelocalizeDate("en-SG", oldEndDate)
    '                        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
    '                        oldEndDate = Convert.ToDateTime(usDate)
    '                        Dim newEndDtm As Date = oldEndDate.AddMonths(dateDif)
    '                        Dim newEndDate As String = newEndDtm.ToString()
    '                        'Dim newEndDate As String = oldEndDate.ToString() '"M/d/yyyy") 'GetNewEndDate(oldStartDate, newStartDate, oldEndDate)
    '                        'chg end date
    '                        'usDate = String.Empty
    '                        'usDate = DelocalizeDate(System.Globalization.CultureInfo.CurrentCulture.ToString(), newEndDate)
    '                        line = line.Substring(0, startPos) + "=" + newEndDtm.ToString() + line.Substring(endPos)
    '                        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(cult)
    '                    End If
    '                End If

    '                contents.Add(line)
    '            End While
    '        End Using
    '        If contents.Count > 0 Then
    '            Using writer As New StreamWriter(fullPath, False)
    '                For Each line As String In contents
    '                    writer.WriteLine(line)
    '                Next
    '            End Using
    '        End If
    '        Return True
    '    Catch ex As Exception
    '        MsgBox("Error converting dashboard " + dashboardName + ": " + ex.Message)
    '        Return False
    '    End Try
    'End Function

    Private Function ChangeThisFilesStartOrEndDates(fullPath As String, dropdownNewEndDate As Date, dashboardName As String,
                                                    cult As String) As Boolean
        'Dim cult As String = System.Threading.Thread.CurrentThread.CurrentCulture.ToString()


        Dim oldLines As New List(Of String)
        Dim newLines As New List(Of String)
        Try
            'get all the new lines here
            Using reader As New StreamReader(fullPath)
                While Not reader.EndOfStream
                    Dim line As String = reader.ReadLine()
                    If line.Trim().Length > 0 Then
                        oldLines.Add(line)
                    End If
                End While
            End Using

            'do the date changes
            For Each oldLine As String In oldLines
                'check for if Radar chart or refinery trends report. If so, then there is no end date, and make the start date in the URL match the date in the dropdown.
                'make end date match cbo instead of start date matching cbo
                Dim parts() As String = oldLine.Split(_delim)
                Dim newLine As String = String.Empty
                Dim startDateOnly As Boolean = False
                Select Case parts(1)
                    'Case 0 'Table

                    Case 1, 2  'report, Custom REport
                        If parts.Length >= 3 AndAlso parts(2).Contains("rn=Refinery Trends Report") Then
                            startDateOnly = True
                        End If
                    Case 3  'Col/stacked col chart

                    Case 4
                        startDateOnly = True
                End Select

                If startDateOnly Then
                    If Not DoWorkForChangeThisLineStartOrEndDates(oldLine, "sd=", cult, dropdownNewEndDate, Nothing) Then
                        'error is messaged in the function. 
                        Return False
                    End If
                Else
                    Dim dateDif As Long? = Nothing
                    If Not DoWorkForChangeThisLineStartOrEndDates(oldLine, "ed=", cult, dropdownNewEndDate, dateDif) Then
                        'error is messaged in the function. 
                        Return False
                    End If
                    If Not DoWorkForChangeThisLineStartOrEndDates(oldLine, "sd=", cult, dropdownNewEndDate, dateDif) Then
                        'error is messaged in the function. 
                        Return False
                    End If
                End If
                newLines.Add(oldLine)
            Next
        Catch ex As Exception
            MsgBox("Error in changing dates for dashboard " + dashboardName + ": " & ex.Message)
            Return False
        End Try
        Try
            'if no errors, then write new lines over old file
            Using writer As New StreamWriter(fullPath, False)
                For Each newLine As String In newLines
                    If newLine.Trim().Length > 0 Then
                        writer.WriteLine(newLine)
                    End If
                Next
            End Using
        Catch ex2 As Exception
            MsgBox("Error converting dashboard " + dashboardName + ": " + ex2.Message)
            Return False
        End Try
        Return True
    End Function

    Private Function DoWorkForChangeThisLineStartOrEndDates(ByRef line As String, ByVal queryStringKey As String,
                                                            ByVal cultureInfo As String, ByVal newDropDownEndDate As Date,
                                                            ByRef dateDif As Long?) As Boolean
        'expect queryStringKey to be either "sd=" or "ed="
        'expect cultureInfo to be something like "en-US", "en-SG", etc.
        Try
            If line.Trim().Length > 0 And line.Contains("&" & queryStringKey) Then
                Dim startPos As Integer = line.IndexOf(queryStringKey) + 3
                Dim endPos As Integer = line.IndexOf("&", startPos)
                Dim oldDate As Date = CDate(line.Substring(startPos, endPos - startPos))
                Dim usDate As String = DelocalizeDate(cultureInfo, oldDate)
                Dim newDate As Date = Nothing

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
                oldDate = Convert.ToDateTime(usDate)
                If IsNothing(dateDif) Then
                    Dim longDateDif As Long = DateDiff(DateInterval.Month, oldDate, newDropDownEndDate)
                    dateDif = longDateDif
                    newDate = newDropDownEndDate.ToString()
                Else
                    newDate = oldDate.AddMonths(dateDif)
                End If
                line = line.Substring(0, startPos) + newDate.ToShortDateString() + line.Substring(endPos)
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(cultureInfo)
            Else
                Return True 'no change
            End If
            Return True
        Catch ex As Exception
            MsgBox("Error converting dates in DoWorkForChangeThisLineStartOrEndDates(): " & ex.Message)
            Return False
        End Try
    End Function

    Private Function HandleStartOrEndDateChanges(lineIn As String) As String
        Try

        Catch ex As Exception
            MsgBox("Error in HandleStartOrEndDateChanges(): " & ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Sub LoadLoadedMonthsIntoCbo()
        Try
            'read months from LoadedMonths.xml
            Dim configPath As String = Application.StartupPath & "\_CONFIG"
            If Directory.Exists(configPath) Then
                Dim fileName As String = Dir(configPath + "\LoadedMonths.xml")
                If fileName.Length > 0 Then
                    Dim ds As New DataSet()
                    ds.ReadXml(configPath & "\" & fileName)
                    If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            Dim itm As New DropdownItem()
                            itm.Month = dr("PeriodMonth")
                            itm.Year = dr("PeriodYear")
                            Dim tempDate As New DateTime(itm.Year, itm.Month, 1)
                            itm.Text = tempDate.ToString("MMM, yyyy")
                            _loadedMonths.Add(itm)
                            DashboardDateChoices.Items.Add(itm.Text)
                        Next
                    End If
                End If
            End If
            'add them as text, but keep their number value, in the DashboardDateChoices cbo.

        Catch ex As Exception

        End Try
    End Sub

    Private Class DropdownItem
        Public Text As String
        Public Month As Integer
        Public Year As Integer
    End Class

End Class