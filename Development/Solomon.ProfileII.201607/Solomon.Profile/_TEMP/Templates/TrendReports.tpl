<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)
ShowIf(Ytd,IfYtdOn)
ShowIf(Avg,IfAvgOn)

<table height="100%" width="100%" border=0>
  <!--tr>
    <td width=5></td>
    <td></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
  </tr-->

<tr>
<!-- CURRENT -->

<td valign="top">
<table class="small">
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom>ReportPeriod</td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Target,Refinery<br/>Target)</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Avg,Rolling</br>Average>)</strong</td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Ytd,YTD</br>Average)</strong></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC)
  INCLUDETABLE(GENSUM)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom ><strong>@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td nowrap>@ChartTitle<span align=left>USEUNIT(AxisLabelUS,AxisLabelMetric)</span></td>
    <td align=right>&nbsp;NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
    <td align=right>&nbsp;TAG(Target,NoShowZero(Format(@@TargetField,@DecPlaces)))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Avg,NoShowZero(Format(@@AvgField,@DecPlaces)))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Ytd,NoShowZero(Format(@@YTDField,@DecPlaces)))</td>
    <td></td>
  </tr>
  END
 </table>
</td>
<!-- PREVIOUS -->
<td valign="top">
 <table class="small" height="%100">
 <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom>&nbsp;</td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Target,Refinery<br/>Target 1)</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Avg,Rolling</br>Average 1)</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Ytd,YTD</br>Average 1)</strong></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC)
  INCLUDETABLE(GENSUM)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp;NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Target,NoShowZero(Format(@@TargetField,@DecPlaces)))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Avg,NoShowZero(Format(@@AvgField,@DecPlaces)))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Ytd,NoShowZero(Format(@@YTDField,@DecPlaces)))</td>
    <td></td>
  </tr>
  END
 </table>
</td>

<!--- PREVIOUS -1 -->
<td valign="top">
 <table class="small" height="%100">
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom>&nbsp;</td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Target,Refinery<br/>Target 2)</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Avg,Rolling</br>Average 2)</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Ytd,YTD</br>Average 2)</strong></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC)
  INCLUDETABLE(GENSUM)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField12,@DecPlaces))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Target,NoShowZero(Format(@@TargetField,@DecPlaces)))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Avg,NoShowZero(Format(@@AvgField,@DecPlaces)))</td>
    <td></td>
    <td align=right>&nbsp;Tag(Ytd,NoShowZero(Format(@@YTDField,@DecPlaces)))</td>
    <td></td>
  </tr>
  END
  </table>
</td>

</tr>
 
  
 
</table>
<!-- template-end -->
