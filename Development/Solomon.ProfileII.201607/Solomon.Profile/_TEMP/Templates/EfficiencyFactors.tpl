<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small width=900 border=0>
  <tr>
    <td colspan=20 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
  <tr>
    <td width=5></td>
    <td width=200></td>
    <td width=10></td>
    <td width=70></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Process Unit Name</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Process ID</strong></td>
    <td colspan=2 align=center valign=bottom><strong>MEI</br>Standard, k</strong></td>
    <td colspan=2 align=center valign=bottom><strong>mPEI</br>Standard, k</strong></td>
    <td colspan=2 align=center valign=bottom><strong>nmPEI</br>Standard, k</strong></td>
    <td colspan=2 align=center valign=bottom><strong>PEI</br>Standard, k</strong></td>
    <td colspan=2 align=center valign=bottom><strong>NEI</br>Standard, k</strong></td>
  </tr>

  SECTION(ProcessUnits, ProcessGrouping = 'Process', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 valign=bottom><strong>Ancillary Units</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Ancillary', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 valign=bottom><strong>Idle Units</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Idle', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 height=30 valign=bottom><strong>Utilities and Off-sites</strong></td>
  </tr>

  SECTION(ProcessUnits,ProcessGrouping = 'Utility', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

SECTION(UnitsOffsites,,)
  BEGIN
 
  <tr>
    <td></td>
    <td colspan=3>Receipts and Shipments</td>
    <td align=right>Format(TotRSMaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSMaintPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSNonMaintPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSNEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>

  <tr>
    <td height=30 valign=bottom colspan=4><strong>Total Refinery</strong></td>
    <td align=right valign=bottom><strong>Format(MaintEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(MaintPersEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(NonMaintPersEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(PersEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(NEOpexEffDiv,'#,##0')</strong></td>
    <td></td>
  </tr>
  END
 
</table>

<!-- template-end -->