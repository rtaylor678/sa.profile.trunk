<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)
ShowIf(Ytd,IfYtdOn)
ShowIf(Avg,IfAvgOn)

<table nowrap height="100%" width="750" border=0 class="trendtable">

<tr>
<!-- CURRENT -->

<td valign="top">
<table border=0 class="SMALL">
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>ReportPeriod<b></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC)
  INCLUDETABLE(GENSUM)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom ><strong>@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td nowrap valign=center>@ChartTitle<span align=left>USEUNIT(AxisLabelUS,AxisLabelMetric)</span></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
 </table>
</td>

<!-- PREVIOUS -->
<td valign="top">
 <table border=0 class="SMALL">
SECTION(GENSUM_FIRST_PREVIOUS,,)
BEGIN 
HEADER('
<tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>Format(PeriodStart,'MMMM yyyy')<b></td>
  </tr>
')
END 
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC_FIRST_PREVIOUS)
  INCLUDETABLE(GENSUM_FIRST_PREVIOUS)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
 </table>
</td>

<!--- PREVIOUS -1 -->
<td valign="top">
 <table border=0 class="SMALL">
SECTION(GENSUM_SECOND_PREVIOUS,,)
BEGIN 
HEADER('
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>Format(PeriodStart,'MMMM yyyy')<b></td>
  </tr>
  ')
END 

  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC_SECOND_PREVIOUS)
  INCLUDETABLE(GENSUM_SECOND_PREVIOUS)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
  </table>
</td>

<!--- PREVIOUS -2 -->
<td valign="top">
 <table border=0 class="SMALL">
  SECTION(GENSUM_THIRD_PREVIOUS,,)
BEGIN 
HEADER('
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>Format(PeriodStart,'MMMM yyyy')<b></td>
  </tr>
  ')
END 
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC_THIRD_PREVIOUS)
  INCLUDETABLE(GENSUM_THIRD_PREVIOUS)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
  </table>
</td>


</tr>
 
  
 
</table>
<!-- template-end -->
