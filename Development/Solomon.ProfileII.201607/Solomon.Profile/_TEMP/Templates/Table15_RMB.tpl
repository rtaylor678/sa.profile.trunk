<!-- config-start -->
FILE=_CONFIG/Yield.xml;

<!-- config-end -->
<!-- template-start -->
<table class='small' width=100% border=0>
  <tr>
    <td width=5></td>
    <td width=20></td>
    <td width=5></td>
    <td width=350></td>
    <td width=75></td>
    <td width=10></td>
    <td width=60></td>
    <td width=15></td>
  </tr>
  <tr>
    <td colspan=2 align=center valign=bottom><strong>Solomon</br>Material</br>Number</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Material Name</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Barrels</br>Input</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Purchased</br>Input Cost<br/>(CurrencyCode/bbl)</strong></td>
  </tr>
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Other Raw Material Blended Input</strong></td>
  </tr>
 SECTION(YIELD_RMB,Category='OTHRM',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Returns from Lube Refinery </strong></td>
  </tr>
 SECTION(YIELD_RMB,Category='RLUBE',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Returns from Chemical Plant</strong></td>
  </tr>

 SECTION(YIELD_RMB,Category='RCHEM',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
</table>
<!-- template-end -->

