﻿

Public Class TileDock
    Implements IDocker


    Private _userControls() As SA_Browser
    Private _panel As System.Windows.Forms.TableLayoutPanel
    Private _visible As Boolean = True
    Private _top As Integer
    Private _left As Integer
    Private _width As Integer
    Private _height As Integer


    Public Property Visible As Boolean
        Get
            Return _visible
        End Get
        Set(value As Boolean)
            _visible = value
            _panel.Visible = _visible
        End Set
    End Property
    Public Property Top As Integer
        Get
            Return _top
        End Get
        Set(value As Integer)
            _top = value
            _panel.Top = _top
        End Set
    End Property
    Public Property Left As Integer
        Get
            Return _left
        End Get
        Set(value As Integer)
            _left = value
            _panel.Left = _left
        End Set
    End Property
    Public Property Width As Integer
        Get
            Return _width
        End Get
        Set(value As Integer)
            _width = value
            _panel.Width = _width
        End Set
    End Property
    Public Property Height As Integer
        Get
            Return _height
        End Get
        Set(value As Integer)
            _height = value
            _panel.Height = _height
        End Set
    End Property
    Friend Property Controls As SA_Browser()
        Get
            Return _userControls
        End Get
        Set(value As SA_Browser())
            _userControls = value
        End Set
    End Property


    Friend Sub New(ByRef tablePanel As System.Windows.Forms.TableLayoutPanel, userControls() As SA_Browser)
        _panel = tablePanel
        Visible = True
        _userControls = userControls
        'PrepForm(userControls)
    End Sub

    Public Sub PrepPanel() Implements IDocker.PrepPanel
        ClearColsRows()
        For count As Integer = 0 To _userControls.Length - 1
            _userControls(count).lblHeader.Text = String.Empty
            _userControls(count).btnDataEntry.Visible = False
            _userControls(count).btnDump.Visible = False

            _userControls(count).btnZoomUp.Visible = False
            _userControls(count).btnAddToDashboard.Visible = False

            _userControls(count).Visible = True
            _userControls(count).Dock = DockStyle.None
            '_userControls(count).CallingForm = Nothing ' _panel
            _userControls(count).CallingPanel = Me
            If _userControls(count).wbWebBrowser.DocumentText.Length < 15 Then
                _userControls(count).wbWebBrowser.Navigate(_userControls(count).Url)
            End If
            '_callingPanel

            'Dim x As Integer = 0
            If count Mod 2 = 0 Then
                _panel.ColumnCount += 1
            End If
            If count = 0 Or count = 2 Or count = 6 Or count = 8 Then
                _panel.RowCount += 1
            End If
            _panel.Controls.Add(_userControls(count))
        Next
        Dim colsRows = _panel.ColumnCount.ToString() + "-" + _panel.RowCount.ToString()
        RedisplayUsercontrols()
        'colsRows = TablePanel.ColumnCount.ToString() + "-" + TablePanel.RowCount.ToString()
    End Sub

    Public Sub RedisplayUsercontrols() '(u As UserControl)
        'Dim count As Integer = TablePanel.Controls.Count
        Dim width As Integer = _panel.Width
        Dim height As Integer = _panel.Height '+ 200

        For count As Integer = 0 To _panel.Controls.Count - 1

            _panel.Controls(count).Height = height / _panel.RowCount '+ 1
            _panel.Controls(count).Width = width / _panel.ColumnCount ' + 1
            _panel.Controls(count).Dock = DockStyle.Fill

        Next
    End Sub

    'Public Overrides Sub CloseThis(ordinal As Integer)
    Public Sub CloseThis(ordinal As Integer) Implements IDocker.CloseThis
        Dim newUserControls(_userControls.Length - 2) As SA_Browser
        Dim iadded As Integer = 0
        'remove from collection
        For i As Integer = 0 To _userControls.Length - 1
            If _userControls(i).Ordinal <> ordinal Then
                newUserControls(iadded) = _userControls(i)
                iadded += 1
            End If
        Next
        _userControls = newUserControls
        'redisplay
        PrepPanel()
    End Sub

    'Public Overrides Sub AddThis(ordinal As Integer)
    Public Sub AddThis(ordinal As Integer) Implements IDocker.AddThis
        ReDim Preserve _userControls(_userControls.Length)
        Dim v As New SA_Browser()
        v.Ordinal = ordinal
        _userControls(_userControls.Length - 1) = v
        PrepPanel()

    End Sub

    Public Sub MakeTopmost(ordinal As Integer) Implements IDocker.MakeTopmost
        Return 'only used for Cascade
    End Sub

    Public Sub ClearColsRows()
        While _panel.Controls.Count > 0
            _panel.Controls.RemoveAt(0)
        End While

        While _panel.RowCount > 1
            _panel.RowCount = _panel.RowCount = -1
        End While
        While _panel.ColumnCount > 1
            _panel.ColumnCount = _panel.ColumnCount = -1
        End While
    End Sub


    'Private Sub TablePanel_MouseClick(sender As Object, e As MouseEventArgs) Handles TablePanel.MouseClick
    Private Sub TablePanel_MouseClick(sender As Object, e As MouseEventArgs)
        If e.Button = Windows.Forms.MouseButtons.Right Then
            AddThis(_userControls.Length + 1)
        End If
    End Sub


End Class
