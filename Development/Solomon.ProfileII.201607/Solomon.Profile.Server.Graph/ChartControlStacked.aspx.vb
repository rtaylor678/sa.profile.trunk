﻿Imports System.Web
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Data.SqlClient
Imports System.Collections.Specialized

Public Class ChartControlStacked
    Inherits System.Web.UI.Page

    'IF error "System.Web.HttpException: No http handler was found for request type 'GET'" then:
    ' put the handlers in web.config  <system.webserver><handlers> :
    '<add name="ChartImg" verb="*" path="ChartImg.axd" type="System.Web.UI.DataVisualization.Charting.ChartHttpHandler,     System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  />


    'TEST URL:http://localhost:2153/ChartControlStacked.aspx?TEST=1&cn=Non-Volume-Related+Expenses&sd=12/1/2012&ed=12/1/2013&ds=ACTUAL&sn=CLIENT&table=GenSum&UOM=US&currency=USD&YR=2014    &YTD=NonVolOpexUEDC_YTD&TwelveMonthAvg=NonVolOpexUEDC_Avg&target=NonVolOpexUEDC_Target&field1=ISNULL(NonVolOpexUEDC_SWB%2c0)+AS+%27SWB%27&field2=ISNULL(NonVolOpexUEDC_TA%2c0)+AS+%27Turnarounds%27&field3=ISNULL(NonVolOpexUEDC_Rout%2c0)+AS+%27Routine%27&field4=ISNULL(NonVolOpexUEDC_OthContract%2c0)+AS+%27Other+Contract%27&field5=ISNULL(NonVolOpexUEDC_Other%2c0)+AS+%27All+Other+Non-Volume-Related%27&TS=5/25/2016 5:31:16 PM

    'Protected WithEvents ListUnits As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents PanelUnits As System.Web.UI.WebControls.Panel
    Protected chrtData As New DataSet
    'Protected WithEvents C1WebChart1 As C1.Web.C1WebChart.C1WebChart
    'Protected WithEvents ltMessage As System.Web.UI.WebControls.Literal
    Protected totField As String

    'Protected WithEvents Chart1 As System.Web.UI.DataVisualization.Charting.Chart

    Dim _refineryid As String


    Property RefineryID()
        Get
            Return _refineryid
        End Get
        Set(ByVal Value)
            _refineryid = Value
        End Set
    End Property



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'If Not My.Computer.Name.Contains("S1F7B50") Then
        If IsNothing(Request.Headers.Get("WsP")) And ListUnits.Visible = False Then
            Response.Redirect("InvalidRequest.htm")
        End If

        'Response.Write("The Key is " + Request.Headers.Get("WsP"))
        'Validate key 
        If Not ValidateKey() And Not IsPostBack Then
            Response.Redirect("InvalidUser.htm")
        End If

        'Get refinery id from key
        If Not IsPostBack Then
            RefineryID = GetRefineryID()
            Session("RefID") = RefineryID
        Else
            RefineryID = Session("RefID")
        End If
        'Else
        '    RefineryID = "XXPAC"
        'End If
        Dim dsTot As DataSet


        'Check if it is a stack chart.If it is use ValueField1 instead of TotField 
        If Not IsNothing(Request.QueryString("field2")) AndAlso _
          Request.QueryString("field2").Trim.Length > 0 Then
            dsTot = QueryDb("SELECT 'ISNULL('+ ValueField1 +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString("cn").Trim + "' ")
        Else
            dsTot = QueryDb("SELECT 'ISNULL('+ replace(TotField,'_Target','') +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString("cn").Trim + "' ")
        End If


        totField = dsTot.Tables(0).Rows(0)("TotField")

        Dim dsLoc As DataSet = QueryDb("SELECT location FROM Submissions WHERE refineryID='" + RefineryID + "'")
        Dim location As String = dsLoc.Tables(0).Rows(0)("Location")

        totField = totField.Replace("Location", location)


        Select Case (Request.QueryString("table").ToUpper)

            Case "MaintAvailCalc".ToUpper, "Gensum".ToUpper, "MaintIndex".ToUpper
                QueryRefinery()
                If chrtData.Tables.Count <= 0 Or chrtData.Tables(0).Rows.Count <= 0 Then
                    Server.Transfer("NoData.htm")
                End If

            Case "UnitIndicators".ToUpper

                PanelUnits.Visible = True
                If Not IsPostBack Then
                    Dim unitStmt As String = "SELECT DISTINCT c.UnitID, RTRIM(c.ProcessID) +': '+ RTRIM(c.UnitName) AS UnitName,p.SortKey FROM Config c,ProcessID_LU p WHERE p.ProcessID=c.ProcessID AND p.MaintDetails='Y' AND c.SubmissionID IN " + _
                                             "(SELECT SubmissionID FROM Submissions WHERE RefineryID='" + RefineryID + "'" + _
                                             " AND PeriodStart BETWEEN '" + Request.QueryString("sd") + "' AND '" + Request.QueryString("ed") + "') ORDER BY p.SortKey"


                    Dim dsUnits As DataSet

                    Dim u As Integer
                    dsUnits = QueryDb(unitStmt)

                    'Populate Unit list
                    ListUnits.DataSource = dsUnits.Tables(0).DefaultView
                    ListUnits.DataTextField = "UnitName"
                    ListUnits.DataValueField = "UnitID"
                    ListUnits.DataBind()
                    ListUnits.Items.Insert(0, "")


                End If
            Case "CustomUnitData".ToUpper


                PanelUnits.Visible = True
                If Not IsPostBack Then


                    Dim customStmt As String = "SELECT DISTINCT c.UnitId, RTRIM(c.ProcessID) +': ' + RTRIM(c.unitname) AS UnitName,ul.Property " + _
                                               "FROM UnitTargets_LU ul,Config c" + _
                                                " WHERE ul.Property IN (select TotField from chart_lu WHERE ChartTitle='" + Request.QueryString("cn") + "') " + _
                                                " AND  ul.ProcessID=c.ProcessID" + _
                                                " AND c.SubmissionID IN " + _
                                                "(SELECT SubmissionID FROM Submissions WHERE RefineryID='" + RefineryID + "'" + _
                                                " AND PeriodStart BETWEEN '" + Request.QueryString("sd") + "' AND '" + Request.QueryString("ed") + "') "


                    Dim dsCustom As DataSet
                    Dim u As Integer

                    'Response.Write(customStmt)
                    dsCustom = QueryDb(customStmt)




                    'Populate Unit list
                    ListUnits.DataSource = dsCustom.Tables(0).DefaultView
                    ListUnits.DataTextField = "UnitName"
                    ListUnits.DataValueField = "UnitID"
                    ListUnits.DataBind()
                    ListUnits.Items.Insert(0, "")


                End If


            Case Else
                Response.Redirect("NoData.htm")
        End Select

        'GetChart()
        GetWindowsChart()


    End Sub 'Page_Load

    Private Function GetRefineryID() As String
        Dim refId As String

        Dim refLocation As Integer = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray).Length - 1
        refId = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray)(refLocation)
        Return refId
    End Function

    Private Function ValidateKey() As Boolean
        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(Request.Headers.Get("WsP")) Then
                company = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray)(0)

                locIndex = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray).Length - 2
                location = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray)(locIndex)

                If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Else
                    Return False
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = Request.Headers.Get("WsP").Trim)
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Private Function AreCalcsDone() As Boolean
        Dim sqlstmt As String = "SELECT Distinct SubmissionID,CalcsNeeded FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'"

        Dim ds As DataSet = QueryDb(sqlstmt)
        Dim calcsNeeded As Object

        If ds.Tables(0).Rows.Count > 0 Then
            calcsNeeded = ds.Tables(0).Rows(0)("CalcsNeeded")
            Return IsDBNull(calcsNeeded) Or IsNothing(calcsNeeded)
        Else
            Return False
        End If
    End Function
    Private Sub QueryCustomUnits()
        Dim strRow, query As String
        Dim selectedUnit As String
        Dim chartOptions As NameValueCollection = Request.QueryString

        If Not IsNothing(Request.Form("ListUnits")) AndAlso _
         Request.Form("ListUnits").Trim.Length > 0 Then
            selectedUnit = Request.Form("ListUnits")
        Else
            Exit Sub
        End If


        If chartOptions("UOM").StartsWith("US") Then
            totField = "USValue AS 'UnitName'"
        Else
            totField = "MetValue AS 'UnitName'"
        End If

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS ,AxisLabelMetric,CASE(Decplaces)" & _
                       " WHEN 0 then '{0:#,##0}'" + _
                       " WHEN 1 then '{0:#,##0.0}'" + _
                       " WHEN 2 then '{0:N}' " + _
                       " END AS DecFormat ," & totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
                  chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        If Not IsNothing(chartOptions("YTD")) AndAlso _
           chartOptions("YTD").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
           chartOptions("target").Trim.Length > 0 Then
            Dim colName As String
            If chartOptions("UOM").StartsWith("US") Then
                colName = "USTarget"
            Else
                colName = "MetTarget"
            End If

            query += ",ISNULL(" & colName + ",0) AS 'Target' "
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
             chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
        End If

        query += " FROM Chart_LU cl,CustomUnitData mc ,Submissions s " & _
             " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
             "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
             "(RTRIM(s.DataSet) = '" + chartOptions("ds") + "') " + _
             " AND ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
             " AND(mc.FactorSet='N/A' OR mc.FactorSet='" + chartOptions("YR").Trim + "') " + _
             " AND(mc.Currency='N/A' OR mc.Currency='" + chartOptions("currency").Trim + "')  AND mc.Property=replace(cl.TotField,'_Target','')" + _
            " ORDER BY s.PeriodStart"

        Dim unitName As String = ListUnits.Items.FindByValue(selectedUnit).Text()
        query = query.Replace("UnitName", unitName.Trim)
        'Response.Write(query)
        chrtData = QueryDb(query)

    End Sub

    Private Sub QueryUnits()
        Dim strRow, query As String
        Dim selectedUnit As String
        Dim chartOptions As NameValueCollection = Request.QueryString

        If Not IsNothing(Request.Form("ListUnits")) AndAlso _
           Request.Form("ListUnits").Trim.Length > 0 Then
            selectedUnit = Request.Form("ListUnits")
        Else
            Exit Sub
        End If

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" & _
                " WHEN 0 then '{0:#,##0}'" + _
                " WHEN 1 then '{0:#,##0.0}'" + _
                " WHEN 2 then '{0:N}' " + _
                " END AS DecFormat ," & totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
           chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        If Not IsNothing(chartOptions("YTD")) AndAlso _
           chartOptions("YTD").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
           chartOptions("target").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("target") + ",0) AS 'Target' "
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
             chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
        End If

        query += " FROM Chart_LU," & chartOptions("table") & " mc ,Submissions s " & _
             " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
             "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
             "(RTRIM(s.DataSet) = '" + chartOptions("ds") + "') " + _
             " AND ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
            " AND mc.Currency='" + chartOptions("currency").Trim + "' " + _
            " ORDER BY s.PeriodStart"

        Dim unitName As String = ListUnits.Items.FindByValue(selectedUnit).Text()
        query = query.Replace("UnitName", unitName.Trim)
        'Response.Write(query)
        chrtData = QueryDb(query)

    End Sub



    Private Sub QueryRefinery()
        Dim strRow, query As String
        Dim chartOptions As NameValueCollection = Request.QueryString
        Dim scenario As String = "CLIENT"

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" & _
                " WHEN 0 then '{0:#,##0}'" + _
                " WHEN 1 then '{0:#,##0.0}'" + _
                " WHEN 2 then '{0:N}' " + _
                " END AS DecFormat ," & totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
           chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        Dim specCases As String

        If Not IsNothing(chartOptions("YTD")) AndAlso _
            chartOptions("YTD").Trim.Length > 0 Then
            'specCases = chartOptions("YTD").Trim
            'If specCases.ToUpper.EndsWith("_YTD") Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
            'End If
            'End If
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
            chartOptions("target").Trim.Length > 0 Then
            'specCases = chartOptions("target").Trim
            'If specCases.ToUpper.EndsWith("_TARGET") Then
            query += ",ISNULL(" & chartOptions("target") + ",0) AS 'Target' "
            'End If
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
           chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            'specCases = chartOptions("twelveMonthAvg").Trim
            'If specCases.ToUpper.EndsWith("_AVG") Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
            'End If
        End If


        query += " FROM Chart_LU," & chartOptions("table") & " m, Submissions s " & _
             " WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + RefineryID + "' AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
             "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
             "RTRIM(s.DataSet) = '" + chartOptions("ds") + "'  AND  ChartTitle='" + chartOptions("cn").Trim + "' "

        If Not chartOptions("sn") Is Nothing AndAlso _
           chartOptions("sn").Length > 0 Then
            If chartOptions("sn") <> "BASE" Then
                scenario = chartOptions("sn")
            End If
        End If

        If chartOptions("table").ToUpper = "GENSUM" Then
            query += " AND Currency='" + chartOptions("currency") + "' AND m.FactorSet = '" + chartOptions("yr") + "' AND m.UOM='" + chartOptions("UOM") + "' AND m.Scenario='" + scenario + "' "
        ElseIf chartOptions("table").ToUpper = "MAINTAVAILCALC" Then
            query += " AND (FactorSet ='" + chartOptions("yr") + "' ) "
        ElseIf chartOptions("table").ToUpper = "MAINTINDEX" Then
            query += " AND Currency='" + chartOptions("currency") + "' AND m.FactorSet = '" + chartOptions("yr") + "' "
        End If

        query += "ORDER BY s.PeriodStart"

        chrtData = QueryDb(query)

    End Sub

    'Sub GetCustomChart()
    '    ltMessage.Text = String.Empty

    '    If Not AreCalcsDone() Then
    '        ltMessage.Text = CreateHTMLErrorMessage("The report is not available because" + _
    '                                               " the calculations for the requested time period" + _
    '                                               "is not completed.")
    '        Exit Sub
    '    End If

    '    Dim aliasName As String
    '    If Not IsNothing(Request.Form("ListUnits")) Then
    '        aliasName = ListUnits.Items.FindByValue(Request.Form("ListUnits")).Text().Trim
    '    End If
    '    'Dim field1 As String = Request.QueryString("field1").Replace("UnitName", aliasName)
    '    Dim field1 As String = totField.Replace("UnitName", aliasName)
    '    Dim t As Integer


    '    If chrtData.Tables.Count = 0 Then
    '        ltMessage.Text = CreateHTMLErrorMessage("Select a process unit to see results.")
    '    ElseIf chrtData.Tables(0).Rows.Count = 0 Then
    '        ltMessage.Text = CreateHTMLErrorMessage("No data was found")
    '    Else

    '        Dim plotter As New PlotData
    '        C1WebChart1.Visible = True
    '        C1WebChart1.LoadChartFromFile(Server.MapPath("Template\ChartTplt1.chart2dxml"))
    '        C1WebChart1.ChartArea.AxisY.GridMajor.Visible = True
    '        'field1 = field1.Replace(":", "")
    '        chrtData.WriteXml("C:\results.xml")
    '        plotter.Plot(chrtData, C1WebChart1, 0, Request.QueryString("UOM"), Request.QueryString("currency"), "", _
    '                     "Target", "", field1, "", "", "", "")

    '    End If
    'End Sub

    Sub GetChart()
        ltMessage.Text = String.Empty

        If Not AreCalcsDone() Then
            ltMessage.Text = CreateHTMLErrorMessage("The report is not available because" + _
                                                   " the calculations for the requested time period" + _
                                                   "is not completed.")
            Exit Sub
        End If

        Dim aliasName As String
        If Not IsNothing(Request.Form("ListUnits")) Then
            aliasName = ListUnits.Items.FindByValue(Request.Form("ListUnits")).Text().Trim
        End If
        'Dim field1 As String = Request.QueryString("field1").Replace("UnitName", aliasName)
        Dim field1 As String = totField.Replace("UnitName", aliasName)
        Dim t As Integer


        If chrtData.Tables.Count = 0 Then
            ltMessage.Text = CreateHTMLErrorMessage("Select a process unit to see results.")
        ElseIf chrtData.Tables(0).Rows.Count = 0 Then
            ltMessage.Text = CreateHTMLErrorMessage("No data was found")
        Else

            'Dim plotter As New PlotData
            'C1WebChart1.Visible = True
            'C1WebChart1.LoadChartFromFile(Server.MapPath("Template\ChartTplt1.chart2dxml"))
            'C1WebChart1.ChartArea.AxisY.GridMajor.Visible = True
            'plotter.Plot(chrtData, C1WebChart1, 0, Request.QueryString("uom"), Request.QueryString("currency"), Request.QueryString("TwelveMonthAvg"), _
            '             Request.QueryString("target"), Request.QueryString("YTD"), field1, Request.QueryString("field2"), Request.QueryString("field3"), _
            '             Request.QueryString("field4"), Request.QueryString("field5"))

        End If
    End Sub


    Public Sub GetChartData()
        Dim k As Integer
        Dim strRow, query As String
        Dim s, g As Integer

        If chrtData.Tables.Count > 0 Then
            strRow += "<table frame=box rules=rows width=70% style='font-size: xx-small; font-family: Tahoma;' border=1  cellPadding=1 cellSpacing=0 bordercolor=#000000  id=dataTable>"
            strRow += "<tr bgcolor='dimgray' >"
            strRow += "<th><font color='white'>Month</font></th>"

            'BUILD HEADER
            For k = 4 To chrtData.Tables(0).Columns.Count - 1
                strRow += "<th ><font color='white'>" & chrtData.Tables(0).Columns(k).ColumnName & "</font></th>"
            Next
            strRow += "</tr>"

            'BUILD BODY
            For s = 0 To chrtData.Tables(0).Rows.Count - 1
                Dim row As DataRow = chrtData.Tables(0).Rows(s)
                Dim fieldFormat As String = row(3) 'Assign Decimal Format


                strRow += "<tr>"
                strRow += "<td style=""border-right: solid thin;""><div align=center>" & row(0) & "&nbsp;</div></td>"
                'Fields
                For g = 4 To chrtData.Tables(0).Columns.Count - 1
                    If Not IsDBNull(row(g)) Then
                        strRow += "<td><div align=center>" & String.Format(fieldFormat, row(g)) & "&nbsp;</div></td>"
                    Else
                        strRow += "<td><div align=center> - </div></td>"
                    End If
                Next
                strRow += "</tr>"
            Next
            strRow += "</table> "


        End If
        Response.Write(strRow)

    End Sub



    Private Sub ListUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListUnits.SelectedIndexChanged
        If Request.QueryString("table").ToUpper = "CUSTOMUNITDATA" Then
            QueryCustomUnits()
            'GetCustomChart()
        Else
            QueryUnits()

        End If
        GetChart()

    End Sub

    Private Function CreateHTMLErrorMessage(ByVal msg As String) As String
        Dim txt As String = "<table width=672 height=368 border=0 align=center cellspacing=0 bordercolor=#76777C>" + _
                                  " <tr>" + _
                                      "<td height=35 align=center valign=top><p class=style1>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>" + msg + "</p></td>" + _
                                  "</tr>" + _
                                  "</table>"

        Return txt
    End Function


    'Private Sub C1WebChart1_DrawDataSeries(ByVal sender As Object, ByVal e As C1.Win.C1Chart.DrawDataSeriesEventArgs) Handles C1WebChart1.DrawDataSeries
    '    Dim ds As C1.Win.C1Chart.ChartDataSeries = sender

    '    Dim clr1 As Color = ds.LineStyle.Color
    '    Dim clr2 As Color = Color.White          'ds.SymbolStyle.Color
    '    C1WebChart1.ChartGroups(e.GroupIndex).ShowOutline = False
    '    'C1WebChart1.ChartGroups(e.GroupIndex).Use3D = True
    '    If (C1WebChart1.ChartGroups(e.GroupIndex).ChartType = C1.Win.C1Chart.Chart2DTypeEnum.Bar) AndAlso _
    '         (e.Bounds.Height > 0 And e.Bounds.Width > 0) Then
    '        Dim lgb As System.Drawing.Drawing2D.LinearGradientBrush = _
    '          New LinearGradientBrush(e.Bounds, clr1, clr2, LinearGradientMode.Horizontal)
    '        e.Brush = lgb
    '    End If


    'End Sub


    Private Sub InitializeComponent()

    End Sub

    Sub GetWindowsChart()
        ltMessage.Text = String.Empty

        If Not AreCalcsDone() Then
            ltMessage.Text = CreateHTMLErrorMessage("The report is not available because" + _
                                                   " the calculations for the requested time period" + _
                                                   "is not completed.")
            Exit Sub
        End If

        Dim aliasName As String
        If Not IsNothing(Request.Form("ListUnits")) Then
            aliasName = ListUnits.Items.FindByValue(Request.Form("ListUnits")).Text().Trim
        End If
        'Dim field1 As String = Request.QueryString("field1").Replace("UnitName", aliasName)
        Dim field1 As String = totField.Replace("UnitName", aliasName)
        Dim t As Integer


        If chrtData.Tables.Count = 0 Then
            ltMessage.Text = CreateHTMLErrorMessage("Select a process unit to see results.")
        ElseIf chrtData.Tables(0).Rows.Count = 0 Then
            ltMessage.Text = CreateHTMLErrorMessage("No data was found")
        Else

            'Dim plotter As New PlotData
            'C1WebChart1.Visible = True
            'C1WebChart1.LoadChartFromFile(Server.MapPath("Template\ChartTplt1.chart2dxml"))
            'C1WebChart1.ChartArea.AxisY.GridMajor.Visible = True
            'plotter.Plot(chrtData, C1WebChart1, 0, Request.QueryString("uom"), Request.QueryString("currency"), Request.QueryString("TwelveMonthAvg"), _
            '             Request.QueryString("target"), Request.QueryString("YTD"), field1, Request.QueryString("field2"), Request.QueryString("field3"), _
            '             Request.QueryString("field4"), Request.QueryString("field5"))

            PrepStackedColumnChart(chrtData.Tables(0))
        End If
    End Sub


    Private Sub PrepStackedColumnChart(dt As DataTable)
        Chart1.Visible = True

        Chart1.Legends.Add(New System.Web.UI.DataVisualization.Charting.Legend("Legend1"))
        Chart1.Legends("Legend1").Docking = System.Web.UI.DataVisualization.Charting.Docking.Bottom
        Chart1.Legends("Legend1").Alignment = Drawing.StringAlignment.Center ' .Center() ' = System.Web.UI.DataVisualization.Charting.Are
        'Or else use:
        'MyChart.Series[0].SmartLabelStyle.Enabled = false;
        'MyChart.Series[0].LabelAngle = 90; // Can vary from -90 to 90;

        Dim processedFirstDataColumn As Boolean = False
        Dim colorCounter As Integer = 1
        Dim columnCounter As Integer = 0
        For Each col As DataColumn In dt.Columns
            If columnCounter > 3 Then
                Dim name As String = col.ColumnName
                If name = "Year-To-Date" Then
                    NewSeries("YTD", dt, name, DataVisualization.Charting.SeriesChartType.Line, Color.Black, DataVisualization.Charting.MarkerStyle.Diamond, 10)
                ElseIf name = "Target" Then
                    Dim targetBlanks() As Object
                    Dim targetNumbers() As Object
                    Dim moreThanZero As Boolean = False
                    For count As Integer = 0 To dt.Rows.Count - 1
                        ReDim Preserve targetBlanks(count)
                        ReDim Preserve targetNumbers(count)
                        targetBlanks(count) = String.Empty
                        targetNumbers(count) = dt.Rows(count)(name)
                        If targetNumbers(count) > 0 Then moreThanZero = True
                    Next
                    If moreThanZero Then
                        Chart1.Series.Add("Target")
                        Chart1.Series("Target").ChartType = DataVisualization.Charting.SeriesChartType.Line
                        Chart1.Series("Target").Color = Drawing.Color.Red
                        Chart1.DataManipulator.IsStartFromFirst = True
                        Chart1.Series("Target").MarkerSize = 10
                        Chart1.Series("Target").MarkerStyle = DataVisualization.Charting.MarkerStyle.Square
                        Chart1.Series("Target").Points.DataBindXY(targetBlanks, targetNumbers)
                    End If
                ElseIf name = "Rolling Average" Then
                    NewSeries("RollingAverage", dt, name, DataVisualization.Charting.SeriesChartType.Point, Color.Black,
                              DataVisualization.Charting.MarkerStyle.Circle, 10)
                Else
                    If Not processedFirstDataColumn Then
                        'creaet the first data column
                        Dim dates() As Object '= ds.Tables(0).Rows(0).ItemArray()
                        Dim qtys() As Object '= ds.Tables(0).Rows(1).ItemArray()
                        For count As Integer = 0 To dt.Rows.Count - 1
                            ReDim Preserve dates(count)
                            ReDim Preserve qtys(count)
                            dates(count) = dt.Rows(count)(0)
                            qtys(count) = dt.Rows(count)(name)
                        Next
                        'first
                        'Chart1.Series(0).Name = name
                        'Chart1.Series.Add(name)
                        'MsgBox("Left off trying to not hardcode base bar chart to name 'SWB'")
                        Chart1.Series.Add(name)
                        Chart1.Series(name).ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
                        Chart1.Series(name).Color = System.Drawing.Color.MediumBlue
                        Chart1.Series(name).Points.DataBindXY(dates, qtys)
                        'rotate dates
                        Chart1.ChartAreas(0).AxisX.LabelStyle.Angle = -90
                        processedFirstDataColumn = True
                        colorCounter += 1
                    Else
                        'do the bind to data columns
                        NewSeries(name, dt, name, DataVisualization.Charting.SeriesChartType.StackedColumn, _
                          GetColor(colorCounter))
                        'BindDataToSeries(name, dt, name, GetColor(colorCounter))
                        colorCounter += 1
                    End If
                End If
            End If
            columnCounter += 1
        Next


        'BindDataToSeries("Turnarounds", dt, 5, Color.Yellow)

        'BindDataToSeries("OC", dt, 7, Color.Orange)

        'If Not dt.Columns.Count >= 9 Then Exit Sub
        'BindDataToSeries("AONVR", dt, 8, Color.BlanchedAlmond)

        'If Not dt.Columns.Count >= 10 Then Exit Sub
        'NewSeries("YTD", dt, 9, DataVisualization.Charting.SeriesChartType.Line, _
        '          Color.Black, DataVisualization.Charting.MarkerStyle.Diamond, 10)

        'If Not dt.Columns.Count >= 11 Then Exit Sub
        'Dim greaterThanZero As Boolean = False
        'Dim sngle As Single = 0.0
        'For count As Integer = 0 To dt.Rows.Count - 1
        '    If Convert.ToSingle(dt.Rows(count)(10)) > 0.0 Then
        '        NewSeries("Target", dt, 10, DataVisualization.Charting.SeriesChartType.Line, _
        '            Color.Red, DataVisualization.Charting.MarkerStyle.Square, 10)
        '        Exit For
        '    End If
        'Next
        'If Not dt.Columns.Count >= 12 Then Exit Sub
        'NewSeries("RollingAverage", dt, 11, DataVisualization.Charting.SeriesChartType.Point, _
        '          Color.Black, DataVisualization.Charting.MarkerStyle.Circle, 10)

    End Sub

    Private Function GetColor(counter As Integer) As Color
        Select Case counter
            Case 1
                Return Color.MediumBlue
            Case 2
                Return Color.Yellow
            Case 3
                Return Color.Green
            Case 4
                Return Color.Orange
            Case 5
                Return Color.BlanchedAlmond
            Case Else
                Throw New Exception("Need another case statement for counter " + counter.ToString() + " in ChartControlStahcked.aspx.vb.GetColor()")
        End Select
    End Function


    'Private Sub BindDataToSeries(seriesName As String, dt As DataTable, columnName As String, _
    '                           color As Drawing.Color)
    '    'Chart1.Series(seriesName).ChartType = chartType ' DataVisualization.Charting.SeriesChartType.Point
    '    Chart1.Series(seriesName).Color = color ' Drawing.Color.Black
    '    Chart1.DataManipulator.IsStartFromFirst = True
    '    Dim blanks() As Object
    '    Dim numbers() As Object
    '    For count As Integer = 0 To dt.Rows.Count - 1
    '        ReDim Preserve blanks(count)
    '        ReDim Preserve numbers(count)
    '        blanks(count) = String.Empty
    '        numbers(count) = dt.Rows(count)(columnName)
    '    Next
    '    'If Not IsNothing(size) Then
    '    '    Chart1.Series(name).MarkerSize = size ' 10
    '    'End If
    '    'If Not IsNothing(style) Then
    '    '    Chart1.Series(name).MarkerStyle = style ' DataVisualization.Charting.MarkerStyle.Circle
    '    'End If
    '    Chart1.Series(seriesName).Points.DataBindXY(blanks, numbers)
    'End Sub

    Private Sub NewSeries(name As String, dt As DataTable, columnName As String, chartType As DataVisualization.Charting.SeriesChartType, _
                               color As Drawing.Color, Optional style As DataVisualization.Charting.MarkerStyle = Nothing, _
                               Optional size As Integer = Nothing)

        Chart1.Series.Add(name)
        Chart1.Series(name).ChartType = chartType ' DataVisualization.Charting.SeriesChartType.Point
        Chart1.Series(name).Color = color ' Drawing.Color.Black
        Chart1.DataManipulator.IsStartFromFirst = True
        Dim blanks() As Object
        Dim numbers() As Object
        For count As Integer = 0 To dt.Rows.Count - 1
            ReDim Preserve blanks(count)
            ReDim Preserve numbers(count)
            blanks(count) = String.Empty
            numbers(count) = dt.Rows(count)(columnName)
        Next
        If Not IsNothing(Size) Then
            Chart1.Series(name).MarkerSize = Size ' 10
        End If
        If Not IsNothing(style) Then
            Chart1.Series(name).MarkerStyle = style ' DataVisualization.Charting.MarkerStyle.Circle
        End If
        Chart1.Series(name).Points.DataBindXY(blanks, numbers)
    End Sub

End Class
