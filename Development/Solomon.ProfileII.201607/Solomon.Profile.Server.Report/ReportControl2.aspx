﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportControl2.aspx.vb" Inherits="Solomon.Profile.Server.ReportControls.ReportControl2" %>

<HTML>
	<HEAD>
		<title></title>
		<!--DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"-->
		<!--META http-equiv="PRAGMA" content="NO-CACHE"-->
		<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style>
			.small { FONT-SIZE: 12px; FONT-FAMILY: tahoma }
		</style>
		<LINK media="print" href="print.css" type="text/css" rel="StyleSheet">
	</HEAD>
	<body style="PAGE-BREAK-BEFORE: auto" MS_POSITIONING="GridLayout" color="#FFFFFF">
		<form id="Form1" method="post" runat="server">
			<table border="0">
				<tr>
					<td valign="bottom">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-REPEAT: no-repeat" width="350" background="ReportBGLeft.gif" bgColor="white">
									<DIV id="LabelName" style="DISPLAY: inline; FONT-WEIGHT: bold; FONT-SIZE: 12pt; Z-INDEX: 106; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px"
										align="center">
										<%
										' This section added by GMO 12/6/2007
										' Code behind function invoked in order to get long description
										' for the report title.  Used only for process unit reports.
										    Select Case HttpContext.Current.Request.QueryString("rn").ToUpper
										        Case "CDU REPORT", "FCC REPORT", "HYC REPORT", "REF REPORT", "PXYL REPORT"
										            HttpContext.Current.Response.Write(GetReportTitle(HttpContext.Current.Request.QueryString("rn")))
										        Case Else
										            ' END GMO 12/6/2007
										            HttpContext.Current.Response.Write(HttpContext.Current.Request.QueryString("rn"))
										    End Select
										
										%>
									</DIV>
									<br>
									<DIV id="LabelMonth" style="DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px"
										align="center"><%= IIf(HttpContext.Current.Request.QueryString("rn") = "Gross Product Value", Format(CDate(HttpContext.Current.Request.QueryString("sd")), "MMMM yyyy"), Request.QueryString("yr") + " Methodology")%>
									</DIV>
									<br>
									<DIV id="LabelDataFilter" style="DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px"
										align="center">
										<%
										    Dim bYTD As Boolean = CType(HttpContext.Current.Request.QueryString("ytd"), Boolean)
										    Dim bAVG As Boolean = CType(HttpContext.Current.Request.QueryString("avg"), Boolean)

										    If HttpContext.Current.Request.QueryString("rn") = "Refinery Trends Report" Then
										        If (bYTD = True And bAVG = True) Or (bYTD = False And bAVG = False) Then
										            ' show total fields only
										            HttpContext.Current.Response.Write("Current Month")
										        Else
										            If bYTD = True Then
										                ' show ytd fields only
										                HttpContext.Current.Response.Write("Year To Date")
										            Else
										                If bAVG = True Then
										                    ' show avg fields only
										                    HttpContext.Current.Response.Write("Rolling Average")
										                End If
										            End If
										        End If
										    Else
										        HttpContext.Current.Response.Write(GetCoLoc())
										    End If
										%>
									</DIV>
								</td>
								<TD></TD>
								<td width="42%">
									<div align="right"><IMG height="50" alt="" src="SA logo RECT.jpg" width="300"><br>
									</div>
									<DIV id="DIV1" style="FONT-SIZE: 10pt; FONT-FAMILY: Tahoma; HEIGHT: 16px" align="right">
										<%=  Date.Today.ToLongDateString()%>
									</DIV>
								</td>
							</tr>
						</table>
						<hr width="100%" color="#000000" SIZE="1">
					</td>
				</tr>
				<tr>
					<td><asp:placeholder id="phReport" runat="server"></asp:placeholder>
						<%GetReport()%>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
