<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)
ShowIf(Ytd,IfYtdOn)
ShowIf(Avg,IfAvgOn)

<table height="100%" width="100%" border=0>
  <!--tr>
    <td width=5></td>
    <td></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
  </tr-->

<tr>
<!-- CURRENT -->

<td valign="top">
<table class="small" border=0>
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>ReportPeriod<b></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC)
  INCLUDETABLE(GENSUM)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom ><strong>@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td nowrap>@ChartTitle<span align=left>USEUNIT(AxisLabelUS,AxisLabelMetric)</span></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
 </table>
</td>

<!-- PREVIOUS -->
<td valign="top">
 <table class="small" border=0>
 <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>PreviousPeriod<b></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC_FIRST_PREVIOUS)
  INCLUDETABLE(GENSUM_FIRST_PREVIOUS)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
 </table>
</td>

<!--- PREVIOUS -1 -->
<td valign="top">
 <table class="small" border=0>
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>SecondPeriod<b></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC_SECOND_PREVIOUS)
  INCLUDETABLE(GENSUM_SECOND_PREVIOUS)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
  </table>
</td>

<!--- PREVIOUS -2 -->
<td valign="top">
 <table class="small" border=0>
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><b>ThirdPeriod<b></td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC_THIRD_PREVIOUS)
  INCLUDETABLE(GENSUM_THIRD_PREVIOUS)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong style="display:none">@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td></td>
    <td align=right>&nbsp; NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
  </tr>
  END
  </table>
</td>


</tr>
 
  
 
</table>
<!-- template-end -->
