<!-- config-start -->

<!-- config-end -->
<!-- template-start -->

<table class='small' border=0 width=100%>
	<tr>
		<td width=3%></td>
		<td width=3%></td>
		<td width=3%></td>
		<td width=400></td>
		<td width=12%></td>
		<td width=4%></td>
		<td width=11%></td>
		<td width=5%></td>
		<td width=10%></td>
		<td width=6%></td>
	</tr>
	<tr>
		<td colspan=4></td>
		<td colspan=6 align=center><strong>--------------- Fuels Refinery Costs ---------------</strong></td>
	</tr>
	<tr>
		<td colspan=4></td>
		<td colspan=2 align=center><strong>(CurrencyCode/1000)</strong></td>
		<td colspan=2 align=center><strong>(100 CurrencyCode/bbl)</strong></td>
		<td colspan=2 align=center><strong>(100 CurrencyCode/UEDC)</strong></td>
	</tr>
	<tr>
		<td colspan=10><strong>ReportPeriod</strong></td>
	</tr>
	<tr>
		<td></td>
		<td colspan=9 height=30 valign=bottom><strong>Non-Volume-Related Expenses</strong></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=8 height=30 valign=bottom>Salaries & Wages</td>
	</tr>
	<tr>
		<td colspan=3></td>
		<td>Operator,Craft & Clerical</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OCCSal,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OCCSal,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OCCSal,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3></td>
		<td>Management,Professional & Staff</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(MPSSal,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(MPSSal,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(MPSSal,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=8 height=30 valign=bottom>Employee Benefits</td>
	</tr>
	<tr>
		<td colspan=3></td>
		<td>Operator,Craft & Clerical</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OCCBen,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OCCBen,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OCCBen,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3></td>
		<td>Management,Professional & Staff</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(MPSBen,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(MPSBen,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(MPSBen,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2 height=30 valign=bottom>Maintenance Materials</td>
		<td align=right valign=bottom>Section(Opex, DataType='ADJ',) BEGIN Format(MaintMatl,'#,##0.0') END </td>
		<td></td>
		<td align=right valign=bottom>Section(Opex, DataType='C/BBL',) BEGIN Format(MaintMatl,'#,##0.0') END </td>
		<td></td>
	    	<td align=right valign=bottom>Section(Opex, DataType='UEDC',) BEGIN Format(MaintMatl,'#,##0.0') END </td>
		<td></td>
	</tr>

	<tr>
		<td colspan=2></td>
		<td colspan=2>Contract Maintenance Labor Expense</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(ContMaintLabor,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(ContMaintLabor,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(ContMaintLabor,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Other Contract Services</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OthCont,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OthCont,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OthCont,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Turnaround Adjustment</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(TAAdj,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(TAAdj,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(TAAdj,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Environmental Expenses</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(Envir,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(Envir,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(Envir,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Other Non-Volume-Related Expenses</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OthNonVol,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OthNonVol,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OthNonVol,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Utilized G&A Personnel Cost</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(GAPers,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(GAPers,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(GAPers,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3></td>
		<td><strong>Subtotal Non-Volume-Related Expenses</strong></td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(STNonVol,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(STNonVol,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(STNonVol,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td colspan=9 height=30 valign=bottom><strong>Volume-Related Expenses</strong></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td height=30 valign=bottom colspan=2>Chemicals</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(Chemicals,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(Chemicals,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(Chemicals,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Catalysts</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(Catalysts,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(Catalysts,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(Catalysts,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Purchased Energy</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(PurchasedEnergy,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(PurchasedEnergy,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(PurchasedEnergy,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Produced Energy</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(ProducedEnergy,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(ProducedEnergy,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(ProducedEnergy,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Purchased Energy Other than Electric and Steam</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(PurOth,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(PurOth,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(PurOth,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td colspan=2>Other Volume-Related Expenses</td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OthVol,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OthVol,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OthVol,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3></td>
		<td><strong>Subtotal Volume-Related Expenses</strong></td>
		<td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(STVol,'#,##0.0') END </td>
		<td></td>
		<td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(STVol,'#,##0.0') END </td>
		<td></td>
	    	<td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(STVol,'#,##0.0') END </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3></td>
		<td height=30 valign=bottom><strong>Total Cash Operating Expenses</strong></td>
		<td align=right valign=bottom>Section(Opex, DataType='ADJ',) BEGIN Format(TotCashOpEx,'#,##0.0') END </td>
		<td></td>
		<td align=right valign=bottom>Section(Opex, DataType='C/BBL',) BEGIN Format(TotCashOpEx,'#,##0.0') END </td>
		<td></td>
	    	<td align=right valign=bottom>Section(Opex, DataType='UEDC',) BEGIN Format(TotCashOpEx,'#,##0.0') END </td>
		<td></td>

	</tr>
</table>

<!-- template-end -->