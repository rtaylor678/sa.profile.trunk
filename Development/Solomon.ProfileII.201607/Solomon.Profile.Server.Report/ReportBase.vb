Imports System.Data.SqlClient
Imports System.IO

Public Class ReportBase
    Inherits System.Web.UI.UserControl

    Protected _refineryid As String

    Public Property RefineryID()
        Get
            Return _refineryID
        End Get
        Set(ByVal Value As Object)
            _refineryid = Value
        End Set
    End Property



    Protected Function SelectDistinct(ByVal TableName As String, ByVal SourceTable As DataTable, ByVal FieldName As String, Optional ByVal SortField As String = "") As DataTable
        Dim dt As DataTable = New DataTable(TableName)
        dt.Columns.Add(FieldName, SourceTable.Columns(FieldName).DataType)
        Dim key() As DataColumn = {dt.Columns(0)}
        dt.PrimaryKey = key
        Dim dr As DataRow

        If SortField.Trim.Length = 0 Then
            SortField = FieldName
        End If

        For Each dr In SourceTable.Select("", SortField)
            If Not dt.Rows.Contains(dr(FieldName)) Then
                dt.Rows.Add(New Object() {dr(FieldName)})
            End If
        Next
        Return dt

    End Function


    'Find similar words at the beginning sub heads the second string
    Protected Function SubHead(ByVal rt As String, ByVal lt As String) As String
        Dim rArray() As String = rt.Split
        Dim lArray() As String = lt.Split
        Dim u As Integer
        Dim stopAt As Integer = rArray.Length - 1
        Dim foundMatch As Boolean = False

        'Stop at the shorter string 
        If lArray.Length - 1 < stopAt Then
            stopAt = lArray.Length - 1
        End If

        For u = 0 To stopAt
            If rArray(u) = lArray(u) Then
                If u > 0 Then
                    If rArray(u - 1) <> lArray(u - 1) Then
                        Exit For
                    End If
                End If

                lt = lt.Replace(rArray(u), String.Empty)
                foundMatch = True
            End If
        Next

        If foundMatch Then
            Return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + _
                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + lt
        Else
            Return lt
        End If

    End Function

    Protected Function SubHeadIgnoreFirst(ByVal rt As String, ByVal lt As String) As String
        Dim rArray() As String = rt.Split
        Dim lArray() As String = lt.Split
        Dim u As Integer
        Dim stopAt As Integer = rArray.Length - 1
        Dim foundMatch As Boolean = False

        'Stop at the shorter string 
        If lArray.Length - 1 < stopAt Then
            stopAt = lArray.Length - 1
        End If

        For u = 0 To stopAt
            If rArray(u) = lArray(u) Then
                If u > 0 Then
                    If rArray(u - 1) <> lArray(u - 1) Then
                        Exit For
                    End If
                    lt = lt.Replace(rArray(u - 1), String.Empty)
                    lt = lt.Replace(rArray(u), String.Empty)
                    foundMatch = True
                End If

            End If
        Next

        If foundMatch Then
            Return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + _
                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + lt
        Else
            Return lt
        End If

    End Function
End Class
