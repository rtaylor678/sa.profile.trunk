<!-- config-start -->

<!-- config-end -->
<!-- template-start -->


<table class=small border=0>
  <tr>
    <td width=300></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=80></td>
  </tr>

  <tr>
    <td><img src="https://webservices.solomononline.com/RefineryReports/Templates/ChevronLogo.gif" align=left></td>
    <td Colspan=9><img src="https://webservices.solomononline.com/RefineryReports/Templates/ChevronTag.gif" align=right></td>
  </tr>


  <tr>
    <td height=50 colspan=10 align=Left valign=bottom><strong>Company KPI Scorecard</strong></td>
  </tr>

  <tr>
    <td align=left colspan=10 valign=bottom>Rolling Average&nbsp;&nbsp;&nbsp;::&nbsp;&nbsp;&nbsp;ReportPeriod</td>
  </tr>

  <tr>
    <td align=left valign=bottom height=50>Refinery Abbreviation</td>
    <td align=right valign=bottom>BU&nbsp;</td>
    <td align=right valign=bottom>CT&nbsp;</td>
    <td align=right valign=bottom>ES&nbsp;</td>
    <td align=right valign=bottom>HI&nbsp;</td>
    <td align=right valign=bottom>PA&nbsp;</td>
    <td align=right valign=bottom>RI&nbsp;</td>
    <td align=right valign=bottom>SL&nbsp;</td>
    <td align=right valign=bottom><strong>Chevron<br>Total&nbsp;&nbsp;</br></strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  SECTION(CVX_AvgKPI,,)  
  BEGIN

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Refinery Utilization, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_UtilPcnt_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_UtilPcnt_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_UtilPcnt_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_UtilPcnt_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_UtilPcnt_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_UtilPcnt_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_UtilPcnt_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_UtilPcnt_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UEDC&trade;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KUEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KUEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KUEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KUEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KUEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KUEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KUEDC_Avg,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KUEDC_Avg,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDC&trade;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KEDC_Avg,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KEDC_Avg,0)</strong></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Mechanical Availability, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_MechAvail_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_MechAvail_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_MechAvail_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_MechAvail_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_MechAvail_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_MechAvail_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_MechAvail_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_MechAvail_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MA-EDC&sup1;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KMAEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KMAEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KMAEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KMAEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KMAEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KMAEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KMAEDC_Avg,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KMAEDC_Avg,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process EDC&trade;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KProcEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KProcEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KProcEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KProcEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KProcEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KProcEDC_Avg,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KProcEDC_Avg,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KProcEDC_Avg,0)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Energy Intensity Index&reg; (EII&reg;)</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_EII_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_EII_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_EII_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_EII_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_EII_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_EII_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_EII_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_EII_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Daily Energy Usage, GBtu/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KEnergyUseDay_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KEnergyUseDay_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KEnergyUseDay_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KEnergyUseDay_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KEnergyUseDay_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KEnergyUseDay_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KEnergyUseDay_Avg,1))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KEnergyUseDay_Avg,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Standard Energy, GBtu/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KTotStdEnergy_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KTotStdEnergy_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KTotStdEnergy_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KTotStdEnergy_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KTotStdEnergy_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KTotStdEnergy_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KTotStdEnergy_Avg,1))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KTotStdEnergy_Avg,1)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Maintenance Index, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_MaintIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_MaintIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_MaintIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_MaintIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_MaintIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_MaintIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_MaintIndex_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_MaintIndex_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Turnaround Index, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_TAIndex_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_TAIndex_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround Index, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_RoutIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_RoutIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_RoutIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_RoutIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_RoutIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_RoutIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_RoutIndex_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_RoutIndex_Avg,2)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Equivalent Personnel&sup2;</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_TotEqPEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_TotEqPEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_TotEqPEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_TotEqPEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TotEqPEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_TotEqPEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_TotEqPEDC_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_TotEqPEDC_Avg,2)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Non-Energy OpEx&sup3;, US &cent;/UEDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_NEOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_NEOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_NEOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_NEOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NEOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_NEOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_NEOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_NEOpexUEDC_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Total OpEx&sup3;, US &cent;/UEDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_TotCashOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_TotCashOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_TotCashOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_TotCashOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TotCashOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_TotCashOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_TotCashOpexUEDC_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_TotCashOpexUEDC_Avg,2)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Net Cash Margin, US $/bbl</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_CashMargin_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_CashMargin_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_CashMargin_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_CashMargin_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_CashMargin_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_CashMargin_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_CashMargin_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_CashMargin_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Net Input Barrels, kb/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_NetInputBPD_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_NetInputBPD_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_NetInputBPD_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_NetInputBPD_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NetInputBPD_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_NetInputBPD_Avg,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_NetInputBPD_Avg,1))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_NetInputBPD_Avg,0)</strong></td>
  </tr>

  END  

  <tr>
    <td height=75 colspan=10 valign=bottom>&sup1; Amount of process EDC&trade; that is mechanically available for the period</td>
  </tr>

  <tr>
    <td colspan=10 valign=bottom>&sup2; Equivalent Personnel per 100,000 EDC; 1 person = 2080 hours/year</td>
  </tr>

  <tr>
    <td colspan=10 valign=bottom>&sup3; OpEx = total refinery operating expenses</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td colspan=10 valign=bottom>&nbsp;&nbsp;General note: calculations may not add up due to the number of significant digits being displayed</td>  
  </tr>

</table>
<!-- template-end -->
