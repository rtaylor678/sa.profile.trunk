<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)


<table class='small' width="100%" border="0">
  <tr>
    <td width=5></td>
    <td width=5></td>
    <td width=5></td>
    <td width=340></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
SECTION(ProcessUnits,,)
BEGIN 
HEADER('
<tr>
    <td colspan=4></td>
    <td colspan=2 align=center valign=bottom><b>Format(PeriodStart,'MM.yyyy')<b></td>
    <td colspan=4></td>
  </tr>
')
END

<!--  <tr> -->
<!--    <td colspan=4></td> -->
<!--    <td colspan=2 align=center valign=bottom><strong>ReportPeriod</strong></td> -->
<!----    <td colspan=2 align=center valign=bottom><strong>Process Target</strong></td> -->
<!--    <td colspan=4></td> -->
<!--  </tr> -->
  <tr>
    <td colspan=12><strong>Технологические установки</strong></td>
  </tr>
  SECTION(ProcessUnits,,)
  BEGIN
  Header('<tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>@DescriptionRussian</strong></td>
  </tr>')
  <tr>
    <td colspan=2></td>
    <td colspan=2 height=30 valign=bottom>Название установки</td>
    <td colspan=8 height=30 valign=bottom>@UnitName</td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Идентификатор технологического процесса</td>
    <td colspan=8 valign=bottom>@ProcessID</td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Тип технологического процесса</td>
    <td colspan=8 valign=bottom>@ProcessType</td>
  </tr>
  <tr>	
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Производительность, @CapUnitsRussian </td>
    <td valign=bottom align=right>Format(Cap,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>ЭДП</td>
    <td valign=bottom align=right>Format(EDC,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>ИЭДП</td>
    <td valign=bottom align=right>Format(UEDC,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Используемая производительность, %</td>
    <td valign=bottom align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(UtilPcnt_Target,'#,##0.0')))</td>
    <td colspan=5></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Механическая готовность, %</td>
    <td valign=bottom align=right>Format(MechAvail,'#,##0.0')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(MechAvail_Target,'#,##0.0')))</td>
    <td colspan=5></td>
  </tr>
   <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Эксплуатационная готовность, %</td>
    <td valign=bottom align=right>Format(OpAvail,'#,##0.0')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(OpAvail_Target,'#,##0.0')))</td>
    <td colspan=5></td>
  </tr> 
   <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Затраты на капитальные ремонты, руб./барр.</td>
    <td valign=bottom align=right>NoShowZero(Format(TACost,'N'))</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(TACost_Target,'N')))</td>
    <td colspan=5></td>
   </tr> 
 END

  <tr>
    <td colspan=12></td>
  </tr>
  <tr>
    <td colspan=4></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>Производительность</strong></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>Используемая</br>производительность, %</strong></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>ЭДП</strong></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>ИЭДП</strong></td>
  </tr>
  <tr>
    <td colspan=12><strong>энергетических систем и внешних объектов</strong></td>
  </tr>
   
  SECTION(Generated, ProcessID='STEAMGEN' ,)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>Генерация пара,@CapUnitsRussian</strong></td>
  </tr>
  ')
  END
  
  SECTION(Generated,ProcessID='STEAMGEN' AND ProcessType='SFB',)
   BEGIN 
  <tr>
    <td colspan=2></td>
    <td colspan=2>Котлы на твердом топливе</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
   END
   
  SECTION(Generated, ProcessID='STEAMGEN' AND ProcessType='LGFB',)
   BEGIN 
  <tr>
    <td colspan=2></td>
    <td colspan=2>Котлы на жидком топливе и газе</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  
  SECTION(Generated, ProcessID='ELECGEN' ,)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>Генерация электроэнергии, кВт</strong></td>
  </tr>
  ')
  END
  
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='SFB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>котлами на твердом топливе</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='LGFB',)
  BEGIN
   <tr>
    <td colspan=2></td>
    <td colspan=2>котлами на жидк. топливе и газе</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr> 
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='DSL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>дизельным двигателем</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
<!---
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='FT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Fired Turbine Driver (No Steam Produced)</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
--->
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='EXP',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>турбиной-экспандером</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='STT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>предвключ. паровой турбиной</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='EXTR',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>турбиной с регулируемым отбором</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr> 
   END
  
SECTION(Generated,ProcessID='FCCPOWER',)
BEGIN
<tr>
    <td></td>
    <td colspan=3 valign=bottom height = 30>Система утилиз. энергии дым. газов кат. крекинга - эфф. мощность на валу, л.с.</td>
    <td valign=bottom align=right valign=bottom height = 30>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right valign=bottom height = 30>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right valign=bottom height = 30>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right valign=bottom height = 30>Format(UEDC,'#,##0')</td>
    <td></td>
</tr>
END

  <tr>
    <td colspan=12></td>
  </tr>
  <tr>
    <td colspan=4></td>
    <td height=30 valign=bottom colspan=2 align=center valign=bottom><strong>барр.</strong></td>
    <td height=30 valign=bottom colspan=2 align=center valign=bottom><strong>ЭДП</strong></td>
    <td height=30 valign=bottom colspan=2 align=center valign=bottom><strong>ИЭДП</strong></td>
    <td colspan=2></td>
  </tr>

  SECTION(RawMaterial,ProcessID='RSCRUDE',)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11><strong>Приемка сырья</strong></td>
  </tr>
  ')
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='RAIL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Железнодорожные и автоцистерны</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='TT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Автоцистерны</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='TB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Терминал для танкеров</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='OMB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Оффшорный причальный буй</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='BB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Терминал для барж</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='PL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Pipeline</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  
  SECTION(RawMaterial,ProcessID='RSPROD',)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>Отгрузка продукции</strong></td>
  </tr>')
  END
  
  SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='RAIL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Железнодорожные цистерны</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='TT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Автоцистерны</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='TB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Терминал для танкеров</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='OMB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Оффшорный причальный буй</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='BB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Терминал для барж</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='PL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Pipeline</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
</table>
<!-- template-end -->