<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small width=900 border=0>
SECTION(ProcessUnits,,)
BEGIN 
HEADER('
  <tr>
    <td colspan=14 valign=bottom><strong>Format(PeriodStart,'MM.yyyy')</strong></td>
  </tr>
')
END
<!--
  <tr>
    <td colspan=14 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
-->
  <tr>
    <td width=5></td>
    <td width=200></td>
    <td width=10></td>
    <td width=70></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
  </tr>
  <tr>
    <td colspan=4></td>
    <td colspan=6 align=center valign=bottom><strong>------ Стандарты эффективности персонала ------</strong></td>
    <td colspan=4 align=center valign=bottom><strong>-- Стандарт эффективности затрат --</td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Название установки</strong></td>  <!-- Unit Name -->
    <td colspan=2 align=center valign=bottom><strong>Идентификатор</br>технологического</br>процесса</strong></td>  <!-- ProcessID -->
    <td colspan=2 align=center valign=bottom><strong>Ремонтный</br>тыс. раб. часов</strong></td>  <!-- mPEI Standard, k -->
    <td colspan=2 align=center valign=bottom><strong>Неремонтный</br>тыс. раб. часов</strong></td>  <!-- nmPEI Standard, k -->
    <td colspan=2 align=center valign=bottom><strong>Всего</br>тыс. раб. часов</strong></td>   <!-- PEI Standard, k -->
    <td colspan=2 align=center valign=bottom><strong>ремонтных,</br>тыс. $</strong></td>  <!-- MEI Standard, k -->
    <td colspan=2 align=center valign=bottom><strong>неэнерг.,</br>тыс. $</strong></td>   <!-- NEI Standard, k -->
  </tr>

  SECTION(ProcessUnits, ProcessGrouping = 'Process', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 valign=bottom><strong>Вспомогательные установки</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Ancillary', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 valign=bottom><strong>Простаивающие установки</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Idle', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 height=30 valign=bottom><strong>Внешние объекты, вспомогательные и энергетические системы</strong></td>
  </tr>

  SECTION(ProcessUnits,ProcessGrouping = 'Utility', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitNameRussian</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(mPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(nmPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

SECTION(UnitsOffsites,,)
  BEGIN
 
  <tr>
    <td></td>
    <td colspan=3>Приемка сырья и Отгрузка продукции</td>
    <td align=right>Format(TotRSMaintPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSNonMaintPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSMaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSNEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>

  <tr>
    <td height=30 valign=bottom colspan=4><strong>Все топливное производство</strong></td>
    <td align=right valign=bottom><strong>Format(MaintPersEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(NonMaintPersEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(PersEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(MaintEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(NEOpexEffDiv,'#,##0')</strong></td>
    <td></td>
  </tr>
  END
 
</table>

<!-- template-end -->