<!-- config-start -->

<!-- config-end -->
<!-- template-start -->


<table class=small border=0 style="background:url(https://webservices.solomononline.com/RefineryReports/Templates/Valero.jpg) no-repeat;">
  <tr>
    <td width=200></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
  </tr>

  <tr>
    <td height=140 Colspan=13 align=Left valign=bottom><strong>Business Unit Review Scorecard</strong></td>
  </tr>

  <tr>
    <td align=left Colspan=13 valign=bottom>Current Month&nbsp;&nbsp;&nbsp;::&nbsp;&nbsp;&nbsp;ReportPeriod</td>
  </tr>

  <tr>
    <td align=left valign=bottom height=50>Refinery Abbreviation</td>
    <td align=right valign=bottom>AD&nbsp;</td>
    <td align=right valign=bottom>BN&nbsp;</td>
    <td align=right valign=bottom>CC&nbsp;</td>
    <td align=right valign=bottom>HO&nbsp;</td>
    <td align=right valign=bottom>MK&nbsp;</td>
    <td align=right valign=bottom>MP&nbsp;</td>
    <td align=right valign=bottom>PA&nbsp;</td>
    <td align=right valign=bottom>QB&nbsp;</td>
    <td align=right valign=bottom>SC&nbsp;</td>
    <td align=right valign=bottom>TC&nbsp;</td>
    <td align=right valign=bottom>TR&nbsp;</td>
    <td align=right valign=bottom>WM&nbsp;</td>
    <td align=right valign=bottom><strong>Valero<br>Total&nbsp;</br></strong></td>
  </tr>

  SECTION(BURMonthly,,)  
  BEGIN
  <tr>
    <td height=25 valign=bottom>&nbsp;&nbsp;&nbsp;Reliability Metrics</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mechanical Availability, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_MechAvail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_MechAvail,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_MechAvail,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mechanical Unavailability, %</td>
  </tr>

  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_TAMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_TAMechUnavail,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_TAMechUnavail,1)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_RoutMechUnavail,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_RoutMechUnavail,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_RoutMechUnavail,1)</strong></td>
  </tr>
  
  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maint. Index&sup1;, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_AdjMaintIndex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_AdjMaintIndex,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_AdjMaintIndex,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refinery Utilization, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_UtilPcnt,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_UtilPcnt,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_UtilPcnt,1)</strong></td>
  </tr>

  <tr>
    <td height=25 valign=bottom>&nbsp;&nbsp;&nbsp;Efficiency Metrics</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OpEx less Energy, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_NEOpexEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_NEOpexEDC,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_NEOpexEDC,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Energy Intensity Index&reg;</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_EII,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_EII,0))</td>
    <td align=right valign=bottom><strong>Format(@Valero_EII,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pers. Index&sup1;, wk hr/100 EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_TotWHrEDC,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_TotWHrEDC,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_TotWHrEDC,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contractor Headcount&sup2;, #</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_NumContPers,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_NumContPers,0))</td>
    <td align=right valign=bottom><strong>Format(@Valero_NumContPers,0)<strong></td>
  </tr>

  <tr>
    <td height=25 valign=bottom>&nbsp;&nbsp;&nbsp;Volumetric Metrics</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refinery Throughput, kd/d</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_NetInputKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_NetInputKBPD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_NetInputKBPD,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Crude Charge Rate, kb/d</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_CrudeKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_CrudeKBPD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_CrudeKBPD,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FCC Charge Rate, kb/d</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_FCCRateKBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_FCCRateKBPD,1))</td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@Valero_FCCRateKBPD,0))</strong></td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDC, k � refinery; M � company</span></td>
    <td align=right valign=top>NoShowZero(Format(@AD_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@BN_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@CC_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@HO_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@MK_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@MP_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@PA_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@QB_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@SC_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@TC_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@TR_kEDC,0))</td>
    <td align=right valign=top>NoShowZero(Format(@WM_kEDC,0))</td>
    <td align=right valign=top><strong>Format(@Valero_MEDC,1)</strong></td>
  </tr>
  END  

  <tr>
    <td height=100 Colspan=13 valign=bottom>&sup1; Maint. Index = Maintenance Index; Pers. Index = Personnel Index</td>
  </tr>

  <tr>
    <td Colspan=13 valign=bottom>&sup2; Contractor headcount in Profile includes both on-sites and off-sites</td>
  </tr>
</table>
<!-- template-end -->
