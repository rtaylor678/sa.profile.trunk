<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small border=0>
<!--
  <tr>
    <td colspan=8 valign=bottom><strong>Последнего капитального ремонта as of ReportPeriod</strong></td>
  </tr>
-->
SECTION(Turnarounds,,)
BEGIN 
HEADER('
  <tr>
    <td colspan=8 valign=bottom><strong>Последний капитальный ремонта по состоянию на Format(PeriodStart,'MM.yyyy')</strong></td>
  </tr>
')
END

  <tr>
    <td width=200></td>
    <td width=80></td>
    <td width=80></td>
    <td width=60></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
  </tr>
  <tr>
    <td valign=bottom><strong>Название установки</strong></td> <!-- Unit Name -->
    <td align=center valign=bottom><strong>Дата начала</br>последнего</br>кап. ремонта</strong></td> <!-- TA Date -->
    <td align=center valign=bottom><strong>Дата начала</br>предыдущего</br>кап. ремонта</strong></td> <!-- Previous TA Date -->
    <td align=center valign=bottom><strong>Срок простоя</br>установки</br>в связи с</br>кап. ремонтами,</br>часы</strong></td>  <!-- Hours Down -->
    <td align=center valign=bottom><strong>Полные расходы</strong></td> <!-- Cost -->
    <!-- <td align=center valign=bottom><strong>Валюта</strong></td>  Currency -->
    <td align=center valign=bottom><strong>Затраты времени</br>штатного персонала,</br>часы</strong></td> <!-- Company Work Hours -->
    <td align=center valign=bottom><strong>Затраты времени</br>персонала,</br>работающего</br>по договорам,</br>часы</strong></td> <!-- Contract Work Hours -->
  </tr>



  SECTION(Turnarounds,, SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;@UnitName</td>
    <td align=center>Format(TADate,'dd.MM.yyyy')</td>
    <td align=center>Format(PrevTADate,'dd.MM.yyyy')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>
    <td align=right>Format(TACostLocal,'#,##0')</td>
    <!-- <td align=center>&nbsp;@TACurrency</td> -->
    <td align=right>Format(TACompWHr,'#,##0')</td>
    <td align=right>Format(TAContWHr,'#,##0')</td>
  </tr>
  END

</table>

<!-- template-end -->
