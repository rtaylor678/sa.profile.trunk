<!-- config-start -->

<!-- config-end -->
<!-- template-start -->


<table class=small border=0>
  <tr>
    <td width=300></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=80></td>
  </tr>

  <tr>
    <td><img src="https://webservices.solomononline.com/RefineryReports/Templates/ChevronLogo.gif" align=left></td>
    <td Colspan=9><img src="https://webservices.solomononline.com/RefineryReports/Templates/ChevronTag.gif" align=right></td>
  </tr>


  <tr>
    <td height=50 colspan=10 align=Left valign=bottom><strong>Company KPI Scorecard</strong></td>
  </tr>

  <tr>
    <td align=left colspan=10 valign=bottom>Current Month&nbsp;&nbsp;&nbsp;::&nbsp;&nbsp;&nbsp;ReportPeriod</td>
  </tr>

  <tr>
    <td align=left valign=bottom height=50>Refinery Abbreviation</td>
    <td align=right valign=bottom>BU&nbsp;</td>
    <td align=right valign=bottom>CT&nbsp;</td>
    <td align=right valign=bottom>ES&nbsp;</td>
    <td align=right valign=bottom>HI&nbsp;</td>
    <td align=right valign=bottom>PA&nbsp;</td>
    <td align=right valign=bottom>RI&nbsp;</td>
    <td align=right valign=bottom>SL&nbsp;</td>
    <td align=right valign=bottom><strong>Chevron<br>Total&nbsp;&nbsp;</br></strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  SECTION(CVX_KPI,,)  
  BEGIN

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Refinery Utilization, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_UtilPcnt,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_UtilPcnt,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_UtilPcnt,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_UtilPcnt,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_UtilPcnt,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_UtilPcnt,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_UtilPcnt,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_UtilPcnt,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UEDC&trade;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KUEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KUEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KUEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KUEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KUEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KUEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KUEDC,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KUEDC,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDC&trade;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KEDC,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KEDC,0)</strong></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Mechanical Availability, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_MechAvail,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_MechAvail,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_MechAvail,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_MechAvail,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_MechAvail,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_MechAvail,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_MechAvail,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_MechAvail,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MA-EDC&sup1;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KMAEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KMAEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KMAEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KMAEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KMAEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KMAEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KMAEDC,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KMAEDC,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process EDC&trade;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KProcEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KProcEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KProcEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KProcEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KProcEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KProcEDC,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KProcEDC,0))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KProcEDC,0)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Energy Intensity Index&reg; (EII&reg;)</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_EII,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_EII,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_EII,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_EII,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_EII,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_EII,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_EII,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_EII,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Daily Energy Usage, GBtu/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KEnergyUseDay,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KEnergyUseDay,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KEnergyUseDay,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KEnergyUseDay,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KEnergyUseDay,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KEnergyUseDay,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KEnergyUseDay,1))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KEnergyUseDay,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Standard Energy, GBtu/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_KTotStdEnergy,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_KTotStdEnergy,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_KTotStdEnergy,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_KTotStdEnergy,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_KTotStdEnergy,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_KTotStdEnergy,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_KTotStdEnergy,1))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_KTotStdEnergy,1)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Maintenance Index, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_AdjMaintIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_AdjMaintIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_AdjMaintIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_AdjMaintIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_AdjMaintIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_AdjMaintIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_AdjMaintIndex,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_AdjMaintIndex,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Turnaround Index, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_TAIndex_Avg,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_TAIndex_Avg,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_TAIndex_Avg,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround Index, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_RoutIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_RoutIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_RoutIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_RoutIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_RoutIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_RoutIndex,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_RoutIndex,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_RoutIndex,2)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Equivalent Personnel&sup2;</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_TotEqPEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_TotEqPEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_TotEqPEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_TotEqPEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TotEqPEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_TotEqPEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_TotEqPEDC,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_TotEqPEDC,2)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Non-Energy OpEx&sup3;, US &cent;/UEDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_NEOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_NEOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_NEOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_NEOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NEOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_NEOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_NEOpexUEDC,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_NEOpexUEDC,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Total OpEx&sup3;, US &cent;/UEDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_TotCashOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_TotCashOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_TotCashOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_TotCashOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TotCashOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_TotCashOpexUEDC,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_TotCashOpexUEDC,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_TotCashOpexUEDC,2)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Net Cash Margin, US $/bbl</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_CashMargin,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_CashMargin,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_CashMargin,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_CashMargin,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_CashMargin,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_CashMargin,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_CashMargin,2))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_CashMargin,2)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Net Input Barrels, kb/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@BU_NetInputBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CT_NetInputBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@ES_NetInputBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HI_NetInputBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NetInputBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RI_NetInputBPD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SL_NetInputBPD,1))</td>
    <td align=right valign=bottom><strong>Format(@CHEVRON_NetInputBPD,0)</strong></td>
  </tr>

  END  

  <tr>
    <td height=75 colspan=10 valign=bottom>&sup1; Amount of process EDC&trade; that is mechanically available for the period</td>
  </tr>

  <tr>
    <td colspan=10 valign=bottom>&sup2; Equivalent Personnel per 100,000 EDC; 1 person = 2080 hours/year</td>
  </tr>

  <tr>
    <td colspan=10 valign=bottom>&sup3; OpEx = total refinery operating expenses</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td colspan=10 valign=bottom>&nbsp;&nbsp;General note: calculations may not add up due to the number of significant digits being displayed</td>  
  </tr>

</table>
<!-- template-end -->
