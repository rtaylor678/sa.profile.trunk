<!-- config-start -->

<!-- config-end -->
<!-- template-start -->


<table class=small border=0 style="background:url(https://webservices.solomononline.com/RefineryReports/Templates/Valero.jpg) no-repeat;">
  <tr>
    <td width=200></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
  </tr>

  <tr>
    <td height=140 Colspan=13 align=Left valign=bottom><strong>Business Unit Review Scorecard</strong></td>
  </tr>

  <tr>
    <td Colspan=13 align=left valign=bottom>Year-to-date&nbsp;&nbsp;&nbsp;::&nbsp;&nbsp;&nbsp;ReportPeriod</td>
  </tr>

  <tr>
    <td align=left valign=bottom height=50>Refinery Abbreviation</td>
    <td align=right valign=bottom>AD&nbsp;</td>
    <td align=right valign=bottom>BN&nbsp;</td>
    <td align=right valign=bottom>CC&nbsp;</td>
    <td align=right valign=bottom>HO&nbsp;</td>
    <td align=right valign=bottom>MK&nbsp;</td>
    <td align=right valign=bottom>MP&nbsp;</td>
    <td align=right valign=bottom>PA&nbsp;</td>
    <td align=right valign=bottom>QB&nbsp;</td>
    <td align=right valign=bottom>SC&nbsp;</td>
    <td align=right valign=bottom>TC&nbsp;</td>
    <td align=right valign=bottom>TR&nbsp;</td>
    <td align=right valign=bottom>WM&nbsp;</td>
    <td align=right valign=bottom><strong>Valero<br>Total&nbsp;</br></strong></td>
  </tr>

  SECTION(BURYTD,,)  
  BEGIN
  <tr>
    <td height=25 valign=bottom>&nbsp;&nbsp;&nbsp;Reliability Metrics</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mechanical Availability, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_MechAvail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_MechAvail_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_MechAvail_YTD,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mechanical Unavailability, %</td>
  </tr>

  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_TAMechUnavail_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_TAMechUnavail_YTD,1)</strong></td>
  </tr>

  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_RoutMechUnavail_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_RoutMechUnavail_YTD,1)</strong></td>
  </tr>
  
  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maint. Index&sup1;, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_AdjMaintIndex_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_AdjMaintIndex_YTD,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refinery Utilization, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_UtilPcnt_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_UtilPcnt_YTD,1)</strong></td>
  </tr>

  <tr>
    <td height=25 valign=bottom>&nbsp;&nbsp;&nbsp;Efficiency Metrics</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OpEx less Energy, US $/EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_NEOpexEDC_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_NEOpexEDC_YTD,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Energy Intensity Index&reg;</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_EII_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_EII_YTD,0))</td>
    <td align=right valign=bottom><strong>Format(@Valero_EII_YTD,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pers. Index&sup1;, wk hr/100 EDC</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_TotWHrEDC_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_TotWHrEDC_YTD,1)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contractor Headcount&sup2;, #</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_NumContPers_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_NumContPers_YTD,0))</td>
    <td align=right valign=bottom><strong>Format(@Valero_NumContPers_YTD,0)<strong></td>
  </tr>

  <tr>
    <td height=25 valign=bottom>&nbsp;&nbsp;&nbsp;Volumetric Metrics</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refinery Throughput, kd/d</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_NetInputKBPD_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_NetInputKBPD_YTD,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Crude Charge Rate, kb/d</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_CrudeKBPD_YTD,1))</td>
    <td align=right valign=bottom><strong>Format(@Valero_CrudeKBPD_YTD,0)</strong></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FCC Charge Rate, kb/d</td>
    <td align=right valign=bottom>NoShowZero(Format(@AD_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@BN_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CC_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@HO_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MK_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MP_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PA_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@QB_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SC_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TC_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TR_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@WM_FCCRateKBPD_YTD,1))</td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@Valero_FCCRateKBPD_YTD,0))</strong></td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDC, k � refinery; M � company</span></td>
    <td align=right valign=top>NoShowZero(Format(@AD_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@BN_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@CC_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@HO_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@MK_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@MP_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@PA_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@QB_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@SC_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@TC_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@TR_kEDC_YTD,0))</td>
    <td align=right valign=top>NoShowZero(Format(@WM_kEDC_YTD,0))</td>
    <td align=right valign=top><strong>Format(@Valero_MEDC_YTD,1)</strong></td>
  </tr>
  END  

  <tr>
    <td Colspan=13 height=100 valign=bottom>&sup1; Maint. Index = Maintenance Index; Pers. Index = Personnel Index</td>
  </tr>

  <tr>
    <td Colspan=13 valign=bottom>&sup2; Contractor headcount in Profile includes both on-sites and off-sites</td>
  </tr>
</table>
<!-- template-end -->
