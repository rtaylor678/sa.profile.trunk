<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table width=100% border=0 class='small'>
  <tr>
    <td width=10></td>
    <td width=10></td>
    <td width=400></td>
    <td width=125></td>
    <td width=5></td>
  </tr>
  <tr>
    <td colspan=3></td>
    <td colspan=2 align=right><strong>ReportPeriod</strong></td>
  </tr>
SECTION(Summary,,)
BEGIN
  <tr>
    <td colspan=3 height=30 valign=bottom><strong>Energy Intensity Index</strong></td>
    <td valign=bottom align=right><strong>Format(EII,'#,##0')</strong></td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Energy Usage, MBtu/d</td>
    <td align=right>Format(EIIDailyUsage,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Standard Energy, MBtu/d</td>
    <td align=right>Format(EIIStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Energy Intensity Index, YTD</td>
    <td align=right>Format(EII_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Energy Usage, MBtu YTD</td>
    <td align=right>Format(EIIUsage_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Standard Energy, MBtu YTD</td>
    <td align=right>Format(EIIEstMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>

  <tr>
    <td height=30 colspan=3 valign=bottom><strong>Volumetric Expansion Index</strong></td>
    <td align=right valign=bottom><strong>Format(VEI,'#,##0.0')</strong></td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Reported Gain, b/mo</td>
    <td align=right>Format(VEIActualGain,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Standard Gain, b/mo</td>
    <td align=right>Format(VEIStdGain,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Volumetric Expansion Index, YTD</td>
    <td align=right>Format(VEI_YTD,'#,##0.0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Reported Gain, bbl YTD</td>
    <td align=right>Format(VEIActualGain_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Standard Gain, bbl YTD</td>
    <td align=right>Format(VEIStdGain_YTD,'#,##0')</td>
    <td></td>
  </tr>

  <tr>
    <td height=30 valign=bottom colspan=5><hr color=Black SIZE=1></td>
  </tr>

 END
 SECTION(SensHeat,,)
 BEGIN
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Sensible Heat</strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>@BarrelDesc</td>
    <td align=right>Format(SensGrossInput,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy Formula, kBtu/bbl</td>
    <td align="right">@SensHeatFormula</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu/d</td>
    <td align="right">Format(SensHeatStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu YTD</td>
    <td align="right">Format(EstSensHeatMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
 END
 
 SECTION(SensHeat,SensHeatFormula<>'35',)
 BEGIN
 <tr>
    <td colspan=2></td>
    <td>A- Crude Gravity, �API</td>
    <td align=right>Format(CrudeGravity,'#,##0.0')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=5><hr color=Black SIZE=1></td>
  </tr>
  
  SECTION(Functions,,)
  BEGIN
  HEADER('<tr>
    <td colspan=5 height=30 valign=bottom><strong>@Description</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td valign=bottom colspan=2>Unit Name</td>
    <td valign=bottom align=right>@Unitname</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Process ID</td>
    <td align=right>@ProcessID</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Process Type</td>
    <td align=right>@ProcessType</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Utilized Capacity, @DisplayTextUS</td>
    <td align=right>Format(@UtilCap,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Current Month Process Unit EII</td>
    <td align=right>Format(@UnitEII,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Year-to-date Process Unit EII</td>
    <td align=right>Format(@UnitEII_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2 valign=top>Standard Energy Formula, kBtu/bbl</td>
    <td align=right valign=top>@EIIFormulaForReport</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu/d</td>
    <td align=right>Format(@StdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu YTD</td>
    <td align=right>Format(@EstMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2 valign=top>Standard Gain Formula</td>
    <td align=right valign=top>@VEIFormulaForReport</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Gain, b/d</td>
    <td align=right>Format(@StdGain,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Gain, bbl YTD</td>
    <td align=right>Format(@EstGain_YTD,'#,##0')</td>
    <td></td>
  </tr>

  SUBSECTION(ProcessData,UnitID=@UnitID,)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=5 valign=bottom>Table 2 Data from Current Month</td>
  </tr>
  ')
  <tr>
    <td colspan=2></td>
    <td>@FormulaSymbol - @USDescription</td>
    <td align=right>Format(@SAValue,@USDecPlaces)</td>
    <td></td>
  </tr>
  ENDSUBSECTION

  <tr>
  <td height=30 valign=bottom colspan=5><hr color=Black SIZE=1></td>
  </tr>

  END  

  SECTION(ASPHALT,,)
  BEGIN
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Asphalt</strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Utilized Capacity, b/d</td>
    <td align=right>Format(ASPUtilCap,'#,##0')</td>
    <td></td>
  </tr>
  </tr><tr>
    <td></td>
    <td colspan=2>Standard Energy Formula, kBtu/bbl</td>
    <td align=right>@ASPEIIFormula</td>
    <td></td>
  <tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu/d</td>
    <td align=right>Format(ASPStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu YTD</td>
    <td align=right>Format(EstASPMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Utilities And Off-Sites</strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Net Input Barrels</td>
    <td align=right>Format(OffsitesUtilCap,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy Formula, kBtu/bbl</td>
    <td align=right>@OffsitesFormula</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu/d</td>
    <td align=right>Format(OffsitesStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu YTD</td>
    <td align=right>Format(EstOffsitesMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>A- Complexity</td>
    <td align=right>Format(Complexity,'#,##0.0')</td>
    <td></td>
  </tr>
  END
 
</table>
<!-- template-end -->