<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table width=100% border=0 class='small'>
  <tr>
    <td width=10></td>
    <td width=10></td>
    <td width=400></td>
    <td width=125></td>
    <td width=5></td>
  </tr>
SECTION(Summary,,)
BEGIN 
HEADER('
  <tr>
    <td colspan=3></td>
    <td colspan=2 align=right valign=bottom><strong>Format(PeriodStart,'MM.yyyy')</strong></td>
  </tr>
')
END
<!--
  <tr>
    <td colspan=3></td>
    <td colspan=2 align=right><strong>ReportPeriod</strong></td>
  </tr>
-->
SECTION(Summary,,)
BEGIN
  <tr>
    <td colspan=3 height=30 valign=bottom><strong>Индекс энергоемкости</strong></td> <!-- Energy Intensity Index -->
    <td valign=bottom align=right><strong>Format(EII,'#,##0')</strong></td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Потребление энергии, МБТЕ/день</td>  <!-- Energy Usage, MBtu/d -->
    <td align=right>Format(EIIDailyUsage,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Стандартное энергопотребление, МБТЕ/день</td>  <!-- Standard Energy, MBtu/d -->
    <td align=right>Format(EIIStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>Индекс энергоемкости (с начала года по наст. момент)</td>  <!-- Energy Intensity Index, YTD 
    <td align=right>Format(EII_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Потребление энергии, МБТЕ с начала года по наст. момент</td>  <!-- Energy Usage, MBtu YTD 
    <td align=right>Format(EIIUsage_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Станд. энергопотребление, МБТЕ с начала года по наст. момент</td>  <!-- Standard Energy, MBtu YTD 
    <td align=right>Format(EIIEstMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
-->
  <tr>
    <td height=30 colspan=3 valign=bottom><strong>Индекс объемного увеличения</strong></td>  <!-- Volumetric Expansion Index -->
    <td align=right valign=bottom><strong>Format(VEI,'#,##0.0')</strong></td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Указанный прирост, барр./мес.</td>  <!-- Reported Gain, b/mo -->
    <td align=right>Format(VEIActualGain,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Стандартный прирост, барр./мес.</td>  <!-- Standard Gain, b/mo -->
    <td align=right>Format(VEIStdGain,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>Индекс объемного увеличения, с начала года по наст. момент</td>  <!-- Volumetric Expansion Index, YTD 
    <td align=right>Format(VEI_YTD,'#,##0.0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Указанный прирост, баррели с начала года по наст. момент</td>  <!-- Reported Gain, bbl YTD 
    <td align=right>Format(VEIActualGain_YTD,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Стандартный прирост, баррели с начала года по наст. момент</td>  <!-- Standard Gain, bbl YTD 
    <td align=right>Format(VEIStdGain_YTD,'#,##0')</td>
    <td></td>
  </tr>
-->
  <tr>
    <td height=30 valign=bottom colspan=5><hr color=Black SIZE=1></td>
  </tr>

 END

 SECTION(MatlBal,,)
 BEGIN
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Материальный баланс</strong></td>  <!-- Material Balance -->
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=3 height=30 valign=bottom><strong>Поступление сырья</strong></td>  <!-- Raw Material Input -->
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Нефть - Таблица 14, барр.</td>  <!-- Crude Charge - Table 14, bbl -->
    <td align=right>Format(CrudeBbl,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Сырье, поступившее сразу на смешение (без переработки), барр.</td>  <!-- Raw Materials Blended Only, bbl -->
    <td align=right>Format(RMB,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Прочее сырье, Таблица 15, барр.</td>  <!-- Other Raw Materials, bbl -->
    <td align=right>Format(OTHRM,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>М106 – Покупная энергия для продаж (МЭ)</td>  <!-- M106 - Purchased Energy Consumed for Sales (FOE), bbl -->
    <td align=right>Format(M106,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>&nbsp;&nbsp;Всего - переработка сырья, барр.</td>  <!-- Total Raw Material Input, bbl -->
    <td align=right>Format(TotInputbbl,'#,##0')</td>
    <td></td>
  </tr>


  <tr>
    <td colspan=2></td>
    <td colspan=3 height=30 valign=bottom><strong>Выход продукции</strong></td>  <!-- Product Yield -->
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Выход конечных продуктов (за искл. кокса, битума, топлива собственной выработки), барр.</td>  <!-- Finished Product Yield (ex. Coke, ASP, RPF), bbl -->
    <td align=right>Format(FinProd,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Товарный кокс, барр.</td>  <!-- Saleable Coke, bbl -->
    <td align=right>Format(Coke,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Производство асфальта и битума, барр.</td>  <!-- Asphalt Production, bbl -->
    <td align=right>Format(Asphalt,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Потребление топлива собственной выработки, барр. МЭ</td>  <!-- Refinery-Produced Fuel Consumed, FOEB -->
    <td align=right>Format(RPFConsFOE,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Топливо, произведенное на НПЗ и проданное на сторону, МЭ</td>  <!-- Refinery-Produced Fuel Sold, FOEB -->
    <td align=right>Format(RPFSoldFOE,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Когенерация и другие продажи, МЭ</td>  <!-- Cogeneration and Other Sales, FOEB -->
    <td align=right>Format(CogenSalesFOE,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>&nbsp;&nbsp;Всего - продукты</td>  <!-- Total Products, bbl -->
    <td align=right>Format(TotProd,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;Указанные (потери)/прирост, барр.</td>  <!-- Reported (Loss)/Gain, bbl -->
    <td align=right>Format(Gain,'#,##0')</td>
    <td></td>
  </tr>

  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Топливо, произведенное на заводе, ГДж</strong></td>  <!-- Refinery-Produced Fuel, GJ -->
  </tr>
  <tr>
    <td colspan=2></td>
    <td>потребленное на производстве топлив</td>  <!-- Consumed -->
    <td align=right>Format(RPFCons,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Продажи на сторону</td>  <!-- Sold to Others -->
    <td align=right>Format(RPFSold,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>Когенерация и другие продажи</td>  <!-- Cogeneration and Other Sales -->
    <td align=right>Format(RPFCogen,'#,##0')</td>
    <td></td>
  </tr>


  SUBSECTION(CDUUtil,,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Используемая производительность установок атм. дистилляции, барр./потокодень</strong></td>  <!-- Atmospheric Crude Unit Utlized Capacity, b/sd -->
  </tr>
  ')
  <tr>
    <td colspan=2></td>
    <td>@UnitName</td>
    <td align=right>Format(@UtilCap,'#,##0')</td>
    <td></td>
  </tr>
  ENDSUBSECTION

  <tr>
    <td colspan=2></td>
    <td>&nbsp;&nbsp;Суммарная загрузка по нефти, барр./мес.</td>  <!-- Total Crude Charge, bbl/month -->
    <td align=right>Format(CDUCharge,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Указанная загрузка по нефти, барр./мес.</td>  <!-- Reported Crude Charge, bbl/month -->
    <td align="right">Format(CrudeBbl,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Необъяснимая разница, барр./мес.</td>  <!-- Unexplained difference, bbl/month -->
    <td align="right">Format(CrudeDelta,'#,##0')</td>
    <td></td>
  </tr>
 END

  <tr>
    <td height=30 valign=bottom colspan=5><hr color=Black SIZE=1></td>
  </tr>

 SECTION(SensHeat,,)
 BEGIN
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Теплосодержание</strong></td>  <!-- Sensible Heat -->
  </tr>
  <tr>
    <td></td>
    <td colspan=2>@BarrelDescRussian</td>
    <td align=right>Format(SensGrossInput,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Формула для расчета станд. энергопотребления, тыс. БТЕ/барр.</td>  <!-- Standard Energy Formula, kBtu/bbl -->
    <td align="right">@SensHeatFormula</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ/день</td>  <!-- Standard Energy, MBtu/d -->
    <td align="right">Format(SensHeatStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ с начала года по наст. момент</td>  <!-- Standard Energy, MBtu YTD
    <td align="right">Format(EstSensHeatMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
 END
--> 

 SECTION(SensHeat,SensHeatFormula<>'35',)
 BEGIN
 <tr>
    <td colspan=2></td>
    <td>A- Удельный вес нефти, град. API</td>  <!-- Crude Gravity, °API -->
    <td align=right>Format(CrudeGravity,'#,##0.0')</td>
    <td></td>
  </tr>
 END

  <tr>
    <td height=30 valign=bottom colspan=5><hr color=Black SIZE=1></td>
  </tr>
  
  SECTION(Functions,,)
  BEGIN
  HEADER('<tr>
    <td colspan=5 height=30 valign=bottom><strong>@DescriptionRussian</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td valign=bottom colspan=2>Название установки</td>  <!-- Unit Name -->
    <td valign=bottom align=right>@Unitname</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Идентификатор процесса</td>  <!-- Process ID -->
    <td align=right>@ProcessID</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Тип процесса</td>  <!-- Process Type -->
    <td align=right>@ProcessType</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Используемая производительность, @DisplayTextUSRussian</td>  <!-- Utilized Capacity -->
    <td align=right>Format(@UtilCap,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>EII технологической установки за текущий месяц</td> <!--Current Month Process Unit EII
    <td align=right>Format(@UnitEII,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>EII технол. установки с начала года по наст. момент</td>  <!--Year-to-date Process Unit EII
    <td align=right>Format(@UnitEII_YTD,'#,##0')</td>
    <td></td>
  </tr>
-->
  <tr>
    <td></td>
    <td colspan=2 valign=top>Формула для расчета станд. энергопотребления, тыс. БТЕ/барр.</td>  <!-- Standard Energy Formula, kBtu/bbl -->
    <td align=right valign=top>@EIIFormulaForReport</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ/день</td>  <!-- Standard Energy, MBtu/d -->
    <td align=right>Format(@StdEnergy,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ с начала года по наст. момент</td>  <!-- Standard Energy, MBtu YTD 
    <td align=right>Format(@EstMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
-->
  <tr>
    <td></td>
    <td colspan=2 valign=top>Формула для стандартного прироста</td>  <!-- Standard Gain Formula -->
    <td align=right valign=top>@VEIFormulaForReport</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Стандартный прирост, барр./день</td>  <!-- Standard Gain, b/d -->
    <td align=right>Format(@StdGain,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>Стандартный прирост, барр. с начала года по наст. момент/td>  <!-- Standard Gain, bbl YTD 
    <td align=right>Format(@EstGain_YTD,'#,##0')</td>
    <td></td>
  </tr>
-->

  SUBSECTION(ProcessData,UnitID=@UnitID,)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=5 valign=bottom>Таблица 2 – Данные за текущий месяц</td>  <!-- Table 2 Data from Current Month -->
  </tr>
  ')
  <tr>
    <td colspan=2></td>
    <td>@FormulaSymbol - @USDescriptionRussian</td>
    <td align=right>Format(@SAValue,@USDecPlaces)</td>
    <td></td>
  </tr>
  ENDSUBSECTION

  <tr>
  <td height=30 valign=bottom colspan=5><hr color=Black SIZE=1></td>
  </tr>

  END  

  SECTION(ASPHALT,,)
  BEGIN
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Асфальт и битум</strong></td>  <!-- Asphalt -->
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Используемая производительность, барр./день</td>  <!-- Utilized Capacity, b/d -->
    <td align=right>Format(ASPUtilCap,'#,##0')</td>
    <td></td>
  </tr>
  </tr><tr>
    <td></td>
    <td colspan=2>Формула для расчета станд. энергопотребления, тыс. БТЕ/барр.</td>  <!-- Standard Energy Formula, kBtu/bbl -->
    <td align=right>@ASPEIIFormula</td>
    <td></td>
  <tr>
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ/день</td>  <!-- Standard Energy, MBtu/d -->
    <td align=right>Format(ASPStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ с начала года по наст. момент</td>  <!-- Standard Energy, MBtu YTD 
    <td align=right>Format(EstASPMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
-->
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Вспомогательные и энергетические системы и внешние объекты</strong></td>  <!-- Utilities And Off-Sites -->
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Чистое поступление сырья</td>  <!-- Net Input Barrels -->
    <td align=right>Format(OffsitesUtilCap,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Формула для расчета станд. энергопотребления, тыс. БТЕ/барр.</td>  <!-- Standard Energy Formula, kBtu/bbl -->
    <td align=right>@OffsitesFormula</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ/день</td>  <!-- Standard Energy, MBtu/d -->
    <td align=right>Format(OffsitesStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
<!--
  <tr>
    <td></td>
    <td colspan=2>Стандартное энергопотребление, МБТЕ с начала года по наст. момент</td>  <!-- Standard Energy, MBtu YTD 
    <td align=right>Format(EstOffsitesMBTU_YTD,'#,##0')</td>
    <td></td>
  </tr>
-->
  <tr>
    <td colspan=2></td>
    <td>A- сложность (коэфф. конфигурации)</td>  <!-- Complexity -->
    <td align=right>Format(Complexity,'#,##0.0')</td>
    <td></td>
  </tr>
  END
 
</table>
<!-- template-end -->