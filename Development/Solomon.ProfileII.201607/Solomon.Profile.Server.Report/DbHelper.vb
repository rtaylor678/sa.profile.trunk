Imports System.Data.SqlClient

Module DbHelper


    Function QueryDb(ByVal sqlString As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim sqlstmt As String
        Dim SqlConnection1 As New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        SqlConnection1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
        '"packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
        '"1.7"";persist security info=True;initial catalog=ProfileFuels12;password=ProfileFu" & _
        '"els"

        sqlstmt = sqlString

        'Reads only the first sql statement. 
        'It's to gaurd against attacks on the data
        Try
            If sqlstmt.ToUpper.Trim().StartsWith("SELECT") Or sqlstmt.ToUpper.Trim().StartsWith("EXEC") Then
                Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, SqlConnection1)
                da.Fill(ds)
            End If
        Catch ex As Exception

            Throw New Exception("A database error just occurred." + ex.Message)
        Finally
            SqlConnection1.Close()
        End Try

        Return ds

    End Function





End Module
