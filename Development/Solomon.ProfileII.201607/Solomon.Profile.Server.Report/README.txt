This project is the Report Control for Profile II. It is responsible for generating reports using template files (*.tpl).
An example of the template file looks like the following:

This expects a Dataset with a table name = SenisbleHeat with the fields MaterialID,MaterialName,BBL,BPD and ,SortKey

<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class='small' width=100% border=0>
  <tr>
      <td width=3%></td>
      <td width=15%></td>
      <td width=350></td>
      <td width=15%></td>
      <td width=3%></td>
      <td width=15%></td>
      <td width=3%></td>
  </tr>
  <tr>
      <td colspan=2 align=center valign=bottom><strong>Material ID</strong></td>
      <td align=center valign=bottom><strong>Material Name</strong></td>
      <td colspan=2 align=center valign=bottom><strong>Total</br>Barrels</strong></td>
      <td colspan=2 align=center valign=bottom><strong>Barrels</br>per day</strong></td>
  </tr>
  <tr>
    <td colspan=7></td>
  </tr>

  SECTION(SensibleHeat,,SortKey ASC) #SECTION is repeater which reads the table name in a Dataset called SensibleHeat and  sorts by the field name  called SortKey  
  BEGIN
  <tr>
      <td></td>
      <td align=left valign=bottom>@MaterialID</td>   #fieldname starts with @ followed by the field name, @MaterialID 
      <td align=left valign=bottom>@MaterialName</td>
      <td align=right valign=bottom>NoShowZero(Format(BBL,'#,##0'))</td>  #NoShowZero says do not show value if zero and function Format formats field, #,##0 
      <td></td>
      <td align=right valign=bottom>NoShowZero(Format(BPD,'#,##0'))</td>
      <td></td>
  </tr>
  END
</table>
<!-- template-end -->



