<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table width=100% border=0 class='small'>
  <tr>
    <td width=5></td>
    <td width=5></td>
    <td width=400></td>
    <td width=125></td>
    <td width=5></td>
  </tr>
  <tr>
    <td colspan=3></td>
    <td colspan=2 align=right><strong>ReportPeriod</strong></td>
  </tr>
SECTION(EII,,)
BEGIN
  <tr>
    <td colspan=3 height=30 valign=bottom><strong>Energy Intensity Index</strong></td>
    <td valign=bottom align=right><strong>Format(EII,'#,##0')</strong></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Energy Daily Usage, MBtu</td>
    <td align=right>Format(EIIDailyUsage,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu</td>
    <td align=right>Format(EIIStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td height=30 colspan=3 valign=bottom><strong>Volumetric Expansion Index</strong></td>
    <td align=right valign=bottom><strong>Format(VEI,'#,##0.0')</strong></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Reported Gain, bbl</td>
    <td align=right>Format(VEIActualGain,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Gain, bbl</td>
    <td align=right>Format(VEIStdGain,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=5><hr color=Black SIZE=1></td>
  </tr>
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Sensible Heat</strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>@BarrelDesc</td>
    <td align=right>Format(SensGrossInput,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy</td>
    <td align="right">Format(SensHeatStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>EII Formula</td>
    <td align="right">@SensHeatFormula</td>
    <td></td>
  </tr>
 END
 
 SECTION(EII,SensHeatFormula<>'35',)
 BEGIN
 <tr>
    <td colspan=2></td>
    <td>A- Crude Gravity, �API</td>
    <td align=right>Format(Crude,'#,##0.0')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td colspan=5><hr color=Black SIZE=1></td>
  </tr>
  
  
  SECTION(Functions,,)
  BEGIN
  HEADER('<tr>
    <td colspan=5 height=30 valign=bottom><strong>@Description</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td valign=bottom colspan=2>Unit Name</td>
    <td valign=bottom align=right>@Unitname</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Process ID</td>
    <td align=right>@ProcessID</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Process Type</td>
    <td align=right>@ProcessType</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Utilized Capacity, @DisplayTextUS</td>
    <td align=right>Format(@UtilCap,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu/d</td>
    <td align=right>Format(@StdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Gain, BPCD/d</td>
    <td align=right> Format(@StdGain,'#,##0') </td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2 valign=top>EII Formula</td>
    <td align=right>@EIIFormulaForReport</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2 valign=top>VEI Formula</td>
    <td align=right>@VEIFormulaForReport</td>
    <td></td>
  </tr>
  @Variables
  <tr>
    <td colspan=5>&nbsp;</td>
  </tr>
  END
  <tr>
  <td colspan=5><hr color=""Black"" SIZE=1></td>
  </tr>
  
  SECTION(ASPHALT,,)
  BEGIN
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Asphault</strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Utilized Capacity, b/d</td>
    <td align=right>Format(ASPUtilCap,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu/d</td>
    <td align=right>Format(ASPStdEnergy,'#,##0')</td>
    <td></td>
  </tr><tr>
    <td></td>
    <td colspan=2>EII Formula</td>
    <td align=right>@ASPEIIFormula</td>
    <td></td>
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>Utilities And Off-Sites</strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Net Input Barrels</td>
    <td align=right>Format(OffsitesUtilCap,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>Standard Energy, MBtu/d</td>
    <td align=right>Format(OffsitesStdEnergy,'#,##0')</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2>EII Formula</td>
    <td align=right>@OffsitesFormula</td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td>A- Complexity</td>
    <td align=right>Format(Complexity,'#,##0.0')</td>
    <td></td>
  </tr>
  END
 
</table>
<!-- template-end -->