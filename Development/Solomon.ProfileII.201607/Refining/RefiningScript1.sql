﻿USE [Refining]
GO

DROP PROCEDURE profile.spQuartileBreaks
GO

DROP SCHEMA profile
GO
--===============================
CREATE SCHEMA profile;
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON SCHEMA :: profile TO [guest]
GO

CREATE PROCEDURE profile.spQuartileBreaks
	@ReflistRankBreak NVARCHAR(200),
	@RankVariable  NVARCHAR(100)
AS
	--Really, the delim is $$, but need 1 since charindex wont find 2
	DECLARE @Delim CHAR = '$';-- char(14); -- = '';
	DECLARE @Delim1Pos INTEGER = CHARINDEX(@Delim,@ReflistRankBreak,1);
	DECLARE @Delim2Pos INTEGER = CHARINDEX(@Delim,@ReflistRankBreak,@Delim1Pos+2);
	DECLARE @RefListNumber NVARCHAR(10) = SUBSTRING(@ReflistRankBreak,1,@Delim1Pos-1);
	DECLARE @RankBreak NVARCHAR(50)  = SUBSTRING(@ReflistRankBreak,@Delim1Pos+2,@Delim2Pos - @Delim1Pos-2);
	DECLARE @BreakValue NVARCHAR(50)= SUBSTRING(@ReflistRankBreak,@Delim2Pos+2,LEN(@ReflistRankBreak));
	--to find reflistname: select ListName from dbo.RefList_LU where reflistno = 1597;
	select 
	--RefListNo, RefListName, RankBreak, BreakValue, RankVariable, LastTime, DataCount, NumTiles, Top2, Tile1Break, Tile2Break, Tile3Break, Btm2, RankSpecsID
	RefListName, RankBreak, BreakValue, RankVariable, NumTiles, Top2, Tile1Break, Tile2Break, Tile3Break, Btm2
	from [dbo].[RankSummary] where 
	RefListNo = @RefListNumber 
	and RTRIM(RankBreak) = @RankBreak 
	and BreakValue = @BreakValue 
	and RankVariable = @RankVariable;
GO
GRANT CONTROL, EXECUTE, ALTER ON profile.spQuartileBreaks TO [guest]
GO

/*
create dummy table to test that sql will not fail
CREATE TABLE [dbo].[RankSummary](
	[RefListNo] INTEGER NULL,
	[RefListName] NVARCHAR(50) NULL,
	[RankBreak] [varchar](128) NOT NULL,
	[BreakValue] [char](12) NOT NULL,
	[RankVariable] [varchar](128) NOT NULL,
	[LastTime]  Date NULL,
	[DataCount] INTEGER NULL,
	[NumTiles] INTEGER NULL,
	 Top2 REAL NULL, Tile1Break REAL NULL, Tile2Break REAL NULL, Tile3Break REAL NULL, Btm2 REAL NULL, RankSpecsID INTEGER NULL
	 )
	 GO
*/

EXEC profile.spQuartileBreaks '1597AreaUSA','CashMargin'
GO