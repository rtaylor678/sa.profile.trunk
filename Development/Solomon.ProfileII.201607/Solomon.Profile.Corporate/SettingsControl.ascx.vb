﻿Public Class SettingsControl
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub navigateSettings(sender As Object, e As EventArgs)
        If (Not sender.InnerText Is Nothing) Then
            If (sender.InnerText = "Create User") Then
                Response.Redirect("createuser.aspx")
                Return
            End If
            If (sender.InnerText = "Manage Users") Then
                Response.Redirect("manageusers.aspx")
                Return
            End If
            If (sender.InnerText = "Change Password") Then
                Response.Redirect("changepasswordnew.aspx")
                Return
            End If
        End If
    End Sub

End Class