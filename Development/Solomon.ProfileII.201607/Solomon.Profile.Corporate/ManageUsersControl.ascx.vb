﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Text

Public Class ManageUsersControl
    Inherits System.Web.UI.UserControl

    Protected WithEvents sdUsers As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents DsUsers As Solomon.Profile.Corporate.dsUsers
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand

#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
    Dim _adminEmailAddress As String = ConfigurationManager.AppSettings("AdminEmailAddress").ToString()
    Private Shared _currentRecord As String

#End Region


    Private Sub InitializeComponent()

        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.sdUsers = New System.Data.SqlClient.SqlDataAdapter
        Me.DsUsers = New Solomon.Profile.Corporate.dsUsers
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand

        CType(Me.DsUsers, System.ComponentModel.ISupportInitialize).BeginInit()

        Me.SqlConnection1.ConnectionString = _connectionString
        ' ---------------------------------------------------------------------
        ' sdUsers
        '
        Me.sdUsers.SelectCommand = Me.SqlSelectCommand1
        Me.sdUsers.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Users",
                                                                                                                             New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("UserName", "UserName"),
                                                                                                                             New System.Data.Common.DataColumnMapping("Email", "Email"),
                                                                                                                             New System.Data.Common.DataColumnMapping("Company", "Company")})})

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand
        '
        Me.SqlSelectCommand1.CommandText = "SELECT UserId,UserName, Email, Company FROM dbo.Users"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' DsUsers
        '
        Me.DsUsers.DataSetName = "dsUsers"
        Me.DsUsers.Locale = New System.Globalization.CultureInfo("en-US")

        ' ---------------------------------------------------------------------

        CType(Me.DsUsers, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        Me.InitializeComponent()

        If IsNothing(Session("AppUser")) Then
            Response.Redirect("index.aspx")
        End If

        If Not IsPostBack Then
            sdUsers.SelectCommand.CommandText += " WHERE UserId NOT IN ('1')"
            sdUsers.Fill(DsUsers)

            GridView1.DataSource = DsUsers
            GridView1.DataMember = DsUsers.Tables(0).TableName
            'GridView1.DataTextField = DsUsers.Tables(0).Columns(0).ColumnName
            'GridView1.DataValueField = DsUsers.Tables(0).Columns(0).ColumnName
            GridView1.AutoGenerateColumns = False
            GridView1.DataBind()
            'If Not GridView1.Contains(New ListItem("USD")) Then
            'End If

        End If

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    'Protected Sub DeleteUser_Click(ByVal sender As Object, ByVal e As CommandEventArgs)


    '    lblModalTitle.Text = "Delete User"
    '    lblModalBody.Text = "Are you sure you want to Delete the user:"
    '    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
    '    upModal.Update()
    'End Sub

    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim currentUser As String = String.Empty
        Dim reader As SqlDataReader
        SqlConnection1 = New System.Data.SqlClient.SqlConnection
        SqlConnection1.ConnectionString = _connectionString
        Dim cmd As New SqlCommand("SELECT * FROM Users WHERE UserId='" + index.ToString() + "'", SqlConnection1)
        cmd.Connection.Open()
        reader = cmd.ExecuteReader()
        If reader.Read Then
            currentUser = reader("UserName")
        End If

        If e.CommandName = "DeleteUser" Then
            _currentRecord = index.ToString()
            lblModalTitle.Text = "Delete User"
            lblModalBody.Text = "Are you sure you want to Delete the user: " + currentUser
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
            upModal.Update()
        End If
        If e.CommandName = "ResetPass" Then
            _currentRecord = index.ToString()
            lblModalTitle.Text = "Reset User Details"
            lblModalBody.Text = "Are you sure you want to reset the login credentials for this user :" + currentUser
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal1", "$('#myModal1').modal();", True)
            upModal.Update()
        End If
        If e.CommandName = "Refineries" Then

        End If
    End Sub

    Protected Sub delete_ServerClick(sender As Object, e As EventArgs)
        Try
            SqlConnection1 = New System.Data.SqlClient.SqlConnection
            SqlConnection1.ConnectionString = _connectionString
            Dim cmd As New SqlCommand("Delete FROM Users WHERE UserId='" + _currentRecord + "'", SqlConnection1)
            cmd.Connection.Open()
            cmd.ExecuteNonQuery()
            _currentRecord = ""
            Response.Redirect(Request.RawUrl)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Reset_ServerClick(sender As Object, e As EventArgs)
        Try
            Dim TemporaryPassword As String = CreatePassword(12)
            Dim rowsAffected As Integer

            Using con As New SqlConnection(_connectionString)
                Using cmd As New SqlCommand("UPDATE Users Set  Password=@tempPass , FirstLogin=@firstLogin , TempPassword=@tempPass WHERE UserId =@Id", con)

                    cmd.Parameters.Add("@tempPass", SqlDbType.VarChar).Value = TemporaryPassword
                    cmd.Parameters.Add("@firstLogin", SqlDbType.Bit).Value = 1
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Convert.ToInt32(_currentRecord)
                    con.Open()
                    rowsAffected = cmd.ExecuteNonQuery()
                End Using
            End Using
            SendEmail(Convert.ToInt32(_currentRecord), TemporaryPassword)
            ltManageUsers.Text = "<font size=1px style=""COLOR:GREEN""> User Credentials Were Reset. User will receive an email with temporary Password. </font>"
        Catch ex As Exception

        End Try
    End Sub

    Public Function CreatePassword(ByVal length As Integer) As String
        Const valid As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        Dim res As StringBuilder = New StringBuilder()
        Dim rnd As Random = New Random()

        While 0 < Math.Max(System.Threading.Interlocked.Decrement(length), length + 1)
            res.Append(valid(rnd.[Next](valid.Length)))
        End While

        Return res.ToString()
    End Function


    Public Sub SendEmail(ByVal userId As Integer, ByVal tempPassword As String)
        Dim reader As SqlDataReader
        SqlConnection1 = New System.Data.SqlClient.SqlConnection
        SqlConnection1.ConnectionString = _connectionString
        Dim cmd As New SqlCommand("SELECT * FROM Users WHERE UserId='" + userId.ToString() + "'", SqlConnection1)
        cmd.Connection.Open()
        reader = cmd.ExecuteReader()
        If reader.Read Then
            'create the mail message
            Dim mail As New MailMessage()
            'set the addresses
            mail.From = New MailAddress(_adminEmailAddress)
            mail.[To].Add(reader(reader("Email")))
            'set the content
            mail.Subject = "Profile Corp Login Details"

            Dim mailBody As StringBuilder = New StringBuilder()
            mailBody.AppendFormat("Profile II administrator did a reset on your account.").AppendLine()
            mailBody.AppendFormat("Please sign in with the following credentials and make sure you change your password upon sign in").AppendLine()
            mailBody.AppendFormat("Username: {0}", reader("UserName")).AppendLine()
            mailBody.AppendFormat("Password: {0}", tempPassword).AppendLine()
            mailBody.AppendFormat("").AppendLine()
            mailBody.AppendFormat("Thanks").AppendLine()
            mailBody.AppendFormat("Solomon Profile II Administrator").AppendLine()

            mail.Body = mailBody.ToString()
            'set the server
            Dim smtp As New SmtpClient("localhost")
            'send the message
            Try
                smtp.Send(mail)
                'Response.Write("Your Email has been sent sucessfully - Thank You")
            Catch exc As Exception
                'Response.Write("Send failure: " & exc.ToString())
            End Try
        End If
    End Sub

    'Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
    '    e.Row.Cells[0].Visible = False
    'End Sub
End Class