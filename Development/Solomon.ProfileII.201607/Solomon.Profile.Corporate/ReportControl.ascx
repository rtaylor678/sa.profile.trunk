<%@ Control Language="VB" ClassName="ReportControl" AutoEventWireup="false" CodeBehind="ReportControl.ascx.vb" Inherits="Solomon.Profile.Corporate.ReportControl" %>
<link media="print" href="styles/print.css" type="text/css" rel="StyleSheet" />
<style>
	.small {
		font-size: 11px;
		font-family: Tahoma;
	}
</style>
<script>
	<!--
	function UpdatePageCounter(direction) {
		if (direction == 'NEXT') {
			document.getElementById('ltPage').value = parseInt(document.getElementById('ltPage').value) + 1;
		}

		if (direction == 'PREVIOUS') {
			document.getElementById('ltPage').value = parseInt(document.getElementById('ltPage').value) - 1;
		}

		var loc = location.toString().substring(0, location.toString().lastIndexOf("&page"));
		var size = location.toString().substring(0, location.toString().lastIndexOf("&page")).length;

		if (size > 0) {
			document.location = loc + "&page=" + document.getElementById('ltPage').value;
		}
		else {
			document.location = location + "&page=" + document.getElementById('ltPage').value;
		}
	}
	//-->		
</script>
<input id="totPages" type="hidden" value="<%= TotalPages %>">
<table style="Z-INDEX: 100; LEFT: 1px; POSITION: absolute; TOP: 15px" height="91" cellspacing="0"
	cellpadding="0" width="100%" border="0">
	<tr>
		<td width="314" background="images\ReportBGLeft.gif" bgcolor="dimgray">
			<div id="LabelName" align="center">
				<strong><font color="white">
							<%=ReportName %>
						</font></strong>
				<br>
			</div>
			<br>
			<div id="Methodology" align="center">
				<font size="2" color="white">
						<%= IIf ( ReportName="Gross Product Value",Format(CDate(StartDate),"MMMM yyyy"), StudyYear.ToString + " Methodology <br> " + Format(CDate(StartDate),"MMMM yyyy")   )%>
					</font>
			</div>
			<td colspan="8" width="42%">
				<div align="right">
					<img height="50" alt="" src="https://webservices.solomononline.com/profileii/images/SA logo RECT.jpg"
						width="300"><br>
				</div>
				<div id="LabelMonth" align="right">
					<font size="2">
						<%=  Date.Today.ToLongDateString()%>
					</font>
				</div>
			</td>
	</tr>
</table>
<hr style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 105px" width="100%" color="#000000"
	size="1">
<div style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 120px" width="637">
	<p>
		<script>
			<!--
	if (parseInt(document.getElementById('ltPage').value) != 1) {
		document.writeln("<a href=javascript:UpdatePageCounter('PREVIOUS'); >Previous </a>");
	}
	//-->
		</script>
		&nbsp;&nbsp;
			<script>
			<!--
	if (parseInt(document.getElementById('ltPage').value) < parseInt(document.getElementById('totPages').value)) {
		document.writeln("<a href=javascript:UpdatePageCounter('NEXT'); >Next </a>");
	}
	//-->
			</script>
	</p>
	<p align="center">
		<asp:Panel ID="Panel2" runat="server" Height="104px" Width="650">
			<asp:Literal ID="ltContent" runat="server"></asp:Literal>
		</asp:Panel>
	</p>
</div>
