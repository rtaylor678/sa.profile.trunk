﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Solomon
{
	internal partial class Extensible
	{
		internal partial class Load
		{
			internal static void ProductYield(Solomon.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Product Yield"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				map.Yield_Prod.BeginLoadData();

				//while (Common.RangeHasValue(rng))
				//{
				//}

				map.Yield_Prod.AcceptChanges();
				map.Yield_Prod.EndLoadData();
			}
		}
	}
}