﻿using System;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	public partial class XL
	{
		public static class Return
		{
			public static System.String String(Excel.Range rng)
			{
				try
				{
					return Convert.ToString(rng.Value).Trim();
				}
				catch
				{
					return string.Empty;
				}
			}	
		
			public static System.String String(Excel.Range rng, UInt32 FieldLength)
			{
				try
				{
					string t = Convert.ToString(rng.Value).Trim();
					return t.Substring(0, Math.Min(t.Length, (int)FieldLength));
				}
				catch
				{
					return string.Empty;
				}
			}

			public static System.Single Single(Excel.Range rng)
			{
				try
				{
					string s = Convert.ToString(rng.Value).Trim();
					return System.Single.Parse(s);
				}
				catch
				{
					return System.Single.NaN;
				}
			}

			public static System.Single Single(Excel.Range rng, Single Default)
			{
				try
				{
					string s = Convert.ToString(rng.Value).Trim();
					if (System.Single.TryParse(s, out Default))
					{
						return Default;
					}
					else
					{
						return Default;
					}
				}
				catch
				{
					return System.Single.NaN;
				}
			}

			public static System.Double Double(Excel.Range rng)
			{
				try
				{
					string s = Convert.ToString(rng.Value).Trim();
					return System.Double.Parse(s);
				}
				catch
				{
					return System.Double.NaN;
				}
			}

			public static System.DateTime? DateTime(Excel.Range rng)
			{
				try
				{
					double d;
					string s = Convert.ToString(rng.Value).Trim();

					if (double.TryParse(s, out d))
					{
						return System.DateTime.FromOADate(d);
					}
					else
					{
						return System.DateTime.Parse(s);
					}
				}
				catch
				{
					return null;
				}
			}

			public static System.Byte Byte(Excel.Range rng)
			{
				System.Byte b = 0;

				string s = Convert.ToString(rng.Value).Substring(0, 1).ToUpper();

				if (s == "T" || s == "X" || s == "Y" || s == "1" || s == "-") b = 1;

				return b;
			}

			public static System.UInt16? UInt16(Excel.Range rng)
			{
				try
				{
					string s = Convert.ToString(rng.Value).Trim();
					return System.UInt16.Parse(s);
				}
				catch
				{
					return null;
				}
			}

			public static System.Int16? Int16(Excel.Range rng)
			{
				try
				{
					string s = Convert.ToString(rng.Value).Trim();
					return System.Int16.Parse(s);
				}
				catch
				{
					return null;
				}
			}

			public static System.Int32? Int32(Excel.Range rng)
			{
				try
				{
					string s = Convert.ToString(rng.Value).Trim();
					return System.Int32.Parse(s);
				}
				catch
				{
					return null;
				}
			}
		}
	}
}