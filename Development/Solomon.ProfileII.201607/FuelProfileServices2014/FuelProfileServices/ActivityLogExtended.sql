USE [ProfileFuels12]
GO

CREATE TABLE [dbo].[ActivityLogExtended](
	[ActivityTime] [datetime] NOT NULL,
	[Application] [varchar](20) NULL,
	[Methodology] [varchar](20) NULL,
	[RefineryID] [varchar](6) NOT NULL,
	[CallerIP] [varchar](20) NOT NULL,
	[UserID] [varchar](20) NULL,
	[ComputerName] [varchar](50) NULL,
	[Service] [varchar](50) NULL,
	[Method] [varchar](50) NULL,
	[EntityName] [varchar](50) NULL,
	[PeriodStart] [varchar](20) NULL,
	[PeriodEnd] [varchar](20) NULL,
	[Notes] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
	[AppVersion] [nvarchar](50) NULL,
	[ErrorMessages] [nvarchar] (1000) NULL,
	[LocalDomainName] [nvarchar] (100) NULL,
	[OSVersion] [nvarchar] (50) NULL,
	[BrowserVersion] [nvarchar] (100) NULL,
	[OfficeVersion] [nvarchar] (100) NULL,
	[DataImportedFromBridgeFile] [nvarchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


CREATE PROC [dbo].[spLogActivityExtended]
@Application varchar(20),
@Methodology varchar(20),
@RefineryID varchar(6), 
@CallerIP varchar(20), 
@UserID varchar(20), 
@ComputerName varchar(50), 
@Service varchar(50),
@Method varchar(50), 
@EntityName varchar(50), 
@PeriodStart Varchar(20),
@PeriodEnd varchar(20),
@Notes varchar(max),
@Status varchar(50) ,
@AppVersion nvarchar(50) ,
@ErrorMessages nvarchar (1000) ,
@LocalDomainName nvarchar (100) ,
@OSVersion nvarchar (50) ,
@BrowserVersion nvarchar (100) ,
@OfficeVersion nvarchar (100) ,
@DataImportedFromBridgeFile nvarchar(1000)  
AS
INSERT INTO dbo.spLogActivityExtended(ActivityTime, [Application], Methodology, RefineryID, CallerIP, UserID,ComputerName,  [Service], Method,EntityName,PeriodStart,PeriodEnd,Notes, [Status],
AppVersion, ErrorMessages, LocalDomainName,OSVersion,BrowserVersion,OfficeVersion,DataImportedFromBridgeFile)
VALUES(GETDATE(),@Application, @Methodology, @RefineryID, @CallerIP, @UserID, @ComputerName,  @Service, @Method, @EntityName, @PeriodStart, @PeriodEnd, @Notes, @Status,
@AppVersion ,@ErrorMessages,@LocalDomainName,@OSVersion ,@BrowserVersion ,@OfficeVersion ,@DataImportedFromBridgeFile)

GO
