﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Sa.Forms
{
    public partial class frmBuildMap : Form
    {
        public frmBuildMap()
        {
            InitializeComponent();
            string appPath = AppDomain.CurrentDomain.BaseDirectory.ToString();
            txtNewMap.Text = appPath + "IoFiles/AutoMapping.xml";
            txtBridgeFile.Text = appPath + "IoFiles/Profile Upload Template.xlsx";
            txtTemplate.Text   = appPath + "IoFiles/mappingTemplate.xml";
            if (chkDefaultMap.Checked == true) txtNewMap.Enabled = false;
            else txtNewMap.Enabled = true;
            richTextBox1.Rtf = Properties.Resources.InstructionsRTF;
        }

        private void btnBridge_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = Path.GetFullPath(txtBridgeFile.Text);
            openFileDialog1.Title = "Locate Bridge File";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "xlsx";

            openFileDialog1.Filter = "Excel files (*.xls,*.xlsx,*.xlsm)|*.xls;*.xlsx;*.xlsm|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtBridgeFile.Text  = openFileDialog1.FileName;
                if (chkDefaultMap.Checked == true)   txtNewMap.Text = Path.ChangeExtension(txtBridgeFile.Text, "xml");
            } 
        }

        private void chkDefaultMap_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDefaultMap.Checked == true)
            {
                txtNewMap.Enabled = false;
                txtNewMap.Text = Path.ChangeExtension(txtBridgeFile.Text, "xml");
            }
            else txtNewMap.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTemplate_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = Path.GetFullPath(txtTemplate.Text);
            openFileDialog1.Title = "Locate Mapping Template File";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "xml";

            openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtTemplate.Text  = openFileDialog1.FileName;
            } 
        }

        private void btnNewMap_Click(object sender, EventArgs e)
        {
            if (chkDefaultMap.Checked == true)
            {
                DialogResult result;
                result = MessageBox.Show("Please Uncheck the Default Box if you wish to change this file name.", "Using Default Filename", MessageBoxButtons.OK);
                txtNewMap.Text = Path.ChangeExtension(txtBridgeFile.Text, "xml");
            }
            else
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Path.GetFullPath(txtNewMap.Text);
                openFileDialog1.Title = "Locate Mapping Template File";
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;
                openFileDialog1.DefaultExt = "xml";

                openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                  txtNewMap.Text  = openFileDialog1.FileName;
                }
            }
        }

        private void btnBuild_Click(object sender, EventArgs e)
        {
            DialogResult result;
            
            Sa.Extensible.CreateMappings(txtTemplate.Text, txtBridgeFile.Text, txtNewMap.Text);

            result = MessageBox.Show("Processing completed", "Done", MessageBoxButtons.OK);
        }

        
    }
}
