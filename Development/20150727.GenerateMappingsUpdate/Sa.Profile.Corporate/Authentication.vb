Imports System.Data.SqlClient
Imports System.Configuration

Public Structure SecurityData
    Dim CompanyID As String
    Dim CompanyName As String
    Dim Password As String
    Dim Salt As String
End Structure

Public Class Authentication

#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
#End Region

    Public Function Authenticate(ByVal login As String, ByVal password As String) As Boolean
        Dim saltPwd As String
        Dim secData As SecurityData
       
        secData = GetUserDataByLogin(login)
        If Not (IsNothing(secData.Password) And IsNothing(secData.Salt)) AndAlso _
            (secData.Password.Length > 0 And secData.Salt.Length > 0) Then
            saltPwd = Encrypt(password, secData.Salt)
            Return saltPwd.Equals(secData.Password)
        End If

        Return False
    End Function
    Public Function GetUserDataByID(ByVal id As String) As SecurityData
        Dim reader As SqlDataReader
        Dim secData As SecurityData
        Dim saltPwd As String
        SqlConnection1 = New System.Data.SqlClient.SqlConnection

        SqlConnection1.ConnectionString = _connectionString

        Dim cmd As New SqlCommand("SELECT * FROM Company_LU WHERE CompanyID='" + id + "'", SqlConnection1)
        cmd.Connection.Open()
        reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If reader.Read Then
            secData.CompanyID = reader("CompanyID")
            secData.CompanyName = reader("CompanyName")
            secData.Password = reader("CompanyPwd")
            secData.Salt = reader("PwdSaltKey")
        End If
        Return secData
    End Function
    Public Function GetUserDataByLogin(ByVal login As String) As SecurityData
        Dim reader As SqlDataReader
        Dim secData As SecurityData
        Dim saltPwd As String
        SqlConnection1 = New System.Data.SqlClient.SqlConnection

        SqlConnection1.ConnectionString = _connectionString

        Dim cmd As New SqlCommand("SELECT t.CompanyID,c.CompanyName,c.CompanyPwd,c.PwdSaltKey FROM Company_LU c ,TSort t WHERE t.Company=c.CompanyName AND c.CompanyLogin=@login", SqlConnection1)
        cmd.Parameters.Add("@login", login)
        cmd.Connection.Open()
        reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If reader.Read Then
            secData.CompanyID = reader("CompanyID")
            secData.CompanyName = reader("CompanyName")
            secData.Password = reader("CompanyPwd")
            secData.Salt = reader("PwdSaltKey")
        End If
        Return secData
    End Function

    Public Function GetUserDataByName(ByVal name As String) As SecurityData
        Dim reader As SqlDataReader
        Dim secData As SecurityData
        Dim saltPwd As String
        SqlConnection1 = New System.Data.SqlClient.SqlConnection

        SqlConnection1.ConnectionString = _connectionString

        Dim cmd As New SqlCommand("SELECT t.CompanyID,c.CompanyName,c.CompanyPwd,c.PwdSaltKey FROM Company_LU c ,TSort t WHERE t.Company=c.CompanyName AND c.CompanyName=@name", SqlConnection1)
        cmd.Parameters.Add("@name", name)
        cmd.Connection.Open()
        reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If reader.Read Then
            secData.CompanyID = reader("CompanyID")
            secData.CompanyName = reader("CompanyName")
            secData.Password = reader("CompanyPwd")
            secData.Salt = reader("PwdSaltKey")
        End If
        Return secData
    End Function



End Class
