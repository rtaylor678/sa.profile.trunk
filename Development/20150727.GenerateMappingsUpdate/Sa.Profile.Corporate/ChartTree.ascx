<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ChartTree.ascx.vb" Inherits="ProfileII_Corporate.ChartTree" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style> .trigger { FONT: FONT: 9/10px geneva, arial, verdana, sans-serif; CURSOR: hand }
	.branch { DISPLAY: none; FONT: 9/10px geneva, arial, verdana, sans-serif;; MARGIN-LEFT: 2px }
	.branch A:link { FONT: 9/10px geneva, arial, verdana, sans-serif; MARGIN-LEFT: 5px; COLOR: black; TEXT-DECORATION: none ;display:inline-block;width:90%;}
	.branch A:visited { FONT: 9/10px geneva, arial, verdana, sans-serif; MARGIN-LEFT: 5px; COLOR: black; TEXT-DECORATION: none;display:inline-block;width:90%; }
	.branch A:hover { FONT: 9/10px geneva, arial, verdana, sans-serif;border-top: white thin solid; border-bottom: gray thin solid;border-right: gray thin solid;MARGIN-LEFT: 5px; COLOR: darkcyan; TEXT-DECORATION: none ;display:inline-block;width:90%;}
	.branch A:active{ width:92%;border-top: white thin solid; border-bottom: gray thin solid;border-right: gray thin solid;/*border-right: black thin solid;border-top: black thin solid;margin-left: 5px;border-left: black thin solid;border-bottom: black thin solid;*/background-color: #cccccc;}
</style>
<!--script language="JavaScript"  src="cookie.js" /-->
<script language="JavaScript">
  
   var openImg = new Image();
   openImg.src = "TreeIcons\\Styles\\TMenu\\minus.gif";
   var closedImg = new Image();
   closedImg.src = "TreeIcons\\Styles\\TMenu\\plusik.gif";
   
   function showBranch(branch) {
      var objBranch = document.getElementById(branch).style;
      if(objBranch.display=="block")
         objBranch.display="none";
      else
         objBranch.display="block";
      
       createCookie('openedNodes',branch,1);
   }
   
   function swapFolder(img) {
      objImg = document.getElementById(img);
      if(objImg.src.indexOf('plusik.gif')>-1)
         objImg.src = openImg.src;
      else
         objImg.src = closedImg.src;
   }
   
   function setChartValue(node){
		document.forms[0]['SelectedNode'].value =node;
		document.forms[0]['Section'].value='CHARTS';
	
		 createCookie('pickednode',node,1);
		 createCookie('pickedsection','CHARTS',1);
		 
		 SummaryInfo(node);
		 NodePicked('CHARTS');
   }
  
   function getValue(theVal){
		return theVal.replace(/[^\d\.]/g,"")*1;
	}

   function restoreChartState(){
		var branch = readCookie('openedNodes');
		var section = readCookie('pickedsection');
		var node = readCookie('pickednode');
		if(section)
		{
			if (section=='CHARTS') 
			{
				if (branch) 
				{
					showBranch(branch);
					swapFolder('folder'+ getValue(branch));								
				}
			}
		  NodePicked(section); 
		}
		SummaryInfo(node);
   }
   
  
</script>
<%= BuildChartMenu() %>
<input id="Section" type="hidden" name="Section"> <input id="SelectedNode" type="hidden" name="SelectedNode">
<!--input type=hidden name="OpenedBraches" id="OpendedBranches"-->
