<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ReportTree.ascx.vb" Inherits="ProfileII_Corporate.ReportTree" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style>
/* BODY { FONT: 10/14px geneva, arial, verdana, sans-serif; COLOR: black }*/
	.rTrigger { FONT: 10/14px geneva, arial, verdana, sans-serif; CURSOR: hand }
	.rBranch { DISPLAY: block; font-weight :bold  ; MARGIN-LEFT: 5px ;}
	.rBranch A:link { font-weight :bold ; MARGIN-LEFT: 5px;COLOR: #3f4755; TEXT-DECORATION: none }
	.rBranch A:visited {font-weight :bold ; MARGIN-LEFT: 5px;COLOR: #3f4755; TEXT-DECORATION: none }
	.rBranch A:hover {font-weight :bold ;border-top: white thin solid; border-bottom: gray thin solid;border-right: gray thin solid; MARGIN-LEFT: 5px; COLOR: darkcyan; TEXT-DECORATION: none;width:90% }
	.rBranch A:active {font-weight :bold ; border-top: white thin solid; border-bottom: gray thin solid;border-right: gray thin solid;/*border-right: black thin solid;border-top: black thin solid;*/margin: 4pt;/*border-left: black thin solid;border-bottom: black thin solid;*/background-color: #cccccc;width:90% }
</style>
<script language="JavaScript" src="scripts/cookie.js"></script>
<script language="JavaScript">
   function NodePicked(section){}
   function SummaryInfo(info){}
   
   function setReportValue(node)
   {
	document.forms[0]['SelectedNode'].value=node;
    document.forms[0]['Section'].value='REPORTS';
	
	createCookie('pickednode',node,1);
	createCookie('pickedsection','REPORTS',1);
	createCookie('openedNodes','reportBranch',1);
    SummaryInfo(node);
    NodePicked('REPORTS');
   }
</script>
<div class="rBranch" id="rBranch1"> <A href="javascript:setReportValue('Refinery Scorecard');"> Refinery Scorecard</A> </div>
<input id="ReportSection" type="hidden" name="ReportSection">
<input id="ReportSelectedNode" type="hidden" name="ReportSelectedNode">
<input id="ReportOpendedBranches" type="hidden" name="ReportOpenedBranches">
