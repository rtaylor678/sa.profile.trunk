<!--
// Solomon js
// MM PRELOAD & SWAP IMAGES
// 
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
// FORM FUNCTIONS
// - VALIDATORS
function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') {
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (val<min || max<val) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}

// 
// WINDOW MANAGEMENT
// - Macromedia Window Opener
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_displayStatusMsg(msgStr) { //v1.0
  status=msgStr;
  document.MM_returnValue = true;
}
//Dropdown menus
//var ie = (document.all) ? 1:0;
var ie = (document.getElementsByName("*")) ? 1:0;
var mnu;
var chkit;
var z;
// Solomon configs
var top = -30;
var bot = 101;
var x=new Array(5);
//menu layers
x[0] = 'menu1';
x[1] = 'menu2';
x[2] = 'menu3';
x[3] = 'menu4';
x[4] = 'menu5';
var speed = 12;
var delay = 10;
function rollup() {
	for (z=0;x[z];z++){
		if(ie) { chkit = document.all[x[z]].style.pixelTop > top; }
		else { chkit = document.layers[x[z]].top > top; }
	        if (chkit) { upper(x[z]); } 
	}
}
function rolldn(obj) {
        clearTimeout(mnu);
	for (z=0;x[z];z++){
		if(x[z] != obj) {
			if(ie) { document.all[x[z]].style.pixelTop = top; }
			else { document.layers[x[z]].top = top; }
		}
	}
	if(ie) { chkit = document.all[obj].style.pixelTop >= bot; }
	else { chkit = document.layers[obj].top >= bot; }
        if (chkit) {
	        clearTimeout(mnu);
		if(ie) { document.all.menuclear.style.visibility = 'visible'; }
		else { document.layers.menuclear.visibility = 'visible'; }
		if(ie) { document.all.menuclear2.style.visibility = 'visible'; }
		else { document.layers.menuclear2.visibility = 'visible'; }
	} else {
		if(ie) { document.all[obj].style.pixelTop += speed; }
		else { document.layers[obj].top += speed; }
                mnu=setTimeout("rolldn('"+obj+"')", delay);
        }
}
function upper(obj) {
        clearTimeout(mnu);
	if(ie) { chkit = document.all[obj].style.pixelTop <= top; }
	else { chkit = document.layers[obj].top <= top; }
        if (chkit) {
                clearTimeout(mnu);
		if(ie) { document.all.menuclear.style.visibility = 'hidden'; }
		else { document.layers.menuclear.visibility = 'hidden'; }
		if(ie) { document.all.menuclear2.style.visibility = 'hidden'; }
		else { document.layers.menuclear2.visibility = 'hidden'; }
        } else {
		if(ie) { document.all[obj].style.pixelTop -= speed; }
		else { document.layers[obj].top -= speed; }
                mnu=setTimeout("upper('" + obj + "')", delay);
        }
}
function show() {
	for (z=0;x[z];z++){ //turn on menus
		/*
	    if(ie) { document.all[x[z]].style.visibility = 'visible'; }
	    else { document.layers[x[z]].visibility = 'visible'; }
        */
	    var layers =true;
	    if(typeof document.layers == "undefined" ) //&& document.layers !== null) 
	    {
	        layers=false;
	    }
	    if(layers)
	    {
	        try 
	        {
	            document.all[x[z]].style.visibility = 'visible'; 
	        }
	        catch(err) { }
	    }
	}
}
//end menu
//print content div
function PrintThisPage() 
{ 
var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
var sWinHTML = document.getElementById('contentstart').innerHTML;    
var winprint=window.open("","",sOption); 
winprint.document.open(); 
winprint.document.write('<HTML><HEAD><TITLE>Solomon Associates</TITLE><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">');
winprint.document.write('<LINK HREF=/css/sa_style.css REL=Stylesheet>');
winprint.document.write('<LINK HREF=/css/sa_style_print.css REL=Stylesheet></HEAD>');
winprint.document.write('<BODY><TABLE WIDTH="100%" BORDER="0" CELLPADDING="0" CELLSPACING="0" ID="SA">');
winprint.document.write('<TR><TD><IMG SRC="/images/head1.gif" WIDTH="350" HEIGHT="62"><BR></TD><TD ALIGN="RIGHT">');
winprint.document.write('<A HREF="#" onClick="window.close();" STYLE="color:#000000;text-decoration:underline overline">CLOSE</A>&nbsp;&nbsp;</TD>');
winprint.document.write('</TR></TABLE><P>&nbsp;</P><!--Begin content from div -->');
winprint.document.write(sWinHTML);
winprint.document.write('<!--End content from div --><P>&nbsp;</P>');
winprint.document.write('<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0"><TR><TD COLSPAN=2><HR COLOR=333333 SIZE=1 NOSHADE></TD></TR>');
winprint.document.write('<TR><TD ALIGN="LEFT" CLASS="copy-small">Copyright&copy; HSB Solomon Associates. SolomonOnline.com</TD><TD ALIGN="RIGHT">');
winprint.document.write('<A HREF="#" onClick="window.close();" STYLE="color:#000000;text-decoration:underline overline">CLOSE</A>&nbsp;&nbsp;</TD>');
winprint.document.write('</TR></TABLE></BODY></HTML>');
winprint.document.close(); 
winprint.focus(); 
}
//End print div content
