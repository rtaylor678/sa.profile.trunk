Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens

Module WebServicesHelper
    

    '<summary>
    '   Return refinery id from user token string
    '</summary>
    ' <param name="context">Current context</param>
    '<returns>Refinery id</returns>
    Public Function GetRefineryID(ByVal context As SoapContext) As String

        Dim tok As UsernameToken
        For Each tok In context.Security.Tokens
            If TypeOf tok Is UsernameToken Then
                Dim tokens() As String = Decrypt(tok.Username).Split("$".ToCharArray)
                Return tokens(tokens.Length - 1)
            End If
        Next

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function


    '<summary>
    '   Return refinery id from user token string
    '</summary>
    ' <param name="context">Current context</param>
    '<returns>Refinery id</returns>
    Public Function GetCompanyID(ByVal context As SoapContext) As String

        Dim tok As UsernameToken
        For Each tok In context.Security.Tokens
            If TypeOf tok Is UsernameToken Then
                Dim tokens() As String = Decrypt(tok.Username).Split("$".ToCharArray)
                Return tokens(0)
            End If
        Next

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function
End Module
