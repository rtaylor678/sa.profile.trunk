Imports System.Web.Services
Imports System.Data.SqlClient
Imports Microsoft.Web.Services2
Imports System.IO
Imports System.Configuration

<System.Web.Services.WebService(Namespace:="http://solomononline.com/FuelProfileWebServices/DataServices")> _
    Public Class DataServices
    Inherits System.Web.Services.WebService

#Region " Web Services Designer Generated Code "


#Region "Private Variables"
    Dim _workstationConnectionString As String = ConfigurationManager.AppSettings("WorkstationSqlConnectionString")
#End Region

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = _workstationConnectionString

    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub

#End Region


#Region "Get Lookup References"
    <WebMethod(Description:="Method used to lookup tables ")> _
    Public Function GetLookups() As DataSet
        '
        ' Retrieve the response's soap context
        '
        Dim responseContext As SoapContext = ResponseSoapContext.Current
        '
        ' Disable timestamp
        '
        responseContext.Security.Timestamp.TtlInSeconds = -1

        Dim refineryId As String = GetRefineryID(RequestSoapContext.Current)
        Dim companyId As String = GetCompanyID(RequestSoapContext.Current)

        Dim dsLookups As New DataSet
        Dim fuelsLubeCombo As Boolean


        Dim dsFLCombo As DataSet = QueryDb("SELECT FuelsLubesCombo FROM TSort WHERE RefineryID ='" + refineryId + "'")
        fuelsLubeCombo = dsFLCombo.Tables(0).Rows(0)(0)

        'Get Currency_LU
        Dim sqlstmt As String = " SELECT RTRIM(Country) + ' - ' + RTRIM(CurrencyCode) AS Description, " + _
                                " RTRIM(Country) AS Country, RTRIM(Currency) AS Currency, " + _
                                " RTRIM(KCurrency) AS KCurrency,RTRIM(CurrencyCode) as CurrencyCode " + _
                                " FROM Currency_LU " + _
                                " WHERE LastYear Is NULL" + _
                                " ORDER BY Country"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(0).TableName = "Currency_LU"

        'Get UnitTargets_LU
        sqlstmt = " SELECT RTRIM(ProcessID) AS ProcessID, RTRIM(Property) AS Property," + _
                  " RTRIM(USDescription) AS USDescription,RTRIM(MetDescription) AS MetDescription," + _
                  " RTRIM(USDecPlaces) AS USDecPlaces,RTRIM(MetDecPlaces) AS MetDecPlaces,SortKey " + _
                  " FROM UnitTargets_LU WHERE " + _
                  "CustomGroup=0 OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CompanyID='" + companyId + "' AND CustomType='T2')"


        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(1).TableName = "UnitTargets_LU"




        'Get Table2_LU
        sqlstmt = " SELECT RTRIM(ProcessID) AS ProcessID, RTRIM(Property) AS Property," + _
                  " RTRIM(USDescription) AS USDescription, RTRIM(MetDescription) AS MetDescription, " + _
                  " USDecPlaces, MetDecPlaces,SortKey " + _
                  " FROM Table2_LU WHERE " + _
                  "CustomGroup=0 OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CompanyID='" + companyId + "' AND CustomType='T2')"


        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(2).TableName = "Table2_LU"


        'Get Energy_LU
        sqlstmt = "SELECT RTRIM(Header) as Header, RTRIM(TransType) as TransType," + _
                    "RTRIM(TransTypeDesc) as TransTypeDesc, RTRIM(TransferTo) as TransferTo," + _
                    "RTRIM(EnergyType) as EnergyType, RTRIM(EnergyTypeDesc) as EnergyTypeDesc, Composition," + _
                    " SortKey " + _
                    " FROM Energy_LU " + _
                    "ORDER BY SortKey"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(3).TableName = "Energy_LU"


        'Get Crude_LU
        sqlstmt = "SELECT RTRIM(CNum) as CNum, RTRIM(CrudeName) as CrudeName, TypicalAPI, TypicalSulfur," + _
                    "RTRIM(ProdCountry) as ProdCountry, SortKey " + _
                    "FROM Crude_LU " + _
                    "ORDER BY ProdCountry, CrudeName"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(4).TableName = "Crude_LU"

        'Get  MaterialCategory_LU
        sqlstmt = "SELECT RTRIM(Category) as Category, RTRIM(CategoryName) as CategoryName " + _
                    " FROM MaterialCategory_LU " + _
                    " WHERE Category not in ('RMI','RPF')" + _
                    " ORDER BY SortKey"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(5).TableName = "MaterialCategory_LU"

        'Get Material_LU
        sqlstmt = "EXEC GetMaterialLU '" + refineryId + "'"
        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(6).TableName = "Material_LU"

        'Get ProcessGroup_LU
        If fuelsLubeCombo Then
            sqlstmt = "SELECT RTRIM(ProcessGroup) AS ProcessGroup, RTRIM(Description) AS GroupDesc, " + _
                        " SortKey" + _
                        " FROM ProcessGroup_LU" + _
                        " ORDER BY SortKey"
        Else

            sqlstmt = "SELECT RTRIM(ProcessGroup) AS ProcessGroup, RTRIM(Description) AS GroupDesc, " + _
                        " SortKey" + _
                        " FROM ProcessGroup_LU WHERE SortKey NOT IN (2,3) " + _
                        " ORDER BY SortKey"
        End If

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(7).TableName = "ProcessGroup_LU"



        'Get ProcessID_LU
        If fuelsLubeCombo Then
            sqlstmt = "SELECT p.SortKey,RTRIM(Description) AS ProcessDesc, RTRIM(ProcessID) AS ProcessID, " + _
                      "RTRIM(ProcessGroup) AS ProcessGroup, RTRIM(ProfileProcFacility) AS ProfileProcFacility, RTRIM(MaintDetails) AS MaintDetails, " + _
                      "RTRIM(DisplayTextUS) AS DisplayTextUS, " + _
                      "RTRIM(DisplayTextMet) AS DisplayTextMet, " + _
                      "RTRIM(SulfurAdj) AS SulfurAdj " + _
                      "FROM ProcessID_LU p, DisplayUnits_LU d " + _
                      "WHERE p.DisplayUnits = d.DisplayUnits " + _
                      "AND p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') " + _
                      "ORDER BY p.SortKey"
        Else
            sqlstmt = "SELECT p.SortKey,RTRIM(Description) AS ProcessDesc, RTRIM(ProcessID) AS ProcessID, " + _
                      "RTRIM(ProcessGroup) AS ProcessGroup, RTRIM(ProfileProcFacility) AS ProfileProcFacility, RTRIM(MaintDetails) AS MaintDetails, " + _
                      "RTRIM(DisplayTextUS) AS DisplayTextUS, " + _
                      "RTRIM(DisplayTextMet) AS DisplayTextMet, " + _
                      "RTRIM(SulfurAdj) AS SulfurAdj " + _
                      "FROM ProcessID_LU p, DisplayUnits_LU d " + _
                      "WHERE p.DisplayUnits = d.DisplayUnits  AND ProcessGroup NOT IN ('L','W') " + _
                      "AND p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') " + _
                      "ORDER BY p.SortKey"
        End If


        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(8).TableName = "ProcessID_LU"


        'Get ProcessType_LU
        sqlstmt = "SELECT RTRIM(ProcessID) AS ProcessID, RTRIM(ProcessType) AS ProcessType " + _
                  "FROM ProcessType_LU"


        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(9).TableName = "ProcessType_LU"

        'Get TankType_LU
        sqlstmt = "SELECT RTRIM(TankType) as TankType FROM TankType_LU WHERE TankType in ('CRD','INT','FIN')"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(10).TableName = "TankType_LU"

        'Get Pers_LU
        sqlstmt = "SELECT RTRIM(PersID) as PersID,RTRIM(Description) as Description,PersCode,IncludeForInput,SortKey,RTRIM(ParentID) AS ParentID, RTRIM(Indent) AS Indent,RTRIM(DetailStudy) AS DetailStudy,RTRIM(DetailProfile) AS DetailProfile FROM Pers_LU  ORDER BY SortKey"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(11).TableName = "Pers_LU"


        'Get Absence_LU
        sqlstmt = "SELECT RTRIM(CategoryID) as CategoryID,RTRIM(Description) as Description,SortKey,IncludeForInput,RTRIM(ParentID) AS ParentID, RTRIM(Indent) AS Indent,RTRIM(DetailStudy) AS DetailStudy,RTRIM(DetailProfile) AS DetailProfile FROM ABSENCE_LU ORDER BY SortKey"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(12).TableName = "Absence_LU"

        'Get Chart_LU
        sqlstmt = "SELECT RTRIM(ChartTitle) as ChartTitle,RTRIM(SectionHeader) as SectionHeader, " + _
                  "SortKey,RTRIM(ChartType) as ChartType,RTRIM(AxisLabelUS) as AxisLabelUS," + _
                  "RTRIM(AxisLabelMetric) as AxisLabelMetric,RTRIM(DataTable) as DataTable,RTRIM(ValueField1) as ValueField1," + _
                  "RTRIM(ValueField2) as ValueField2,RTRIM(ValueField3) as ValueField3,RTRIM(ValueField4) as ValueField4," + _
                  "RTRIM(ValueField5) as ValueField5,RTRIM(TargetField) as TargetField,RTRIM(YTDField) as YTDField," + _
                  "RTRIM(AvgField) as AvgField,RTRIM(Legend1) As Legend1,RTRIM(Legend2) As Legend2,RTRIM(Legend3) As Legend3," + _
                  "RTRIM(Legend4) As Legend4,RTRIM(Legend5) As Legend5,DecPlaces FROM Chart_LU WHERE " + _
                  "CustomGroup=0 OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CompanyID='" + companyId + "' AND CustomType='C') " + _
                  "ORDER BY SortKey"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(13).TableName = "Chart_LU"


        'Get Report_LU
        sqlstmt = "SELECT RTRIM(ReportCode) AS ReportCode, RTRIM(ReportName) AS ReportName, SortKey FROM Report_LU WHERE CustomGroup=0 OR CustomGroup IN ( SELECT CustomGroup FROM CoCustom WHERE CompanyID='" + companyId + "' and CustomType='R') ORDER BY SortKey"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(14).TableName = "Report_LU"


        'Get Methodology
        sqlstmt = "SELECT FactorSet FROM FactorSets WHERE Calculate = 'Y' AND RefineryType = 'FUELS' ORDER BY FactorSet DESC"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(15).TableName = "Methodology"


        'Get Opex_LU
        sqlstmt = " SELECT OpexID,RTRIM(Description) AS Description,Indent, RTRIM(ParentID) AS ParentID, RTRIM(DetailStudy) AS DetailStudy, RTRIM(DetailProfile) AS DetailProfile,SortKey FROM Opex_LU ORDER BY SortKey"

        dsLookups.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsLookups.Tables(16).TableName = "Opex_LU"

        Return dsLookups
    End Function
#End Region

#Region "Get Refinery References"
    <WebMethod(Description:="Method used to reference tables ")> _
    Public Function GetReferences() As DataSet
        '
        ' Retrieve the response's soap context
        '
        Dim responseContext As SoapContext = ResponseSoapContext.Current

        '
        ' Disable timestamp
        '
        responseContext.Security.Timestamp.TtlInSeconds = -1

        Dim dsReferences As New DataSet

        Dim refineryId As String = GetRefineryID(RequestSoapContext.Current)

        'Get UnitList
        Dim sqlstmt As String = "SELECT c.UnitID, TAID = ISNULL(max(m.TAID) , 0) " + _
               "FROM Config c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID " + _
               "LEFT JOIN MaintTA m ON m.RefineryId = s.RefineryID AND m.DataSet = s.DataSet AND m.Unitid = c.Unitid " + _
               "WHERE s.RefineryID = '" + refineryId + "' AND s.DataSet = 'Actual' " + _
               "GROUP BY c.UnitID"

        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(0).TableName = "UnitList"

        'Get Config
        sqlstmt = "SELECT UnitID, C.SortKey, RTRIM(C.ProcessID) as ProcessID, " + _
                    "RTRIM(ProcessType) as ProcessType, InServicePcnt, DesignFeedSulfur," + _
                    "RTRIM(UnitName) as UnitName, RTRIM(ProcessGroup) as ProcessGroup, " + _
                    "RptCap, RptStmCap, (CASE WHEN (UtilPcnt<> NULL) THEN 0.0 END) AS UtilPcnt , (CASE WHEN (StmUtilPcnt<> NULL) THEN 0.0 END) AS  StmUtilPcnt,C.EnergyPcnt " + _
                    "FROM Config C, ProcessID_LU P" + _
                    " WHERE C.ProcessID = P.ProcessID AND C.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND ProfileProcFacility = 'Y' AND " + _
                    " SubmissionID = (Select MAX(SubmissionID) FROM Submissions WHERE RefineryID= '" + _
                    refineryId.Trim + "' ) ORDER BY C.SortKey"

        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(1).TableName = "Config"

        'sqlstmt = "SELECT UnitID, C.SortKey, RTRIM(C.ProcessID) as ProcessID, " + _
        '           "RTRIM(ProcessType) as ProcessType, InServicePcnt, DesignFeedSulfur," + _
        '           "RTRIM(UnitName) as UnitName, RTRIM(ProcessGroup) as ProcessGroup, " + _
        '           "RptCap, RptStmCap, (CASE WHEN (UtilPcnt<> NULL) THEN 0.0 END) AS UtilPcnt , (CASE WHEN (StmUtilPcnt<> NULL) THEN 0.0 END) AS  StmUtilPcnt  " + _
        '           "FROM Config C, ProcessID_LU P" + _
        '           " WHERE C.ProcessID = P.ProcessID AND C.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND ProfileProcFacility = 'Y' AND " + _
        '           " SubmissionID = (Select MAX(SubmissionID) FROM Submissions WHERE RefineryID= '" + _
        '           refineryId.Trim + "' ) ORDER BY C.SortKey"

        'dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        'dsReferences.Tables(1).TableName = "Config"


        'Get MaintTA_Process
        sqlstmt = "SELECT DISTINCT C.UnitID,TAID,C.SortKey, RTRIM(C.ProcessID) as ProcessID, " + _
                    "RTRIM(C.UnitName) as UnitName, TADate, TAHrsDown, TACostLocal," + _
                    "TAMatlLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContMPS," + _
                    "PrevTADate, TAExceptions " + _
                    "FROM MaintTA M, Config C " + _
                    "WHERE RefineryID ='" + refineryId + "' AND M.UnitID = C.UnitID AND C.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND C.SubmissionID IN " + _
                    "(SELECT Top 1 SubmissionID FROM Submissions WHERE RefineryID='" + refineryId + "' ORDER BY PeriodStart DESC)"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(2).TableName = "MaintTA_Process"

        'Get MaintTA_Other
        sqlstmt = "SELECT TAID, M.UnitID, P.SortKey, RTRIM(P.ProcessID) as ProcessID, " + _
                    "RTRIM(P.Description) as UnitName, TADate, TAHrsDown, TACostLocal," + _
                    "TAMatlLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContMPS," + _
                    "PrevTADate, TAExceptions " + _
                    "FROM 	MaintTA M, ProcessID_LU P " + _
                    "WHERE 	M.ProcessID = P.ProcessID  AND ProfileProcFacility = 'N' AND MaintDetails = 'Y' " + _
                    "AND RefineryID = '" + refineryId + "'"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(3).TableName = "MaintTA_Other"


        'Get LoadedMonths
        sqlstmt = "SELECT PeriodYear, PeriodMonth " + _
                  "FROM Submissions " + _
                  "WHERE RefineryID = '" + refineryId + "' " + _
                  "ORDER BY PeriodStart DESC "
        Dim tempDS As DataSet = QueryDb(sqlstmt)
        'Dim u As Integer

        'Dim tm As New DataTable
        'tm.Columns.Add("PeriodStart", GetType(Date))
        'tm.Columns.Add("PeriodEnd", GetType(Date))
        'For u = 0 To tempDS.Tables(0).Rows.Count - 1
        '    Dim row As DataRow = tm.NewRow
        '    row!PeriodStart = CType(tempDS.Tables(0).Rows(u)("PeriodStart"), Date)
        '    row!PeriodEnd = CType(tempDS.Tables(0).Rows(u)("PeriodEnd"), Date)
        '    tm.Rows.Add(row)
        'Next

        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(4).TableName = "LoadedMonths"


        'Get Inventory Schema
        sqlstmt = "SELECT RTRIM(TankType) as TankType, NumTank, FuelsStorage, AvgLevel FROM Inventory i WHERE   SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "') "
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(5).TableName = "Inventory"


        'Get ConfigRS Schema
        sqlstmt = "SELECT RTRIM(ProcessID) as ProcessID, Throughput as RailcarBBL, Throughput as TankTruckBBL, Throughput as TankerBerthBBL, Throughput as OffshoreBuoyBBL, Throughput as BargeBerthBBL, Throughput as PipelineBBL FROM ConfigRS WHERE SubmissionID = 0"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(6).TableName = "ConfigRS"


        'Get ProcessData Schema
        sqlstmt = "SELECT UnitID, UnitID as SortKey, RTRIM(Property) as ProcessID, RTRIM(Property) as UnitName, RTRIM(Property) as Property, RptValue FROM PROCESSDATA WHERE SUBMISSIONID = 0"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(7).TableName = "ProcessData"


        'Get MaintRout Schema
        sqlstmt = "SELECT UnitID, RTRIM(ProcessID) as ProcessID, CAST(0.0 AS REAL) as RoutCostLocal,CAST(0.0 AS REAL) as RoutMatlLocal, " & _
                  "CAST(0 AS SMALLINT) as RegNum,CAST(0 AS SMALLINT) as MaintNum,CAST(0 AS SMALLINT) as OthNum,CAST(0.0 AS REAL) as OthDownEconomic, " & _
                  "CAST(0.0 AS REAL) as OthDownExternal, CAST(0.0 AS REAL) as OthDownUnitUpsets,CAST(0.0 AS REAL) as OthDownOffsiteUpsets," & _
                  "CAST(0.0 AS REAL) as OthDownOther, CAST(0 AS SMALLINT) as UnpRegNum,CAST(0.0 AS SMALLINT) as UnpRegDown," & _
                  "CAST(0 AS SMALLINT) as UnpMaintNum, CAST(0.0 AS REAL) as UnpMaintDown,CAST(0 AS SMALLINT) as UnpOthNum,CAST(0.0 AS REAL) as UnpOthDown," & _
                  "CAST(0.0 AS REAL)as RegDown, CAST(0.0 AS REAL) as RegSlow,CAST(0.0 AS REAL) as MaintDown,CAST(0.0 AS REAL) as MaintSlow," & _
                  "CAST(0.0 AS REAL) as OthDown,CAST(0.0 AS REAL) as OthSlow,CAST(0 AS SMALLINT) AS SortKey,RTRIM(ProcessID) as UnitName" & _
                  " FROM MAINTROUT WHERE SubmissionID = (SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "') "
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(8).TableName = "MaintRout"



        'Get OpEx Schema
        sqlstmt = "SELECT OCCSal, MPSSal, OCCBen, MPSBen, MaintMatl, ContMaintLabor, OthCont, Envir, OthNonVol, GAPers, Chemicals, Catalysts, PurOth, OthVol, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd,POXO2 FROM OpEx WHERE SubmissionID = 0"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(9).TableName = "OpEx"

        'Get OpexAdd Schema
        sqlstmt = "SELECT OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock," & _
                "OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,MPSBenAbs,MPSBenInsur," & _
                "MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth," & _
                "MaintMatl,ContMaintMatl,EquipMaint,OthContProcOp,OthContTransOp," & _
                "OthContFire,OthContVacTrucks,OthContConsult,OthContInsp,OthContSecurity," & _
                "OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin," & _
                "OthContLegal,OthContOth,EnvirDisp,EnvirPermits,EnvirFines," & _
                "EnvirSpill,EnvirLab,EnvirEng,EnvirOth,EquipNonMaint,Tax,Insur," & _
                "OthNonVolSupply,OthNonVolSafety,OthNonVolComm,OthNonVolDonations,OthNonVolDues," & _
                "OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks," & _
                "OthNonVolOth,ChemicalsAlkyAcid,ChemicalsLube,ChemicalsH2OTreat," & _
                "ChemicalsProcess,ChemicalsOthAcid,ChemicalsGasAdd,ChemicalsDieselAdd," & _
                "ChemicalsOthAdd,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv," & _
                "ChemicalsWasteH2O,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK," & _
                "ChemicalsMEK,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv," & _
                "ChemicalsDewaxAids,ChemicalsOth,CatalystsFCC,CatalystsHYC," & _
                "CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT," & _
                "CatalystsHYFT,CatalystsCDWax,CatalystsREF,CatalystsHYG," & _
                "CatalystsS2Plant,CatalystsPetChem,CatalystsOth,PurOthN2,PurOthH2O," & _
                "PurOthOth,Royalties,OthVolDemCrude,OthVolDemLightering,OthVolDemProd," & _
                "EmissionsTaxes,EmissionsPurch,EmissionsCredits,OthVolOth " & _
                "FROM OpexAdd WHERE SubmissionID=0"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(10).TableName = "OpexAdd"


        'Get OpexAll
        sqlstmt = " SELECT OCCSal," & _
                  " MPSSal, OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, " & _
                  " OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, OCCBen, " & _
                  " MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock, " & _
                  " MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, MaintMatl, " & _
                  " ContMaintMatl, EquipMaint, MaintMatlST, ContMaintLabor, " & _
                  " ContMaintInspect, ContMaintLaborST, OthContProcOp, OthContTransOp," & _
                  " OthContFire, OthContVacTrucks, OthContConsult, OthContSecurity," & _
                  " OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin," & _
                  " OthContLegal, OthContOth, OthCont, TAAdj, EnvirDisp, EnvirPermits, " & _
                  " EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, Envir, EquipNonMaint," & _
                  " Tax, InsurBI, InsurPC, InsurOth, OthNonVolSupply, OthNonVolSafety, " & _
                  " OthNonVolNonPersSafety, OthNonVolComm, OthNonVolDonations, " & _
                  " OthNonVolNonContribPers, OthNonVolDues, OthNonVolTravel, OthNonVolTrain," & _
                  " OthNonVolComputer, OthNonVolTanks, OthNonVolExtraExpat, OthNonVolOth, " & _
                  " OthNonVol, STNonVol, GAPers, ChemicalsAntiknock, ChemicalsAlkyAcid, " & _
                  " ChemicalsLube, ChemicalsH2OTreat, ChemicalsProcess, ChemicalsOthAcid," & _
                  " ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, " & _
                  " ChemicalsClay, ChemicalsAmines, ChemicalsASESolv, ChemicalsWasteH2O, " & _
                  " ChemicalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene," & _
                  " ChemicalsPropane, ChemicalsOthSolv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth, " & _
                  " Chemicals, CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, " & _
                  " CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, CatalystsHYG, CatalystsS2Plant," & _
                  " CatalystsPetChem, CatalystsOth, Catalysts, PurOthN2, PurOthH2O, PurOthOth, PurOth," & _
                  " Royalties, EmissionsTaxes, EmissionsPurch, EmissionsCredits, OthVolOth, OthVol, " & _
                  " GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, " & _
                  " OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, OthVolDemCrude, " & _
                  " OthVolDemLightering, OthVolDemProd, STVol, TotCashOpEx, ExclFireSafety, ExclEnvirFines, " & _
                  " ExclOth, TotExpExcl, STSal, STBen, PersCostExclTA, PersCost, EnergyCost, NEOpex " & _
                  " FROM ProfileFuels.dbo.OpExAll WHERE SubmissionID=0"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(11).TableName = "OpexAll"

        'Get Pers Schema
        sqlstmt = "SELECT RTRIM(PersID) as PersID, NumPers,AbsHrs, STH, OVTHours, OVTPcnt, Contract, GA, MaintPcnt FROM Pers WHERE SubmissionID = 0"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(12).TableName = "Pers"

        'Get Absence Schema
        sqlstmt = "SELECT RTRIM(CategoryID) as CategoryID, OCCAbs, MPSAbs FROM ABSENCE WHERE SubmissionID = 0"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(13).TableName = "Absence"

        'Get Crude Schema
        sqlstmt = "SELECT RTRIM(CNum) as CNum, RTRIM(CrudeName) as CrudeName, Gravity, Sulfur, 0.0 AS  BBL,0.0 AS CostPerBBL FROM CRUDE WHERE SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "') "
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(14).TableName = "Crude"


        'Get Yield_RM Schema
        sqlstmt = "SELECT m.SortKey, RTRIM(y.Category) AS Category, RTRIM(y.MaterialID) as MaterialID," + _
                  " RTRIM(y.MaterialName) as MaterialName, CAST(0.0 AS FLOAT) AS BBL, CAST(0.0 AS FLOAT) AS PriceLocal FROM YIELD y , Material_LU m WHERE  m.MaterialID=y.MaterialID AND " + _
                  " y.Category IN ('OTHRM','RCHEM','RLUBE') AND y.SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "') "
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(15).TableName = "Yield_RM"


        'Get Yield_Prod Schema
        sqlstmt = "SELECT m.SortKey, RTRIM(y.Category) AS Category, RTRIM(y.MaterialID) as MaterialID," + _
                  " RTRIM(y.MaterialName) as MaterialName, CAST(0.0 AS FLOAT) AS BBL, CAST(0.0 AS FLOAT) AS PriceLocal FROM YIELD y , Material_LU m WHERE  m.MaterialID=y.MaterialID AND " + _
                  " y.Category NOT IN ('OTHRM','RCHEM','RLUBE','RMI','RPF') AND " + _
                  " y.MaterialID <> 'GAIN' AND y.SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "') "
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(16).TableName = "Yield_Prod"


        'Get Energy Schema
        sqlstmt = "SELECT RTRIM(e.TransType) as TransType, RTRIM(e.TransferTo) as TransferTo, RTRIM(e.EnergyType) as EnergyType, e.TransCode,elu.SortKey, 0.0 AS RptSource, 0.0 AS RptPriceLocal," + _
                  "e.Hydrogen,e.Methane,e.Ethane,e.Ethylene,e.Propane,e.Propylene,e.Butane,e.Isobutane,e.C5Plus,e.CO,e.CO2,e.N2,e.Inerts FROM Energy e , Energy_LU elu WHERE SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "') AND e.TransferTo = elu.TransferTo AND e.TransType = elu.TransType AND e.EnergyType = elu.EnergyType "
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(17).TableName = "Energy"


        'Get MaintRoutHist Schema
        sqlstmt = "EXEC GetMaintRoutHist '" + refineryId + "', 'ACTUAL'"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(18).TableName = "MaintRoutHist"


        'Get Settings Schema
        sqlstmt = "SELECT RTRIM(S.Company) AS Company, RTRIM(S.Location) AS Location, RTRIM(S.CoordName) AS CoordName, RTRIM(S.CoordTitle) AS CoordTitle, " + _
                  "RTRIM(S.CoordPhone) AS CoordPhone, RTRIM(S.CoordEMail) AS CoordEMail, RTRIM(S.RptCurrency) AS RptCurrency,RTRIM(S.RptCurrencyT15) AS RptCurrencyT15, (CASE RTRIM(UOM) " + _
                  "WHEN 'US' THEN 'US Units' WHEN 'MET' THEN 'Metric' END) AS UOM, '  ' AS BridgeLocation, T.FuelsLubesCombo, " + _
                  "PeriodYear, PeriodMonth,DataSet,BridgeVersion,ClientVersion  " + _
                  "FROM dbo.Submissions S ," + _
                  "dbo.TSort T WHERE S.RefineryID = T.RefineryID AND S.RefineryID='" + refineryId + "' AND S.SubmissionID IN " + _
                  "(SELECT Top 1 SubmissionID FROM  Submissions WHERE refineryID='" + refineryId + "' ORDER BY PeriodStart DESC)"

        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(19).TableName = "Settings"


        'Get Electric Schema
        sqlstmt = "SELECT RTRIM(TransType) as TransType, RTRIM(TransferTo) as TransferTo, RTRIM(EnergyType) as EnergyType, TransCode As SortKey,0.0 AS RptGenEff,0.0 AS RptMWH,0.0 AS PriceLocal FROM Electric WHERE SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "') "
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(20).TableName = "Electric"

        'Get RefTargets Schema
        sqlstmt = "SELECT RTRIM(c.SectionHeader) AS SectionHeader,RTRIM(r.Property) AS Property, r.Target, RTRIM(r.CurrencyCode) AS CurrencyCode,c.SortKey FROM RefTargets r,Chart_LU c " + _
       " WHERE c.TargetField=r.Property AND c.Sortkey<800 AND  r.SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "')"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(21).TableName = "RefTargets"

        'Get UnitTargets Schema
        sqlstmt = "SELECT u.UnitID, u.MechAvail, u.OpAvail,u.OnStream, u.UtilPcnt, u.RoutCost, u.TACost, RTRIM(u.CurrencyCode) AS CurrencyCode,c.SortKey,RTRIM(c.ProcessID) AS ProcessID, RTRIM(c.UnitName) AS UnitName FROM UnitTargets u ,Config c " + _
        "WHERE c.UnitID=u.UnitID  AND c.SubmissionId=u.submissionid AND u.SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "')"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(22).TableName = "UnitTargets"

        'Get UserDefined Schema
        sqlstmt = "SELECT RTRIM(u.HeaderText) AS HeaderText,RTRIM(u.VariableDesc) AS VariableDesc, 0.0 AS RptValue, 0.0 AS RptValue_Target, 0.0 AS RptValue_Avg, 0.0 AS RptValue_YTD, 0 AS DecPlaces FROM UserDefined u " + _
        " WHERE u.SubmissionID =(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID='" + refineryId + "')"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(23).TableName = "UserDefined"

        'Get UnitTargetsNew
        sqlstmt = "SELECT ut.UnitID,c.UnitName,c.ProcessID,ut.Property,ut.Target,ut.CurrencyCode," + _
                  " ul.USDescription, ul.MetDescription, ul.USDecPlaces, ul.MetDecPlaces,ul.SortKey " + _
                  " FROM UnitTargetsNew ut,UnitTargets_LU ul, Config c " + _
                  " WHERE ut.SubmissionID = c.SubmissionID AND ut.UnitID = c.UnitID AND ul.Property = ut.Property " + _
                  " AND (ul.ProcessID='ALL' OR ul.ProcessID=c.ProcessID) " + _
                  " AND c.SubmissionID=(Select MAX(SubmissionID) FROM Submissions WHERE RefineryID= '" + refineryId + "') ORDER BY ul.SortKey"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(24).TableName = "UnitTargetsNew"

        'Get LoadEDCStabilizers
        sqlstmt = "SELECT EffDate,AnnInputBbl,AnnCokeBbl,AnnElecConsMWH, " + _
                  "AnnRSCRUDE_RAIL,AnnRSCRUDE_TT,AnnRSCRUDE_TB,AnnRSCRUDE_OMB, " + _
                  "AnnRSCRUDE_BB,AnnRSPROD_RAIL,AnnRSPROD_TT,AnnRSPROD_TB,AnnRSPROD_OMB," + _
                  "AnnRSPROD_BB " + _
                  "FROM LoadEDCStabilizers " + _
                  "WHERE RefineryID ='" + refineryId + "'"
        dsReferences.Tables.Add(QueryDb(sqlstmt).Tables(0).Copy)
        dsReferences.Tables(25).TableName = "EDCStabilizers"

        Return dsReferences
    End Function
#End Region

#Region "Get Report Data Dump"
    <WebMethod(Description:="Returns the results of a refinery")> _
    Public Function GetDataDump(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, ByVal studyYear As Integer, ByVal includeTarget As Boolean, ByVal includeYTD As Boolean, ByVal includeAVG As Boolean) As DataSet
        '
        ' Retrieve the response's soap context
        '
        Dim responseContext As SoapContext = ResponseSoapContext.Current

        '
        ' Disable timestamp
        '
        responseContext.Security.Timestamp.TtlInSeconds = -1
        Dim refineryId As String = GetRefineryID(RequestSoapContext.Current)

        Dim dsResp As DataSet = QueryDb("SELECT ReportName FROM Report_LU WHERE ReportCode='" + ReportCode + "'")
        If dsResp.Tables(0).Rows.Count > 0 Then
            Dim ReportName As String = dsResp.Tables(0).Rows(0)("ReportName")
            Dim ddmpMgr As New DataDumpManager
            Return ddmpMgr.GetDataDump(ReportCode.ToUpper.Trim, ReportName, refineryId, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG)
        End If


    End Function

#End Region


End Class

