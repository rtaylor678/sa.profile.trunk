Option Explicit On

Friend Class SA_Personnel
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents gbTools As System.Windows.Forms.GroupBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cbSheets As System.Windows.Forms.ComboBox
    Friend WithEvents tbFilePath As System.Windows.Forms.TextBox
    Friend WithEvents rButtonProfile As System.Windows.Forms.RadioButton
    Friend WithEvents rButtonStudy As System.Windows.Forms.RadioButton
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents rbLinks As System.Windows.Forms.RadioButton
    Friend WithEvents rbData As System.Windows.Forms.RadioButton
    Friend WithEvents tabPageAbsences As System.Windows.Forms.TabPage
    Friend WithEvents lblABSHeader As System.Windows.Forms.Label
    Friend WithEvents tabPageMPS As System.Windows.Forms.TabPage
    Friend WithEvents lblMPSHeader As System.Windows.Forms.Label
    Friend WithEvents tabPageOCC As System.Windows.Forms.TabPage
    Friend WithEvents lblOCCHeader As System.Windows.Forms.Label
    Friend WithEvents tabControlPers As System.Windows.Forms.TabControl
    Friend WithEvents dgvOCCLinks As System.Windows.Forms.DataGridView
    Friend WithEvents dgvABSLinks As System.Windows.Forms.DataGridView
    Friend WithEvents dgvOCC As System.Windows.Forms.DataGridView
    Friend WithEvents dgvMPSLinks As System.Windows.Forms.DataGridView
    Friend WithEvents dgvMPS As System.Windows.Forms.DataGridView
    Friend WithEvents dgvABS As System.Windows.Forms.DataGridView
    Friend WithEvents gbFooterMPS As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents gbFooterOCC As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gbFooterABS As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents OccDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccNumPers As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccSTH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccOVTHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccOVTPcnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccContract As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccMaintPcnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccAbsHrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AbsDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OCCAbs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MPSAbs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OCCAbsLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MPSAbsLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsNumPers As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsSTH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsOVTHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsOVTPcnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsContract As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsMaintPcnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsAbsHrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsDescriptionLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsNumPersLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsSTHLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsOVTHoursLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsOVTPcntLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsContractLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsGALink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsMaintPcntLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MpsAbsHrsLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccDescriptionLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccNumPersLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccSTHLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccOVTHoursLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccOVTPcntLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccContractLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccGALink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccMaintPcntLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccAbsHrsLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tlpStudyLevel As System.Windows.Forms.TableLayoutPanel

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_Personnel))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle53 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle46 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle47 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle48 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle49 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle50 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle51 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle52 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnlTools = New System.Windows.Forms.Panel()
        Me.gbTools = New System.Windows.Forms.GroupBox()
        Me.chkComplete = New System.Windows.Forms.CheckBox()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.lblView = New System.Windows.Forms.Label()
        Me.rbLinks = New System.Windows.Forms.RadioButton()
        Me.rbData = New System.Windows.Forms.RadioButton()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbSheets = New System.Windows.Forms.ComboBox()
        Me.tbFilePath = New System.Windows.Forms.TextBox()
        Me.rButtonProfile = New System.Windows.Forms.RadioButton()
        Me.rButtonStudy = New System.Windows.Forms.RadioButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.tlpStudyLevel = New System.Windows.Forms.TableLayoutPanel()
        Me.tabPageAbsences = New System.Windows.Forms.TabPage()
        Me.dgvABSLinks = New System.Windows.Forms.DataGridView()
        Me.dgvABS = New System.Windows.Forms.DataGridView()
        Me.gbFooterABS = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblABSHeader = New System.Windows.Forms.Label()
        Me.tabPageMPS = New System.Windows.Forms.TabPage()
        Me.dgvMPS = New System.Windows.Forms.DataGridView()
        Me.dgvMPSLinks = New System.Windows.Forms.DataGridView()
        Me.gbFooterMPS = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblMPSHeader = New System.Windows.Forms.Label()
        Me.tabPageOCC = New System.Windows.Forms.TabPage()
        Me.dgvOCC = New System.Windows.Forms.DataGridView()
        Me.dgvOCCLinks = New System.Windows.Forms.DataGridView()
        Me.gbFooterOCC = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblOCCHeader = New System.Windows.Forms.Label()
        Me.tabControlPers = New System.Windows.Forms.TabControl()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.OccDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccNumPers = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccSTH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccOVTHours = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccOVTPcnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccContract = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccMaintPcnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccAbsHrs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccDescriptionLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccNumPersLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccSTHLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccOVTHoursLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccOVTPcntLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccContractLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccGALink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccMaintPcntLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccAbsHrsLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsNumPers = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsSTH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsOVTHours = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsOVTPcnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsContract = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsMaintPcnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsAbsHrs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsDescriptionLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsNumPersLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsSTHLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsOVTHoursLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsOVTPcntLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsContractLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsGALink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsMaintPcntLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MpsAbsHrsLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OCCAbsLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MPSAbsLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AbsDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OCCAbs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MPSAbs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlTools.SuspendLayout()
        Me.gbTools.SuspendLayout()
        Me.tlpStudyLevel.SuspendLayout()
        Me.tabPageAbsences.SuspendLayout()
        CType(Me.dgvABSLinks, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvABS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFooterABS.SuspendLayout()
        Me.tabPageMPS.SuspendLayout()
        CType(Me.dgvMPS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMPSLinks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFooterMPS.SuspendLayout()
        Me.tabPageOCC.SuspendLayout()
        CType(Me.dgvOCC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvOCCLinks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFooterOCC.SuspendLayout()
        Me.tabControlPers.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTools
        '
        Me.pnlTools.BackColor = System.Drawing.Color.Transparent
        Me.pnlTools.Controls.Add(Me.gbTools)
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 105)
        Me.pnlTools.TabIndex = 880
        '
        'gbTools
        '
        Me.gbTools.BackColor = System.Drawing.Color.Transparent
        Me.gbTools.Controls.Add(Me.chkComplete)
        Me.gbTools.Controls.Add(Me.btnBrowse)
        Me.gbTools.Controls.Add(Me.lblView)
        Me.gbTools.Controls.Add(Me.rbLinks)
        Me.gbTools.Controls.Add(Me.rbData)
        Me.gbTools.Controls.Add(Me.Label10)
        Me.gbTools.Controls.Add(Me.Label11)
        Me.gbTools.Controls.Add(Me.cbSheets)
        Me.gbTools.Controls.Add(Me.tbFilePath)
        Me.gbTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTools.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbTools.Location = New System.Drawing.Point(0, 0)
        Me.gbTools.Name = "gbTools"
        Me.gbTools.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gbTools.Size = New System.Drawing.Size(742, 99)
        Me.gbTools.TabIndex = 942
        Me.gbTools.TabStop = False
        '
        'chkComplete
        '
        Me.chkComplete.AutoSize = True
        Me.chkComplete.BackColor = System.Drawing.Color.Transparent
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkComplete.Location = New System.Drawing.Point(9, 49)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(136, 17)
        Me.chkComplete.TabIndex = 961
        Me.chkComplete.Text = "Check when completed"
        Me.chkComplete.UseVisualStyleBackColor = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(711, 15)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(21, 21)
        Me.btnBrowse.TabIndex = 960
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for bridge workbook")
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'lblView
        '
        Me.lblView.AutoSize = True
        Me.lblView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblView.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblView.Location = New System.Drawing.Point(6, 72)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(33, 13)
        Me.lblView.TabIndex = 959
        Me.lblView.Text = "View"
        Me.lblView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbLinks
        '
        Me.rbLinks.AutoSize = True
        Me.rbLinks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLinks.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbLinks.Location = New System.Drawing.Point(92, 70)
        Me.rbLinks.Name = "rbLinks"
        Me.rbLinks.Size = New System.Drawing.Size(53, 17)
        Me.rbLinks.TabIndex = 958
        Me.rbLinks.Text = "LINKS"
        Me.rbLinks.UseVisualStyleBackColor = True
        '
        'rbData
        '
        Me.rbData.AutoSize = True
        Me.rbData.Checked = True
        Me.rbData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbData.Location = New System.Drawing.Point(41, 70)
        Me.rbData.Name = "rbData"
        Me.rbData.Size = New System.Drawing.Size(52, 17)
        Me.rbData.TabIndex = 957
        Me.rbData.TabStop = True
        Me.rbData.Text = "DATA"
        Me.rbData.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(389, 45)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 13)
        Me.Label10.TabIndex = 952
        Me.Label10.Text = "Worksheet"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(389, 18)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 951
        Me.Label11.Text = "Workbook"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbSheets
        '
        Me.cbSheets.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets.FormattingEnabled = True
        Me.cbSheets.Location = New System.Drawing.Point(464, 42)
        Me.cbSheets.Name = "cbSheets"
        Me.cbSheets.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets.TabIndex = 950
        '
        'tbFilePath
        '
        Me.tbFilePath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFilePath.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbFilePath.Location = New System.Drawing.Point(464, 15)
        Me.tbFilePath.Name = "tbFilePath"
        Me.tbFilePath.ReadOnly = True
        Me.tbFilePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbFilePath.Size = New System.Drawing.Size(241, 21)
        Me.tbFilePath.TabIndex = 948
        '
        'rButtonProfile
        '
        Me.rButtonProfile.AutoSize = True
        Me.rButtonProfile.BackColor = System.Drawing.Color.Transparent
        Me.rButtonProfile.Checked = True
        Me.rButtonProfile.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rButtonProfile.Location = New System.Drawing.Point(3, 3)
        Me.rButtonProfile.Name = "rButtonProfile"
        Me.rButtonProfile.Size = New System.Drawing.Size(99, 17)
        Me.rButtonProfile.TabIndex = 955
        Me.rButtonProfile.TabStop = True
        Me.rButtonProfile.Text = "PROFILE-LEVEL"
        Me.rButtonProfile.UseVisualStyleBackColor = False
        '
        'rButtonStudy
        '
        Me.rButtonStudy.AutoSize = True
        Me.rButtonStudy.BackColor = System.Drawing.Color.Transparent
        Me.rButtonStudy.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rButtonStudy.Location = New System.Drawing.Point(108, 3)
        Me.rButtonStudy.Name = "rButtonStudy"
        Me.rButtonStudy.Size = New System.Drawing.Size(89, 17)
        Me.rButtonStudy.TabIndex = 954
        Me.rButtonStudy.Text = "STUDY-LEVEL"
        Me.rButtonStudy.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon_Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon_Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(500, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 2
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Update input form with linked values")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'tlpStudyLevel
        '
        Me.tlpStudyLevel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlpStudyLevel.BackColor = System.Drawing.Color.Transparent
        Me.tlpStudyLevel.ColumnCount = 2
        Me.tlpStudyLevel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpStudyLevel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpStudyLevel.Controls.Add(Me.rButtonStudy, 1, 0)
        Me.tlpStudyLevel.Controls.Add(Me.rButtonProfile, 0, 0)
        Me.tlpStudyLevel.Location = New System.Drawing.Point(546, 142)
        Me.tlpStudyLevel.Name = "tlpStudyLevel"
        Me.tlpStudyLevel.RowCount = 1
        Me.tlpStudyLevel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tlpStudyLevel.Size = New System.Drawing.Size(197, 22)
        Me.tlpStudyLevel.TabIndex = 958
        '
        'tabPageAbsences
        '
        Me.tabPageAbsences.BackColor = System.Drawing.Color.Transparent
        Me.tabPageAbsences.Controls.Add(Me.dgvABS)
        Me.tabPageAbsences.Controls.Add(Me.gbFooterABS)
        Me.tabPageAbsences.Controls.Add(Me.lblABSHeader)
        Me.tabPageAbsences.Controls.Add(Me.dgvABSLinks)
        Me.tabPageAbsences.Location = New System.Drawing.Point(4, 22)
        Me.tabPageAbsences.Name = "tabPageAbsences"
        Me.tabPageAbsences.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.tabPageAbsences.Size = New System.Drawing.Size(734, 425)
        Me.tabPageAbsences.TabIndex = 2
        Me.tabPageAbsences.Text = "Table 7"
        Me.tabPageAbsences.UseVisualStyleBackColor = True
        '
        'dgvABSLinks
        '
        Me.dgvABSLinks.AllowUserToAddRows = False
        Me.dgvABSLinks.AllowUserToDeleteRows = False
        Me.dgvABSLinks.AllowUserToResizeColumns = False
        Me.dgvABSLinks.AllowUserToResizeRows = False
        Me.dgvABSLinks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvABSLinks.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvABSLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvABSLinks.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn37, Me.OCCAbsLink, Me.MPSAbsLink})
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvABSLinks.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgvABSLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvABSLinks.Location = New System.Drawing.Point(6, 0)
        Me.dgvABSLinks.Name = "dgvABSLinks"
        Me.dgvABSLinks.RowHeadersVisible = False
        Me.dgvABSLinks.RowHeadersWidth = 25
        Me.dgvABSLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvABSLinks.Size = New System.Drawing.Size(722, 419)
        Me.dgvABSLinks.TabIndex = 1037
        '
        'dgvABS
        '
        Me.dgvABS.AllowUserToAddRows = False
        Me.dgvABS.AllowUserToDeleteRows = False
        Me.dgvABS.AllowUserToResizeColumns = False
        Me.dgvABS.AllowUserToResizeRows = False
        Me.dgvABS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvABS.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvABS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvABS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AbsDescription, Me.OCCAbs, Me.MPSAbs})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvABS.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvABS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvABS.Location = New System.Drawing.Point(6, 36)
        Me.dgvABS.MultiSelect = False
        Me.dgvABS.Name = "dgvABS"
        Me.dgvABS.RowHeadersVisible = False
        Me.dgvABS.RowHeadersWidth = 25
        Me.dgvABS.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvABS.Size = New System.Drawing.Size(722, 305)
        Me.dgvABS.TabIndex = 1038
        '
        'gbFooterABS
        '
        Me.gbFooterABS.Controls.Add(Me.Label6)
        Me.gbFooterABS.Controls.Add(Me.Label7)
        Me.gbFooterABS.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbFooterABS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFooterABS.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbFooterABS.Location = New System.Drawing.Point(6, 341)
        Me.gbFooterABS.Name = "gbFooterABS"
        Me.gbFooterABS.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbFooterABS.Size = New System.Drawing.Size(722, 78)
        Me.gbFooterABS.TabIndex = 1039
        Me.gbFooterABS.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(6, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 13)
        Me.Label6.TabIndex = 945
        Me.Label6.Text = "Helpful hint:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(6, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(302, 13)
        Me.Label7.TabIndex = 944
        Me.Label7.Text = "Entering Profile-level data clears associated Study-level data."
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblABSHeader
        '
        Me.lblABSHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblABSHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblABSHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblABSHeader.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblABSHeader.Location = New System.Drawing.Point(6, 0)
        Me.lblABSHeader.Name = "lblABSHeader"
        Me.lblABSHeader.Size = New System.Drawing.Size(722, 36)
        Me.lblABSHeader.TabIndex = 869
        Me.lblABSHeader.Text = "Company Employee Absenses"
        Me.lblABSHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabPageMPS
        '
        Me.tabPageMPS.Controls.Add(Me.gbFooterMPS)
        Me.tabPageMPS.Controls.Add(Me.lblMPSHeader)
        Me.tabPageMPS.Controls.Add(Me.dgvMPS)
        Me.tabPageMPS.Controls.Add(Me.dgvMPSLinks)
        Me.tabPageMPS.Location = New System.Drawing.Point(4, 22)
        Me.tabPageMPS.Name = "tabPageMPS"
        Me.tabPageMPS.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.tabPageMPS.Size = New System.Drawing.Size(734, 425)
        Me.tabPageMPS.TabIndex = 1
        Me.tabPageMPS.Text = "Table 6"
        Me.tabPageMPS.UseVisualStyleBackColor = True
        '
        'dgvMPS
        '
        Me.dgvMPS.AllowUserToAddRows = False
        Me.dgvMPS.AllowUserToDeleteRows = False
        Me.dgvMPS.AllowUserToResizeColumns = False
        Me.dgvMPS.AllowUserToResizeRows = False
        Me.dgvMPS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMPS.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvMPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMPS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MpsDescription, Me.MpsNumPers, Me.MpsSTH, Me.MpsOVTHours, Me.MpsOVTPcnt, Me.MpsContract, Me.MpsGA, Me.MpsMaintPcnt, Me.MpsAbsHrs})
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMPS.DefaultCellStyle = DataGridViewCellStyle21
        Me.dgvMPS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMPS.Location = New System.Drawing.Point(6, 0)
        Me.dgvMPS.MultiSelect = False
        Me.dgvMPS.Name = "dgvMPS"
        Me.dgvMPS.RowHeadersVisible = False
        Me.dgvMPS.RowHeadersWidth = 25
        Me.dgvMPS.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvMPS.Size = New System.Drawing.Size(722, 419)
        Me.dgvMPS.TabIndex = 1035
        '
        'dgvMPSLinks
        '
        Me.dgvMPSLinks.AllowUserToAddRows = False
        Me.dgvMPSLinks.AllowUserToDeleteRows = False
        Me.dgvMPSLinks.AllowUserToResizeColumns = False
        Me.dgvMPSLinks.AllowUserToResizeRows = False
        Me.dgvMPSLinks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMPSLinks.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.dgvMPSLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMPSLinks.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MpsDescriptionLink, Me.MpsNumPersLink, Me.MpsSTHLink, Me.MpsOVTHoursLink, Me.MpsOVTPcntLink, Me.MpsContractLink, Me.MpsGALink, Me.MpsMaintPcntLink, Me.MpsAbsHrsLink})
        Me.dgvMPSLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMPSLinks.Location = New System.Drawing.Point(6, 0)
        Me.dgvMPSLinks.Name = "dgvMPSLinks"
        Me.dgvMPSLinks.RowHeadersVisible = False
        Me.dgvMPSLinks.RowHeadersWidth = 25
        Me.dgvMPSLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvMPSLinks.Size = New System.Drawing.Size(722, 419)
        Me.dgvMPSLinks.TabIndex = 1036
        '
        'gbFooterMPS
        '
        Me.gbFooterMPS.Controls.Add(Me.Label4)
        Me.gbFooterMPS.Controls.Add(Me.Label5)
        Me.gbFooterMPS.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbFooterMPS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFooterMPS.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbFooterMPS.Location = New System.Drawing.Point(6, 341)
        Me.gbFooterMPS.Name = "gbFooterMPS"
        Me.gbFooterMPS.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbFooterMPS.Size = New System.Drawing.Size(722, 78)
        Me.gbFooterMPS.TabIndex = 1037
        Me.gbFooterMPS.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(6, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 13)
        Me.Label4.TabIndex = 945
        Me.Label4.Text = "Helpful hint:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(6, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(302, 13)
        Me.Label5.TabIndex = 944
        Me.Label5.Text = "Entering Profile-level data clears associated Study-level data."
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMPSHeader
        '
        Me.lblMPSHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblMPSHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblMPSHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMPSHeader.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblMPSHeader.Location = New System.Drawing.Point(6, 0)
        Me.lblMPSHeader.Name = "lblMPSHeader"
        Me.lblMPSHeader.Size = New System.Drawing.Size(722, 36)
        Me.lblMPSHeader.TabIndex = 919
        Me.lblMPSHeader.Text = "Management, Professional && Staff"
        Me.lblMPSHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabPageOCC
        '
        Me.tabPageOCC.BackColor = System.Drawing.Color.Transparent
        Me.tabPageOCC.Controls.Add(Me.gbFooterOCC)
        Me.tabPageOCC.Controls.Add(Me.lblOCCHeader)
        Me.tabPageOCC.Controls.Add(Me.dgvOCC)
        Me.tabPageOCC.Controls.Add(Me.dgvOCCLinks)
        Me.tabPageOCC.Location = New System.Drawing.Point(4, 22)
        Me.tabPageOCC.Name = "tabPageOCC"
        Me.tabPageOCC.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.tabPageOCC.Size = New System.Drawing.Size(734, 425)
        Me.tabPageOCC.TabIndex = 0
        Me.tabPageOCC.Text = "Table 5"
        Me.tabPageOCC.UseVisualStyleBackColor = True
        '
        'dgvOCC
        '
        Me.dgvOCC.AllowUserToAddRows = False
        Me.dgvOCC.AllowUserToDeleteRows = False
        Me.dgvOCC.AllowUserToResizeColumns = False
        Me.dgvOCC.AllowUserToResizeRows = False
        Me.dgvOCC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOCC.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle32
        Me.dgvOCC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOCC.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OccDescription, Me.OccNumPers, Me.OccSTH, Me.OccOVTHours, Me.OccOVTPcnt, Me.OccContract, Me.OccGA, Me.OccMaintPcnt, Me.OccAbsHrs})
        DataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle42.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle42.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOCC.DefaultCellStyle = DataGridViewCellStyle42
        Me.dgvOCC.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOCC.Location = New System.Drawing.Point(6, 0)
        Me.dgvOCC.MultiSelect = False
        Me.dgvOCC.Name = "dgvOCC"
        Me.dgvOCC.RowHeadersVisible = False
        Me.dgvOCC.RowHeadersWidth = 25
        Me.dgvOCC.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOCC.Size = New System.Drawing.Size(722, 419)
        Me.dgvOCC.TabIndex = 1021
        '
        'dgvOCCLinks
        '
        Me.dgvOCCLinks.AllowUserToAddRows = False
        Me.dgvOCCLinks.AllowUserToDeleteRows = False
        Me.dgvOCCLinks.AllowUserToResizeColumns = False
        Me.dgvOCCLinks.AllowUserToResizeRows = False
        Me.dgvOCCLinks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle43.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle43.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle43.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOCCLinks.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle43
        Me.dgvOCCLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOCCLinks.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OccDescriptionLink, Me.OccNumPersLink, Me.OccSTHLink, Me.OccOVTHoursLink, Me.OccOVTPcntLink, Me.OccContractLink, Me.OccGALink, Me.OccMaintPcntLink, Me.OccAbsHrsLink})
        DataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle53.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle53.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle53.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle53.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle53.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOCCLinks.DefaultCellStyle = DataGridViewCellStyle53
        Me.dgvOCCLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOCCLinks.Location = New System.Drawing.Point(6, 0)
        Me.dgvOCCLinks.Name = "dgvOCCLinks"
        Me.dgvOCCLinks.RowHeadersVisible = False
        Me.dgvOCCLinks.RowHeadersWidth = 25
        Me.dgvOCCLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOCCLinks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvOCCLinks.Size = New System.Drawing.Size(722, 419)
        Me.dgvOCCLinks.TabIndex = 1024
        '
        'gbFooterOCC
        '
        Me.gbFooterOCC.Controls.Add(Me.Label2)
        Me.gbFooterOCC.Controls.Add(Me.Label3)
        Me.gbFooterOCC.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbFooterOCC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFooterOCC.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbFooterOCC.Location = New System.Drawing.Point(6, 341)
        Me.gbFooterOCC.Name = "gbFooterOCC"
        Me.gbFooterOCC.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbFooterOCC.Size = New System.Drawing.Size(722, 78)
        Me.gbFooterOCC.TabIndex = 1025
        Me.gbFooterOCC.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(6, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 945
        Me.Label2.Text = "Helpful hint:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(6, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(302, 13)
        Me.Label3.TabIndex = 944
        Me.Label3.Text = "Entering Profile-level data clears associated Study-level data."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOCCHeader
        '
        Me.lblOCCHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblOCCHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblOCCHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOCCHeader.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblOCCHeader.Location = New System.Drawing.Point(6, 0)
        Me.lblOCCHeader.Name = "lblOCCHeader"
        Me.lblOCCHeader.Size = New System.Drawing.Size(722, 36)
        Me.lblOCCHeader.TabIndex = 868
        Me.lblOCCHeader.Text = "Operator, Craft && Clerical"
        Me.lblOCCHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabControlPers
        '
        Me.tabControlPers.Controls.Add(Me.tabPageOCC)
        Me.tabControlPers.Controls.Add(Me.tabPageMPS)
        Me.tabControlPers.Controls.Add(Me.tabPageAbsences)
        Me.tabControlPers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabControlPers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabControlPers.ItemSize = New System.Drawing.Size(100, 18)
        Me.tabControlPers.Location = New System.Drawing.Point(4, 145)
        Me.tabControlPers.Name = "tabControlPers"
        Me.tabControlPers.SelectedIndex = 0
        Me.tabControlPers.Size = New System.Drawing.Size(742, 451)
        Me.tabControlPers.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tabControlPers.TabIndex = 957
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1033
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(500, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Personnel"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'OccDescription
        '
        Me.OccDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.OccDescription.DataPropertyName = "Description"
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle33.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle33.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.OccDescription.DefaultCellStyle = DataGridViewCellStyle33
        Me.OccDescription.HeaderText = ""
        Me.OccDescription.Name = "OccDescription"
        Me.OccDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'OccNumPers
        '
        Me.OccNumPers.DataPropertyName = "NumPers"
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle34.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle34.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle34.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle34.Format = "N1"
        Me.OccNumPers.DefaultCellStyle = DataGridViewCellStyle34
        Me.OccNumPers.HeaderText = "Number of Personnel"
        Me.OccNumPers.Name = "OccNumPers"
        Me.OccNumPers.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccNumPers.Width = 75
        '
        'OccSTH
        '
        Me.OccSTH.DataPropertyName = "STH"
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle35.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle35.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle35.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle35.Format = "N0"
        Me.OccSTH.DefaultCellStyle = DataGridViewCellStyle35
        Me.OccSTH.HeaderText = "Straight Time Hours"
        Me.OccSTH.Name = "OccSTH"
        Me.OccSTH.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccSTH.Width = 75
        '
        'OccOVTHours
        '
        Me.OccOVTHours.DataPropertyName = "OVTHours"
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle36.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle36.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle36.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle36.Format = "N0"
        Me.OccOVTHours.DefaultCellStyle = DataGridViewCellStyle36
        Me.OccOVTHours.HeaderText = "Overtime Hours"
        Me.OccOVTHours.Name = "OccOVTHours"
        Me.OccOVTHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccOVTHours.Width = 75
        '
        'OccOVTPcnt
        '
        Me.OccOVTPcnt.DataPropertyName = "OVTPcnt"
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle37.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle37.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle37.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle37.Format = "N1"
        Me.OccOVTPcnt.DefaultCellStyle = DataGridViewCellStyle37
        Me.OccOVTPcnt.HeaderText = "Overtime Percent"
        Me.OccOVTPcnt.Name = "OccOVTPcnt"
        Me.OccOVTPcnt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccOVTPcnt.Visible = False
        Me.OccOVTPcnt.Width = 75
        '
        'OccContract
        '
        Me.OccContract.DataPropertyName = "Contract"
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle38.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle38.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle38.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle38.Format = "N0"
        Me.OccContract.DefaultCellStyle = DataGridViewCellStyle38
        Me.OccContract.HeaderText = "Contract Personnel Total Hours"
        Me.OccContract.Name = "OccContract"
        Me.OccContract.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccContract.Width = 75
        '
        'OccGA
        '
        Me.OccGA.DataPropertyName = "GA"
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle39.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle39.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle39.Format = "N0"
        Me.OccGA.DefaultCellStyle = DataGridViewCellStyle39
        Me.OccGA.HeaderText = "Utilized G&A Total Hours"
        Me.OccGA.Name = "OccGA"
        Me.OccGA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccGA.Width = 75
        '
        'OccMaintPcnt
        '
        Me.OccMaintPcnt.DataPropertyName = "MaintPcnt"
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle40.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle40.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle40.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle40.Format = "N1"
        Me.OccMaintPcnt.DefaultCellStyle = DataGridViewCellStyle40
        Me.OccMaintPcnt.HeaderText = "Percent Dedicated to Maintenance"
        Me.OccMaintPcnt.Name = "OccMaintPcnt"
        Me.OccMaintPcnt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccMaintPcnt.Visible = False
        Me.OccMaintPcnt.Width = 75
        '
        'OccAbsHrs
        '
        Me.OccAbsHrs.DataPropertyName = "AbsHrs"
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle41.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle41.Format = "N1"
        Me.OccAbsHrs.DefaultCellStyle = DataGridViewCellStyle41
        Me.OccAbsHrs.HeaderText = "Absense Hours"
        Me.OccAbsHrs.Name = "OccAbsHrs"
        Me.OccAbsHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'OccDescriptionLink
        '
        Me.OccDescriptionLink.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.OccDescriptionLink.DataPropertyName = "Description"
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle44.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle44.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.OccDescriptionLink.DefaultCellStyle = DataGridViewCellStyle44
        Me.OccDescriptionLink.HeaderText = ""
        Me.OccDescriptionLink.Name = "OccDescriptionLink"
        Me.OccDescriptionLink.ReadOnly = True
        Me.OccDescriptionLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'OccNumPersLink
        '
        Me.OccNumPersLink.DataPropertyName = "NumPers"
        DataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle45.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle45.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle45.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccNumPersLink.DefaultCellStyle = DataGridViewCellStyle45
        Me.OccNumPersLink.HeaderText = "Number of Personnel Cell Reference"
        Me.OccNumPersLink.Name = "OccNumPersLink"
        Me.OccNumPersLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccNumPersLink.Width = 75
        '
        'OccSTHLink
        '
        Me.OccSTHLink.DataPropertyName = "STH"
        DataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle46.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle46.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle46.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccSTHLink.DefaultCellStyle = DataGridViewCellStyle46
        Me.OccSTHLink.HeaderText = "Straight Time Hours Cell Reference"
        Me.OccSTHLink.Name = "OccSTHLink"
        Me.OccSTHLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccSTHLink.Width = 75
        '
        'OccOVTHoursLink
        '
        Me.OccOVTHoursLink.DataPropertyName = "OVTHours"
        DataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle47.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle47.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle47.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccOVTHoursLink.DefaultCellStyle = DataGridViewCellStyle47
        Me.OccOVTHoursLink.HeaderText = "Overtime Hours Cell Reference"
        Me.OccOVTHoursLink.Name = "OccOVTHoursLink"
        Me.OccOVTHoursLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccOVTHoursLink.Width = 75
        '
        'OccOVTPcntLink
        '
        Me.OccOVTPcntLink.DataPropertyName = "OVTPcnt"
        DataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle48.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle48.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle48.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccOVTPcntLink.DefaultCellStyle = DataGridViewCellStyle48
        Me.OccOVTPcntLink.HeaderText = "Overtime Percent Cell Reference"
        Me.OccOVTPcntLink.Name = "OccOVTPcntLink"
        Me.OccOVTPcntLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccOVTPcntLink.Visible = False
        Me.OccOVTPcntLink.Width = 75
        '
        'OccContractLink
        '
        Me.OccContractLink.DataPropertyName = "Contract"
        DataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle49.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle49.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle49.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccContractLink.DefaultCellStyle = DataGridViewCellStyle49
        Me.OccContractLink.HeaderText = "Contract Personnel Total Hours Cell Reference"
        Me.OccContractLink.Name = "OccContractLink"
        Me.OccContractLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccContractLink.Width = 75
        '
        'OccGALink
        '
        Me.OccGALink.DataPropertyName = "GA"
        DataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle50.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle50.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle50.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccGALink.DefaultCellStyle = DataGridViewCellStyle50
        Me.OccGALink.HeaderText = "Utilized G&A Total Hours Cell Reference"
        Me.OccGALink.Name = "OccGALink"
        Me.OccGALink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccGALink.Width = 75
        '
        'OccMaintPcntLink
        '
        Me.OccMaintPcntLink.DataPropertyName = "MaintPcnt"
        DataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle51.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle51.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle51.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccMaintPcntLink.DefaultCellStyle = DataGridViewCellStyle51
        Me.OccMaintPcntLink.HeaderText = "Percent Dedicated to Maintenance Cell Reference"
        Me.OccMaintPcntLink.Name = "OccMaintPcntLink"
        Me.OccMaintPcntLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OccMaintPcntLink.Visible = False
        Me.OccMaintPcntLink.Width = 75
        '
        'OccAbsHrsLink
        '
        Me.OccAbsHrsLink.DataPropertyName = "AbsHrs"
        DataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle52.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle52.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle52.ForeColor = System.Drawing.Color.MediumBlue
        Me.OccAbsHrsLink.DefaultCellStyle = DataGridViewCellStyle52
        Me.OccAbsHrsLink.HeaderText = "Absence Hours Cell Reference"
        Me.OccAbsHrsLink.Name = "OccAbsHrsLink"
        Me.OccAbsHrsLink.ReadOnly = True
        '
        'MpsDescription
        '
        Me.MpsDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MpsDescription.DataPropertyName = "Description"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MpsDescription.DefaultCellStyle = DataGridViewCellStyle12
        Me.MpsDescription.HeaderText = ""
        Me.MpsDescription.Name = "MpsDescription"
        Me.MpsDescription.ReadOnly = True
        Me.MpsDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'MpsNumPers
        '
        Me.MpsNumPers.DataPropertyName = "NumPers"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle13.Format = "N1"
        Me.MpsNumPers.DefaultCellStyle = DataGridViewCellStyle13
        Me.MpsNumPers.HeaderText = "Number of Personnel"
        Me.MpsNumPers.Name = "MpsNumPers"
        Me.MpsNumPers.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsNumPers.Width = 75
        '
        'MpsSTH
        '
        Me.MpsSTH.DataPropertyName = "STH"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle14.Format = "N0"
        Me.MpsSTH.DefaultCellStyle = DataGridViewCellStyle14
        Me.MpsSTH.HeaderText = "Straight Time Hours"
        Me.MpsSTH.Name = "MpsSTH"
        Me.MpsSTH.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsSTH.Width = 75
        '
        'MpsOVTHours
        '
        Me.MpsOVTHours.DataPropertyName = "OVTHours"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle15.Format = "N0"
        Me.MpsOVTHours.DefaultCellStyle = DataGridViewCellStyle15
        Me.MpsOVTHours.HeaderText = "Overtime Hours"
        Me.MpsOVTHours.Name = "MpsOVTHours"
        Me.MpsOVTHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsOVTHours.Visible = False
        Me.MpsOVTHours.Width = 75
        '
        'MpsOVTPcnt
        '
        Me.MpsOVTPcnt.DataPropertyName = "OVTPcnt"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle16.Format = "N1"
        Me.MpsOVTPcnt.DefaultCellStyle = DataGridViewCellStyle16
        Me.MpsOVTPcnt.HeaderText = "Overtime Percent"
        Me.MpsOVTPcnt.Name = "MpsOVTPcnt"
        Me.MpsOVTPcnt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsOVTPcnt.Width = 75
        '
        'MpsContract
        '
        Me.MpsContract.DataPropertyName = "Contract"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle17.Format = "N0"
        Me.MpsContract.DefaultCellStyle = DataGridViewCellStyle17
        Me.MpsContract.HeaderText = "Contract Personnel Total Hours"
        Me.MpsContract.Name = "MpsContract"
        Me.MpsContract.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsContract.Width = 75
        '
        'MpsGA
        '
        Me.MpsGA.DataPropertyName = "GA"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle18.Format = "N0"
        Me.MpsGA.DefaultCellStyle = DataGridViewCellStyle18
        Me.MpsGA.HeaderText = "Utilized G&A Total Hours"
        Me.MpsGA.Name = "MpsGA"
        Me.MpsGA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsGA.Width = 75
        '
        'MpsMaintPcnt
        '
        Me.MpsMaintPcnt.DataPropertyName = "MaintPcnt"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle19.Format = "N1"
        Me.MpsMaintPcnt.DefaultCellStyle = DataGridViewCellStyle19
        Me.MpsMaintPcnt.HeaderText = "Percent Dedicated to Maintenance"
        Me.MpsMaintPcnt.Name = "MpsMaintPcnt"
        Me.MpsMaintPcnt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsMaintPcnt.Visible = False
        Me.MpsMaintPcnt.Width = 85
        '
        'MpsAbsHrs
        '
        Me.MpsAbsHrs.DataPropertyName = "AbsHrs"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.Format = "N1"
        Me.MpsAbsHrs.DefaultCellStyle = DataGridViewCellStyle20
        Me.MpsAbsHrs.HeaderText = "Absence Hours"
        Me.MpsAbsHrs.Name = "MpsAbsHrs"
        Me.MpsAbsHrs.ReadOnly = True
        '
        'MpsDescriptionLink
        '
        Me.MpsDescriptionLink.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MpsDescriptionLink.DataPropertyName = "Description"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MpsDescriptionLink.DefaultCellStyle = DataGridViewCellStyle23
        Me.MpsDescriptionLink.HeaderText = ""
        Me.MpsDescriptionLink.Name = "MpsDescriptionLink"
        Me.MpsDescriptionLink.ReadOnly = True
        Me.MpsDescriptionLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'MpsNumPersLink
        '
        Me.MpsNumPersLink.DataPropertyName = "NumPers"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsNumPersLink.DefaultCellStyle = DataGridViewCellStyle24
        Me.MpsNumPersLink.HeaderText = "Number of Personnel Cell Reference"
        Me.MpsNumPersLink.Name = "MpsNumPersLink"
        Me.MpsNumPersLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsNumPersLink.Width = 75
        '
        'MpsSTHLink
        '
        Me.MpsSTHLink.DataPropertyName = "STH"
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsSTHLink.DefaultCellStyle = DataGridViewCellStyle25
        Me.MpsSTHLink.HeaderText = "Straight Time Hours Cell Reference"
        Me.MpsSTHLink.Name = "MpsSTHLink"
        Me.MpsSTHLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsSTHLink.Width = 75
        '
        'MpsOVTHoursLink
        '
        Me.MpsOVTHoursLink.DataPropertyName = "OVTHours"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsOVTHoursLink.DefaultCellStyle = DataGridViewCellStyle26
        Me.MpsOVTHoursLink.HeaderText = "Overtime Hours Cell Reference"
        Me.MpsOVTHoursLink.Name = "MpsOVTHoursLink"
        Me.MpsOVTHoursLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsOVTHoursLink.Visible = False
        Me.MpsOVTHoursLink.Width = 75
        '
        'MpsOVTPcntLink
        '
        Me.MpsOVTPcntLink.DataPropertyName = "OVTPcnt"
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsOVTPcntLink.DefaultCellStyle = DataGridViewCellStyle27
        Me.MpsOVTPcntLink.HeaderText = "Overtime Percent Cell Reference"
        Me.MpsOVTPcntLink.Name = "MpsOVTPcntLink"
        Me.MpsOVTPcntLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsOVTPcntLink.Width = 75
        '
        'MpsContractLink
        '
        Me.MpsContractLink.DataPropertyName = "Contract"
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsContractLink.DefaultCellStyle = DataGridViewCellStyle28
        Me.MpsContractLink.HeaderText = "Contract Personnel Total Hours Cell Reference"
        Me.MpsContractLink.Name = "MpsContractLink"
        Me.MpsContractLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsContractLink.Width = 75
        '
        'MpsGALink
        '
        Me.MpsGALink.DataPropertyName = "GA"
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsGALink.DefaultCellStyle = DataGridViewCellStyle29
        Me.MpsGALink.HeaderText = "Utilized G&A Total Hours Cell Reference"
        Me.MpsGALink.Name = "MpsGALink"
        Me.MpsGALink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsGALink.Width = 75
        '
        'MpsMaintPcntLink
        '
        Me.MpsMaintPcntLink.DataPropertyName = "MaintPcnt"
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle30.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsMaintPcntLink.DefaultCellStyle = DataGridViewCellStyle30
        Me.MpsMaintPcntLink.HeaderText = "Percent Dedicated to Maintenance Cell Reference"
        Me.MpsMaintPcntLink.Name = "MpsMaintPcntLink"
        Me.MpsMaintPcntLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MpsMaintPcntLink.Visible = False
        Me.MpsMaintPcntLink.Width = 85
        '
        'MpsAbsHrsLink
        '
        Me.MpsAbsHrsLink.DataPropertyName = "AbsHrs"
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle31.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.Color.MediumBlue
        Me.MpsAbsHrsLink.DefaultCellStyle = DataGridViewCellStyle31
        Me.MpsAbsHrsLink.HeaderText = "Absence Hours Cell Reference"
        Me.MpsAbsHrsLink.Name = "MpsAbsHrsLink"
        Me.MpsAbsHrsLink.ReadOnly = True
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.DataPropertyName = "Description"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DataGridViewTextBoxColumn37.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn37.FillWeight = 300.0!
        Me.DataGridViewTextBoxColumn37.HeaderText = ""
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        Me.DataGridViewTextBoxColumn37.ReadOnly = True
        Me.DataGridViewTextBoxColumn37.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn37.Width = 300
        '
        'OCCAbsLink
        '
        Me.OCCAbsLink.DataPropertyName = "OCCAbs"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.MediumBlue
        Me.OCCAbsLink.DefaultCellStyle = DataGridViewCellStyle8
        Me.OCCAbsLink.FillWeight = 70.0!
        Me.OCCAbsLink.HeaderText = "O,C&C Hours Cell Reference"
        Me.OCCAbsLink.Name = "OCCAbsLink"
        Me.OCCAbsLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OCCAbsLink.Width = 70
        '
        'MPSAbsLink
        '
        Me.MPSAbsLink.DataPropertyName = "MPSAbs"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.MediumBlue
        Me.MPSAbsLink.DefaultCellStyle = DataGridViewCellStyle9
        Me.MPSAbsLink.FillWeight = 70.0!
        Me.MPSAbsLink.HeaderText = "M,P&S Hours Cell Reference"
        Me.MPSAbsLink.Name = "MPSAbsLink"
        Me.MPSAbsLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MPSAbsLink.Width = 70
        '
        'AbsDescription
        '
        Me.AbsDescription.DataPropertyName = "Description"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.AbsDescription.DefaultCellStyle = DataGridViewCellStyle2
        Me.AbsDescription.FillWeight = 300.0!
        Me.AbsDescription.HeaderText = ""
        Me.AbsDescription.Name = "AbsDescription"
        Me.AbsDescription.ReadOnly = True
        Me.AbsDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AbsDescription.Width = 300
        '
        'OCCAbs
        '
        Me.OCCAbs.DataPropertyName = "OCCAbs"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle3.Format = "N0"
        Me.OCCAbs.DefaultCellStyle = DataGridViewCellStyle3
        Me.OCCAbs.FillWeight = 70.0!
        Me.OCCAbs.HeaderText = "O,C&C Hours"
        Me.OCCAbs.Name = "OCCAbs"
        Me.OCCAbs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OCCAbs.Width = 70
        '
        'MPSAbs
        '
        Me.MPSAbs.DataPropertyName = "MPSAbs"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle4.Format = "N0"
        Me.MPSAbs.DefaultCellStyle = DataGridViewCellStyle4
        Me.MPSAbs.FillWeight = 70.0!
        Me.MPSAbs.HeaderText = "M,P&S Hours"
        Me.MPSAbs.Name = "MPSAbs"
        Me.MPSAbs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MPSAbs.Width = 70
        '
        'SA_Personnel
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tlpStudyLevel)
        Me.Controls.Add(Me.tabControlPers)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Name = "SA_Personnel"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.pnlTools.ResumeLayout(False)
        Me.gbTools.ResumeLayout(False)
        Me.gbTools.PerformLayout()
        Me.tlpStudyLevel.ResumeLayout(False)
        Me.tlpStudyLevel.PerformLayout()
        Me.tabPageAbsences.ResumeLayout(False)
        CType(Me.dgvABSLinks, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvABS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFooterABS.ResumeLayout(False)
        Me.gbFooterABS.PerformLayout()
        Me.tabPageMPS.ResumeLayout(False)
        CType(Me.dgvMPS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMPSLinks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFooterMPS.ResumeLayout(False)
        Me.gbFooterMPS.PerformLayout()
        Me.tabPageOCC.ResumeLayout(False)
        CType(Me.dgvOCC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvOCCLinks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFooterOCC.ResumeLayout(False)
        Me.gbFooterOCC.PerformLayout()
        Me.tabControlPers.ResumeLayout(False)
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private IsLoaded, HasRights As Boolean
    Private MyParent As Main
    Friend PersonnelNotSaved As Boolean
    Friend NotFirstTime As Boolean = False

    Private dvWB As New DataView
    Private dvOCC As New DataView
    Private dvMPS As New DataView
    Private dvABS As New DataView

    Private dvOCCLinks As New DataView
    Private dvMPSLinks As New DataView
    Private dvABSLinks As New DataView

    Friend Sub SetDataSet()
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.PersRights

        dvOCC.Table = MyParent.dsPersonnel.Tables("Pers")
        dvOCC.RowFilter = "PersID like 'OCC%'" ' and IncludeForInput = 'Y'"
        dvOCC.Sort = "SortKey ASC"
        dgvOCC.DataSource = dvOCC


        dvMPS.Table = MyParent.dsPersonnel.Tables("Pers")
        dvMPS.RowFilter = "PersID like 'MPS%'" ' and IncludeForInput = 'Y'"
        dvMPS.Sort = "SortKey ASC"
        dgvMPS.DataSource = dvMPS

        dvABS.Table = MyParent.dsPersonnel.Tables("Absence")
        dvABS.RowFilter = "" ' IncludeForInput = 'Y'"
        dvABS.Sort = "SortKey ASC"
        dgvABS.DataSource = dvABS

        dvOCCLinks.Table = MyParent.dsMappings.Tables("Personnel")
        dvOCCLinks.RowFilter = "PersID like 'OCC%'" ' and IncludeForInput = 'Y'"
        dvOCCLinks.Sort = "SortKey ASC"
        dgvOCCLinks.DataSource = dvOCCLinks

        dvMPSLinks.Table = MyParent.dsMappings.Tables("Personnel")
        dvMPSLinks.RowFilter = "PersID like 'MPS%'" ' and IncludeForInput = 'Y'"
        dvMPSLinks.Sort = "SortKey ASC"
        dgvMPSLinks.DataSource = dvMPSLinks

        dvABSLinks.Table = MyParent.dsMappings.Tables("Absence")
        dvABSLinks.RowFilter = "" ' IncludeForInput = 'Y'"
        dvABSLinks.Sort = "SortKey ASC"
        dgvABSLinks.DataSource = dvABSLinks

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Personnel'")
        chkComplete.Checked = CBool(row(0)!Checked)

        'tbFilePath.Text = My.Settings.workbookPersonnel
        'cbSheets.Text = My.Settings.worksheetPersonnel

        Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = 'Pers'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
            cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
        End If

        rButtonProfile.Checked = Not My.Settings.viewDetailPersonnel
        rButtonStudy.Checked = My.Settings.viewDetailPersonnel

        IsLoaded = True
        NotFirstTime = True
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        If ProfileMsgBox("YN", "CLEAR", "personnel tables") = DialogResult.Yes Then

            Select Case Me.tabControlPers.SelectedTab.Name.ToString()
                Case "tabPageOCC"
                    If rbData.Checked Then
                        For Each r As DataGridViewRow In Me.dgvOCC.Rows
                            r.Cells("OccNumPers").Value = DBNull.Value
                            r.Cells("OccSTH").Value = DBNull.Value
                            r.Cells("OccOVTHours").Value = DBNull.Value
                            r.Cells("OccOVTPcnt").Value = DBNull.Value
                            r.Cells("OccContract").Value = DBNull.Value
                            r.Cells("OccGA").Value = DBNull.Value
                            r.Cells("OccMaintPcnt").Value = DBNull.Value
                            r.Cells("OccAbsHrs").Value = DBNull.Value
                        Next r
                    Else
                        For Each r As DataGridViewRow In Me.dgvOCCLinks.Rows
                            r.Cells("OccNumPersLink").Value = DBNull.Value
                            r.Cells("OccSTHLink").Value = DBNull.Value
                            r.Cells("OccOVTHoursLink").Value = DBNull.Value
                            r.Cells("OccOVTPcntLink").Value = DBNull.Value
                            r.Cells("OccContractLink").Value = DBNull.Value
                            r.Cells("OccGALink").Value = DBNull.Value
                            r.Cells("OccMaintPcntLink").Value = DBNull.Value
                            r.Cells("OccAbsHrsLink").Value = DBNull.Value
                        Next r
                    End If
                Case "tabPageMPS"
                    If rbData.Checked Then
                        For Each r As DataGridViewRow In Me.dgvMPS.Rows
                            r.Cells("MpsNumPers").Value = DBNull.Value
                            r.Cells("MpsSTH").Value = DBNull.Value
                            r.Cells("MpsOVTHours").Value = DBNull.Value
                            r.Cells("MpsOVTPcnt").Value = DBNull.Value
                            r.Cells("MpsContract").Value = DBNull.Value
                            r.Cells("MpsGA").Value = DBNull.Value
                            r.Cells("MpsMaintPcnt").Value = DBNull.Value
                            r.Cells("MpsAbsHrs").Value = DBNull.Value
                        Next r
                    Else
                        For Each r As DataGridViewRow In Me.dgvMPSLinks.Rows
                            r.Cells("MpsNumPersLink").Value = DBNull.Value
                            r.Cells("MpsSTHLink").Value = DBNull.Value
                            r.Cells("MpsOVTHoursLink").Value = DBNull.Value
                            r.Cells("MpsOVTPcntLink").Value = DBNull.Value
                            r.Cells("MpsContractLink").Value = DBNull.Value
                            r.Cells("MpsGALink").Value = DBNull.Value
                            r.Cells("MpsMaintPcntLink").Value = DBNull.Value
                            r.Cells("MpsAbsHrsLink").Value = DBNull.Value
                        Next r
                    End If
                Case "tabPageAbsences"
                    If rbData.Checked Then
                        For Each r As DataGridViewRow In Me.dgvABS.Rows
                            r.Cells("OCCAbs").Value = DBNull.Value
                            r.Cells("MPSAbs").Value = DBNull.Value
                        Next r
                    Else
                        For Each r As DataGridViewRow In Me.dgvABSLinks.Rows
                            r.Cells("OCCAbsLink").Value = DBNull.Value
                            r.Cells("MPSAbsLink").Value = DBNull.Value
                        Next r
                    End If
            End Select

            ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SavePersonnel()
        If Not NotFirstTime Then modeViewStudyLevel()
    End Sub

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        If IsLoaded = False Then Exit Sub
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "Personnel")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Personnel'")
        row(0)!Checked = chk.Checked
        btnSave.BackColor = Color.Red
        btnSave.ForeColor = Color.White
        PersonnelNotSaved = True
    End Sub

    Private Sub dgvOCC_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvOCC.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvOCC_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOCC.CellEndEdit
        ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
    End Sub

    Private Sub dgMPS1_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvMPS.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgMPS1_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMPS.CellEndEdit
        ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
    End Sub

    Private Sub dgvABS_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvABS.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvABS_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvABS.CellEndEdit
        ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
    End Sub

    Private Sub dgvOCCLinks_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvOCCLinks.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvOCCLinks_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOCCLinks.CellEndEdit
        ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
    End Sub

    Private Sub dgvMPSLinks_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvMPSLinks.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvMPSLinks_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMPSLinks.CellEndEdit
        ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
    End Sub

    Private Sub dgvABSLinks_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvABSLinks.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvABSLinks_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvABSLinks.CellEndEdit
        ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If PersonnelNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Personnel")
            Select Case result
                Case DialogResult.Yes : MyParent.SavePersonnel()
                Case DialogResult.No
                    MyParent.dsPersonnel.RejectChanges()
                    MyParent.dsMappings.Tables("Personnel").RejectChanges()
                    MyParent.dsMappings.Tables("Absence").RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, PersonnelNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Select Case tabControlPers.SelectedTab.Name
            Case tabPageOCC.Name : MyParent.PrintPreviews("Table 5", Nothing)
            Case tabPageMPS.Name : MyParent.PrintPreviews("Table 6", Nothing)
            Case tabPageAbsences.Name : MyParent.PrintPreviews("Table 7", Nothing)
        End Select
    End Sub

    Private Sub SA_Personnel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.btnSave.TabIndex = 0
        Me.btnClear.TabIndex = 1

        Me.rButtonStudy.TabIndex = 2
        Me.rButtonProfile.TabIndex = 3

        Me.tbFilePath.TabIndex = 4
        Me.btnBrowse.TabIndex = 5
        Me.cbSheets.TabIndex = 6

        Me.tabControlPers.TabIndex = 8
        Me.dgvOCC.TabIndex = 9

        Me.dgvMPS.TabIndex = 11
        Me.dgvABS.TabIndex = 13

        Me.btnPreview.TabIndex = 14

        Me.chkComplete.TabIndex = 15
        Me.btnClose.TabIndex = 16

    End Sub
    Private Sub StudyLevelView()
        If NotFirstTime Then
            Select Case rButtonStudy.Checked
                Case True
                    If MessageBox.Show("You have selected Study Level Data details.  To clear the Study Level Details and view only the Profile Level data, press OK.  To maintain Study Level Details, press Cancel?", "Warning", MessageBoxButtons.OKCancel) = vbCancel Then
                        RemoveHandler rButtonStudy.CheckedChanged, AddressOf rButtonStudy_CheckedChanged
                        rButtonStudy.Checked = False
                        rButtonProfile.Checked = True
                        AddHandler rButtonStudy.CheckedChanged, AddressOf rButtonStudy_CheckedChanged
                        Exit Sub
                    End If
                Case False
                    If MessageBox.Show("You have selected Profile Level Data details.  To clear the Profile Level Details and view only the Study Level data, press OK.  To maintain Profile Level Details, press Cancel?", "Warning", MessageBoxButtons.OKCancel) = vbCancel Then
                        RemoveHandler rButtonStudy.CheckedChanged, AddressOf rButtonStudy_CheckedChanged
                        rButtonStudy.Checked = True
                        rButtonProfile.Checked = False
                        AddHandler rButtonStudy.CheckedChanged, AddressOf rButtonStudy_CheckedChanged
                        Exit Sub
                    End If
            End Select
        End If
    End Sub
    Private Sub rButtonStudy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rButtonStudy.CheckedChanged
        My.Settings.viewDetailPersonnel = rButtonStudy.Checked

        My.Settings.Save()

        StudyLevelView()
        modeViewStudyLevel()



    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        selectWorkBook(Me.Cursor, tbFilePath, cbSheets, MyParent.MyParent.pnlProgress, dvWB)
    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub viewLinksData()

        If rbData.Checked Then
            Me.dgvOCC.BringToFront()
            Me.dgvMPS.BringToFront()
            Me.dgvABS.BringToFront()
            Me.lblOCCHeader.Text = "Operator, Craft && Clerical:   DATA"
            Me.lblMPSHeader.Text = "Management, Professional && Staff:   DATA"
            Me.lblABSHeader.Text = "Company Employee Absenses:   DATA"

        Else
            Me.dgvOCCLinks.BringToFront()
            Me.dgvMPSLinks.BringToFront()
            Me.dgvABSLinks.BringToFront()
            Me.lblOCCHeader.Text = "Operator, Craft && Clerical:   LINKS"
            Me.lblMPSHeader.Text = "Management, Professional && Staff:   LINKS"
            Me.lblABSHeader.Text = "Company Employee Absenses:   LINKS"

        End If

        If IsLoaded And HasRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)

        If Not NotFirstTime Then modeViewStudyLevel()
        Me.dgvOCC.Refresh()

    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If ProfileMsgBox("YN", "IMPORT", "personnel") = DialogResult.Yes Then importPersonnel()
    End Sub

    Friend Function importPersonnel() As Boolean

        Dim MyExcel As Object = OpenExcel()
        Dim MyWB As Object = OpenWorkbook(MyExcel, tbFilePath.Text)
        Dim MyWS As Object = OpenSheet(MyExcel, MyWB, cbSheets.Text)

        If MyExcel Is Nothing Then Exit Function
        If MyWB Is Nothing Then Exit Function
        If MyWS Is Nothing Then Exit Function

        Dim dr() As DataRow

        Dim dtMapping As DataTable = MyParent.dsMappings.Tables("Personnel")   ' Making import code more generic
        Dim dtTarget As DataTable = MyParent.dsPersonnel.Tables("Pers")    ' Making import code more generic

        For Each r As DataRow In dtMapping.Rows

            dr = dtTarget.Select("PersID = '" & r.Item("PersID").ToString() & "'")

            If (dr.GetUpperBound(0) > -1) Then
                For Each c As DataColumn In dtMapping.Columns
                    Select Case c.ColumnName
                        Case "NumPers", "AbsHrs", "STH", "OVTHours", "OVTPcnt", "Contract", "GA", "MaintPcnt"
                            Try

                                If r.Item(c.ColumnName).ToString() <> "" AndAlso _
                                    Not IsDBNull(r.Item(c.ColumnName).ToString()) AndAlso _
                                    IsNumeric(MyWS.Range(r.Item(c.ColumnName).ToString()).Value) Then

                                    dr(0).Item(c.ColumnName) = MyWS.Range(r.Item(c.ColumnName).ToString()).Value

                                Else
                                    If dr(0).Item(c.ColumnName).ToString() = "" Then
                                        dr(0).Item(c.ColumnName) = DBNull.Value
                                    End If
                                End If

                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Personnel for the " & Trim(r.Item("Description").ToString()) & " item")
                                GoTo CleanupExcel
                            End Try

                    End Select
                Next c
            End If

        Next r

        dtMapping = MyParent.dsMappings.Tables("Absence")   ' Making import code more generic
        dtTarget = MyParent.dsPersonnel.Tables("Absence")    ' Making import code more generic

        For Each r As DataRow In dtMapping.Rows

            dr = dtTarget.Select("CategoryID = '" & r.Item("CategoryID").ToString() & "'")

            If (dr.GetUpperBound(0) > -1) Then
                For Each c As DataColumn In dtMapping.Columns
                    Select Case c.ColumnName
                        Case "OCCAbs", "MPSAbs"
                            Try

                                If r.Item(c.ColumnName).ToString() <> "" AndAlso _
                                    Not IsDBNull(r.Item(c.ColumnName).ToString()) AndAlso _
                                    IsNumeric(MyWS.Range(r.Item(c.ColumnName).ToString()).Value) Then

                                    dr(0).Item(c.ColumnName) = MyWS.Range(r.Item(c.ColumnName).ToString()).Value

                                Else
                                    If dr(0).Item(c.ColumnName).ToString() = "" Then
                                        dr(0).Item(c.ColumnName) = DBNull.Value
                                    End If
                                End If

                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Absence for the " & Trim(r.Item("Description").ToString()) & " item")
                                GoTo CleanupExcel
                            End Try

                    End Select
                Next c
            End If

        Next r

        ChangesMade(btnSave, chkComplete, PersonnelNotSaved)
        PersonnelNotSaved = True

CleanupExcel:
        CloseExcel(MyExcel, MyWB, MyWS)
        Return PersonnelNotSaved

    End Function

    Private Sub modeViewStudyLevel()

        dgViewAggregate(dgvOCC, rButtonProfile.Checked, rbData.Checked, "PersID")
        dgViewAggregate(dgvMPS, rButtonProfile.Checked, rbData.Checked, "PersID")
        dgViewAggregate(dgvABS, rButtonProfile.Checked, rbData.Checked, "PersID")
        dgViewAggregate(dgvOCCLinks, rButtonProfile.Checked, rbData.Checked, "PersID")
        dgViewAggregate(dgvMPSLinks, rButtonProfile.Checked, rbData.Checked, "PersID")
        dgViewAggregate(dgvABSLinks, rButtonProfile.Checked, rbData.Checked, "PersID")

    End Sub

    Private Sub tabControlPers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabControlPers.SelectedIndexChanged
        If Not NotFirstTime Then modeViewStudyLevel()
    End Sub

    Private Sub dgvOCC_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOCC.CellValidated
        calculateAggregate(Me.dgvOCC, e.ColumnIndex, e.RowIndex, rButtonStudy.Checked, "PersID")
    End Sub

    Private Sub dgMPS_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMPS.CellValidated
        'calculateAggregate(Me.dgvMPS, e.ColumnIndex, e.RowIndex, rButtonStudy.Checked, "PersID", "STH")
        '2017-09-19: Richard wants the Straight Time Hours col subtotals to be Sum of the STH.
        'He wants the Overtime Percent subtotal to be (sum of (STH * Overtime Pct)) / (sum of STH)
        'STH col is 3, Overtime Pct col is 5.

        Dim colIndex As Integer = e.ColumnIndex
        If colIndex = 3 Then
            calculateAggregateWtAvg(Me.dgvMPS, 5, e.RowIndex, rButtonStudy.Checked, "PersID", "STH", "OVTPcnt") ' "mpsOVTPcnt")
            calculateAggregateWtAvg(Me.dgvMPS, 3, e.RowIndex, rButtonStudy.Checked, "PersID", "STH", "OVTPcnt") ' "mpsOVTPcnt")
        ElseIf colIndex = 5 Then
            calculateAggregateWtAvg(Me.dgvMPS, 5, e.RowIndex, rButtonStudy.Checked, "PersID", "STH", "OVTPcnt") ' "mpsOVTPcnt")
            calculateAggregateWtAvg(Me.dgvMPS, 3, e.RowIndex, rButtonStudy.Checked, "PersID", "STH", "OVTPcnt") ' "mpsOVTPcnt")
        Else
            calculateAggregateWtAvg(Me.dgvMPS, e.ColumnIndex, e.RowIndex, rButtonStudy.Checked, "PersID", "STH", "OVTPcnt") ' "mpsOVTPcnt")
        End If
    End Sub

    Private Sub dgvABS_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvABS.CellValidated
        calculateAggregate(Me.dgvABS, e.ColumnIndex, e.RowIndex, rButtonStudy.Checked, "CategoryID")
    End Sub

    Private Sub dgvOCC_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvOCC.BindingContextChanged

        modeViewStudyLevel()

        Dim cWidth As Integer = 75

        For Each c As DataGridViewColumn In dgvOCC.Columns
            Select Case c.DataPropertyName.ToString
                Case "Description" : setColumnFormat(c, "", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "NumPers" : setColumnFormat(c, "Number of Personnel", 1, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "STH" : setColumnFormat(c, "Straight Time Hours", 2, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "OVTHours" : setColumnFormat(c, "Overtime Hours", 3, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Contract" : setColumnFormat(c, "Contract Personnel Total Hours", 4, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "GA" : setColumnFormat(c, "Utilized G&A Total Hours", 5, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "AbsHrs" : setColumnFormat(c, "Absense Hours", 6, "Body", False, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
            End Select
        Next c

        For Each r As DataGridViewRow In dgvOCC.Rows
            setRowFormat(r, False, "Indent", "NumPers", "MaintPcnt")
        Next r

    End Sub

    Private Sub dgvOCCLinks_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvOCCLinks.BindingContextChanged

        modeViewStudyLevel()

        Dim cWidth As Integer = 75

        For Each c As DataGridViewColumn In dgvOCCLinks.Columns
            Select Case c.DataPropertyName.ToString
                Case "Description" : setColumnFormat(c, "", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "NumPers" : setColumnFormat(c, "Number of Personnel Cell Reference", 1, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "STH" : setColumnFormat(c, "Straight Time Hours Cell Reference", 2, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "OVTHours" : setColumnFormat(c, "Overtime Hours Cell Reference", 3, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Contract" : setColumnFormat(c, "Contract Personnel Total Hours Cell Reference", 4, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "GA" : setColumnFormat(c, "Utilized G&A Total Hours Cell Reference", 5, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "AbsHrs" : setColumnFormat(c, "Absense Hours Cell Reference", 6, "Body", False, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
            End Select
        Next c

    End Sub

    Private Sub dgvMPS_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvMPS.BindingContextChanged

        modeViewStudyLevel()

        Dim cWidth As Integer = 75

        For Each c As DataGridViewColumn In dgvMPS.Columns
            Select Case c.DataPropertyName.ToString
                Case "Description" : setColumnFormat(c, "", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "NumPers" : setColumnFormat(c, "Number of Personnel", 1, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "STH" : setColumnFormat(c, "Straight Time Hours", 2, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "OVTPcnt" : setColumnFormat(c, "Overtime Percent", 3, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Contract" : setColumnFormat(c, "Contract Personnel Total Hours", 4, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "GA" : setColumnFormat(c, "Utilized G&A Total Hours", 5, "Body", True, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "AbsHrs" : setColumnFormat(c, "Absense Hours", 6, "Body", False, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
            End Select
        Next c

        For Each r As DataGridViewRow In dgvMPS.Rows
            setRowFormat(r, False, "Indent", "NumPers", "MaintPcnt")
        Next r

    End Sub

    Private Sub dgvMPSLinks_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvMPSLinks.BindingContextChanged

        modeViewStudyLevel()

        Dim cWidth As Integer = 75

        For Each c As DataGridViewColumn In dgvMPSLinks.Columns
            Select Case c.DataPropertyName.ToString
                Case "Description" : setColumnFormat(c, "", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "NumPers" : setColumnFormat(c, "Number of Personnel Cell Reference", 1, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "STH" : setColumnFormat(c, "Straight Time Hours Cell Reference", 2, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "OVTPcnt" : setColumnFormat(c, "Overtime Percent Cell Reference", 3, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Contract" : setColumnFormat(c, "Contract Personnel Total Hours Cell Reference", 4, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "GA" : setColumnFormat(c, "Utilized G&A Total Hours Cell Reference", 5, "Body", True, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "AbsHrs" : setColumnFormat(c, "Absense Hours Cell Reference", 6, "Body", False, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
            End Select
        Next c

    End Sub

    Private Sub dgvABSLinks_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvABSLinks.BindingContextChanged
        If Not NotFirstTime Then modeViewStudyLevel()
    End Sub

    Private Sub dgvABS_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvABS.BindingContextChanged
        If Not NotFirstTime Then modeViewStudyLevel()
    End Sub

    Private Sub tbFilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Pers", tbFilePath.Name, tbFilePath.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, PersonnelNotSaved)

        My.Settings.workbookPersonnel = tbFilePath.Text
        My.Settings.Save()
    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Pers", cbSheets.Name, cbSheets.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, PersonnelNotSaved)

        My.Settings.worksheetPersonnel = cbSheets.Text
        My.Settings.Save()
    End Sub

    Private Sub dgvOCC_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvOCC.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgvMPS_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvMPS.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgvABSLinks_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvABSLinks.DataError
        dgvDataError(sender, e)
    End Sub

End Class