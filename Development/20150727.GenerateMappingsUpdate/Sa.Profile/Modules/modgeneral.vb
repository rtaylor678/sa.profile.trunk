Option Compare Binary
Option Explicit On

Imports ProfileEnvironment
Imports System.Data

Friend Module modGeneral

    ' RRH 2009 Friend ci As Object
    Private ci As System.Globalization.CultureInfo
    Friend ExcelWasNotRunning As Boolean
    Public LoggedInUser As String

    Friend Sub NoRights(ByVal hdr As Panel, ByVal gb As GroupBox)
        For Each ctl As Control In hdr.Controls
            Select Case ctl.Name
                Case "btnClose", "lblHeader"
                Case Else : ctl.Visible = False
            End Select
        Next
        If Not gb Is Nothing Then gb.Enabled = False
    End Sub

    Friend Sub ViewingLinks(ByVal hdr As Panel, ByVal gb As GroupBox, ByVal viewing As Boolean)
        For Each ctl As Control In hdr.Controls
            Select Case ctl.Name
                Case "btnImport", "btnPreview" : ctl.Enabled = viewing
            End Select
        Next

        For Each ctl As Control In gb.Controls
            Select Case ctl.Name
                Case "lblView", "rbData", "rbLinks"
                Case Else : ctl.Enabled = viewing
            End Select
        Next
    End Sub

    Friend Sub ViewingLUs(ByVal hdr As Panel, ByVal lu As Panel, ByVal data As Panel, ByVal viewing As Boolean)
        For Each ctl As Control In hdr.Controls
            Select Case ctl.Name
                Case "lblHeader"
                Case Else : ctl.Enabled = viewing
            End Select
        Next

        lu.Visible = Not viewing
        data.Visible = viewing
    End Sub

    Friend Sub ReloadXML(ByVal ds As DataSet, ByVal path As String)
        For Each dt As DataTable In ds.Tables
            dt.Rows.Clear()
        Next
        ds.ReadXml(path, XmlReadMode.ReadSchema)
    End Sub

    Friend Sub ChangesMade(ByVal btn As Button, ByVal chk As CheckBox, ByRef IsNotChanged As Boolean)
        IsNotChanged = True
        btn.ForeColor = Color.White
        btn.BackColor = Color.Red
        If Not chk Is Nothing Then chk.Checked = False
    End Sub

    Friend Sub SaveMade(ByVal btn As Button, ByRef IsNotChanged As Boolean)
        IsNotChanged = False
        btn.ForeColor = SystemColors.Window
        btn.BackColor = SystemColors.Highlight
    End Sub

    Friend Function OpenExcel() As Object
        Dim MyExcel As Object
        Dim culture As New ProfileEnvironment.Culture
        Try
            MyExcel = GetObject(, "Excel.Application")
        Catch ex As Exception
            MyExcel = CreateObject("Excel.Application")
            ExcelWasNotRunning = True
        End Try
        modGeneral.ci = System.Threading.Thread.CurrentThread.CurrentCulture

        'TODO:
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")

        

        Return MyExcel
    End Function

    Friend Function OpenWorkbook(ByVal MyExcel As Object, ByVal FileName As String) As Object
        Try
            Return MyExcel.workbooks.open(FileName, False, True, , , , , , , , False)
        Catch ex As Exception
            ProfileMsgBox("CRIT", "NOWB", FileName)
            CloseExcel(MyExcel, Nothing, Nothing)
            Return Nothing
        End Try
    End Function

    Friend Function OpenSheet(ByVal MyExcel As Object, ByVal MyWB As Object, ByVal SheetName As String) As Object
        Try
            Return MyWB.Worksheets(SheetName)
        Catch ex As Exception
            ProfileMsgBox("CRIT", "NOWS", "")
            CloseExcel(MyExcel, MyWB, Nothing)
            Return Nothing
        End Try
    End Function

    Friend Sub CloseExcel(ByRef MyExcel As Object, ByRef MyWB As Object, ByRef MyWS As Object)

        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyWS)
        Catch
        Finally
            MyWS = Nothing
        End Try
        GC.WaitForPendingFinalizers()

        Try
            MyWB.Close(False)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyWB)
        Catch
        Finally
            MyWB = Nothing
        End Try
        GC.WaitForPendingFinalizers()

        If ExcelWasNotRunning Then MyExcel.Quit()

        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyExcel)
        Catch ex As Exception
        Finally
            MyExcel = Nothing
        End Try
        GC.WaitForPendingFinalizers()

        GC.Collect()

        Try
            'rrh 20091216 System.Threading.Thread.CurrentThread.CurrentCulture = modGeneral.ci
        Finally
        End Try

        ExcelWasNotRunning = False
    End Sub

    Friend Function FormatDoubles(ByVal val As Double, ByVal DecNo As Integer) As String
        Select Case DecNo
            Case 0 : FormatDoubles = Format(val, "#,##0")
            Case 1 : FormatDoubles = Format(val, "#,##0.0")
            Case 2 : FormatDoubles = Format(val, "N")
            Case Else : FormatDoubles = Format(val, "#,##0.0")
        End Select
    End Function

    Friend Function IsNull(ByVal tmpValue As Object) As Double
        If DBNull.Value.Equals(tmpValue) Then IsNull = 0 Else IsNull = CType(tmpValue, Double)
    End Function

    Friend Function strToNull(ByVal str As String) As Object
        If str = "" Then strToNull = DBNull.Value Else strToNull = str
    End Function

    Friend Function nz(ByVal dgv As DataGridView, ByVal intColumn As Integer, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) As Single
        If ((dgv.Item(intColumn, e.RowIndex).Value Is System.DBNull.Value) _
            Or (Trim(dgv.Item(intColumn, e.RowIndex).ToString()) = "")) Then
            nz = CSng(0.0)
        Else
            nz = CSng(dgv.Item(intColumn, e.RowIndex).Value)
        End If
    End Function

   
End Module
