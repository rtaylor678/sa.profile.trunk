
Option Explicit On

Imports System.Configuration


Friend Class RefTablesQueries
    Inherits DataServiceExtension


#Region "Reference Queries"
    Friend Shadows Function GetReferences(clientKey As String) As DataSet

        If AppCertificate.IsCertificateInstall() Then

            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Me.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
            'End If
            Dim b As New Solomon.Query12.DataServices
            b.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
            Dim ds As DataSet = b.GetReferences(clientKey)
            Return ds
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function

    Friend Function GetRefineryData(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, _
                                    ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, _
                                    ByVal studyYear As Integer, ByVal includeTarget As Boolean, _
                                    ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, refineryID As String) As DataSet
        If AppCertificate.IsCertificateInstall() Then
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Me.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
            'End If
            Dim b As New Solomon.Query12.DataServices
            b.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
            Dim dset As DataSet = b.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, refineryID)
            Return dset
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function
#End Region

#Region "Test Code"
    Friend Shared Sub Main(refineryID As String)
        Dim service As New RefTablesQueries
        Dim refs As DataSet = service.GetReferences(refineryID)
        refs.WriteXml("C:\RefsDump.xml", XmlWriteMode.WriteSchema)
    End Sub

#End Region
End Class
