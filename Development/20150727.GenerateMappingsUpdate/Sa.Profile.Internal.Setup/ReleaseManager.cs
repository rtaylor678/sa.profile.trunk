﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace UpdateSetup
{
    public partial class ReleaseManager : Form
    {
        public ReleaseManager()
        {
            InitializeComponent();

            buttonOutput.Enabled = false;
            textBoxOutputDirectoryName.Enabled = false;
           
        }

        private DataSet dsSave = new DataSet();
       
        private void buttonDirSelect_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty;
            string rootDirectory = string.Empty;

            OpenFileDialog fDialog = new OpenFileDialog();

            //To set the title of window
            fDialog.Title = "Open Profile Root";

            //To apply filter, which only allows the files with the name or extension specified to be selected. in this example i m only using jpeg and GIF files
            fDialog.Filter = "All Files (*.*)|*.*";

            //To set the Initial Directory property ,means which directory to show when the open file dialog windows opens
            fDialog.InitialDirectory = @"C:\";

            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                //MessageBox.Show(fDialog.FileName.ToString());
                fileName = fDialog.FileName.ToString();

                int lengthOfString = fileName.Length;
                int dirMark = fileName.LastIndexOf(@"\");
                string baseDir = fileName.Substring(0, dirMark + 1);

                textBoxSelectedDir.Text = baseDir;
                textBoxSelectedDir.ReadOnly = true;

                DataTable dataTableDirFiles = new DataTable();
                dataTableDirFiles = CreateXmlDataTable();

                GetDirectoriesAndFiles(baseDir, ref dataTableDirFiles);

                dataGridViewFiles.DataSource = dataTableDirFiles;
                //dataGridViewFiles



            }
        }


        private DataTable CreateXmlDataTable()
        {
            DataTable dataTableFiles = new DataTable();

            DataColumn dcDirFile = new DataColumn("DirFile");
            dataTableFiles.Columns.Add(dcDirFile);
            DataColumn dcType = new DataColumn("DirFileType");
            dataTableFiles.Columns.Add(dcType);
            DataColumn dcDirFileExclude = new DataColumn("DirFileExclude");
            dataTableFiles.Columns.Add(dcDirFileExclude);

            return dataTableFiles;

        }

        private void GetDirectoriesAndFiles(string directory, ref DataTable dataTableDirFiles)
        {

            List<string> dirs = Classes.FileHelper.GetFilesRecursive(directory);

            foreach (string directoryOrFile in dirs)
            {
                //Now Add some data to the DataTable            
                DataRow dr;

                dr = dataTableDirFiles.NewRow();
                dr["DirFile"] = directoryOrFile;

                string directoryCheck = string.Empty;
                int length = directoryOrFile.Length;

                directoryCheck = directoryOrFile.Substring(length - 1, 1);

                if (directoryCheck == "\\")
                {
                    dr["DirFileType"] = "Dir";
                }
                else
                {
                    dr["DirFileType"] = "File";
                }

                dr["DirFileExclude"] = Convert.ToBoolean(false);
                dataTableDirFiles.Rows.Add(dr);
            }
        }

        private void dataGridViewFiles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string compareSub = string.Empty;
            string previousDir = string.Empty;

            if (e.ColumnIndex == 0) return;
            if (e.ColumnIndex == 1) return;

            try
            {
                DataGridViewCheckBoxCell checkBoxCellRemove = (DataGridViewCheckBoxCell)dataGridViewFiles.Rows[e.RowIndex].Cells[e.ColumnIndex];
                DataGridViewTextBoxCell textBoxCellDirFile = (DataGridViewTextBoxCell)dataGridViewFiles.Rows[e.RowIndex].Cells[0];

                if (checkBoxCellRemove != null)
                {

                    bool isChecked = (bool)checkBoxCellRemove.EditingCellFormattedValue;

                    if (isChecked == true)
                    {
                        //Now check if this a directory item
                        string directoryCheck = string.Empty;
                        string dirFile = textBoxCellDirFile.Value.ToString();

                        int length = dirFile.Length;

                        directoryCheck = dirFile.Substring(length - 1, 1);

                        //This is a directory so select all files within the directory.
                        if (directoryCheck == "\\")
                        {

                            //Now start at the current row and if there are any subordinates, set the checked value. 
                            int startIndex = e.RowIndex + 1;
                            int idx = startIndex;

                            for (int i = startIndex; i < dataGridViewFiles.Rows.Count - 1; i++)
                            {
                                //Get the remaining
                                DataGridViewTextBoxCell textBoxCellDirFileSub = (DataGridViewTextBoxCell)dataGridViewFiles.Rows[idx].Cells[0];

                                if (textBoxCellDirFileSub != null)
                                {
                                    string dirFileSub = textBoxCellDirFileSub.Value.ToString();

                                    bool directoryChange = IsDirectoryChange(dirFileSub, length);

                                    //Check for directory change
                                    if (directoryChange == false)
                                    {
                                        compareSub = dirFileSub.Substring(0, length);
                                    }
                                    else
                                    {
                                        break;
                                    }


                                    if (compareSub == dirFile)
                                    {
                                        //Means we are in the same directory
                                        DataGridViewCheckBoxCell checkBoxCellRemoveSub = (DataGridViewCheckBoxCell)dataGridViewFiles.Rows[idx].Cells[e.ColumnIndex];
                                        checkBoxCellRemoveSub.Value = true;

                                        previousDir = compareSub;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                idx++;
                            }
                        }
                    }
                    else
                    {
                        //Now check if this a directory item
                        string directoryCheck = string.Empty;
                        string dirFile = textBoxCellDirFile.Value.ToString();

                        int length = dirFile.Length;

                        directoryCheck = dirFile.Substring(length - 1, 1);

                        //This is a directory so select all files within the directory.
                        if (directoryCheck == "\\")
                        {

                            //Now start at the current row and if there are any subordinates, set the checked value. 
                            int startIndex = e.RowIndex + 1;
                            int idx = startIndex;

                            for (int i = startIndex; i < dataGridViewFiles.Rows.Count - 1; i++)
                            {
                                //Get the remaining
                                DataGridViewTextBoxCell textBoxCellDirFileSub = (DataGridViewTextBoxCell)dataGridViewFiles.Rows[idx].Cells[0];

                                if (textBoxCellDirFileSub != null)
                                {
                                    string dirFileSub = textBoxCellDirFileSub.Value.ToString();

                                    bool directoryChange = IsDirectoryChange(dirFileSub, length);

                                    //Check for directory change
                                    if (directoryChange == false)
                                    {
                                        compareSub = dirFileSub.Substring(0, length);
                                    }
                                    else
                                    {
                                        break;
                                    }


                                    if (compareSub == dirFile)
                                    {
                                        //Means we are in the same directory
                                        DataGridViewCheckBoxCell checkBoxCellRemoveSub = (DataGridViewCheckBoxCell)dataGridViewFiles.Rows[idx].Cells[e.ColumnIndex];
                                        checkBoxCellRemoveSub.Value = false;

                                        previousDir = compareSub;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                idx++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool IsDirectoryChange(string dirFileSub, int length)
        {
            string compareSub = string.Empty;

            try
            {
                compareSub = dirFileSub.Substring(0, length);
                return false;
            }
            catch
            {
                return true;
            }
        }

        private void buttonSaveUpdateSetupFiles_Click(object sender, EventArgs e)
        {
            if (this.textBoxClientXmlFile.Text != string.Empty)
            {
                buttonOutput.Enabled = true;
                textBoxOutputDirectoryName.Enabled = true;
                buttonGenerate.Visible = true;
            }
            else
            {
                //Use default Update.xml file. Get values from grid and save to xml file     
                DataTable dtSave = CreateXmlDataTable();
                string applicationPathRoot = string.Empty;
                string xmlFile = Application.StartupPath + "\\Xml\\" + "Update.xml";
                dsSave.ReadXml(xmlFile);
                int rowIdx = 0;

                foreach (DataGridViewRow row in dataGridViewFiles.Rows)
                {

                    if (row.Cells[0].Value != null)
                    {
                        if (rowIdx == 0)
                        {
                            applicationPathRoot = SetApplicationRoot(row.Cells[0].Value.ToString());
                            textBoxDestinationDirRoot.Text = SetDestinationRoot();
                        }

                        string directoryOrFile = RemoveApplicationRoot(applicationPathRoot, row.Cells[0].Value.ToString());
                        //string directoryOrFile = Convert.ToString(row.Cells[0].Value);
                        string type = Convert.ToString(row.Cells[1].Value);
                        bool exclude = Convert.ToBoolean(row.Cells[2].Value);

                        if (exclude == true)
                        {
                            //Now Add some data to the DataTable 
                            DataRow dr = dtSave.NewRow();
                            dr["DirFile"] = directoryOrFile;
                            dr["DirFileType"] = type;
                            dr["DirFileExclude"] = Convert.ToBoolean(exclude);

                            dtSave.Rows.Add(dr);
                        }
                    }

                    rowIdx++;
                }

                dsSave.Tables.Add(dtSave);
            }


            saveFileDialog1.Filter = "xml files (*.xml)|*.xml";
            saveFileDialog1.InitialDirectory = Application.StartupPath + "\\Xml\\Client\\";

            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK
              && saveFileDialog1.FileName.Length > 0)
            {

                richTextBox1.SaveFile(saveFileDialog1.FileName,
                    RichTextBoxStreamType.PlainText);

                dsSave.WriteXml(saveFileDialog1.FileName);
                textBoxClientXmlFile.Text = saveFileDialog1.FileName;

                dataGridViewExcludeFiles.DataSource = dsSave.Tables[0];
                dataGridViewExcludeFiles.Visible = true;
                dataGridViewFiles.Visible = false;

                buttonOutput.Enabled = true;
                textBoxOutputDirectoryName.Enabled = true;
                buttonSave.Visible = false;
                buttonGenerate.Visible = true;
            }
        }

        private string SetApplicationRoot(string file)
        {
           
            //Being the first record in the set we remove the file and we will have the root directory
            int lengthOfString = file.Length;
            int dirMark = file.LastIndexOf(@"\");
            string baseDir = file.Substring(0, dirMark + 1);

            return baseDir;
        }

        private string SetDestinationRoot()
        {
            string retVal = string.Empty;

            retVal = Application.StartupPath + "\\Output\\";

            return retVal;
        }


        private string RemoveApplicationRoot(string applicationPathRoot, string file)
        {
            string retVal = string.Empty;

            retVal = file.Remove(0, applicationPathRoot.Length);

            return retVal;
        }

        private void useToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void OpenRemoveXmlAndPopulateGrid()
        {
            DataTable dtSave = CreateXmlDataTable();
            string xmlFile = Application.StartupPath + "\\Xml\\" + "Update.xml";
        }

        private void buttonOpenClientXml_Click(object sender, EventArgs e)
        {

            OpenFileDialog fDialog = new OpenFileDialog();

            //To set the title of window
            fDialog.Title = "Open Client Xml File";

            //To apply filter, which only allows the files with the name or extension specified to be selected. in this example i m only using jpeg and GIF files
            fDialog.Filter = "xml files (*.xml)|*.xml";

            //To set the Initial Directory property, means which directory to show when the open file dialog windows opens
            fDialog.InitialDirectory = Application.StartupPath + "\\Xml\\Client\\";

            if (fDialog.ShowDialog() == DialogResult.OK)
            {

                string fileName = fDialog.FileName.ToString();

                textBoxClientXmlFile.Text = fileName;
                textBoxClientXmlFile.ReadOnly = true;

                DataTable dataTableClientXml = new DataTable();
                dataTableClientXml = CreateXmlDataTable();

                dataGridViewExcludeFiles.Visible = true;
                dataGridViewFiles.Visible = false;

                //GetDirectoriesAndFiles(baseDir, ref dataTableDirFiles);

                //dataGridViewFiles.DataSource = dataTableDirFiles;
                //dataGridViewFiles

                buttonOutput.Enabled = true;
                textBoxOutputDirectoryName.Enabled = true;

            }
        }

        private void buttonCommit_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Not currently working");

            //Open client xml file, locate destination directory and loop through records and delete items which are a match.
            if (textBoxSelectedDir.Text != string.Empty && textBoxClientXmlFile.Text != string.Empty && textBoxDestinationDirRoot.Text != string.Empty && textBoxOutputDirectoryName.Text != string.Empty)
            {
                //Open client xml file, locate destination directory and loop through records and delete items which are a match.
                
            }
            else
            {
                 if (textBoxSelectedDir.Text == string.Empty) {MessageBox.Show("Source directory required"); return;}
                 if (textBoxClientXmlFile.Text == string.Empty) {MessageBox.Show("Client xml required"); return;}
                 if (textBoxDestinationDirRoot.Text == string.Empty) {MessageBox.Show("Destination root directory required"); return;}
                 if (textBoxOutputDirectoryName.Text == string.Empty) { MessageBox.Show("Destination directory required"); return; }
            }
        }

        private void buttonOutput_Click(object sender, EventArgs e)
        {
           
            string destinationDirectory = textBoxDestinationDirRoot.Text + textBoxOutputDirectoryName.Text;

            try
            {

                if (textBoxOutputDirectoryName.Text != string.Empty)
                {

                    FileSystem.CopyDirectory(textBoxSelectedDir.Text, destinationDirectory);


                    toolStripStatusLabelResultMsg.Text = "Directories and files created: " + destinationDirectory;


                }
                else
                {
                    MessageBox.Show("Please enter a destination directory");
                    textBoxOutputDirectoryName.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not complete copy to destination directory." + Environment.NewLine + "Message: " + ex.Message);
            }


        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This functionality is not currently available");
        }

    }
}
