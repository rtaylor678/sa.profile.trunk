﻿namespace UpdateSetup
{
    partial class ReleaseManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.toolTipDirSelect = new System.Windows.Forms.ToolTip(this.components);
            this.buttonDirSelect = new System.Windows.Forms.Button();
            this.buttonOpenClientXml = new System.Windows.Forms.Button();
            this.buttonOutput = new System.Windows.Forms.Button();
            this.dataGridViewFiles = new System.Windows.Forms.DataGridView();
            this.colFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.buttonSave = new System.Windows.Forms.Button();
            this.dataGridViewExcludeFiles = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panelCreateNew = new System.Windows.Forms.Panel();
            this.textBoxSelectedDir = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxClientXmlFile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxOutputDirectoryName = new System.Windows.Forms.TextBox();
            this.textBoxDestinationDirRoot = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelResultMsg = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExcludeFiles)).BeginInit();
            this.panelCreateNew.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1085, 619);
            this.shapeContainer1.TabIndex = 3;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.BorderWidth = 2;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 47;
            this.lineShape1.X2 = 1018;
            this.lineShape1.Y1 = 114;
            this.lineShape1.Y2 = 115;
            // 
            // toolTipDirSelect
            // 
            this.toolTipDirSelect.IsBalloon = true;
            // 
            // buttonDirSelect
            // 
            this.buttonDirSelect.Location = new System.Drawing.Point(911, 3);
            this.buttonDirSelect.Name = "buttonDirSelect";
            this.buttonDirSelect.Size = new System.Drawing.Size(146, 21);
            this.buttonDirSelect.TabIndex = 5;
            this.buttonDirSelect.Text = "Select Project Directory";
            this.toolTipDirSelect.SetToolTip(this.buttonDirSelect, "Use Open Dialog. Select any file to set root directory.");
            this.buttonDirSelect.UseVisualStyleBackColor = true;
            this.buttonDirSelect.Click += new System.EventHandler(this.buttonDirSelect_Click);
            // 
            // buttonOpenClientXml
            // 
            this.buttonOpenClientXml.Location = new System.Drawing.Point(952, 5);
            this.buttonOpenClientXml.Name = "buttonOpenClientXml";
            this.buttonOpenClientXml.Size = new System.Drawing.Size(105, 21);
            this.buttonOpenClientXml.TabIndex = 5;
            this.buttonOpenClientXml.Text = "Select Client Xml";
            this.toolTipDirSelect.SetToolTip(this.buttonOpenClientXml, "Use Open Dialog. Select any file to set root directory.");
            this.buttonOpenClientXml.UseVisualStyleBackColor = true;
            this.buttonOpenClientXml.Click += new System.EventHandler(this.buttonOpenClientXml_Click);
            // 
            // buttonOutput
            // 
            this.buttonOutput.Location = new System.Drawing.Point(519, 33);
            this.buttonOutput.Name = "buttonOutput";
            this.buttonOutput.Size = new System.Drawing.Size(145, 21);
            this.buttonOutput.TabIndex = 5;
            this.buttonOutput.Text = "Create Output Directory";
            this.toolTipDirSelect.SetToolTip(this.buttonOutput, "Use Open Dialog. Select any file to set root directory.");
            this.buttonOutput.UseVisualStyleBackColor = true;
            this.buttonOutput.Click += new System.EventHandler(this.buttonOutput_Click);
            // 
            // dataGridViewFiles
            // 
            this.dataGridViewFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFile,
            this.Type,
            this.colCheck});
            this.dataGridViewFiles.Location = new System.Drawing.Point(7, 173);
            this.dataGridViewFiles.Name = "dataGridViewFiles";
            this.dataGridViewFiles.Size = new System.Drawing.Size(1065, 378);
            this.dataGridViewFiles.TabIndex = 4;
            this.dataGridViewFiles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFiles_CellContentClick);
            // 
            // colFile
            // 
            this.colFile.DataPropertyName = "DirFile";
            this.colFile.HeaderText = "Directories & Files";
            this.colFile.Name = "colFile";
            this.colFile.ReadOnly = true;
            this.colFile.Width = 850;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "DirFileType";
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // colCheck
            // 
            this.colCheck.DataPropertyName = "DirFileExclude";
            this.colCheck.HeaderText = "Exclude";
            this.colCheck.Name = "colCheck";
            this.colCheck.Width = 50;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(974, 567);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(99, 23);
            this.buttonSave.TabIndex = 5;
            this.buttonSave.Text = "Save Client Xml";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSaveUpdateSetupFiles_Click);
            // 
            // dataGridViewExcludeFiles
            // 
            this.dataGridViewExcludeFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewExcludeFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dataGridViewExcludeFiles.Location = new System.Drawing.Point(7, 173);
            this.dataGridViewExcludeFiles.Name = "dataGridViewExcludeFiles";
            this.dataGridViewExcludeFiles.Size = new System.Drawing.Size(1065, 307);
            this.dataGridViewExcludeFiles.TabIndex = 6;
            this.dataGridViewExcludeFiles.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DirFile";
            this.dataGridViewTextBoxColumn1.HeaderText = "Excluded Directories & Files";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 850;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DirFileType";
            this.dataGridViewTextBoxColumn2.HeaderText = "Type";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGenerate.Location = new System.Drawing.Point(883, 567);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(78, 23);
            this.buttonGenerate.TabIndex = 7;
            this.buttonGenerate.Text = "Generate";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Visible = false;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonCommit_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.Location = new System.Drawing.Point(789, 567);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(78, 23);
            this.buttonReset.TabIndex = 8;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Visible = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // panelCreateNew
            // 
            this.panelCreateNew.Controls.Add(this.buttonDirSelect);
            this.panelCreateNew.Controls.Add(this.textBoxSelectedDir);
            this.panelCreateNew.Controls.Add(this.label1);
            this.panelCreateNew.Location = new System.Drawing.Point(13, 11);
            this.panelCreateNew.Name = "panelCreateNew";
            this.panelCreateNew.Size = new System.Drawing.Size(1060, 29);
            this.panelCreateNew.TabIndex = 12;
            // 
            // textBoxSelectedDir
            // 
            this.textBoxSelectedDir.Location = new System.Drawing.Point(173, 5);
            this.textBoxSelectedDir.Name = "textBoxSelectedDir";
            this.textBoxSelectedDir.ReadOnly = true;
            this.textBoxSelectedDir.Size = new System.Drawing.Size(700, 20);
            this.textBoxSelectedDir.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Profile II Project Directory :";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonOpenClientXml);
            this.panel1.Controls.Add(this.textBoxClientXmlFile);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(12, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1060, 29);
            this.panel1.TabIndex = 13;
            // 
            // textBoxClientXmlFile
            // 
            this.textBoxClientXmlFile.Location = new System.Drawing.Point(85, 5);
            this.textBoxClientXmlFile.Name = "textBoxClientXmlFile";
            this.textBoxClientXmlFile.ReadOnly = true;
            this.textBoxClientXmlFile.Size = new System.Drawing.Size(789, 20);
            this.textBoxClientXmlFile.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Client Xml  :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.textBoxOutputDirectoryName);
            this.panel2.Controls.Add(this.buttonOutput);
            this.panel2.Controls.Add(this.textBoxDestinationDirRoot);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(12, 88);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1060, 57);
            this.panel2.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Output Project Directory :";
            // 
            // textBoxOutputDirectoryName
            // 
            this.textBoxOutputDirectoryName.Location = new System.Drawing.Point(200, 33);
            this.textBoxOutputDirectoryName.Name = "textBoxOutputDirectoryName";
            this.textBoxOutputDirectoryName.Size = new System.Drawing.Size(312, 20);
            this.textBoxOutputDirectoryName.TabIndex = 6;
            // 
            // textBoxDestinationDirRoot
            // 
            this.textBoxDestinationDirRoot.Location = new System.Drawing.Point(168, 7);
            this.textBoxDestinationDirRoot.Name = "textBoxDestinationDirRoot";
            this.textBoxDestinationDirRoot.ReadOnly = true;
            this.textBoxDestinationDirRoot.Size = new System.Drawing.Size(706, 20);
            this.textBoxDestinationDirRoot.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(188, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Output Project Directory Name :";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(8, 519);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(388, 21);
            this.richTextBox1.TabIndex = 15;
            this.richTextBox1.Text = "";
            this.richTextBox1.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelResultMsg});
            this.statusStrip1.Location = new System.Drawing.Point(0, 597);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1085, 22);
            this.statusStrip1.TabIndex = 16;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelResultMsg
            // 
            this.toolStripStatusLabelResultMsg.Name = "toolStripStatusLabelResultMsg";
            this.toolStripStatusLabelResultMsg.Size = new System.Drawing.Size(144, 17);
            this.toolStripStatusLabelResultMsg.Text = "toolStripStatusLabelResult";
            // 
            // ReleaseManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 619);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelCreateNew);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.dataGridViewExcludeFiles);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.dataGridViewFiles);
            this.Controls.Add(this.shapeContainer1);
            this.Name = "ReleaseManager";
            this.Text = "Release Manager";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExcludeFiles)).EndInit();
            this.panelCreateNew.ResumeLayout(false);
            this.panelCreateNew.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTipDirSelect;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.DataGridView dataGridViewFiles;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCheck;
        private System.Windows.Forms.DataGridView dataGridViewExcludeFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Panel panelCreateNew;
        private System.Windows.Forms.Button buttonDirSelect;
        private System.Windows.Forms.TextBox textBoxSelectedDir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonOpenClientXml;
        private System.Windows.Forms.TextBox textBoxClientXmlFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonOutput;
        private System.Windows.Forms.TextBox textBoxDestinationDirRoot;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBoxOutputDirectoryName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelResultMsg;
    }
}

