﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.Devices;
using System.Net.NetworkInformation;


namespace Sa.Profile.ConxTester
{
    public partial class Form1 : Form
    {
        string _results = string.Empty;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _results = string.Empty;
            //Beware - many schools and offices block the ping protocol. Silly, I know. – Colonel Panic Feb 28 '13 at 15:57 
            int timeout = 0;
            try
            {
                timeout = Int32.Parse(textBox1.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error converting timeout to a number: " + ex.Message);
                return;
            }

            //these don't seem to respond to pings: pingThis("www.pemex.com", timeout); pingThis("www.microsoft.com", timeout); pingThis("www.google.com", timeout);
            //pingThis("localhost", timeout);            
          pingThis("webservices.solomononline.com", timeout);  

          string protocolAndUrl = "https://webservices.solomononline.com";
          _results += Environment.NewLine + "CheckForInternetConnection for " + protocolAndUrl + " returned " + CheckForInternetConnection(protocolAndUrl).ToString();
          protocolAndUrl = "http://webservices.solomononline.com";
          _results += Environment.NewLine + "CheckForInternetConnection for " + protocolAndUrl + " returned " + CheckForInternetConnection(protocolAndUrl).ToString();

          _results+=Environment.NewLine +  TestGet("https://webservices.solomononline.com/ProfileLiteWS/GET.htm");
          _results += Environment.NewLine + TestGet("http://webservices.solomononline.com/ProfileLiteWS/GET.htm");

        //http://webservices.solomononline.com/ProfileLiteWS/ProfileLiteWebServices.asmx?op=TestWS  //will write "TEST" to: dbs8 "D:\inetpub\ProfileLiteWS\bin\ProfileLiteWebService.log"
        //another post: http://webservices.solomononline.com/ProfileLiteWS/ProfileLiteWebServices.asmx?op=CheckService shoud return  <?xml version="1.0" encoding="UTF-8"?>  <string xmlns="http://solomononline.com/ProfileLiteWS/">CheckService = Success!</string>
                        
          try
          {
              prod.ProfileLiteWebServices ws = new prod.ProfileLiteWebServices();
              ws.WriteActivityLog("PROFILE CONX TEST", "XXPAC", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", DateTime.Now.ToString(), DateTime.Now.ToString(), "PROFILE CONNECTIVITY TEST", "TEST");
              //writes to:  " select top 50 * from dbo.ActivityLog order by ActivityTime desc;"
              _results += Environment.NewLine + "HTTP POST test complete at " + DateTime.Now.ToString();
          }
          catch (Exception ex)
          {
              _results += Environment.NewLine + "Error in HTTP POST test: " + buildBestExceptionMessage(ex);
          }
          try
          {
              prod.ProfileLiteWebServices ws = new prod.ProfileLiteWebServices();
              ws.Url = "https://webservices.solomononline.com/ProfileLiteWS/ProfileLiteWebServices.asmx";
              ws.WriteActivityLog("PROFILE CONX TEST", "XXPAC", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", "PROFILE TEST", DateTime.Now.ToString(), DateTime.Now.ToString(), "PROFILE CONNECTIVITY TEST", "TEST");
              //writes to:  " select top 50 * from dbo.ActivityLog order by ActivityTime desc;"
              _results += Environment.NewLine + "HTTPS POST test complete at " + DateTime.Now.ToString();
          }
          catch (Exception ex)
          {
              _results += Environment.NewLine + "Error in HTTPS POST test: " + buildBestExceptionMessage(ex);
          }
          MessageBox.Show(_results);
        }

        private void pingThis(string url, int timeout)
        {
            try
            {
                Microsoft.VisualBasic.Devices.Computer computer = new Computer();
                if (computer.Network.Ping(url, timeout))
                {
                    _results+=Environment.NewLine +  "Server " + url + " pinged successfully.";
                }
                else
                {
                    _results+=Environment.NewLine +  "Ping request timed out for " + url;
                }
            }
            catch(Exception ex)
            {
                _results += Environment.NewLine + "Error in ping: " + buildBestExceptionMessage(ex);
            }
        }

        public bool CheckForInternetConnection(string protocolAndUrl)
        {
            try
            {
                using (var client = new System.Net.WebClient())
                {
                    using (client.OpenRead(protocolAndUrl)) //"http://clients3.google.com/generate_204"))
                    {
                        return true;
                    }
                }
            }
            catch(Exception ex)
            {
                _results += Environment.NewLine + "Error in CheckForInternetConnection: " + buildBestExceptionMessage(ex);                
                return false;
            }
        }

        public string TestGet(string protocolAndUrl)
        {
            try
            {
                using (var client = new System.Net.WebClient())
                {

                    //using (client.OpenRead(protocolAndUrl)) //"http://clients3.google.com/generate_204"))
                    //{
                        return client.DownloadString(protocolAndUrl);
                    //}
                }
            }
            catch(Exception ex)
            {
                _results += Environment.NewLine + "Error in TestGet: " + buildBestExceptionMessage(ex);
                return string.Empty;
            }
        }

        private string buildBestExceptionMessage(Exception ex)
        {
            string result = string.Empty;
            if (ex != null)
            {
                if (ex.Message != null)
                    result = ex.Message;
                if (ex.InnerException != null && ex.InnerException.Message != null)
                    result += Environment.NewLine + ex.InnerException.Message;
            }
            return result;
            
        }
    }
}

