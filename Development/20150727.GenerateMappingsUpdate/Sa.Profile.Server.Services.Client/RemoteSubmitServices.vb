Imports System.Configuration
Public Class RemoteSubmitServices
    Inherits SubmitServicesExtension
   
#Region "Data Submission Records"

    Public Sub SubmitData(ByVal ds As DataSet)
        If AppCertificate.IsCertificateInstall() Then
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Me.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            'Me.Url = "http://localhost/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            'End If
            MyBase.SubmitRefineryData(ds)
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Sub

    Function GetData(ByVal startDate As Date, ByVal endDate As Date) As DataSet
        If AppCertificate.IsCertificateInstall() Then
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Me.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            'Me.Url = "http://localhost/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            'End If
            Return MyBase.GetDataByPeriod(startDate, endDate)
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If
    End Function
    ' Added for GetInputData() testing - GMO 10/4/2007

    Function GetRefineryInputData() As DataSet
        If AppCertificate.IsCertificateInstall() Then
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Me.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            'Me.Url = "http://localhost/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            'End If
            Return MyBase.GetInputData()
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If
    End Function

#End Region

#Region "Test code"
    Public Shared Sub Main()
        Dim service As New RemoteSubmitServices
        Dim ds As New DataSet
        Dim y As Integer

        'Dim startPrd As String = "12" + _
        '                        "/1/" + _
        '                        "2004" + _
        '                        " 12:00:00 AM"

        'Dim endPrd As String = DateAdd(DateInterval.Month, 1, CDate(startPrd)).ToString
        ''Console.WriteLine("StartDate " + startPrd + " EndDate " + endPrd)

        'Console.WriteLine("StartDate " + CDate(startPrd).ToString + " EndDate " + CDate(endPrd).ToString)
        'ds.ReadXml("C:\MonthlyUpload.xml")
        'service.SubmitData(ds)
        'ds = service.GetData(#12/1/2004#, #1/1/2005#)
        'ds.WriteXml("C:\dump.xml", XmlWriteMode.WriteSchema)
        'For y = 0 To ds.Tables.Count - 1
        '    Console.WriteLine(ds.Tables(y).TableName + "," + ds.Tables(y).Rows.Count.ToString)
        'Next

        ' Added for GetInputData() testing - GMO 10/4/2007
        Dim dsInputData As New DataSet
        dsInputData = service.GetInputData()

        ' GMO 10/5/2007
        dsInputData.WriteXml("C:\InputDataDownload.xml", XmlWriteMode.WriteSchema)
        For y = 0 To dsInputData.Tables.Count - 1
            Console.WriteLine("GMO, " + dsInputData.Tables(y).TableName + "," + dsInputData.Tables(y).Rows.Count.ToString)
        Next

        Console.WriteLine("GMO, " + dsInputData.Tables(0).Columns(0).ToString())

        ' END GMO
    End Sub

#End Region

End Class
