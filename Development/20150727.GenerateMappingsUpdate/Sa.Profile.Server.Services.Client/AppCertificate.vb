Imports System.Security.Cryptography
Imports WSX509 = Microsoft.Web.Services2.Security.X509

Public Class AppCertificate

    Public Shared Function IsCertificateInstall() As Boolean
        Dim store As WSX509.X509CertificateStore = WSX509.X509CertificateStore.CurrentUserStore(WSX509.X509CertificateStore.RootStore)
        Dim fopen As Boolean = store.OpenRead
        Dim cers As WSX509.X509CertificateCollection = store.FindCertificateBySubjectString("SolomonAssociates")

        If cers.Count = 0 Then
            Return False
        End If

        Return True
    End Function

    Public Shared Sub InstallCertficate()
        If Not IsCertificateInstall() Then
            System.Diagnostics.Process.Start("_CERT/ProfileNET.cer")
        End If
    End Sub

End Class
