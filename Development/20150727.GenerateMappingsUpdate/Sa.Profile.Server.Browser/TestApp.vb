Public Class TestApp
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "
    Private brwControl As BrowserControls.ChartControl
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        brwControl = New BrowserControls.ChartControl
        Me.Controls.Add(brwControl)
        brwControl.Dock = DockStyle.Fill
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'TestApp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(648, 614)
        Me.Name = "TestApp"
        Me.Text = "TestApp"

    End Sub

#End Region

    Private Sub TestApp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim chart As New ChartControl
        chart.Dock = DockStyle.Fill
        chart.BringToFront()
        Me.Controls.Add(chart)

        'chart.Show()
        ' chart.DisplayInputReport(Nothing, "Table 2", "test", "test", "Process Data", "US", "USD", Convert.ToDateTime("7/1/2005"))
    End Sub
End Class
