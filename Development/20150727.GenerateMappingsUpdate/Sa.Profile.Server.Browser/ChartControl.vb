Imports System.Web
Imports System.Security.Cryptography
Imports SysX509 = System.Security.Cryptography.X509Certificates
Imports WSX509 = Microsoft.Web.Services2.Security.X509
Imports System.IO
Imports GenrateInTableHtml
Imports System.Configuration

Public Class ChartControl
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "
    Dim key As String

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        If Not File.Exists("_CERT\\policy.ct") Then
            Throw New Exception("Key is not was not found please register your application " + _
                                "before you start submiting data.")
        End If

        Dim reader As New StreamReader("_CERT\\policy.ct")
        Dim password As String

        If reader.Peek <> -1 Then
            key = reader.ReadLine
            Console.WriteLine(key)
            reader.Close()
        Else
            Throw New Exception("A key was not found")
        End If

        DisplayReport("161EUR", "BAPCO", "TEST", #3/1/2015#, "MET", "USD", "2012", "ACTUAL", False, False, False)
        'DisplayInputReport(Nothing, "Table 2", "test", "test", "Process Data", "US", "USD", Convert.ToDateTime("7/1/2005"))
        'DisplayReport("144NSA", "Pembroke", "Refinery Scorecard", #10/1/2007#, "MET", "USD", "2006", "ACTUAL", False, False, False)
        'DisplayRefnumChart("Energy Intensity Index", "98NSA", "", "ACTUAL", "CLIENT", #1/1/2004#, #2/1/2005#, "2002", "Gensum", "US", "USD", "EII", "EII_YTD", "EII_AVG", "EII_Target")
        'DisplayRefnumChart("Crude Gravity", "98NSA", "HOUSTON", "ACTUAL", "CLIENT", #1/1/2004#, #12/1/2004#, "2002", "Gensum", "US", "USD", "CrudeAPI", "CrudeAPI_YTD", "CrudeAPI_AVG", "CrudeAPI_Target")
        'DisplayRefnumChart("Cash Operating Expenses UEDC Basis", "98NSA", "HOUSTON", "ACTUAL", "CLIENT", #1/1/2004#, #12/1/2004#, "2004", "Gensum", "US", "USD", "NonVolOpexUEDC", "TotCashOpexUEDC_YTD", "TotCashOpexUEDC_Avg", "TotCashOpexUEDC_Target", "VolOpexUEDC")
        'DisplayRefnumChart("Cash Margin", "98NSA", "Local", "ACTUAL", "CLIENT", #1/1/2004#, #12/1/2004#, "2004", "Gensum", "US", "USD", "CashMargin", "CashMargin_YTD", "CashMargin_AVG", "CashMargin_Target")
        'DisplayRefnumChart("Annualized T/A Expenses", "98NSA", "", "ACTUAL", "BASE", #1/1/2003#, #12/1/2003#, "UnitIndicators", "US", "USD", "OnStream_Act", "OnStream_Act_YTD", "OnStream_Act_Avg", "OnStream_Act_Target", "TAIndex")

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    'Friend WithEvents AxWebBrowser1 As AxSHDocVw.AxWebBrowser
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.SuspendLayout()
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser1.Location = New System.Drawing.Point(0, 0)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(150, 150)
        Me.WebBrowser1.TabIndex = 0
        '
        'ChartControl
        '
        Me.Controls.Add(Me.WebBrowser1)
        Me.Name = "ChartControl"
        Me.ResumeLayout(False)

    End Sub

#End Region
    'Dim AxWebBrowser1 As Object

    'Friend WithEvents AxWebBrowser1 As AxSHDocVw.AxWebBrowser
    Private Sub Preload()

        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ChartControl))

        'Me.AxWebBrowser1 = New AxSHDocVw.AxWebBrowser
        'CType(Me.AxWebBrowser1, System.ComponentModel.ISupportInitialize).BeginInit()
        'Me.SuspendLayout()
        ''
        ''AxWebBrowser1
        ''
        'Me.AxWebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
        'Me.AxWebBrowser1.Enabled = True
        'Me.AxWebBrowser1.Location = New System.Drawing.Point(5, 5)
        'Me.AxWebBrowser1.OcxState = CType(resources.GetObject("AxWebBrowser1.OcxState"), System.Windows.Forms.AxHost.State)
        'Me.AxWebBrowser1.Size = New System.Drawing.Size(710, 502)
        'Me.AxWebBrowser1.TabIndex = 0
        '
        'ChartControl
        '
        Me.AutoScroll = True
        Me.Controls.Add(Me.WebBrowser1)
        Me.DockPadding.All = 5
        Me.Name = "ChartControl"
        Me.Size = New System.Drawing.Size(720, 512)
        CType(Me.WebBrowser1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
    End Sub

    Public Sub InstallCertificate()
       
        Dim store As WSX509.X509CertificateStore = WSX509.X509CertificateStore.CurrentUserStore(WSX509.X509CertificateStore.RootStore)
        Dim fopen As Boolean = store.OpenRead

        Dim cers As WSX509.X509CertificateCollection = store.FindCertificateBySubjectString("SolomonAssociates")


        'Install certificate if not available
        If (cers.Count = 0) Then

      
            Dim result As DialogResult
            result = MsgBox("You are attempting to retrieve unsecure data from Solomon.  In order to secure the data, click 'Yes' and install the electronic certificate issued by Solomon.  By clicking 'No', you will still be able to view the data, but you will be prompted each time that you are attempting to view unsecure data.", MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo, "Profile II")
            If result = DialogResult.Yes Then
                System.Diagnostics.Process.Start("_CERT/ProfileNET.cer")
            End If

        End If

    End Sub


    Private Sub UserControl1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'install certificate if not install on local machine
        'InstallCertificate()
    End Sub

    Public Sub DisplayRefnumChart(ByVal refineryID As String, _
                                   ByVal location As String, _
                                   ByVal dataset As String, _
                                   ByVal senario As String, _
                                   ByVal periodStart As Date, _
                                   ByVal periodEnd As Date, _
                                   ByVal chartRow As DataRow, _
                                   ByVal UOM As String, _
                                   ByVal currency As String, _
                                   ByVal studyYear As String, _
                                   ByVal TargetField As String, _
                                   ByVal AvgField As String, _
                                   ByVal YTDField As String)

        Dim v1, v2, v3, v4, v5 As String
        Dim leg1, leg2, leg3, leg4, leg5 As String

        Dim u As Integer



        For u = 0 To chartRow.Table.Columns.Count - 1
            If IsDBNull(chartRow(u)) Then
                chartRow(u) = ""
            End If
        Next



        v1 = chartRow("ValueField1")
        v2 = chartRow("ValueField2")
        v3 = chartRow("ValueField3")
        v4 = chartRow("ValueField4")
        v5 = chartRow("ValueField5")

        
        If Not IsDBNull(chartRow("Legend1")) Then
            If CStr(chartRow("Legend1")).Length > 0 Then
                v1 = "ISNULL(" + v1 + ",0) AS '" + chartRow("Legend1") + "'"
            End If
        Else
            v1 = "ISNULL(" + v1 + ",0) AS '" + v1 + "'"
        End If

        If Not IsNothing(location.Trim) Then
            If location.Trim.Length > 0 Then
                v1 = v1.Replace("Location", location)
            End If
        End If





        If Not IsDBNull(chartRow("Legend2")) Then
            If CStr(chartRow("Legend2")).Length > 0 Then
                v2 = "ISNULL(" + v2 + ",0) AS '" + chartRow("Legend2") + "'"
            End If
        Else
            v2 = "ISNULL(" + v2 + ",0) AS '" + v2 + "'"
        End If



        If Not IsDBNull(chartRow("Legend3")) Then
            If CStr(chartRow("Legend3")).Length > 0 Then
                v3 = "ISNULL(" + v3 + ",0) AS '" + chartRow("Legend3") + "'"
            End If
        Else
            v1 = "ISNULL(" + v3 + ",0) AS '" + v3 + "'"
        End If



        If Not IsDBNull(chartRow("Legend4")) Then
            If CStr(chartRow("Legend4")).Length > 0 Then
                v4 = "ISNULL(" + v4 + ",0) AS '" + chartRow("Legend4") + "'"
            End If
        Else
            v4 = "ISNULL(" + v4 + ",0) AS '" + v4 + "'"
        End If



        If Not IsDBNull(chartRow("Legend5")) Then
            If CStr(chartRow("Legend5")).Length > 0 Then
                v5 = "ISNULL(" + v5 + ",0) AS '" + chartRow("Legend5") + "'"
            End If
        Else
            v1 = "ISNULL(" + v5 + ",0) AS '" + v5 + "'"
        End If


        DisplayRefnumChart(chartRow("ChartTitle"), _
                           refineryID, _
                           location, _
                           dataset, _
                           senario, _
                           periodStart, _
                           periodEnd, _
                           studyYear, _
                           chartRow("DataTable"), _
                           UOM, _
                           currency, _
                           v1, _
                           YTDField, _
                           AvgField, _
                           TargetField, _
                           v2, _
                           v3, _
                           v4, _
                          v5)
    End Sub

    Public Sub DisplayRefnumChart(ByVal chartName As String, _
                                  ByVal refineryID As String, _
                                  ByVal location As String, _
                                  ByVal dataset As String, _
                                  ByVal senario As String, _
                                  ByVal startDate As Date, _
                                  ByVal endDate As Date, _
                                  ByVal StudyYear As String, _
                                  ByVal table As String, _
                                  ByVal UOM As String, _
                                  ByVal currency As String, _
                                  ByVal field1 As String, _
                                  Optional ByVal YTD As String = "", _
                                  Optional ByVal twelveMonthAvg As String = "", _
                                  Optional ByVal target As String = "", _
                                  Optional ByVal field2 As String = "", _
                                  Optional ByVal field3 As String = "", _
                                  Optional ByVal field4 As String = "", _
                                  Optional ByVal field5 As String = "")

        Dim header As Object = "WsP: " & key + vbCrLf
        Dim obj As Object

        chartName = HttpUtility.UrlEncode(chartName)
        field1 = HttpUtility.UrlEncode(field1)
        field2 = HttpUtility.UrlEncode(field2)
        field3 = HttpUtility.UrlEncode(field3)
        field4 = HttpUtility.UrlEncode(field4)
        field5 = HttpUtility.UrlEncode(field5)
        'If field1.ToUpper.IndexOf("AS") <= 0 Then
        '    If Not IsNothing(location) Then
        '        If location.Length > 0 Then
        '            field1 = field1 + " AS '" + location + "' "
        '        End If
        '    End If
        'End If



        WebBrowser1.Navigate(ConfigurationManager.AppSettings("LocalhostGraphControlChartControlUrl").ToString() + _
                             "?cn=" & chartName & "&sd=" & startDate.ToString("M/d/yyyy") & _
                               "&ed=" & endDate.ToString("M/d/yyyy") & _
                               "&ds=" & dataset & _
                               "&sn=" & senario & "&table=" & table & _
                               "&UOM=" & UOM & "&currency=" & currency & "&YR=" & StudyYear & _
                               "&YTD=" & YTD & "&TwelveMonthAvg=" & twelveMonthAvg & _
                               "&target=" & target & _
                               "&field1=" & field1 & "&field2=" & field2 & _
                               "&field3=" & field3 & "&field4=" & field4 & _
                               "&field5=" & field5, obj, obj, header)

        'AxWebBrowser1.Refresh()


        'AxWebBrowser1.Navigate("https:\\webservices.solomononline.com\RefineryCharts\ChartControl.aspx?cn=" & chartName & "&sd=" & startDate.ToString("M/d/yyyy") & _
        '                       "&ed=" & endDate.ToString("M/d/yyyy") & _
        '                       "&ds=" & dataset & _
        '                       "&sn=" & senario & "&table=" & table & _
        '                       "&UOM=" & UOM & "&currency=" & currency & "&YR=" & StudyYear & _
        '                       "&YTD=" & YTD & "&TwelveMonthAvg=" & twelveMonthAvg & _
        '                       "&target=" & target & _
        '                       "&field1=" & field1 & "&field2=" & field2 & _
        '                       "&field3=" & field3 & "&field4=" & field4 & _
        '                       "&field5=" & field5, obj, obj, obj, header)
    End Sub

    Public Sub DisplayReport(ByVal refineryID As String, _
                             ByVal location As String, _
                             ByVal reportCode As String, _
                             ByVal periodStart As Date, _
                             ByVal UOM As String, _
                             ByVal currency As String, _
                             ByVal studyYear As String, _
                             ByVal dataSet As String, _
                             ByVal includeTarget As Boolean, _
                             ByVal includeAvg As Boolean, _
                             ByVal includeYTD As Boolean)

        Dim header As Object = "WsP: " & key + vbCrLf
        Dim obj As Object

        If UOM.StartsWith("US") Then
            UOM = "US"
        Else
            UOM = "MET"
        End If


        If Convert.ToBoolean(ConfigurationManager.AppSettings("testing").ToString()) Then
            'AxWebBrowser1.Navigate("https:\\webservices.solomononline.com\ReportServicesTestSite\ReportControl.aspx?rn=" & reportCode & "&sd=" & periodStart.ToShortDateString & _
            '                                     "&ref=" & refineryID & "&ds=" & dataSet & _
            '                                     "&UOM=" & UOM & "&currency=" & currency & _
            '                                     "&yr=" & studyYear & _
            '                                     "&target=" & includeTarget & "&avg=" & includeAvg & _
            '                                     "&ytd=" & includeYTD, obj, obj, obj, header)
            reportCode = "TEST"
            Dim url As String = ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToString("M/d/yyyy") & _
                                                 "&ds=" & dataSet & _
                                                 "&UOM=" & UOM & "&currency=" & currency & _
                                                 "&yr=" & studyYear & _
                                                 "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                 "&ytd=" & includeYTD

            WebBrowser1.Navigate(url, obj, obj, header)
        Else

            WebBrowser1.Navigate(ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToString("M/d/yyyy") & _
                                                 "&ds=" & dataSet & _
                                                 "&UOM=" & UOM & "&currency=" & currency & _
                                                 "&yr=" & studyYear & _
                                                 "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                 "&ytd=" & includeYTD, obj, obj, header)

        End If
    End Sub


    Public Sub DisplayInputReport(ByVal ds As DataSet, ByVal tableName As String, ByVal company As String, ByVal location As String, ByVal description As String, ByVal UOM As String, ByVal currency As String, ByVal period As Date)
        Dim pg As New Page
        Dim htmlText As String = pg.createPage(ds, tableName, _
                                                  company, _
                                                  location, _
                                                  description, _
                                                  UOM, currency, _
                                                  Month(period), Year(period), Nothing)

        WebBrowser1.Navigate("about:blank")
        Dim doc As mshtml.IHTMLDocument2 = WebBrowser1.Document
        doc.clear()
        doc.writeln(htmlText)
        doc.close()
    End Sub


    Public Sub PrintPage()
        WebBrowser1.Print()
        'AxWebBrowser1.ExecWB(SHDocVw.OLECMDID.OLECMDID_PRINT, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_PROMPTUSER)

    End Sub

    Public Sub GoHelpPage()

        WebBrowser1.Navigate(ConfigurationManager.AppSettings("SolomonUrl").ToString())

    End Sub

    Public Sub GoHome()

        WebBrowser1.Navigate(ConfigurationManager.AppSettings("SolomonUrl").ToString())

    End Sub

    Private Sub ChartControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
