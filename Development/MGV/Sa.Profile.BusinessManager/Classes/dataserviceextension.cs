﻿
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Reflection;
using System.Configuration;
using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security.Policy;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using ProfileBusinessManager.Classes;

namespace ProfileBusinessManager.Classes
{
    public class DataServiceExtension : ProfileBusinessManager.SolomonQueryWS.DataServices
    {

        public DataServiceExtension()
        {
            
            ConfigureProxy();
            Policy policy = new Policy();
            //string pathCert = System.Configuration.ConfigurationSettings.AppSettings["PathCertification"];
            
            
            //Application exe path
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Remove(0, 6);
            
            string pathCert = path + "\\_CERT\\";
            //  20081001 RRH path SecureWebServiceRequest(frmMain.AppPath & "\_CERT\policy.ct")
            SecureWebServiceRequest((pathCert + policy.GetFileName(pathCert)));
        }

        public DataServiceExtension(string keypath)
        {
            SecureWebServiceRequest(keypath);
        }

        private void ConfigureProxy()
        {

            this.PreAuthenticate = true;
            this.Credentials = CredentialCache.DefaultCredentials;
            this.Proxy = WebRequest.GetSystemWebProxy();
          
            this.Proxy.Credentials = CredentialCache.DefaultCredentials;
            IWebProxy wp = WebRequest.GetSystemWebProxy();
         
            wp.Credentials = CredentialCache.DefaultCredentials;
            string username = System.Configuration.ConfigurationSettings.AppSettings["ProxyUsername"];
            if ((!(username == null)
                        && (username.Trim().Length > 0)))
            {
                string password = System.Configuration.ConfigurationSettings.AppSettings["ProxyPassword"];
                System.Net.NetworkCredential cr = new System.Net.NetworkCredential(username, password);
                this.Proxy.Credentials = cr;
                this.Credentials = cr;
                wp.Credentials = cr;
            }
        }

        private void SecureWebServiceRequest(string keyPath)
        {
            if (!File.Exists(keyPath))
            {
                throw new Exception((Directory.GetCurrentDirectory() + ("\r\n" + ("Key was not found. If you have a valid key, restart your application. Otherwise, please register your" +
                    " application " + "before you submit or download data."))));
            }
            string key;
            UsernameToken token;
            StreamReader reader = new StreamReader(keyPath);
            string password;
            if ((reader.Peek() != -1))
            {
                key = reader.ReadLine();
                reader.Close();
                password = CalcMiniKey(key);
                token = new UsernameToken(key, password, PasswordOption.SendHashed);
                ConfigureProxy(this, token);
            }
            else
            {
                throw new Exception("A key was not found");
            }
        }

        private void ConfigureProxy(WebServicesClientProtocol proxy, UsernameToken token)
        {
            proxy.RequestSoapContext.Security.Timestamp.TtlInSeconds = -1;
            proxy.RequestSoapContext.Security.Tokens.Add(token);
            DerivedKeyToken dk = new DerivedKeyToken(token);
            proxy.RequestSoapContext.Security.Tokens.Add(dk);
            proxy.RequestSoapContext.Security.Elements.Add(new MessageSignature(dk));
            proxy.RequestSoapContext.Security.Elements.Add(new EncryptedData(dk));
        }

        // ConfigureProxy
        private string CalcMiniKey(string key)
        {
            int lastpos = (key.Length - 1);
            //DWW TODO:
          
            string firstPart = key.Substring(0, (int)(Math.Round(lastpos * .375)));
            //Original
            //string secondPart = key.Substring(((int)(Math.Round((lastpos * 0.75)))), ((int)(((lastpos - (lastpos * 0.75)) + 1))));
            string secondPart = key.Substring(((int)(Math.Round((lastpos * 0.75)))), ((int)(((lastpos - (lastpos * 0.75)) + 2))));
            return (secondPart + firstPart);
        }

        // Resolves the 'underlying connection is closed' error
        protected override System.Net.WebRequest GetWebRequest(Uri uri)
        {
            PropertyInfo requestPropertyInfo = null;
            WebRequest request = base.GetWebRequest(uri);
            if ((requestPropertyInfo == null))
            {
                requestPropertyInfo = request.GetType().GetProperty("Request");
            }
            //  Retrieve underlying web request 
            HttpWebRequest webRequest = ((HttpWebRequest)(requestPropertyInfo.GetValue(request, null)));
            webRequest.KeepAlive = false;
            return request;
        }
    }
}
