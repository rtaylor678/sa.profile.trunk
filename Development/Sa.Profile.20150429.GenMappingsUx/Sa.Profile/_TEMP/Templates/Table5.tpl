<!-- config-start -->
<!-- config-end -->
<!-- template-start -->
<style>
.Indent0{width:0px;}
.Indent1{width:20px;}
.Indent2{width:40px;}
.Indent3{width:60px;}
.Indent4{width:80px;}
</style>

<table class=small width=100% border=0>
   <tr>
      <td width=50%></td>
      <td width=8%></td>
      <td width=2%></td>
      <td width=8%></td>
      <td width=2%></td>
      <td width=8%></td>
      <td width=2%></td>
      <td width=8%></td>
      <td width=2%></td>
      <td width=8%></td>
      <td width=2%></td>
      <td width=8%></td>
      <td width=2%></td>
   </tr>
  <tr>
    <td></td>
    <td colspan=6 align=center><strong>---- Company Personnel ----</strong></td>
    <td colspan=2></td>
    <td colspan=2 align=center><strong>Utilized</strong></td>
    <td colspan=2></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2 align=center><strong>Number</strong></td>
    <td colspan=2 align=center><strong>Straight-</strong></td>
    <td colspan=2 align=center><strong></strong></td>
    <td colspan=2 align=center><strong>Contract</strong></td>
    <td colspan=2 align=center><strong>G&A</strong></td>
    <td colspan=2 align=center><strong></strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2 align=center><strong>of</strong></td>
    <td colspan=2 align=center><strong>Time</strong></td>
    <td colspan=2 align=center><strong>Overtime</strong></td>
    <td colspan=2 align=center><strong>Total</strong></td>
    <td colspan=2 align=center><strong>Staff</strong></td>
    <td colspan=2 align=center><strong></strong></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=2 align=center><strong>Personnel</strong></td>
    <td colspan=2 align=center><strong>Hours</strong></td>
    <td colspan=2 align=center><strong>Hours</strong></td>
    <td colspan=2 align=center><strong>Hours</strong></td>
    <td colspan=2 align=center><strong>Hours</strong></td>
    <td colspan=2 align=center><strong></strong></td>
  </tr>
  <tr>
    <td colspan=13 height=30 valign=bottom><strong>O,C&C Personnel</strong></td>
  </tr>
  SECTION(Pers,SortKey<500,SortKey)
  BEGIN
  <tr>
    <td><span class="Indent@Indent">&nbsp;&nbsp;</span>@Description</td>
    <td width=55 align=right>Format(NumPers,'#,##0.0')</td>
    <td width=5></td>
    <td width=55 align=right>Format(STH,'#,##0')</td>
    <td width=5></td>
    <td width=55 align=right>Format(OVTHours,'#,##0')</td>
    <td width=5></td>
    <td width=55 align=right>Format(Contract,'#,##0')</td>
    <td width=5></td>
    <td width=55 align=right>Format(GA,'#,##0')</td>
    <td width=5></td>
    <td width=55></td>
    <td width=5></td>
  </tr>
  END
</table>

<!-- template-end -->