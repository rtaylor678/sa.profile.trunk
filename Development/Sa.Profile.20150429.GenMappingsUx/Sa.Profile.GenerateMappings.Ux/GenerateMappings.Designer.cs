﻿namespace Sa.Profile.GenerateMappings.Ux
{
	partial class GenerateMappings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtPathMapping = new System.Windows.Forms.TextBox();
			this.txtPathBridge = new System.Windows.Forms.TextBox();
			this.txtPathOutput = new System.Windows.Forms.TextBox();
			this.btnPathMapping = new System.Windows.Forms.Button();
			this.btnPathBridge = new System.Windows.Forms.Button();
			this.btnPathOutput = new System.Windows.Forms.Button();
			this.btnGenMappings = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtPathMapping
			// 
			this.txtPathMapping.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtPathMapping.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPathMapping.Enabled = false;
			this.txtPathMapping.Location = new System.Drawing.Point(118, 15);
			this.txtPathMapping.Name = "txtPathMapping";
			this.txtPathMapping.ReadOnly = true;
			this.txtPathMapping.Size = new System.Drawing.Size(304, 20);
			this.txtPathMapping.TabIndex = 3;
			this.txtPathMapping.TabStop = false;
			// 
			// txtPathBridge
			// 
			this.txtPathBridge.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtPathBridge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPathBridge.Enabled = false;
			this.txtPathBridge.Location = new System.Drawing.Point(118, 44);
			this.txtPathBridge.Name = "txtPathBridge";
			this.txtPathBridge.ReadOnly = true;
			this.txtPathBridge.Size = new System.Drawing.Size(304, 20);
			this.txtPathBridge.TabIndex = 4;
			this.txtPathBridge.TabStop = false;
			// 
			// txtPathOutput
			// 
			this.txtPathOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtPathOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPathOutput.Enabled = false;
			this.txtPathOutput.Location = new System.Drawing.Point(118, 73);
			this.txtPathOutput.Name = "txtPathOutput";
			this.txtPathOutput.ReadOnly = true;
			this.txtPathOutput.Size = new System.Drawing.Size(304, 20);
			this.txtPathOutput.TabIndex = 5;
			this.txtPathOutput.TabStop = false;
			// 
			// btnPathMapping
			// 
			this.btnPathMapping.Location = new System.Drawing.Point(12, 12);
			this.btnPathMapping.Name = "btnPathMapping";
			this.btnPathMapping.Size = new System.Drawing.Size(100, 23);
			this.btnPathMapping.TabIndex = 6;
			this.btnPathMapping.Text = "Select Mapping...";
			this.btnPathMapping.UseVisualStyleBackColor = true;
			this.btnPathMapping.Click += new System.EventHandler(this.btnPathMapping_Click);
			// 
			// btnPathBridge
			// 
			this.btnPathBridge.Location = new System.Drawing.Point(12, 41);
			this.btnPathBridge.Name = "btnPathBridge";
			this.btnPathBridge.Size = new System.Drawing.Size(100, 23);
			this.btnPathBridge.TabIndex = 7;
			this.btnPathBridge.Text = "Select Bridge...";
			this.btnPathBridge.UseVisualStyleBackColor = true;
			this.btnPathBridge.Click += new System.EventHandler(this.btnPathBridge_Click);
			// 
			// btnPathOutput
			// 
			this.btnPathOutput.Location = new System.Drawing.Point(12, 70);
			this.btnPathOutput.Name = "btnPathOutput";
			this.btnPathOutput.Size = new System.Drawing.Size(100, 23);
			this.btnPathOutput.TabIndex = 8;
			this.btnPathOutput.Text = "Select Output...";
			this.btnPathOutput.UseVisualStyleBackColor = true;
			this.btnPathOutput.Click += new System.EventHandler(this.btnPathOutput_Click);
			// 
			// btnGenMappings
			// 
			this.btnGenMappings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGenMappings.Location = new System.Drawing.Point(297, 102);
			this.btnGenMappings.Name = "btnGenMappings";
			this.btnGenMappings.Size = new System.Drawing.Size(125, 23);
			this.btnGenMappings.TabIndex = 9;
			this.btnGenMappings.Text = "Generate Mappings";
			this.btnGenMappings.UseVisualStyleBackColor = true;
			this.btnGenMappings.Click += new System.EventHandler(this.btnGenMappings_Click);
			// 
			// GenerateMappings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(434, 137);
			this.Controls.Add(this.btnGenMappings);
			this.Controls.Add(this.btnPathOutput);
			this.Controls.Add(this.btnPathBridge);
			this.Controls.Add(this.btnPathMapping);
			this.Controls.Add(this.txtPathOutput);
			this.Controls.Add(this.txtPathBridge);
			this.Controls.Add(this.txtPathMapping);
			this.MinimumSize = new System.Drawing.Size(450, 175);
			this.Name = "GenerateMappings";
			this.Text = "Generate Mappings";
			this.Load += new System.EventHandler(this.GenerateMappings_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtPathMapping;
		private System.Windows.Forms.TextBox txtPathBridge;
		private System.Windows.Forms.TextBox txtPathOutput;
		private System.Windows.Forms.Button btnPathMapping;
		private System.Windows.Forms.Button btnPathBridge;
		private System.Windows.Forms.Button btnPathOutput;
		private System.Windows.Forms.Button btnGenMappings;
	}
}

