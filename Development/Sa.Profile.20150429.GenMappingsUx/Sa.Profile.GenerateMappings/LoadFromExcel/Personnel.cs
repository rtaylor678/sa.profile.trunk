﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	public partial class Extensible
	{
		internal partial class Load
		{
			internal static void Personnel(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Personnel:            {0}", DateTime.Now.ToString());

				Excel.Worksheet wks = wkb.Worksheets["Personnel"];
				Excel.Range rng = null;

				int colId = 1;
				int colDesc = 2;
				int colNum = 3;
				int colSth = 4;
				int colOvtOcc = 5;
				int colOvtMps = 6;

				int colCon = 7;
				int colSga = 8;
				int colAbs = 9;
                string PersonnelID;

				map.Personnel.BeginLoadData();

				for (int r = 11; r <= 108; r++)
				{
					rng = wks.Cells[r, colId];

					if (Common.RangeHasValue(rng))
					{
				        PersonnelID = Common.ReturnString(rng);

                        Xsd.MappingsSchema.PersonnelRow dr = map.Personnel.FindByPersID(Common.ReturnString(rng));

						if (dr != null)
						{
							rng = wks.Cells[r, colNum];
							dr.NumPers = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colSth];
							dr.STH = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colOvtOcc];
                            if (PersonnelID.StartsWith("OCC"))
                            {
                                dr.OVTHours = Common.ReturnAddress(rng);
                                dr.OVTPcnt = "";
                            }
                            else
                            {
								rng = wks.Cells[r, colOvtMps];
                                dr.OVTHours = "";
                                dr.OVTPcnt = Common.ReturnAddress(rng);
                            }
                                                         
							rng = wks.Cells[r, colCon];
							dr.Contract = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colSga];
							dr.GA = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colAbs];
							dr.AbsHrs = Common.ReturnAddress(rng);
						}
						else
						{
							Xsd.MappingsSchema.PersonnelRow nr = map.Personnel.NewPersonnelRow();
//                            PersonnelID = Common.ReturnString(rng);

							nr.PersID = Common.ReturnString(rng);

							rng = wks.Cells[r, colDesc];
							nr.Description = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colNum];
							nr.NumPers = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colSth];
							nr.STH = Common.ReturnAddress(rng);

                            if (PersonnelID.StartsWith("OCC"))
                            {
                                nr.OVTHours = Common.ReturnAddress(rng);
                                nr.OVTPcnt = "";
                            }
                            else
                            {
								rng = wks.Cells[r, colOvtMps];
                                nr.OVTHours = "";
                                nr.OVTPcnt = Common.ReturnAddress(rng);
                            }

							rng = wks.Cells[r, colCon];
							nr.Contract = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colSga];
							nr.GA = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colAbs];
							nr.AbsHrs = Common.ReturnAddress(rng);

							map.Personnel.AddPersonnelRow(nr);

							//  <SortKey></SortKey>
							//  <Indent></Indent>
							//  <ParentID></ParentID>
							//  <detailStudy></detailStudy>
							//  <detailProfile></detailProfile>
						}
					}
				}
				map.Personnel.AcceptChanges();
				map.Personnel.EndLoadData();
			}

			internal static void PersonnelAbsences(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Personnel Absences:   {0}", DateTime.Now.ToString());

				Excel.Worksheet wks = wkb.Worksheets["Personnel"];
				Excel.Range rng = null;

				int colId = 1;
				int colDesc = 2;
				int colOcc = 3;
				int colMps = 4;

				map.Absence.BeginLoadData();

				for (int r = 111; r <= 122; r++)
				{
					rng = wks.Cells[r, colId];

					if (Common.RangeHasValue(rng))
					{
						Xsd.MappingsSchema.AbsenceRow dr = map.Absence.FindByCategoryID(Common.ReturnString(rng));

						if (dr != null)
						{
							rng = wks.Cells[r, colOcc];
							dr.OCCAbs = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colMps];
							dr.MPSAbs = Common.ReturnAddress(rng);
						}
						else
						{
							Xsd.MappingsSchema.AbsenceRow nr = map.Absence.NewAbsenceRow();

							nr.CategoryID = Common.ReturnString(rng);

							rng = wks.Cells[r, colDesc];
							nr.Description = Common.ReturnString(rng);

							rng = wks.Cells[r, colOcc];
							nr.OCCAbs = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colMps];
							nr.MPSAbs = Common.ReturnAddress(rng);

							map.Absence.AddAbsenceRow(nr);

							//  <SortKey></SortKey>
							//  <Indent></Indent>
							//  <ParentID></ParentID>
							//  <detailStudy></detailStudy>
							//  <detailProfile></detailProfile>
						}
					}
				}
				map.Absence.AcceptChanges();
				map.Absence.EndLoadData();
			}
		}
	}
}