﻿using System;
using System.Collections.Generic;
using System.Text;
using WSX509 = Microsoft.Web.Services2.Security.X509;
using System.Security.Cryptography;

namespace ProfileBusinessManager.Classes
{
    public class AppCertificate
    {

        public bool IsValidCertificateInstalled()
        {
            //20120110 RRH Removing certificate validation
            //bool retVal = false;

            //WSX509.X509CertificateStore  store = Microsoft.Web.Services2.Security.X509.X509CertificateStore.CurrentUserStore(Microsoft.Web.Services2.Security.X509.X509CertificateStore.RootStore);
            //bool open = store.OpenRead();
            //Microsoft.Web.Services2.Security.X509.X509CertificateCollection certificates = store.FindCertificateBySubjectString("SolomonAssociates");
            
            //if (certificates.Count == 0)
            //{
            //    retVal = false;
            //}
            //else
            //{
            //    retVal = true;
            //}

            return true; //retVal;
        }

        public Microsoft.Web.Services2.Security.X509.X509Certificate ValidCertificate()
        {
            Microsoft.Web.Services2.Security.X509.X509Certificate certificate;

            WSX509.X509CertificateStore store = Microsoft.Web.Services2.Security.X509.X509CertificateStore.CurrentUserStore(Microsoft.Web.Services2.Security.X509.X509CertificateStore.RootStore);
            bool open = store.OpenRead();
            Microsoft.Web.Services2.Security.X509.X509CertificateCollection certificates = store.FindCertificateBySubjectString("SolomonAssociates");


            if (certificates.Count > 0)
            {
                certificate = (Microsoft.Web.Services2.Security.X509.X509Certificate)certificates[0];              
            }
            else
            {
                certificate = null;
            }

            return certificate;
        }


        public void InstallCertificate()
        {
           if (!IsValidCertificateInstalled() == true)
           {
                //System.Diagnostics.Process.Start(pathCert & "ProfileNET.cer")
           }
        }
    }
}
