﻿USE [ProfileFuels12]
GO
--Please watch the GRANTS and change as needed

CREATE SCHEMA [profile]
GO

CREATE TABLE [profile].[ReportLayout_LU](
    [Id]  INT IDENTITY   NOT NULL,
	[Description] [varchar](256) NULL,
	[Name] [varchar](256) NULL,
	[SortKey] [int] NULL,
	[Indent] [tinyint] NULL,
	[ParentId] [INT] NULL,
	[DetailStudy] [char](5) NULL,
	[DetailProfile] [char](5) NULL,
    [AxisLabelUS]            VARCHAR (50) NULL,
    [AxisLabelMetric]        VARCHAR (50) NULL, 
    [DataTable]              VARCHAR (50) NULL, 
    [ValueField1]            VARCHAR (50)   NULL,
	[TargetField]            VARCHAR (50)   NULL,
    [YTDField]               VARCHAR (50)   NULL,
    [AvgField]               VARCHAR (50)   NULL,
    [DecPlaces]              TINYINT        NULL,
    [TotField]               VARCHAR (50)   NULL,
	[Active] nvarchar(1) DEFAULT 'Y'  NOT NULL
    PRIMARY KEY CLUSTERED ([Id] ASC)
) ON [PRIMARY]
GO
GRANT ALTER, SELECT, UPDATE, DELETE, INSERT on [profile].[ReportLayout_LU] to public
GO


INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Average Data','Average Data',102,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('EDC®','EDC',105,3,NULL,NULL,NULL,NULL,NULL,'Gensum','EDC',NULL,NULL,'EDC_AVG',0,'EDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('UEDC','UEDC',110,3,NULL,NULL,NULL,NULL,NULL,'Gensum','UEDC',NULL,NULL,'UEDC_AVG',0,'UEDC',1);

INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values 
('Composite Refinery Configuration','Composite Refinery Configuration',111,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values 
('Crude Capacity, k bbl/d','Crude Capacity, k bbl/d',112,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values 
('Crude Unit Utilization, %','Crude Unit Utilization, %',113,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0);

INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Crude Gravity, Degrees API','Crude Gravity, °API',115,3,NULL,NULL,NULL,'°API','°API','Gensum','CrudeAPI','CrudeAPI_Target','CrudeAPI_YTD','CrudeAPI_AVG',1,'CrudeAPI',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Crude Sulfur, wt %','Crude Sulfur, wt %',120,3,NULL,NULL,NULL,'wt %','wt %','Gensum','CrudeSulfur','CrudeSulfur_Target','CrudeSulfur_YTD','CrudeSulfur_AVG',2,'CrudeSulfur',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Net Raw Material Input, kb/d','Net Raw Material Input, kb/d',125,3,NULL,NULL,NULL,'kb/d','kb/d','GenSum','CrudeInputBPD','NetInputBPD_Target','NetInputBPD_YTD','NetInputBPD_AVG',0,'NetInputBPD',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Replacement Value, Million USD','Replacement Value, Million USD',130,3,NULL,NULL,NULL,'Million CurrencyCode','Million CurrencyCode','Gensum','RV',NULL,'RV_YTD','RV_AVG',1,'RV',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Performance Indicators','Performance Indicators',132,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Financial','Financial',133,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Gross Product Value, USD/bbl','Gross Product Value, USD/bbl',135,3,NULL,NULL,NULL,'CurrencyCode/bbl','CurrencyCode/bbl','GenSum','GPV','GPV_Target','GPV_YTD','GPV_Avg',2,'GPV',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Raw Material Costs, USD/bbl','Raw Material Costs, USD/bbl',140,3,NULL,NULL,NULL,'CurrencyCode/bbl','CurrencyCode/bbl','GenSum','RMC','RMC_Target','RMC_YTD','RMC_Avg',2,'RMC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Gross Margin, USD/bbl','Gross Margin, USD/bbl',145,3,NULL,NULL,NULL,'CurrencyCode/bbl','CurrencyCode/bbl','GenSum','GrossMargin','GrossMargin_Target','GrossMargin_YTD','GrossMargin_Avg',2,'GrossMargin',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Cash Operating Expenses Barrel Basis, USD/bbl','Cash Operating Expenses Barrel Basis, USD/bbl',150,3,NULL,NULL,NULL,'CurrencyCode/bbl','CurrencyCode/bbl','Gensum','TotCashOpexBbl','TotCashOpexBbl_Target','TotCashOpexBbl_YTD','TotCashOpexBbl_Avg',2,'TotCashOpexBbl',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Net Cash Margin, USD/bbl Net Input','Cash Margin, USD/bbl',155,3,NULL,NULL,NULL,'CurrencyCode/bbl','CurrencyCode/bbl','Gensum','CashMargin','CashMargin_Target','CashMargin_YTD','CashMargin_AVG',2,'CashMargin',1);

INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Return on Investment, %','Cash Basis ROI, %',160,3,NULL,NULL,NULL,'%','%','Gensum','ROI','ROI_Target','ROI_YTD','ROI_AVG',1,'ROI',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Cash Operating Expenses','Cash Operating Expenses',165,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Total Cash Expenses, USD cents/UEDC','Cash Operating Expenses UEDC Basis, 100 USD/UEDC',170,3,NULL,NULL,NULL,'100 CurrencyCode/UEDC','100 CurrencyCode/UEDC','Gensum','NonVolOpexUEDC','TotCashOpexUEDC_Target','TotCashOpexUEDC_YTD','TotCashOpexUEDC_Avg',1,'TotCashOpexUEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Non-Energy Cost Efficiency Index (NEI™)','Non-Energy Efficiency Index, %',175,3,NULL,NULL,NULL,'%','%','GenSum','NEI','NEI_Target','NEI_YTD','NEI_Avg',1,'NEI',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Volume-Related Expenses, 100 USD/UEDC','Volume-Related Expenses, 100 USD/UEDC',180,3,NULL,NULL,NULL,'100 CurrencyCode/UEDC','100 CurrencyCode/UEDC','GenSum','VolOpexUEDC_Energy','VolOpexUEDC_Target','VolOpexUEDC_YTD','VolOpexUEDC_Avg',1,'VolOpexUEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Non-Volume-Related Expenses, 100 USD/UEDC','Non-Volume-Related Expenses, 100 USD/UEDC',185,3,NULL,NULL,NULL,'100 CurrencyCode/UEDC','100 CurrencyCode/UEDC','GenSum','NonVolOpexUEDC_SWB','NonVolOpexUEDC_Target','NonVolOpexUEDC_YTD','NonVolOpexUEDC_Avg',1,'NonVolOpexUEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Non-Energy Expenses, USD/EDC','Non-Energy Operating Expenses EDC Basis, USD/EDC',190,3,NULL,NULL,NULL,'CurrencyCode/EDC','CurrencyCode/EDC','GenSum','NEOpexEDC','NEOpexEDC_Target','NEOpexEDC_YTD','NEOpexEDC_Avg',1,'NEOpexEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Non-Energy Expenses, 100 USD/UEDC','Non-Energy Operating Expenses UEDC Basis, 100 USD/UEDC',195,3,NULL,NULL,NULL,'100 CurrencyCode/UEDC','100 CurrencyCode/UEDC','GenSum','NEOpexUEDC','NEOpexUEDC_Target','NEOpexUEDC_YTD','NEOpexUEDC_Avg',1,'NEOpexUEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Energy','Energy',197,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Energy Intensity Index (EII®)','Energy Intensity Index, %',200,3,NULL,NULL,NULL,'%','%','Gensum','EII','EII_Target','EII_YTD','EII_AVG',0,'EII',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Energy Cost Index, USD cents/UEDC','',205,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Energy Cost Efficiency Index (ECEI™)','',210,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Average Purchased Energy Cost, USD/MBtu','Average Purchased Energy Cost, USD/MBtu',215,3,NULL,NULL,NULL,'CurrencyCode/MBtu','CurrencyCode/GJ','GenSum','EnergyCost_Pur','EnergyCost_Pur_Target','EnergyCost_Pur_YTD','EnergyCost_Pur_AVG',2,'EnergyCost_Pur',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Average Produced Fuel Cost, USD/MBtu','Average Produced Fuel Cost, USD/MBtu',220,3,NULL,NULL,NULL,'CurrencyCode/MBtu','CurrencyCode/GJ','GenSum','EnergyCost_Prod','EnergyCost_Prod_Target','EnergyCost_Prod_YTD','EnergyCost_Prod_AVG',2,'EnergyCost_Prod',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Average Energy Cost, USD/MBtu','Average Energy Cost, USD/MBtu',225,3,NULL,NULL,NULL,'CurrencyCode/MBtu','CurrencyCode/GJ','GenSum','EnergyCost','EnergyCost_Target','EnergyCost_YTD','EnergyCost_AVG',2,'EnergyCost',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Energy Consumption per Barrel, kBtu/bbl','Energy Consumption per Barrel, kBtu/bbl',230,3,NULL,NULL,NULL,'kBtu/bbl','MJ/bbl','GenSum','EnergyConsPerBbl_Pur','EnergyConsPerBbl_Target','EnergyConsPerBbl_YTD','EnergyConsPerBbl_AVG',0,'EnergyConsPerBbl',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Reliability and Maintenance','Reliability and Maintenance',233,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Mechanical Availability, %','Mechanical Availability, %',235,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','MechAvail_Ann','MechAvail_Ann_Target','MechAvail_Ann_YTD','MechAvail_Ann_Avg',1,'MechAvail_Ann',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Operational Availability, %','Operational Availability, %',240,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','OpAvail_Ann','OpAvail_Ann_Target','OpAvail_Ann_YTD','OpAvail_Ann_Avg',1,'OpAvail_Ann',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('On-Stream Factor, %','On-Stream Factor, %',245,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','OnStream_Ann','OnStream_Ann_Target','OnStream_Ann_YTD','OnStream_Ann_Avg',1,'OnStream_Ann',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('On-Stream Factor w/ Slowdowns, %','On-Stream Factor w/ Slowdowns, %',250,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','OnStreamSlow_Ann','OnStreamSlow_Ann_Target','OnStreamSlow_Ann_YTD','OnStreamSlow_Ann_Avg',1,'OnStreamSlow_Ann',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Maintenance Cost Efficiency Index (MEI™)','Maintenance Efficiency Index, %',255,3,NULL,NULL,NULL,'%','%','GenSum','MEI_Rout_Avg','MEI_Target','MEI_YTD','MEI_Avg',1,'MEI',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Non-Turnaround MEI Component','Non-Turnaround MEI, %',260,3,NULL,NULL,NULL,'%','%','Gensum','MEI_Rout','MEI_Rout_Target','MEI_Rout_YTD','MEI_Rout_Avg',1,'MEI_Rout',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Turnaround MEI Component','Turnaround MEI, %',265,3,NULL,NULL,NULL,'%','%','Gensum','MEI_TA','MEI_TA_Target','MEI_TA_YTD','MEI_TA_Avg',1,'MEI_TA',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Maintenance Index (MI), USD/EDC','Maintenance Index, USD/EDC',270,3,NULL,NULL,NULL,'CurrencyCode/EDC','CurrencyCode/EDC','Gensum','RoutIndex_Avg','MaintIndex_Target','MaintIndex_YTD','MaintIndex_AVG',1,'MaintIndex',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Non-Turnaround MI Component, USD/EDC','Non-Turnaround Index, USD/EDC',275,3,NULL,NULL,NULL,'CurrencyCode/EDC','CurrencyCode/EDC','Gensum','RoutIndex','RoutIndex_Target','RoutIndex_YTD','RoutIndex_Avg',1,'RoutIndex',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Turnaround MI Component, USD/EDC','Turnaround Index, USD/EDC',280,3,NULL,NULL,NULL,'CurrencyCode/EDC','CurrencyCode/EDC','Gensum','TAIndex','TAIndex_Target','TAIndex_YTD','TAIndex_Avg',1,'TAIndex',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Monthly Values','Monthly Values',285,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Mechanical Availability Monthly T/A, %','Mechanical Availability Monthly T/A, %',295,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','MechAvail_Act','MechAvail_Act_Target','MechAvail_Act_YTD','MechAvail_Act_Avg',1,'MechAvail_Act',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Operational Availability Monthly T/A, %','Operational Availability Monthly T/A, %',300,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','OpAvail_Act','OpAvail_Act_Target','OpAvail_Act_YTD','OpAvail_Act_Avg',1,'OpAvail_Act',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('On-Stream Factor Monthly T/A, %','On-Stream Factor Monthly T/A, %',305,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','OnStream_Act','OnStream_Act_Target','OnStream_Act_YTD','OnStream_Act_Avg',1,'OnStream_Act',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('On-Stream Factor w/ Slowdowns & Monthly T/A, %','On-Stream Factor w/ Slowdowns & Monthly T/A, %',310,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','OnStreamSlow_Act','OnStreamSlow_Act_Target','OnStreamSlow_Act_YTD','OnStreamSlow_Act_Avg',1,'OnStreamSlow_Act',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Mechanical Unavailability due to T/A, %','Mechanical Unavailability due to T/A, %',315,3,NULL,NULL,NULL,'%','%','MaintAvailCalc','MechUnavailTA_Act',NULL,NULL,'MechUnavailTA_Ann',2,'MechUnavailTA_Act',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Maintenance Index, Monthly NTA & Annualized TA, USD/EDC','Maintenance Index, Monthly NTA & Annualized TA, USD/EDC',320,3,NULL,NULL,NULL,'CurrencyCode/EDC','CurrencyCode/EDC','MaintIndex','RoutIndex',NULL,NULL,NULL,1,'AdjMaintIndex',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Personnel','Personnel',323,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Personnel Efficiency Index (PEI™)','Personnel Efficiency Index, %',325,3,NULL,NULL,NULL,'%','%','GenSum','PEI','PEI_Target','PEI_YTD','PEI_Avg',1,'PEI',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Non-Maintenance Personnel Efficiency Index (nmPEI™)','Non-Maintenance Personnel Efficiency Index, %',330,3,NULL,NULL,NULL,'%','%','GenSum','NonMaintPEI','NonMaintPEI_Target','NonMaintPEI_YTD','NonMaintPEI_Avg',1,'NonMaintPEI',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Maintenance Personnel Efficiency Index (mPEI™)','Maintenance Personnel Efficiency Index, %',335,3,NULL,NULL,NULL,'%','%','GenSum','MaintPEI','MaintPEI_Target','MaintPEI_YTD','MaintPEI_Avg',1,'MaintPEI',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Personnel Cost Index, USD/EDC','',340,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Personnel Index, work hours/100 EDC','Personnel Index, work hours/100 EDC',345,3,NULL,NULL,NULL,'work hours/100 EDC','work hours/100 EDC','Gensum','OCCWHrEDC','TotWHrEDC_Target','TotWHrEDC_YTD','TotWHrEDC_AVG',1,'TotWHrEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('OCC Personnel Index, work hours/100 EDC','O,C&C Personnel Index, work hours/100 EDC',350,3,NULL,NULL,NULL,'work hours/100 EDC','work hours/100 EDC','GenSum','OCCWHrEDC_Oper','OCCWHrEDC_Target','OCCWHrEDC_YTD','OCCWHrEDC_Avg',1,'OCCWHrEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('MPS Personnel Index, work hours/100 EDC','M,P&S Personnel Index, work hours/100 EDC',355,3,NULL,NULL,NULL,'work hours/100 EDC','work hours/100 EDC','GenSum','MPSWHrEDC_Oper','MPSWHrEDC_Target','MPSWHrEDC_YTD','MPSWHrEDC_Avg',1,'MPSWHrEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('O,C&C Employee Overtime, %','O,C&C Employee Overtime, %',360,3,NULL,NULL,NULL,'%','%','GenSum','OCCOvtPcnt','OCCOvtPcnt_Target','OCCOvtPcnt_YTD','OCCOvtPcnt_Avg',1,'OCCOvtPcnt',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('M,P&S Employee Overtime, %','M,P&S Employee Overtime, %',365,3,NULL,NULL,NULL,'%','%','GenSum','MPSOvtPcnt','MPSOvtPcnt_Target','MPSOvtPcnt_YTD','MPSOvtPcnt_Avg',1,'MPSOvtPcnt',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('O,C&C Total Absences, %','O,C&C Total Absences, %',370,3,NULL,NULL,NULL,'%','%','GenSum','OCCAbsPcnt_Sick','OCCAbsPcnt_Target','OCCAbsPcnt_YTD','OCCAbsPcnt_Avg',1,'OCCAbsPcnt',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('M,P&S Total Absences, %','M,P&S Total Absences, %',375,3,NULL,NULL,NULL,'%','%','GenSum','MPSAbsPcnt_Sick','MPSAbsPcnt_Target','MPSAbsPcnt_YTD','MPSAbsPcnt_Avg',1,'MPSAbsPcnt',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Process Wage Earners/Supervisors, %','Process Wage Earners/Supervisors, %',380,3,NULL,NULL,NULL,'%','%','GenSum','ProcOCCMPSRatio','ProcOCCMPSRatio_Target','ProcOCCMPSRatio_YTD','ProcOCCMPSRatio_Avg',1,'ProcOCCMPSRatio',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Maintenance Wage Earners/Supervisors, %','Maintenance Wage Earners/Supervisors, %',385,3,NULL,NULL,NULL,'%','%','GenSum','MaintOCCMPSRatio','MaintOCCMPSRatio_Target','MaintOCCMPSRatio_YTD','MaintOCCMPSRatio_Avg',1,'MaintOCCMPSRatio',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Maintenance Work Force, work hours/100 EDC','Maintenance Work Force, work hours/100 EDC',390,3,NULL,NULL,NULL,'work hours/100 EDC','work hours/100 EDC','Gensum','MaintForceCompWHrEDC','TotMaintForceWHrEDC_Target','TotMaintForceWHrEDC_YTD','TotMaintForceWHrEDC_AVG',1,'TotMaintForceWHrEDC',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Process','Process',393,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Process Unit Utilization, %','Process Unit Utilization, %',395,3,NULL,NULL,NULL,'%','%','GenSum','ProcessUtilPcnt','ProcessUtilPcnt_Target','ProcessUtilPcnt_YTD','ProcessUtilPcnt_AVG',1,'ProcessUtilPcnt',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Process Utilization Outside T/A, %','Process Unit Utilization Outside T/A, %',400,3,NULL,NULL,NULL,'%','%','Gensum','UtilOSTA','UtilOSTA_Target','UtilOSTA_YTD','UtilOSTA_AVG',1,'UtilOSTA',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Refinery Utilization, %','Refinery Utilization, %',405,3,NULL,NULL,NULL,'%','%','GenSum','UtilPcnt','UtilPcnt_Target','UtilPcnt_YTD','UtilPcnt_AVG',1,'UtilPcnt',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Volumetric Gain, %','Volumetric Gain, %',410,3,NULL,NULL,NULL,'%','%','GenSum','GainPcnt','GainPcnt_Target','GainPcnt_YTD','GainPcnt_Avg',1,'GainPcnt',1);
INSERT INTO [profile].[ReportLayout_LU] ([Description],[Name],[SortKey],[Indent],[ParentId],[DetailStudy],[DetailProfile],[AxisLabelUS],[AxisLabelMetric],[DataTable],[ValueField1],[TargetField],[YTDField],[AvgField],[DecPlaces],[TotField],[Active]) values ('Volumetric Expansion Index (VEI)','Volumetric Expansion Index, %',415,3,NULL,NULL,NULL,'%','%','Gensum','VEI','VEI_Target','VEI_YTD','VEI_AVG',1,'VEI',1);


update [profile].[ReportLayout_LU]  set ParentId =
 (select id from [profile].[ReportLayout_LU] where SortKey=102 )  --ID 70
 where id in
(select id from  [profile].[ReportLayout_LU] where SortKey > 102 and SortKey < 131 );

update [profile].[ReportLayout_LU]  
set ParentId =  (select id from [profile].[ReportLayout_LU] where SortKey=132 )  --ID  77 
where SortKey in(133,165,197,233,353);

update [profile].[ReportLayout_LU]  set ParentId = (select id from [profile].[ReportLayout_LU] where SortKey=233 )  --ID 100 
where sortkey = 285;

update [profile].[ReportLayout_LU]  set ParentId =  (select id from [profile].[ReportLayout_LU] where SortKey=133 )  --ID 78 
where id in
(select id from  [profile].[ReportLayout_LU] where parentID IS NULL
and SortKey >=135 and SortKey < 165);


update [profile].[ReportLayout_LU]  set ParentId =  (select id from [profile].[ReportLayout_LU] where SortKey=165 )  --ID 85 
where id in
(select id from  [profile].[ReportLayout_LU] where
 SortKey > 165 and SortKey < 197);
 
update [profile].[ReportLayout_LU]  set ParentId = (select id from [profile].[ReportLayout_LU] where SortKey=197 )  --ID 92 
where id in
(select id from  [profile].[ReportLayout_LU] where parentID IS NULL
and SortKey >=200 and SortKey < 233);

update [profile].[ReportLayout_LU]  set ParentId =(select id from [profile].[ReportLayout_LU] where SortKey=233 )  --ID  100 
where id in
(select id from  [profile].[ReportLayout_LU] where 
SortKey >=235 and SortKey < 285);


update [profile].[ReportLayout_LU]  set ParentId = (select id from [profile].[ReportLayout_LU] where SortKey=285 )  --ID 111 
where id in
(select id from  [profile].[ReportLayout_LU] where 
SortKey >285 and SortKey < 323);

update [profile].[ReportLayout_LU]  set ParentId = (select id from [profile].[ReportLayout_LU] where SortKey=323 )  --ID 118 
where id in
(select id from  [profile].[ReportLayout_LU] where 
SortKey >323 and SortKey < 393);

update [profile].[ReportLayout_LU]  set ParentId =(select id from [profile].[ReportLayout_LU] where SortKey=393 )  --ID  133 
where id in
(select id from  [profile].[ReportLayout_LU] where 
SortKey >393);


update [profile].[ReportLayout_LU]  set Indent = 3 where Id in
(select id from [profile].[ReportLayout_LU] where Description in
('Financial','Cash Operating Expenses','Energy','Reliability and Maintenance','Personnel') );

update [profile].[ReportLayout_LU]  set Indent = 6 where Id in
(select id from [profile].[ReportLayout_LU] where Description in
('Monthly Values') );

update [profile].[ReportLayout_LU]  set Indent = 6 where Id in (
select id from [profile].[ReportLayout_LU] where ParentId= 
(select id from [profile].[ReportLayout_LU] where Description = 'Financial')
);

update [profile].[ReportLayout_LU]  set Indent = 6 where Id in (
select id from [profile].[ReportLayout_LU] where ParentId= 
(select id from [profile].[ReportLayout_LU] where Description = 'Cash Operating Expenses')
);

update [profile].[ReportLayout_LU]  set Indent = 6 where Id in (
select id from [profile].[ReportLayout_LU] where ParentId= 
(select id from [profile].[ReportLayout_LU] where Description = 'Energy')
);

update [profile].[ReportLayout_LU]  set Indent = 6 where 
SortKey > 233 and sortkey < 285;

update [profile].[ReportLayout_LU]  set Indent = 9 where 
SortKey > 285 and sortkey < 323;

update [profile].[ReportLayout_LU]  set Indent = 6 where Id in (
select id from [profile].[ReportLayout_LU] where ParentId= 
(select id from [profile].[ReportLayout_LU] where Description = 'Personnel')
);

update [profile].[ReportLayout_LU]  set Indent = 6 where Id in (
select id from [profile].[ReportLayout_LU] where ParentId= 
(select id from [profile].[ReportLayout_LU] where Description = 'Personnel')
);


ALTER  TABLE [profile].[ReportLayout_LU] ADD [IsHeader] [int] DEFAULT 0;
GO

UPDATE [profile].[ReportLayout_LU] set [IsHeader] = 0
GO

UPDATE [profile].[ReportLayout_LU] set [IsHeader] = 1 where SortKey in 
(102,132,133,165,197,233,323,393)
GO

UPDATE [profile].[ReportLayout_LU] Set [IsHeader] = 1 where SortKey = 285
GO


ALTER TABLE profile.ReportLayout_LU ADD VKey INTEGER NULL
GO

UPDATE profile.ReportLayout_LU SET VKey =  489 where SortKey = 340;
UPDATE profile.ReportLayout_LU SET VKey =  462 where SortKey = 155;
UPDATE profile.ReportLayout_LU SET VKey =  471 where SortKey = 200;
UPDATE profile.ReportLayout_LU SET VKey =  1630 where SortKey = 390;
UPDATE profile.ReportLayout_LU SET VKey =  476 where SortKey = 335;
UPDATE profile.ReportLayout_LU SET VKey =  563 where SortKey = 295;
UPDATE profile.ReportLayout_LU SET VKey =  583 where SortKey = 260;
UPDATE profile.ReportLayout_LU SET VKey =  587 where SortKey = 265;
UPDATE profile.ReportLayout_LU SET VKey =  480 where SortKey = 175;
UPDATE profile.ReportLayout_LU SET VKey =  478 where SortKey = 190;
UPDATE profile.ReportLayout_LU SET VKey =  481 where SortKey = 195;
UPDATE profile.ReportLayout_LU SET VKey =  504 where SortKey = 170;
UPDATE profile.ReportLayout_LU SET VKey =  507 where SortKey = 345;
UPDATE profile.ReportLayout_LU SET VKey =  570 where SortKey = 305;
UPDATE profile.ReportLayout_LU SET VKey =  571 where SortKey = 310;
UPDATE profile.ReportLayout_LU SET VKey =  572 where SortKey = 300;
UPDATE profile.ReportLayout_LU SET VKey =  491 where SortKey = 325;
UPDATE profile.ReportLayout_LU SET VKey =  474 where SortKey = 320;
UPDATE profile.ReportLayout_LU SET VKey =  585 where SortKey = 275;
UPDATE profile.ReportLayout_LU SET VKey =  588 where SortKey = 280;
UPDATE profile.ReportLayout_LU SET VKey =  2071 where SortKey = 150;
UPDATE profile.ReportLayout_LU SET VKey =  510 where SortKey = 415;


UPDATE profile.ReportLayout_LU SET VKey =  468 where SortKey = 115;
UPDATE profile.ReportLayout_LU SET VKey =  469 where SortKey = 120;
UPDATE profile.ReportLayout_LU SET VKey =  482 where SortKey = 125;
UPDATE profile.ReportLayout_LU SET VKey =  494 where SortKey = 395;
UPDATE profile.ReportLayout_LU SET VKey =  509 where SortKey = 400;
UPDATE profile.ReportLayout_LU SET VKey =  2062 where SortKey = 130;
UPDATE profile.ReportLayout_LU SET VKey =  122 where SortKey = 225;
UPDATE profile.ReportLayout_LU SET VKey =  123 where SortKey = 220;
UPDATE profile.ReportLayout_LU SET VKey =  127 where SortKey = 215;
UPDATE profile.ReportLayout_LU SET VKey =  150 where SortKey = 230;
UPDATE profile.ReportLayout_LU SET VKey =  2086 where SortKey = 160;
UPDATE profile.ReportLayout_LU SET VKey =  1819 where SortKey = 330;
UPDATE profile.ReportLayout_LU SET VKey =  2077 where SortKey = 145;
UPDATE profile.ReportLayout_LU SET VKey =  2074 where SortKey = 135;
UPDATE profile.ReportLayout_LU SET VKey =  2083 where SortKey = 140;
UPDATE profile.ReportLayout_LU SET VKey =  2102 where SortKey = 410;

UPDATE profile.ReportLayout_LU SET VKey = 473 WHERE SortKey = 255;
UPDATE profile.ReportLayout_LU SET VKey = NULL WHERE SortKey = 320;
UPDATE profile.ReportLayout_LU SET VKey = 474 WHERE SortKey = 270;
UPDATE profile.ReportLayout_LU SET VKey = NULL WHERE SortKey = 295;
UPDATE profile.ReportLayout_LU SET VKey = 563 WHERE SortKey = 235;
UPDATE profile.ReportLayout_LU SET VKey = 568 WHERE SortKey = 315;
UPDATE profile.ReportLayout_LU SET VKey = NULL WHERE SortKey = 305;
UPDATE profile.ReportLayout_LU SET VKey = 570 WHERE SortKey = 245;
UPDATE profile.ReportLayout_LU SET VKey = NULL WHERE SortKey = 310;
UPDATE profile.ReportLayout_LU SET VKey = 571 WHERE SortKey = 250;
UPDATE profile.ReportLayout_LU SET VKey = NULL WHERE SortKey = 300;
UPDATE profile.ReportLayout_LU SET VKey = 572 WHERE SortKey = 240;
UPDATE profile.ReportLayout_LU SET VKey = 500 WHERE SortKey = 405;

