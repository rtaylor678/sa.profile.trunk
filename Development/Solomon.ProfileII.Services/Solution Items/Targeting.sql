﻿
--Run the RefineryScorecard2017-06-28.sql first
--Please watch the GRANTS and change as needed

USE [ProfileFuels12]
GO

CREATE TABLE [profile].[TargetingVariablesAvailable](
    [Id]  INT IDENTITY   NOT NULL,
	[RefNum] [nvarchar](20) NULL, --not needed now; is for future use
	[VKey] INT NOT NULL,
	[Active] nvarchar(1) DEFAULT 'Y'  NOT NULL
    PRIMARY KEY CLUSTERED ([Id] ASC)
) ON [PRIMARY]
GO
GRANT ALTER, SELECT, UPDATE, DELETE, INSERT on [profile].[TargetingVariablesAvailable] to public
GO

INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (462);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (471);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (473);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (474);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (476);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (478);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (480);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (481);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (489);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (491);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (504);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (506);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (507);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (510);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (563);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (570);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (571);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (572);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (583);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (585);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (587);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (588);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (1630);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (1820);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (2071);
INSERT INTO profile.TargetingVariablesAvailable (Vkey) VALUES (9849);
GO


ALTER TABLE dbo.Chart_LU ADD VKey INTEGER NULL
GO
UPDATE dbo.Chart_LU Set Vkey = 2071 WHERE SORTKEY = 704;
UPDATE dbo.Chart_LU Set Vkey = 462 WHERE SORTKEY = 211;
UPDATE dbo.Chart_LU Set Vkey = 504 WHERE SORTKEY = 208;
UPDATE dbo.Chart_LU Set Vkey = 480 WHERE SORTKEY = 209;
UPDATE dbo.Chart_LU Set Vkey = 478 WHERE SORTKEY = 504;
UPDATE dbo.Chart_LU Set Vkey = 481 WHERE SORTKEY = 503;
UPDATE dbo.Chart_LU Set Vkey = 471 WHERE SORTKEY = 201;
UPDATE dbo.Chart_LU Set Vkey = 583 WHERE SORTKEY = 403;
UPDATE dbo.Chart_LU Set Vkey = 587 WHERE SORTKEY = 404;
UPDATE dbo.Chart_LU Set Vkey = 585 WHERE SORTKEY = 401;
UPDATE dbo.Chart_LU Set Vkey = 588 WHERE SORTKEY = 402;
UPDATE dbo.Chart_LU Set Vkey = 563 WHERE SORTKEY = 411;
UPDATE dbo.Chart_LU Set Vkey = 572 WHERE SORTKEY = 412;
UPDATE dbo.Chart_LU Set Vkey = 570 WHERE SORTKEY = 413;
UPDATE dbo.Chart_LU Set Vkey = 571 WHERE SORTKEY = 416;
UPDATE dbo.Chart_LU Set Vkey = 474 WHERE SORTKEY = 420;
UPDATE dbo.Chart_LU Set Vkey = 491 WHERE SORTKEY = 203;
UPDATE dbo.Chart_LU Set Vkey = 476 WHERE SORTKEY = 204;
UPDATE dbo.Chart_LU Set Vkey = 507 WHERE SORTKEY = 202;
UPDATE dbo.Chart_LU Set Vkey = 1630 WHERE SORTKEY = 609;
UPDATE dbo.Chart_LU Set Vkey = 510 WHERE SORTKEY = 210;
UPDATE dbo.Chart_LU Set Vkey = 489 WHERE SORTKEY = 505;

--new
UPDATE dbo.Chart_LU Set Vkey = 473 WHERE SORTKEY =  207  ;
--new
UPDATE dbo.Chart_LU Set Vkey = 474 WHERE SORTKEY =  206  ;
--chg
UPDATE dbo.Chart_LU Set Vkey = NULL WHERE SORTKEY =  411  ;
UPDATE dbo.Chart_LU Set Vkey = 563 WHERE SORTKEY =  405  ;
--new
UPDATE dbo.Chart_LU Set Vkey = NULL WHERE SORTKEY =  417  ;
--chg
UPDATE dbo.Chart_LU Set Vkey = NULL WHERE SORTKEY =  413  ;
UPDATE dbo.Chart_LU Set Vkey = 570 WHERE SORTKEY =  407  ;
--chg
UPDATE dbo.Chart_LU Set Vkey = NULL WHERE SORTKEY =  416  ;
UPDATE dbo.Chart_LU Set Vkey = 571 WHERE SORTKEY =  410  ;
--chg
UPDATE dbo.Chart_LU Set Vkey = NULL WHERE SORTKEY =  412  ;
UPDATE dbo.Chart_LU Set Vkey = 572 WHERE SORTKEY =   406 ;
--new
UPDATE dbo.Chart_LU Set Vkey = 500 WHERE SORTKEY =  103 ;
--new
UPDATE dbo.Chart_LU Set Vkey = NULL WHERE SORTKEY =  420 ;

--all new:
UPDATE dbo.Chart_LU Set Vkey = 122 WHERE SortKey = 303;
UPDATE dbo.Chart_LU Set Vkey = 123 WHERE SortKey = 302;
UPDATE dbo.Chart_LU Set Vkey = 127 WHERE SortKey = 301;
UPDATE dbo.Chart_LU Set Vkey = 150 WHERE SortKey = 304;
UPDATE dbo.Chart_LU Set Vkey = 468 WHERE SortKey = 105;
UPDATE dbo.Chart_LU Set Vkey = 469 WHERE SortKey = 106;
UPDATE dbo.Chart_LU Set Vkey = 482 WHERE SortKey = 104;
UPDATE dbo.Chart_LU Set Vkey = 494 WHERE SortKey = 101;
UPDATE dbo.Chart_LU Set Vkey = 509 WHERE SortKey = 102;
UPDATE dbo.Chart_LU Set Vkey = 1819 WHERE SortKey = 205;
UPDATE dbo.Chart_LU Set Vkey = 2062 WHERE SortKey = 109;
UPDATE dbo.Chart_LU Set Vkey = 2074 WHERE SortKey = 702;
UPDATE dbo.Chart_LU Set Vkey = 2077 WHERE SortKey = 703;
UPDATE dbo.Chart_LU Set Vkey = 2083 WHERE SortKey = 701;
UPDATE dbo.Chart_LU Set Vkey = 2086 WHERE SortKey = 212;
UPDATE dbo.Chart_LU Set Vkey = 2102 WHERE SortKey = 705;

CREATE TABLE profile.RefnumStudies
(
	Id INTEGER IDENTITY,
	RefID nvarchar(15),
	Company nvarchar(50),
	Location nvarchar(50),
	Studies2012 nvarchar(75),
	Studies2014 nvarchar(75),
	Studies2016 nvarchar(75),
	CONSTRAINT [PK_RefnumStudies] PRIMARY KEY CLUSTERED 
	(
		RefID ASC
	)
)
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON profile.RefnumStudies to PUBLIC
GO
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('118NSA','IRVING OIL','SAINT JOHN','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('120NSA','DELEK','EL DORADO','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('133NSA','PEMEX','Salina Cruz','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('142NSA','PEMEX','Cadereyta','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('143NSA','PEMEX','Madero','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('144NSA','PEMEX','Minatitlan','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('145NSA','PEMEX','Salamanca','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('155PAC','BANGCHAK','BANGKOK','PAC,','PAC,','PAC,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('161EUR','BAPCO','MANAMA','EUR,','EUR,','EUR,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('163EUR','KNPC','MINA AL-AHMADI','EUR,','EUR,','EUR,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('166EUR','KNPC','MINA ABDULLAH','EUR,','EUR,','EUR,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('172NSA','ECOPETROL ','BARRANCABERMEJA','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('192NSA','ANCAP','LA TEJA','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('194NSA','PES','Philadelphia','NSA,','NSA,',NULL);
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('244NSA','EP PETROECUADOR','ESMERALDAS','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('313EUR','LUKOIL','PETROTEL','EUR,','EUR,','EUR,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('325EUR','NIS PETROL ','PANCHEVO','EUR,','EUR,','EUR,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('355EUR','YASREF','Yanbu',NULL,'EUR,','EUR,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('37NSA ','DELEK','TYLER','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('46NSA','NCRA','McPherson','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('58NSA ','MONROE ENERGY','TRAINER','NSA,','NSA,','NSA,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('176PAC','Sinochem','Quanzhou','','PAC,','PAC,');
INSERT INTO profile.RefnumStudies(RefID, Company, Location, Studies2012,Studies2014,Studies2016) VALUES ('XXPAC','SOLOMON','SINGAPORE REFINERY','PAC,','PAC,','PAC,');


CREATE TABLE profile.PeerGroups2014
(
	ID INTEGER IDENTITY,
	PGKey INTEGER NOT NULL,
	Study nvarchar(6) NOT NULL,
	Methodology INTEGER NOT NULL,
	RegionCode nvarchar(20),
	CONSTRAINT [PK_PeerGroups2014] PRIMARY KEY CLUSTERED 
	(
		PGKey ASC
	)
)

GRANT ALTER, SELECT, INSERT, UPDATE, DELETE   on profile.PeerGroups2014 TO PUBLIC
GO

INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15711,'EUR',2014,'EAME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15799,'EUR',2014,'WEUR');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15800,'EUR',2014,'WEUR');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15801,'EUR',2014,'WEUR');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15802,'EUR',2014,'WEUR');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15803,'EUR',2014,'WEUR');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18870,'EUR',2014,'EAME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18871,'EUR',2014,'EAME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18872,'EUR',2014,'EAME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18873,'EUR',2014,'EAME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18874,'EUR',2014,'EAME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15960,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (15961,'PAC',2014,'AP');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16022,'PAC',2014,'AP');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16023,'PAC',2014,'AP');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16024,'PAC',2014,'AP');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16025,'PAC',2014,'AP');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16026,'PAC',2014,'AP');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16042,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16043,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16044,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16045,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16046,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16047,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16048,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16049,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (16061,'PAC',2014,'APxJ');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18815,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18816,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18817,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18818,'PAC',2014,'APME');
INSERT INTO profile.PeerGroups2014 (PGKey,Study,Methodology,RegionCode) VALUES (18819,'PAC',2014,'APME');

--new
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15696,'EUR',2014,'EAME');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15697,'EUR',2014,'EAME');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15698,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15699,'EUR',2014,'EAME');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15700,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15701,'EUR',2014,'EAME');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15702,'EUR',2014,'CSE');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15703,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15704,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15705,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15706,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15707,'EUR',2014,'ME');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15712,'EUR',2014,'EAME');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15713,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15714,'EUR',2014,'EAME');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15718,'EUR',2014,'WEUR');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15719,'EUR',2014,'EAME');

INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15251,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15264,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15265,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15266,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15267,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15268,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15269,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15354,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15355,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15356,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15357,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15358,'NSA',2014,'USA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (18731,'NSA',2014,'USC');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15224,'NSA',2014,'NSA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (18774,'NSA',2014,'NSA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (18775,'NSA',2014,'NSA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (18776,'NSA',2014,'NSA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (18777,'NSA',2014,'NSA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (18778,'NSA',2014,'NSA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (18779,'NSA',2014,'NSA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15144,'NSA',2014,'LTA');
INSERT INTO [profile].[PeerGroups2014] ([PGKey] ,[Study]      ,[Methodology]      ,[RegionCode]) VALUES (15100,'NSA',2014,'CAN');


