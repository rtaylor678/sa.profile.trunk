﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Charting = System.Web.UI.DataVisualization.Charting;
using System.IO;

using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;


namespace Solomon.ProfileII.WCFService
{
    public partial class ChartPage : System.Web.UI.Page
    {
        public Charting.Chart Chart1;

        private string _refineryId = String.Empty;
        protected DataSet chrtData = new DataSet();
        protected String _totField = String.Empty;
        protected Solomon.Profile.ChartService.ChartFactory svc = null;
        protected bool _unitsChart = false;
        protected bool _showChrtData = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            'IF error "System.Web.HttpException: No http handler was found for request type 'GET'" then:
            ' put the handlers in web.config  <system.webserver><handlers> :
            '<add name="ChartImg" verb="*" path="ChartImg.axd" type="System.Web.UI.DataVisualization.Charting.ChartHttpHandler,     System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  />

            'Use this URL:
            'http://localhost:2153/ChartPage.aspx?cn=Average+Produced+Fuel+Cost&field1=ISNULL(EnergyCost_Prod%2c0)+AS+%27Singapore%27&field2=&field3=&field4=&field5=&sd=6/1/2015&ed=6/1/2016&UOM=US&currency=USD&sn=2014&chart=Column|Red|||&WsP=X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=
            'troubleshoot this error:
            */
            string clientKey = string.Empty;

            var hdrs = Request.Headers;
            foreach (var hdr in hdrs)
            {                
                string hdrText = hdr.ToString();
                if (hdrText == "WsP")
                {
                     clientKey = Request.Headers["Wsp"];
                }
            }

            if (clientKey.Length<1)
                clientKey = Session["ClientKey"].ToString(); //for when called from UnitIndicators.aspx

            if (!ValidateKey(clientKey, ref _refineryId) && !IsPostBack)
                Response.Redirect("InvalidUser.htm");

            string chartInfo = Request.QueryString["chart"];
            Charting.Chart chart = null;
            string conx = ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString();
            svc = new Solomon.Profile.ChartService.ChartFactory(conx, _refineryId);
            string chartInfoRequested = Request.QueryString["cn"]; //'name of set of data requested
            string[] chartInfoParts = chartInfo.Split('|');

            string chartType = chartInfoParts[0];
            string chartColor = chartInfoParts[1];
            string avgColor = chartInfoParts[2];
            string ytdColor = chartInfoParts[3];
            string targetColor = chartInfoParts[4];

            List<string> fields = new List<string>();
            if (Request.QueryString["field1"] != null)
            {
                fields.Add(Request.QueryString["field1"]);
                _totField = Request.QueryString["field1"];
            }

            if (Request.QueryString["field2"] != null)
                fields.Add(Request.QueryString["field2"]);
            if (Request.QueryString["field3"] != null)
                fields.Add(Request.QueryString["field3"]);
            if (Request.QueryString["field4"] != null)
                fields.Add(Request.QueryString["field4"]);
            if (Request.QueryString["field5"] != null)
                fields.Add(Request.QueryString["field5"]);

            string periodStart = Request.QueryString["sd"];
            string periodEnd = Request.QueryString["ed"];
            string currency = Request.QueryString["currency"];
            string scenario = Request.QueryString["sn"];
            string factorSet = string.Empty;  //'lookup from TSort
            string uom = Request.QueryString["UOM"];
            string tableName = Request.QueryString["tableName"];

            if (Request.QueryString["chrtData"] != null)
            {
                if (Request.QueryString["chrtData"] == "N")
                    _showChrtData = false;
            }

            if (chartType.ToUpper().Trim() != "RADAR")
            {
                if (!Request.QueryString.ToString().Contains("unitId"))
                {
                    chart = svc.GetCommonChartWithTheseFields(fields, _refineryId, periodStart, periodEnd, chartInfoRequested, currency,
                        ref factorSet, uom, scenario, chartInfoRequested, chartType, chartColor, avgColor, ytdColor, targetColor,
                        Request.QueryString["YTD"], Request.QueryString["target"], Request.QueryString["TwelveMonthAvg"], tableName.ToUpper());
                    if(chart==null)
                        Server.Transfer("NoData.htm");
                    QueryRefinery();
                }
                else
                {
                    _unitsChart = true;
                    chart = svc.GetUnitsCommonChart(fields, _refineryId, periodStart, periodEnd, chartInfoRequested, currency,
                        "factorset", uom, scenario, chartInfoRequested, chartType, chartColor, avgColor, ytdColor, targetColor,
                        Request.QueryString["unitId"]);
                    DataSet dsTot = new DataSet();
                    //'Check if it is a stack chart.If it is use ValueField1 instead of TotField 

                    if (Request.QueryString["field2"] != null && Request.QueryString["field2"].Trim().Length > 0)
                    {
                        dsTot = DbHelper.QueryDb("SELECT 'ISNULL('+ ValueField1 +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString["cn"].Trim() + "' ");
                    }
                    else
                    {
                        dsTot = dsTot = DbHelper.QueryDb("SELECT 'ISNULL('+ replace(TotField,'_Target','') +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString["cn"].Trim() + "' ");
                    }
                    _totField = dsTot.Tables[0].Rows[0]["TotField"].ToString();
                    DataSet dsLoc = dsTot = DbHelper.QueryDb("SELECT location FROM Submissions WHERE refineryID='" + _refineryId + "'");
                    string location = dsLoc.Tables[0].Rows[0]["Location"].ToString();
                    _totField = _totField.Replace("Location", location);
                    QueryUnits(Request.QueryString["unitId"]);
                }

                if (chrtData.Tables.Count <= 0 || chrtData.Tables[0].Rows.Count <= 0)
                    Server.Transfer("NoData.htm");
                try
                {
                    Panel1.Controls.Remove(Chart1);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error removing Chart1: " + ex.Message);
                }
                Panel1.Controls.Add(chart);
            }
            else
            {
                string peerGroup = Request.QueryString["PeerGroups"];
                List<int> peerGroups = new List<int>();
                peerGroups.Add(Int32.Parse(peerGroup));
                List<int> rankVariables = new List<int>();
                string[] rankVariableIds = Request.QueryString["rankVariables"].Split('|');
                for (int i = 0; i < rankVariableIds.Length; i++)
                {
                    rankVariables.Add(Int32.Parse(rankVariableIds[i]));
                }
                //''split peer group $$
                string monthColor = targetColor;// ' no targets in radarchart
                string warnings = string.Empty;
                chart = svc.GetRadarChart(_refineryId, periodStart, periodEnd, chartInfoRequested,
                        currency,uom, scenario, chartColor, avgColor, ytdColor, monthColor, peerGroups, rankVariables, ref warnings);
                lblWarnings.Text = warnings;
            }
            if (chartType.ToUpper().Trim() == "RADAR")
            {
                if (Panel1 == null)
                    Response.Write("Panel was not created");

                if (chart == null)
                    throw new Exception("Chart was not created. ");

                try
                {
                    Panel1.Controls.Remove(Chart1);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error removing Chart1: " + ex.Message);
                }

                try
                {
                    chart.Height = Int32.Parse(ConfigurationManager.AppSettings["RadarChartsHeight"].ToString());
                    chart.Width = Int32.Parse(ConfigurationManager.AppSettings["RadarChartsWidth"].ToString());
                }
                catch (Exception ex2)
                {
                    throw new Exception("Error setting chart height: " + ex2.Message);
                }
                chart.BorderlineWidth = 0;
            }
            Panel1.Controls.Add(chart);
        }


        public static bool ValidateKey(string clientKey, ref string refNum)
        {
            string company, location, path = string.Empty;
            int locIndex = 0;
            try
            {
                if (clientKey != null)
                {
                    char[] delim = { '$' };
                    company = Cryptographer.Decrypt(clientKey).Split(delim)[0]; // (0);
                    locIndex = Cryptographer.Decrypt(clientKey).Split(delim).Length - 2;
                    location = Cryptographer.Decrypt(clientKey).Split(delim)[locIndex];
                    string[] array = Cryptographer.Decrypt(clientKey).Split(delim);
                    refNum = array[array.Length - 1];                                                       
                    string clientKeysFolder = string.Empty;
                    try
                    {
                        clientKeysFolder = ConfigurationManager.AppSettings["ClientKeysFolder"].ToString();
                    }
                    catch { }

                    if (clientKeysFolder.Length > 0)
                    {
                        if (System.IO.File.Exists(clientKeysFolder + company + "_" + location + ".key"))
                        {
                            path = clientKeysFolder + company + "_" + location + ".key";
                        }
                    }
                    else
                    {
                        if (System.IO.File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key"))
                        {
                            path = "C:\\ClientKeys\\" + company + "_" + location + ".key";
                        }
                        else if (File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key"))
                        {
                            path = "D:\\ClientKeys\\" + company + "_" + location + ".key";
                        }
                        else
                        {
                            return false;
                        }
                    }

                    if (path.Trim().Length == 0)
                    {
                        return false;
                    }

                    string readKey = string.Empty;
                    using (StreamReader reader = new System.IO.StreamReader(path))
                    {
                        readKey = reader.ReadLine();
                    }
                    if (readKey.Length > 0)
                    {
                        return (readKey.Trim() == clientKey.Trim());
                    }
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        protected string GetChartData() 
        {
            if (!_showChrtData)
                return string.Empty;
            string row = string.Empty;
            string query = string.Empty;
            if (chrtData.Tables.Count > 0)
            {
                row += "<table frame=box rules=rows width=70% style='font-size: xx-small; font-family: Tahoma;' border=1  cellPadding=1 cellSpacing=0 bordercolor=#000000  id=dataTable>";
                row += "<tr bgcolor='dimgray' >";
                row += "<th><font color='white'>Month</font></th>";

                //BUILD HEADER
                for (int k = 4; k < chrtData.Tables[0].Columns.Count - 1; k++)
                {
                    row += "<th ><font color='white'>" + chrtData.Tables[0].Columns[k].ColumnName + "</font></th>";
                }
                row += "</tr>";

                //'BUILD BODY
                for (int s = 0; s < chrtData.Tables[0].Rows.Count - 1; s++)
                {
                    DataRow r = chrtData.Tables[0].Rows[s];
                    string fieldFormat = r[3].ToString();// 'Assign Decimal Format
                    row += "<tr>";
                    row += "<td style=\"border-right: solid thin;\"><div align=center>" + r[0] + "&nbsp;</div></td>";
                    //'Fields
                    for (int g = 4; g < chrtData.Tables[0].Columns.Count - 1; g++)
                    {
                        if (r[g] != null)
                        {
             
                            row += "<td><div align=center>" + String.Format(fieldFormat, r[g]) + "&nbsp;</div></td>";

                        }
                        else
                        {
                            row += "<td><div align=center> - </div></td>";
                        }
                    }
                    row += "</tr>";
                }
                row += "</table> ";


            }
            Response.Write(row);
            return string.Empty;
        }

        private void QueryRefinery()
        {
            string strRow = string.Empty;
            string query = string.Empty;
            NameValueCollection chartOptions = Request.QueryString;
            string scenario = "CLIENT";

            query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" +
                " WHEN 0 then '{0:#,##0}'" +
                " WHEN 1 then '{0:#,##0.0}'" +
                " WHEN 2 then '{0:N}' " +
                " END AS DecFormat ," + _totField;

            if (chartOptions["field2"] != null && chartOptions["field2"].Trim().Length > 0)
            {
                query += "," + chartOptions["field2"];
            }
            if (chartOptions["field3"] != null && chartOptions["field3"].Trim().Length > 0)
            {
                query += "," + chartOptions["field3"];
            }
            if (chartOptions["field4"] != null && chartOptions["field4"].Trim().Length > 0)
            {
                query += "," + chartOptions["field4"];
            }
            if (chartOptions["field5"] != null && chartOptions["field5"].Trim().Length > 0)
            {
                query += "," + chartOptions["field5"];
            }

            string specCases = string.Empty;

            if (chartOptions["YTD"] != null && chartOptions["YTD"].Trim().Length > 0)
            {
                query += ",ISNULL(" + chartOptions["YTD"] + ",0) AS 'Year-To-Date' ";
            }
            if (chartOptions["target"] != null && chartOptions["target"].Trim().Length > 0)
            {
                query += ",ISNULL(" + chartOptions["target"] + ",0) AS 'Target' ";
            }
            if (chartOptions["twelveMonthAvg"] != null && chartOptions["twelveMonthAvg"].Trim().Length > 0)
            {
                query += ",ISNULL(" + chartOptions["TwelveMonthAvg"] + ",0) AS 'Rolling Average' ";
            }
            query += " FROM Chart_LU, " + svc.dbtable + " m, Submissions s " +
             " WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + _refineryId + "' AND (s.PeriodStart BETWEEN '" + CDate(chartOptions["sd"]).ToShortDateString() +
             "' AND '" + CDate(chartOptions["ed"]).ToShortDateString() + "') AND " +
             "RTRIM(s.DataSet) = 'Actual'  AND  ChartTitle='" + chartOptions["cn"].Trim() + "' ";
            if (chartOptions["sn"] != null && chartOptions["sn"].Trim().Length > 0)
            {
                if (chartOptions["sn"] != "BASE")
                    scenario = chartOptions["sn"];
            }

            string factorSet = svc.FactorSet;
            if (svc.dbtable.ToUpper() == "GENSUM")
            {
                query += " AND Currency='" + chartOptions["currency"] + "' AND m.FactorSet = '" + factorSet + "' AND m.UOM='" + chartOptions["UOM"] + "' AND m.Scenario='" + scenario + "' ";
            }
            else if (svc.dbtable.ToUpper() == "MAINTAVAILCALC")
            {
                query += " AND (FactorSet ='" + factorSet + "' ) ";
            }
            else if (svc.dbtable.ToUpper() == "MAINTINDEX")
            {
                query += " AND Currency='" + chartOptions["currency"] + "' AND m.FactorSet = '" + factorSet + "' ";
            }

            query += "ORDER BY s.PeriodStart";

            chrtData = DbHelper.QueryDb(query);
        }

        private void QueryUnits(string unitId)
        {
            string strRow = string.Empty;
            string query = string.Empty;
            string selectedUnit = unitId;
            NameValueCollection chartOptions = Request.QueryString;
            if (unitId.Length < 1)
                return;
            query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" +
                " WHEN 0 then '{0:#,##0}'" +
                " WHEN 1 then '{0:#,##0.0}'" +
                " WHEN 2 then '{0:N}' " +
                " END AS DecFormat ," + _totField;

            if (chartOptions["field2"] != null && chartOptions["field2"].Trim().Length > 0)
                query += "," + chartOptions["field2"];
            if (chartOptions["field3"] != null && chartOptions["field3"].Trim().Length > 0)
                query += "," + chartOptions["field3"];
            if (chartOptions["field4"] != null && chartOptions["field4"].Trim().Length > 0)
                query += "," + chartOptions["field4"];
            if (chartOptions["field5"] != null && chartOptions["field5"].Trim().Length > 0)
                query += "," + chartOptions["field5"];

            if (chartOptions["YTD"] != null && chartOptions["YTD"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["YTD"] + ",0) AS 'Year-To-Date' ";
            if (chartOptions["target"] != null && chartOptions["target"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["target"] + ",0) AS 'Target' ";
            if (chartOptions["twelveMonthAvg"] != null && chartOptions["twelveMonthAvg"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["twelveMonthAvg"] + ",0) AS 'Rolling Average' ";

            string tableName = "UnitIndicators";

            query += " FROM Chart_LU, " + tableName + " mc ,Submissions s " +
                " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + _refineryId + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions["sd"]).ToShortDateString() +
                "' AND '" + CDate(chartOptions["ed"]).ToShortDateString() + "') " +
                " AND ChartTitle='" + chartOptions["cn"].Trim() + "' AND mc.UnitID='" + selectedUnit + "' " +
                " AND mc.Currency='" + chartOptions["currency"].Trim() + "' " +
                " ORDER BY s.PeriodStart";
            System.Text.StringBuilder queryUnitName = new System.Text.StringBuilder();
            queryUnitName.Append("SELECT DISTINCT RTRIM(c.ProcessID) +': '+ RTRIM(c.UnitName) AS UnitName ");
            queryUnitName.Append(" FROM Config c, ProcessID_LU p ");
            queryUnitName.Append(" WHERE p.ProcessID=c.ProcessID AND p.MaintDetails='Y' AND c.SubmissionID IN ");
            queryUnitName.Append(" (SELECT SubmissionID FROM Submissions WHERE RefineryID='" + _refineryId + "' AND PeriodStart BETWEEN '" + CDate(chartOptions["sd"]).ToShortDateString() +
                "' AND '" + CDate(chartOptions["ed"]).ToShortDateString() + "') ");
            queryUnitName.Append(" and c.UnitID = '" + unitId + "'");
            string unitName = DbHelper.QueryDb(queryUnitName.ToString()).Tables[0].Rows[0][0].ToString();
            query = query.Replace("UnitName", unitName.Trim());
            chrtData = DbHelper.QueryDb(query);
        }

        DateTime CDate(string whatToConvert)
        {
            return DateTime.Parse(whatToConvert);
        }


    }
}