<!-- config-start -->

<!-- config-end -->
<!-- template-start -->


<table class=small border=0>
  <tr>
    <td width=270></td>
    <td width=60></td>
    <td width=60></td>
    <td width=60></td>
    <td width=60></td>
    <td width=60></td>
    <td width=60></td>
    <td width=60></td>
    <td width=60></td>
  </tr>

  <tr>
    <td><img src="https://webservices.solomononline.com/RefineryReports/Templates/ChevronLogo.gif" align=left></td>
    <td Colspan=8><img src="https://webservices.solomononline.com/RefineryReports/Templates/ChevronTag.gif" align=right></td>
  </tr>


  <tr>
    <td height=50 colspan=9 align=Left valign=bottom><strong>Refinery Reconciliation Report</strong></td>
  </tr>
  <tr>
    <td align=left colspan=9 valign=bottom>Current Month&nbsp;&nbsp;&nbsp;::&nbsp;&nbsp;&nbsp;ReportPeriod</td>
  </tr>
  <tr>
    <td align=left colspan=9 valign=bottom height=60>Utilization and Availabilities</td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center valign=bottom>Total</td>
    <td align=center valign=bottom>Purchased</br>Utility</br>Credit</td>
    <td align=center colspan=2 valign=bottom>Refinery Utilization</td>
    <td align=center valign=bottom></td>
    <td align=center colspan=2 valign=bottom>Process Unit</br>Utilization</td>
    <td align=center valign=bottom></td>
  </tr>

  SECTION(CVX_RECON,,)  
  BEGIN

  <tr>
    <td valign=bottom height=20>&nbsp;&nbsp;&nbsp;UEDC&trade;, k</span></td>
    <td align=center valign=bottom>NoShowZero(Format(@UEDC,0))</td>
    <td align=center valign=bottom>NoShowZero(Format(@PurUtilUEDC,0))</td>
    <td align=center valign=bottom><u>NoShowZero(Format(@RefUtilUEDC,0))</u></td>
    <td align=Left valign=bottom>=&nbsp;&nbsp;&nbsp;NoShowZero(Format(@UtilPcnt,1))%</td>
    <td align=right valign=bottom></td>
    <td align=center valign=bottom><u>NoShowZero(Format(@TotProcessUEDC,0))</u></td>
    <td align=left valign=bottom>=&nbsp;&nbsp;&nbsp;NoShowZero(Format(@ProcessUtilPcnt,1))%</td>
    <td align=right valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;EDC&trade;, k</span></td>
    <td align=right colspan=2 valign=bottom></td>
    <td align=center valign=bottom>NoShowZero(Format(@EDC,0))</td>
    <td align=right colspan=2 valign=bottom></td>
    <td align=center valign=bottom>NoShowZero(Format(@TotProcessEDC,0))</td>
    <td align=right colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40></td>
    <td align=center colspan=2 valign=bottom>--- Mechanical ---</td>
    <td align=center colspan=2 valign=bottom>--- Operational ---</td>
    <td align=center colspan=2 valign=bottom>--- OnStream ---</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>Calculated</td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center valign=bottom>Outage</td>
    <td align=center valign=bottom>Slowdown</td>
    <td align=center valign=bottom>Outage</td>
    <td align=center valign=bottom>Slowdown</td>
    <td align=center valign=bottom>Outage</td>
    <td align=center valign=bottom>Slowdown</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>Utilization</td>
  </tr>

  <tr>
    <td valign=bottom height=20>&nbsp;&nbsp;&nbsp;Process Calculation, %</span></td>
    <td align=center valign=bottom>Format(@MechUnavail_Act,1)</td>
    <td align=center valign=bottom>Format(@MechUnavailSlow_Act,1)</td>
    <td align=center valign=bottom>Format(@OpUnavail_Act,1)</td>
    <td align=center valign=bottom>Format(@OpUnavailSlow_Act,1)</td>
    <td align=center valign=bottom>Format(@OffStream_Act,1)</td>
    <td align=center valign=bottom>Format(@OffStreamSlow_Act,1)</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>NoShowZero(Format(@ProcessUtilPcntCalc,1))</td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=60>Operating Expenses</td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center colspan=2 valign=bottom>Current Month</td>
    <td align=center colspan=2 valign=bottom>Year-to-Date</td>
    <td align=center colspan=2 valign=bottom>Rolling Average</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US �/UEDC</td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US �/UEDC</td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US �/UEDC</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=20>&nbsp;&nbsp;&nbsp;Total Cash Operating Expenses</span></td>
    <td align=center valign=bottom>Format(@TotCashOpex,1)</td>
    <td align=center valign=bottom>Format(@TotCashOpexUEDC,1)</td>
    <td align=center valign=bottom>Format(@TotCashOpex_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotCashOpexUEDC_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotCashOpex_AVG,1)</td>
    <td align=center valign=bottom>Format(@TotCashOpexUEDC_AVG,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Purchased Energy</span></td>
    <td align=center valign=bottom>Format(@TotPurEnergy,1)</td>
    <td align=center valign=bottom>Format(@TotPurEnergyUEDC,1)</td>
    <td align=center valign=bottom>Format(@TotPurEnergy_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotPurEnergyUEDC_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotPurEnergy_AVG,1)</td>
    <td align=center valign=bottom>Format(@TotPurEnergyUEDC_AVG,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Produced Energy</span></td>
    <td align=center valign=bottom>Format(@TotProdEnergy,1)</td>
    <td align=center valign=bottom>Format(@TotProdEnergyUEDC,1)</td>
    <td align=center valign=bottom>Format(@TotProdEnergy_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotProdEnergyUEDC_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotProdEnergy_AVG,1)</td>
    <td align=center valign=bottom>Format(@TotProdEnergyUEDC_AVG,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other Energy Purchases</span></td>
    <td align=center valign=bottom>Format(@TotOthEnergy,1)</td>
    <td align=center valign=bottom>Format(@TotOthEnergyUEDC,1)</td>
    <td align=center valign=bottom>Format(@TotOthEnergy_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotOthEnergyUEDC_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotOthEnergy_AVG,1)</td>
    <td align=center valign=bottom>Format(@TotOthEnergyUEDC_AVG,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Energy Operating Expenses</span></td>
    <td align=center valign=bottom>Format(@NEOpex,1)</td>
    <td align=center valign=bottom>Format(@NEOpexUEDC,1)</td>
    <td align=center valign=bottom>Format(@NEOpex_YTD,1)</td>
    <td align=center valign=bottom>Format(@NEOpexUEDC_YTD,1)</td>
    <td align=center valign=bottom>Format(@NEOpex_AVG,1)</td>
    <td align=center valign=bottom>Format(@NEOpexUEDC_AVG,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;UEDC, k</span></td>
    <td align=center valign=bottom>Format(@UEDC,0)</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>Format(@UEDC_YTD,0)</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>Format(@UEDC_AVG,0)</td>
    <td align=center colspan=3 valign=bottom></td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=60>Personnel</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;(Equivalent people per 100,000 EDC)</td>
    <td align=center valign=bottom>Straight-</br>Time Hours</td>
    <td align=center valign=bottom>Overtime</br>Hours</td>
    <td align=center valign=bottom>Contract</br>Hours</td>
    <td align=center valign=bottom>G&A</br>Hours</td>
    <td align=center valign=bottom>Absence</br>Hours</td>
    <td align=center valign=bottom>Total</br>Hours</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>Equivalent</br>Personnel</td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=20>&nbsp;&nbsp;&nbsp;Operator, Carft & Clerical</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process Operations</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCPO_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCPO_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCPO_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCPO_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCPO_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCPO_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCPO_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maintenance Activities</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCTAADJ_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCTAADJ_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCTAADJ_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCTAADJ_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCTAADJ_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCTAADJ_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCTAADJ_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCMA_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCMA_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCMA_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCMA_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCMA_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCMA_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCMA_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Administrative Services</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCAS_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCAS_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCAS_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCAS_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCAS_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCAS_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@OCCAS_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal O,C&C</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCC_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCC_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCC_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCC_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCC_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OCC_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@OCC_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=40>&nbsp;&nbsp;&nbsp;Management, Professional & Staff</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process Operations</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSPO_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSPO_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSPO_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSPO_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSPO_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSPO_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSPO_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maintenance Activities</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTAADJ_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTAADJ_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTAADJ_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTAADJ_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTAADJ_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTAADJ_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTAADJ_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSMA_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSMA_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSMA_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSMA_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSMA_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSMA_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSMA_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Technical Support</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTS_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTS_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTS_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTS_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTS_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTS_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSTS_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Administrative Services</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSAS_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSAS_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSAS_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSAS_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSAS_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSAS_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@MPSAS_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal M,P&S</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPS_STH,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPS_OVT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPS_Contract,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPS_GA,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPS_AbsHrs,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPS_TotWHr,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@MPS_TotEqPEDC,1))</td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=60>Maintenance</td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center colspan=2 valign=bottom>--- Current Month ---</td>
    <td align=center colspan=2 valign=bottom>--- Year-to-Date ---</td>
    <td align=center colspan=2 valign=bottom>-- Rolling Average --</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US $/EDC</td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US $/EDC</td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US $/EDC</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=20>&nbsp;&nbsp;&nbsp;Annualized Turnaround</span></td>
    <td align=center valign=bottom>Format(@AnnTACost,1)</td>
    <td align=center valign=bottom>Format(@TAIndex,1)</td>
    <td align=center valign=bottom>Format(@AnnTACost_YTD,1)</td>
    <td align=center valign=bottom>Format(@TAIndex,1)</td>
    <td align=center valign=bottom>Format(@AnnTACost_AVG,1)</td>
    <td align=center valign=bottom>Format(@TAIndex,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Non-Turnaround</span></td>
    <td align=center valign=bottom>Format(@CurrRoutCost,1)</td>
    <td align=center valign=bottom>Format(@RoutIndex,1)</td>
    <td align=center valign=bottom>Format(@CurrRoutCost_YTD,1)</td>
    <td align=center valign=bottom>Format(@RoutIndex_YTD,1)</td>
    <td align=center valign=bottom>Format(@CurrRoutCost_AVG,1)</td>
    <td align=center valign=bottom>Format(@RoutIndex_AVG,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Maintenance</span></td>
    <td align=center valign=bottom>Format(@TotMaintCost,1)</td>
    <td align=center valign=bottom>Format(@MaintIndex,1)</td>
    <td align=center valign=bottom>Format(@TotMaintCost_YTD,1)</td>
    <td align=center valign=bottom>Format(@MaintIndex_YTD,1)</td>
    <td align=center valign=bottom>Format(@TotMaintCost_AVG,1)</td>
    <td align=center valign=bottom>Format(@MaintIndex_AVG,1)</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>


  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;EDC, k</span></td>
    <td align=center valign=bottom>Format(@EDC,0)</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>Format(@EDC_YTD,0)</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>Format(@EDC_AVG,0)</td>
    <td align=center colspan=3 valign=bottom></td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=60>Energy Consumption</td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center valign=bottom>Thermal</br>Energy,</br>MBtu</td>
    <td align=center valign=bottom>Electricity,</br>MWh</td>
    <td align=center colspan=6 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=20>&nbsp;&nbsp;&nbsp;Purchased</td>
    <td align=right valign=bottom>NoShowZero(Format(@RptSource_PUR,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SourceMWH_PUR,0))</td>
    <td align=center colspan=6 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Produced</td>
    <td align=right valign=bottom>NoShowZero(Format(@RptSource_PRO,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SourceMWH_PRO,0))</td>
    <td align=center colspan=6 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Transfers into Refinery</td>
    <td align=right valign=bottom>NoShowZero(Format(@RptSource_IN,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SourceMWH_IN,0))</td>
    <td align=center colspan=6 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Transfers to Affiliates</td>
    <td align=right valign=bottom>NoShowZero(Format(@RptSource_OUT,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SourceMWH_OUT,0))</td>
    <td align=center colspan=6 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Sold</td>
    <td align=right valign=bottom>NoShowZero(Format(@RptSource_SOL,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SourceMWH_SOL,0))</td>
    <td align=center colspan=6 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Consumption</td>
    <td align=right valign=bottom>NoShowZero(Format(@TotEnergyConsMBTU,0))</td>
    <td align=center colspan=7 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Daily Energy Use</td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyUseDay,0))</td>
    <td align=center colspan=7 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40></td>
    <td align=center valign=bottom>Process</br>Unit</td>
    <td align=center valign=bottom>Sensible</br>Heat</td>
    <td align=center valign=bottom>Utilities</br>Off-sites</td>
    <td align=center valign=bottom>Asphalt</td>
    <td align=center valign=bottom>Total</td>
    <td align=center valign=bottom></td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=20>&nbsp;&nbsp;&nbsp;Standard Energy</td>
    <td align=right valign=bottom>NoShowZero(Format(@ProcessStdEnergy,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SensHeatStdEnergy,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OffsitesStdEnergy,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@AspStdEnergy,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TotStdEnergy,0))</td>
    <td align=center valign=bottom></td>
    <td align=center valign=bottom>EII&reg;</td>
    <td align=left valign=bottom>NoShowZero(Format(@EII,0))</td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=60>Material Balance</td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center colspan=3 valign=bottom>------ Current Month ------</td>
    <td align=center colspan=3 valign=bottom>------ Year-to-Date ------</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td align=center valign=bottom>Barrels, k</td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US $/bbl</td>
    <td align=center valign=bottom>Barrels, k</td>
    <td align=center valign=bottom>US $M</td>
    <td align=center valign=bottom>US $/bbl</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=20>&nbsp;&nbsp;&nbsp;Raw Materials</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Crude Charge Detail</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMI,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMICost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMIPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMI_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMICost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMIPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other Raw Materials</td>
    <td align=right valign=bottom>NoShowZero(Format(@OTHRM,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OTHRMCost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OTHRMPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OTHRM_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OTHRMCost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@OTHRMPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal</td>
    <td align=right valign=bottom>NoShowZero(Format(@RM,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMCost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RM_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMCost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RMPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;Net Input Barrels (monthly basis)</td>
    <td align=right valign=bottom>NoShowZero(Format(@NetInputBPD,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@NetInputBPD_YTD,0))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom></td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td align=left colspan=9 valign=bottom height=40>&nbsp;&nbsp;&nbsp;Product Yield</td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Yield</td>
    <td align=right valign=bottom>NoShowZero(Format(@PROD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PRODCost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PRODPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PROD_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PRODCost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@PRODPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refinery-Produced Fuels</td>
    <td align=right valign=bottom>NoShowZero(Format(@RPF,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RPFCost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RPFPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RPF_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RPFCost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@RPFPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Specialty Solvents</td>
    <td align=right valign=bottom>NoShowZero(Format(@SOLV,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SOLVCost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SOLVPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SOLV_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SOLVCost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@SOLVPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saleable Petroleum Coke</td>
    <td align=right valign=bottom>NoShowZero(Format(@COKE,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@COKECost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@COKEPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@COKE_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@COKECost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@COKEPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Miscellaneous Products</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPROD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPRODCost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPRODPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPROD_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPRODCost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@MPRODPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal</td>
    <td align=right valign=bottom>NoShowZero(Format(@YIELD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@YIELDCost,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@YIELDPriceUS,2))</td>
    <td align=right valign=bottom>NoShowZero(Format(@YIELD_YTD,0))</td>
    <td align=right valign=bottom>NoShowZero(Format(@YIELDCost_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@YIELDPriceUS_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;Gross Margin</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotGrossMargin,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@GrossMargin,2))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotGrossMargin_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@GrossMargin_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;Operating Expenses</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpex,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpexBbl,2))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpex_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpexBbl_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>

  <tr>
    <td valign=bottom height=40>&nbsp;&nbsp;&nbsp;Net Cash Margin</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashMargin,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CashMargin,2))</td>
    <td align=right valign=bottom></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashMargin_YTD,1))</td>
    <td align=right valign=bottom>NoShowZero(Format(@CashMargin_YTD,2))</td>
    <td align=center colspan=2 valign=bottom></td>
  </tr>
  END  

</table>
<!-- template-end -->
