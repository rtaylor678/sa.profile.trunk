<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small width=900 border=0>
SECTION(Total,,)
BEGIN 
HEADER('
  <tr>
    <td colspan=20 valign=bottom><strong>Format(PeriodStart,'MM.yyyy')</strong></td>
  </tr>
')
END
<!--  <tr>
    <td colspan=20 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
-->
  <tr>
    <td width=5></td>
    <td width=200></td>
    <td width=10></td>
    <td width=70></td>
    <td width=60></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
  </tr>
  <tr>
    <td colspan=6></td>
    <td colspan=4 align=center valign=bottom><strong>---- Часы простоев ----</strong></td>  <!-- Downtime Hours -->
    <td colspan=8></td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Название установки</strong></td>  <!-- Process Unit Name -->
    <td colspan=2 align=center valign=bottom><strong>Идентификатор</br>технологического</br>процесса</strong></td>  <!-- Process ID -->
    <td colspan=2 align=center valign=bottom><strong>Средний ЭДП/1000</strong></td>  <!-- Average EDC -->
    <td colspan=2 align=center valign=bottom><strong>по регламентным и технологическим причинам</strong></td>  <!-- Process/Regulatory -->
    <td colspan=2 align=center valign=bottom><strong>по механическим причинам</strong></td>  <!-- Mechanical -->
    <td colspan=2 align=center valign=bottom><strong>Кол-во часов в периоде</strong></td>  <!-- Period Hours -->
    <td colspan=2 align=center valign=bottom><strong>Потеря готовности в связи с кап. ремонтами</strong></td>  <!-- Unavailability Due to T/A -->
    <td colspan=2 align=center valign=bottom><strong>Механическая готовность</strong></td>  <!-- Mechanical Availability -->
    <td colspan=2 align=center valign=bottom><strong>Эксплуатационная готовность</strong></td>  <!-- Operational Availability -->
  </tr>

  SECTION(Unit,,)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(AvgEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(RegDown,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(MaintDown,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(PeriodHrs,'#,##0')</td>
    <td></td>
    <td align=right>Format(MechUnavailTA,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(MechAvail,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(OpAvail,'#,##0.0')</td>
    <td></td>
  </tr>
  END

  SECTION(Total,,)
  BEGIN
  <tr>
    <td colspan=12><strong>Всего - технологические установки</strong></td>  <!-- Total Process Units -->
    <td align=right valign=bottom><strong>Format(MechUnavailTA,'#,##0.00')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(MechAvail,'#,##0.0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(OpAvail,'#,##0.0')</strong></td>
    <td></td>
  </tr>
  END

</table>

<!-- template-end -->