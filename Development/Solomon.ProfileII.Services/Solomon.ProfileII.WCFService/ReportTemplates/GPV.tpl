<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small border=0>
  <tr>
    <td width=300></td>
    <td width=100></td>
    <td width=100></td>
    <td width=100></td>
  </tr>

  <tr>
    <td colspan=4><strong>ReportPeriod</strong></td>
  </tr>

  <tr>
    <td height=60 valign=middle><strong>&nbsp;&nbsp;&nbsp;Product Yield</strong></td>
    <td valign=middle align=right><strong>Total Barrels</strong></td>
    <td valign=middle align=right><strong>Price, CurrencyCode/b</strong></td>
    <td valign=middle align=right><strong>GPV, k CurrencyCode</strong></td>
  </tr>
  SECTION(Prod,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Asphalt,,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Asphalts</td>
  </tr>
  ')
  END
  SECTION(Asphalt,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Category,Category='FCHEM',)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Feedstocks to Chemical Plant</td>
  </tr>
  ')
  END
  SECTION(Category,Category='FCHEM',)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Solvents,,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Specialty Solvents</td>
  </tr>
  ')
  END
  SECTION(Solvents,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Lubes,,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Feedstocks to Paraffinic Lube Refinery</td>
  </tr>
  ')
  END
  SECTION(Lubes,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Category,Category='FLUBE',)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Feedstocks to Paraffinic Lube Refinery</td>
  </tr>
  ')
  END
  SECTION(Category,Category='FLUBE',)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Coke,,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saleable Petroleum Coke (FOE)</td>
  </tr>
  ')
  END
  SECTION(Coke,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(MiscProd,,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Miscellaneous Products</td>
  </tr>
  ')
  END
  SECTION(MiscProd,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  <tr>
    <td height=60 valign=middle><strong>&nbsp;&nbsp;&nbsp;Raw Materials</strong></td>
    <td valign=middle align=right><strong>Total Barrels</strong></td>
    <td valign=middle align=right><strong>Price, CurrencyCode/b</strong></td>
    <td valign=middle align=right><strong>RMC, k CurrencyCode</strong></td>
  </tr>

  SECTION(Crudes,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Category,Category='RCHEM',)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Returns from Chemical Plant</td>
  </tr>
  ')
  END
  SECTION(Category,Category='RCHEM',)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(Category,Category='RLUBE',)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Returns from Paraffinic Lube Refinery</td>
  </tr>
  ')
  END
  SECTION(Category,Category='RLUBE',)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

  SECTION(OtherRM,,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other Raw Materials</td>
  </tr>
  ')
  END
  SECTION(OtherRM,,)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MaterialName</td>
    <td align=right>Format(Bbl,'#,##0')</td>
    <td align=right>Format(PricePerBbl,'#,##0.00')</td>
    <td align=right>Format(Value,'#,##0.0')</td>
  </tr>
  END

</table>
<!-- template-end -->