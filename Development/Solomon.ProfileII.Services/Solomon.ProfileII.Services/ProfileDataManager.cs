﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.ProfileII.Contracts;
using System.Data;
using Solomon.ProfileII.Data;


namespace Solomon.ProfileII.Services
{
    public class ProfileDataManager //: IProfileDataService
    {        
        string _db12Conx = string.Empty;
        string _userHostAddress = string.Empty;
        string _appName = "ProfileII.Services";
        

        public ProfileDataManager(string db12Conx, string userHostAddress)
        {
            _db12Conx = db12Conx;
            _userHostAddress = userHostAddress;

        }

        public string CheckService()
        {
            FileTransfer svc = new FileTransfer( _db12Conx);
            return svc.CheckService(_userHostAddress);
        }

        public byte[] DownloadTplFile(string fileinfo, string appVersion, string refNum)
        {
            FileTransfer svc = new FileTransfer(   _db12Conx);
            return svc.DownloadTplFile(fileinfo, appVersion,  _userHostAddress);
        }

        public DataSet GetDataByPeriod(DateTime periodStart, DateTime periodEnd, string refineryID)
        {
            SubmitServices svc = new SubmitServices(   _db12Conx);
            return svc.GetDataByPeriod(periodStart, periodEnd, refineryID);
        }

        public DataSet GetDataDump(string ReportCode, string ds, string scenario, string currency, 
            DateTime startDate, string UOM, int studyYear, bool includeTarget, bool includeYTD, 
            bool includeAVG, string refineryID)
        {
            DataServices svc = new DataServices(   _db12Conx);
            return svc.GetDataDump(ReportCode.Trim(), ds, scenario, currency, startDate,
                UOM, studyYear, includeTarget, includeYTD, includeAVG, refineryID, _userHostAddress);
        }

        public DataSet GetInputData(string refineryID)
        {
            SubmitServices svc = new SubmitServices( _db12Conx);
            return svc.GetInputData(refineryID);
        }

        public DataSet GetLookups(string refineryID, string companyID)
        {
            DataServices svc = new DataServices( _db12Conx);
            return svc.GetLookups(refineryID, companyID, _userHostAddress);
        }

        public DataSet GetReferences(string refineryID)
        {
            DataServices svc = new DataServices( _db12Conx);
            return svc.GetReferences(refineryID, _userHostAddress);
        }

        public bool RefineryIs2012(string RefineryID)
        {
            DataServices svc = new DataServices( _db12Conx);
            return svc.RefineryIs2012(RefineryID);
        }

        public void SubmitRefineryData(DataSet ds, string refineryID)
        {
            SubmitServices svc = new SubmitServices( _db12Conx);
            svc.SubmitRefineryData(ds, refineryID, _userHostAddress);
        }

        public string UploadFile(string fileName, byte[] bytes, string refNum)
        {
            FileTransfer svc = new FileTransfer( _db12Conx);
            return svc.UploadFile(fileName, bytes, refNum, _userHostAddress);
        }

        public void WriteActivityLogExtended(string applicationName, string methodology, string refNum, string CallerIP, string UserID, string ComputerName, string service, string Method, string EntityName, DateTime? PeriodStart, DateTime? PeriodEnd, string status, string version, string errorMessages, string localDomainName, string osVersion, string browserVersion, string officeVersion, string dataImportedFromBridgeFile)
        {
            DataServices svc = new DataServices( _db12Conx);
            svc.WriteActivityLogExtended(applicationName, methodology, refNum, CallerIP, UserID, ComputerName, service, Method, EntityName,  PeriodStart,  PeriodEnd, status, version, errorMessages, localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile);
        }

        internal void UndoLastXXPACSubmission(int submissionToRollBackTo)
        {
            try
            {
                string testRefineryId = "XXPAC";
                string sql = string.Empty;
                DataSet ids = new DataSet();
                DateTime startDate = new DateTime();
                sql = "SELECT SubmissionID, PeriodStart from dbo.SubmissionsAll where RefineryID='" + testRefineryId + "' ORDER BY SubmissionID desc;";
                ids = ProfileDbManager.QueryDB(sql);
                startDate = Convert.ToDateTime(ids.Tables[0].Rows[0]["PeriodStart"]);
                sql = "UPDATE dbo.SubmissionsAll set CalcsNeeded=null, UseSubmission=0 where submissionid = " + Convert.ToInt32(ids.Tables[0].Rows[0]["SubmissionId"]);
                ProfileDbManager.InsertUpdateDelete(sql);
                sql = "UPDATE dbo.SubmissionsAll set CalcsNeeded=null, UseSubmission=1 where submissionid = " + submissionToRollBackTo; 
                ProfileDbManager.InsertUpdateDelete(sql);
            }
            catch(Exception discard)
            {
                string debugInfo = discard.Message;
            }
        }

        public string Authenticate(string clientKey, string callerIP, ref string refineryID)
        {
            //does all checking and logs any errors, returns generic messge to client.
            ProfileDataManager pdm = null;
            string errorMessage = string.Empty;
            string result = string.Empty;
            try
            {
                pdm = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, callerIP);
                if (clientKey == null)
                {
                    errorMessage = "Client Key is null";
                }
                else if (clientKey.Length == 0)
                {
                    errorMessage = "Client Key is empty";
                }
                else
                {
                    errorMessage  = Utilities.ValidateKey2(clientKey, ref refineryID);
                    if (errorMessage.Length<1)
                    {
                        byte[] certName = System.Text.Encoding.ASCII.GetBytes(clientKey);
                        errorMessage = GoodCert(clientKey, certName);
                    }
                }                   
            }
            catch (Exception ex)
            {
                if (pdm != null)
                    errorMessage += Utilities.ExMessageExtended(ex);
            }
            if (errorMessage.Length > 0)
            {
                WriteActivityLogExtended(_appName, null, refineryID, callerIP, null, null, "ProfileII.Data",
                        "Authenticate()", null, null,null, "ERROR", null, errorMessage, null, null, null, null, null);
                return "There was an error authenticating to ProfileII. Please call Solomon";
            }
            else
                return string.Empty;

        }
        /* private string GetRefNum(string clientKey)
        {
            string refId = string.Empty;
            ProfileDataManager pdm = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
            try
            {
                char[] delim = { '$' };
                int refLocation = Cryptographer.Decrypt(clientKey).Split(delim).Length - 1;
                refId = Cryptographer.Decrypt(clientKey).Split(delim)[refLocation];
                return refId;
            }
            catch (Exception ex)
            {
                pdm.WriteActivityLogExtended("ProfileII.WCFService", null, refId, GetUserHostAddress(), null, null, "ProfileService.svc",
                "GetRefNum", null, null, null, "ERROR", null, "Invalid client key: " + clientKey + " | " + ExMessageExtended(ex), null, null, null, null, null);

                //ProfileLogManager.LogException(new Exception("Invalid client key:" + clientKey));
                throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
            }
        }*/



        /*private string GoodKey(string clientKey, ref string refNum)
        {
                       foreach (var hdr in OperationContext.Current.IncomingMessageHeaders)
            {
                System.ServiceModel.Channels.MessageHeaders headers = OperationContext.Current.IncomingMessageHeaders;
                if (hdr.Name.ToUpper().Trim() == "WSP")
                {
                    clientKey = headers.GetHeader<string>("WsP", string.Empty);// hdr.Namespace;
                }
            }
            string result = Utilities.ValidateKey2(clientKey, ref refNum);
            if (result.Length>0)
            
                //ProfileLogManager.LogException(new Exception("Invalid client key: " + clientKey));
                //throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
            }
            else
            {
                return string.Empty;
            }
                    }*/
                    
        private string GoodCert(string clientKey, byte[] cert)
        {
            string errorResponse = string.Empty;
            if (!Utilities.AuthenticateByCertificateAndClientId(ref errorResponse, cert, clientKey, false))
            {
                return "Invalid Certificate.";
            }
            else
            {
                return string.Empty;
            }
            return string.Empty;
        }
    }
}


