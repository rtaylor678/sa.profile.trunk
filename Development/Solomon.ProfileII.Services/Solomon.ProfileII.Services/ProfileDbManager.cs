﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//using Solomon.ProfileII.Logger;


namespace Solomon.ProfileII.Services
{
    public static class ProfileDbManager
    {
        static ProfileDbManager()
        {
        }
        public static DataSet QueryDB(string sql)
        {
            DataSet ds =new DataSet();
            try
            {
                using (SqlConnection conx = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString()))
                {
                    if (sql.Trim().ToUpper().StartsWith("SELECT") || sql.Trim().ToUpper().StartsWith("EXEC"))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(sql, conx);
                        da.Fill(ds);
                    }
                }
                return ds;
            }
            catch (Exception ex)
            {
                try
                {
                    ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                    pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "ProfileDbManager",
                    "QueryDB", null, null, null, "ERROR",null, null, ExMessageExtended(ex), null, null, null, null);
                }
                catch (Exception ex2)
                {
                    //do nothing for now
                }
                //ProfileLogManager.LogException(new Exception("Error in QueryDB: " +  ex.Message));
                return null;
            }

        }

        private static string ExMessageExtended(Exception ex)
        {
            string result = ex.Message;
            if (ex.InnerException != null)
                result += " | " + ex.InnerException.Message;
            return result;
        }
        internal static DataSet InsertUpdateDelete(string sql)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conx = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString()))
                {
                        SqlDataAdapter da = new SqlDataAdapter(sql, conx);
                        da.Fill(ds);
                    
                }
                return ds;
            }
            catch (Exception ex)
            {
                ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "ProfileDbManager",
                "InsertUpdateDelete", null, null, null, "ERROR", null, ExMessageExtended(ex), null, null, null, null,null);
                //ProfileLogManager.LogException(new Exception("Error in QueryDB: " + ex.Message));
                return null;
            }

        }

    }

}
