﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
//using Solomon.ProfileII.Logger;
using System.Security.Cryptography.X509Certificates;
using ProfileLiteSecurity.Client;
using System.Data.SqlClient;




namespace Solomon.ProfileII.Services
{
    public static class Utilities
    {
        public static string GetQuerystringVariable(string queryString, string key)
        {
            try
            {
                string[] qsParts = queryString.Split('&');
                for (int i = 0; i < qsParts.Length; i++)
                {
                    string[] parts = qsParts[i].Split('=');
                    if (parts[0].ToUpper() == key.ToUpper())
                        return parts[1];
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
            
    public static bool ValidateKey(string clientKey, ref string refNum)
    {
        string company,  location, path = string.Empty;
        int locIndex = 0;
        try
        {
            if (clientKey != null && clientKey.Length>0)
            {
                char[] delim = { '$' };
                company = Cryptographer.Decrypt(clientKey).Split(delim)[0]; // (0);
                locIndex = Cryptographer.Decrypt(clientKey).Split(delim).Length - 2;
                location = Cryptographer.Decrypt(clientKey).Split(delim)[locIndex];
                string[] array = Cryptographer.Decrypt(clientKey).Split(delim);
                refNum = array[array.Length - 1]; // 'last position
                string clientKeysFolder = string.Empty;
                try
                {
                    clientKeysFolder = ConfigurationManager.AppSettings["ClientKeysFolder"].ToString();
                }
                catch { }

                if(clientKeysFolder.Length>0)
                {
                    if (System.IO.File.Exists(clientKeysFolder + company + "_" + location + ".key"))
                    {
                        path = clientKeysFolder + company + "_" + location + ".key";
                    }
                }
                else
                {
                    if (System.IO.File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key"))
                    {
                        path = "C:\\ClientKeys\\" + company + "_" + location + ".key";
                    } else if (File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key"))
                    {
                        path = "D:\\ClientKeys\\" + company + "_" + location + ".key";
                     } else
                    {
                        return false;
                    }
                }

                if (path.Trim().Length == 0)
                {
                    return false;
                }

                string readKey = string.Empty;
                using (StreamReader reader = new System.IO.StreamReader(path))
                {
                    readKey = reader.ReadLine();
                }
                if(readKey.Length>0)
                {
                    return (readKey.Trim() == clientKey.Trim());
                }
                return false;
            }
            else
            {
                return false;
            }
     
        }
        catch(Exception ex)
        {
                ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
                "ValidateKey", null, null, null, "ERROR", null,  "Error in ValidateKey for RefNum '" + refNum + "', clientKey '" + clientKey + "': " + ExMessageExtended(ex), null, null, null, null ,null);
                //ProfileLogManager.LogException(new Exception("Error in ValidateKey for RefNum '" + refNum + "', clientKey '" + clientKey + "': " + ex.Message));
            throw ex;
        }

    }
        public static string ExMessageExtended(Exception ex)
        {
            string result = ex.Message;
            if (ex.InnerException != null)
                result += " | " + ex.InnerException.Message;
            return result;
        }
         public static string ValidateKey2(string clientKey, ref string refineryID)
        {
            string company, location, path = string.Empty;
            int locIndex = 0;
            try
            {
                if (clientKey != null && clientKey.Length > 0)
                {
                    char[] delim = { '$' };
                    company = Cryptographer.Decrypt(clientKey).Split(delim)[0]; // (0);
                    locIndex = Cryptographer.Decrypt(clientKey).Split(delim).Length - 2;
                    location = Cryptographer.Decrypt(clientKey).Split(delim)[locIndex];
                    string[] array = Cryptographer.Decrypt(clientKey).Split(delim);
                    refineryID = array[array.Length - 1]; // 'last position
                    string clientKeysFolder = string.Empty;
                    try
                    {
                        clientKeysFolder = ConfigurationManager.AppSettings["ClientKeysFolder"].ToString();
                    }
                    catch { }

                    if (clientKeysFolder.Length > 0)
                    {
                        if (System.IO.File.Exists(clientKeysFolder + company + "_" + location + ".key"))
                        {
                            path = clientKeysFolder + company + "_" + location + ".key";
                        }
                    }
                    else
                    {
                        if (System.IO.File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key"))
                        {
                            path = "C:\\ClientKeys\\" + company + "_" + location + ".key";
                        }
                        else if (File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key"))
                        {
                            path = "D:\\ClientKeys\\" + company + "_" + location + ".key";
                        }
                        else
                        {
                            return "Unable to find matching key (company and location) on server using hardcoded paths";
                        }
                    }

                    if (path.Trim().Length == 0)
                    {
                        return "Unable to find matching key (company and location) on server."; 
                    }

                    string readKey = string.Empty;
                    using (StreamReader reader = new System.IO.StreamReader(path))
                    {
                        readKey = reader.ReadLine();
                    }
                    if (readKey.Length > 0)
                    {
                        if (readKey.Trim() == clientKey.Trim())
                        {
                            return string.Empty;
                        }
                    }
                    return "Client key error on server";
                }
                else
                {
                    return "Client key is empty";
                }

            }
            catch (Exception ex)
            {
                /*
                ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
                "ValidateKey", null, null, null, "ERROR", null, "Error in ValidateKey for RefNum '" + refNum + "', clientKey '" + clientKey + "': " + ExMessageExtended(ex), null, null, null, null, null);
                //ProfileLogManager.LogException(new Exception("Error in ValidateKey for RefNum '" + refNum + "', clientKey '" + clientKey + "': " + ex.Message));
                */
                return ExMessageExtended(ex);
            }

        }

           public static bool AuthenticateByCertificateAndClientId(ref  string errorResponse , byte[] encodedSignedCms ,string clientKey , bool displaySysException )
    {
            try
            {
                //ProfileLogManager.LogInfo("Entering AuthenticateByCertificateAndClientId(), with client key " + clientKey);
                ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                //pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
                //"AuthenticateByCertificateAndClientId", null, null, null, "INFO", null,"Entering AuthenticateByCertificateAndClientId(), with client key " + clientKey, null, null, null, null,null);
                X509Certificate2 publicCert = new X509Certificate2();
                ProfileLiteSecurity.Cryptography.Certificate pcc = new ProfileLiteSecurity.Cryptography.Certificate();
                ProfileLiteSecurity.Client.ClientKey profileLiteSecurity = new ProfileLiteSecurity.Client.ClientKey();
                ProfileLiteSecurity.Client.ClientKey profileLiteSecurityClientKey = profileLiteSecurity.GetClientKeyInfoFromToken(clientKey);
                string errors = string.Empty;
                string name = profileLiteSecurityClientKey.Company;
                string refineryId = profileLiteSecurityClientKey.RefNum;
                bool activeAuthenticationAndAuthorizationRecord = false;
                bool certificateRequired = true;
                bool clientKeyRequired = true;
                string clientKeyFromDb = string.Empty;
                //ProfileLogManager.LogInfo("Calling userAuthentication.CheckAuthenticationAndAuthorizationTable");
                //pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
                //"AuthenticateByCertificateAndClientId", null, null, null, "INFO", null, "Calling userAuthentication.CheckAuthenticationAndAuthorizationTable", null, null, null, null ,null);
                if (!Utilities.CheckAuthenticationAndAuthorizationTable(refineryId, ref activeAuthenticationAndAuthorizationRecord, ref certificateRequired, ref clientKeyRequired, ref clientKeyFromDb))
                {
                    pdm.WriteActivityLogExtended("ProfileII.Services", null, refineryId, null, null, null, "Utilities",
                "AuthenticateByCertificateAndClientId", null, null, null, "ERROR",  null, "Error or no record found in AuthenticateByCertificateAndClientId() for RefineryId " + refineryId, null, null, null, null ,null);
                    //ProfileLogManager.LogInfo("Error or no record found in AuthenticateByCertificateAndClientId() for RefineryId " + refineryId);
                    throw new Exception("Your account is not active. Please contact Solomon.");
                }

                if (!activeAuthenticationAndAuthorizationRecord)
                {
                    pdm.WriteActivityLogExtended("ProfileII.Services", null, refineryId, null, null, null, "Utilities",
                    "AuthenticateByCertificateAndClientId", null, null, null, "ERROR",null , "activeAuthenticationAndAuthorizationRecord is false for RefineryId " + refineryId, null, null, null, null ,null);
                    //ProfileLogManager.LogInfo("activeAuthenticationAndAuthorizationRecord is false for RefineryId " + refineryId);
                    throw new Exception("Your account is not active. Please contact Solomon.");
                }
                if (clientKeyRequired)
                {
                    if (clientKey.Trim() != clientKeyFromDb.Trim())
                    {
                        pdm.WriteActivityLogExtended("ProfileII.Services", null, refineryId, null, null, null, "Utilities",
                "AuthenticateByCertificateAndClientId", null, null, null, "ERROR",  null, "Client Key mismatch in AuthenticateByCertificateAndClientId() for RefineryId " + refineryId, null, null, null, null ,null);
//                        ProfileLogManager.LogInfo("Client Key mismatch in AuthenticateByCertificateAndClientId() for RefineryId " + refineryId);
                        throw new Exception("Your key is invalid. Please contact Solomon to get a valid key.");
                    }
                }
                if (certificateRequired)
                {
                    pdm.WriteActivityLogExtended("ProfileII.Services", null, refineryId, null, null, null, "Utilities",
                "AuthenticateByCertificateAndClientId", null, null, null,  "INFO", null, "Attempting to check certificate for company " + name + ", refnum " + refineryId, null, null, null, null ,null);
                    //ProfileLogManager.LogInfo("Attempting to check certificate for company " + name + ", refnum " + refineryId);
                    string nameAndRefNum = profileLiteSecurityClientKey.Company + " (" + profileLiteSecurityClientKey.RefNum + ")";
                    if (!pcc.Get(nameAndRefNum, out publicCert))
                    {
                        pdm.WriteActivityLogExtended("ProfileII.Services", null, refineryId, null, null, null, "Utilities",
                   "AuthenticateByCertificateAndClientId", null, null, null, "ERROR",  null, "Call to pcc.Get in AuthenticateByCertificateAndClientId, using name " + nameAndRefNum + " returned False.", null, null, null, null ,null);
                        //ProfileLogManager.LogInfo("Call to pcc.Get in AuthenticateByCertificateAndClientId, using name " + nameAndRefNum + " returned False.");
                        throw new Exception("There was a problem with your Certificate. Please contact Solomon.");
                    }
                    byte[] originalMessage = new byte[] { };
                    //byte[] encodedSignedCms = System.Text.Encoding.ASCII.GetBytes(clientKey);
                    ProfileLiteSecurity.Cryptography.NonRepudiation pnc = new ProfileLiteSecurity.Cryptography.NonRepudiation();
                    /*
                    //test
                    bool test = pnc.Sign(ref errorResponse, encodedSignedCms, publicCert, out originalMessage, true);
                    test = pnc.Verify(ref errorResponse, encodedSignedCms, publicCert, true, out originalMessage, false);
                    */

                    //end test


                    if (pnc.Sign(ref errorResponse,encodedSignedCms,publicCert,out originalMessage,true))
                    {
                        return pnc.Verify(ref errorResponse, encodedSignedCms, publicCert, true, out originalMessage, false);
                    }
                    else
                    { return false; }

                }
                return true;
            }
            catch(Exception ex)
            {
                if (ex.Message.StartsWith("There was a problem with your Certificate"))
                {
                    throw ex;
                }
                else if (ex.Message.StartsWith("Your key is invalid"))
                {
                    throw ex;
                }
                else
                {

                    ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                    pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
                    "AuthenticateByCertificateAndClientId", null, null, null, "ERROR", null, ExMessageExtended(ex), null, null, null, null, null);
                    //LogError("AuthenticateByCertificateAndClientId", ex.Message, (ex.InnerException == null ? "" : ex.InnerException.Message));
                    return false;
                }
            }
    }
    
        public static bool CheckAuthenticationAndAuthorizationTable(string refineryId ,
                                                             ref bool active ,
                                                             ref bool certificateRequired ,
                                                             ref bool clientKeyRequired,
                                                             ref string clientKeyFromDb )
        {
            try
            {
                string sql = "SELECT RefineryGroupId,RefineryId,Active,CertificateRequired,ClientKeyRequired,ClientKey" +
                " FROM dbo.AuthenticationAndAuthorization where RefineryId =@RefineryId";
                using (SqlConnection conx = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString()))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, conx))
                    {
                        try
                        {
                            cmd.Parameters.Add(new SqlParameter("@RefineryID", refineryId));
                            cmd.CommandType = System.Data.CommandType.Text;
                            conx.Open();
                            
                            SqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.Read(); //expect only 1 line
                                active = (bool)reader["Active"];
                                certificateRequired = (bool)reader["CertificateRequired"];
                                clientKeyRequired = (bool)reader["ClientKeyRequired"];
                                clientKeyFromDb = reader.GetString(5);
                                return true;
                            }
                            else
                            {
                                ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                                pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
                "CheckAuthenticationAndAuthorizationTable", null, null, null,  "ERROR", null, "No record for RefineryId " + refineryId + " found in dbo.AuthenticationAndAuthorization()", null, null, null, null ,null);
                              
                                //ProfileLogManager.LogInfo("No record for RefineryId " + refineryId + " found in dbo.AuthenticationAndAuthorization()");
                                return false;
                            }                            
                        }
                        catch(Exception ex)
                        {
                            ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                            pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
            "CheckAuthenticationAndAuthorizationTable", null, null, null, "ERROR", null, ExMessageExtended(ex), null, null, null, null,null);
                            //LogError("CheckAuthenticationAndAuthorizationTable", ex.Message, "reading record for RefineryId");
                            return false;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                ProfileDataManager pdm = new ProfileDataManager(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), string.Empty);
                pdm.WriteActivityLogExtended("ProfileII.Services", null, null, null, null, null, "Utilities",
"CheckAuthenticationAndAuthorizationTable", null, null, null, "ERROR", null, "unexpected error:" + ExMessageExtended(ex), null, null, null, null, null);
                //LogError("CheckAuthenticationAndAuthorizationTable", ex.Message, "unexpected error");
            }
            return false;
        }

        

        private static bool TranslateSqlServerBitTypeFromNumbersToBoolean(byte value )
        {
            switch(value)
            {
                case 0:
                    return false;
                    break;
                case 255:
                    return true;
                    break;
                default:
                    throw new Exception("Sql Server Bit datatype has value other than 0 or 255!");
            }
        }

        /*
        public static void WriteActivityLogExtended(string applicationName, string methodology, string CallerIP, string UserID,
            string ComputerName, string service, string Method, string EntityName, DateTime? PeriodStart, DateTime? PeriodEnd,
            string status, string version, string errorMessages, string localDomainName, string osVersion, string browserVersion,
            string officeVersion, string dataImportedFromBridgeFile, byte[] cert)
        {
            string refNum = string.Empty;
            string clientKey = string.Empty;
            ComputerName = "";
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
            try
            {
                string finalErrMsg = string.Empty;
                int errorNumber = 0;
                if (!GoodKey(ref refNum, ref clientKey))
                {
                    if (errorMessages.Length > 1)
                    { errorMessages = "Invalid client key | and " + errorMessages; }
                    else
                    { errorMessages = "Invalid client key."; }
                    errorNumber = 1;
                    finalErrMsg = errorMessages;
                }
                if (!GoodCert(clientKey, cert))
                {
                    if (errorMessages.Length > 1)
                    { errorMessages = "Invalid Certificate for client key: " + clientKey + " | and " + errorMessages; }
                    else
                    { errorMessages = "Invalid Certificate for client key: " + clientKey; }
                    errorNumber = 2;
                    finalErrMsg = errorMessages;
                }
                if (CallerIP != null)
                    CallerIP = GetUserHostAddress();

                svc.WriteActivityLogExtended(applicationName, methodology, refNum, CallerIP, UserID, ComputerName, service, Method, EntityName, PeriodStart, PeriodEnd, status, version, errorMessages, localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile);
                if (errorNumber == 1)
                { throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName()); }
                else if (errorNumber == 2)
                { throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName()); }

                versionCheck(version); //pops error if not using supported version
            }
            catch (Exception ex)
            {
                try
                {
                    svc.WriteActivityLogExtended(applicationName, methodology, refNum, CallerIP, UserID, ComputerName, service, "WriteActivityLogExtended", EntityName, PeriodStart, PeriodEnd, "ERROR", version, ex.Message, localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile);
                }
                catch { Exception ex2; } //do nothing
            }
        }
        */
        /*
        private static void LogError(string methodName, string errorMessage, string otherInfo = "")
        {
            try
            {
                string msg = "Error in " + methodName + ", ";
                if (otherInfo.Length > 0)
                {
                    msg += otherInfo + ", ";
                }
                msg += errorMessage;
                ProfileLogManager.LogException(new Exception(msg));
            }
            catch { }

        }
        */
 }


    
}
