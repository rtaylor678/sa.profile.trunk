﻿Imports System.String
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Configuration




Public Class ActivityLog
    Private db As DBHelper
    Public Sub New(db12Conx As String)
        db = New DBHelper(db12Conx)
    End Sub


    Friend Sub LogExtended(ApplicationName As String, methodology As String, refineryID As String, CallerIP As String,
                   UserID As String, ComputerName As String, service As String,
                   Method As String, EntityName As String, PeriodStart As Date?, PeriodEnd As Date?,
                   status As String,
                   version As String, errorMessages As String, localDomainName As String,
                   osVersion As String, browserVersion As String, officeVersion As String,
                   dataImportedFromBridgeFile As String)

        Try
            '    <add key="LogToActivityLog" value="F"/>
            Select Case ConfigurationManager.AppSettings("LogToActivityLog").ToUpper()
                Case "T", "TRUE", "Y", "YES"
                    'continue
                Case Else
                    Exit Sub
            End Select
        Catch
        End Try

        Try

            'Dim methodology As String = String.Empty 'not needed per Richard but is needed per proc
            browserVersion = String.Empty 'this is for future use
            Dim notes As String = String.Empty 'Richard doesn't sseem to want to use this
            service = String.Empty 'will be using the App argument instead for this App

            db.RefineryID = refineryID  'Needed for Is2012() 
            db.QueryDb("spLogActivityExtended '" & db.CleanText(ApplicationName) & "','" & db.CleanText(methodology) & "','" &
                   db.CleanText(refineryID) & "','" & db.CleanText(CallerIP) & "','" & db.CleanText(UserID) & "','" &
                   db.CleanText(ComputerName) & "', '" & db.CleanText(service) & "','" & db.CleanText(Method) & "','" &
                   db.CleanText(EntityName) & "','" & PeriodStart & "','" & PeriodEnd & "','" &
                   db.CleanText(notes) & "','" & db.CleanText(status) & "', '" & db.CleanText(version) &
                    "', '" & db.CleanText(errorMessages) & "', '" & db.CleanText(localDomainName) &
                      "', '" & db.CleanText(osVersion) & "', '" & db.CleanText(browserVersion) &
                        "', '" & db.CleanText(officeVersion) & "', '" & db.CleanText(dataImportedFromBridgeFile) & "'")
            db.RefineryID = Nothing


        Catch ex As Exception
            Dim debugMessage As String = ex.Message
        End Try
    End Sub


End Class
