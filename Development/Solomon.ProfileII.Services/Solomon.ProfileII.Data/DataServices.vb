﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Configuration


Public Class DataServices

    Private db As DBHelper
    Dim ActivityLog As ActivityLog
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection

    Private _db12Conx As String = String.Empty

    Public Sub New(db12Conx As String)
        _db12Conx = db12Conx
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlConnection1.ConnectionString = _db12Conx
        db = New DBHelper(_db12Conx)
        ActivityLog = New ActivityLog(_db12Conx)

    End Sub

    Public Function CheckService(userHostAddress As String) As String
        Try 'running test here

            GetLookups("XXPAC", "EXAMPLE", userHostAddress)
        Catch ex As Exception
            Return "Failed"
        End Try
        Return "Success"
    End Function

    '"Method used to determine if refinery is upgraded to 2012 ")> _
    Public Function RefineryIs2012(RefineryID As String) As Boolean

        Dim Is2012 As String = False
    Dim cn As New SqlConnection()
        cn.ConnectionString = Me.SqlConnection1.ConnectionString

        Dim ds As New DataSet

    Dim sqlstmt As String = "GetRefinery '" & RefineryID & "'"

    Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, cn)
        da.Fill(ds)
        If Not IsDBNull(ds) Then
            Is2012 = ds.Tables(0).Rows(0)(0).ToString
        End If

        cn.Close()
        If Is2012 = "0" Then
    Return False
    End If

    Return True
    End Function

    Public Function GetLookups(refineryID As String, companyID As String, UserHostAddress As String) As DataSet
        'This gets written at client as Reference.xml
        Dim dsLookups As New DataSet
        Dim fuelsLubeCombo As Boolean
        Dim Q As String = "Enter GetLookups"
        db.RefineryID = refineryID
        Try
            Dim dsFLCombo As DataSet = db.QueryDb("EXEC DS_FLCombo '" & refineryID & "'")
            fuelsLubeCombo = dsFLCombo.Tables(0).Rows(0)(0)
            Q = "DS_FLCombo"

            'Get Currency_LU
            Dim sqlstmt As String = " EXEC DS_Currency_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(0).TableName = "Currency_LU"
            Q = "DS_Currency_LU"
            'Get UnitTargets_LU
            sqlstmt = "EXEC DS_UnitTargets_LU '" & companyID & "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(1).TableName = "UnitTargets_LU"
            Q = "DS_UnitTargets_LU"
            'Get Table2_LU
            sqlstmt = " EXEC DS_Table2_LU '" & companyID & "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(2).TableName = "Table2_LU"
            Q = "DS_Table2_LU"
            'Get Energy_LU
            sqlstmt = "EXEC DS_Energy_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(3).TableName = "Energy_LU"
            Q = "DS_Energy_LU"
            'Get Crude_LU
            sqlstmt = "EXEC DS_Crude_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(4).TableName = "Crude_LU"
            Q = "DS_Crude_LU"
            'Get  MaterialCategory_LU
            sqlstmt = "EXEC DS_MaterialCategory_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(5).TableName = "MaterialCategory_LU"
            Q = "DS_MaterialCategory_LU"
            'Get Material_LU
            sqlstmt = "EXEC GetMaterialLU '" & refineryID & "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(6).TableName = "Material_LU"
            Q = "GetMaterialLU"
            'Get ProcessGroup_LU
            If fuelsLubeCombo Then
                sqlstmt = "EXEC DS_ProcessGroupFC_LU"
            Else
                sqlstmt = "EXEC DS_ProcessGroup_LU"
            End If
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(7).TableName = "ProcessGroup_LU"
            Q = "DS_ProcessGroupFC_LU"

            'Get ProcessID_LU
            If fuelsLubeCombo Then
                sqlstmt = "EXEC DS_ProcessIDFC_LU"
            Else
                sqlstmt = "EXEC DS_ProcessID_LU"
            End If
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(8).TableName = "ProcessID_LU"
            Q = "DS_ProcessIDFC_LU"

            'Get ProcessType_LU
            sqlstmt = "EXEC DS_ProcessType_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(9).TableName = "ProcessType_LU"
            Q = "DS_ProcessType_LU"
            'Get TankType_LU
            sqlstmt = "EXEC DS_TankType_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(10).TableName = "TankType_LU"
            Q = "DS_TankType_LU"
            'Get Pers_LU
            sqlstmt = "EXEC DS_Pers_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(11).TableName = "Pers_LU"
            Q = "DS_Pers_LU"
            'Get Absence_LU
            sqlstmt = "EXEC DS_Absence_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(12).TableName = "Absence_LU"
            Q = "DS_Absence_LU"

            'Get Chart_LU
            sqlstmt = "EXEC profile.spGetChart_LU '" & companyID & "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(13).TableName = "Chart_LU"
            Q = "DS_Chart_LU"

            'Get Report_LU
            'should edit the proc to add new Custom field
            sqlstmt = "EXEC profile.spGetReport_LU '" & companyID & "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(14).TableName = "Report_LU"
            Q = "DS_Report_LU"

            'Get Methodology
            sqlstmt = "EXEC DS_Methodology"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(15).TableName = "Methodology"
            Q = "DS_Methodology"

            'Get Opex_LU
            sqlstmt = "EXEC DS_Opex_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(16).TableName = "Opex_LU"
            Q = "DS_Opex_LU"

            'get Targeting choices
            sqlstmt = "EXEC profile.spGetTargetingPeerGroups '" & refineryID & "'"
            Dim dsAllPeers As DataSet = db.QueryDb(sqlstmt)
            dsLookups.Tables.Add(dsAllPeers.Tables(0).Copy)
            dsLookups.Tables(17).TableName = "TargetingPeerGroups"
            Q = "TargetingPeerGroups"

            ''Get  "TargetingRegionCodes"
            'sqlstmt = "EXEC profile.spGetTargetingRegionCodes"
            'dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            'dsLookups.Tables(18).TableName = "TargetingRegionCodes"
            'Q = "TargetingRegionCodes"


            'Get Report Layout
            sqlstmt = "EXEC profile.spGetReportLayout_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(18).TableName = "ReportLayout_LU"
            Q = "ReportLayout_LU"

            ActivityLog.LogExtended("ProfileII", "", refineryID, UserHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetLookups", Q, Nothing, Nothing, "Success", My.Application.Info.Version.ToString(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Return dsLookups

        Catch Ex As Exception
            ActivityLog.LogExtended("ProfileII", "", refineryID, UserHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetLookups", Q, Nothing, Nothing, "Error", My.Application.Info.Version.ToString(), Ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Return Nothing
        End Try

    End Function

    Public Function GetReferences(refineryID As String, userHostAddress As String) As DataSet
        'This gets written at client app but ## NOT ## as Reference.xml

        Dim dsReferences As New DataSet
        db.RefineryID = refineryID
        Dim errorAfter As String = "Start"
        Try
            'Get UnitList
            Dim sqlstmt As String = "EXEC DS_UnitList '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(0).TableName = "UnitList"

            errorAfter = "UnitList"
            'Get Config
            sqlstmt = "EXEC DS_Config '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(1).TableName = "Config"

            errorAfter = "Config"
            'Get MaintTA_Process
            sqlstmt = "EXEC DS_MaintTA_Process '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(2).TableName = "MaintTA_Process"

            errorAfter = "MaintTA_Process"
            'Get MaintTA_Other
            sqlstmt = "EXEC DS_MaintTA_Other '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(3).TableName = "MaintTA_Other"

            errorAfter = "MaintTA_Other"
            'Get LoadedMonths
            sqlstmt = "EXEC DS_LoadMonths '" & refineryID & "'"
            Dim tempDS As DataSet = db.QueryDb(sqlstmt)
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(4).TableName = "LoadedMonths"

            errorAfter = "LoadedMonths"
            'Get Inventory Schema
            sqlstmt = "EXEC DS_InventorySchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(5).TableName = "Inventory"

            errorAfter = "Inventory"
            'Get ConfigRS Schema
            sqlstmt = "EXEC DS_ConfigRSSchema "
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(6).TableName = "ConfigRS"

            errorAfter = "ConfigRS"
            'Get ProcessData Schema
            sqlstmt = "EXEC DS_ProcessDataSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(7).TableName = "ProcessData"

            errorAfter = "ProcessData"
            'Get MaintRout Schema
            sqlstmt = "EXEC DS_MaintRoutSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(8).TableName = "MaintRout"

            errorAfter = "MaintRout"
            'Get OpEx Schema
            sqlstmt = "EXEC DS_OpexSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(9).TableName = "OpEx"

            errorAfter = "OpEx"
            'Get OpexAdd Schema
            sqlstmt = "EXEC DS_OpexAddSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(10).TableName = "OpexAdd"

            errorAfter = "OpexAdd"
            'Get OpexAll
            sqlstmt = "EXEC DS_OpexAllSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(11).TableName = "OpexAll"

            errorAfter = "OpexAll"
            'Get Pers Schema
            sqlstmt = "EXEC DS_PersSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(12).TableName = "Pers"

            errorAfter = "Pers"
            'Get Absence Schema
            sqlstmt = "EXEC DS_AbsenceSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(13).TableName = "Absence"

            errorAfter = "Absence"
            'Get Crude Schema
            sqlstmt = "EXEC DS_CrudeSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(14).TableName = "Crude"

            errorAfter = "Crude"
            'Get Yield_RM Schema
            sqlstmt = "EXEC DS_YieldRMSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(15).TableName = "Yield_RM"

            errorAfter = "Yield_RM"
            'Get Yield_RMB Schema
            sqlstmt = "EXEC DS_YieldRMBSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(16).TableName = "Yield_RMB"

            errorAfter = "Yield_RMB"
            'Get Yield_Prod Schema
            sqlstmt = "EXEC DS_YieldProdSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(17).TableName = "Yield_Prod"

            errorAfter = "Yield_Prod"
            'Get Energy Schema
            sqlstmt = "EXEC DS_EnergySchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(18).TableName = "Energy"

            errorAfter = "Energy"
            'Get MaintRoutHist Schema
            sqlstmt = "EXEC GetMaintRoutHist '" & refineryID & "', 'ACTUAL'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(19).TableName = "MaintRoutHist"

            errorAfter = "MaintRoutHist"
            'Get Settings Schema

            sqlstmt = "EXEC DS_SettingsSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(20).TableName = "Settings"

            errorAfter = "Settings"
            'Get Electric Schema
            sqlstmt = "EXEC DS_ElectricSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(21).TableName = "Electric"

            errorAfter = "Electric"
            'Get RefTargets Schema
            sqlstmt = "EXEC DS_RefTargetsSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(22).TableName = "RefTargets"

            errorAfter = "RefTargets"
            'Get UnitTargets Schema
            sqlstmt = "EXEC DS_UnitTargetsSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(23).TableName = "UnitTargets"

            errorAfter = "UnitTargets"
            'Get UserDefined Schema
            sqlstmt = "EXEC DS_UserDefinedSchema '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(24).TableName = "UserDefined"

            errorAfter = "UserDefined"
            'Get UnitTargetsNew
            sqlstmt = "EXEC DS_UnitTargetsNew '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(25).TableName = "UnitTargetsNew"

            errorAfter = "UnitTargetsNew"
            'Get LoadEDCStabilizers
            sqlstmt = "EXEC DS_LoadEDCStabilizers '" & refineryID & "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(26).TableName = "EDCStabilizers"
            errorAfter = "EdcStabilizers"

            ''already seems to be in Reference.xml somehow...
            'sqlstmt = "SELECT ChartTitle, sectionheader, SortKey  FROM [ProfileFuels12].[dbo].[Chart_LU] where SectionHeader in " &
            '    " ('Average Data','By Process Unit','Energy','Maintenance','Operating Expenses','Performance Indicators','Personnel','Yields and Margins') " &
            '    " order by sortkey asc"
            'dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            'dsReferences.Tables(26).TableName = "Chart_LU"
            'errorAfter = "Chart_LU"


            'ActivityLog.Log("",           refineryID, UserHostAddress, ""    , Environment.MachineName.ToString(), "DataServices", "GetLookups", ""        , ""         , ""       , ""   , "Success")
            ''Friend Sub Log(Methodology , refineryID, CallerIP       , UserID, ComputerName                      , Service       , Method      , EntityName, PeriodStart, PeriodEnd, Notes, status    )
            ActivityLog.LogExtended("ProfileII", "", refineryID, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetReferences", errorAfter, Nothing, Nothing, "Success", My.Application.Info.Version.ToString(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Return dsReferences
        Catch ex As Exception
            ActivityLog.LogExtended("ProfileII", "", refineryID, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetReferences", errorAfter, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Return Nothing
        End Try
    End Function

    Public Function GetDataDump(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, ByVal currency As String,
                                ByVal startDate As Date, ByVal UOM As String, ByVal studyYear As Integer, ByVal includeTarget As Boolean,
                                ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, refineryID As String,
                                userHostAddress As String) As DataSet

        Dim DataDump As DataSet = Nothing
        Dim ReportName As String = Nothing
        Try
            db.RefineryID = refineryID
            Dim dsResp As DataSet = db.QueryDb("EXEC DS_GetDataDump '" & ReportCode & "'")
            If dsResp.Tables(0).Rows.Count > 0 Then
                ReportName = dsResp.Tables(0).Rows(0)("ReportName")
                Dim ddmpMgr As New DataDumpManager(_db12Conx)
                DataDump = ddmpMgr.GetDataDump(ReportCode.ToUpper.Trim, ReportName, refineryID, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG)
            End If
            ActivityLog.LogExtended("ProfileII", "", refineryID, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetDataDump", String.Empty, Nothing, Nothing, "Success", My.Application.Info.Version.ToString(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Return DataDump
        Catch ex As Exception
            ActivityLog.LogExtended("ProfileII", "", refineryID, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetDataDump", String.Empty, Nothing, Nothing, "Error", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Return Nothing
        End Try
    End Function

    Private Function ValidateKey(clientKey As String, ByRef refNum As String) As Boolean
        'done at Svc level
        Return True

        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(clientKey) Then
                company = Decrypt(clientKey).Split("$".ToCharArray)(0)

                locIndex = Decrypt(clientKey).Split("$".ToCharArray).Length - 2
                location = Decrypt(clientKey).Split("$".ToCharArray)(locIndex)
                Dim array As String() = Decrypt(clientKey).Split("$".ToCharArray)
                refNum = array(array.Length - 1) 'last position
                '_refNum = refNum

                Dim clientKeysFolder As String = String.Empty
                Try
                    clientKeysFolder = ConfigurationManager.AppSettings("ClientKeysFolder").ToString()
                Catch
                End Try

                If clientKeysFolder.Length > 0 Then
                    If File.Exists(clientKeysFolder & company & "_" & location & ".key") Then
                        path = clientKeysFolder & company & "_" & location & ".key"
                    End If
                Else
                    If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                        path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                    ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                        path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                    Else
                        Return False
                    End If
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = clientKey.Trim)
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub WriteActivityLogExtended(ApplicationName As String, methodology As String, refineryID As String, CallerIP As String,
                   UserID As String, ComputerName As String, service As String,
                   Method As String, EntityName As String, PeriodStart As Date?, PeriodEnd As Date?,
                   status As String,
                   version As String, errorMessages As String, localDomainName As String,
                   osVersion As String, browserVersion As String, officeVersion As String,
                   dataImportedFromBridgeFile As String)

        'done at higher level:
        'Dim refineryID As String = String.Empty
        'If Not ValidateKey(clientKey, refineryID) Then
        '    Throw New Exception("Invalid key")
        'End If

        browserVersion = String.Empty 'this is for future use
        Dim notes As String = String.Empty 'Richard doesn't sseem to want to use this
        service = String.Empty 'will be using the App argument instead for this App

        db.RefineryID = refineryID  'Needed for Is2012() 
        db.QueryDb("spLogActivityExtended '" & db.CleanText(ApplicationName) & "','" & db.CleanText(methodology) & "','" &
                   db.CleanText(refineryID) & "','" & db.CleanText(CallerIP) & "','" & db.CleanText(UserID) & "','" &
                   db.CleanText(ComputerName) & "', '" & db.CleanText(service) & "','" & db.CleanText(Method) & "','" &
                   db.CleanText(EntityName) & "','" & PeriodStart & "','" & PeriodEnd & "','" &
                   db.CleanText(notes) & "','" & db.CleanText(status) & "', '" & db.CleanText(version) &
                    "', '" & db.CleanText(errorMessages) & "', '" & db.CleanText(localDomainName) &
                      "', '" & db.CleanText(osVersion) & "', '" & db.CleanText(browserVersion) &
                        "', '" & db.CleanText(officeVersion) & "', '" & db.CleanText(dataImportedFromBridgeFile) & "'")
        db.RefineryID = Nothing
    End Sub

    'Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    '    'CODEGEN: This procedure is required by the Web Services Designer
    '    'Do not modify it using the code editor.
    '    If disposing Then
    '        If Not (components Is Nothing) Then
    '            components.Dispose()
    '        End If
    '    End If
    '    MyBase.Dispose(disposing)

    'End Sub
End Class
