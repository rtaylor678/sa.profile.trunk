﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.ProfileII.Services.Tests
{
    [TestClass]
    public class ChartsTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            //ProfileChartsManager mgr = new ProfileChartsManager("XXPAC");
            //var z = mgr.
            //var z = mgr.GetRadarChartHtml("cn=&sd=6/1/2015&ed=6/1/2016&UOM=Metric&currency=SGD&sn=2014&chart=RADAR||Black|Bisque|Blue&PeerGroups=15100&rankVariables=2071|462|504|480|478|481");
            //Assert.IsTrue(z == null);

            List<double> list1 = new List<double> { 150, 75, 0, -75 };
            List<double> results = QuartileAvgMoYtdZeroTo100(list1);
            Assert.IsTrue(results[0] < 101);
            Assert.IsTrue(results[1] < 67);
            Assert.IsTrue(results[2] < 34);
            Assert.IsTrue(results[3] < 1);

            List<double> list2 = new List<double> {-75,0,75,150};
            results = QuartileAvgMoYtdZeroTo100(list2);
            Assert.IsTrue(results[3] < 101);
            Assert.IsTrue(results[2] < 67);
            Assert.IsTrue(results[1] < 34);
            Assert.IsTrue(results[0] < 1);

            List<double> list3 = new List<double> { 63,77,90,74};
            results = QuartileAvgMoYtdZeroTo100(list3);
        }

        public List<double> QuartileAvgMoYtdZeroTo100(List<double> listIn)
        {
            List<double> listOut = new List<double>();
            int listCount = listIn.Count;
            //make temp list to sort high to low
            List<double> sorts = new List<double>();
            foreach (double d in listIn)
            {
                sorts.Add(d);
            }
            sorts.Sort();  //sorts low to high
            //calc spread between low and high
            double spread =  sorts[sorts.Count - 1] - sorts[0];
            //if low < 0, 
            double bumpUp = 0;
            if (sorts[0] < 0)
            {

                bumpUp = 0 - sorts[0];
            }
            else
            {
                bumpUp = 0 - sorts[0];
            }
            foreach (double d in listIn)
            {
                //if low < 0,
                //-make new list and add a # to each in list so low is now 0
                //divide each # in list by the spread, add to listOut
                listOut.Add(((d + bumpUp) / spread)*100);
            }
            return listOut;
        }
    }
}
