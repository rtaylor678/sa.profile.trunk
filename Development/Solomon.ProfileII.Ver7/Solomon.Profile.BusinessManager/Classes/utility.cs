﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfileBusinessManager.Classes
{
    public class Utility
    {
        public Single StringToSingle(string inputString)
        {
            try
            {
                if (inputString != string.Empty)
                {
                    Single beforeValue = Convert.ToSingle(inputString);
                    Single retValue = 0;

                    int stringLength = inputString.Length;

                    switch (stringLength)
                    {
                        case 9:
                            retValue = beforeValue / 10000;
                            break;
                        case 8:
                            retValue = beforeValue / 1000;
                            break;
                        case 7:
                            retValue = beforeValue / 100;
                            break;
                        case 6:
                            retValue = beforeValue / 10;
                            break;
                        default:
                            break;
                    }

                    return retValue;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }
    }
}
