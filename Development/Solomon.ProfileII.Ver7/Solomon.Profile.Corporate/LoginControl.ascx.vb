Public Class LoginControl
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Submit1 As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents ltStatus As System.Web.UI.WebControls.Literal
    Protected WithEvents txtUsername As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents txtPassword As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents UserValidator As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents PasswordValidator As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		'Put user code to initialize the page here
	End Sub

	Protected Sub frmSignInSignIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		Dim auth As New Authentication
		Dim secData As SecurityData
		Dim txtUName As String = Request.Form("frmSignInUName")
		Dim txtPWord As String = Request.Form("frmSignInPWord")

		If auth.Authenticate(txtUName, txtPWord) Then
			secData = auth.GetUserDataByLogin(txtUName)
			Session("AppUser") = secData.CompanyName
			Session("CompID") = secData.CompanyID
			Response.Redirect("Main.aspx")
		Else
			ltStatus.Text = "<font size=1px style=""COLOR:RED""> Login failed. Please try again. </font>"
		End If
	End Sub

End Class
