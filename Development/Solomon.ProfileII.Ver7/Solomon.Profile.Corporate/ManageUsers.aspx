﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="ManageUsers.aspx.vb" Inherits="Solomon.Profile.Corporate.ManageUsers" %>
<%@ Register TagPrefix="mn1" TagName="ManageUsersControl" Src="ManageUsersControl.ascx" %>

<asp:Content ID="ManageUsers_head" runat="server" ContentPlaceHolderID="PH_Head">
</asp:Content>

<asp:Content ID="ManageUsers_Sidebar" runat="server" ContentPlaceHolderID="PH_Sidebar">
    <asp:PlaceHolder ID="PlaceHolder5" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="ManageUsers_body" runat="server" ContentPlaceHolderID="PH_Work">

    <mn1:ManageUsersControl id="mnControl1" runat="server"></mn1:ManageUsersControl>

    <script type="text/javascript">
        $(function () {
            $('#summary').html('Manage Users');
            $('.navigationStandard,.navigationHome').show();
            //settings menu highlight active item
            $('.settingsNav li').removeClass('settingsActive');
            $('.settingsNav li:contains("Manage Users")').addClass('settingsActive');
        });
        Cookies.set('solomon_test', 'solomon_test', { expires: 1 });
        var cookieVal = Cookies.get('solomon_test');

        if (cookieVal == null) {
            document.writeln("<center><span style=\"COLOR:RED;\">Cookies must be enabled to use this site.</span></center>");
        }
        else {
            Cookies.remove('solomon_test');
        }
    </script>
</asp:Content>
