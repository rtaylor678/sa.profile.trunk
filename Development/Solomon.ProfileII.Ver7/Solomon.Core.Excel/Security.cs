﻿using System;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	public partial class XL
	{
		public static void ProtectWorkbook(Excel.Workbook wkb, string password)
		{
			wkb.Protect(password);
		}

		public static void ProtectWorksheet(Excel.Worksheet wks, string password)
		{
			wks.Protect(password);
		}

		public static void ProtectWorkbookAll(Excel.Workbook wkb, string password)
		{
			foreach (Excel.Worksheet wks in wkb.Worksheets)
			{
				ProtectWorksheet(wks, password);
			}

			ProtectWorkbook(wkb, password);
		}

		public static void UnProtectWorkbook(Excel.Workbook wkb, string password)
		{
			wkb.Unprotect(password);
		}

		public static void UnProtectWorksheet(Excel.Worksheet wks, string password)
		{
			wks.Unprotect(password);
		}

		public static void UnProtectWorkbookAll(Excel.Workbook wkb, string password)
		{
			UnProtectWorkbook(wkb, password);

			foreach (Excel.Worksheet wks in wkb.Worksheets)
			{
				UnProtectWorksheet(wks, password);
			}
		}

		[Obsolete("Process takes too long due to Try-Catch", true)]
		public static bool WorksheetPasswordCracked(Excel.Worksheet wks, out string password)
		{
			if (wks.ProtectContents || wks.ProtectDrawingObjects || wks.ProtectScenarios)
			{
				const int cBeg = 65;
				const int cEnd = 66;

				const int zBeg = 32;
				const int zEnd = 126;

				string pwd;

				Excel.Application xla = wks.Parent.Parent;

				bool inter = xla.DisplayAlerts;

				xla.DisplayAlerts = false;
				xla.EnableEvents = false;
				xla.Interactive = false;
				xla.ScreenUpdating = false;

				for (int i00 = cBeg; i00 <= cEnd; i00++)
				{
					for (int i01 = cBeg; i01 <= cEnd; i01++)
					{
						for (int i02 = cBeg; i02 <= cEnd; i02++)
						{
							for (int i03 = cBeg; i03 <= cEnd; i03++)
							{
								for (int i04 = cBeg; i04 <= cEnd; i04++)
								{
									for (int i05 = cBeg; i05 <= cEnd; i05++)
									{
										for (int i06 = cBeg; i06 <= cEnd; i06++)
										{
											for (int i07 = cBeg; i07 <= cEnd; i07++)
											{
												for (int i08 = cBeg; i08 <= cEnd; i08++)
												{
													for (int i09 = cBeg; i09 <= cEnd; i09++)
													{
														for (int i10 = cBeg; i10 <= cEnd; i10++)
														{
															for (int i11 = zBeg; i11 <= zEnd; i11++)
															{
																pwd = ((char)i00).ToString() +
																		((char)i01).ToString() +
																		((char)i02).ToString() +
																		((char)i03).ToString() +
																		((char)i04).ToString() +
																		((char)i05).ToString() +
																		((char)i06).ToString() +
																		((char)i07).ToString() +
																		((char)i08).ToString() +
																		((char)i09).ToString() +
																		((char)i10).ToString() +
																		((char)i11).ToString();

																try
																{
																	wks.Unprotect(pwd);

																	if (wks.ProtectContents || wks.ProtectDrawingObjects || wks.ProtectScenarios)
																	{
																	}
																	else
																	{
																		xla.DisplayAlerts = inter;
																		xla.EnableEvents = inter;
																		xla.Interactive = inter;
																		xla.ScreenUpdating = inter;

																		password = pwd;
																		return false;
																	}
																}
																catch(System.Runtime.InteropServices.COMException)
																{
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}

				xla.DisplayAlerts = inter;
				xla.EnableEvents = inter;
				xla.Interactive = inter;
				xla.ScreenUpdating = inter;

				password = string.Empty;
				return true;
			}
			else
			{
				password = string.Empty;
				return false;
			}
		}
	}
}