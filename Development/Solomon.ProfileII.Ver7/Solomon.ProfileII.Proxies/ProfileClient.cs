﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Solomon.ProfileII.Contracts;
using System.Data;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.Serialization.Formatters.Binary;
using ProfileLiteSecurity;

namespace Solomon.ProfileII.Proxies
{
    public class ProfileClient : ClientBase<IProfileII>, IProfileII  //  ClientBase<IProfileReportService>, IProfileReportService  // IProfileChartService
    {
        private string _clientKeyFolder = AppDomain.CurrentDomain.BaseDirectory + @"_CERT\";// @"C:\tfs\CPA\Sa.Profile.Trunk\Development\Solomon.ProfileII.201607\Solomon.Profile\bin\Debug\_CERT";
        private byte[] _encodedSignedCms = null;
        private string _resource = "Solomon.ProfileII.Proxies.ProfileLiteSecurity.dll";
        private string _resourceName = "ProfileLiteSecurity.dll";
        //private ProfileLiteSecurity  _profileLiteSecurity = null;

        public ProfileClient(string endpointName)
            : base(endpointName)
        {
            try
            {
                if (EmbeddedAssembly.Load(_resource, _resourceName))
                    AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading ProfileLiteSecurity");
            }
            if (!GetEncodedSignedCms(GetClientKey(), ref _encodedSignedCms))
            {
                _encodedSignedCms = null; //prevent from sending to server, so server should reject.
                throw new Exception("Certificate error in ProfileClient 1.");
            }
        }
        public ProfileClient(Binding binding, EndpointAddress address)
            : base(binding, address)
        {
            try
            {
                if (EmbeddedAssembly.Load(_resource, _resourceName))
                    AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading ProfileLiteSecurity");
            }

            if (!GetEncodedSignedCms(GetClientKey(), ref _encodedSignedCms))
            {
                _encodedSignedCms = null; //prevent from sending to server, so server should reject.
                throw new Exception("Certificate error in ProfileClient 2.");
            }
        }
        public string GetReportHtml(string queryString, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                // now make the WCF call within this using block
                return Channel.GetReportHtml(queryString, _encodedSignedCms);
            }
        }

        public string GetRefineryScorecardReport(string queryString, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                // now make the WCF call within this using block
                return Channel.GetRefineryScorecardReport(queryString, _encodedSignedCms);
            }
        }

        public string GetRefineryTrendsReport(string queryString, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                // now make the WCF call within this using block
                return Channel.GetRefineryTrendsReport(queryString, _encodedSignedCms);
            }
        }

        public string GetTargetingReport(string queryString, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                // now make the WCF call within this using block
                return Channel.GetTargetingReport(queryString, _encodedSignedCms);
            }
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return EmbeddedAssembly.Get(args.Name);
        }

        public string CheckService(string version, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                /*
                MessageHeader<string> header = new MessageHeader<string>("WsP");
                var untyped = header.GetUntypedHeader("WsP", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(untyped);
                */

                //MessageHeader certHeader = MessageHeader.CreateHeader("CsP", "", certBytes);
                //OperationContext.Current.OutgoingMessageHeaders.Add(certHeader);


                //trying to serialize before send to avoid error Base64 sequence length (3) not valid. Must be a multiple of 4

                //MessageHeader<byte[]> certHeader = new MessageHeader<byte[]>(ToByteArray(_encodedSignedCms));
                /*
                MessageHeader<byte[]> certHeader = new MessageHeader<byte[]>(_encodedSignedCms);
                var untypedCert = header.GetUntypedHeader("CsP", string.Empty);
                OperationContext.Current.OutgoingMessageHeaders.Add(untypedCert);
                */

                return Channel.CheckService(version, _encodedSignedCms);
            }

        }

        public byte[] DownloadTplFile(string fileinfo, string appVersion, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                return Channel.DownloadTplFile(fileinfo, appVersion, _encodedSignedCms);
            }
        }

        public DataSet GetDataByPeriod(DateTime periodStart, DateTime periodEnd, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                return Channel.GetDataByPeriod(periodStart, periodEnd, _encodedSignedCms);
            }
        }

        public DataSet GetDataDump(string ReportCode, string ds, string scenario, string currency, DateTime startDate, string UOM, int studyYear, bool includeTarget, bool includeYTD, bool includeAVG, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                return Channel.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG, _encodedSignedCms);
            }
        }

        public DataSet GetInputData(byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                return Channel.GetInputData(_encodedSignedCms);
            }
        }

        public DataSet GetLookups(string companyID, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                return Channel.GetLookups(companyID, _encodedSignedCms);
            }
        }

        public DataSet GetReferences(byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                return Channel.GetReferences(_encodedSignedCms);
            }
        }



        //public bool RefineryIs2012(string RefineryID)
        //{
        //    using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
        //    {
        //        MessageHeader<string> header = new MessageHeader<string>("WsP");
        //        var untyped = header.GetUntypedHeader("WsP", GetClientKey()); 
        //        OperationContext.Current.OutgoingMessageHeaders.Add(untyped);
        //        return Channel.RefineryIs2012(RefineryID);
        //    }
        //}

        public void SubmitRefineryData(DataSet ds, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                Channel.SubmitRefineryData(ds, _encodedSignedCms);
            }
        }

        public string UploadFile(string fileName, byte[] bytes, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                return Channel.UploadFile(fileName, bytes, _encodedSignedCms);
            }
        }

        public void WriteActivityLogExtended(string applicationName, string methodology, string CallerIP,
                   string UserID, string ComputerName, string service,
                   string Method, string EntityName, DateTime? PeriodStart, DateTime? PeriodEnd,
                   string status,
                   string version, string errorMessages, string localDomainName,
                   string osVersion, string browserVersion, string officeVersion,
                   string dataImportedFromBridgeFile, byte[] cert)
        {
            using (OperationContextScope scope = new OperationContextScope((IContextChannel)Channel))
            {
                MessageHeader clientKeyHeader = MessageHeader.CreateHeader("WsP", "", GetClientKey());
                OperationContext.Current.OutgoingMessageHeaders.Add(clientKeyHeader);
                Channel.WriteActivityLogExtended(applicationName, methodology, CallerIP, UserID, ComputerName, service,
                    Method, EntityName, PeriodStart, PeriodEnd, status,
                    version, errorMessages, localDomainName,
                    osVersion, browserVersion, officeVersion,
                    dataImportedFromBridgeFile, _encodedSignedCms);
            }
        }

        //public IEnumerable<ChartData> GetUnits(string queryString)
        //{
        //    return Channel.GetUnits(queryString);
        //}


        //public string GetCommonChartHtml(string queryString)
        //{
        //    return Channel.GetCommonChartHtml(queryString);
        //}

        //public string GetRadarChartHtml(string queryString)
        //{
        //    throw new NotImplementedException();
        //}

        //public string GetUnitsCommonChartHtml(string queryString)
        //{
        //    throw new NotImplementedException();
        //}



        private string GetClientKey()
        {
            string result = string.Empty;
            try
            {
                DirectoryInfo folder = new DirectoryInfo(_clientKeyFolder);
                using (StreamReader reader = new StreamReader(folder.GetFiles("*.ct")[0].FullName))
                {
                    result = reader.ReadLine().Trim();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error in ProfileClient.GetClientKey(): " + ex.Message);
            }
            result = result ?? string.Empty;
            if (result.Length < 10)
            {
                throw new Exception("Error in ProfileClient.GetClientKey(): Key too short: " + result);
            }
            return result;
        }
        
        private bool GetEncodedSignedCms(string clientKey, ref byte[] encodedSignedCms)
        {
            int errorCode = 1500;
            string applicationName = String.Empty;
            string methodology = String.Empty;
            string localRefineryID = String.Empty;
            string CallerIP = String.Empty;
            string userid = String.Empty;
            string computername = String.Empty;
            string version = String.Empty;
            System.Exception err = null;
            string errorMessages = String.Empty;
            string localDomainName = String.Empty;
            string osVersion = String.Empty;
            string officeVersion = String.Empty;
            string browserVersion = string.Empty;
            string dataImportedFromBridgeFile = string.Empty;
            try
            {
                ProfileLiteSecurity.Client.ClientKey clientKeyManager = new ProfileLiteSecurity.Client.ClientKey();
                errorCode += 1;
                clientKeyManager = clientKeyManager.GetClientKeyInfoFromToken(clientKey);
                string errors = string.Empty;
                errorCode += 1;
                string name = clientKeyManager.Company;
                string refineryId = clientKeyManager.RefNum;
                string search = name + " (" + refineryId + ")";
                ProfileLiteSecurity.Cryptography.Certificate certManager = new ProfileLiteSecurity.Cryptography.Certificate();
                errorCode += 1;
                X509Certificate2 privateX509Cert = new X509Certificate2();
                errorCode += 1;
                bool success = certManager.Get(search, out privateX509Cert, false, true);
                errorCode += 1;
                if (!success)
                {
                    PrepActivityLogArguments(ref applicationName, ref methodology, ref localRefineryID, ref CallerIP,
                                 ref userid, ref computername, ref version, err, ref errorMessages, ref localDomainName,
                                  ref osVersion, ref officeVersion);                 
                    WriteActivityLogExtended(applicationName, methodology, CallerIP, userid, computername,null, "GetEncodedSignedCMS", null,
                            null,null,"ERROR",version,"Error in GetEncodedSignedCms at " + errorCode.ToString() + " for refineryID",
                            localDomainName,osVersion,browserVersion,officeVersion,dataImportedFromBridgeFile,null);
                    throw new Exception("Error in GetEncodedSignedCms() at " + errorCode.ToString());
                 }
                errorCode += 1;
                ProfileLiteSecurity.Cryptography.NonRepudiation nonRepManager = new ProfileLiteSecurity.Cryptography.NonRepudiation();
                errorCode += 1;
                string errorMsg = string.Empty;
                byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(clientKey);
                errorCode += 1;
                if (!nonRepManager.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedSignedCms, true))
                {
                    PrepActivityLogArguments(ref applicationName, ref methodology, ref localRefineryID, ref CallerIP,
                        ref userid, ref computername, ref version, err, ref errorMessages, ref localDomainName,
                        ref osVersion, ref officeVersion);
                    WriteActivityLogExtended(applicationName, methodology, CallerIP, userid, computername, null, "GetEncodedSignedCMS", null,
                    null, null, "ERROR", version, "Error in GetEncodedSignedCms at " + errorCode.ToString() + " for refineryID",
                    localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile, null);
                    throw new Exception("Error in GetEncodedSignedCms() at " + errorCode.ToString() + errorMsg);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Profile authentication/authorization error (error code " + errorCode.ToString() + "): " + ex.Message);
            }

        }
        private void PrepActivityLogArguments(ref string applicationName, ref string Methodology, ref  string refineryID, ref string CallerIP,
            ref  string userid, ref  string computername, ref  string version, Exception err,
            ref string errorMessages, ref  string localDomainName, ref string osVersion, ref  string officeVersion)
        {
            try
            {
                
                applicationName = "Profile II";
                        Methodology = "0000";
                        //refineryID =Me.RefineryID;
                        CallerIP = GetClientIpV4();
                        if (userid==null || userid.Length < 1)
                            userid = System.Environment.UserName;                
                        if (userid==null || userid.Length < 1)
                        {
                                userid = "UNKNOWN";
                        }
                        computername = Environment.MachineName.ToString();
                        //version = System.Windows.Forms.Application.ProductVersion.ToString();
                        if(err!=null)
                            {errorMessages = err.Message.Substring(0, 998);}
                        else
                            {errorMessages = String.Empty;}

                        if (System.Environment.UserName.Contains(@"\"))
                        {   
                            char[] delim = {'\\'};
                            string[] parts = System.Environment.UserName.ToString().Split(delim);
                            localDomainName = parts[0];
                   }     
                 else
                 {
                            if( Environment.UserDomainName.Length > 0)
                                {localDomainName = Environment.UserDomainName;}
                            else
                              {  localDomainName = String.Empty;}
                            

                 }

                        /*       
                If Environment.Is64BitOperatingSystem Then
                    result += "64 bit"
                Else
                    result += "NOT 64 bit"
                End If
                         * osVersion =Solomon.ProfileII.Proxies.C  System.Computer.Info.OSVersion.ToString() + "; "
                                osVersion = "OsVersion(): " + osVersion ;
                         * */
                        officeVersion = String.Empty; // 'haven't found a reliable way of doing this yet.  No-one seems to be able to get this post to work all the time:  http://stackoverflow.com/questions/2203980/detect-whether-office-is-32bit-or-64bit-via-the-registry
            }
            catch { }
        }
        private string GetClientIpV4()
        {
            try
            {
                string hostName = System.Net.Dns.GetHostName() ;
                System.Net.IPHostEntry ipEntry  = System.Net.Dns.GetHostEntry(hostName);
                System.Net.IPAddress[]  addresses = ipEntry.AddressList;
                return addresses[addresses.Length - 1].ToString();

            } catch
            {
                return string.Empty;
            }
        }

    }
}
