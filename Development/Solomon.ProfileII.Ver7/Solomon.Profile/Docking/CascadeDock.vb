﻿
Public Class CascadeDock
    Implements IDocker

    Private _capturingMoves As Boolean = False
    Private _selectedControlOrdinal = 1
    Private _highest As Integer = 0
    Private _leftmost As Integer = 0
    Private _lowest As Integer = 0
    Private _rightmost As Integer = 0
    Private _padding As Integer = 20

    Private _userControls() As SA_Browser
    Private _panel As System.Windows.Forms.Panel
    Private _visible As Boolean = True
    Private _top As Integer
    Private _left As Integer
    Private _width As Integer
    Private _height As Integer


    Public Property Visible As Boolean
        Get
            Return _visible
        End Get
        Set(value As Boolean)
            _visible = value
            _panel.Visible = _visible
        End Set
    End Property
    Public Property Top As Integer
        Get
            Return _top
        End Get
        Set(value As Integer)
            _top = value
            _panel.Top = _top
        End Set
    End Property
    Public Property Left As Integer
        Get
            Return _left
        End Get
        Set(value As Integer)
            _left = value
            _panel.Left = _left
        End Set
    End Property
    Public Property Width As Integer
        Get
            Return _width
        End Get
        Set(value As Integer)
            _width = value
            _panel.Width = _width
        End Set
    End Property
    Public Property Height As Integer
        Get
            Return _height
        End Get
        Set(value As Integer)
            _height = value
            _panel.Height = _height
        End Set
    End Property

    Friend Sub New(panel As System.Windows.Forms.Panel, userControls() As SA_Browser)
        _panel = panel
        Visible = True
        _userControls = userControls
    End Sub
    Public Sub PrepPanel() Implements IDocker.PrepPanel
        ClearColsRows()
        _highest = _padding
        _leftmost = _padding
        _rightmost = _panel.Width - _padding
        _lowest = _panel.Height - _padding
        For count As Integer = 0 To _userControls.Length - 1
            _userControls(count).Visible = True
            _userControls(count).Dock = DockStyle.None
            _userControls(count).CallingPanel = Me
            _panel.Controls.Add(_userControls(count))
        Next

        SetPositions()

        _panel.Controls(0).BringToFront()
    End Sub

    Private Sub SetPositions()
        Try

            Dim ucsCount As Integer = _panel.Controls.Count
            Dim offset As Integer = (_lowest - _highest) / ucsCount

            Dim heightPos As Integer = _highest
            Dim leftPos As Integer = _leftmost
            Dim rightPos As Integer = 0
            Dim bottomPos As Integer = 0

            Dim reverseCount As Integer = 0
            Dim count As Integer = 1
            For Each ctrl As Control In _panel.Controls
                If TypeOf (ctrl) Is SA_Browser Then
                    heightPos += _padding
                    leftPos += _padding
                    ctrl.Top = heightPos
                    ctrl.Left = leftPos
                    ctrl.Width = _rightmost - leftPos - (_padding * (ucsCount - reverseCount))
                    ctrl.Height = _lowest - heightPos - (_padding * (ucsCount - reverseCount))
                    reverseCount += 1
                    count += 1
                End If
            Next
        Catch ex As Exception
            Dim msg As String = ex.Message
        End Try
    End Sub

    Public Sub MakeTopmost(ordinal As Integer) Implements IDocker.MakeTopmost
        'Me.Text = ordinal.ToString()
        _selectedControlOrdinal = ordinal
        For Each ctl As SA_Browser In _panel.Controls
            Dim ord As Integer = ctl.Ordinal
            If ord = ordinal Then
                ctl.BringToFront()
                _selectedControlOrdinal = ordinal
                Return
            End If
        Next
    End Sub

    Private Function GetUsercontrol(ordinal As Integer) As SA_Browser
        For Each ctl As SA_Browser In _panel.Controls
            If ctl.Ordinal = ordinal Then
                Return ctl
            End If
        Next
    End Function

    Public Sub CloseThis(ordinal As Integer) Implements IDocker.CloseThis
        Dim newUserControls(_userControls.Length - 2) As SA_Browser
        Dim iadded As Integer = 0
        'remove from collection
        For i As Integer = 0 To _userControls.Length - 1
            If _userControls(i).Ordinal <> ordinal Then
                newUserControls(iadded) = _userControls(i)
                iadded += 1
            End If
        Next
        _userControls = newUserControls
        'redisplay
        PrepPanel()
    End Sub

    Private Sub ClearColsRows()
        While _panel.Controls.Count > 0
            _panel.Controls.RemoveAt(0)
        End While
    End Sub

    Public Sub AddThis(ordinal As Integer) Implements IDocker.AddThis
        ReDim Preserve _userControls(_userControls.Length)
        Dim v As New SA_Browser()
        v.Ordinal = ordinal
        _userControls(_userControls.Length - 1) = v
        PrepPanel()

    End Sub


    'Private Sub Cascade_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
    '    'not working - is the SA_Browser capturing the event and keeping it from the frm?
    '    If e.Button <> Windows.Forms.MouseButtons.Left Then
    '        Return
    '    End If
    '    '// Might want to pad these values a bit if the line is only 1px,
    '    '// might be hard for the user to hit directly
    '    Dim SA_Browser As SA_Browser = GetUsercontrol(_selectedControlOrdinal)
    '    'If e.Y = SA_Browser.Top Then
    '    If e.Y <= SA_Browser.Top AndAlso e.Y >= SA_Browser.Top + 20 Then
    '        If e.X >= SA_Browser.Left AndAlso e.X <= SA_Browser.Left + SA_Browser.Width Then
    '            _capturingMoves = True
    '            Return
    '        End If
    '    End If
    '    _capturingMoves = False

    'End Sub

    'Private Sub Cascade_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
    '    If Not _capturingMoves Then Return
    'End Sub

    'Private Sub Cascade_MouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp
    '    If _capturingMoves Then
    '        _capturingMoves = False
    '    End If
    'End Sub

    'Private Sub Panel_MouseClick(sender As Object, e As MouseEventArgs) Handles Panel.MouseClick
    '    If e.Button = Windows.Forms.MouseButtons.Right Then
    '        AddThis(_userControls.Length + 1)
    '    End If
    'End Sub



End Class
