﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Dashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Dashboard))
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.GroupBoxChgDates = New System.Windows.Forms.GroupBox()
        Me.DashboardDateChoices = New System.Windows.Forms.ComboBox()
        Me.btnDashboardChgDates = New System.Windows.Forms.Button()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.DashboardDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnCreate = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboDashboards = New System.Windows.Forms.ComboBox()
        Me.pnlTable = New System.Windows.Forms.TableLayoutPanel()
        Me.pnlOther = New System.Windows.Forms.Panel()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pnlHeader.SuspendLayout()
        Me.GroupBoxChgDates.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.pnlOther.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlHeader
        '
        Me.pnlHeader.Controls.Add(Me.GroupBoxChgDates)
        Me.pnlHeader.Controls.Add(Me.btnTest)
        Me.pnlHeader.Controls.Add(Me.DashboardDateTimePicker)
        Me.pnlHeader.Controls.Add(Me.Panel1)
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.GroupBox1)
        Me.pnlHeader.Controls.Add(Me.btnDelete)
        Me.pnlHeader.Controls.Add(Me.btnLoad)
        Me.pnlHeader.Controls.Add(Me.Label1)
        Me.pnlHeader.Controls.Add(Me.cboDashboards)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(889, 93)
        Me.pnlHeader.TabIndex = 0
        '
        'GroupBoxChgDates
        '
        Me.GroupBoxChgDates.Controls.Add(Me.DashboardDateChoices)
        Me.GroupBoxChgDates.Controls.Add(Me.btnDashboardChgDates)
        Me.GroupBoxChgDates.Location = New System.Drawing.Point(650, 4)
        Me.GroupBoxChgDates.Name = "GroupBoxChgDates"
        Me.GroupBoxChgDates.Size = New System.Drawing.Size(128, 86)
        Me.GroupBoxChgDates.TabIndex = 13
        Me.GroupBoxChgDates.TabStop = False
        Me.GroupBoxChgDates.Text = "Change all charts and reports dates to:"
        '
        'DashboardDateChoices
        '
        Me.DashboardDateChoices.FormattingEnabled = True
        Me.DashboardDateChoices.Location = New System.Drawing.Point(14, 35)
        Me.DashboardDateChoices.Name = "DashboardDateChoices"
        Me.DashboardDateChoices.Size = New System.Drawing.Size(98, 21)
        Me.DashboardDateChoices.TabIndex = 13
        '
        'btnDashboardChgDates
        '
        Me.btnDashboardChgDates.Location = New System.Drawing.Point(20, 60)
        Me.btnDashboardChgDates.Name = "btnDashboardChgDates"
        Me.btnDashboardChgDates.Size = New System.Drawing.Size(82, 23)
        Me.btnDashboardChgDates.TabIndex = 12
        Me.btnDashboardChgDates.Text = "Change"
        Me.btnDashboardChgDates.UseVisualStyleBackColor = True
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(802, 4)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 10
        Me.btnTest.Text = "test"
        Me.btnTest.UseVisualStyleBackColor = True
        Me.btnTest.Visible = False
        '
        'DashboardDateTimePicker
        '
        Me.DashboardDateTimePicker.CustomFormat = """MM/yyyy"""
        Me.DashboardDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DashboardDateTimePicker.Location = New System.Drawing.Point(821, 40)
        Me.DashboardDateTimePicker.Name = "DashboardDateTimePicker"
        Me.DashboardDateTimePicker.Size = New System.Drawing.Size(56, 20)
        Me.DashboardDateTimePicker.TabIndex = 11
        Me.DashboardDateTimePicker.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(2, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(302, 85)
        Me.Panel1.TabIndex = 9
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Gray
        Me.Label10.Location = New System.Drawing.Point(81, 47)
        Me.Label10.Name = "Label10"
        Me.Label10.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label10.Size = New System.Drawing.Size(221, 38)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Refinery Settings and Preferences"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(81, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(203, 44)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 17
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(81, 85)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 10
        Me.PictureBox1.TabStop = False
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(411, 6)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        Me.btnSave.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnCreate)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(510, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(136, 86)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Create New Dashboard"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "New Dashboard Name"
        '
        'btnCreate
        '
        Me.btnCreate.Location = New System.Drawing.Point(25, 60)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(75, 23)
        Me.btnCreate.TabIndex = 6
        Me.btnCreate.Text = "Create"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(6, 36)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(122, 20)
        Me.TextBox1.TabIndex = 5
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(411, 60)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(321, 60)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(75, 23)
        Me.btnLoad.TabIndex = 2
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(318, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Dashboard"
        '
        'cboDashboards
        '
        Me.cboDashboards.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDashboards.FormattingEnabled = True
        Me.cboDashboards.Location = New System.Drawing.Point(321, 32)
        Me.cboDashboards.Name = "cboDashboards"
        Me.cboDashboards.Size = New System.Drawing.Size(121, 21)
        Me.cboDashboards.TabIndex = 0
        '
        'pnlTable
        '
        Me.pnlTable.AutoScroll = True
        Me.pnlTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.pnlTable.ColumnCount = 2
        Me.pnlTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlTable.Location = New System.Drawing.Point(13, 99)
        Me.pnlTable.Name = "pnlTable"
        Me.pnlTable.RowCount = 1
        Me.pnlTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlTable.Size = New System.Drawing.Size(864, 389)
        Me.pnlTable.TabIndex = 1
        '
        'pnlOther
        '
        Me.pnlOther.Controls.Add(Me.pnlButtons)
        Me.pnlOther.Location = New System.Drawing.Point(0, 141)
        Me.pnlOther.Name = "pnlOther"
        Me.pnlOther.Size = New System.Drawing.Size(547, 108)
        Me.pnlOther.TabIndex = 2
        '
        'pnlButtons
        '
        Me.pnlButtons.AutoScroll = True
        Me.pnlButtons.Controls.Add(Me.Button1)
        Me.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlButtons.Location = New System.Drawing.Point(0, 0)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(547, 39)
        Me.pnlButtons.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(13, 7)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Dashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(889, 500)
        Me.Controls.Add(Me.pnlOther)
        Me.Controls.Add(Me.pnlTable)
        Me.Controls.Add(Me.pnlHeader)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Dashboard"
        Me.Text = "Test"
        Me.pnlHeader.ResumeLayout(False)
        Me.pnlHeader.PerformLayout()
        Me.GroupBoxChgDates.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnlOther.ResumeLayout(False)
        Me.pnlButtons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboDashboards As System.Windows.Forms.ComboBox
    Friend WithEvents pnlTable As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pnlOther As System.Windows.Forms.Panel
    Friend WithEvents pnlButtons As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnTest As System.Windows.Forms.Button
    Friend WithEvents GroupBoxChgDates As System.Windows.Forms.GroupBox
    Friend WithEvents btnDashboardChgDates As System.Windows.Forms.Button
    Friend WithEvents DashboardDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents DashboardDateChoices As System.Windows.Forms.ComboBox
End Class
