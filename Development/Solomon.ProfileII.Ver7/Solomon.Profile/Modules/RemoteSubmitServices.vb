Option Explicit On

Imports System.Configuration

Imports Solomon.ProfileII.Proxies
Imports Solomon.ProfileII.Contracts


Friend Class RemoteSubmitServices
    Inherits SubmitServicesExtension

#Region "Data Submission Records"

    Friend Sub SubmitData(ByVal ds As DataSet, refineryID As String)
        If AppCertificate.IsCertificateInstall() Then
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Me.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            'End If
            Dim b As New Solomon.Submit12.SubmitServices2
            b.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            b.SubmitRefineryData(ds, GetClientKey())
        Else
            Throw New Exception("Remote service call can not invoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Sub
    Friend Sub SubmitActivityLog(method As String, EntityName As String,
                                 PeriodStart As Date?, PeriodEnd As Date?, status As String,
                                 Err As Exception, dataImportedFromBridgeFile As String)
        If AppCertificate.IsCertificateInstall() Then
            Dim applicationName As String, methodology As String, refineryID As String, CallerIP As String, userid As String, computername As String, service As String
            Dim version As String, errorMessages As String, localDomainName As String, osVersion As String, officeVersion As String
            frmMain.PrepActivityLogArguments(applicationName, methodology, refineryID, CallerIP,
            userid, computername, version, Err,
            errorMessages, localDomainName, osVersion, officeVersion)
            'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            Me.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            'End If
            Dim b As New Solomon.Submit12.SubmitServices2
            b.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            Try
                b.WriteActivityLogExtended(applicationName, methodology, GetClientKey(), CallerIP, userid, computername,
                                   service, method, EntityName, PeriodStart, PeriodEnd, status, version,
                                   errorMessages, localDomainName, osVersion, String.Empty, officeVersion, dataImportedFromBridgeFile)
            Catch ex As Exception
                Dim stopHere As String = String.Empty
            End Try
        Else
            Throw New Exception("Remote service call can not invoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Sub

    Function GetData(ByVal startDate As Date, ByVal endDate As Date, clientKey As String, currency As String) As DataSet
        Dim useWebService As Boolean = False
        If useWebService Then
            If AppCertificate.IsCertificateInstall() Then
                'If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
                'End If
                Dim b As New Solomon.Submit12.SubmitServices2
                b.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
                Return b.GetDataByPeriod(startDate, endDate, clientKey, currency)
            Else
                Throw New Exception("Remote service call can not invoke because Solomon Associates' certificate is not installed on your machine.")
            End If
        Else
            Dim proxy As New ProfileDataClient("BasicHttpBinding_IProfileDataService") ' ProfileDataReportsClient("BasicHttpBinding_IProfileReportService")
            Dim result As String = String.Empty
            Try
                Return proxy.GetDataByPeriod(startDate, endDate, clientKey) ', currency)
            Catch ex As Exception
                Throw ex
            Finally
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If
    End Function
#End Region

#Region "Test code"
    Friend Shared Sub Main(refineryID As String)
        Dim service As New RemoteSubmitServices
        Dim ds As New DataSet

        'Dim startPrd As String = "1" + _
        '                        "/1/" + _
        '                        "2000" + _
        '                        " 12:00:00 AM"

        'Dim endPrd As String = DateAdd(DateInterval.Month, 1, CDate(startPrd)).ToString
        'Console.WriteLine("StartDate " + startPrd + " EndDate " + endPrd)

        'Console.WriteLine("StartDate " + CDate(startPrd).ToString + " EndDate " + CDate(endPrd).ToString)
        ' ds.ReadXml("C:\MonthlyUpload.xml")
        'service.SubmitData(ds)
        ds = service.GetData(#1/1/2004#, #2/1/2004#, GetClientKey(), "USD")
        ds.WriteXml("C:\dump.xml", XmlWriteMode.WriteSchema)
        'For y = 0 To ds.Tables.Count - 1
        '    Console.WriteLine(ds.Tables(y).TableName + "," + ds.Tables(y).Rows.Count.ToString)
        'Next
    End Sub
#End Region

End Class
