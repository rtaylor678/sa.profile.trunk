Option Compare Binary
Option Explicit On
Option Strict On

Friend Module DialogMessages

    Friend Function ProfileMsgBox(ByVal cat As String, ByVal code As String, ByVal custom As String) As DialogResult
        Dim myDialog As New ProfileDialog
        Dim msg As String

        Select Case cat
            Case "CRIT", "INFO"
                myDialog.pnlYNC.Visible = False
                myDialog.pnlYN.Visible = False
                myDialog.AcceptButton = myDialog.Button6

                If cat = "INFO" Then
                    myDialog.pbImage.Image = myDialog.ilImages.Images(0)
                    myDialog.Text = "  Information"
                Else
                    myDialog.pbImage.Image = myDialog.ilImages.Images(3)
                    myDialog.Text = "  Critical Message"
                End If

            Case "YN"
                myDialog.pnlYNC.Visible = False
                myDialog.pnlIC.Visible = False
                myDialog.pbImage.Image = myDialog.ilImages.Images(4)
                myDialog.Text = "  Yes or No"
            Case "YNC"
                myDialog.pnlYN.Visible = False
                myDialog.pnlIC.Visible = False
                myDialog.pbImage.Image = myDialog.ilImages.Images(4)
                myDialog.Text = "  Yes, No or Cancel"
        End Select

        msg = GetDialogMsg(code, custom)
        If Microsoft.VisualBasic.Strings.Len(msg) > 170 Then myDialog.Height = myDialog.Height + 25
        myDialog.lblMessage.Text = msg

        Return myDialog.ShowDialog

    End Function

    Private Function GetDialogMsg(ByVal code As String, ByVal Custom As String) As String

        Select Case code
            Case "WAIT" : Return "The action that you have requested may take up to a couple minutes. Do you still wish to procede?"
            Case "OPENEXCEL" : Return "The download requested has been saved as " & Custom & ". Click 'YES' to attempt to open the file. Click 'NO' to manually open the file later."
            Case "NOCERT" : Return "You are attempting to download unsecure data from Solomon. In order to secure the data, click 'Yes' and install the electronic certificate issued by Solomon and re-download the report. By clicking 'No', you will not be able to view the data."
            Case "DELETEAM" : Return "Are you sure you want to delete the event log below?"
            Case "CLEAR" : Return "Are you sure you want to clear the " & Custom & "?"
            Case "DELETE" : Return "Are you sure you want to delete the " & Custom & "?"
            Case "DELETEPU" : Return "Are you sure you want to delete the current process and its corresponding turnarounds and performance targets?"
            Case "DELETETA" : Return "Are you sure you want to delete the current turnaround?"
            Case "CRUDEDEFAULTS" : Return "Would you like to populate the entry with Solomon's defaults for crude name, gravity, and sulfur?"
            Case "MATLDEFAULTS" : Return "Would you like to populate the entry with Solomon's default for material name?"
            Case "PREVPERIODS" : Return "Would you like to prepopulate the new period with the figures from the previous period?"
            Case "UNSAVED" : Return "Do you want to save the changes made to " & Custom & "?"
            Case "NOPRINT" : Return "You do not have a printer set up on this machine."
            Case "PRINTRESULTS" : Return "You must be viewing a table view, report or chart before printing."
            Case "WRONGPATH" : Return "The path designated for the bridge file on the Settings tab of the Control Panel is invalid. Please make sure the path is correct and try again."
            Case "KEY" : Return "You must have valid key before using Solomon Profile� II. Please contact Solomon."
            Case "CONN" : Return "Could not connect to Solomon database. Make sure you have internet connectivity, and try again later."
            Case "RESSEL" : Return "You must first select a valid " & Custom & " from the list provided to the left."
            Case "NOXL" : Return "Could not initialize Excel."
            Case "UPYES" : Return "Successfully uploaded '" & Custom & "' to Solomon."
            Case "CHTDATE" : Return "Verify the 'Start Date' is before or on the 'End Date' and that both dates are set on the form."
            Case "INVALID_CREDENTIALS" : Return "Reference data has not been downloaded from Solomon. Please have your coordinator log in or try again and make sure you are not signing in locally only."
            Case "NO_CERTIFICATE" : Return "A Solomon issued certificate has not been installed on this machine. Therefore, you cannot create a secured connection with Solomon which prohibits the downloading of data. Please contact Solomon for assistance."
            Case "PASSWORD" : Return "The username and/or password you supplied is not correct. Verify that the CAPS LOCK key is off and be sure to use the correct capitalization."
            Case "CHANGEPW" : Return "Unable to change password. Please try again next time."
            Case "CLOSEMONTH" : Return "You must first close all open months before accessing this functionality."
            Case "SELECT" : Return "Select a valid " & Custom & "."
            Case "TWOPERIODS" : Return "You cannot have two periods with the same starting month. Please correct and try again."
            Case "UPYES" : Return "Successfully uploaded '" & Custom & "' to Solomon."
            Case "NEW" : Return "You have just created a dataset for the month of '" & Custom & "'."
            Case "IMPORT" : Return "Are you sure you want to import " & Custom & " from Excel?"
            Case "NOWB" : Return "The following workbook was not found: " & Custom
            Case "NOWS" : Return "A valid worksheet has not been assigned."
            Case "NOXL" : Return "Unable to open an instance of Microsoft Excel."
            Case "MAPPING" : Return Custom & " is not mapped correctly. Please check your row and column mappings and try again."
            Case "CLEARALL" : Return "Do you want to clear all " & Custom & " data? Click 'YES' to delete all of the data, or click 'NO' to delete only the current table."
            Case "ISREADY" : Return Custom & " data has already been marked as 'Ready'. Do you want to overwrite this data?"
            Case "CLOSEMONTHS" : Return "All months must be closed in order to execute this functionality."
            Case "ONETA" : Return "You must have at least one turnaround record per process unit. You can change the 'T/A Exception' if you did not mean to recognize a turnaround."
            Case "LOADERR" : Return "You must have at least one turnaround record per process unit. You can change the 'T/A Exception' if you did not mean to recognize a turnaround."

            Case "NoFileXML" : Return "The XML file " & Custom & " does not exist. Please verify its location."

            Case "NoReadXMLBackup" : Return "An error occured while reading " & Custom & ". A temporal copy is being used."
            Case "NoReadXMLBackupError" : Return "An error occured while reading the temporal file" & Custom & ". Contact Solomon for further assistance."
            Case "NoReadXMLStandard" : Return "An error occured while reading " & Custom & ". The default Solomon supplied copy is being used.  Contact Solomon for further assistance."
            Case "NoReadXMLStandardError" : Return "An error occured while reading the Solomon supplied " & Custom & " file.  Contact Solomon for further assistance."

            Case "MakeXMLBackupError" : Return Custom & " could not be backed up before writing. Contact Solomon for further assistance."
            Case "NoWriteXMLBackup" : Return Custom & " could not be saved. A temporal copy has replaced the new data."
            Case "NoWriteXMLBackupError" : Return Custom & " could not be saved. The temporal copy could not be restored."
            Case "NoWriteXMLNoBackup" : Return Custom & " could not be saved. A temporal copy could not be found."
            Case "CULTURE" : Return "There was a problem converting the culture at " & Custom
            Case Else : Return Custom
        End Select

    End Function

End Module
