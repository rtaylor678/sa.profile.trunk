<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small width=100% border=0>
  <tr>
    <td width=3%></td>
    <td width=450></td>
    <td width=10%></td>
    <td width=3%></td>
    <td width=10%></td>
    <td width=3%></td>
    <td width=10%></td>
    <td width=3%></td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Product Yield</strong></td>
    <td colspan=2 valign=bottom align=center><strong>Total</br>Barrels</strong></td>
    <td colspan=2 valign=bottom align=center><strong>Price,</br>CurrencyCode/bbl</strong></td>
    <td colspan=2 valign=bottom align=center><strong>GPV,</br>CurrencyCode/1000</strong></td>
  </tr>
  <tr>
    <td colspan=8></td>
  </tr>
  SECTION(Yield,Category<>'RMI',)
  BEGIN
  <tr>
    <td></td>
    <td>@MaterialName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(Price,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(GPV,'#,##0.0')</td>
    <td></td>
  </tr>
  END
  
  SECTION(Category,Category<>'OTHRM',)
  BEGIN
  <tr>
    <td></td>
    <td>@MaterialName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(Price,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(USEUNIT(GPVUS,GPVLocal),'#,##0.0')</td>
    <td></td>
  </tr>
  END
  
  SECTION(YieldLubes,,)
  BEGIN
  <tr>
    <td></td>
    <td>@MaterialName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(Price,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(GPV,'#,##0.0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=8 valign=bottom><strong>&nbsp;</strong></td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Raw Material Details</strong></td>
    <td colspan=2 valign=bottom align=center><strong>Total</br>Barrels</strong></td>
    <td colspan=2 valign=bottom align=center><strong>Price,</br>CurrencyCode/bbl</strong></td>
    <td colspan=2 valign=bottom align=center><strong>RMC,</br>CurrencyCode/1000</strong></td>
  </tr>

 SECTION(Yield,Category='RMI',)
  BEGIN
  <tr>
    <td></td>
    <td>@MaterialName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(Price,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(GPV,'#,##0.0')</td>
    <td></td>
  </tr>
  END

  SECTION(Category,Category='RMB',)
  BEGIN
  <tr>
    <td></td>
    <td>@MaterialName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(Price,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(USEUNIT(GPVUS,GPVLocal),'#,##0.0')</td>
    <td></td>
  </tr>

  SECTION(Category,Category='OTHRM',)
  BEGIN
  <tr>
    <td></td>
    <td>@MaterialName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(Price,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(USEUNIT(GPVUS,GPVLocal),'#,##0.0')</td>
    <td></td>
  </tr>
  END
</table>
<!-- template-end -->