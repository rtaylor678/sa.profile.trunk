
<table width="800" border="1">
  <tr>
    <th height="25" scope="col">Material ID </th>
    <th scope="col">Material Name </th>
    <th scope="col">Total Barrel </th>
    <th scope="col">Barrels per Day </th>
  </tr>
  SECTION(Sensible,,SortKey ASC)
  BEGIN
  <tr>
    <td scope="col"><div align="center">@MaterialID</div></td>
    <td scope="col"><div align="center">@MaterialName</div></td>
    <td scope="col"><div align="center">@BBL</div></td>
    <td scope="col"><div align="center">@BPD</div></td>
  </tr>
  END
</table>
