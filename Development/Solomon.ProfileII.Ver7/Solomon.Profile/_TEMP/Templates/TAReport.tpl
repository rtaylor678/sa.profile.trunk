<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small border=0>
  <tr>
    <td colspan=20 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
  <tr>
    <td width=200></td>
    <td width=80></td>
    <td width=80></td>
    <td width=60></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
  </tr>
  <tr>
    <td valign=bottom><strong>Process Unit Name</strong></td>
    <td align=right valign=bottom><strong>T/A Date</strong></td>
    <td align=right valign=bottom><strong>Previous</br>T/A Date</strong></td>
    <td align=right valign=bottom><strong>Hours</br>Down</strong></td>
    <td align=right valign=bottom><strong>Cost</strong></td>
    <td align=right valign=bottom><strong>Currency</strong></td>
    <td align=right valign=bottom><strong>Company&nbsp;&nbsp;</br>Work Hours</strong></td>
    <td align=right valign=bottom><strong>Contract&nbsp;</br>Work Hours</strong></td>
  </tr>

  SECTION(Turnarounds,, SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;@UnitName</td>
    <td align=right>Format(TADate,'dd-MMM-yy')</td>
    <td align=right>Format(PrevTADate,'dd-MMM-yy')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>
    <td align=right>Format(TACostLocal,'#,##0')</td>
    <td align=right>&nbsp;@TACurrency</td>
    <td align=right>Format(TACompWHr,'#,##0')</td>
    <td align=right>Format(TAContWHr,'#,##0')</td>
  </tr>
  END

</table>

<!-- template-end -->
