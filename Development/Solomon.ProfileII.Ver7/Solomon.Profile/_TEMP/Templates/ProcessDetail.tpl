<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small width=900 border=0>
  <tr>
    <td colspan=20 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
  <tr>
    <td width=5></td>
    <td width=200></td>
    <td width=10></td>
    <td width=70></td>
    <td width=70></td>
    <td width=10></td>
    <td width=50></td>
    <td width=5></td>
    <td width=50></td>
    <td width=5></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Process Unit Name</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Process ID</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Capacity</strong></td>
    <td colspan=2 align=center valign=bottom><strong>kEDC</strong></td>
    <td colspan=2 align=center valign=bottom><strong>kUEDC</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Estimated</br>Gain</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Standard</br>Energy</strong></td>
    <td colspan=2 align=center valign=bottom><strong>k Standard</br>MEI</strong></td>
    <td colspan=2 align=center valign=bottom><strong>k Standard</br>PEI</strong></td>
    <td colspan=2 align=center valign=bottom><strong>k Standard</br>NEI</strong></td>
  </tr>

  SECTION(ProcessUnits, ProcessGrouping = 'Process', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>Format(ProcessID,'#,##0')</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 valign=bottom><strong>Ancillary Units</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Ancillary', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>Format(ProcessID,'#,##0')</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 valign=bottom><strong>Idle Units</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Idle', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>Format(ProcessID,'#,##0')</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 height=30 valign=bottom><strong>Utilities and Off-sites</strong></td>
  </tr>

  SECTION(ProcessUnits,ProcessGrouping = 'Utility', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>Format(ProcessID,'#,##0')</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>
  END


  SECTION(UnitsOffsites,,)
  BEGIN
 
  <tr>
    <td></td>
    <td colspan=3>Standard Tankage</td>
    <td align=right>Format(TnkStdCap,'#,##0')</td>
    <td></td>
    <td align=right>Format(TnkStdEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(TnkStdEDC,'#,##0')</td>
    <td colspan=11></td>
  </tr>

  <tr>
    <td></td>
    <td colspan=7>Purchased Utility UEDC Credit</td>
    <td align=right>Format(PurchasedUtilityUEDC,'#,##0')</td>
    <td colspan=11></td>
  </tr>

  <tr>
    <td></td>
    <td colspan=3>Receipts and Shipments</td>
    <td colspan=2></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td colspan=5></td>
    <td align=right>Format(TotRSMaintEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSPersEffDiv,'#,##0')</td>
    <td></td>
    <td align=right>Format(TotRSNEOpexEffDiv,'#,##0')</td>
    <td></td>
  </tr>

  <tr>
    <td></td>
    <td colspan=3>Sensible Heat</td>
    <td colspan=8></td>
    <td align=right>Format(SensHeatStdEnergy,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=3>Asphalt</td>
    <td colspan=8></td>
    <td align=right>Format(AspStdEnergy,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=3>All Other</td>
    <td colspan=8></td>
    <td align=right>Format(OffsitesStdEnergy,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td height=30 valign=bottom colspan=6><strong>Total Refinery</strong></td>
    <td align=right valign=bottom><strong>Format(TotEDC,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(TotUEDC,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(EstGain,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(TotStdEnergy,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(MaintEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(PersEffDiv,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(NEOpexEffDiv,'#,##0')</strong></td>
    <td></td>
  </tr>
  END
</table>

<!-- template-end -->