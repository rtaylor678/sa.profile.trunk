<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)
ShowIf(Ytd,IfYtdOn)
ShowIf(Avg,IfAvgOn)

<table class=small width=100% border=0>
  <tr>
    <td width=5></td>
    <td></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
  </tr>

  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center valign=bottom><strong>ReportPeriod</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Target,Refinery<br/>Target</strong>)</td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Avg,Rolling</br>Average</strong>)</td>
    <td colspan=2 align=center valign=bottom><strong>Tag(Ytd,YTD</br>Average</strong>)</td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC)
  INCLUDETABLE(GENSUM)
  BEGIN
  Header('
  <tr>
    <td colspan=10 height=30 valign=bottom><strong>@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td>@ChartTitle<span align=left>USEUNIT(AxisLabelUS,AxisLabelMetric)</span></td>
    <td align=right>NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td></td>
    <td align=right>TAG(Target,NoShowZero(Format(@@TargetField,@DecPlaces)))</td>
    <td></td>
    <td align=right>Tag(Avg,NoShowZero(Format(@@AvgField,@DecPlaces)))</td>
    <td></td>
    <td align=right>Tag(Ytd,NoShowZero(Format(@@YTDField,@DecPlaces)))</td>
    <td></td>
  </tr>
  END
  
 SECTION(UserDefined,,HeaderText ASC)
  BEGIN
  HEADER('<tr>
    <td colspan=10 height=30 valign=bottom><strong>@HeaderText</strong></td>
  </tr>')
  <tr>
    <td></td>
    <td>@VariableDesc</td>
    <td align=right>NoShowZero(Format(@RptValue,@DecPlaces))</td>
    <td></td>
    <td align=right>Tag(Target, NoShowZero(Format(@RptValue_Target,@DecPlaces)))</td>
    <td></td>
    <td align=right>Tag(Avg,NoShowZero(Format(@RptValue_Avg,@DecPlaces)))</td>
    <td></td>
    <td align=right>Tag(Ytd,NoShowZero(Format(@RptValue_YTD,@DecPlaces)))</td>
    <td></td>
  </tr>
  END
</table>
<!-- template-end -->
