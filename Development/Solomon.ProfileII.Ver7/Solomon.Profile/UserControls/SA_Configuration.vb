Option Explicit On

Friend Class SA_Configuration
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblRptStmCap As System.Windows.Forms.Label
    Friend WithEvents lblStmUnits As System.Windows.Forms.Label
    Friend WithEvents lblRptCap As System.Windows.Forms.Label
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents lblConfigDesc As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtProcessID As System.Windows.Forms.TextBox
    Friend WithEvents txtUnitName As System.Windows.Forms.TextBox
    Friend WithEvents cmbProcessType As System.Windows.Forms.ComboBox
    Friend WithEvents txtInServicePcnt As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbProcessID As System.Windows.Forms.ComboBox
    Friend WithEvents cmbProcessGroup As System.Windows.Forms.ComboBox
    Friend WithEvents txtDesignFeedSulfur As System.Windows.Forms.TextBox
    Friend WithEvents lblDesignFeedSulfur As System.Windows.Forms.Label
    Friend WithEvents txtRptCap As System.Windows.Forms.TextBox
    Friend WithEvents txtRptStmCap As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tcProcessFacilities As System.Windows.Forms.TabControl
    Friend WithEvents pageConfiguration As System.Windows.Forms.TabPage
    Friend WithEvents pageTargets As System.Windows.Forms.TabPage
    Friend WithEvents lblTargetsHdr As System.Windows.Forms.Label
    Friend WithEvents dgUnitTargets As System.Windows.Forms.DataGridView
    Friend WithEvents USDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MetDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UnitID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProcessUnitName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProcessUnitID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UnitTarget As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UnitCurrency As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents UnitProperty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UnitSortKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USDecPlaces As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MetDecPlaces As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents pageTurnaround As System.Windows.Forms.TabPage
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents dtpTADate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTADesc As System.Windows.Forms.Label
    Friend WithEvents dtpPrevTADate As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtTAHrsDown As System.Windows.Forms.TextBox
    Friend WithEvents txtTACostLocal As System.Windows.Forms.TextBox
    Friend WithEvents cmbTAExceptions As System.Windows.Forms.ComboBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtTAContOCC As System.Windows.Forms.TextBox
    Friend WithEvents txtTAMPSSTH As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtTAMPSOVTPcnt As System.Windows.Forms.TextBox
    Friend WithEvents txtTAContMPS As System.Windows.Forms.TextBox
    Friend WithEvents txtTAOCCSTH As System.Windows.Forms.TextBox
    Friend WithEvents txtTAOCCOVT As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents lblKCurr As System.Windows.Forms.Label
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtTALaborCostLocal As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTAExpLocal As System.Windows.Forms.TextBox
    Friend WithEvents txtTACptlLocal As System.Windows.Forms.TextBox
    Friend WithEvents txtTAOvhdLocal As System.Windows.Forms.TextBox
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_Configuration))
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblConfigDesc = New System.Windows.Forms.Label()
        Me.lblDesignFeedSulfur = New System.Windows.Forms.Label()
        Me.txtDesignFeedSulfur = New System.Windows.Forms.TextBox()
        Me.txtRptCap = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtInServicePcnt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbProcessID = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.cmbProcessGroup = New System.Windows.Forms.ComboBox()
        Me.txtProcessID = New System.Windows.Forms.TextBox()
        Me.lblStmUnits = New System.Windows.Forms.Label()
        Me.cmbProcessType = New System.Windows.Forms.ComboBox()
        Me.lblUnits = New System.Windows.Forms.Label()
        Me.txtRptStmCap = New System.Windows.Forms.TextBox()
        Me.lblRptStmCap = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblRptCap = New System.Windows.Forms.Label()
        Me.txtUnitName = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.tcProcessFacilities = New System.Windows.Forms.TabControl()
        Me.pageConfiguration = New System.Windows.Forms.TabPage()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.pageTurnaround = New System.Windows.Forms.TabPage()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.dtpTADate = New System.Windows.Forms.DateTimePicker()
        Me.lblTADesc = New System.Windows.Forms.Label()
        Me.dtpPrevTADate = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtTAHrsDown = New System.Windows.Forms.TextBox()
        Me.txtTACostLocal = New System.Windows.Forms.TextBox()
        Me.cmbTAExceptions = New System.Windows.Forms.ComboBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtTAContOCC = New System.Windows.Forms.TextBox()
        Me.txtTAMPSSTH = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtTAMPSOVTPcnt = New System.Windows.Forms.TextBox()
        Me.txtTAContMPS = New System.Windows.Forms.TextBox()
        Me.txtTAOCCSTH = New System.Windows.Forms.TextBox()
        Me.txtTAOCCOVT = New System.Windows.Forms.TextBox()
        Me.txtTALaborCostLocal = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.lblKCurr = New System.Windows.Forms.Label()
        Me.pageTargets = New System.Windows.Forms.TabPage()
        Me.dgUnitTargets = New System.Windows.Forms.DataGridView()
        Me.USDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UnitID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProcessUnitName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProcessUnitID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UnitTarget = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UnitCurrency = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.UnitProperty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UnitSortKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USDecPlaces = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetDecPlaces = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblTargetsHdr = New System.Windows.Forms.Label()
        Me.pnlTools = New System.Windows.Forms.Panel()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTAExpLocal = New System.Windows.Forms.TextBox()
        Me.txtTACptlLocal = New System.Windows.Forms.TextBox()
        Me.txtTAOvhdLocal = New System.Windows.Forms.TextBox()
        Me.tcProcessFacilities.SuspendLayout()
        Me.pageConfiguration.SuspendLayout()
        Me.pageTurnaround.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.pageTargets.SuspendLayout()
        CType(Me.dgUnitTargets, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblConfigDesc
        '
        Me.lblConfigDesc.BackColor = System.Drawing.Color.Transparent
        Me.lblConfigDesc.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblConfigDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConfigDesc.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblConfigDesc.Location = New System.Drawing.Point(6, 0)
        Me.lblConfigDesc.Name = "lblConfigDesc"
        Me.lblConfigDesc.Size = New System.Drawing.Size(722, 36)
        Me.lblConfigDesc.TabIndex = 134
        Me.lblConfigDesc.Text = "Table 1 Configuration Data"
        Me.lblConfigDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDesignFeedSulfur
        '
        Me.lblDesignFeedSulfur.AutoSize = True
        Me.lblDesignFeedSulfur.BackColor = System.Drawing.Color.Transparent
        Me.lblDesignFeedSulfur.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDesignFeedSulfur.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblDesignFeedSulfur.Location = New System.Drawing.Point(24, 259)
        Me.lblDesignFeedSulfur.Name = "lblDesignFeedSulfur"
        Me.lblDesignFeedSulfur.Size = New System.Drawing.Size(95, 13)
        Me.lblDesignFeedSulfur.TabIndex = 149
        Me.lblDesignFeedSulfur.Text = "Feed Sulfur, wt %"
        Me.lblDesignFeedSulfur.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtDesignFeedSulfur
        '
        Me.txtDesignFeedSulfur.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtDesignFeedSulfur.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDesignFeedSulfur.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtDesignFeedSulfur.Location = New System.Drawing.Point(219, 256)
        Me.txtDesignFeedSulfur.Name = "txtDesignFeedSulfur"
        Me.txtDesignFeedSulfur.Size = New System.Drawing.Size(83, 21)
        Me.txtDesignFeedSulfur.TabIndex = 10
        Me.txtDesignFeedSulfur.Tag = " 2"
        Me.txtDesignFeedSulfur.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRptCap
        '
        Me.txtRptCap.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtRptCap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRptCap.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtRptCap.Location = New System.Drawing.Point(219, 175)
        Me.txtRptCap.Name = "txtRptCap"
        Me.txtRptCap.Size = New System.Drawing.Size(83, 21)
        Me.txtRptCap.TabIndex = 7
        Me.txtRptCap.Tag = " 0"
        Me.txtRptCap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(25, 122)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 124
        Me.Label2.Tag = " "
        Me.Label2.Text = "Unit Name"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(24, 232)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(139, 13)
        Me.Label6.TabIndex = 144
        Me.Label6.Text = "Percent of Month in Service"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtInServicePcnt
        '
        Me.txtInServicePcnt.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtInServicePcnt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInServicePcnt.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtInServicePcnt.Location = New System.Drawing.Point(219, 229)
        Me.txtInServicePcnt.Name = "txtInServicePcnt"
        Me.txtInServicePcnt.Size = New System.Drawing.Size(83, 21)
        Me.txtInServicePcnt.TabIndex = 9
        Me.txtInServicePcnt.Tag = " 1"
        Me.txtInServicePcnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(312, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 125
        Me.Label3.Tag = " "
        Me.Label3.Text = "Process ID"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(312, 122)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 13)
        Me.Label4.TabIndex = 126
        Me.Label4.Tag = " "
        Me.Label4.Text = "Process Type"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmbProcessID
        '
        Me.cmbProcessID.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbProcessID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProcessID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbProcessID.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbProcessID.Location = New System.Drawing.Point(30, 94)
        Me.cmbProcessID.Name = "cmbProcessID"
        Me.cmbProcessID.Size = New System.Drawing.Size(272, 21)
        Me.cmbProcessID.TabIndex = 4
        Me.cmbProcessID.Tag = " "
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(24, 78)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(100, 13)
        Me.Label18.TabIndex = 131
        Me.Label18.Tag = " "
        Me.Label18.Text = "Process Description"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'cmbProcessGroup
        '
        Me.cmbProcessGroup.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbProcessGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProcessGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbProcessGroup.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbProcessGroup.ItemHeight = 13
        Me.cmbProcessGroup.Location = New System.Drawing.Point(30, 54)
        Me.cmbProcessGroup.Name = "cmbProcessGroup"
        Me.cmbProcessGroup.Size = New System.Drawing.Size(358, 21)
        Me.cmbProcessGroup.TabIndex = 3
        Me.cmbProcessGroup.Tag = " "
        '
        'txtProcessID
        '
        Me.txtProcessID.BackColor = System.Drawing.SystemColors.Control
        Me.txtProcessID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProcessID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtProcessID.Location = New System.Drawing.Point(308, 94)
        Me.txtProcessID.Name = "txtProcessID"
        Me.txtProcessID.ReadOnly = True
        Me.txtProcessID.Size = New System.Drawing.Size(80, 21)
        Me.txtProcessID.TabIndex = 130
        Me.txtProcessID.TabStop = False
        Me.txtProcessID.Tag = " "
        '
        'lblStmUnits
        '
        Me.lblStmUnits.BackColor = System.Drawing.Color.Transparent
        Me.lblStmUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStmUnits.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblStmUnits.Location = New System.Drawing.Point(311, 203)
        Me.lblStmUnits.Name = "lblStmUnits"
        Me.lblStmUnits.Size = New System.Drawing.Size(72, 16)
        Me.lblStmUnits.TabIndex = 139
        Me.lblStmUnits.Tag = " "
        Me.lblStmUnits.Text = "(b/sd)"
        Me.lblStmUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbProcessType
        '
        Me.cmbProcessType.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbProcessType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProcessType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbProcessType.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbProcessType.Items.AddRange(New Object() {"SCU", "MCU"})
        Me.cmbProcessType.Location = New System.Drawing.Point(308, 138)
        Me.cmbProcessType.Name = "cmbProcessType"
        Me.cmbProcessType.Size = New System.Drawing.Size(80, 21)
        Me.cmbProcessType.TabIndex = 6
        Me.cmbProcessType.Tag = " "
        '
        'lblUnits
        '
        Me.lblUnits.BackColor = System.Drawing.Color.Transparent
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblUnits.Location = New System.Drawing.Point(311, 176)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(72, 16)
        Me.lblUnits.TabIndex = 136
        Me.lblUnits.Tag = " "
        Me.lblUnits.Text = "(b/sd)"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRptStmCap
        '
        Me.txtRptStmCap.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtRptStmCap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRptStmCap.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtRptStmCap.Location = New System.Drawing.Point(219, 202)
        Me.txtRptStmCap.Name = "txtRptStmCap"
        Me.txtRptStmCap.Size = New System.Drawing.Size(83, 21)
        Me.txtRptStmCap.TabIndex = 8
        Me.txtRptStmCap.Tag = " 0"
        Me.txtRptStmCap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRptStmCap
        '
        Me.lblRptStmCap.AutoSize = True
        Me.lblRptStmCap.BackColor = System.Drawing.Color.Transparent
        Me.lblRptStmCap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRptStmCap.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblRptStmCap.Location = New System.Drawing.Point(24, 205)
        Me.lblRptStmCap.Name = "lblRptStmCap"
        Me.lblRptStmCap.Size = New System.Drawing.Size(82, 13)
        Me.lblRptStmCap.TabIndex = 141
        Me.lblRptStmCap.Tag = " "
        Me.lblRptStmCap.Text = "Steam Capacity"
        Me.lblRptStmCap.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.lblRptStmCap.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(25, 38)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(80, 13)
        Me.Label19.TabIndex = 133
        Me.Label19.Tag = " "
        Me.Label19.Text = "Process Facility"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lblRptCap
        '
        Me.lblRptCap.AutoSize = True
        Me.lblRptCap.BackColor = System.Drawing.Color.Transparent
        Me.lblRptCap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRptCap.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblRptCap.Location = New System.Drawing.Point(25, 178)
        Me.lblRptCap.Name = "lblRptCap"
        Me.lblRptCap.Size = New System.Drawing.Size(94, 13)
        Me.lblRptCap.TabIndex = 138
        Me.lblRptCap.Tag = " "
        Me.lblRptCap.Text = "Electrical Capacity"
        Me.lblRptCap.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtUnitName
        '
        Me.txtUnitName.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtUnitName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnitName.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtUnitName.Location = New System.Drawing.Point(30, 138)
        Me.txtUnitName.Name = "txtUnitName"
        Me.txtUnitName.Size = New System.Drawing.Size(272, 21)
        Me.txtUnitName.TabIndex = 5
        Me.txtUnitName.Tag = " "
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon.Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnNew
        '
        Me.btnNew.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnNew.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.SystemColors.Window
        Me.btnNew.Image = CType(resources.GetObject("btnNew.Image"), System.Drawing.Image)
        Me.btnNew.Location = New System.Drawing.Point(500, 0)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(80, 34)
        Me.btnNew.TabIndex = 7
        Me.btnNew.Text = "New"
        Me.btnNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnNew, "Create a new process unit and/or turnaround")
        Me.btnNew.UseVisualStyleBackColor = False
        '
        'tcProcessFacilities
        '
        Me.tcProcessFacilities.Controls.Add(Me.pageConfiguration)
        Me.tcProcessFacilities.Controls.Add(Me.pageTurnaround)
        Me.tcProcessFacilities.Controls.Add(Me.pageTargets)
        Me.tcProcessFacilities.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcProcessFacilities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcProcessFacilities.ItemSize = New System.Drawing.Size(150, 18)
        Me.tcProcessFacilities.Location = New System.Drawing.Point(4, 46)
        Me.tcProcessFacilities.Name = "tcProcessFacilities"
        Me.tcProcessFacilities.SelectedIndex = 0
        Me.tcProcessFacilities.Size = New System.Drawing.Size(742, 550)
        Me.tcProcessFacilities.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcProcessFacilities.TabIndex = 890
        '
        'pageConfiguration
        '
        Me.pageConfiguration.Controls.Add(Me.Label5)
        Me.pageConfiguration.Controls.Add(Me.Label20)
        Me.pageConfiguration.Controls.Add(Me.lblConfigDesc)
        Me.pageConfiguration.Controls.Add(Me.lblDesignFeedSulfur)
        Me.pageConfiguration.Controls.Add(Me.Label19)
        Me.pageConfiguration.Controls.Add(Me.txtDesignFeedSulfur)
        Me.pageConfiguration.Controls.Add(Me.txtRptCap)
        Me.pageConfiguration.Controls.Add(Me.txtUnitName)
        Me.pageConfiguration.Controls.Add(Me.Label2)
        Me.pageConfiguration.Controls.Add(Me.Label6)
        Me.pageConfiguration.Controls.Add(Me.lblRptCap)
        Me.pageConfiguration.Controls.Add(Me.txtInServicePcnt)
        Me.pageConfiguration.Controls.Add(Me.lblRptStmCap)
        Me.pageConfiguration.Controls.Add(Me.Label3)
        Me.pageConfiguration.Controls.Add(Me.txtRptStmCap)
        Me.pageConfiguration.Controls.Add(Me.Label4)
        Me.pageConfiguration.Controls.Add(Me.lblUnits)
        Me.pageConfiguration.Controls.Add(Me.cmbProcessID)
        Me.pageConfiguration.Controls.Add(Me.cmbProcessType)
        Me.pageConfiguration.Controls.Add(Me.Label18)
        Me.pageConfiguration.Controls.Add(Me.lblStmUnits)
        Me.pageConfiguration.Controls.Add(Me.cmbProcessGroup)
        Me.pageConfiguration.Controls.Add(Me.txtProcessID)
        Me.pageConfiguration.Location = New System.Drawing.Point(4, 22)
        Me.pageConfiguration.Name = "pageConfiguration"
        Me.pageConfiguration.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageConfiguration.Size = New System.Drawing.Size(734, 524)
        Me.pageConfiguration.TabIndex = 0
        Me.pageConfiguration.Text = "Configuration"
        Me.pageConfiguration.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(24, 289)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 13)
        Me.Label5.TabIndex = 152
        Me.Label5.Text = "Important"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(48, 302)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(335, 83)
        Me.Label20.TabIndex = 151
        Me.Label20.Text = resources.GetString("Label20.Text")
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pageTurnaround
        '
        Me.pageTurnaround.Controls.Add(Me.Label23)
        Me.pageTurnaround.Controls.Add(Me.Label21)
        Me.pageTurnaround.Controls.Add(Me.Label1)
        Me.pageTurnaround.Controls.Add(Me.txtTAExpLocal)
        Me.pageTurnaround.Controls.Add(Me.txtTACptlLocal)
        Me.pageTurnaround.Controls.Add(Me.txtTAOvhdLocal)
        Me.pageTurnaround.Controls.Add(Me.Label24)
        Me.pageTurnaround.Controls.Add(Me.Label11)
        Me.pageTurnaround.Controls.Add(Me.Label16)
        Me.pageTurnaround.Controls.Add(Me.Label22)
        Me.pageTurnaround.Controls.Add(Me.Label17)
        Me.pageTurnaround.Controls.Add(Me.Label14)
        Me.pageTurnaround.Controls.Add(Me.Label13)
        Me.pageTurnaround.Controls.Add(Me.Label8)
        Me.pageTurnaround.Controls.Add(Me.Label7)
        Me.pageTurnaround.Controls.Add(Me.Label15)
        Me.pageTurnaround.Controls.Add(Me.dtpTADate)
        Me.pageTurnaround.Controls.Add(Me.lblTADesc)
        Me.pageTurnaround.Controls.Add(Me.dtpPrevTADate)
        Me.pageTurnaround.Controls.Add(Me.GroupBox1)
        Me.pageTurnaround.Controls.Add(Me.Label10)
        Me.pageTurnaround.Controls.Add(Me.Label12)
        Me.pageTurnaround.Controls.Add(Me.Label29)
        Me.pageTurnaround.Controls.Add(Me.txtTAHrsDown)
        Me.pageTurnaround.Controls.Add(Me.txtTACostLocal)
        Me.pageTurnaround.Controls.Add(Me.cmbTAExceptions)
        Me.pageTurnaround.Controls.Add(Me.Label48)
        Me.pageTurnaround.Controls.Add(Me.txtTAContOCC)
        Me.pageTurnaround.Controls.Add(Me.txtTAMPSSTH)
        Me.pageTurnaround.Controls.Add(Me.Label46)
        Me.pageTurnaround.Controls.Add(Me.txtTAMPSOVTPcnt)
        Me.pageTurnaround.Controls.Add(Me.txtTAContMPS)
        Me.pageTurnaround.Controls.Add(Me.txtTAOCCSTH)
        Me.pageTurnaround.Controls.Add(Me.txtTAOCCOVT)
        Me.pageTurnaround.Controls.Add(Me.txtTALaborCostLocal)
        Me.pageTurnaround.Controls.Add(Me.Label28)
        Me.pageTurnaround.Controls.Add(Me.lblKCurr)
        Me.pageTurnaround.Location = New System.Drawing.Point(4, 22)
        Me.pageTurnaround.Name = "pageTurnaround"
        Me.pageTurnaround.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageTurnaround.Size = New System.Drawing.Size(734, 524)
        Me.pageTurnaround.TabIndex = 1
        Me.pageTurnaround.Text = "Turnaround"
        Me.pageTurnaround.ToolTipText = "Removed for 2014"
        Me.pageTurnaround.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label24.Location = New System.Drawing.Point(210, 285)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(59, 13)
        Me.Label24.TabIndex = 329
        Me.Label24.Text = "Labor Cost"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(212, 345)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(184, 13)
        Me.Label11.TabIndex = 322
        Me.Label11.Text = "--------- Company Personnel ----------"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(456, 386)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(90, 13)
        Me.Label16.TabIndex = 321
        Me.Label16.Text = "Contractor Hours"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(22, 431)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(171, 13)
        Me.Label22.TabIndex = 319
        Me.Label22.Text = "Management, Professional && Staff"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(399, 432)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(44, 13)
        Me.Label17.TabIndex = 318
        Me.Label17.Text = "percent"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(399, 405)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 13)
        Me.Label14.TabIndex = 317
        Me.Label14.Text = "hours"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(270, 311)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(169, 13)
        Me.Label13.TabIndex = 316
        Me.Label13.Text = "(0-None, 1- New Unit, 2 - No T/A)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(234, 238)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 314
        Me.Label8.Text = "Total Costs"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(214, 128)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(97, 13)
        Me.Label7.TabIndex = 313
        Me.Label7.Text = "Turnaround Costs,"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(22, 402)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(135, 13)
        Me.Label15.TabIndex = 143
        Me.Label15.Text = "Operators, Craft && Clerical"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'dtpTADate
        '
        Me.dtpTADate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTADate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTADate.Location = New System.Drawing.Point(302, 55)
        Me.dtpTADate.Name = "dtpTADate"
        Me.dtpTADate.Size = New System.Drawing.Size(120, 21)
        Me.dtpTADate.TabIndex = 312
        '
        'lblTADesc
        '
        Me.lblTADesc.BackColor = System.Drawing.Color.Transparent
        Me.lblTADesc.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTADesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTADesc.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblTADesc.Location = New System.Drawing.Point(6, 0)
        Me.lblTADesc.Name = "lblTADesc"
        Me.lblTADesc.Size = New System.Drawing.Size(722, 36)
        Me.lblTADesc.TabIndex = 149
        Me.lblTADesc.Text = "Table 9 Turnaround Data"
        Me.lblTADesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPrevTADate
        '
        Me.dtpPrevTADate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPrevTADate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPrevTADate.Location = New System.Drawing.Point(432, 55)
        Me.dtpPrevTADate.Name = "dtpPrevTADate"
        Me.dtpPrevTADate.Size = New System.Drawing.Size(120, 21)
        Me.dtpPrevTADate.TabIndex = 311
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(6, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox1.Size = New System.Drawing.Size(176, 187)
        Me.GroupBox1.TabIndex = 171
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Current Turnarounds"
        '
        'ListBox1
        '
        Me.ListBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ListBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.ForeColor = System.Drawing.Color.MediumBlue
        Me.ListBox1.Location = New System.Drawing.Point(6, 20)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(164, 161)
        Me.ListBox1.TabIndex = 171
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(213, 59)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(50, 13)
        Me.Label10.TabIndex = 129
        Me.Label10.Text = "T/A Date"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(213, 96)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(129, 13)
        Me.Label12.TabIndex = 130
        Me.Label12.Text = "Total Hours Down for T/A"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label29.Location = New System.Drawing.Point(212, 311)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(56, 13)
        Me.Label29.TabIndex = 137
        Me.Label29.Text = "Exclusions"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtTAHrsDown
        '
        Me.txtTAHrsDown.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAHrsDown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAHrsDown.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAHrsDown.Location = New System.Drawing.Point(464, 93)
        Me.txtTAHrsDown.Name = "txtTAHrsDown"
        Me.txtTAHrsDown.Size = New System.Drawing.Size(88, 21)
        Me.txtTAHrsDown.TabIndex = 301
        Me.txtTAHrsDown.Tag = "0"
        Me.txtTAHrsDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTACostLocal
        '
        Me.txtTACostLocal.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTACostLocal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTACostLocal.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTACostLocal.Location = New System.Drawing.Point(464, 235)
        Me.txtTACostLocal.Name = "txtTACostLocal"
        Me.txtTACostLocal.Size = New System.Drawing.Size(88, 21)
        Me.txtTACostLocal.TabIndex = 302
        Me.txtTACostLocal.Tag = "1"
        Me.txtTACostLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmbTAExceptions
        '
        Me.cmbTAExceptions.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbTAExceptions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTAExceptions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTAExceptions.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbTAExceptions.Items.AddRange(New Object() {"0", "1", "2"})
        Me.cmbTAExceptions.Location = New System.Drawing.Point(462, 309)
        Me.cmbTAExceptions.Name = "cmbTAExceptions"
        Me.cmbTAExceptions.Size = New System.Drawing.Size(88, 21)
        Me.cmbTAExceptions.TabIndex = 304
        Me.cmbTAExceptions.Tag = ""
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label48.Location = New System.Drawing.Point(323, 386)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(51, 13)
        Me.Label48.TabIndex = 161
        Me.Label48.Text = "Overtime"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtTAContOCC
        '
        Me.txtTAContOCC.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAContOCC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAContOCC.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAContOCC.Location = New System.Drawing.Point(459, 403)
        Me.txtTAContOCC.Name = "txtTAContOCC"
        Me.txtTAContOCC.Size = New System.Drawing.Size(88, 21)
        Me.txtTAContOCC.TabIndex = 309
        Me.txtTAContOCC.Tag = "0"
        Me.txtTAContOCC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTAMPSSTH
        '
        Me.txtTAMPSSTH.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAMPSSTH.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAMPSSTH.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAMPSSTH.Location = New System.Drawing.Point(211, 428)
        Me.txtTAMPSSTH.Name = "txtTAMPSSTH"
        Me.txtTAMPSSTH.Size = New System.Drawing.Size(88, 21)
        Me.txtTAMPSSTH.TabIndex = 307
        Me.txtTAMPSSTH.Tag = "0"
        Me.txtTAMPSSTH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.BackColor = System.Drawing.Color.Transparent
        Me.Label46.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label46.Location = New System.Drawing.Point(220, 373)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(71, 26)
        Me.Label46.TabIndex = 159
        Me.Label46.Text = "Straight-Time" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Hours"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtTAMPSOVTPcnt
        '
        Me.txtTAMPSOVTPcnt.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAMPSOVTPcnt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAMPSOVTPcnt.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAMPSOVTPcnt.Location = New System.Drawing.Point(305, 429)
        Me.txtTAMPSOVTPcnt.Name = "txtTAMPSOVTPcnt"
        Me.txtTAMPSOVTPcnt.Size = New System.Drawing.Size(88, 21)
        Me.txtTAMPSOVTPcnt.TabIndex = 308
        Me.txtTAMPSOVTPcnt.Tag = "1"
        Me.txtTAMPSOVTPcnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTAContMPS
        '
        Me.txtTAContMPS.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAContMPS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAContMPS.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAContMPS.Location = New System.Drawing.Point(459, 429)
        Me.txtTAContMPS.Name = "txtTAContMPS"
        Me.txtTAContMPS.Size = New System.Drawing.Size(88, 21)
        Me.txtTAContMPS.TabIndex = 310
        Me.txtTAContMPS.Tag = "0"
        Me.txtTAContMPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTAOCCSTH
        '
        Me.txtTAOCCSTH.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAOCCSTH.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAOCCSTH.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAOCCSTH.Location = New System.Drawing.Point(211, 402)
        Me.txtTAOCCSTH.Name = "txtTAOCCSTH"
        Me.txtTAOCCSTH.Size = New System.Drawing.Size(88, 21)
        Me.txtTAOCCSTH.TabIndex = 305
        Me.txtTAOCCSTH.Tag = "0"
        Me.txtTAOCCSTH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTAOCCOVT
        '
        Me.txtTAOCCOVT.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAOCCOVT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAOCCOVT.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAOCCOVT.Location = New System.Drawing.Point(305, 402)
        Me.txtTAOCCOVT.Name = "txtTAOCCOVT"
        Me.txtTAOCCOVT.Size = New System.Drawing.Size(88, 21)
        Me.txtTAOCCOVT.TabIndex = 306
        Me.txtTAOCCOVT.Tag = "0"
        Me.txtTAOCCOVT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTALaborCostLocal
        '
        Me.txtTALaborCostLocal.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTALaborCostLocal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTALaborCostLocal.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTALaborCostLocal.Location = New System.Drawing.Point(462, 282)
        Me.txtTALaborCostLocal.Name = "txtTALaborCostLocal"
        Me.txtTALaborCostLocal.Size = New System.Drawing.Size(88, 21)
        Me.txtTALaborCostLocal.TabIndex = 330
        Me.txtTALaborCostLocal.Tag = "1"
        Me.txtTALaborCostLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label28.Location = New System.Drawing.Point(440, 39)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(94, 13)
        Me.Label28.TabIndex = 136
        Me.Label28.Text = "Previous T/A Date"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblKCurr
        '
        Me.lblKCurr.AutoSize = True
        Me.lblKCurr.BackColor = System.Drawing.Color.Transparent
        Me.lblKCurr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKCurr.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblKCurr.Location = New System.Drawing.Point(308, 128)
        Me.lblKCurr.Name = "lblKCurr"
        Me.lblKCurr.Size = New System.Drawing.Size(89, 13)
        Me.lblKCurr.TabIndex = 140
        Me.lblKCurr.Text = "(k local currency)"
        Me.lblKCurr.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'pageTargets
        '
        Me.pageTargets.Controls.Add(Me.dgUnitTargets)
        Me.pageTargets.Controls.Add(Me.lblTargetsHdr)
        Me.pageTargets.Location = New System.Drawing.Point(4, 22)
        Me.pageTargets.Name = "pageTargets"
        Me.pageTargets.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageTargets.Size = New System.Drawing.Size(734, 524)
        Me.pageTargets.TabIndex = 2
        Me.pageTargets.Text = "Performance Targets"
        Me.pageTargets.UseVisualStyleBackColor = True
        '
        'dgUnitTargets
        '
        Me.dgUnitTargets.AllowUserToAddRows = False
        Me.dgUnitTargets.AllowUserToDeleteRows = False
        Me.dgUnitTargets.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgUnitTargets.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgUnitTargets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgUnitTargets.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.USDescription, Me.MetDescription, Me.UnitID, Me.ProcessUnitName, Me.ProcessUnitID, Me.UnitTarget, Me.UnitCurrency, Me.UnitProperty, Me.UnitSortKey, Me.USDecPlaces, Me.MetDecPlaces})
        Me.dgUnitTargets.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgUnitTargets.Location = New System.Drawing.Point(6, 36)
        Me.dgUnitTargets.MultiSelect = False
        Me.dgUnitTargets.Name = "dgUnitTargets"
        Me.dgUnitTargets.RowHeadersWidth = 25
        Me.dgUnitTargets.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgUnitTargets.Size = New System.Drawing.Size(722, 482)
        Me.dgUnitTargets.TabIndex = 946
        '
        'USDescription
        '
        Me.USDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.USDescription.DataPropertyName = "USDescription"
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.USDescription.DefaultCellStyle = DataGridViewCellStyle12
        Me.USDescription.FillWeight = 350.0!
        Me.USDescription.HeaderText = "Target Description"
        Me.USDescription.Name = "USDescription"
        Me.USDescription.ReadOnly = True
        '
        'MetDescription
        '
        Me.MetDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MetDescription.DataPropertyName = "MetDescription"
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetDescription.DefaultCellStyle = DataGridViewCellStyle13
        Me.MetDescription.FillWeight = 350.0!
        Me.MetDescription.HeaderText = "Target Description"
        Me.MetDescription.Name = "MetDescription"
        Me.MetDescription.ReadOnly = True
        '
        'UnitID
        '
        Me.UnitID.DataPropertyName = "UnitID"
        Me.UnitID.HeaderText = "UnitID"
        Me.UnitID.Name = "UnitID"
        Me.UnitID.ReadOnly = True
        Me.UnitID.Visible = False
        '
        'ProcessUnitName
        '
        Me.ProcessUnitName.DataPropertyName = "UnitName"
        Me.ProcessUnitName.HeaderText = "UnitName"
        Me.ProcessUnitName.Name = "ProcessUnitName"
        Me.ProcessUnitName.ReadOnly = True
        Me.ProcessUnitName.Visible = False
        '
        'ProcessUnitID
        '
        Me.ProcessUnitID.DataPropertyName = "ProcessID"
        Me.ProcessUnitID.HeaderText = "ProcessID"
        Me.ProcessUnitID.Name = "ProcessUnitID"
        Me.ProcessUnitID.ReadOnly = True
        Me.ProcessUnitID.Visible = False
        '
        'UnitTarget
        '
        Me.UnitTarget.DataPropertyName = "Target"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.MediumBlue
        Me.UnitTarget.DefaultCellStyle = DataGridViewCellStyle14
        Me.UnitTarget.HeaderText = "Value"
        Me.UnitTarget.Name = "UnitTarget"
        '
        'UnitCurrency
        '
        Me.UnitCurrency.DataPropertyName = "CurrencyCode"
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.MediumBlue
        Me.UnitCurrency.DefaultCellStyle = DataGridViewCellStyle15
        Me.UnitCurrency.HeaderText = "Currency"
        Me.UnitCurrency.Items.AddRange(New Object() {"USD"})
        Me.UnitCurrency.Name = "UnitCurrency"
        '
        'UnitProperty
        '
        Me.UnitProperty.DataPropertyName = "Property"
        Me.UnitProperty.HeaderText = "Property"
        Me.UnitProperty.Name = "UnitProperty"
        Me.UnitProperty.Visible = False
        '
        'UnitSortKey
        '
        Me.UnitSortKey.DataPropertyName = "SortKey"
        Me.UnitSortKey.HeaderText = "Sort Key"
        Me.UnitSortKey.Name = "UnitSortKey"
        Me.UnitSortKey.ReadOnly = True
        Me.UnitSortKey.Visible = False
        '
        'USDecPlaces
        '
        Me.USDecPlaces.DataPropertyName = "USDecPlaces"
        Me.USDecPlaces.HeaderText = "USDecPlaces"
        Me.USDecPlaces.Name = "USDecPlaces"
        Me.USDecPlaces.ReadOnly = True
        Me.USDecPlaces.Visible = False
        '
        'MetDecPlaces
        '
        Me.MetDecPlaces.DataPropertyName = "MetDecPlaces"
        Me.MetDecPlaces.HeaderText = "MetDecPlaces"
        Me.MetDecPlaces.Name = "MetDecPlaces"
        Me.MetDecPlaces.ReadOnly = True
        Me.MetDecPlaces.Visible = False
        '
        'lblTargetsHdr
        '
        Me.lblTargetsHdr.BackColor = System.Drawing.Color.Transparent
        Me.lblTargetsHdr.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTargetsHdr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTargetsHdr.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblTargetsHdr.Location = New System.Drawing.Point(6, 0)
        Me.lblTargetsHdr.Name = "lblTargetsHdr"
        Me.lblTargetsHdr.Size = New System.Drawing.Size(722, 36)
        Me.lblTargetsHdr.TabIndex = 135
        Me.lblTargetsHdr.Text = "Process Unit Performance Targets: "
        Me.lblTargetsHdr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTools
        '
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 4, 0, 8)
        Me.pnlTools.Size = New System.Drawing.Size(742, 6)
        Me.pnlTools.TabIndex = 889
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.btnNew)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1035
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(580, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Process Facilities"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label23.Location = New System.Drawing.Point(235, 212)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(119, 13)
        Me.Label23.TabIndex = 339
        Me.Label23.Text = "Maintenance Overhead"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(236, 184)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(104, 13)
        Me.Label21.TabIndex = 338
        Me.Label21.Text = "Maintenance Capital"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(235, 155)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 13)
        Me.Label1.TabIndex = 337
        Me.Label1.Text = "Maintenance Expense"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtTAExpLocal
        '
        Me.txtTAExpLocal.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAExpLocal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAExpLocal.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAExpLocal.Location = New System.Drawing.Point(464, 152)
        Me.txtTAExpLocal.Name = "txtTAExpLocal"
        Me.txtTAExpLocal.Size = New System.Drawing.Size(88, 21)
        Me.txtTAExpLocal.TabIndex = 340
        Me.txtTAExpLocal.Tag = "1"
        Me.txtTAExpLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTACptlLocal
        '
        Me.txtTACptlLocal.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTACptlLocal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTACptlLocal.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTACptlLocal.Location = New System.Drawing.Point(463, 181)
        Me.txtTACptlLocal.Name = "txtTACptlLocal"
        Me.txtTACptlLocal.Size = New System.Drawing.Size(88, 21)
        Me.txtTACptlLocal.TabIndex = 341
        Me.txtTACptlLocal.Tag = "1"
        Me.txtTACptlLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTAOvhdLocal
        '
        Me.txtTAOvhdLocal.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtTAOvhdLocal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTAOvhdLocal.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtTAOvhdLocal.Location = New System.Drawing.Point(463, 208)
        Me.txtTAOvhdLocal.Name = "txtTAOvhdLocal"
        Me.txtTAOvhdLocal.Size = New System.Drawing.Size(88, 21)
        Me.txtTAOvhdLocal.TabIndex = 342
        Me.txtTAOvhdLocal.Tag = "1"
        Me.txtTAOvhdLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'SA_Configuration
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tcProcessFacilities)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.ForeColor = System.Drawing.Color.MediumBlue
        Me.Name = "SA_Configuration"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.tcProcessFacilities.ResumeLayout(False)
        Me.pageConfiguration.ResumeLayout(False)
        Me.pageConfiguration.PerformLayout()
        Me.pageTurnaround.ResumeLayout(False)
        Me.pageTurnaround.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.pageTargets.ResumeLayout(False)
        CType(Me.dgUnitTargets, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private IsLoaded, FirstTimeStart, HasRights As Boolean
    Private CurrentUnitID As Integer
    Private dvConf As New DataView
    Private dvTA As New DataView
    Private dvPG As New DataView
    Private dvPID As New DataView
    Private dvPT As New DataView
    Private dvUnit As New DataView
    Private dvUnitTargets As New DataView
    Private PeriodStart As Date
    Private CEndDate As Date
    Private ColName As String
    Private USText As String
    Private MetText As String
    Private PrevProcFacility As String
    Private MyParent As Main
    Private UnitDesc As String

    Private RowFilter As String
    Friend ConfigNotSaved As Boolean

    Friend Sub SetDataSet(ByRef tmpds As DataSet, ByVal tmpdsRef As DataSet, ByRef tmpdsUnit As DataSet)
        FirstTimeStart = True
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.ConfigRights

        PeriodStart = MyParent.PeriodStart

        dvConf.Table = MyParent.dsProcess.Tables("Config")
        dvTA.Table = MyParent.dsProcess.Tables("MaintTA")
        dvTA.Sort = "TADate DESC"

        dvPG.Table = tmpdsRef.Tables("ProcessGroup_LU")

        dvPID.Table = tmpdsRef.Tables("ProcessID_LU")
        dvPT.Table = tmpdsRef.Tables("ProcessType_LU")

        dvUnit.Table = MyParent.dsUnitList.Tables(0)
        BindConfig()
        BindTA()

        With dvUnitTargets
            .Table = MyParent.dsProcess.Tables("UnitTargetsNew")
            .Sort = "SortKey ASC"
        End With

        dgUnitTargets.DataSource = dvUnitTargets

        SetUOMandCurrency(MyParent.UOM, MyParent.CurrencyCode, MyParent.KCurrency)

        ConfigNotSaved = False
        IsLoaded = True
    End Sub

    Private Sub HandleFirstTimeEnter(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FirstTimeStart = False
    End Sub

    Private Sub BindTA()
        Dim ctl As Control
        Dim b As Binding
        Dim tb As TextBox
        Dim cmb As ComboBox
        Dim i As Integer

        '20090622 RRH Turnaround Sort
        dvTA.Sort = "TADate DESC"

        With ListBox1
            .DisplayMember = "TADate"
            .ValueMember = "TADate"
            .DataSource = dvTA
        End With

        '20080903 RRH For Each ctl In pnlTACtls.Controls
        For Each ctl In pageTurnaround.Controls
            If TypeOf ctl Is DateTimePicker Then
                b = ctl.DataBindings.Add("Text", dvTA, ctl.Name.Substring(3))
                AddHandler b.Format, AddressOf ShortenDate
                AddHandler ctl.TextChanged, AddressOf HandleDTPChanged
                AddHandler ctl.Enter, AddressOf HandleFirstTimeEnter
            ElseIf TypeOf ctl Is TextBox Then
                tb = CType(ctl, TextBox)
                If IsNumeric(tb.Tag) Then
                    i = CType(tb.Tag, Integer)
                    Select Case i
                        Case 0
                            b = ctl.DataBindings.Add("Text", dvTA, ctl.Name.Substring(3))
                            AddHandler b.Format, AddressOf FormatDoubles0
                        Case 1
                            b = ctl.DataBindings.Add("Text", dvTA, ctl.Name.Substring(3))
                            AddHandler b.Format, AddressOf FormatDoubles1
                        Case 2
                            b = ctl.DataBindings.Add("Text", dvTA, ctl.Name.Substring(3))
                            AddHandler b.Format, AddressOf FormatDoubles2
                    End Select
                    AddHandler tb.TextChanged, AddressOf HandleTBChanged
                    AddHandler tb.Enter, AddressOf HandleFirstTimeEnter
                    AddHandler tb.KeyDown, AddressOf UpDownNavigate
                Else
                    ctl.DataBindings.Add("Text", dvTA, ctl.Name.Substring(3))
                    AddHandler tb.TextChanged, AddressOf HandleTBChanged
                    AddHandler tb.Enter, AddressOf HandleFirstTimeEnter
                End If
            ElseIf TypeOf ctl Is ComboBox Then
                ctl.DataBindings.Add("Text", dvTA, ctl.Name.Substring(3))
                cmb = CType(ctl, ComboBox)
                AddHandler cmb.SelectedIndexChanged, AddressOf HandleCMBChanged
                AddHandler cmb.Enter, AddressOf HandleFirstTimeEnter
            End If
        Next
    End Sub

    Friend Sub SetUOMandCurrency(ByVal UOM As String, ByVal Currency As String, ByVal KCurrency As String)
        lblKCurr.Text = "(" & KCurrency & ")"
        Dim cmbUnit As DataGridViewComboBoxColumn = CType(dgUnitTargets.Columns("UnitCurrency"), DataGridViewComboBoxColumn)
        cmbUnit.Items.Clear()
        cmbUnit.Items.Add("USD")

        If Trim(Currency) <> "USD" Then cmbUnit.Items.Add(Trim(Currency))

        If UOM = "US Units" Then
            ColName = "DisplayTextUS"
            lblUnits.Text = USText
            lblStmUnits.Text = "(klb/h)"
            dgUnitTargets.Columns("USDescription").Visible = True
            dgUnitTargets.Columns("MetDescription").Visible = False
        Else
            ColName = "DisplayTextMet"
            lblUnits.Text = MetText
            lblStmUnits.Text = "(MT/h)"
            dgUnitTargets.Columns("USDescription").Visible = False
            dgUnitTargets.Columns("MetDescription").Visible = True
        End If

    End Sub

    Friend Sub SelectedUnitID(ByVal UnitID As Integer, ByVal Desc As String)
        IsLoaded = False
        CurrentUnitID = UnitID
        RowFilter = "UnitID = " & UnitID
        dvConf.RowFilter = RowFilter
        dvTA.RowFilter = RowFilter
        dvUnitTargets.RowFilter = RowFilter
        UnitDesc = Desc

        pageConfiguration.Enabled = HasRights
        pageTurnaround.Enabled = HasRights
        pageTargets.Enabled = HasRights

        If dvConf.Count = 0 Then
            '20080903 RRH pnlConfig.Visible = False
            lblTADesc.Text = "Table 9 Turnaround Data: " & Desc
            tcProcessFacilities.SelectedTab = pageTurnaround
            pageConfiguration.Enabled = False
            pageTargets.Enabled = False

        ElseIf dvConf.Count = 1 Then
            tcProcessFacilities.SelectedTab = pageConfiguration
            '20080903 RRH pnlConfig.Visible = True
            lblTADesc.Text = "Table 9 Turnaround Data: " & Desc
            lblTargetsHdr.Text = "Process Unit Performance Targets: " & Desc

            Dim ProcessGroup As String = dvConf.Item(0).Item("ProcessGroup").ToString
            Dim ProcessID As String = dvConf.Item(0).Item("ProcessID").ToString

            Dim dv As New DataView(MyParent.MyParent.ds_Ref.Tables("ProcessID_LU"))
            dv.RowFilter = "ProcessID = '" & ProcessID & "'"
            USText = dv.Item(0).Item("DisplayTextUS").ToString
            MetText = dv.Item(0).Item("DisplayTextMet").ToString
            If ColName = "DisplayTextUS" Then lblUnits.Text = USText Else lblUnits.Text = MetText

            If dv.Item(0).Item("SulfurAdj").ToString = "Y" Then
                lblDesignFeedSulfur.Visible = True
                txtDesignFeedSulfur.Visible = True
            Else
                lblDesignFeedSulfur.Visible = False
                txtDesignFeedSulfur.Visible = False
            End If

            SetupComboBoxes(ProcessGroup, ProcessID)

            If ProcessID = "FTCOGEN" Then
                lblRptStmCap.Visible = True
                txtRptStmCap.Visible = True
                lblStmUnits.Visible = True
                lblRptCap.Text = "Electrical Capacity"
            Else
                lblRptStmCap.Visible = False
                txtRptStmCap.Visible = False
                lblStmUnits.Visible = False
                lblRptCap.Text = "Capacity"
            End If
        End If

        If dvTA.Count = 0 Then
            '20080903 RRH pnlTA.Visible = False
            pageTurnaround.Enabled = False
            pageTargets.Enabled = False
        Else
            '20080903 RRH pnlTA.Visible = True
            ListBox1.SelectedIndex = 0
        End If

        IsLoaded = True
    End Sub

    Private Sub BindConfig()

        With cmbProcessGroup
            .DisplayMember = "GroupDesc"
            .ValueMember = "ProcessGroup"
            .DataSource = dvPG
            .DataBindings.Add("SelectedValue", dvConf, "ProcessGroup")
        End With

        With cmbProcessID
            .DisplayMember = "ProcessDesc"
            .ValueMember = "ProcessID"
            .DataSource = dvPID
            .DataBindings.Add("SelectedValue", dvConf, "ProcessID")
        End With

        With cmbProcessType
            .DisplayMember = "ProcessType"
            .ValueMember = "ProcessType"
            .DataSource = dvPT
            .DataBindings.Add("SelectedValue", dvConf, "ProcessType")
        End With

        Dim ctl As Control
        Dim tb As TextBox
        Dim b As Binding
        Dim i As Integer
        ' 20080903 RRH For Each ctl In pnlConfCtls.Controls
        For Each ctl In pageConfiguration.Controls
            If TypeOf ctl Is TextBox Then
                tb = CType(ctl, TextBox)
                If IsNumeric(tb.Tag) Then
                    i = CType(tb.Tag, Integer)
                    Select Case i
                        Case 0
                            b = ctl.DataBindings.Add("Text", dvConf, ctl.Name.Substring(3))
                            AddHandler b.Format, AddressOf FormatDoubles0
                        Case 1
                            b = ctl.DataBindings.Add("Text", dvConf, ctl.Name.Substring(3))
                            AddHandler b.Format, AddressOf FormatDoubles1
                        Case 2
                            b = ctl.DataBindings.Add("Text", dvConf, ctl.Name.Substring(3))
                            AddHandler b.Format, AddressOf FormatDoubles2
                    End Select
                    AddHandler tb.TextChanged, AddressOf HandleTBChanged
                    AddHandler tb.Enter, AddressOf HandleFirstTimeEnter
                Else
                    ctl.DataBindings.Add("Text", dvConf, ctl.Name.Substring(3))
                    AddHandler tb.TextChanged, AddressOf HandleTBChanged
                    AddHandler tb.Enter, AddressOf HandleFirstTimeEnter
                End If
                AddHandler tb.KeyDown, AddressOf UpDownNavigate
            End If
        Next
    End Sub

    Private Sub SetupComboBoxes(ByVal ProcessGroup As String, ByVal ProcessID As String)

        With cmbProcessGroup
            .DataBindings.Clear()
            .DisplayMember = "GroupDesc"
            .ValueMember = "ProcessGroup"
            .DataSource = dvPG
            .DataBindings.Add("SelectedValue", dvConf, "ProcessGroup")
        End With

        With cmbProcessID
            .DataBindings.Clear()
            dvPID.RowFilter = "ProcessGroup = '" & ProcessGroup & "'"
            .DisplayMember = "ProcessDesc"
            .ValueMember = "ProcessID"
            .DataSource = dvPID
            .DataBindings.Add("SelectedValue", dvConf, "ProcessID")
        End With

        If dvPT.Count = 0 Then
            cmbProcessType.Enabled = False
            cmbProcessType.BackColor = Color.Gainsboro
        Else
            cmbProcessType.Enabled = True
            cmbProcessType.BackColor = Color.LemonChiffon
        End If

        With cmbProcessType
            .DataBindings.Clear()
            dvPT.RowFilter = "ProcessID = '" & ProcessID & "'"
            .DisplayMember = "ProcessType"
            .ValueMember = "ProcessType"
            .DataSource = dvPT
            .DataBindings.Add("SelectedValue", dvConf, "ProcessType")
        End With

    End Sub
    Private Sub cmbProcessGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProcessGroup.SelectedIndexChanged
        'MsgBox("cmbProcessGroup_SelectedIndexChanged")

    End Sub
    Private Sub cmbProcessGroup_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProcessGroup.SelectionChangeCommitted
        'MsgBox("cmbProcessGroup_SelectionChangeCommitted")
        dvPID.RowFilter = "ProcessGroup = '" & cmbProcessGroup.SelectedValue.ToString() & "'"

        If dvPT.Count = 0 Then
            cmbProcessType.Enabled = False
            cmbProcessType.BackColor = Color.Gainsboro
        Else
            cmbProcessType.Enabled = True
            cmbProcessType.BackColor = Color.LemonChiffon
        End If

        ChangesMade(btnSave, Nothing, ConfigNotSaved)
    End Sub

    Private Sub cmbProcessID_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProcessID.SelectionChangeCommitted
        dvPT.RowFilter = "ProcessID = '" & cmbProcessID.SelectedValue.ToString & "'"

        If dvPT.Count = 0 Then
            cmbProcessType.Enabled = False
            cmbProcessType.BackColor = Color.Gainsboro
        Else
            cmbProcessType.Enabled = True
            cmbProcessType.BackColor = Color.LemonChiffon
        End If

        ChangesMade(btnSave, Nothing, ConfigNotSaved)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveConfiguration()
    End Sub

    Private Sub DeleteTargets()
        Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "targets for the unit")
        If result = DialogResult.Yes Then
            For i As Integer = 0 To dvUnitTargets.Count
                dvUnitTargets.Item(i)!Target = DBNull.Value
            Next
            ChangesMade(btnSave, Nothing, ConfigNotSaved)
        End If
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        Select Case tcProcessFacilities.SelectedTab.Name
            Case pageConfiguration.Name
                Dim result As DialogResult = ProfileMsgBox("YN", "DELETEPU", "")
                Dim UnitID As Integer = CurrentUnitID
                If result = vbYes Then
                    MyParent.DeleteConfiguration(UnitID)
                    ChangesMade(btnSave, Nothing, ConfigNotSaved)
                End If
            Case pageTurnaround.Name : DeleteTurnaround()
            Case pageTargets.Name : DeleteTargets()
        End Select
    End Sub

    Private Sub ShortenDate(ByVal sender As Object, ByVal cevent As ConvertEventArgs)
        If IsDate(cevent.Value) Then
            Dim mydate As DateTime = CType(cevent.Value, DateTime)
            cevent.Value = mydate.ToShortDateString
        End If
    End Sub

    Private Sub FormatDoubles0(ByVal sender As Object, ByVal cevent As ConvertEventArgs)
        Try
            cevent.Value = FormatDoubles(CType(cevent.Value, Double), 0)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub FormatDoubles1(ByVal sender As Object, ByVal cevent As ConvertEventArgs)
        Try
            cevent.Value = FormatDoubles(CType(cevent.Value, Double), 1)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub FormatDoubles2(ByVal sender As Object, ByVal cevent As ConvertEventArgs)
        Try
            cevent.Value = FormatDoubles(CType(cevent.Value, Double), 2)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ListBox1_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles ListBox1.DrawItem
        Dim g As Graphics = e.Graphics
        Dim dr As DataRowView
        Dim dt As Date
        Dim s As String

        Try
            dr = DirectCast(ListBox1.Items.Item(e.Index), DataRowView)
            dt = Date.Parse(dr.Item("TADate").ToString)
            s = Format(dt, "MMMM dd, yyyy")
        Catch ex As Exception
            s = "<No Date Entered>"
            Trace.WriteLine(ex.ToString)
        End Try

        g.FillRectangle(Brushes.White, e.Bounds)

        If CBool(e.State And DrawItemState.Selected) Then
            g.FillRectangle(Brushes.Gainsboro, e.Bounds)
            g.DrawString(s, ListBox1.Font, Brushes.Black, _
            RectangleF.op_Implicit(e.Bounds))
        Else
            g.DrawString(s, ListBox1.Font, Brushes.MediumBlue, _
            RectangleF.op_Implicit(e.Bounds))
        End If

    End Sub

    Private Sub dtpTADate_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTADate.Leave
        ListBox1.Refresh()
    End Sub

    Private Sub dtpPrevTADate_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPrevTADate.Leave
        ListBox1.Refresh()
    End Sub

    Private Sub HandleTBChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If FirstTimeStart Then Exit Sub
        If IsLoaded = False Then Exit Sub
        If HasRights = False Then Exit Sub
        ChangesMade(btnSave, Nothing, ConfigNotSaved)
    End Sub

    Private Sub HandleCMBChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If FirstTimeStart Then Exit Sub
        If IsLoaded = False Then Exit Sub
        If HasRights = False Then Exit Sub
        ChangesMade(btnSave, Nothing, ConfigNotSaved)
    End Sub

    'Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
    '    If ListBox1.SelectedIndex > 0 Then
    '        TextBoxState(False)
    '    Else
    '        TextBoxState(True)
    '    End If
    'End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click

        Select Case tcProcessFacilities.SelectedTab.Name
            Case pageConfiguration.Name
                Dim MyNewUnit As New AddUnit
                Dim result As DialogResult
                Dim dv As New DataView

                MyParent.gbAbout.Visible = False
                MyParent.pnlBlank.BringToFront()

                dv.Table = MyParent.MyParent.ds_Ref.Tables("ProcessID_LU")
                dv.RowFilter = "ProfileProcFacility = 'Y'"

                With MyNewUnit.ComboBox1
                    .DisplayMember = "ProcessDesc"
                    .ValueMember = "ProcessID"
                    .DataSource = dv
                End With

                result = MyNewUnit.ShowDialog(Me)
                If result = DialogResult.OK Then
                    Dim IsNotThere As Boolean
                    Dim ProcessID As String = MyNewUnit.ComboBox1.SelectedValue.ToString
                    dv.RowFilter = "ProcessID = '" & ProcessID & "'"
                    Dim SortKey As String
                    Dim tmpstr As String = ""
                    SortKey = CStr(dv.Item(0).Item("Sortkey"))
                    Dim i As Integer
                    Do While IsNotThere = False
                        i = i + 1
                        If i < 10 Then tmpstr = SortKey & "0" & i Else tmpstr = SortKey & i
                        dvUnit.RowFilter = "UnitID = " & CInt(tmpstr)
                        If dvUnit.Count = 0 Then
                            IsNotThere = True
                        End If
                    Loop

                    Dim row As DataRow = MyParent.dsUnitList.Tables(0).NewRow
                    row.Item("UnitID") = CInt(tmpstr)
                    MyParent.dsUnitList.Tables(0).Rows.Add(row)

                    Dim UnitName As String = Trim(MyNewUnit.TextBox1.Text)
                    Dim HasTA, TAInt As Integer
                    Dim TADate, PrevTADate As Date
                    If MyNewUnit.HasTA Then
                        HasTA = 1
                        TADate = MyNewUnit.dtpTADate.Value
                        TAInt = MyNewUnit.cmbTAInt.SelectedIndex + 12
                        PrevTADate = DateAdd(DateInterval.Month, -TAInt, TADate)
                    Else
                        HasTA = 2
                    End If

                    row = MyParent.dsProcess.Tables("Config").NewRow
                    row!UnitID = CInt(tmpstr)
                    row!UnitName = UnitName
                    row!SortKey = CInt(SortKey)
                    row!ProcessID = ProcessID
                    row!ProcessGroup = dv.Item(0).Item("ProcessGroup")
                    MyParent.dsProcess.Tables("Config").Rows.Add(row)

                    If MyNewUnit.HasNoMaintDetails = False Then
                        row = MyParent.dsProcess.Tables("MaintTA").NewRow
                        row!UnitID = CInt(tmpstr)
                        row!UnitName = UnitName
                        row!SortKey = CInt(SortKey)
                        row!ProcessID = ProcessID
                        If HasTA = 1 Then
                            row!TAID = 1
                            row!TADate = TADate
                            row!PrevTADate = PrevTADate
                        Else
                            row!TAID = 1
                        End If
                        row!TAExceptions = HasTA
                        MyParent.dsProcess.Tables("MaintTA").Rows.Add(row)
                    End If
                    MyParent.AddProcessUnit(CInt(tmpstr))
                    MyNewUnit.Close()
                    ChangesMade(btnSave, Nothing, ConfigNotSaved)
                Else
                    MyNewUnit.Close()
                End If
                MyParent.MyParent.ControlBox = True
                'MyParent.pnlBlank.Text = lblstr
                Me.BringToFront()
            Case pageTurnaround.Name : CreateTurnaround()
        End Select
    End Sub

    Private Sub CreateTurnaround()
        Dim MyNewTA As New AddTurnaround
        Dim result As DialogResult
        Dim TAID As Integer
        Dim dvUL As New DataView(MyParent.dsUnitList.Tables(0))
        Dim dv As New DataView(MyParent.dsProcess.Tables("Config"))

        Dim prevTADate As Date

        MyParent.gbAbout.Visible = False
        MyParent.pnlBlank.BringToFront()

        If dvTA.Count > 0 Then
            MyNewTA.TextBox1.Text = Format(dvTA.Item(0).Item("TADate"), "d")
            prevTADate = CType(dvTA.Item(0).Item("TADate"), DateTime)
        End If

        MyNewTA.tbUnitName.Text = UnitDesc
        result = MyNewTA.ShowDialog(Me)
        If result = DialogResult.OK Then
            dv.RowFilter = "UnitID = " & CurrentUnitID
            dvTA.RowFilter = "UnitID = " & CurrentUnitID
            dvTA.Sort = "TAID DESC"
            Dim row As DataRow = MyParent.dsProcess.Tables("MaintTA").NewRow
            row!UnitID = CurrentUnitID
            dvUL.RowFilter = "UnitID = " & CurrentUnitID
            dvUL.Sort = "TAID DESC"
            If dvUL.Count > 0 Then TAID = CType(dvUL.Item(0)!TAID, Integer) + 1 Else TAID = 1
            row!TAID = TAID

            If dv.Count = 1 Then
                row!UnitName = dv.Item(0)!UnitName
                row!SortKey = dv.Item(0)!SortKey
                row!ProcessID = dv.Item(0)!ProcessID
            Else
                TAID = CType(dvTA.Item(0)!TAID, Integer)
                row!TAID = TAID + 1
                Select Case CurrentUnitID
                    Case 90051
                        row!ProcessID = "OFFTANK"
                        row!UnitName = "Offsite - Tankage"
                        row!SortKey = 6000
                    Case 90052
                        row!ProcessID = "OFFBLEND"
                        row!UnitName = "Offsite - Coke Handling/Loading"
                        row!SortKey = 6005
                    Case 90053
                        row!ProcessID = "OFFCOKE"
                        row!UnitName = "Offsite - Coke Handling/Loading"
                        row!SortKey = 6010
                    Case 90054
                        row!ProcessID = "OFFOTH"
                        row!UnitName = "Offsite - Other"
                        row!SortKey = 6015
                    Case 90055
                        row!ProcessID = "MARINE"
                        row!UnitName = "Marine Facility"
                        row!SortKey = 6100
                    Case 90056
                        row!ProcessID = "UTILSTM"
                        row!UnitName = "Utilities - Steam/Electrical"
                        row!SortKey = 6200
                    Case 90057
                        row!ProcessID = "UTILENV"
                        row!UnitName = "Utilities - Environmental"
                        row!SortKey = 6205
                    Case 90058
                        row!ProcessID = "UTILOTH"
                        row!UnitName = "Utilities - Other"
                        row!SortKey = 6210
                End Select
            End If

            row!TADate = MyNewTA.dtpTADate.Value

            If (MyNewTA.TextBox1.Text <> "") Then row!PrevTADate = CDate(MyNewTA.TextBox1.Text)

            MyParent.dsProcess.Tables("MaintTA").Rows.Add(row)

            ChangesMade(btnSave, Nothing, ConfigNotSaved)

        End If
        MyNewTA.Close()

        '20090904 RRH - added default TA Date
        dtpPrevTADate.Value = prevTADate

        MyParent.MyParent.ControlBox = True
        Me.BringToFront()
        'MyParent.lblBlank.Text = lblstr
    End Sub

    Private Sub TextBoxState(ByVal Enable As Boolean)
        dtpTADate.Enabled = Enable
        dtpPrevTADate.Enabled = Enable
        txtTAHrsDown.Enabled = Enable
        txtTACostLocal.Enabled = Enable
        'txtTAMatlLocal.Enabled = Enable
        txtTALaborCostLocal.Enabled = Enable
        txtTAExpLocal.Enabled = Enable
        txtTACptlLocal.Enabled = Enable
        txtTAOvhdLocal.Enabled = Enable
        cmbTAExceptions.Enabled = Enable
        txtTAOCCSTH.Enabled = Enable
        txtTAOCCOVT.Enabled = Enable
        txtTAMPSSTH.Enabled = Enable
        txtTAMPSOVTPcnt.Enabled = Enable
        txtTAContOCC.Enabled = Enable
        txtTAContMPS.Enabled = Enable
    End Sub

    Private Sub DeleteTurnaround()

        If ListBox1.Items.Count = 1 Then
            ProfileMsgBox("CRIT", "ONETA", "")
            Exit Sub
        End If

        Dim cindex As Integer = ListBox1.SelectedIndex
        Dim result As DialogResult = ProfileMsgBox("YN", "DELETETA", "")

        If result = DialogResult.Yes Then
            dvTA.Item(cindex).Delete()
            ChangesMade(btnSave, Nothing, ConfigNotSaved)
        End If

    End Sub

    Private Sub SA_Configuration_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FirstTimeStart = True
    End Sub

    Private Sub UpDownNavigate(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Dim ctl, ctl1 As Control
        Dim tb As TextBox = TryCast(sender, TextBox)
        Dim pnl As Panel
        Dim isThere As Boolean
        Dim cTabIndex As Integer = tb.TabIndex
        Dim nTabIndex As Integer
        Dim tmpstr As String

        pnl = TryCast(tb.Parent, Panel)
        tmpstr = CType(pnl.Tag, String)
        Select Case tmpstr
            Case "T1"
                Select Case e.KeyCode
                    Case Keys.Right : nTabIndex = cTabIndex + 1
                    Case Keys.Left : nTabIndex = cTabIndex - 1
                    Case Else : Exit Sub
                End Select
            Case "T9"
                Select Case e.KeyCode
                    Case Keys.Right : nTabIndex = cTabIndex + 1
                    Case Keys.Left : nTabIndex = cTabIndex - 1
                    Case Else : Exit Sub
                End Select
        End Select

        For Each ctl In pnl.Controls
            If (TypeOf ctl Is TextBox Or TypeOf ctl Is ComboBox) And ctl.TabIndex = nTabIndex Then
                If TypeOf ctl Is TextBox And ctl.Visible Then
                    tb = CType(ctl, TextBox)
                    isThere = True
                    Exit For
                End If
                If TypeOf ctl Is ComboBox Or Not (ctl.Visible) Then
                    If e.KeyCode = Keys.Right Then nTabIndex = nTabIndex + 1 Else nTabIndex = nTabIndex - 1
                    For Each ctl1 In pnl.Controls
                        If TypeOf ctl1 Is TextBox And ctl1.TabIndex = nTabIndex Then
                            tb = CType(ctl, TextBox)
                            isThere = True
                            Exit For
                        End If
                    Next
                    Exit For
                End If
            End If
        Next
        If isThere Then
            tb.Focus()
            tb.SelectAll()
        End If
    End Sub

    Private Sub HandleDTPChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If FirstTimeStart Then Exit Sub
        If IsLoaded = False Then Exit Sub
        If HasRights = False Then Exit Sub
        ChangesMade(btnSave, Nothing, ConfigNotSaved)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If ConfigNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Configuration")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveConfiguration()
                Case DialogResult.No
                    MyParent.dsProcess.RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, ConfigNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub dgUnitTargets_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgUnitTargets.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, Nothing, ConfigNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub tcProcessFacilities_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcProcessFacilities.SelectedIndexChanged
        Select Case tcProcessFacilities.SelectedTab.Name
            Case pageTargets.Name
                btnNew.Enabled = False
                btnPreview.Enabled = True
            Case pageConfiguration.Name
                btnPreview.Enabled = False
                btnNew.Enabled = HasRights
            Case pageTurnaround.Name
                btnNew.Enabled = True
                btnPreview.Enabled = True
        End Select
    End Sub

    Private Sub dgUnitTargets_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgUnitTargets.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Select Case tcProcessFacilities.SelectedTab.Name
            Case pageTargets.Name : MyParent.PrintPreviews("Performance Targets", "Process Unit Level")
            Case pageTurnaround.Name : MyParent.PrintPreviews("Table 9", Nothing)
        End Select
    End Sub


    Private Sub cmbProcessType_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProcessType.SelectionChangeCommitted
        ChangesMade(btnSave, Nothing, ConfigNotSaved)

    End Sub
End Class

