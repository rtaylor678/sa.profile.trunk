﻿Imports System.Collections.Generic

Public Class SA_ReportRowsFilter
    'Private _top As Integer = 0
    Private _headerTag As String = String.Empty
    Private _pixelIncrementer As Integer = 20
    Private _pixelTop As Integer = 60 'start here and add cbs below this 
    Private _list As New System.Collections.Generic.List(Of String) '= modGeneral.GetListOfChartRows("Chart_LU")
    Private _rptLayout As DataTable = Nothing

    Private Sub SA_ReportRowsFilter_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub


    Public Sub BuildFilter(storedProcTableName As String)
        _rptLayout = modGeneral.GetReportLayout(storedProcTableName)
        Dim lbl As New System.Windows.Forms.Label()
        lbl.Text = "Choose the rows for your report"
        lbl.Top = 10
        lbl.Left = 10
        lbl.AutoSize = True
        Me.Controls.Add(lbl)

        For Each row As DataRow In _rptLayout.Rows
            _pixelTop += _pixelIncrementer
            Dim indent As Integer = CInt(row("Indent"))
            If IsDBNull(row("IsHeader")) OrElse CInt(row("IsHeader")) = 1 Then
                'If item.StartsWith(Chr(14)) Then   'If item.Contains(Chr(14)) Then
                Dim cHeader As New HeaderCheckbox()
                cHeader.PrepControl(row("Description").ToString(), False, Me)
                cHeader.HeaderName = row("Description").ToString()
                _headerTag = row("Description").ToString()
                cHeader.Top = _pixelTop
                cHeader.Left = 20 + indent * 10
                cHeader.Tag = CInt(row("SortKey"))
                Me.Controls.Add(cHeader)
            Else
                Dim ck As New CheckBox()
                ck.Text = row("Description").ToString()
                'parts(1) is sortkey, which can act as an ID for each record
                ck.Top = _pixelTop
                ck.Left = 20 + indent * 10 ' 30
                ck.AutoSize = True
                ck.Tag = _headerTag & Chr(14) & CInt(row("SortKey"))
                AddHandler ck.Click, AddressOf CheckboxClick
                Me.Controls.Add(ck)
            End If
            '_top += _pixelIncrementer
        Next
    End Sub
    Private Sub AddCB(text As String)
        Dim cb As New System.Windows.Forms.CheckBox()
        cb.Text = text
        _pixelTop += _pixelIncrementer
        cb.Top = _pixelTop
        cb.Left = 20
        cb.AutoSize = True
        Me.Controls.Add(cb)
    End Sub

    Private Sub btnReportRowsFilterGetReport_Click(sender As Object, e As EventArgs) Handles btnReportRowsFilterGetReport.Click
        Dim rowsToIncludeList As String = String.Empty
        Dim oneIsUnchecked As Boolean = False
        For Each ctl As Control In Me.Controls
            'Debug.WriteLine(ctl.Text)
            If TypeOf (ctl) Is CheckBox Then
                Try
                    Dim cb As System.Windows.Forms.CheckBox = TryCast(ctl, System.Windows.Forms.CheckBox)
                    If cb.Checked Then
                        Dim parts As String() = cb.Tag.Split(Chr(14))
                        'Dim text As String = cb.Text
                        rowsToIncludeList += parts(1).PadLeft(3) ' GetOrdinal(text)
                    Else
                        oneIsUnchecked = True
                    End If
                Catch ex As Exception
                    'not a checkbox
                End Try
            ElseIf TypeOf (ctl) Is HeaderCheckbox Then
                Try
                    Dim cb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                    If cb.Checked Then
                        'Dim parts As String() = cb.Tag.Split(Chr(14))
                        'Dim text As String = cb.Text
                        'rowsToIncludeList += cb.Tag.ToString().PadLeft(3) ' parts(0).PadLeft(3) ' GetOrdinal(text)
                    Else
                        oneIsUnchecked = True
                    End If
                Catch ex As Exception
                    'not a checkbox
                End Try
            End If
        Next

        If Not oneIsUnchecked Then rowsToIncludeList = String.Empty 'default for all

        Me.Visible = False
        modGeneral.MainForm.btnRptCreateRoutine(rowsToIncludeList)

    End Sub

    Private Sub btnReportRowsFilterSelectAll_Click(sender As Object, e As EventArgs) Handles btnReportRowsFilterSelectAll.Click
        'instead, check all the header boxes
        Dim checkedValue As Boolean = (btnReportRowsFilterSelectAll.Text.ToUpper = "SELECT ALL")
        For Each ctl As Control In Me.Controls
            If TypeOf (ctl) Is HeaderCheckbox Then
                Try
                    Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                    hcb.Checked = checkedValue '(btnReportRowsFilterSelectAll.Text.ToUpper = "SELECT ALL")
                    If checkedValue Then
                        HeaderCheck(hcb.HeaderName)
                    Else
                        HeaderUncheck(hcb.HeaderName)
                    End If
                Catch ex As Exception
                    'not a checkbox
                End Try
            End If
        Next
        If btnReportRowsFilterSelectAll.Text.ToUpper() = "SELECT ALL" Then
            btnReportRowsFilterSelectAll.Text = "Unselect All"
        Else
            btnReportRowsFilterSelectAll.Text = "Select All"
        End If
    End Sub

    Private Sub HeaderCheck(headerName As String)

        For Each ctl As Control In Me.Controls
            If TypeOf (ctl) Is HeaderCheckbox Then
                Try
                    Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                    If hcb.HeaderName = headerName Then
                        hcb.Checked = True
                        Exit For
                    End If
                Catch ex As Exception
                    'not a checkbox
                End Try
            End If
        Next


        ChangeChildCheckboxes(headerName, True)
        ChangeChildHeaderboxes(headerName, True)

        Dim parentHeaderName As String = String.Empty
        If HasParentHeader(headerName, parentHeaderName) Then
            ParentHeaderRoutine(headerName, parentHeaderName, True)
        End If
    End Sub

    Private Sub HeaderUncheck(headerName As String)
        For Each ctl As Control In Me.Controls
            If TypeOf (ctl) Is HeaderCheckbox Then
                Try
                    Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                    If hcb.HeaderName = headerName Then
                        hcb.Checked = False
                        Exit For
                    End If
                Catch ex As Exception
                    'not a checkbox
                End Try
            End If
        Next

        ChangeChildCheckboxes(headerName, False)
        ChangeChildHeaderboxes(headerName, False)

        Dim parentHeaderName As String = String.Empty
        If HasParentHeader(headerName, parentHeaderName) Then
            ParentHeaderRoutine(headerName, parentHeaderName, False)
        End If
    End Sub

    Private Sub ParentHeaderRoutine(headerName As String, parentHeaderName As String, checkedValue As Boolean)
        Dim siblingHeadersList As List(Of String) = SiblingHeaders(headerName, parentHeaderName)
        Dim allSiblingsAreChecked As Boolean = True
        Dim allSiblingsAreUnchecked As Boolean = True
        For Each siblingHeaderName As String In siblingHeadersList
            For Each ctl As Control In Me.Controls
                If TypeOf (ctl) Is HeaderCheckbox Then
                    Try
                        Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                        If hcb.HeaderName <> headerName Then
                            If hcb.HeaderName = siblingHeaderName Then
                                If Not hcb.Checked Then
                                    allSiblingsAreChecked = False
                                Else
                                    allSiblingsAreUnchecked = False
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        'not a checkbox
                    End Try
                End If
            Next
        Next

        If parentHeaderName.Length > 0 Then
            Dim headerValue As Boolean = False
            If checkedValue = True And allSiblingsAreChecked Then
                headerValue = True
            End If
            For Each ctl As Control In Me.Controls
                If TypeOf (ctl) Is HeaderCheckbox Then
                    Try
                        Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                        If hcb.HeaderName = parentHeaderName Then
                            hcb.Checked = headerValue
                            Exit For
                        End If
                    Catch ex As Exception
                        'do nothing for now
                    End Try
                End If
            Next


        End If

        'If checkedValue = True And allSiblingsAreChecked Then
        '    'if has parent header, then
        '    '  if has sibling header, then
        '    '    if all are checked then 
        '    '       check parent
        '    '       recurse
        '    Dim superParentHeaderName As String = String.Empty
        '    If HasParentHeader(parentHeaderName, superParentHeaderName) Then
        '        ParentHeaderRoutine(parentHeaderName, superParentHeaderName, True)
        '    End If
        'ElseIf checkedValue = False And allSiblingsAreUnchecked Then
        '    'if has parent header, then
        '    '  if has sibling header, then
        '    '    if all are Unchecked then 
        '    '       Uncheck parent
        '    '       recurse
        '    Dim superParentHeaderName As String = String.Empty
        '    If HasParentHeader(parentHeaderName, superParentHeaderName) Then
        '        ParentHeaderRoutine(parentHeaderName, superParentHeaderName, False)
        '    End If
        'End If

    End Sub

    Private Sub ChangeChildCheckboxes(headerName As String, checkedValue As Boolean)
        Dim headerId As Integer = -1
        For Each row As DataRow In _rptLayout.Rows
            If row("Description").ToString() = headerName Then
                headerId = CInt(row("Id"))
                Exit For
            End If
        Next
        Dim sortKeys As New List(Of Integer)
        For Each row As DataRow In _rptLayout.Rows
            If CInt(row("ParentId")) = headerId And CInt(row("IsHeader")) = 0 Then
                sortKeys.Add(CInt(row("SortKey")))
            End If
        Next
        For Each ctl As Control In Me.Controls
            If TypeOf (ctl) Is CheckBox Then
                Try
                    Dim cb As CheckBox = TryCast(ctl, CheckBox)
                    For Each sortKey As Integer In sortKeys
                        Dim parts As String() = cb.Tag.Split(Chr(14))
                        If CInt(parts(1)) = sortKey Then
                            cb.Checked = checkedValue
                            Exit For ' go to next control
                        End If
                    Next
                Catch ex As Exception
                    'do nothing for now
                End Try
            End If
        Next

    End Sub

    Private Sub ChangeChildHeaderboxes(headerName As String, checkedValue As Boolean)
        Dim childHeaderboxes As New List(Of String)
        Dim parentId As Integer = -1
        For Each row As DataRow In _rptLayout.Rows
            If row("Description").ToString() = headerName Then
                parentId = CInt(row("Id"))
                Exit For
            End If
        Next
        For Each row As DataRow In _rptLayout.Rows
            If CInt(row("ParentId")) = parentId And CInt(row("IsHeader")) > 0 Then
                childHeaderboxes.Add(row("Description").ToString())
            End If
        Next
        For Each childHeaderName As String In childHeaderboxes
            If checkedValue Then
                HeaderCheck(childHeaderName)
                ChangeChildHeaderboxes(childHeaderName, checkedValue)
            Else
                HeaderUncheck(childHeaderName)
                ChangeChildHeaderboxes(childHeaderName, checkedValue)
            End If
        Next

    End Sub

    Private Function HasParentHeader(headerName As String, ByRef parentHeaderName As String) As Boolean
        'do the check here
        Dim parentHeaderId As Integer = -1
        For Each row As DataRow In _rptLayout.Rows
            If row("Description").ToString() = headerName Then
                parentHeaderId = CInt(row("ParentId"))
                Exit For
            End If
        Next
        If parentHeaderId > 0 Then
            For Each row As DataRow In _rptLayout.Rows
                If CInt(row("Id")) = parentHeaderId Then
                    parentHeaderName = row("Description").ToString()
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    Private Function SiblingHeaders(headerName As String, parentHeaderName As String) As List(Of String)
        Dim parentHeaderId As Integer = -1
        For Each row As DataRow In _rptLayout.Rows
            If row("Description") = headerName Then
                parentHeaderId = CInt(row("ParentId"))
                Exit For
            End If
        Next
        Dim list As New List(Of String)
        For Each row As DataRow In _rptLayout.Rows
            If CInt(row("ParentId")) = parentHeaderId AndAlso
                row("Description").ToString() <> headerName AndAlso
                CInt(row("IsHeader")) > 0 Then
                ' "row("Description").ToString() <> headerName " => exclude header we are changing from the list of siblings.
                ' "CInt(row("IsHeader")) > 0"  means is header, not checkbox
                list.Add(row("Description").ToString())
            End If
            If CInt(row("Id")) = parentHeaderId Then
                parentHeaderName = row("Description").ToString()
            End If
        Next
        Return list
    End Function

    Private Sub CheckboxCheck(checkboxName As String, checkedValue As Boolean)
        Dim parentHeaderName As String = String.Empty
        Dim siblingCheckBoxSortkeyList As List(Of String) = SiblingCheckboxesSortkeys(checkboxName, parentHeaderName)
        Dim allSiblingsAreChecked As Boolean = True
        Dim allSiblingsAreUnchecked As Boolean = True
        For Each siblingCheckBoxSortkey As String In siblingCheckBoxSortkeyList
            For Each ctl As Control In Me.Controls
                If TypeOf (ctl) Is CheckBox Then
                    Try
                        Dim cb As CheckBox = TryCast(ctl, CheckBox)
                        If cb.Text <> checkboxName Then
                            Dim parts As String() = cb.Tag.Split(Chr(14))
                            If parts(1) = siblingCheckBoxSortkey Then
                                'cb.Checked = checkedValue
                                'Else
                                If Not cb.Checked Then
                                    allSiblingsAreChecked = False
                                Else
                                    allSiblingsAreUnchecked = False
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        allSiblingsAreChecked = False
                        allSiblingsAreUnchecked = False
                    End Try
                End If
            Next
        Next

        If parentHeaderName.Length > 0 Then
            Dim headerValue As Boolean = False
            If checkedValue = True And allSiblingsAreChecked Then
                headerValue = True
            End If
            'HeaderUncheck(parentHeaderName)
            For Each ctl As Control In Me.Controls
                If TypeOf (ctl) Is HeaderCheckbox Then
                    Try
                        Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                        If hcb.HeaderName = parentHeaderName Then
                            hcb.Checked = headerValue
                            Exit For
                        End If
                    Catch ex As Exception
                        'do nothing for now
                    End Try
                End If
            Next

            Dim superParentHeaderName As String = String.Empty
            RecurseParentHeadersUncheck(parentHeaderName, superParentHeaderName, headerValue)

            'If HasParentHeader(parentHeaderName, superParentHeaderName) Then
            '    'ParentHeaderRoutine(parentHeaderName, superParentHeaderName, checkedValue)
            '    For Each ctl As Control In Me.Controls
            '        If TypeOf (ctl) Is HeaderCheckbox Then
            '            Try
            '                Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
            '                If hcb.HeaderName = superParentHeaderName Then
            '                    hcb.Checked = headerValue
            '                    Exit For
            '                End If
            '            Catch ex As Exception
            '                'do nothing for now
            '            End Try
            '        End If
            '    Next
            'End If

        End If



    End Sub

    Private Sub RecurseParentHeadersUncheck(headerName As String, ByRef parentHeaderName As String, checkboxValue As Boolean)
        If HasParentHeader(headerName, parentHeaderName) Then
            For Each ctl As Control In Me.Controls
                If TypeOf (ctl) Is HeaderCheckbox Then
                    Try
                        Dim hcb As HeaderCheckbox = TryCast(ctl, HeaderCheckbox)
                        If hcb.HeaderName = parentHeaderName Then
                            hcb.Checked = checkboxValue
                            Exit For
                        End If
                    Catch ex As Exception
                        'do nothing for now
                    End Try
                End If
            Next
        End If
        If parentHeaderName.Length > 0 Then
            Dim superParentHeaderName As String = String.Empty
            RecurseParentHeadersUncheck(parentHeaderName, superParentHeaderName, checkboxValue)
        End If
    End Sub


    Private Function SiblingCheckboxesSortkeys(checkboxName As String, ByRef parentHeaderName As String) As List(Of String)
        Dim parentHeaderId As Integer = -1
        For Each row As DataRow In _rptLayout.Rows
            If row("Description") = checkboxName Then
                parentHeaderId = CInt(row("ParentId"))
                Exit For
            End If
        Next
        Dim list As New List(Of String)
        For Each row As DataRow In _rptLayout.Rows
            If CInt(row("ParentId")) = parentHeaderId AndAlso
                row("Description").ToString() <> checkboxName AndAlso
                CInt(row("IsHeader")) < 1 Then
                ' "row("Description").ToString() <> checkboxName " => exclude checkbox we are changing from the list of siblings.
                ' "CInt(row("IsHeader")) < 1"  means is checkbox, not header
                list.Add(row("SortKey").ToString())
            End If
            If CInt(row("Id")) = parentHeaderId Then
                parentHeaderName = row("Description").ToString()
            End If
        Next
        Return list
    End Function

    Public Sub CheckboxClick(sender As Object, e As EventArgs)
        CheckboxCheck(sender.Text, sender.Checked)
    End Sub
    Public Sub HeaderClick(headerName As String, checkIt As Boolean) 'sender As Object, e As EventArgs)
        If checkIt Then
            HeaderCheck(headerName)
        Else
            HeaderUncheck(headerName)
        End If
    End Sub

End Class
