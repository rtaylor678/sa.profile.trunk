﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HeaderCheckbox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.checkbox = New System.Windows.Forms.CheckBox()
        Me.label = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'checkbox
        '
        Me.checkbox.AutoSize = True
        Me.checkbox.Location = New System.Drawing.Point(0, 1)
        Me.checkbox.Name = "checkbox"
        Me.checkbox.Size = New System.Drawing.Size(15, 14)
        Me.checkbox.TabIndex = 0
        Me.checkbox.UseVisualStyleBackColor = True
        '
        'label
        '
        Me.label.AutoSize = True
        Me.label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label.Location = New System.Drawing.Point(16, 1)
        Me.label.Name = "label"
        Me.label.Size = New System.Drawing.Size(0, 13)
        Me.label.TabIndex = 1
        '
        'HeaderCheckbox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.label)
        Me.Controls.Add(Me.checkbox)
        Me.Name = "HeaderCheckbox"
        Me.Size = New System.Drawing.Size(687, 16)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents checkbox As System.Windows.Forms.CheckBox
    Friend WithEvents label As System.Windows.Forms.Label

End Class
