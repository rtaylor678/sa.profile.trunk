﻿Public Class HeaderCheckbox

    Private _owningControl As SA_ReportRowsFilter

    Private _controlHeaderName As String
    Public Property HeaderName As String
        Get
            Return _controlHeaderName
        End Get
        Set(value As String)
            _controlHeaderName = value
        End Set
    End Property

    Private _checked As Boolean = False
    Public Property Checked As Boolean
        Get
            Return _checked
        End Get
        Set(value As Boolean)
            _checked = value
            checkbox.Checked = value
        End Set
    End Property

    Public Sub PrepControl(text As String, checked As Boolean, ByRef owningForm As SA_ReportRowsFilter)
        If checked Then
            checkbox.Checked = True
        Else
            checkbox.Checked = False
        End If
        _checked = checked
        label.Text = text
        _owningControl = owningForm
    End Sub

    Public Sub Uncheck()
        _checked = False
        '_owningForm.HeaderClick(_controlHeaderName, False)
        checkbox.Checked = False
    End Sub

    Public Sub checkbox_Clicked(sender As Object, e As EventArgs) Handles checkbox.Click
        _checked = checkbox.Checked
        Dim chk As CheckBox = TryCast(sender, CheckBox)
        _owningControl.HeaderClick(_controlHeaderName, chk.Checked)
    End Sub
End Class
