Option Explicit On
Imports System.Collections.Generic

Friend Class SA_Process
    Private MyParent As Main
    Private HasRights As Boolean
    Private dvWB As New DataView
    Public ProcessNotSaved As Boolean
    Private isloaded As Boolean
    Private DetailsChanged9 As Boolean = False
    Private DetailsChanged10 As Boolean = False

    Friend Sub SetCurrencyAndUOM()
        Dim tmpbool As Boolean
        Try
            RemoveHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged
            RemoveHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged
            dgMaintRout.Columns("RoutCostLocal").HeaderText = "Total Cost (" & MyParent.KCurrency & ")"
            dgMaintRout.Columns("RoutExpLocal").HeaderText = "Expense (" & MyParent.KCurrency & ")"
            dgMaintRout.Columns("RoutCptlLocal").HeaderText = "Capital (" & MyParent.KCurrency & ")"
            dgMaintRout.Columns("RoutOvhdLocal").HeaderText = "Overhead (" & MyParent.KCurrency & ")"
            dgMaintTA.Columns("TACostLocal").HeaderText = "Total Cost (" & MyParent.KCurrency & ")"
            dgMaintTA.Columns("TAExpLocal").HeaderText = "Expense (" & MyParent.KCurrency & ")"
            dgMaintTA.Columns("TACptlLocal").HeaderText = "Capital (" & MyParent.KCurrency & ")"
            dgMaintTA.Columns("TAOvhdLocal").HeaderText = "Overhead (" & MyParent.KCurrency & ")"
            dgMaintTA.Columns("TALaborCostLocal").HeaderText = "Labor (" & MyParent.KCurrency & ")"
            If MyParent.UOM = "US Units" Then tmpbool = True
            dgProcessData.Columns("USDescription").Visible = tmpbool
            dgProcessData.Columns("MetDescription").Visible = Not (tmpbool)
            dgProcessDataMap.Columns("PMapUSDescription").Visible = tmpbool
            dgProcessDataMap.Columns("PMapMetDescription").Visible = Not (tmpbool)
            AddHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged
            AddHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged
        Catch ex As Exception

        End Try
    End Sub

    Friend Sub SetDataSet()
        Try

            DetailsChanged9 = False
            DetailsChanged10 = False
            MyParent = CType(Me.ParentForm, Main)
            HasRights = MyParent.MyParent.ProcessRights

            Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Process Facilities'")
            chkComplete.Checked = CBool(row(0)!Checked)

            Dim dv1 As New DataView(MyParent.dsProcess.Tables("Config"))
            dv1.Sort = "SortKey ASC, UnitID ASC"
            dgConfig.DataSource = dv1

            Dim dv1Map As New DataView(MyParent.dsMappings.Tables("Config"))
            dv1Map.Sort = "SortKey ASC, UnitID ASC"
            dgConfigMap.DataSource = dv1Map

            Dim dv2 As New DataView(MyParent.dsProcess.Tables("ProcessData"))
            dv2.Sort = "SortKey ASC, UnitID ASC"
            dgProcessData.DataSource = dv2

            Dim dv2Map As New DataView(MyParent.dsMappings.Tables("ProcessData"))
            'dv2Map.Sort = "UnitID ASC, SortKey ASC"
            dgProcessDataMap.DataSource = dv2Map

            Dim dv9 As New DataView(MyParent.dsProcess.Tables("MaintTA"))
            ' 20090115 RRH This line causes new Turnarounds to not be displayed: dv9.RowFilter = "TAExceptions <> 2"
            dv9.Sort = "SortKey ASC, UnitID ASC, TADate DESC"
            RemoveHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged
            dgMaintTA.DataSource = dv9
            AddHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged

            Dim dv10 As New DataView(MyParent.dsProcess.Tables("MaintRout"))
            dv10.Sort = "SortKey ASC, UnitID ASC"
            RemoveHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged
            dgMaintRout.DataSource = dv10
            AddHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged

            Dim dv10Map As New DataView(MyParent.dsMappings.Tables("MaintRout"))
            dv10Map.Sort = "SortKey ASC, UnitID ASC"
            dgMaintRoutMap.DataSource = dv10Map

            'tbFilePath.Text = My.Settings.workbookProcess
            'cbSheets.Text = My.Settings.worksheetProcess

            Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
            Dim dr() As DataRow = dt.Select("frmName = 'Process'")

            If (dr.GetUpperBound(0) = -1) Then
            Else
                If Not System.IO.File.Exists(dr(0).Item(tbFilePath.Name).ToString) Then
                    'Throw New Exception("Mapping file problem: Cannot find file at " & dr(0).Item(tbFilePath.Name).ToString)
                End If
                tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
                cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
            End If

            rButtonProfile.Checked = Not My.Settings.viewDetailProcess
            rButtonStudy.Checked = My.Settings.viewDetailProcess

            isloaded = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgConfig_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgConfig.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, ProcessNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgConfigMap_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgConfigMap.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, ProcessNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgProcessData_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgProcessData.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, ProcessNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgProcessDataMap_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgProcessDataMap.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, ProcessNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgMaintRout_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgMaintRout.CellBeginEdit
        If HasRights Then

            ChangesMade(btnSave, chkComplete, ProcessNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub


    Private Sub dgMaintRoutMap_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgMaintRoutMap.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, ProcessNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        If isloaded = False Then Exit Sub
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "Process Facilities")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Process Facilities'")
        row(0)!Checked = chk.Checked

        btnSave.BackColor = Color.Red
        btnSave.ForeColor = Color.White
        ProcessNotSaved = True
    End Sub

#Region " Order Datagrids "

    Private Sub dgConfig_RowPrePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgConfig.RowPrePaint

        For Each c As DataGridViewColumn In dgConfig.Columns
            Select Case c.DataPropertyName
                Case "UnitName" : c.DisplayIndex = 0
                Case "ProcessID" : c.DisplayIndex = 1
                Case "ProcessType" : c.DisplayIndex = 2
                Case "InServicePcnt" : c.DisplayIndex = 3
                Case "RptCap" : c.DisplayIndex = 4
                Case "RptStmCap" : c.DisplayIndex = 5
                Case "UtilPcnt" : c.DisplayIndex = 6
                Case "StmUtilPcnt" : c.DisplayIndex = 7
                Case "EnergyPcnt" : c.DisplayIndex = 8
            End Select
        Next c

        Select Case dgConfig.Rows(e.RowIndex).Cells("ProcessID").Value.ToString()
            Case "FTCOGEN"
                editCell(dgConfig, "StmUtilPcnt", e.RowIndex, True, Color.LemonChiffon) ' Allow changes
                editCell(dgConfig, "EnergyPcnt", e.RowIndex, False, SystemColors.Control) ' Don't Allow changes
            Case "STEAMGEN", "ELECGEN", "FCCPOWER"
                editCell(dgConfig, "EnergyPcnt", e.RowIndex, False, SystemColors.Control) ' Don't Allow changes
        End Select

    End Sub

    Private Sub dgConfigMap_RowPrePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgConfigMap.RowPrePaint
        For Each c As DataGridViewColumn In dgConfigMap.Columns
            Select Case c.DataPropertyName
                Case "UnitName" : c.DisplayIndex = 0
                Case "ProcessID" : c.DisplayIndex = 1
                Case "UtilPcnt" : c.DisplayIndex = 2
                Case "StmUtilPcnt" : c.DisplayIndex = 3
                Case "EnergyPcnt" : c.DisplayIndex = 4
            End Select
        Next c

        Select Case dgConfigMap.Rows(e.RowIndex).Cells("MapProcessID").Value.ToString()
            Case "FTCOGEN"
                editCell(dgConfigMap, "MapStmUtilPcnt", e.RowIndex, True, Color.White) ' Allow changes
                editCell(dgConfigMap, "MapEnergyPcnt", e.RowIndex, False, SystemColors.Control) ' Don't Allow changes
            Case "STEAMGEN", "ELECGEN", "FCCPOWER"
                editCell(dgConfigMap, "MapEnergyPcnt", e.RowIndex, False, SystemColors.Control) ' Don't Allow changes
        End Select
    End Sub

    Private Sub dgProcessData_RowPrePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgProcessData.RowPrePaint
        For Each c As DataGridViewColumn In dgProcessData.Columns
            Select Case c.DataPropertyName
                Case "UnitName" : c.DisplayIndex = 0
                Case "ProcessID" : c.DisplayIndex = 1
                Case "USDescription" : c.DisplayIndex = 2
                Case "MetDescription" : c.DisplayIndex = 3
                Case "RptValue" : c.DisplayIndex = 4
            End Select
        Next c
    End Sub

    Private Sub dgProcessDataMap_RowPrePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgProcessDataMap.RowPrePaint
        For Each c As DataGridViewColumn In dgProcessDataMap.Columns
            Select Case c.DataPropertyName
                Case "UnitName" : c.DisplayIndex = 0
                Case "ProcessID" : c.DisplayIndex = 1
                Case "USDescription" : c.DisplayIndex = 2
                Case "MetDescription" : c.DisplayIndex = 3
                Case "RptValue" : c.DisplayIndex = 4
            End Select
        Next c
    End Sub

    Private Sub dgMaintTA_RowPrePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgMaintTA.RowPrePaint
        For Each c As DataGridViewColumn In dgMaintTA.Columns
            Select Case c.DataPropertyName
                Case "UnitName" : c.DisplayIndex = 0 : Exit Select
                Case "ProcessID" : c.DisplayIndex = 1 : Exit Select
                Case "TADate" : c.DisplayIndex = 2 : Exit Select
                Case "TAHrsDown" : c.DisplayIndex = 3 : Exit Select
                Case "TACostLocal" : c.DisplayIndex = 4 : Exit Select
            End Select
        Next c
    End Sub

    Private Sub dgMaintRout_RowPrePaint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgMaintRout.RowPrePaint
        For Each c As DataGridViewColumn In dgMaintRout.Columns
            Select Case c.DataPropertyName
                Case "ProcessID" : c.DisplayIndex = 0 : Exit Select
                Case "UnitName" : c.DisplayIndex = 1 : Exit Select

                Case "RoutCostLocal" : c.DisplayIndex = 2 : Exit Select

                Case "RoutExpLocal" : c.DisplayIndex = 3 : Exit Select
                Case "RoutCptlLocal" : c.DisplayIndex = 4 : Exit Select
                Case "RoutOvhdLocal" : c.DisplayIndex = 5 : Exit Select

                Case "RegNum" : c.DisplayIndex = 7 : Exit Select
                Case "RegDown" : c.DisplayIndex = 8 : Exit Select


                Case "MaintNum" : c.DisplayIndex = 9 : Exit Select
                Case "MaintDown" : c.DisplayIndex = 10 : Exit Select


                Case "OthNum" : c.DisplayIndex = 11 : Exit Select
                Case "OthDown" : c.DisplayIndex = 12 : Exit Select


                Case "OthDownEconomic" : c.DisplayIndex = 13 : Exit Select
                Case "OthDownExternal" : c.DisplayIndex = 14 : Exit Select
                Case "OthDownUnitUpsets" : c.DisplayIndex = 15 : Exit Select
                Case "OthDownOffsiteUpsets" : c.DisplayIndex = 16 : Exit Select
                Case "OthDownOther" : c.DisplayIndex = 17 : Exit Select

                    'Case "UnpRegNum" : c.DisplayIndex = 18 : Exit Select
                    'Case "UnpRegDown" : c.DisplayIndex = 19 : Exit Select
                    'Case "UnpMaintNum" : c.DisplayIndex = 20 : Exit Select
                    'Case "UnpMaintDown" : c.DisplayIndex = 21 : Exit Select
                    'Case "UnpOthNum" : c.DisplayIndex = 22 : Exit Select
                    'Case "UnpOthDown" : c.DisplayIndex = 23 : Exit Select

            End Select

        Next c
    End Sub

    Private Sub dgMaintRoutMap_RowPrePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgMaintRoutMap.RowPrePaint
        For Each c As DataGridViewColumn In dgMaintRoutMap.Columns
            Select Case c.DataPropertyName
                Case "ProcessID" : c.DisplayIndex = 0 : Exit Select
                Case "UnitName" : c.DisplayIndex = 1 : Exit Select

                Case "RoutCostLocal" : c.DisplayIndex = 2 : Exit Select
                Case "RoutExpLocal" : c.DisplayIndex = 3 : Exit Select
                Case "RoutCptlLocal" : c.DisplayIndex = 4 : Exit Select
                Case "RoutOvhdLocal" : c.DisplayIndex = 5 : Exit Select


                Case "RegNum" : c.DisplayIndex = 6 : Exit Select
                Case "RegDown" : c.DisplayIndex = 7 : Exit Select
                    'Case "RegSlow" : c.DisplayIndex = 8 : Exit Select

                Case "MaintNum" : c.DisplayIndex = 8 : Exit Select
                Case "MaintDown" : c.DisplayIndex = 9 : Exit Select
                    'Case "MaintSlow" : c.DisplayIndex = 11 : Exit Select

                Case "OthNum" : c.DisplayIndex = 11 : Exit Select
                Case "OthDown" : c.DisplayIndex = 12 : Exit Select
                    'Case "OthSlow" : c.DisplayIndex = 14 : Exit Select

                Case "OthDownEconomic" : c.DisplayIndex = 13 : Exit Select
                Case "OthDownExternal" : c.DisplayIndex = 14 : Exit Select
                Case "OthDownUnitUpsets" : c.DisplayIndex = 15 : Exit Select
                Case "OthDownOffsiteUpsets" : c.DisplayIndex = 16 : Exit Select
                Case "OthDownOther" : c.DisplayIndex = 17 : Exit Select

                    'Case "UnpRegNum" : c.DisplayIndex = 18 : Exit Select
                    'Case "UnpRegDown" : c.DisplayIndex = 19 : Exit Select
                    'Case "UnpMaintNum" : c.DisplayIndex = 20 : Exit Select
                    'Case "UnpMaintDown" : c.DisplayIndex = 21 : Exit Select
                    'Case "UnpOthNum" : c.DisplayIndex = 22 : Exit Select
                    'Case "UnpOthDown" : c.DisplayIndex = 23 : Exit Select

            End Select

        Next c

    End Sub

#End Region

#Region " Button Clicks "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveProcess()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        MyParent.EditConfiguration("Process Facilities")
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim result As DialogResult = ProfileMsgBox("YN", "IMPORT", "process facilities")
        If result = DialogResult.Yes Then
            ImportProcessFacilities()
        End If
    End Sub

    Private Function CheckForBadUnitIDs(GoodIds As List(Of Integer), ByVal DataTableToClean As DataTable) As String
        'only using this to log where Config Unitids don't match.
        Try
            Dim blnFound As Boolean = False
            Dim badUnitIds As New List(Of Integer)
            Dim unitIdToCheck As Integer
            For Each row As DataRow In DataTableToClean.Rows
                unitIdToCheck = row("UnitID")
                For Each UnitID As Integer In GoodIds
                    If unitIdToCheck = UnitID Then
                        blnFound = True
                        Exit For
                    End If
                Next
                If Not blnFound Then
                    badUnitIds.Add(unitIdToCheck)
                End If
                blnFound = False
            Next
            If badUnitIds.Count > 0 Then
                Dim message As String = "Warning: found unit ID(s) in Mappings file Config section that don't exist in this month's (" + MyParent.CurrentMonth.ToString() + ") Config submission: "
                For Each badUnitID As Integer In badUnitIds
                    message += badUnitID.ToString() + "  "
                Next
                Return message
            Else
                Return String.Empty
            End If
        Catch
        End Try
    End Function

    Friend Function ImportProcessFacilities() As Boolean
        Dim MyExcel As Object = OpenExcel()
        Dim MyWB As Object = OpenWorkbook(MyExcel, tbFilePath.Text)
        Dim MyWS As Object = OpenSheet(MyExcel, MyWB, cbSheets.Text)
        Dim rtn As Boolean = True

        If MyExcel Is Nothing Then Exit Function
        If MyWB Is Nothing Then Exit Function
        If MyWS Is Nothing Then Exit Function

        Dim row As DataRow
        Dim col As DataColumn
        Dim mrow() As DataRow
        Dim UnitID As Integer
        Try
            Try
                'start checking mappings file's config (later-also do processdata, MaintRout) sections with this month's config unitid list
                'do this because if August month has unitid 11001, which was changed in June, from 10001, then if client reopens June, the 10001 will
                'be back in the mappings file. then client reopes August, and 10001 exists along with 11001, breaking mappings file.
                Dim GoodUnitIDs As New List(Of Integer)
                For Each crow As DataRow In MyParent.dsProcess.Tables("Config").Rows
                    GoodUnitIDs.Add(crow("UnitID"))
                Next
                Dim problems As String = CheckForBadUnitIDs(GoodUnitIDs, MyParent.dsMappings.Tables("Config"))
                If problems.Length > 1 Then
                    Dim applicationName As String = "Profile II" 'MyParent.ProductName
                    Dim methodology As String = String.Empty
                    Dim refID As String = MyParent.RefineryID
                    Dim callerIP As String = String.Empty
                    Dim userid As String = String.Empty
                    Dim computername As String = String.Empty
                    Dim version As String = Application.ProductVersion.ToString
                    Dim errorMessages As String = problems
                    Dim localDomainName As String = String.Empty
                    Dim os_Version As String = String.Empty
                    Dim officeVersion As String = String.Empty
                    Dim service As String = String.Empty
                    Dim localRefineryID As String = refID
                    Dim err As Object = Nothing
                    Dim osVersion As String = String.Empty
                    Dim entityName As String = Nothing
                    Dim periodStart As Date?
                    Dim periodEnd As Date?
                    Dim status As String = "ERROR"
                    Dim browserVersion As String = String.Empty
                    Dim dataImportedFromBridgeFile As String = tbFilePath.Text
                    If MyParent.CurrentMonth.Length = 6 Then
                        periodStart = New Date(MyParent.CurrentMonth.Substring(0, 4), MyParent.CurrentMonth.Substring(4, 2), 1)
                    End If
                    MyParent.WriteToActivityLog(applicationName, methodology, refID, callerIP, userid, computername, version,
                    errorMessages, localDomainName, os_Version, officeVersion, service, "SA_Process.Import*", entityName,
                    periodStart, periodEnd, status, err, osVersion, browserVersion, dataImportedFromBridgeFile)
                End If
            Catch
            End Try

            For Each row In MyParent.dsProcess.Tables("Config").Rows
                UnitID = CInt(row!UnitID)
                mrow = MyParent.dsMappings.Tables("Config").Select("UnitID=" & UnitID)

                For Each col In MyParent.dsProcess.Tables("Config").Columns
                    Select Case col.ColumnName
                        Case "UtilPcnt", "StmUtilPcnt", "EnergyPcnt"
                            Try
                                System.Diagnostics.Debug.WriteLine(col.ColumnName & " | " & row(1).ToString())
                                If col.ColumnName = "StmUtilPcnt" AndAlso row(1).ToString().Trim().ToUpper() <> "FTCOGEN" Then
                                    Dim breakpoint As String = String.Empty
                                Else
                                    'If Not (mrow(0).Item(col.ColumnName) Is DBNull.Value) Then

                                    Dim temp As String = MyWS.Range(mrow(0).Item(col.ColumnName)).value

                                    If Not IsDBNull(mrow(0).Item(col.ColumnName).ToString) AndAlso mrow(0).Item(col.ColumnName).ToString <> "" Then
                                        If IsNumeric(MyWS.Range(mrow(0).Item(col.ColumnName)).value) Then
                                            row(col) = CleanseNumericDataFromBridgeFile(MyWS.Range(mrow(0).Item(col.ColumnName)).Value, cbSheets.Text, mrow(0).Item(col.ColumnName).ToString())
                                        Else
                                            row(col) = DBNull.Value
                                        End If
                                    End If
                                End If
                            Catch ex As Exception
                                Dim breakpoint As String
                                breakpoint = String.Empty
                                'skip exceptions per Richard 2017-07-28
                                'ProfileMsgBox("CRIT", "MAPPING", "Table 1 for the " & Trim(row!UnitName.ToString) & " unit")
                                'GoTo CleanupExcel
                            End Try
                    End Select
                Next
            Next

            For Each row In MyParent.dsProcess.Tables("ProcessData").Rows

                mrow = MyParent.dsMappings.Tables("ProcessData").Select("UnitID=" & CInt(row!UnitID) & " And Property = '" & row!Property.ToString & "'")
                For Each col In MyParent.dsProcess.Tables("ProcessData").Columns
                    Select Case col.ColumnName
                        Case "RptValue"

                            Try
                                If Not IsDBNull(mrow(0).Item(col.ColumnName).ToString) AndAlso mrow(0).Item(col.ColumnName).ToString <> "" Then
                                    If IsNumeric(MyWS.Range(mrow(0).Item(col.ColumnName).ToString).value) Then
                                        row(col) = CleanseNumericDataFromBridgeFile(MyWS.Range(mrow(0).Item(col.ColumnName).ToString).Value, cbSheets.Text, mrow(0).Item(col.ColumnName).ToString())
                                    Else
                                        row(col) = DBNull.Value
                                    End If
                                End If
                            Catch exNull As DBConcurrencyException
                                row(col) = DBNull.Value
                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Table 2 for the " & Trim(row!UnitName.ToString) & " unit")
                                GoTo CleanupExcel
                            End Try

                    End Select
                Next
            Next

            For Each row In MyParent.dsProcess.Tables("MaintRout").Rows
                UnitID = CInt(row!UnitID)
                mrow = MyParent.dsMappings.Tables("MaintRout").Select("UnitID=" & UnitID)
                For Each col In MyParent.dsProcess.Tables("MaintRout").Columns
                    Select Case col.ColumnName
                        Case "UnitID", "ProcessID", "UnitName", "SortKey"
                        Case Else
                            Try
                                'If Not (mrow(0).Item(col.ColumnName) Is DBNull.Value) Then
                                If Not IsDBNull(mrow(0).Item(col.ColumnName).ToString) AndAlso mrow(0).Item(col.ColumnName).ToString <> "" Then
                                    If IsNumeric(MyWS.Range(mrow(0).Item(col.ColumnName)).value) Then
                                        row(col) = CleanseNumericDataFromBridgeFile(MyWS.Range(mrow(0).Item(col.ColumnName)).Value, cbSheets.Text, mrow(0).Item(col.ColumnName).ToString())
                                    Else
                                        row(col) = DBNull.Value
                                    End If
                                End If
                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Table 10 for the " & Trim(row!UnitName.ToString) & " unit")
                                GoTo CleanupExcel
                            End Try
                    End Select
                Next
            Next

            rtn = True
            ChangesMade(btnSave, chkComplete, ProcessNotSaved)

CleanupExcel:
            CloseExcel(MyExcel, MyWB, MyWS)
            MyExcel = Nothing
            MyWB = Nothing
            MyWS = Nothing
            Return rtn

        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "SA_Process.ImportProcessFacilities")
            Exit Function
        End Try
    End Function

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim result As DialogResult
        Dim dg As DataGridView = Nothing
        Dim dt As DataTable = Nothing
        Try
            Select Case tabData.SelectedTab.Name
                Case tpMaintTA.Name : Exit Sub  'CAN'T DELETE FROM HERE EXIT SUB 
                Case tpConfig.Name 'HANDLE TABLE 1
                    If rbData.Checked Then
                        result = ProfileMsgBox("YN", "CLEAR", "Table 1 data")
                        dt = MyParent.dsProcess.Tables("Config")
                        dg = dgConfig
                    Else
                        result = ProfileMsgBox("YN", "CLEAR", "Table 1 links")
                        dt = MyParent.dsMappings.Tables("Config")
                        dg = dgConfigMap
                    End If
                Case tpProcessData.Name 'HANDLE TABLE 2
                    If rbData.Checked Then
                        result = ProfileMsgBox("YN", "CLEAR", "Table 2 data")
                        dt = MyParent.dsProcess.Tables("ProcessData")
                        dg = dgProcessData
                    Else
                        result = ProfileMsgBox("YN", "CLEAR", "Table 2 links")
                        dt = MyParent.dsMappings.Tables("ProcessData")
                        dg = dgProcessDataMap
                    End If
                Case tpMaintRout.Name 'HANDLE TABLE 10
                    If rbData.Checked Then
                        result = ProfileMsgBox("YN", "CLEAR", "Table 10 data")
                        dt = MyParent.dsProcess.Tables("MaintRout")
                        dg = dgMaintRout
                    Else
                        result = ProfileMsgBox("YN", "CLEAR", "Table 10 links")
                        dt = MyParent.dsMappings.Tables("MaintRout")
                        dg = dgMaintRoutMap
                    End If
            End Select

            If result = DialogResult.Yes Then
                For Each row As DataRow In dt.Rows
                    For Each col As DataGridViewColumn In dg.Columns
                        If col.ReadOnly = False Then row.Item(col.DataPropertyName) = DBNull.Value
                    Next
                Next
                ChangesMade(btnSave, chkComplete, ProcessNotSaved)
            End If

        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "SA_Process.btnClearClick")
            Exit Sub
        End Try
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        selectWorkBook(Me.Cursor, tbFilePath, cbSheets, MyParent.MyParent.pnlProgress, dvWB)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If ProcessNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Process Unit Facilities")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveProcess()
                Case DialogResult.No
                    MyParent.dsProcess.RejectChanges()
                    MyParent.dsMappings.Tables("Config").RejectChanges()
                    MyParent.dsMappings.Tables("ProcessData").RejectChanges()
                    MyParent.dsMappings.Tables("MaintRout").RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, ProcessNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click

        Select Case tabData.SelectedTab.Name
            Case tpConfig.Name
                MyParent.PrintPreviews("Table 1", "Process Facilities")
            Case tpProcessData.Name
                MyParent.PrintPreviews("Table 2", "")
            Case tpMaintTA.Name
                MyParent.PrintPreviews("Table 9", "")
            Case tpMaintRout.Name
                MyParent.PrintPreviews("Table 10", "")
        End Select
    End Sub

#End Region

    Private Sub SA_Process_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.btnSave.TabIndex = 0
        Me.btnImport.TabIndex = 1
        Me.btnEdit.TabIndex = 2
        Me.btnClear.TabIndex = 3

        Me.tbFilePath.TabIndex = 4
        Me.btnBrowse.TabIndex = 5
        Me.cbSheets.TabIndex = 6

        Me.tabData.TabIndex = 8
        Me.dgMaintRoutMap.TabIndex = 9
        Me.dgConfigMap.TabIndex = 10
        Me.dgProcessDataMap.TabIndex = 11
        Me.dgMaintTA.TabIndex = 12

        Me.btnPreview.TabIndex = 13

        Me.chkComplete.TabIndex = 17
        Me.btnClose.TabIndex = 18

    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub viewLinksData()
        dgConfigMap.Visible = Not rbData.Checked
        dgProcessDataMap.Visible = Not rbData.Checked
        dgMaintRoutMap.Visible = Not rbData.Checked
        dgProcessData.Visible = rbData.Checked
        dgMaintTA.Visible = rbData.Checked
        dgMaintRout.Visible = rbData.Checked
        dgConfig.Visible = rbData.Checked
        btnPreview.Enabled = rbData.Checked

        'If rbData.Checked Then
        '    lblTable1.Text = "Process Facilities:   DATA"
        '    lblTable2.Text = "Process Data:   DATA"
        '    lblTable9.Text = "Turnaround Maintenance:   DATA"
        '    lblTable10.Text = "Non-Turnaround Maintenance:   DATA"
        'Else
        '    lblTable1.Text = "Process Facilities:   LINKS"
        '    lblTable2.Text = "Process Data:   LINKS"
        '    lblTable9.Text = "Turnaround Maintenance:   N/A"
        '    lblTable10.Text = "Non-Turnaround Maintenance:   LINKS"
        'End If

        viewStudyLevel(rButtonStudy.Checked)

        If isloaded And HasRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)

    End Sub

    Private Sub tbFilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Process", tbFilePath.Name, tbFilePath.Text)
        If isloaded Then ChangesMade(btnSave, chkComplete, ProcessNotSaved)

        My.Settings.workbookProcess = tbFilePath.Text
        My.Settings.Save()
    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Process", cbSheets.Name, cbSheets.Text)
        If isloaded Then ChangesMade(btnSave, chkComplete, ProcessNotSaved)

        My.Settings.worksheetProcess = cbSheets.Text
        My.Settings.Save()
    End Sub

    Private Sub tabData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabData.SelectedIndexChanged
        Select Case tabData.SelectedTab.Name
            Case tpMaintRout.Name : tlpStudyLevel.Visible = True
            Case Else : tlpStudyLevel.Visible = False
        End Select
    End Sub

    Private Sub rButtonStudy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rButtonStudy.CheckedChanged
        viewStudyLevel(rButtonStudy.Checked)
    End Sub

    Private Sub viewStudyLevel(ByVal bVisible As Boolean)

        My.Settings.viewDetailProcess = bVisible
        My.Settings.Save()

        dgMaintRout.CurrentCell = Nothing
        If (Not dgMaintRout.Columns("RegNum") Is Nothing) Then
            dgMaintRout.Columns("RegNum").Visible = bVisible
            dgMaintRout.Columns("MaintNum").Visible = bVisible
            dgMaintRout.Columns("OthNum").Visible = bVisible
            dgMaintRout.Columns("OthDownEconomic").Visible = bVisible
            dgMaintRout.Columns("OthDownExternal").Visible = bVisible
            dgMaintRout.Columns("OthDownUnitUpsets").Visible = bVisible
            dgMaintRout.Columns("OthDownOffsiteUpsets").Visible = bVisible
            dgMaintRout.Columns("OthDownOther").Visible = bVisible
            'dgMaintRout.Columns("UnpRegNum").Visible = bVisible
            'dgMaintRout.Columns("UnpRegDown").Visible = bVisible
            'dgMaintRout.Columns("UnpMaintNum").Visible = bVisible
            'dgMaintRout.Columns("UnpMaintDown").Visible = bVisible
            'dgMaintRout.Columns("UnpOthNum").Visible = bVisible
            'dgMaintRout.Columns("UnpOthDown").Visible = bVisible
        End If

    End Sub

    Private Sub dgConfig_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgConfig.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgProcessDataMap_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgProcessDataMap.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgMaintTA_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgMaintTA.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgMaintRoutMap_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgMaintRoutMap.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgMaintTA_CellValueChanged(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgMaintTA.CellValueChanged
        If Not dgMaintTA.CurrentCell Is Nothing Then
            Dim Current_Row As Integer = dgMaintTA.CurrentCell.RowIndex
            If dgMaintTA.CurrentCell.ColumnIndex = 10 Then
                If MessageBox.Show("Entering the total cost will clear the cost details (Expense, Capital, and Overhead). Press OK if you wish to only report the total or Cancel to keep the details.", "Warning", MessageBoxButtons.OKCancel) = vbOK Then

                    RemoveHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged
                    dgMaintTA.Item(7, Current_Row).Value = 0
                    dgMaintTA.Item(8, Current_Row).Value = 0
                    dgMaintTA.Item(9, Current_Row).Value = 0
                    AddHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged
                    DetailsChanged9 = False
                End If
            End If

            If (dgMaintTA.CurrentCell.ColumnIndex = 7 Or dgMaintTA.CurrentCell.ColumnIndex = 8 Or dgMaintTA.CurrentCell.ColumnIndex = 9) And DetailsChanged9 = False Then
                If MessageBox.Show("Entering cost details (Expense, Capital, and Overhead) will replace the total cost you have entered. Press OK if you wish to only report the details or Cancel to keep the total.", "Warning", MessageBoxButtons.OKCancel) = vbOK Then


                    DetailsChanged9 = True

                End If
                RemoveHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged
                dgMaintTA.Item(10, Current_Row).Value = dgMaintTA.Item(7, Current_Row).Value + dgMaintTA.Item(8, Current_Row).Value + dgMaintTA.Item(9, Current_Row).Value
                AddHandler dgMaintTA.CellValueChanged, AddressOf dgMaintTA_CellValueChanged
            End If

        End If



    End Sub
    Private Sub dgMaintRout_CellValueChanged(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgMaintRout.CellValueChanged
        If Not dgMaintRout.CurrentCell Is Nothing Then
            Dim Current_Row As Integer = dgMaintRout.CurrentCell.RowIndex
            If dgMaintRout.CurrentCell.ColumnIndex = 3 Then
                If MessageBox.Show("Entering the total cost will clear the cost details (Expense, Capital, and Overhead). Press OK if you wish to only report the total or Cancel to keep the details.", "Warning", MessageBoxButtons.OKCancel) = vbOK Then

                    RemoveHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged
                    dgMaintRout.Item(4, Current_Row).Value = 0
                    dgMaintRout.Item(5, Current_Row).Value = 0
                    dgMaintRout.Item(6, Current_Row).Value = 0
                    AddHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged
                    DetailsChanged10 = False
                End If
            End If

            If (dgMaintRout.CurrentCell.ColumnIndex = 4 Or dgMaintRout.CurrentCell.ColumnIndex = 5 Or dgMaintRout.CurrentCell.ColumnIndex = 6) And DetailsChanged10 = False Then
                If MessageBox.Show("Entering cost details (Expense, Capital, and Overhead) will replace the total cost you have entered. Press OK if you wish to only report the details or Cancel to keep the total.", "Warning", MessageBoxButtons.OKCancel) = vbOK Then

                    DetailsChanged10 = True
                End If
                RemoveHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged
                dgMaintRout.Item(3, Current_Row).Value = dgMaintRout.Item(4, Current_Row).Value + dgMaintRout.Item(5, Current_Row).Value + dgMaintRout.Item(6, Current_Row).Value
                AddHandler dgMaintRout.CellValueChanged, AddressOf dgMaintRout_CellValueChanged
            End If


        End If



    End Sub

End Class


