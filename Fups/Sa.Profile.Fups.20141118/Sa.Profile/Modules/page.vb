Option Explicit On

Imports System
Imports System.Data
Imports System.Collections

Friend Class Page
    Private _ds As DataSet
    Private _company As String
    Private _location As String
    Private _reportName As String
    Private _tableName As String
    Private _tableDesc As String
    Private _UOM As String
    Private _currencyCode As String
    Private _month As Integer
    Private _year As Integer
    Private _dt As Date
    Private _replacementValues As Hashtable

    ' 
    ' TODO: Add constructor logic here 
    ' 

    'Friend Sub New()
    'End Sub


    Friend Function createPage(ByVal ds As DataSet, ByVal tableName As String, ByVal company As String, ByVal location As String, ByVal tableDesc As String, ByVal UOM As String, _
    ByVal currencyCode As String, ByVal month As Integer, ByVal year As Integer, ByVal dt As Date, ByVal replacementValues As Hashtable) As String
        _reportName = String.Empty
        _ds = ds
        _company = company
        _location = location
        _tableName = tableName
        _tableDesc = tableDesc
        _UOM = UOM
        _currencyCode = currencyCode
        _month = month
        _year = year
        _dt = dt
        _replacementValues = replacementValues

        Dim htmlPage As String = toHtmlText()
        htmlPage = htmlPage.Replace("@ReportName", tableName)
        htmlPage = htmlPage.Replace("@Description", tableDesc + "<br>" + _company + " - " + _location)
        htmlPage = htmlPage.Replace("@Date", Convert.ToDateTime(dt).ToString("MMMM yyyy"))
        htmlPage = HandleReplacementValues(htmlPage, replacementValues)
        Return htmlPage
    End Function

    Friend Function createPage(ByVal ds As DataSet, ByVal reportName As String, ByVal UOM As String, ByVal currencyCode As String, ByVal replacementValues As Hashtable) As String
        _ds = ds
        _reportName = reportName
        _UOM = UOM
        _currencyCode = currencyCode
        _replacementValues = replacementValues

        Dim htmlPage As String = toHtmlText()
        htmlPage = HandleReplacementValues(htmlPage, replacementValues)
        Return htmlPage
    End Function


    Protected Function HandleReplacementValues(ByVal body As String, ByVal replacementValues As Hashtable) As String

        Dim en As IDictionaryEnumerator = replacementValues.GetEnumerator()
        While en.MoveNext()

            body = body.Replace(DirectCast(en.Key, String), DirectCast(en.Value, String))
        End While

        Return body
    End Function
    ''' <summary> 
    ''' Construct whole page 
    ''' </summary> 
    ''' <returns></returns> 
    Protected Function toHtmlText() As String
        Dim pageBuilder As New System.Text.StringBuilder()
        pageBuilder.Append("<HTML>")
        pageBuilder.Append("<HEAD>")
        pageBuilder.Append("<title></title>")

        pageBuilder.Append("<META http-equiv=""PRAGMA"" content=""NO-CACHE"">")
        pageBuilder.Append("<meta content=""Microsoft Visual Studio .NET 7.1"" name=""GENERATOR"">")
        pageBuilder.Append("<meta content=""Visual Basic .NET 7.1"" name=""CODE_LANGUAGE"">")
        pageBuilder.Append("<meta content=""JavaScript"" name=""vs_defaultClientScript"">")
        pageBuilder.Append("<meta content=""http://schemas.microsoft.com/intellisense/ie5 "" name=""vs_targetSchema"">")
        pageBuilder.Append("<style>.small { FONT-SIZE: 12px; FONT-FAMILY: tahoma }")
        pageBuilder.Append("</style>")
        pageBuilder.Append("<LINK media=""print"" href=""print.css"" type=""text/css"" rel=""StyleSheet"">")
        pageBuilder.Append("</HEAD>")
        pageBuilder.Append("<body style=""PAGE-BREAK-BEFORE: auto"" color=""#FFFFFF"" MS_POSITIONING=""GridLayout"">")
        pageBuilder.Append("<form id=""Form1"" method=""post"" runat=""server"">")
        pageBuilder.Append("<table border=0>")
        pageBuilder.Append("<tr>")
        pageBuilder.Append("<td>")

        'Add Header 
        pageBuilder.Append(buildHeader())


        pageBuilder.Append("</td>")
        pageBuilder.Append("</tr>")
        pageBuilder.Append("<tr>")
        pageBuilder.Append("<td>")

        'Add Data Section 
        pageBuilder.Append(buildBody())

        pageBuilder.Append("</td>")
        pageBuilder.Append("</tr>")
        pageBuilder.Append("</table>")
        pageBuilder.Append("</form>")
        pageBuilder.Append("</body>")
        pageBuilder.Append("</HTML>")

        Return pageBuilder.ToString()
    End Function

    ''' <summary> 
    ''' Construct page header 
    ''' </summary> 
    ''' <returns></returns> 
    Protected Function buildHeader() As String
        Dim htmlText As String = ""
        If System.Configuration.ConfigurationManager.AppSettings("Header") IsNot Nothing Then
            Dim filename As String = System.Configuration.ConfigurationManager.AppSettings("Header")
            ' 20081001 RRH Path - filename = frmMain.AppPath + "//" + filename
            filename = pathStartUp + "/" + filename
            Dim fsHelper As New FileHelper()
            htmlText = fsHelper.ReadFile(filename)
        End If

        Return htmlText
    End Function

    ''' <summary> 
    ''' Construct body 
    ''' </summary> 
    ''' <returns></returns> 
    Protected Function buildBody() As String
        Dim factory As New ReportFactory()
        Dim htmlOutput As String

        If _reportName.Length > 0 Then
            htmlOutput = factory.createReport(_ds, _reportName, _UOM, _currencyCode, _replacementValues)
        Else
            htmlOutput = factory.createReport(_ds, _tableName + ";" + _tableDesc, _company, _location, _UOM, _currencyCode, _
            _month, _year, _replacementValues)
        End If

        Return htmlOutput
    End Function

End Class

