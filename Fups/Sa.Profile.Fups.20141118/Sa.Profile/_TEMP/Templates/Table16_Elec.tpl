<!-- config-start -->
FILE=_CONFIG/Energy.xml;
FILE=_REF/Energy_LU.xml;

<!-- config-end -->
<!-- template-start -->
 

<table class='small' width="100%" border="0">
  <tr>
    <td colspan=9></td>
  </tr>
  <tr>
    <td width=2%></td>
    <td width=2%></td>
    <td width=51%></td>
    <td width=10% align=center valign=bottom><strong>Electricity<br/>Megawatt-hours</strong></td>
    <td width=5%></td>
    <td width=8% align=center valign=bottom><strong>Energy Cost<br />CurrencyCode/kWh</strong></td>
    <td width=7%></td>
    <td width=10% align=center valign=bottom><strong>Efficiency of Electrical Production Facilities<br />USEUNIT('Btu','MJ')/kWh</strong></td>
    <td width=5%></td>
  </tr>
  

  <tr>
    <td height=37 valign=bottom align=left colspan=9><strong>Total Electrical Sources</strong></td>
  </tr>

SECTION(Electric, SortKey=101,)
 BEGIN
  <tr>
    <td align=right></td>
    <td colspan=2>Purchased Electricity from Others</td>
    <td align=right>Format(RptMWH,'#,##0')</td>
    <td align=right></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td align=right></td>
    <td align=right></td>
    <td align=right></td>
  </tr>
  END

  <tr>
    <td align=right></td>
    <td valign=bottom align=left colspan=8>Produced Electricity</td>
  </tr>
  
  SECTION(Electric,SortKey=102,)
  BEGIN
  <tr>
    <td align=right></td>
    <td align=right></td>
    <td>Electric Power Generation</td>
    <td align=right>Format(RptMWH,'#,##0')</td>
    <td align=right></td>
    <td align=right></td>
    <td align=right></td>
    <td align=right>NoShowZero(Format(RptGenEff,'#,##0.0'))</td>
    <td align=right></td>
  </tr>
  END
  SECTION(Electric,SortKey=103,)
  BEGIN
  <tr>
    <td align=right></td>
    <td align=right></td>
    <td>Cogeneration Plants</td>
    <td align=right>Format(RptMWH,'#,##0')</td>
    <td align=right></td>
    <td align=right></td>
    <td align=right></td>
    <td align=right>NoShowZero(Format(RptGenEff,'#,##0'))</td>
    <td align=right></td>
  </tr>
  END
   SECTION(Electric,SortKey=104,)
  BEGIN
  <tr>
    <td align=right></td>
    <td align=right></td>
    <td>Combustion Sources</td>
    <td align=right>Format(RptMWH,'#,##0')</td>
    <td align=right></td>
    <td align=right></td>
    <td align=right></td>
    <td align=right>NoShowZero(Format(RptGenEff,'#,##0'))</td>
    <td align=right></td>
  </tr>
  END
  SECTION(Electric,SortKey=105,)
  BEGIN
  <tr>
    <td align=right></td>
    <td colspan=8 height=37 valign=bottom>Electricity Distributed to</td>
  </tr>
  <tr>
    <td align=right></td>
    <td align=right></td>
    <td>Fuels Refinery from Affiliates</td>
    <td align=right>Format(RptMWH,'#,##0')</td>
    <td align=right></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td align=right></td>
    <td align=right></td>
    <td align=right></td>
  </tr>
  END
  
  SECTION(Electric,SortKey=106,)
  BEGIN
  <tr>
    <td align=right></td>
    <td colspan=8 valign=bottom>Electricity Distributed to</td>
  </tr>
  <tr>
    <td align=right></td>
    <td align=right></td>
    <td>Affiliates from Fuels Refinery</td>
    <td align=right>Format(RptMWH,'#,##0')</td>
    <td align=right></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td align=right></td>
    <td align=right></td>
    <td align=right></td>
  </tr>
  END
  SECTION(Electric,SortKey=107,)
  BEGIN
  <tr>
    <td align=right></td>
    <td valign=bottom colspan=2>Electricity Sold to Others</td>
    <td align=right>Format(RptMWH,'#,##0')</td>
    <td align=right></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td align=right></td>
    <td align=right></td>
    <td align=right></td>
  </tr>
  END
</table>
<!-- template-end -->
