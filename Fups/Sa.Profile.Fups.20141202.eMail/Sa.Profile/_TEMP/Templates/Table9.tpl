<!-- config-start -->
FILE=_CONFIG/Process.xml;
FILE=_REF/Process_LU.xml;

<!-- config-end -->
<!-- template-start -->
<table class='small' width=1000 border=0>
  <tr>
    <td width=5></td>
    <td></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
	<td width=50></td>
	<td width=50></td>
	<td width=50></td>
  </tr>

  <tr>
    <td colspan=4></td>
	<td colspan=3 align=center><strong>Previous</strong></td>
    <td colspan=4 align=center><strong>-------- Company Personnel ---------</strong></td>
    <td colspan=4></td>
  </tr>
  <tr>
    <td colspan=3></td>
    <td colspan=5 align=center><strong>----- T/A Costs -----</strong></td>
    <td colspan=2 align=center><strong>---- O,C&amp;C ----</strong></td>
    <td colspan=2 align=center><strong>---- M,P&amp;S ----</strong></td>
    <td colspan=2 align=center><strong>---- Contract ---</strong></td>
    
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td align=center><strong>Turnaround</strong></td>
    <td align=center ><strong>Total Hours</strong></td>
	<td colspan=4 align=center><strong>(CurrencyCode/1000)</strong></td>
    <td align=center><strong>Straight</strong></td>
    <td align=center><strong>Overtime</strong></td>
    <td align=center><strong>Straight</strong></td>
    <td align=center><strong>Overtime</strong></td>
    <td align=center><strong>O,C&amp;C</strong></td>
    <td align=center><strong>M,P&amp;S</strong></td>
	<td align=center><strong>Labor</strong></td>
    <td align=center><strong>Turnaround</strong></td>
    <td></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td align=center><strong>Date</strong></td>
    <td align=right><strong>Down</strong></td>
   
	<td align=right><strong>Expense</strong></td>
	<td align=right><strong>Capital</strong></td>
	<td align=right><strong>Ovrhd</strong></td>
	<td align=center><strong>Total</strong></td>
    <td align=center><strong>Hours</strong></td>
    <td align=center><strong>Hours</strong></td>
    <td align=center><strong>Hours</strong></td>
    <td align=center><strong>Percent</strong></td>
    <td align=center><strong>Hours</strong></td>
    <td align=center><strong>Hours</strong></td>
	 <td align=center><strong>Costs</strong></td>
    <td align=center><strong>Date</strong></td>
    <td align=right><strong>Ex.</strong></td>
  </tr>

  SECTION(MaintTA,UnitID<>'90051' AND UnitID<>'90052' AND UnitID <>'90053' AND UNitID<>'90054' AND UnitID <> '90055' AND UnitID <> '90056' AND UnitID <> '90057' AND UnitID <> '90058',)
  	RELATE(ProcessID_LU,MaintTA,SortKey,SortKey)
  BEGIN
  HEADER('
  <tr>
    <td colspan=14 height=30 valign=bottom><strong> @ProcessDesc </strong></td>
  </tr> ')
  <tr>
    <td></td>
    <td> @UnitName </td>
    <td align=center>Format(TADate,'d')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>
   
   
	<td align=right>Format(TAExpLocal,'#,##0.0')</td>
	<td align=right>Format(TACptlLocal,'#,##0.0')</td>
	<td align=right>Format(TAOvhdLocal,'#,##0.0')</td>
	 <td align=right>Format(TACostLocal,'#,##0.0')</td>
    <td align=right>Format(TAOCCSTH,'#,##0')</td>
    <td align=right>Format(TAOCCOVT,'#,##0')</td>
    <td align=right>Format(TAMPSSTH,'#,##0')</td>
    <td align=right>Format(TAMPSOVTPcnt,'#,##0.0')</td>
    <td align=right>Format(TAContOCC,'#,##0')</td>
    <td align=right>Format(TAContMPS,'#,##0')</td>
	 <td align=right>Format(TALaborCostLocal,'#,##0.0')</td>
    <td align=center>Format(PrevTADate,'d')</td>
    <td align=right>NoShowZero(Format(TAExceptions,'#,##0'))</td>
  </tr>
  END

  <tr>
    <td colspan=14 height=30 valign=bottom><strong>Off-sites</strong></td>
  </tr> 

SECTION(MaintTA,UnitID='90051' OR UnitID='90052' OR UnitID='90053' OR UNitID='90054',)
  BEGIN
  <tr>
    <td></td>
    <td> @UnitName </td>
    <td align=center>Format(TADate,'d')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>
 
    
	<td align=right>Format(TAExpLocal,'#,##0.0')</td>
	<td align=right>Format(TACptlLocal,'#,##0.0')</td>
	<td align=right>Format(TAOvhdLocal,'#,##0.0')</td>
	   <td align=right>Format(TACostLocal,'#,##0.0')</td>
    <td align=right>Format(TAOCCSTH,'#,##0')</td>
    <td align=right>Format(TAOCCOVT,'#,##0')</td>
    <td align=right>Format(TAMPSSTH,'#,##0')</td>
    <td align=right>Format(TAMPSOVTPcnt,'#,##0.0')</td>
    <td align=right>Format(TAContOCC,'#,##0')</td>
    <td align=right>Format(TAContMPS,'#,##0')</td>
	<td align=right>Format(TALaborCostLocal,'#,##0.0')</td>
    <td align=center>Format(PrevTADate,'d')</td>
    <td align=right>NoShowZero(Format(TAExceptions,'#,##0'))</td>
  </tr>
  END
  <tr>
    <td colspan=14 height=30 valign=bottom><strong>Marine</strong></td>
  </tr> 

SECTION(MaintTA,UnitID='90055',)
  BEGIN
  <tr>
    <td></td>
    <td> @UnitName </td>
    <td align=center>Format(TADate,'d')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>
 
    
	<td align=right>Format(TAExpLocal,'#,##0.0')</td>
	<td align=right>Format(TACptlLocal,'#,##0.0')</td>
	<td align=right>Format(TAOvhdLocal,'#,##0.0')</td>
	   <td align=right>Format(TACostLocal,'#,##0.0')</td>
    <td align=right>Format(TAOCCSTH,'#,##0')</td>
    <td align=right>Format(TAOCCOVT,'#,##0')</td>
    <td align=right>Format(TAMPSSTH,'#,##0')</td>
    <td align=right>Format(TAMPSOVTPcnt,'#,##0.0')</td>
    <td align=right>Format(TAContOCC,'#,##0')</td>
    <td align=right>Format(TAContMPS,'#,##0')</td>
	<td align=right>Format(TALaborCostLocal,'#,##0.0')</td>
    <td align=center>Format(PrevTADate,'d')</td>
    <td align=right>Format(TAExceptions,'#,##0')</td>
  </tr>
  END
  <tr>
    <td colspan=14 height=30 valign=bottom><strong>Utilities</strong></td>
  </tr> 

SECTION(MaintTA,UnitID='90056'OR UnitID='90057' OR UnitID='90058',)
  BEGIN
  <tr>
    <td></td>
    <td> @UnitName </td>
    <td align=center>Format(TADate,'d')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>

    
	<td align=right>Format(TAExpLocal,'#,##0.0')</td>
	<td align=right>Format(TACptlLocal,'#,##0.0')</td>
	<td align=right>Format(TAOvhdLocal,'#,##0.0')</td>
	    <td align=right>Format(TACostLocal,'#,##0.0')</td>
    <td align=right>Format(TAOCCSTH,'#,##0')</td>
    <td align=right>Format(TAOCCOVT,'#,##0')</td>
    <td align=right>Format(TAMPSSTH,'#,##0')</td>
    <td align=right>Format(TAMPSOVTPcnt,'#,##0.0')</td>
    <td align=right>Format(TAContOCC,'#,##0')</td>
    <td align=right>Format(TAContMPS,'#,##0')</td>
	<td align=right>Format(TALaborCostLocal,'#,##0.0')</td>
    <td align=center>Format(PrevTADate,'d')</td>
    <td align=right>Format(TAExceptions,'#,##0')</td>
  </tr>
  END

</table>
<!-- template-end -->
