﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace ProfileBusinessManager.Classes
{
    public class Policy
    {

        internal string GetFileName(string directoryPath)
        {
            string fileName = string.Empty;

            foreach (string f in System.IO.Directory.GetFiles(directoryPath, "*.ct"))
            {
                if (System.IO.File.Exists(f))
                {
                    fileName = System.IO.Path.GetFileName(f);
                    break;
                }
            }

            return fileName;
        }


    }
}
