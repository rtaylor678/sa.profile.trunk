<!-- config-start -->
FILE=_CONFIG/Targets.xml;
<!-- config-end -->
<!-- template-start -->

<table class=small border=0 width=100%>
   <tr>
      <td width=2%></td>
      <td width=58%></td>
      <td width=20%></td>
      <td width=5%></td>
      <td width=15%></td>
   </tr>
   <tr>
      <td colspan=2 align=left><strong></strong></td>
      <td colspan=2 align=center><strong>Target</strong></td>
      <td colspan=1 align=center><strong>Currency</strong></td>
   </tr>

SECTION(UnitTargetsNew,,UnitID ASC,SortKey ASC)
 BEGIN
   Header('<tr>
      <td colspan=5 align=left height=30 valign=bottom><strong>@ProcessID: @UnitName</strong></td>
   </tr>')
   <tr>
      <td></td>
      <td align=left valign=bottom> USEUNIT(USDescription,MetDescription) </td>
      <td align=right valign=bottom>NoShowZero(Format(@Target,USEUNIT(USDecPlaces,MetDecPlaces)))</td>
      <td></td>
      <td align=center valign=bottom>NoShowZero(@CurrencyCode)</td>
   </tr>
END
</table>
<!-- template-end -->
