Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports System.IO
Imports GenrateInTableHtml

Public Class ReportControl
    Inherits System.Web.UI.Page
    'Private Const C_HTTP_HEADER_CONTENT As String = "Content-Disposition"
    'Private Const C_HTTP_ATTACHMENT As String = "attachment;filename="
    'Private Const C_HTTP_INLINE As String = "inline;filename="
    'Private Const C_HTTP_CONTENT_TYPE_OCTET As String = "application/octet-stream"
    'Private Const C_HTTP_CONTENT_TYPE_EXCEL As String = "application/ms-excel"
    'Private Const C_HTTP_CONTENT_LENGTH As String = "Content-Length"
    'Private Const C_QUERY_PARAM_CRITERIA As String = "Criteria"
    'Protected WithEvents C1SubMenu1 As C1.Web.C1Command.C1SubMenu
    'Private Const C_ERROR_NO_RESULT As String = "Data not found."
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents phReport As System.Web.UI.WebControls.PlaceHolder
    Protected refineryId As String
    Protected baseControl As Control


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        System.Net.ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy

        If Not ValidateKey() And Not IsPostBack Then
            Server.Transfer("InvalidUser.htm")
        End If

        'Get refinery id from key
        refineryId = GetRefineryID()
    End Sub

    Private Function GetRefineryID() As String
        Dim refId As String
        Dim refLocation As Integer = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray).Length - 1
        refId = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray)(refLocation)
        Return refId
    End Function

    Private Function ValidateKey() As Boolean
        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(Request.Headers.Get("WsP")) Then
                company = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray)(0)

                locIndex = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray).Length - 2
                location = Decrypt(Request.Headers.Get("WsP")).Split("$".ToCharArray)(locIndex)

                If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Else
                    Return False
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = Request.Headers.Get("WsP").Trim)
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Function AreCalcsDone() As Boolean
        Dim sqlstmt As String = "SELECT Distinct SubmissionID,CalcsNeeded FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'"

        Dim ds As DataSet = QueryDb(sqlstmt)
        Dim calcsNeeded As Object

        If ds.Tables(0).Rows.Count > 0 Then
            calcsNeeded = ds.Tables(0).Rows(0)("CalcsNeeded")
            Return IsDBNull(calcsNeeded) Or IsNothing(calcsNeeded)
        Else
            Return False
        End If
    End Function


    Public Sub GetReport()
        If AreCalcsDone() Then
            Dim ds As New DataSet

            ' GMO 10/16/2007
            Dim tReportName As String = Request.QueryString("rn").ToUpper
            Dim dStartingDate As Date
            Dim dFirstPreviousDate As Date
            Dim dSecondPreviousDate As Date
            Dim dThirdPreviousDate As Date
            Dim tReportDataFilter As String
            ' END GMO

            ' GMO 12/5/2007
            Dim tReportTitle As String
            ' END GMO

            Select Case Request.QueryString("rn").ToUpper


                Case "VALIDATION REPORT"
                    Dim dt As DataTable = QueryDb("SELECT dc.DataCheckID, lu.Header, lu.DataCheckText, ItemDescHeader, " & _
                                                  "Value1Header, Value2Header, value3Header, Value4Header, Value5Header, " & _
                                                  "Value6Header, Value7Header, dc.ItemSortKey, dc.ItemDesc, " & _
                                                  "Value1, Value2, value3, Value4, Value5, Value6, Value7 " & _
                                                  "FROM DataChecks dc INNER JOIN DataCheck_LU lu ON " & _
                                                  "dc.DataCheckID = lu.DataCheckID INNER JOIN Submissions s ON " & _
                                                  "dc.SubmissionID = s.SubmissionID WHERE Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + _
                                                  " AND Year(PeriodStart)=" + Year(CDate(Request.QueryString("sd"))).ToString + _
                                                  " AND RefineryID='" + refineryId + "' ORDER BY lu.RptOrder, dc.ItemSortKey ").Tables(0)
                    dt.TableName = "DataCheck"
                    ds.Merge(dt)

                Case "REFINERY SCORECARD"
                    'Dim dt As DataTable = QueryDb("SELECT ChartTitle,SectionHeader,SortKey," & _
                    '                            " CASE WHEN LEN(AxisLabelUS) > 0 THEN ', ' + AxisLabelUS ELSE AxisLabelUS END as AxisLabelUS , " & _
                    '                            " CASE WHEN LEN(AxisLabelMetric) > 0 THEN ', ' + AxisLabelMetric  ELSE AxisLabelMetric END as AxisLabelMetric,DataTable, " & _
                    '                            " CASE WHEN RIGHT(TargetField,6) = 'Target' THEN SUBSTRING(TargetField, 1, CHARINDEX('_Target',TargetField)-1) ELSE ValueField1 END AS ValueField1," & _
                    '                            " CASE WHEN RIGHT(TargetField,6) = 'Target' THEN TargetField ELSE ' ' END AS TargetField , " & _
                    '                            " CASE WHEN RIGHT(TargetField,6) = 'Target' THEN YTDField ELSE ' ' END AS YTDField , " & _
                    '                            " CASE WHEN RIGHT(TargetField,6) = 'Target' THEN AvgField ELSE ' ' END AS AVGField , " & _
                    '                            " DecPlaces " & _
                    '                            " FROM Chart_LU ").Tables(0)

                    Dim dt As DataTable = QueryDb("SELECT ChartTitle,SectionHeader,SortKey," & _
                                               " CASE WHEN LEN(AxisLabelUS) > 0 THEN ', ' + AxisLabelUS ELSE AxisLabelUS END as AxisLabelUS , " & _
                                               " CASE WHEN LEN(AxisLabelMetric) > 0 THEN ', ' + AxisLabelMetric  ELSE AxisLabelMetric END as AxisLabelMetric,DataTable, " & _
                                               " TotField AS ValueField1," & _
                                               " TargetField, " & _
                                               " YTDField, " & _
                                               " AVGField, " & _
                                               " DecPlaces " & _
                                               " FROM Chart_LU ").Tables(0)
                    ' Dim dt As DataTable = QueryDb("SELECT * FROM Chart_LU ").Tables(0)
                    dt.TableName = "Chart_LU"
                    ds.Merge(dt)

                    dt = QueryDb("SELECT * FROM MAINTAVAILCALC WHERE SubmissionID IN " + _
                          "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                          "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                          Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + GetRefineryID() + "') " + _
                          " AND FactorSet='" + Request.QueryString("yr") + "'").Tables(0)
                    dt.TableName = "MAINTAVAILCALC"
                    ds.Merge(dt)


                    dt = QueryDb("SELECT * FROM GENSUM WHERE SubmissionID IN " + _
                        "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                        "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                        Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + GetRefineryID() + "') " + _
                        " AND Currency='" + Request.QueryString("Currency") + "' AND FactorSet='" + Request.QueryString("yr") + "' and UOM='" + Request.QueryString("UOM") + "'").Tables(0)
                    dt.TableName = "GENSUM"
                    ds.Merge(dt)


                    dt = QueryDb("SELECT * FROM UserDefined WHERE SubmissionID" + _
                                " IN (SELECT SubmissionID FROM Submissions " + _
                                " WHERE RefineryID ='" + GetRefineryID() + "' )").Tables(0)
                    dt.TableName = "UserDefined"
                    ds.Merge(dt)


                Case "CDU REPORT", "FCC REPORT", "HYC REPORT", "REF REPORT", "PXYL REPORT"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' This section added by GMO 12/4/2007

                    Dim tQuery As String
                    Dim tProcessID As String
                    Dim tReport As String
                    Dim tReportCode As String
                    Dim tSubmissionID As String
                    Dim dStartDate As Date
                    Dim tDataSet As String
                    Dim tFactorSet As String
                    Dim tCurrency As String
                    Dim tUOM As String
                    Dim tRefineryID As String

                    ' Get values from query string
                    dStartDate = CDate(Request.QueryString("sd"))
                    tDataSet = Request.QueryString("ds")
                    tFactorSet = Request.QueryString("yr")
                    tCurrency = Request.QueryString("Currency")
                    tUOM = Request.QueryString("UOM")
                    tReport = Request.QueryString("rn").ToUpper

                    ' Get refinery ID
                    tRefineryID = GetRefineryID()

                    Dim tDecPlacesField As String

                    If tUOM.StartsWith("US") Then
                        tDecPlacesField = "USDecPlaces"
                    Else
                        tDecPlacesField = "METDecPlaces"
                    End If

                    ' Get report code from database
                    tQuery = "SELECT ReportCode FROM Report_LU WHERE upper(rtrim(ReportName)) = '" & UCase(Trim(tReport)) & "'"
                    Dim dtReportName As DataTable = QueryDb(tQuery).Tables(0)
                    tReportCode = dtReportName.Rows(0).Item("ReportCode")

                    ' Get submission ID from database
                    tQuery = "SELECT SubmissionID FROM Submissions " _
                    & "WHERE DataSet = '" + tDataSet & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dStartDate) & " AND Year(PeriodStart) = " & Year(dStartDate) _
                    & ") " _
                    & "AND RefineryID = '" + tRefineryID + "'"

                    'Throw New Exception(tQuery)

                    Dim dtSubmissionID As DataTable = QueryDb(tQuery).Tables(0)
                    tSubmissionID = dtSubmissionID.Rows(0).Item("SubmissionID")

                    ' Construct query with parameters
                    tQuery = _
                    "SELECT curd.* " _
                    & "FROM " _
                    & "CustomUnitReportData curd " _
                    & "WHERE " _
                    & "SubmissionID = " & tSubmissionID & " AND " _
                    & "rtrim(curd.ProcessID) = '" & Trim(tReportCode) & "' AND " _
                    & "(FactorSet = '" & tFactorSet & "' or FactorSet = 'N/A') AND " _
                    & "(Currency = '" & tCurrency & "' or Currency = 'N/A') " _
                    & "ORDER BY UnitID, curd.SortKey"

                    ' Get dataset for the report
                    Dim dt As DataTable = QueryDb(tQuery).Tables(0)

                    'Throw New Exception("tReportTitle = " & tReportTitle)

                    dt.TableName = "CustomUnitData"
                    ds.Merge(dt)

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' End 12/4/2007


                Case "REFINERY TRENDS REPORT"
                    ' This section added by GMO 10/2007

                    Dim tStartingDate As String = Request.QueryString("sd")
                    'Dim dStartingDate As Date
                    'Dim dFirstPreviousDate As Date
                    'Dim dSecondPreviousDate As Date
                    'Dim dThirdPreviousDate As Date

                    dStartingDate = CDate(tStartingDate)
                    dFirstPreviousDate = dStartingDate.AddMonths(-1)
                    dSecondPreviousDate = dStartingDate.AddMonths(-2)
                    dThirdPreviousDate = dStartingDate.AddMonths(-3)

                    'Dim tSelectList As String = ""
                    'Dim tTable As String

                    ' GMO 10/16/2007
                    Dim bYTD As Boolean = CType(Request.QueryString("ytd"), Boolean)
                    Dim bAVG As Boolean = CType(Request.QueryString("avg"), Boolean)
                    Dim tFields As String
                    ' END GMO

                    If (bYTD = True And bAVG = True) Or (bYTD = False And bAVG = False) Then
                        ' show total fields only
                        tFields = " TotField AS ValueField1,"
                        tReportDataFilter = "Current Month"
                    Else
                        If bYTD = True Then
                            ' show ytd fields only
                            tFields = " YTDField AS ValueField1, "
                            tReportDataFilter = "Year To Date"
                        Else
                            If bAVG = True Then
                                ' show avg fields only
                                tFields = " AVGField AS ValueField1, "
                                tReportDataFilter = "Rolling Average"
                            End If
                        End If
                    End If

                    Dim dt As DataTable = QueryDb("SELECT ChartTitle,SectionHeader,SortKey," & _
                                               " CASE WHEN LEN(AxisLabelUS) > 0 THEN ', ' + AxisLabelUS ELSE AxisLabelUS END as AxisLabelUS , " & _
                                               " CASE WHEN LEN(AxisLabelMetric) > 0 THEN ', ' + AxisLabelMetric  ELSE AxisLabelMetric END as AxisLabelMetric,DataTable, " & _
                                               tFields & _
                                               " DecPlaces " & _
                                               " FROM Chart_LU ").Tables(0)



                    'Dim dt As DataTable = QueryDb("SELECT ChartTitle,SectionHeader,SortKey," & _
                    '                           " CASE WHEN LEN(AxisLabelUS) > 0 THEN ', ' + AxisLabelUS ELSE AxisLabelUS END as AxisLabelUS , " & _
                    '                           " CASE WHEN LEN(AxisLabelMetric) > 0 THEN ', ' + AxisLabelMetric  ELSE AxisLabelMetric END as AxisLabelMetric,DataTable, " & _
                    '                           " TotField AS ValueField1," & _
                    '                           " TargetField, " & _
                    '                           " YTDField, " & _
                    '                           " AVGField, " & _
                    '                           " DecPlaces " & _
                    '                           " FROM Chart_LU ").Tables(0)

                    dt.TableName = "Chart_LU"
                    ds.Merge(dt)

                    'tTable = "MAINTAVAILCALC"
                    'tSelectList = GetSelectList(tTable)

                    dStartingDate = CDate(tStartingDate)
                    dFirstPreviousDate = dStartingDate.AddMonths(-1)
                    dSecondPreviousDate = dStartingDate.AddMonths(-2)
                    dThirdPreviousDate = dStartingDate.AddMonths(-3)

                    Dim tSQL As String = _
                    "SELECT * FROM MAINTAVAILCALC " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dStartingDate) & " AND Year(PeriodStart) = " & Year(dStartingDate) _
                    & ")" _
                    & "AND RefineryID = '" + GetRefineryID() + "') " _
                    & "AND FactorSet = '" + Request.QueryString("yr") & "'"

                    dt = QueryDb(tSQL).Tables(0)


                    dt.TableName = "MAINTAVAILCALC"
                    ds.Merge(dt)

                    tSQL = _
                    "SELECT * FROM MAINTAVAILCALC " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dFirstPreviousDate) & " AND Year(PeriodStart) = " & Year(dFirstPreviousDate) _
                    & ")" _
                    & "AND RefineryID = '" + GetRefineryID() + "') " _
                    & "AND FactorSet = '" + Request.QueryString("yr") & "'"

                    dt = QueryDb(tSQL).Tables(0)


                    dt.TableName = "MAINTAVAILCALC_FIRST_PREVIOUS"
                    ds.Merge(dt)


                    tSQL = _
                    "SELECT * FROM MAINTAVAILCALC " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dSecondPreviousDate) & " AND Year(PeriodStart) = " & Year(dSecondPreviousDate) _
                    & ")" _
                    & "AND RefineryID = '" + GetRefineryID() + "') " _
                    & "AND FactorSet = '" + Request.QueryString("yr") & "'"

                    dt = QueryDb(tSQL).Tables(0)


                    dt.TableName = "MAINTAVAILCALC_SECOND_PREVIOUS"
                    ds.Merge(dt)


                    tSQL = _
                    "SELECT * FROM MAINTAVAILCALC " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dThirdPreviousDate) & " AND Year(PeriodStart) = " & Year(dThirdPreviousDate) _
                    & ")" _
                    & "AND RefineryID = '" + GetRefineryID() + "') " _
                    & "AND FactorSet = '" + Request.QueryString("yr") & "'"


                    dt = QueryDb(tSQL).Tables(0)


                    dt.TableName = "MAINTAVAILCALC_THIRD_PREVIOUS"
                    ds.Merge(dt)

                    'tTable = "GENSUM"
                    'tSelectList = GetSelectList(tTable)

                    tSQL = _
                    "SELECT * FROM GENSUM " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dStartingDate) & " AND Year(PeriodStart) = " & Year(dStartingDate) _
                    & ")" _
                    & "AND RefineryID = '" & GetRefineryID() & "') " _
                    & "AND Currency='" & Request.QueryString("Currency") & "' " _
                    & "AND FactorSet = '" & Request.QueryString("yr") & "' " _
                    & "AND UOM='" & Request.QueryString("UOM") & "'"

                    dt = QueryDb(tSQL).Tables(0)
                    dt.TableName = "GENSUM"
                    ds.Merge(dt)

                    tSQL = _
                    "SELECT * FROM GENSUM " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dFirstPreviousDate) & " AND Year(PeriodStart) = " & Year(dFirstPreviousDate) _
                    & ")" _
                    & "AND RefineryID = '" & GetRefineryID() & "') " _
                    & "AND Currency='" & Request.QueryString("Currency") & "' " _
                    & "AND FactorSet = '" & Request.QueryString("yr") & "' " _
                    & "AND UOM='" & Request.QueryString("UOM") & "'"

                    dt = QueryDb(tSQL).Tables(0)
                    dt.TableName = "GENSUM_FIRST_PREVIOUS"
                    ds.Merge(dt)

                    tSQL = _
                    "SELECT * FROM GENSUM " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dSecondPreviousDate) & " AND Year(PeriodStart) = " & Year(dSecondPreviousDate) _
                    & ")" _
                    & "AND RefineryID = '" & GetRefineryID() & "') " _
                    & "AND Currency='" & Request.QueryString("Currency") & "' " _
                    & "AND FactorSet = '" & Request.QueryString("yr") & "' " _
                    & "AND UOM='" & Request.QueryString("UOM") & "'"

                    dt = QueryDb(tSQL).Tables(0)
                    dt.TableName = "GENSUM_SECOND_PREVIOUS"
                    ds.Merge(dt)

                    tSQL = _
                    "SELECT * FROM GENSUM " _
                    & "WHERE SubmissionID IN " _
                    & "(" _
                    & "SELECT Distinct SubmissionID FROM Submissions " _
                    & "WHERE  DataSet = '" + Request.QueryString("ds") & "' AND " _
                    & "(" _
                    & "Month(PeriodStart) = " & Month(dThirdPreviousDate) & " AND Year(PeriodStart) = " & Year(dThirdPreviousDate) _
                    & ")" _
                    & "AND RefineryID = '" & GetRefineryID() & "') " _
                    & "AND Currency='" & Request.QueryString("Currency") & "' " _
                    & "AND FactorSet = '" & Request.QueryString("yr") & "' " _
                    & "AND UOM='" & Request.QueryString("UOM") & "'"

                    dt = QueryDb(tSQL).Tables(0)
                    dt.TableName = "GENSUM_THIRD_PREVIOUS"
                    ds.Merge(dt)


                    dt = QueryDb("SELECT * FROM UserDefined WHERE SubmissionID" + _
                                " IN (SELECT SubmissionID FROM Submissions " + _
                                " WHERE RefineryID ='" + GetRefineryID() + "' )").Tables(0)
                    dt.TableName = "UserDefined"
                    ds.Merge(dt)


                    ' END GMO 10/2007

                Case "PROCESS UNIT DETAIL"
                    Dim procesLevelStmt As String = "SELECT description,c.Sortkey,c.UnitID, c.unitname, f.processid,c.Cap,c.RptCap," + _
                        "stdenergy, stdgain, ISNULL(EDCNoMult,0)*ISNULL(MultiFactor,1)/1000 as kEDC, ISNULL(UEDCNoMult,0)*ISNULL(MultiFactor,1)/1000 as kUEDC, " + _
                        "MaintEffDiv/1000 as MaintEffDiv, PersEffDiv/1000 as PersEffDiv, NEOpexEffDiv/1000 as NEOpexEffDiv " + _
                        "FROM factorcalc f ,processid_lu g ,config c WHERE " + _
                        "f.processid=g.processid AND " + _
                        "f.factorset='" + Request.QueryString("yr") + "' AND " + _
                        "c.submissionid=f.submissionid AND c.processid=f.processid AND f.unitid=c.unitid AND " + _
                        "c.processid NOT IN ('BLENDING') AND " + _
                        "f.submissionid IN " + _
                        "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                        "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                        Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + GetRefineryID() + "'" + _
                        " ) ORDER BY c.Sortkey ASC, c.UnitID ASC "


                    Dim unitOffsitesStmt As String = "SELECT EDC/1000 as TotEDC,UEDC/1000 as TotUEDC,TotRSEDC/1000 as kEDC, TotRSUEDC/1000 as kUEDC, SensHeatStdEnergy,AspStdEnergy, " + _
                        " OffsitesStdEnergy,TotStdEnergy,EstGain/DateDiff(d," + CDate(Request.QueryString("sd")) + ",DateAdd(m, 1, " + CDate(Request.QueryString("sd")) + ")) as EstGain, " + _
                        " TotRsMaintEffDiv/1000 as TotRsMaintEffDiv, MaintEffDiv/1000 as MaintEffDiv,TotRsPersEffDiv/1000 as TotRsPersEffDiv, PersEffDiv/1000 as PersEffDiv,TotRSNEOpexEffDiv/1000 as TotRSNEOpexEffDiv,  NEOpexEffDiv/1000 as NEOpexEffDiv, " + _
                        " TnkStdCap , TnkStdEDC/1000 AS TnkStdEDC,((PurElecUEDC+PurStmUEDC)/1000) AS PurchasedUtilityUEDC " + _
                        " FROM FactorTotCalc f WHERE " + _
                        " f.factorset='" + Request.QueryString("yr").ToString + "' AND " + _
                        " f.submissionid IN " + _
                        " (SELECT Distinct SubmissionID FROM Submissions WHERE DataSet='" + Request.QueryString("ds") + "' AND " + _
                        " Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + _
                        " AND Year(PeriodStart)=" + Year(CDate(Request.QueryString("sd"))).ToString + _
                        " AND RefineryID='" + GetRefineryID() + "') "

                    Dim dt As DataTable = QueryDb(procesLevelStmt).Tables(0)
                    dt.TableName = "ProcessUnits"
                    ds.Merge(dt)

                    dt = QueryDb(unitOffsitesStmt).Tables(0)
                    dt.TableName = "UnitsOffsites"
                    ds.Merge(dt)


                Case "PROCESS UNIT SCORECARD"
                    Dim capColName As String = IIf(Request.QueryString("UOM").StartsWith("US"), "Cap", "RptCap")
                    Dim pluQry As String = "SELECT  p.SortKey,p.Description, c.UnitName, c.ProcessID, c.ProcessType, c.Cap,c.RptCap, d.DisplayTextUS, d.DisplayTextMet," + _
                    " ISNULL(fc.EDCNoMult,0) * ISNULL(fc.MultiFactor,1) as EDC, ISNULL(fc.UEDCNoMult,0) * ISNULL(fc.MultiFactor,1) as UEDC, " + _
                    "fc.UtilPcnt, u.UtilPcnt as UtilPcnt_Target," + _
                    "MechAvail_Ann as MechAvail, MechAvail as MechAvail_Target, " + _
                    "OpAvail_Ann as OpAvail, OpAvail as OpAvail_Target," + _
                    "OnStream_Ann as OnStream, OnStream as OnStream_Target," + _
                    "CASE WHEN c.RptCap > 0 THEN 1000*AnnTACost/c." + capColName + " END as TACost, TACost as TACost_Target, " + _
                    "CASE WHEN c.RptCap > 0 THEN 1000*AnnRoutCost/c." + capColName + " END as RoutCost, RoutCost as RoutCost_Target, u.CurrencyCode " + _
                    " FROM 	Config c " + _
                    "INNER JOIN ProcessID_LU p on c.ProcessID = p.ProcessID " + _
                    "INNER JOIN FactorCalc fc on fc.SubmissionID = c.SubmissionID and c.UnitID = fc.UnitID " + _
                    "INNER JOIN FactorProcessCalc fpc on fc.SubmissionID = fpc.SubmissionID and fpc.ProcessID = c.ProcessID " + _
                    "INNER JOIN MaintCalc m on m.SubmissionID = c.SubmissionID and c.UnitID = m.UnitID " + _
                    "INNER JOIN MaintCost mc on mc.SubmissionID = c.SubmissionID and c.UnitID = mc.UnitID " + _
                    "INNER JOIN DisplayUnits_LU d on p.DisplayUnits = d.DisplayUnits " + _
                    "LEFT JOIN UnitTargets u on c.SubmissionID = u.SubmissionID and c.UnitID = u.UnitID " + _
                    "WHERE fc.FactorSet='" + Request.QueryString("yr") + "' AND mc.Currency ='" + Request.QueryString("currency") + "' AND fc.FactorSet=fpc.FactorSet AND C.SubmissionID  IN " + _
                    "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                    "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                    Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + GetRefineryID() + "'" + _
                    "  ) ORDER BY p.SortKey"
                    Dim dt As DataTable = QueryDb(pluQry).Tables(0)
                    dt.TableName = "ProcessUnits"
                    ds.Merge(dt)

                    Dim offsitesQry As String = "SELECT c.UnitID, c.ProcessID, c.ProcessType, c.Cap,c.RptCap, ISNULL(c.UtilPcnt,0) AS UtilPcnt, " + _
                                           "ISNULL(EDCNoMult,0) AS EDCNoMult, ISNULL(UEDCNoMult,0) AS UEDCNoMult, d.DisplayTextUS, d.DisplayTextMET " + _
                                           "FROM config c,factorcalc fc, processid_lu p ,displayunits_lu d " + _
                                           "WHERE c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits AND " + _
                                           "c.unitid = fc.unitid AND c.submissionid = fc.submissionid AND " + _
                                           "c.ProcessID IN ('STEAMGEN', 'ELECGEN', 'FCCPOWER') " + _
                                           " AND fc.factorset=" + Request.QueryString("yr").ToString + " AND c.SubmissionId IN " + _
                                           "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds").ToString + "' AND Month(PeriodStart)=" + _
                                           Month(CDate(Request.QueryString("sd").ToString)).ToString + " AND Year(PeriodStart)=" + Year(CDate(Request.QueryString("sd").ToString)).ToString + _
                                           " AND RefineryID='" + GetRefineryID() + "'" + _
                                           " ) ORDER BY p.SortKey"

                    dt = QueryDb(offsitesQry).Tables(0)
                    dt.TableName = "Generated"
                    ds.Merge(dt)


                    Dim offsitesRSQry As String = "SELECT c.UnitID, c.ProcessID, c.ProcessType, ISNULL(c.Throughput,0) As BDP, ISNULL(EDCNoMult,0) AS EDCNoMult, " + _
                                   " ISNULL(UEDCNoMult,0) AS UEDCNoMult,d.DisplayTextUS, d.DisplayTextMET  " + _
                                   " FROM ConfigRS c, FactorCalc fc,displayunits_lu d,processid_lu p  " + _
                                   "WHERE c.unitid = fc.unitid and c.submissionid = fc.submissionid  AND " + _
                                   "c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits  " + _
                                   " AND fc.factorset=" + Request.QueryString("yr").ToString + " AND c.SubmissionId IN " + _
                                   "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds").ToString + "' AND Month(PeriodStart)=" + _
                                   Month(CDate(Request.QueryString("sd").ToString)).ToString + " AND Year(PeriodStart)=" + Year(CDate(Request.QueryString("sd").ToString)).ToString + _
                                   " AND RefineryID='" + refineryId + "'" + _
                                   "  )"

                    dt = QueryDb(offsitesRSQry).Tables(0)
                    dt.TableName = "RawMaterial"
                    ds.Merge(dt)



                Case "GROSS PRODUCT VALUE"
                    Dim priceCol As String = IIf(Request.QueryString("Currency").StartsWith("US"), "PriceUS", "PriceLocal")
                    Dim yldQry As String = "SELECT SAIName as MaterialName, ISNULL(SUM(BBL),0) AS BBL, " + _
                        " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                        " ELSE " + _
                        " ISNULL(SUM(BBL*PriceLocal)/SUM(BBL),0) " + _
                        " END AS PriceLocal," + _
                        " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                        " ELSE " + _
                        " ISNULL(SUM(BBL*PriceUS)/SUM(BBL),0) " + _
                        " END AS PriceUS, " + _
                        " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                        " ELSE " + _
                        " ISNULL(SUM(BBL*" + priceCol + ")/SUM(BBL),0) " + _
                        " END AS Price, " + _
                        " ISNULL(SUM(BBL),0)*ISNULL(SUM(" + priceCol + "),0)/1000 As GPV," + _
                        " MIN(m.SortKey) AS thisSortKey,Category " + _
                        " FROM Yield y " + _
                        " INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID " + _
                        "   WHERE Category in ( 'Prod','RPF','RMI') And LubesOnly = 0 And SubmissionID IN " + _
                        "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                        "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                        Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + GetRefineryID() + "'" + _
                        " ) GROUP BY SAIName,Category ORDER BY  thisSortKey "
                    Dim dt As DataTable = QueryDb(yldQry).Tables(0)
                    dt.TableName = "Yield"
                    ds.Merge(dt)

                    Dim catQry As String = " SELECT RTRIM(Category) as Category,(CASE Category " + _
                    " WHEN 'MPROD' THEN 'Miscellaneous' " + _
                    " WHEN 'ASP' THEN 'Asphalt' " + _
                    " WHEN 'SOLV' THEN 'Speciality Solvents' " + _
                    " WHEN 'FCHEM' THEN 'Ref. Feedstocks To Chemical Plant' " + _
                    " WHEN 'FLUBE' THEN 'Ref. Feedstocks To Lube Refining' " + _
                    " WHEN 'COKE' THEN 'Saleable Petroleum Coke (FOE)' " + _
                    " WHEN 'OTHRM' THEN 'Other Raw Materials' " + _
                    "  END) " + _
                    " AS MaterialName,  ISNULL(NetBBL,0) as BBL, ISNULL(NetValueMUS,0)*1000000/ISNULL(NetBBL,1)*dbo.ExchangeRate('USD','" + Request.QueryString("Currency") + "','" + CDate(Request.QueryString("sd")).ToString + "') as Price, " + _
                    " ISNULL(NetValueMUS,0)*1000 As GPVUS  ,ISNULL(NetValueMUS,0)*1000*dbo.ExchangeRate('USD','" + Request.QueryString("Currency") + "','" + CDate(Request.QueryString("sd")).ToString + "') As GPVLocal " + _
                    " FROM materialstcalc " + _
                    " WHERE NetBBL > 0 AND Category in ('ASP', 'COKE', 'FCHEM', 'FLUBE', 'MPROD', 'SOLV', 'OTHRM') AND SubmissionID IN " + _
                    " (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                    "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                    Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + GetRefineryID() + "'" + _
                    "  )"


                    dt = QueryDb(catQry).Tables(0)
                    dt.TableName = "Category"
                    ds.Merge(dt)


                    Dim yldLubeQry As String = "SELECT SAIName as MaterialName, ISNULL(SUM(BBL),0) AS BBL, " + _
                        " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                        " ELSE " + _
                        " ISNULL(SUM(BBL*PriceLocal)/SUM(BBL),0) " + _
                        " END AS PriceLocal," + _
                        " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                        " ELSE " + _
                        " ISNULL(SUM(BBL*PriceUS)/SUM(BBL),0) " + _
                        " END AS PriceUS, " + _
                        " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                        " ELSE " + _
                        " ISNULL(SUM(BBL*" + priceCol + ")/SUM(BBL),0) " + _
                        " END AS Price, " + _
                        " ISNULL(SUM(BBL),0)*ISNULL(SUM(" + priceCol + "),0)/1000 As GPV," + _
                        " MIN(m.SortKey) AS thisSortKey " + _
                        " FROM Yield y " + _
                        " INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID " + _
                        " INNER JOIN TSort t ON t.RefineryID ='" + refineryId + "'" + _
                        "   WHERE AllowInPROD = 1 And LubesOnly = 1 And FuelsLubesCombo=1 And SubmissionID IN " + _
                    "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                    "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                    Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + GetRefineryID() + "'" + _
                    " ) GROUP BY SAIName ORDER BY  thisSortKey "

                    dt = QueryDb(yldLubeQry).Tables(0)
                    dt.TableName = "YieldLubes"
                    ds.Merge(dt)

                Case "EII AND VEI BREAKDOWN"
                    Dim refInfQry As String = "SELECT f.SubmissionID,EII,EnergyUseDay AS EIIDailyUsage,TotStdEnergy AS EIIStdEnergy," + _
                           "VEI,EstGain AS VEIStdGain,ReportLossGain AS VEIActualGain," + _
                           "SensHeatUtilCap AS SensGrossInput, SensHeatStdEnergy ,CASE WHEN (" + Request.QueryString("yr") + " < 2006) THEN '44-(0.23*A)' ELSE '35'END As SensHeatFormula,OffsitesUtilCap,OffsitesStdEnergy,AvgGravity AS Crude, " + _
                           "CASE WHEN (" + Request.QueryString("yr") + "< 2006) THEN 'Gross Input Barrels' ELSE 'Gross Non-Crude Processed Barrels' END AS BarrelDesc " + _
                           "FROM FactorTotCalc f,CrudeTot c WHERE c.SubmissionID=f.SubmissionID AND factorSet='" + Request.QueryString("yr") + "'" + _
                           " AND f.SubmissionID IN " + _
                           "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                           "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                           Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'" + _
                           " )"
                    Dim dt As DataTable = QueryDb(refInfQry).Tables(0)
                    dt.TableName = "EII"
                    ds.Merge(dt)

                    Dim funcQry As String = "SELECT c.SubmissionID, p.Description,c.SortKey,c.UnitName, c.unitid, c.ProcessID, c.processType, c.utilcap, fc.stdenergy, fc.stdgain, VEIFormulaForReport, EIIFormulaForReport, displaytextUS ,'' AS Variables " + _
                            " FROM Config c, factorcalc fc, factors f, processid_lu p, displayunits_lu d " + _
                            " WHERE c.unitid = fc.unitid AND c.submissionId = fc.submissionid  AND f.processid = c.processid AND f.processtype = c.processtype AND f.factorset='" + Request.QueryString("yr") + "'" + _
                            " AND f.factorset=fc.factorset AND c.processid = p.processid and p.displayunits = d.displayunits" + _
                            " AND c.PROCESSID NOT IN ('STEAMGEN','ELECGEN','FCCPOWER','FTCOGEN','BLENDING','TNK+BLND') AND c.submissionid IN " + _
                            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                            "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                            Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'" + _
                            " ) ORDER BY c.SortKey"
                    Dim dtFunction As DataTable = QueryDb(funcQry).Tables(0)
                    dtFunction.TableName = "Functions"


                    Dim processDataQry As String = "SELECT p.SubmissionID, c.UnitID,p.Property,SAValue, UsDescription, UsDecplaces FROM processdata p, table2_LU t, config c " + _
                            " WHERE p.unitid = c.unitid AND p.property = t.property AND c.processid = t.processid  AND c.submissionid = p.submissionid" + _
                            " AND c.submissionid IN " + _
                            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                        "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                        Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'" + _
                        " ) ORDER BY c.UnitID"

                    Dim dtProcessData As DataTable = QueryDb(processDataQry).Tables(0)
                    dtProcessData.TableName = "Process Data"


                    Dim n As Integer
                    For n = 0 To dtFunction.Rows.Count - 1
                        'Do substititons in formulas
                        Dim eiiFormula As String = dtFunction.Rows(n)("EIIFormulaForReport").ToString
                        Dim veiFormula As String = dtFunction.Rows(n)("VEIFormulaForReport").ToString
                        Dim unitID As String = dtFunction.Rows(n)("UnitID").ToString
                        Dim submissionID As String = dtFunction.Rows(n)("SubmissionID").ToString
                        Dim varRows() As DataRow = dtProcessData.Select("UnitId='" + unitID + "' AND SubmissionID='" + submissionID + "'")

                        If dtProcessData.Rows.Count > 0 Then
                            Dim j As Integer
                            Dim variables As String = ""
                            Dim fieldFormat As String = ""
                            For j = 0 To varRows.Length - 1
                                Select Case varRows(j)("UsDecPlaces")
                                    Case 0
                                        fieldFormat = "{0:#,##0}"
                                    Case 1
                                        fieldFormat = "{0:#,##0.0}"
                                    Case 2
                                        fieldFormat = "{0:N}"
                                End Select
                                eiiFormula = eiiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))
                                veiFormula = veiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))

                                variables += "<tr>"
                                variables += "<td colspan=2>&nbsp;</td>"
                                variables += "<td>" + Chr(Asc("A") + j) + " - " + varRows(j)("UsDescription").Trim.ToString + "</td>"
                                variables += "<td  align=right>" + String.Format(fieldFormat, varRows(j)("SAValue")).ToString.Trim + "</td>"
                                variables += "<td>&nbsp;</td>"
                                variables += "</tr>"
                            Next
                            dtFunction.Rows(n)("EIIFormulaForReport") = eiiFormula
                            dtFunction.Rows(n)("VEIFormulaForReport") = veiFormula
                            dtFunction.Rows(n)("Variables") = variables

                        End If
                    Next
                    ds.Merge(dtFunction)
                    ds.Merge(dtProcessData)

                    Dim asphaltQry As String = "SELECT SubmissionID,OffsitesUtilCap,OffsitesStdEnergy,CASE WHEN (" + Request.QueryString("yr") + "< 2006) THEN '40+4*(A)' ELSE '15+0.6*(A)' END As OffsitesFormula, Complexity, ASPUtilCap, ASPStdEnergy,CASE WHEN (" + Request.QueryString("yr") + "< 2006) THEN  115 ELSE 200 END as ASPEIIFormula " + _
                          "FROM FactorTotCalc WHERE factorSet='" + Request.QueryString("yr") + "'" + _
                          " AND SubmissionID IN " + _
                          "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                          "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                          Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'" + _
                          " )"
                    Dim dtAsphalt As DataTable = QueryDb(asphaltQry).Tables(0)
                    dtAsphalt.TableName = "Asphalt"
                    ds.Merge(dtAsphalt)

                Case "OPERATING EXPENSES"
                    Dim opexQry As String = "SELECT *,(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS 'PurchasedEnergy', " + _
                      "(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS 'ProducedEnergy' " + _
                      "FROM Opex WHERE DataType='ADJ' AND Currency='@Currency' AND SubmissionID IN " + _
                      "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='@Dataset' AND Month(PeriodStart)=@Month " + _
                      "AND Year(PeriodStart)=@Year " + _
                      "AND RefineryID='@RefineryID')"
                    opexQry = opexQry.Replace("@Currency", Request.QueryString("currency").ToString)
                    opexQry = opexQry.Replace("@Dataset", Request.QueryString("ds").ToString)
                    opexQry = opexQry.Replace("@Month", Month(CDate(Request.QueryString("sd").ToString)).ToString)
                    opexQry = opexQry.Replace("@Year", Year(CDate(Request.QueryString("sd").ToString)).ToString)
                    opexQry = opexQry.Replace("@RefineryID", refineryId)


                    Dim opexCalcQry As String = "SELECT *,(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS 'PurchasedEnergy', " + _
                     "(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS 'ProducedEnergy' " + _
                      "FROM OpexCalc WHERE DataType IN('C/BBL','UEDC') AND FactorSet=@FactorSet AND Currency='@Currency' AND SubmissionID IN " + _
                      "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='@Dataset' AND Month(PeriodStart)=@Month " + _
                      "AND Year(PeriodStart)=@Year " + _
                      "AND RefineryID='@RefineryID')"
                    opexCalcQry = opexCalcQry.Replace("@Currency", Request.QueryString("currency").ToString)
                    opexCalcQry = opexCalcQry.Replace("@Dataset", Request.QueryString("ds").ToString)
                    opexCalcQry = opexCalcQry.Replace("@Month", Month(CDate(Request.QueryString("sd").ToString)).ToString)
                    opexCalcQry = opexCalcQry.Replace("@Year", Year(CDate(Request.QueryString("sd").ToString)).ToString)
                    opexCalcQry = opexCalcQry.Replace("@FactorSet", Request.QueryString("yr").ToString)
                    opexCalcQry = opexCalcQry.Replace("@RefineryID", refineryId)
                    ds = QueryDb(opexQry)

                    Dim ds2 As DataSet = QueryDb(opexCalcQry)
                    ds.Merge(ds2)
                    ds.Tables(0).TableName = "Opex"

            End Select

            If ds.Tables.Count = 0 Then
                Exit Sub
            End If

            Dim pg As New GenrateInTableHtml.Page
            Dim replacementValues As New Hashtable
            replacementValues.Add("ReportPeriod", Format(CDate(Request.QueryString("sd")), "MMMM yyyy"))
            replacementValues.Add("IfTargetOn", Request.QueryString("target"))
            replacementValues.Add("IfYtdOn", Request.QueryString("ytd"))
            replacementValues.Add("IfAvgOn", Request.QueryString("avg"))
            'replacementValues.Add("CurrencyCode", Request.QueryString("currency"))

            ' GMO 10/16/2007
            If tReportName = "REFINERY TRENDS REPORT" Then
                replacementValues.Add("ReportDataFilter", tReportDataFilter)
                replacementValues.Add("PreviousPeriod", Format(dFirstPreviousDate, "MMMM yyyy"))
                replacementValues.Add("SecondPeriod", Format(dSecondPreviousDate, "MMMM yyyy"))
                replacementValues.Add("ThirdPeriod", Format(dThirdPreviousDate, "MMMM yyyy"))
            End If
            ' END GMO

            Dim htmlText As String = pg.createPage(ds, Request.QueryString("rn").ToUpper, Request.QueryString("UOM").Trim(), Request.QueryString("currency").Trim(), replacementValues)

            Response.Write(htmlText)
            'Dim htmlControl As New LiteralControl
            'htmlControl.Text = htmlText

            'If Not IsNothing(htmlControl) Then
            '    phReport.Controls.Add(htmlControl)
            'End If
        Else
            Response.Redirect("CalcsAreNotFinished.htm")
        End If


    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' This section added by GMO 12/6/2007
    ' Function invoked from HTML in order to get long description
    ' for the report title.  Used only for process unit reports.
    Public Function GetReportTitle(ByVal tProcessID As String) As String
        Dim tQuery As String
        Dim tReportTitle As String
        Dim tReportCode As String

        ' First 3 or 4 characters contain code
        tReportCode = Mid(tProcessID, 1, 4)

        ' Construct query with parameters
        tQuery = "SELECT Description FROM ProcessID_LU WHERE ProcessID = '" & Trim(tReportCode) & "'"

        ' Get dataset for the report
        Dim dt As DataTable = QueryDb(tQuery).Tables(0)
        tReportTitle = dt.Rows(0).Item("Description") & " Report"

        GetReportTitle = tReportTitle

    End Function
    ' END GMO 12/4/2007
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    'Private Function GetSelectList(ByVal tTableName As String) As String
    '    Dim dsSelectList As New DataSet
    '    Dim bYTD As Boolean = Request.QueryString("ytd")
    '    Dim bAvg As Boolean = Request.QueryString("avg")
    '    Dim tNameSuffix As String
    '    Dim tSQL As String
    '    Dim tTable As String
    '    Dim iColumnCount As Integer
    '    Dim i As Integer
    '    Dim tColName As String
    '    Dim tSelectList As String = ""
    '    Dim tSuffix As String

    '    If (bYTD = True And bAvg = True) Or (bYTD = False And bAvg = False) Then
    '        tNameSuffix = "NONE"
    '    Else
    '        If bYTD = True Then
    '            tNameSuffix = "_YTD"
    '        Else
    '            If bAvg = True Then
    '                tNameSuffix = "_Avg"
    '            End If
    '        End If
    '    End If

    '    tSQL = _
    '    "SELECT " _
    '    & "o.name, " _
    '    & "c.name, " _
    '    & "SUBSTRING(c.name, (len(c.name)-3), 4) as NameSuffix, " _
    '    & "SUBSTRING(c.name, 1, (len(c.name)-3)) as NamePrefix " _
    '    & "FROM sysobjects o, syscolumns c " _
    '    & "where " _
    '    & "o.name = '" & tTable & "' " _
    '    & "and " _
    '    & "o.id = c.id " _
    '    & "order by NameSuffix desc "

    '    Dim dtSelectList As DataTable = QueryDb(tSQL).Tables(0)
    '    dsSelectList.Merge(dtSelectList)

    '    iColumnCount = dsSelectList.Tables(0).Columns.Count()

    '    If tNameSuffix <> "NONE" Then
    '        For i = 0 To iColumnCount
    '            tColName = dsSelectList.Tables(0).Columns(i).ToString
    '            tSuffix = Mid(Trim(tColName), (Len(tColName) - 4), 4)

    '            If (tSuffix = tNameSuffix) Then
    '                tSelectList = tSelectList & tColName & ", "
    '            Else
    '                If (tSuffix <> "rget") Then
    '                    tSelectList = tSelectList & tColName & ", "
    '                End If
    '            End If
    '        Next
    '    Else
    '        For i = 0 To iColumnCount
    '            tColName = dsSelectList.Tables(0).Columns(i).ToString
    '            tSuffix = Mid(Trim(tColName), (Len(tColName) - 4), 4)

    '            If (tSuffix <> "rget") And (tSuffix <> "_YTD") And (tSuffix <> "_Avg") Then
    '                tSelectList = tSelectList & tColName & ", "
    '            End If
    '        Next
    '    End If

    'End Function ' GetSelectList()_


    'Private Sub TransmitFile(ByVal [output] As String, ByVal extension As String)
    '    Dim filename As String = Page.MapPath("data\report." & extension)
    '    'Dim c1Report As New C1.Web.C1WebReport.C1WebReport
    '    ' Delete old data for precaution 
    '    'If System.IO.File.Exists(filename) Then
    '    'System.IO.File.Delete(filename)
    '    'End If

    '    ' c1Report.Report.RenderToFile(filename, GetFileType([output]))
    '    'Response.WriteFile(filename)
    '    'Dim file As System.IO.FileInfo = New System.IO.FileInfo(filename)

    '    Response.Clear()
    '    Response.ClearHeaders()
    '    Response.AddHeader(C_HTTP_HEADER_CONTENT, C_HTTP_ATTACHMENT & "report." & extension)
    '    Response.ContentType = "application/octet-stream"
    '    'Response.AddHeader(C_HTTP_CONTENT_LENGTH, File.Length())
    '    'Response.TransmitFile(filename)
    '    Response.Flush()

    'End Sub

    'Private Function GetFileType(ByVal [output] As String) As C1.Win.C1Report.FileFormatEnum
    '    Dim ct As C1.Win.C1Report.FileFormatEnum = C1.Win.C1Report.FileFormatEnum.Text
    '    Try
    '        ct = CType([Enum].Parse(GetType(C1.Win.C1Report.FileFormatEnum), [output]), C1.Win.C1Report.FileFormatEnum)
    '    Catch
    '    End Try
    '    Return ct
    'End Function 'getChartType


    'Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    ' C1WebReport1.ShowPDF()
    'End Sub

    'Private Sub WordMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    'Private Sub RTFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("RTF", "rtf")
    'End Sub

    'Private Sub PDFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("PDF", "pdf")
    'End Sub

    'Private Sub ExcelMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub

    'Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub


End Class
