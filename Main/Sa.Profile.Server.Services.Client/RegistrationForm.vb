Imports System.IO

Public Class RegistrationForm
    Inherits System.Windows.Forms.Form

#Region "Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tbxKey As System.Windows.Forms.TextBox
    Friend WithEvents radioDemo As System.Windows.Forms.RadioButton
    Friend WithEvents radioPuchase As System.Windows.Forms.RadioButton
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtCompany As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.radioPuchase = New System.Windows.Forms.RadioButton
        Me.radioDemo = New System.Windows.Forms.RadioButton
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtLocation = New System.Windows.Forms.TextBox
        Me.txtCompany = New System.Windows.Forms.TextBox
        Me.tbxKey = New System.Windows.Forms.TextBox
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(392, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 32)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Register"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.RosyBrown
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Controls.Add(Me.txtLocation)
        Me.Panel1.Controls.Add(Me.txtCompany)
        Me.Panel1.Location = New System.Drawing.Point(24, 16)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(360, 208)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.radioPuchase)
        Me.Panel2.Controls.Add(Me.radioDemo)
        Me.Panel2.Location = New System.Drawing.Point(136, 144)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(192, 40)
        Me.Panel2.TabIndex = 8
        '
        'radioPuchase
        '
        Me.radioPuchase.Location = New System.Drawing.Point(80, 8)
        Me.radioPuchase.Name = "radioPuchase"
        Me.radioPuchase.Size = New System.Drawing.Size(72, 16)
        Me.radioPuchase.TabIndex = 1
        Me.radioPuchase.Text = "Purchase"
        '
        'radioDemo
        '
        Me.radioDemo.Location = New System.Drawing.Point(8, 8)
        Me.radioDemo.Name = "radioDemo"
        Me.radioDemo.Size = New System.Drawing.Size(56, 16)
        Me.radioDemo.TabIndex = 0
        Me.radioDemo.Text = "Demo"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 24)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Demo or Buy"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 24)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Password"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 24)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Location"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 24)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Company"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(130, 104)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(96, 20)
        Me.txtPassword.TabIndex = 2
        Me.txtPassword.Text = ""
        '
        'txtLocation
        '
        Me.txtLocation.Location = New System.Drawing.Point(128, 64)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(96, 20)
        Me.txtLocation.TabIndex = 1
        Me.txtLocation.Text = ""
        '
        'txtCompany
        '
        Me.txtCompany.Location = New System.Drawing.Point(128, 24)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(96, 20)
        Me.txtCompany.TabIndex = 0
        Me.txtCompany.Text = ""
        '
        'tbxKey
        '
        Me.tbxKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbxKey.Location = New System.Drawing.Point(24, 248)
        Me.tbxKey.Multiline = True
        Me.tbxKey.Name = "tbxKey"
        Me.tbxKey.Size = New System.Drawing.Size(480, 72)
        Me.tbxKey.TabIndex = 2
        Me.tbxKey.Text = ""
        '
        'RegistrationForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.IndianRed
        Me.ClientSize = New System.Drawing.Size(544, 326)
        Me.Controls.Add(Me.tbxKey)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "RegistrationForm"
        Me.Text = "Register"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim registor As New Solomon.Registration.RegistrationService
        'Dim registor As New Production.Registration.RegistrationService
        Dim key As String
        Try

            key = registor.ActivateApplication(txtPassword.Text, txtCompany.Text, txtLocation.Text)

            tbxKey.Text = key
            If Not Directory.Exists("_CERT") Then
                Directory.CreateDirectory("_CERT")
            End If

            Dim writer As StreamWriter = File.CreateText("_CERT\policy.ct")

            writer.WriteLine(key)
            writer.Close()
        Catch ex As Exception
            Console.WriteLine(ex)
            MessageBox.Show(ex.Message)
        End Try


    End Sub
End Class
